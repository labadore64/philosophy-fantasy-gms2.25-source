#define duel_replay_start
// initializes a replay

global.duel_actions = -1;
global.duel_actions[0] = "";

var file = file_text_open_read(argument[0])

global.duel_random = real(file_text_readln(file))
global.first_player = real(file_text_readln(file))
global.duel_player[0] = file_text_readln(file)
global.duel_player[1] = file_text_readln(file)
global.dont_record = true;

// skip separator line
file_text_readln(file)

var counter = 0;
// populate actions
while(!file_text_eof(file)){
	var val = file_text_read_string(file)
	file_text_readln(file)
	if(val != ""){
		global.duel_actions[counter]=val;
		counter++;
	}
}
show_debug_message("hello!!");
file_text_close(file);

return 0;