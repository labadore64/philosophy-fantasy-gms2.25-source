#define replayDuel_create
// this is the create code for the replay menu
var nextstring = file_find_first(working_directory+"\\replay\\*.txt", 0);
var counter = 0;

while(nextstring != ""){
	replay_filename[counter] = nextstring
	counter++
	nextstring = file_find_next();
}
replay_total = counter;

file_find_close();

for(var i = 0; i < replay_total; i++){
	var filenabe = working_directory+"\\replay\\" + replay_filename[i];
	if(file_exists(filenabe)){
		var file = file_text_open_read(filenabe);
		// skip first two lines
		file_text_readln(file);
		file_text_readln(file);
		// next two lines contains opponent info
		replay_player[i,0] = text_file_readln_correct(file);
		replay_player[i,1] = text_file_readln_correct(file);
		
		// get the date
		replay_date[i] = text_file_readln_correct(file);
		menupos = i;
		replayDuel_update();
		menu_add_option(replay_char_name[0] + " VS. " + replay_char_name[1],"",-1,-1)
		
		file_text_close(file);
	}
}
menupos = 0;
if(replay_total == 0){
	menu_size = 0;	
}

replayDuel_update();

menu_draw = replayDuel_drawInternal
menu_up = freeDuel_Up
menu_down = freeDuel_Down
menu_select = replayDuel_select
menu_update_values = replayDuel_update

fade_surface = -1
fade_surface_updated = false;

duel = false;
running_object = noone;

translation_rate_start = 25;
if(global.menu_animate){
	translation_start = 150;
} else {
	translation_start = 0	
}

translation_decay_rate = .975

translation = translation_start
translation_rate = translation_rate_start;

menu_help = "replay"

draw_width = 360
mousepos_x = 170
mousepos_y = 10

ax_title = "Select Replay"

return 0;

#define replayDuel_select
// defines the select event for replayDuel.
activated = true;
var the_replay = replay_filename[menupos];
show_debug_message("hello??");
running_object = instance_create(0,0,fadeOut);

return 0;

#define replayDuel_step
// does the step event for replayDuel

// start input
if(destroy > 0){
	destroy--;
} else if(destroy == 0){
	instance_destroy();
}

if(activated){
	if(!instance_exists(running_object)){
		instance_destroy();

		var complete = -1;
		if(replay_filename[menupos] != ""){
			complete = duel_startPlayback(working_directory+"\\replay\\" + replay_filename[menupos])
		}
		if(complete == -1){
			replay = false;	
			
		} else {
			spr_unload_card();	
			room_goto(BattleRoom)
		}
		mouseHandler_clear();
		menu_pop();
	}
} else {

	if(objGenericCanDo()){
		if(global.inputEnabled){

			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_LEFT]) ||
				global.gamepad_state_array[KEYBOARD_KEY_LEFT] == KEY_STATE_HOLD){
				menuParent_moveUp();
				keypress_this_frame = true;
			}

			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_RIGHT]) ||
					global.gamepad_state_array[KEYBOARD_KEY_RIGHT] == KEY_STATE_HOLD){
				menuParent_moveDown();
				keypress_this_frame = true;
			}

			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_UP]) ||
					global.gamepad_state_array[KEYBOARD_KEY_UP] == KEY_STATE_HOLD){
				freeDuel_Up()
				keypress_this_frame = true;
			}

			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_DOWN]) ||
					global.gamepad_state_array[KEYBOARD_KEY_DOWN] == KEY_STATE_HOLD){
				freeDuel_Down();
				keypress_this_frame = true;
			}

			var selected = false;

			if(double_select){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT]) ||
					keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT2]) ||
					global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

					replayDuel_select();
					selected = true
				}
			} else {
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT]) ||
					global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

					replayDuel_select();
					selected = true
				}
			}
			
			if(!selected){
				check_input_cancel_pressed();	
			}
			
			

			if(!keypress_this_frame){
				if(global.drawing_enabled){
					if(mouse_check_button_pressed(mb_right)){
						if(!MouseHandler.clicked){
							menuParent_cancel();
							keypress_this_frame = true;
							with(MouseHandler){
								clicked = true
								clicked_countdown = 5	
							}
						}
					} 
					
					if(enable_scrollwheel && !keypress_this_frame){
						if(mouse_wheel_down()){
							if(menu_up > -1){
								option_top+=scrollwheel_skip;
								if(option_top > menu_count - per_line){
									option_top = menu_count - per_line
								}
								if (option_top < 0){
									option_top = 0	
								}
								keypress_this_frame = true;
							}
						} else if(mouse_wheel_up()){
							if(menu_down > -1){
								option_top-=scrollwheel_skip;
								if (option_top < 0){
									option_top = 0	
								}
								keypress_this_frame = true;
							}
						}
					}
				}
			}
				
			if(!keypress_this_frame){
				replayDuel_mouseHandle()
			}
			
			keypress_this_frame = false
		}
	}
}

// end input

if(translation >= 0){
	surface_update = true;
	translation-=ScaleManager.timer_diff*translation_rate;

	translation_rate=translation_rate*translation_decay_rate;

	if(translation < 0 ){
		translation = 0	
	}
} else {
	translation = 0;
}

if(alpha_adder < alpha_max){
	surface_update=true
	alpha_adder += ScaleManager.timer_diff*alpha_rate	
	if(alpha_adder > alpha_max){
		alpha_adder = alpha_max	
	}
}
return 0;

#define replayDuel_mouseHandle
/// @description Insert description here
// You can write your code in this editor
if(global.drawing_enabled && global.mouse_active){
	with(MouseHandler){
		if(clicked_countdown > -1){
			if(clicked_countdown == 0){
				clicked = false;
			}
			clicked_countdown--
		}
	
		menu_hover = false;
		var hover = false;
	
		if(clicked_countdown <= -1){
			if(active && enabled){

				mouse_xprevious = mousepos_x
				mouse_yprevious = mousepos_y
				
				if(mouse_xprevious != mousepos_x ||
					mouse_yprevious != mousepos_y){
					global.le_mouse = true;	
				}
		
				clicked_this_frame = false

				var clickedz = false;
				var posi = mouse_active_position 
				if(ScaleManager.updated){
					mouseHandler_update_values();
				}

				var mousex = window_mouse_get_x();
				var mousey = window_mouse_get_y()
		
				mousepos_x = mousex;
				mousepos_y = mousey

				selected = -1;	
		
				if(mouse_xprevious != mousex || mouse_yprevious != mousey){
					ignore_menu = false
				}

				if(!ignore_menu && !clickedz){
					if(instance_exists(mouse_menu_obj)){
						var arrays = mouse_menu_script;
	
	
						with(mouse_menu_obj){
							posi = menupos
							if(active){
								if(!keypress_this_frame){
									var sizer = min(option_top + per_line, MouseHandler.mouse_menu_count);
									var counter = 0;
									for(var i = option_top; i < sizer; i++){
										if(mousex >= MouseHandler.mouse_menu_points_trans[i,0] && 
											mousex <= MouseHandler.mouse_menu_points_trans[i,2] &&
											mousey >= MouseHandler.mouse_menu_points_trans[i,1]-MouseHandler.mouse_menu_spacer*option_top &&
											mousey <= MouseHandler.mouse_menu_points_trans[i,3]-MouseHandler.mouse_menu_spacer*option_top){
											hover=true;
											if(mouse_check_button_pressed(mb_left)){
												global.le_mouse = true;
														
												menuParent_selectSound();
												if(arrays[i] != -1){
													menupos = i;
													selected_draw_index = counter;
													replayDuel_update();
													replayDuel_select();
													clickedz = true
													break;
												}
											}
						
											if(posi != i){
												menupos = i;
												if(menupos > menu_count-1){
													menupos = menu_count-1;	
												}
												posi = menupos;
												with(MouseHandler){
													clicked_this_frame = true	
												}
												alpha_adder = .1
												selected_draw_index = counter;
												replayDuel_update();
												tts_say(menu_name[menupos])
											}

										}
										counter++;
									}
								} else {
									// if key pressed, update menupos so it doesn't trigger tts	
									posi = menupos;
								}
							}
						}
					}
				}
	
				if(clickedz){
					clicked = true
					clicked_countdown = 5	
					enabled = false
				}
	
				mouse_active_position = posi
	
				for(var i = 0; i < mouse_count; i++){
					mouse_hovered_last[i] = mouse_hovered[i]
				}
				menu_hover = hover
			}
		}
	}
}

return 0;

#define replayDuel_cleanUp
// this cleans up the replayDuel object.
event_inherited();

if(surface_exists(fade_surface)){
	surface_free(fade_surface);	
}

if(instance_exists(obj_data)){
	with(mainMenu){
		spr_unload_card()
		var counter = 0;
		for(var i = 0; i < obj_data.cards_total; i++){
			if(ds_list_find_index(obj_data.current_deck,i) > -1){
				spr_load_card_front(i)
			}
		}
	}
}

for(var i = 0; i < 2; i++){
	if(surface_exists(replay_char_surf[i])){
		surface_free(replay_char_surf[i]);
	}
}
return 0;

#define replayDuel_event0
// does the event0 for replayDuel
event_inherited();
mouseHandler_clear()
mouseHandler_menu_default()
return 0;

#define replayDuel_draw
// draw event for the replayDuel function.

with(mainMenu){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		mainMenu_DrawBackground()	
	}
}

if(!surface_exists(surf)){
	surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	surface_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h);
		surface_update = true;
	}
}

for(var i = 0; i < 2; i++){
	var scale = .35

	if(!surface_exists(replay_char_surf[i])){
		replay_char_surf[i] = surface_create_access(scale*512*global.scale_factor,scale*512*global.scale_factor)	
		replay_surf_update[i]=true;
	} else {
		if(ScaleManager.updated){
			surface_resize(replay_char_surf[i],scale*512*global.scale_factor,scale*512*global.scale_factor);
			replay_surf_update[i]=true;
		}
	}

	if(replay_surf_update[i]){
		//preview_philosopher	
		surface_set_target(replay_char_surf[i])
		draw_clear_alpha(c_black,0)
		draw_set_alpha(1)
		gpu_set_colorwriteenable(true,true,true,true)

		draw_clear_alpha(c_black,0);
		if(!replay_custom[i]){
			var bgcolor = $222222

			draw_set_color(bgcolor)
			var adjust = 0;
			draw_circle_noratio((256-adjust)*scale,(256-adjust)*scale,75,false)
			gpu_set_colorwriteenable(true,true,true,false)
			if(replay_sprite[i] > 0){
				draw_sprite_extended_noratio(replay_sprite[i],0,0,0,scale,scale,0,c_white,1)
			}
		} else {
			
			var bgcolor = portrait_color[i,CHARA_CUSTOM_COL_BACKGROUND]

			draw_set_color(bgcolor)
			var adjust = 0;
			draw_circle_noratio((256-adjust)*scale,(256-adjust)*scale,75,false)
			gpu_set_colorwriteenable(true,true,true,false)
			
			var hair_color = portrait_color[i,CHARA_CUSTOM_COL_HAIR]
			var skin_color = portrait_color[i,CHARA_CUSTOM_COL_SKIN]
			var eye_color = portrait_color[i,CHARA_CUSTOM_COL_EYE]
			var mouth_color = portrait_color[i,CHARA_CUSTOM_COL_MOUTH]
		
			var earring_color = portrait_color[i,CHARA_CUSTOM_COL_EARRING]
			var hat_color = portrait_color[i,CHARA_CUSTOM_COL_HAT]
			var glasses_color = portrait_color[i,CHARA_CUSTOM_COL_GLASSES]
			var shirt_color = portrait_color[i,CHARA_CUSTOM_COL_SHIRT]
			var necklace_color = portrait_color[i,CHARA_CUSTOM_COL_NECKLACE]
			
				custom_face_draw1(scale,-15-adjust,-35-adjust,
					hair_color,skin_color,shirt_color,necklace_color,
					portrait_property[i,CHARA_CUSTOM_VAL_CUTE],
					portrait_property[i,CHARA_CUSTOM_VAL_BACKHAIR],
					portrait_property[i,CHARA_CUSTOM_VAL_BODY],
					portrait_property[i,CHARA_CUSTOM_VAL_NECK],
					portrait_property[i,CHARA_CUSTOM_VAL_SHIRT],
					portrait_property[i,CHARA_CUSTOM_VAL_NECKLACE])
				custom_face_draw2(scale,-15-adjust,-35-adjust,
					skin_color,eye_color,hair_color,mouth_color,
					portrait_property[i,CHARA_CUSTOM_VAL_FACE],
					portrait_property[i,CHARA_CUSTOM_VAL_NOSE],
					portrait_property[i,CHARA_CUSTOM_VAL_MOUTH],
					portrait_property[i,CHARA_CUSTOM_VAL_EYE],
					portrait_property[i,CHARA_CUSTOM_VAL_EYEBROW],
					portrait_property[i,CHARA_CUSTOM_VAL_SNOUT],
					portrait_color[i,CHARA_CUSTOM_COL_SNOUT])
				custom_face_draw3(scale,-15-adjust,-35-adjust,
					hair_color,skin_color,earring_color,hat_color,glasses_color,
					portrait_property[i,CHARA_CUSTOM_VAL_GLASSES],
					portrait_property[i,CHARA_CUSTOM_VAL_EAR],
					portrait_property[i,CHARA_CUSTOM_VAL_EARRING],
					portrait_property[i,CHARA_CUSTOM_VAL_FRONTHAIR],
					portrait_property[i,CHARA_CUSTOM_VAL_HAT]);
			}
		gpu_set_colorwriteenable(true,true,true,true)

		surface_reset_target();
		replay_surf_update[i] = false;
		surface_update=true
	}
}

if(surface_update){
	surface_set_target(surf)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)

	draw_set_color(c_black)
	draw_set_alpha(.75)
	draw_rectangle_noratio(0,0,800,option_y_offset+2-10,false)
	draw_rectangle_noratio(40,option_y_offset+3-10,400,600,false)
	draw_set_alpha(1)
	draw_set_color(c_white)
	draw_set_font(global.largeFont)
	draw_text_transformed_noratio(50,10,"Replays",4,4,false)
	draw_set_font(global.normalFont)
	
	// draw actual options
	var counter = 0;
	for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
					
		draw_set_color(c_black)
		draw_set_alpha(.85)
		draw_rectangle_noratio(40,option_y_offset+3-10 + option_spacer*counter,
								400,option_y_offset-3-10 + option_spacer*(counter+1),
								false)	
					
		//gpu_set_colorwriteenable(true,true,true,false)
		draw_set_alpha(1)
		draw_set_color(c_white)
		
		draw_set_alpha(1)
		draw_text_transformed_noratio(10+option_x_offset,2+option_y_offset + option_spacer*counter,
							string_copy(menu_name[i],1,30),2,2,0)
		draw_text_transformed_noratio(10+option_x_offset,25+2+option_y_offset + option_spacer*counter,
							string_copy(replay_date[i],1,30),2,2,0)
		
		draw_set_halign(fa_left)
		
		counter++;
	}

	var xmove = 220;
	var ymove = 250;
	
	var mover = 20
	
	// draw names
	draw_sprite_extended_noratio(spr_replay_name,0,650,210+mover,.75,.75,0,$FF7777,1);
	draw_sprite_extended_noratio(spr_replay_name,0,550,460+mover,-.75,.75,0,$7777FF,1);
	
	for(var i = 0; i < 2; i++){
		draw_surface_ext(replay_char_surf[i],(400+xmove*i)*global.scale_factor,(115+mover+ymove*i)*global.scale_factor,1,1,0,c_white,1)
	}

	if(menu_sprite[menupos] > -1){
		draw_sprite_extended_noratio(menu_sprite[menupos],0,455+translation,120+mover,1,1,0,c_white,1)
	}
	
	// draw vs
	draw_sprite_extended_noratio(spr_replay_vs,0,600,330+mover,.75,.75,0,c_white,1);
	
	// draw names
	draw_set_halign(fa_center)
	draw_set_color(c_white)
	draw_text_transformed_noratio(650+25,210-26+mover,
						string_copy(replay_char_name[0],1,30),3,3,0)
	
	draw_text_transformed_noratio(550-25,460-28+mover,
						string_copy(replay_char_name[1],1,30),3,3,0)	
	
	draw_set_halign(fa_left)
	
	// draw thingie
	surface_reset_target();
	surface_update = false;
}
draw_set_alpha(1)
draw_surface(surf,global.display_x,global.display_y);
	
gpu_set_blendmode(bm_add)


draw_set_color(c_white)
if(global.menu_animate){
	draw_set_alpha(alpha_adder);
} else {
	draw_set_alpha(alpha_max)
}
if(global.high_contrast){
	draw_rectangle_ratio(40,option_y_offset+3-10 + option_spacer*selected_draw_index,
				400,option_y_offset-3-10 + option_spacer*(1+selected_draw_index),
				false)
} else {
	draw_rectangle_color_ratio(40,option_y_offset+3-10 + option_spacer*selected_draw_index,
				400,option_y_offset-3-10 + option_spacer*(1+selected_draw_index),
	$333333,$333333,
	c_white,c_white,
	false)
}


gpu_set_blendmode(bm_normal)
	
with(fadeOut){
	draw_fade();	
}
	
draw_letterbox();
return 0;