{
    "id": "0b8a3002-de82-4761-b8b0-aedb13837c00",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "PaidMode_Replay",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2021-04-12 11:04:41",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "6fc769fc-d657-40c2-a054-b9b88797192b",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "replay_menu.gml",
            "final": "",
            "functions": [
                {
                    "id": "10b7a440-6017-424f-a2f0-1be9e1527e0a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_create",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_create",
                    "returnType": 2
                },
                {
                    "id": "aff2f3fb-2a44-4c79-b23a-92301545f56c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_step",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_step",
                    "returnType": 2
                },
                {
                    "id": "4a55da94-3ca1-43f9-9a6e-867a77a651ba",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_cleanUp",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_cleanUp",
                    "returnType": 2
                },
                {
                    "id": "4bb8e581-3ac5-4db2-ab4e-c2a114ef02a0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_event0",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_event0",
                    "returnType": 2
                },
                {
                    "id": "7ab3339d-c4be-40c3-b3a7-9e8beeec40af",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_draw",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_draw",
                    "returnType": 2
                },
                {
                    "id": "26de1312-e6f2-4cd8-aaab-635f2b90a99c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_select",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_select",
                    "returnType": 2
                },
                {
                    "id": "3c926f64-4681-4e9b-93e4-5d7aa503cc56",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "replayDuel_mouseHandle",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "replayDuel_mouseHandle",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "10b7a440-6017-424f-a2f0-1be9e1527e0a",
                "aff2f3fb-2a44-4c79-b23a-92301545f56c",
                "4a55da94-3ca1-43f9-9a6e-867a77a651ba",
                "4bb8e581-3ac5-4db2-ab4e-c2a114ef02a0",
                "7ab3339d-c4be-40c3-b3a7-9e8beeec40af",
                "26de1312-e6f2-4cd8-aaab-635f2b90a99c",
                "3c926f64-4681-4e9b-93e4-5d7aa503cc56"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "022cf6dc-2ade-4243-8850-52ba511ab9a5",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": -1,
            "filename": "replay_func.gml",
            "final": "",
            "functions": [
                {
                    "id": "edc3a56e-75f4-4596-96c0-fb692f7b6391",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "duel_replay_start",
                    "help": "",
                    "hidden": false,
                    "kind": 2,
                    "name": "duel_replay_start",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "edc3a56e-75f4-4596-96c0-fb692f7b6391"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": -1,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "0.0.1"
}