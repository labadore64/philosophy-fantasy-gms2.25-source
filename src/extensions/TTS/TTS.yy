{
    "id": "453cc063-5b0c-41ab-a1f4-0d7d2f05a3ac",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "TTS",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 64,
    "date": "2020-20-22 09:06:23",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "c9875c04-0f24-4c0a-b469-8dd2feffec89",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 35218731827264,
            "filename": "Speech.dll",
            "final": "tts_destroy",
            "functions": [
                {
                    "id": "fa018a46-27a0-464e-8d82-11a30a07f8a5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "initialize",
                    "help": "initializes tts",
                    "hidden": false,
                    "kind": 1,
                    "name": "tts_create",
                    "returnType": 2
                },
                {
                    "id": "1f1f9ed2-5b9f-43cc-b36f-09385d0a4e18",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "uninitialize",
                    "help": "uninitialize",
                    "hidden": false,
                    "kind": 1,
                    "name": "tts_destroy",
                    "returnType": 2
                },
                {
                    "id": "a945deb6-4add-4f6e-adb5-6f95badff8ce",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "SpeakString",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "tts_speak_text",
                    "returnType": 2
                },
                {
                    "id": "0b2f8459-1007-42f3-9faf-022790d7e3a4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "interupt",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "tts_interrupt",
                    "returnType": 2
                },
                {
                    "id": "3c3b74c7-1fac-4264-bb73-569517f3c991",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "IncreaseRate",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "tts_increase_rate",
                    "returnType": 2
                },
                {
                    "id": "94da3cb7-f96d-4539-8b04-d898704ce497",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "DecreaseRate",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "tts_decrease_rate",
                    "returnType": 2
                }
            ],
            "init": "tts_create",
            "kind": 1,
            "order": [
                "fa018a46-27a0-464e-8d82-11a30a07f8a5",
                "1f1f9ed2-5b9f-43cc-b36f-09385d0a4e18",
                "a945deb6-4add-4f6e-adb5-6f95badff8ce",
                "0b2f8459-1007-42f3-9faf-022790d7e3a4",
                "3c3b74c7-1fac-4264-bb73-569517f3c991",
                "94da3cb7-f96d-4539-8b04-d898704ce497"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": -1,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "0.0.1"
}