{
    "id": "994f06c9-eb3a-40b7-8dd6-ccb107034539",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "reviewCards",
    "eventList": [
        {
            "id": "b1b85f15-d5cb-4037-abe5-4ad71db2e903",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "994f06c9-eb3a-40b7-8dd6-ccb107034539"
        },
        {
            "id": "08721c64-cd08-4105-a6a2-b4d62d3bbe1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "994f06c9-eb3a-40b7-8dd6-ccb107034539"
        },
        {
            "id": "b331aeea-882d-4cef-b6b8-e80bcf14ef54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "994f06c9-eb3a-40b7-8dd6-ccb107034539"
        },
        {
            "id": "0e9520de-439d-4920-927a-176871dba339",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "994f06c9-eb3a-40b7-8dd6-ccb107034539"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ddbc7f0d-6ae7-4dbb-8e49-b6e268c69abc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}