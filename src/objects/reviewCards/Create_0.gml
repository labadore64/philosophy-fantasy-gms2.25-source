/// @description Insert description here
// You can write your code in this editor
tooltip_total = 0
// Inherit the parent event
/// @description Insert description here
// You can write your code in this editor
objGenericCreate();
mouseHandler_clear()
menu_width = 100;
menu_height = 100;

ax_title = "Review"

alpha_adder = 0;
alpha_max = .5
alpha_rate = .05

max_knowledge_level = global.max_knowledge

all_cards = ds_list_create()

for(var i = 0; i < obj_data.cards_total; i++){
	ds_list_add(all_cards,i);	
}

// if the menu occupies the whole screen. If it does, its rendered differently.
menu_fullscreen = false;

surf = -1;
menu_draw_surface = menuParent_drawSurface;

menu_draw = menuParent_draw; // this will draw the menu.
menu_step = menuParent_step; // what the menu does every step

menu_cancel = menuParent_cancel;
menu_select = menuParent_select;

menu_up = menuParent_moveUp;
menu_down = menuParent_moveDown;

menu_left = menu_up
menu_right = menu_down

menu_help = "";

menu_name[0] = "";
menu_desc[0] = "";
menu_script[0] = -1;
menu_sprite[0] = -1;

menu_count = 0;

menupos = 0; // pointer position

wrap = true

// only plays sound if tts is enabled
menu_sound_ax = true

textbox = noone;

// input definitions
key_name[0] = "";
key_desc[0] = "";
key_chara[0] = "";

def_count = 0

def_active = true;

default_key_def()

menu_use_generic = false

// values used for mouse
info_x_offset = 0;
info_y_offset = 0

option_spacer = 40;
option_x_offset = 0;
option_y_offset = 0;
per_line = 4;
option_top = 0;
mousepos_x = 0;
mousepos_y = 0
draw_width = 100
draw_height = 20
select_percent = 1

enable_scrollwheel = true

menu_update_values = menuParent_updateSurface

menu_mouse_activate = menuParent_mouse_create

scrollwheel_skip = 1


// start review card here

archetype_string = "";
// start trunk list
var counter = 0;
for(var i = 0; i < obj_data.cards_total; i++){
	if(obj_data.card_in_trunk[i] > 0){
		trunk_cards[counter] = i
		counter++
	}
}


// draw stuff

// whether in trunk or deck
menu_selected = DECK_BUILD_TRUNK


info_x_offset = 20;
info_y_offset = 495

option_spacer = 40;
option_x_offset = 20;
option_y_offset = 80+option_spacer;
per_line =10
option_top = 0;

mousepos_x = 210
draw_width=450

soundPlaySong(sfx_music_review_mode,0)

menu_draw = reviewCard_draw

menu_up = deckBuild_moveUp;
menu_down = deckBuild_moveDown;
menu_tableft = deckBuild_moveLeft;
menu_tabright = deckBuild_moveLeft;//deckBuild_moveRight;
menu_cancel = deckBuild_cancel
menu_left = deckBuild_tabLeft
menu_right = deckBuild_tabRight

menu_select = reviewCard_select

menu_update_values = reviewCard_updateSurface

scrolling = false;

wrap = false;

// card stuff

selected_card = card_create(635,300,.75,0);

deck_menupos = -1
trunk_menupos = -1

// data structures

for(var i = 0; i < per_line; i++){
	draw_id[i] = -1;	
}

selected_draw_index = 0;


// start trunk list
var counter = 0;
for(var i = 0; i < obj_data.cards_total; i++){
	if(obj_data.card_in_trunk[i] > 0){
		trunk_cards[counter] = i
		counter++
	} else {
		if(ds_list_find_index(obj_data.current_deck,i) != -1){
			trunk_cards[counter] = i
			counter++
		}
	}
	review_counter[i] = 0
	deck_display[i] = i;
}

trunk_total = array_length_1d(trunk_cards)

deckSort_abc();

reviewCard_setDisplayArrays();

deckBuild_initParticle();

// shader stuff


shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

instance_create(120,0,fadeIn)

cancelled = false;
running_object = noone;

menu_surf = -1;
menu_surf_update = false;

tts_say("Owned Cards")

//menu_help="deckbuild"


var counter = 0;
var card_total = 0;
// count total card types
for(var i = 0; i < obj_data.cards_total; i++){
	if(obj_data.card_in_trunk[i] > 0 || 
		ds_list_find_index(obj_data.current_deck,i) != -1){
		counter++;		
	}
	card_total+=obj_data.card_in_trunk[i];
}
card_total+= ds_list_size(obj_data.current_deck);

// collected cards
total_cards = string(counter) + "/" + string(obj_data.cards_total);

sort_id[0] = "ABC"
sort_id[1] = "ZYX"
sort_id[2] = "ARC1"
sort_id[3] = "ARC2"

sort_script[0] = deckSort_abc
sort_script[1] = deckSort_zyx
sort_script[2] = deckSort_arch1
sort_script[3] = deckSort_arch2

sort_size = array_length_1d(sort_id)

sort_mode_pos = 0;
sort_mode = sort_id[sort_mode_pos]

clear_key_def()
add_key_def("Up","Move up","up")
add_key_def("Down","Move down","down")
add_key_def("Page Up","","left")
add_key_def("Page Down","","right")
add_key_def("Deck/Trunk","","lefttab")
add_key_def("Deck/Trunk","","righttab")
add_key_def("Select","","select")
add_key_def("Cancel","Go back","cancel")
add_key_def("Sort","","sort")

menu_control = deckBuild_sort

reviewCard_setDisplayArrays();

fave_cando = eventCheck(("fave"));

menu_push(id);


fade_surface = -1
fade_surface_updated = false;

draw_the_menu = true;

textbox = noone;
textboxz = false;
play_select_sound = true