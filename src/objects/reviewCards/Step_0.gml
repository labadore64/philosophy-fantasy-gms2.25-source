/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
if(!scrolling){
	//event_inherited();
	//objGenericStepMain()
	if(!keypress_this_frame){
		if(global.inputEnabled){
			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_OPTIONS]) ||
				global.gamepad_state_array[KEYBOARD_KEY_OPTIONS] == KEY_STATE_PRESS){	
					// if the options key is pressed 
					if(!instance_exists(fadeIn)){
						if(!instance_exists(fadeOut)){
							instance_create(0,0,OptionsMenu)
						}
					}
				}
		}
	}
}

if(!keypress_this_frame){
	if(global.inputEnabled){
			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_KEYDISPLAY]) ||
				global.gamepad_state_array[KEYBOARD_KEY_KEYDISPLAY] == KEY_STATE_PRESS) {
					keypress_this_frame = true;
					if(object_index == menuKeysDisplay){
						menuParent_cancel();	
					} else {
						menuKeysDisplay_show();
					}
			}
	}
}
		
if(objGenericCanDo()){
	if(!keypress_this_frame){
	if(global.drawing_enabled){
		if(mouse_check_button_pressed(mb_right)){
			if(!MouseHandler.clicked){
				if(menu_cancel > -1){
					script_execute(menu_cancel)	
					keypress_this_frame = true;
					with(MouseHandler){
						clicked = true
						clicked_countdown = 5	
					}
				}
			}
		} 
				
		if(enable_scrollwheel && !keypress_this_frame){
			if(mouse_wheel_down()){
				if(menu_up > -1){
					option_top+=scrollwheel_skip;
					if(option_top > menu_count - per_line){
						option_top = menu_count - per_line
					}
					if (option_top < 0){
						option_top = 0	
					}
					keypress_this_frame = true;
				}
			} else if(mouse_wheel_up()){
				if(menu_down > -1){
					option_top-=scrollwheel_skip;
					if (option_top < 0){
						option_top = 0	
					}
					keypress_this_frame = true;
				}
			}
		}
	}
	}
}
		
if(!keypress_this_frame){
	mouseHandler_process()
}

if(!keypress_this_frame){
	var mousex = window_mouse_get_x()
	var mousey = window_mouse_get_y()
	if(mouse_check_button_pressed(mb_left)){
		if(mousex >= MouseHandler.mouse_points_trans[3,0] && 
			mousex <= MouseHandler.mouse_points_trans[3,2] &&
			mousey >= MouseHandler.mouse_points_trans[3,1] && 
			mousey <= MouseHandler.mouse_points_trans[3,3]){
				scrolling = true;
			}
	}
	if(mouse_check_button(mb_left)){
		if(scrolling){
			deckBuild_scroll(mousex,mousey);	
		}
	}
	if(mouse_check_button_released(mb_left)){
		scrolling = false;	
	}
}

repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff
shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;

if(cancelled){
	if(!instance_exists(running_object)){
		save_game()
		room_goto(MainRoom)	
	}
}

if(textboxz && textbox < 0){
	textbox = noone
	textboxz = false
	draw_the_menu = true;
	active = true
	fade_surface_updated = true;
}