/// @description Insert description here
// You can write your code in this editor
if(surface_exists(surf)){
	surface_free(surf);	
}

if(ds_exists(status_effects,ds_type_map)){
	ds_map_destroy(status_effects);	
}

if(ds_exists(counters,ds_type_map)){
	ds_map_destroy(counters);	
}