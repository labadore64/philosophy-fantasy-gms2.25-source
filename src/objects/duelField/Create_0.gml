/// @description Insert description here
// You can write your code in this editor
#macro DUEL_FIELD_DECK 0
#macro DUEL_FIELD_HAND 1
#macro DUEL_FIELD_ACTIVE 2
#macro DUEL_FIELD_THEORY 3
#macro DUEL_FIELD_BENCH 4
#macro DUEL_FIELD_LIMBO 5
#macro DUEL_FIELD_DISCARD 6

#macro DUEL_FIELD_LENGTH 0
#macro DUEL_FIELD_WIDTH 500

surf = -1;
surface_update = false;

hand = instance_create(0,0,duelHand)
deck = instance_create(0,0,duelDeck)
bench = instance_create(0,0,duelBench)
active_monster = instance_create(0,0,duelActive)
theory = instance_create(0,0,duelTheory);
wait = instance_create(0,0,duelWaiting)
discard_field =  instance_create(0,0,duelDiscard);



part[0] = deck;
part[1] = hand;
part[2] = active_monster
part[3] = theory
part[4] = bench
part[5] = wait
part[6] = discard_field

part_size = 7;

for(var i = 0; i < part_size; i++){
	part[i].parent = id;	
	part[i].part_num = i;
}
// ai parts
ai_field = instance_create(0,0,ai_duelField);
ai_field.parent = id;

selected_object = hand;

enemy =false;

// actions
selected_object = hand

menu_left = duelField_move_left
menu_right = duelField_move_right
menu_up = duelField_move_up
menu_down = duelField_move_down
menu_select = duelField_move_select
menu_cancel = -1

switch_counter = 0;
overlay_counter = 0;
bench_counter = 0;

changed = false;

status_effects = ds_map_create();

counters = ds_map_create();

// active philosopher's att/def boosts
att_boost = 0
def_boost = 0

sin_val = 0
cos_val = 1