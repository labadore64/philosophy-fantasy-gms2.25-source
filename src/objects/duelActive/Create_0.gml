/// @description Insert description here
// You can write your code in this editor
event_inherited();

// The amount of cards on the overlay can never go higher than this.
size_limit = 3

// the current total number of cards that can be added
card_limit = 2;

// card 0 = main
// card 1 = overlay 1
// card 2 = overlay 2 (only certain conditions)
menu_activate = duel_activemon_activate
source_name = "Active"

menu_up = duel_activemon_up
menu_down = duel_activemon_down
menu_select = duel_activemon_select

menu_display_info = duel_activemon_display_info

card = noone;

for(var i = 0; i < 3; i++){
	card_array[i] = -1;	
}
