/// @description Insert description here
// You can write your code in this editor
//updateTimingAlarm();
// Inherit the parent event
event_inherited();

// You can get help if the active menu supports it!

if(global.inputEnabled){
	if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_HELP]) ||
		global.gamepad_state_array[KEYBOARD_KEY_HELP] == KEY_STATE_PRESS){
			with(menuBook){
				menu_pop();
				instance_destroy();	
			}
			
			menu_pop();
			instance_destroy();
			
		if(global.menu_sounds){soundfxPlay(sfx_sound_menu_cancel);}
	}
}
