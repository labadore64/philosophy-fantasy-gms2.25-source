/// @description Insert description here
// You can write your code in this editor
menuParentCreate();
// Inherit the parent event

event_inherited();

menu_draw = menuHelpDraw
window_set_cursor(cr_default);
title = "Help Menu"
drawSubtitle = false; 

draw_width =500; //width of box in pixels
draw_height = 400; //height of box in pixels

menu_width = draw_width
menu_height = draw_height

x = 400 - draw_width*.5
y = 50;

title_x_offset = 85 //xoffset for title
title_y_offset = 20; //yoffset for title

drawInfoText = false;

help_file = "testhelp";

title_text[0] = "";
text_text[0] = "";

skip = 7;
per_line = skip
menuParentUpdateBoxDimension();

//newalarm[2] = 1;

draw_obj = menuControl.active_menu;


wrap = false;

menu_up = menuHelpMoveLeft
menu_down = menuHelpMoveRight
menu_left = menuHelpMoveLeft
menu_right = menuHelpMoveRight

option_top = 0
selected_draw_index = 0
option_spacer = option_space

title_x_offset = 10; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 20 //xoffset for subtitle
subtitle_y_offset = 60; //yoffset for subtitle
option_x_offset = 10 //xoffset for options
option_y_offset = 100; //yoffset for options

option_space = 40; //how many pixels between each option
select_alpha = .25; //alpha of current selection
select_color = c_white; //selection color
select_percent = 1; //how much of the menu does the selection graphic occupy

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = c_white; //color of the title of the info box
info_subtitle_color = c_white; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

ax_title = "Help"

cancel_button_enabled = true; // does this menu use the cancel button
cancel_draw_border = true;
right_click_move = true

mousepos_y = 200
mousepos_x = 240

menu_push(id)