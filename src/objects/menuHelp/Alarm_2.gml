/// @description Insert description here
// You can write your code in this editor
// gets the count of the items
var ext = ".ini";
var testname = "";

if(access_mode()){
	var filename = working_directory + "resources\\help\\nodraw\\"

	if(!file_exists(filename+help_file+ext)){
		show_debug_message("No special case found. Getting default!")
		filename = working_directory + "resources\\help\\default\\";
	}
} else {
	var filename = working_directory + "resources\\help\\default\\";
}

testname = filename+help_file+ext;

if(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	var i = 0;

	while(true){
		if(!ini_key_exists("help", "help_title"+ string(i))){
			break;
	    }
		i++
	}

	for(var j = 0; i > j; j++){
	
	    title_text[j] = newline(ini_read_string("help", "help_title"+ string(j),""));
		text_text[j] = newline(ini_read_string("help", "help_text"+ string(j),""));
	}
}

var sizer = array_length_1d(title_text);

for(var i = 0; i < sizer; i++){
	menu_add_option(title_text[i],"",menuHelpScript,-1);
}

menu_add_option(string_replace_all("Go Back",".",""),"",menuParent_cancel,-1)

menuParentUpdateBoxDimension();
menuParent_menu_makeMouse();