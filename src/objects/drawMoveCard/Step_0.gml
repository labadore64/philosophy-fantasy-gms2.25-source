/// @description Insert description here
// You can write your code in this editor
if(global.menu_animate){
	x-=move_x*ScaleManager.timer_diff*FAST
	y-=move_y*ScaleManager.timer_diff*FAST
	// do sound position
	var posy = min(max(y,DUEL_SOUND_POSITION_TOP),DUEL_SOUND_POSITION_BOTTOM)-DUEL_SOUND_POSITION_TOP
	var diff = DUEL_SOUND_POSITION_BOTTOM-DUEL_SOUND_POSITION_TOP;
	
	var amount = POSITION_PITCH_MIN + (posy / diff)*(POSITION_PITCH_MAX-POSITION_PITCH_MIN)
	audio_sound_pitch(sound_id,amount )
}
timer-=ScaleManager.timer_diff*FAST

duelController.card_surface_update = true;

if(timer < -1){
	
	if(instance_exists(card_place_location)){
		var card_id = card_place_id;
		var card_facedown = card_place_facedown
		var source = card_place_source;
		var card_no = card_place_number;
		with(card_place_location){
			cardSource_add_card(card_id,card_facedown);
			with(duelController){
				if(selected_object > 0){
					last_selected_object = selected_object
					last_selected_thingie = selected_object.selected_object	
				}
			}
			
			parent.selected_object = id;
			
			if(enemy){
				duelController.selected_object = duelController.field[1];	
			} else {
				duelController.selected_object = duelController.field[0];	
			}
			
			with(source){
				cardSource_remove_card_index(card_no)
			}

			duelController.mouse_cleared=true
			duelController.field[0].surface_update = true;
			duelController.field[1].surface_update = true;
			menupos = card_array_size-1;
			if(menupos < 0){
				menupos = 0;	
			}
			if(menu_activate != -1){
				script_execute(menu_activate);
			}	
		}
	}
	
	instance_destroy();	
}