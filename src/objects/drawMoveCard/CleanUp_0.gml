/// @description Insert description here
// You can write your code in this editor
if(instance_exists(duelController)){
	with(duelController){
		test_victory = true;
		event_user(0)
	}

}

var sound = sfx_sound_duel_play_card;
var pitch = 1;
var playsound = true;
var queue = 0;
var counte = 0
if(instance_exists(card_place_location)){
	var counte = card_place_location.card_array_size-1;
}

if(!ignore_sound){
	if(instance_exists(card_place_location)){
		if(card_place_location.object_index == duelActive){
			// always play sound, no pitching
			queue = 0;
			if(counte > 0){
				queue = 2;
				sound = sfx_sound_duel_overlay_card;
			}
		} 
		else if(card_place_location.object_index == duelHand){
			// only play in sound ax mode, pitch
			sound = sfx_sound_duel_hand_card;
			pitch = 1 + (counte/10);
			if(!global.extra_sounds){
				playsound = false;
			}
		} else if(card_place_location.object_index == duelWaiting){
			// only play in sound ax mode, no pitching
			sound = sfx_sound_duel_limbo_card;
			if(!global.extra_sounds){
				playsound = false;
			}
		} else if(card_place_location.object_index == duelTheory){
			// only play in sound ax mode, no pitching
			sound = sfx_sound_duel_theory_card;
			if(!global.extra_sounds){
				playsound = false;
			}
		} else if(card_place_location.object_index == duelDeck){
			// only play in sound ax mode, no pitching
			sound = sfx_sound_duel_deck_card;
			if(!global.extra_sounds){
				playsound = false;
			}
		} else if(card_place_location.object_index == duelBench){
			// always play sound, slight pitch change
			sound = sfx_sound_duel_bench_card;
			queue = 1
			if(global.extra_sounds){
				// set pitch
				pitch = 1 + (counte/2)*.2;
			}
		}

		if(pitch > 3){
			pitch = 3;	
		} else if (pitch < .1){
			pitch = .1	
		}

		if(playsound){
			duel_animation_play_sound(sound,pitch,queue)
		}
	
	}
}

if(camera_adjust){
	//duel_animation_wait(30)
	//duelCamera_center_camera_special();
}