/// @description Insert description here
// You can write your code in this editor
if(instance_exists(obj_data)){
	if(ds_exists(load_queue,ds_type_queue)){
		ds_queue_destroy(load_queue);	
	}
	with(menuControl){
		sprite_index = -1
	}
	titleScreen_deleteParticle();

	room_goto(ReviewCardRoom);
	draw_texture_flush()
}