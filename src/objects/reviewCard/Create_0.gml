event_inherited()
tooltip_total = 0
soundPlaySong(-1,0)

Sname = -1;
particle1= -1
duelist_img_sprite = -1
// this queue contains the things to run.
load_queue = ds_queue_create();

menu_draw = duelStart_draw

// only progress if this item doesn't exist
running_object = noone;

menu_push(id);

menu_cancel = -1;
menu_select = -1;

//shader
shader_id = -1;
shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

textbox = noone

menuControl.sprite_index = -1
menuControl.x = 120;
menuControl.y = 0;
menuControl.image_xscale = 1;
menu_control.image_yscale = 1

demo_get_duelists()

// custom cutscene sprites go here
duelist_sprite = spr_portrait_roger_cutscene	
duelist_say1 = "review_intro"

if(eventCheck(("review_tutorial"))){
	room_goto(ReviewCardRoom);
} else {

	if(duelist_particle != -1){
		script_execute(duelist_particle)	
	}

	challenge_music = -1

	// test
	ds_queue_enqueue(load_queue,
					gameStart_script0a,
					demoDuelist_script0,
					demoDuelist_script1,
					gameStart_script4);
}