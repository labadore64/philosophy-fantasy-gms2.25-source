{
    "id": "f3c7f6bd-1483-43fb-bf27-5677a346c9a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationEndTurnMessage",
    "eventList": [
        {
            "id": "61c274ba-623a-4d5a-8bdd-d92c2975df81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3c7f6bd-1483-43fb-bf27-5677a346c9a6"
        },
        {
            "id": "9205b035-688e-41cc-bd37-34a6458c5c1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f3c7f6bd-1483-43fb-bf27-5677a346c9a6"
        },
        {
            "id": "140dc554-b8a4-4c51-bdf8-08d86952b833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3c7f6bd-1483-43fb-bf27-5677a346c9a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}