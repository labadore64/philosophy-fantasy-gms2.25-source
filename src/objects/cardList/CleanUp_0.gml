/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
var selee = select_card && selection_made

// if card is selected add the selected card to the list
if(selee){
	// find the first index of the card you're looking for
	var mycard = trunk_display[ menupos];
	var possi = 0;
	for(var i = 0; i < card_list_source.card_array_size; i++){
		if(card_list_source.card_array[i] == mycard){
			possi = i;
			break;
		}
	}
	
	duelinput_insert("cardselect",card_list_source,extra_card_list[| menupos]);
	duelinput_execute(duelinput_length()-1);
	duel_animation_show_selected(mycard,1)
}

deckBuild_deleteParticle();
with(selected_card){
	instance_destroy();	
}

if(surface_exists(menu_surf)){
	surface_free(menu_surf);	
}


if(card_list_destroy){
	if(ds_exists(list_reference,ds_type_list)){
		ds_list_destroy(list_reference)	
	}
}

if(ds_exists(extra_card_list,ds_type_list)){
	ds_list_destroy(extra_card_list);	
}

with(duelController){
	fade = true;
	if(selee){
		instance_create(120,0,fadeIn);
	} else{
		running_obj = instance_create(120,0,fadeIn);	
	}
}

menu_pop();