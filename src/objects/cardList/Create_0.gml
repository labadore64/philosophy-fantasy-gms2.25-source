/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
window_set_cursor(cr_default);
// draw stuff

// whether in trunk or deck
menu_selected = DECK_BUILD_TRUNK
loaded_list = false;

info_x_offset = 20;
info_y_offset = 495

option_spacer = 90;
option_x_offset = 20;
option_y_offset = 20+option_spacer;
per_line = 4;
option_top = 0;

ax_title = "Limbo"

title = "";

menu_draw = duelList_draw;

menu_up = duelList_moveUp;
menu_down = duelList_moveDown;
menu_cancel = deckBuild_cancel

menu_select = duelList_select

menu_mouse_activate = duelList_update//duelList_setDisplayArrays
menu_update_values = duelList_update

wrap = false;

// card stuff

selected_card = card_create(635,275,.6,0);

deck_menupos = -1
trunk_menupos = -1

// data structures

for(var i = 0; i < per_line; i++){
	draw_id[i] = -1;	
}

selected_draw_index = 0;
list_reference = -1;

deckBuild_initParticle();

// shader stuff

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shader_bg_color=make_color_hsv(shader_counter%255,127,255)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

instance_create(120,0,fadeIn)

cancelled = false;
running_object = noone;

menu_surf = -1;
menu_surf_update = false;
extra_card_list = -1;
mousepos_x = 210
draw_width=450

select_card = false;
selected_side = DUEL_PLAYER_YOU
location = "limbo"
card_list_source = noone
selection_made = false;

menu_push(id);