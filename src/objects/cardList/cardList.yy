{
    "id": "4919c54e-8628-41d1-974e-c906c7aa2a1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "cardList",
    "eventList": [
        {
            "id": "c097d564-9e81-4c32-be92-96676aea54f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4919c54e-8628-41d1-974e-c906c7aa2a1a"
        },
        {
            "id": "326c73d9-e7ab-4257-ba24-68f7b4fb01b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4919c54e-8628-41d1-974e-c906c7aa2a1a"
        },
        {
            "id": "ad1d09ac-948d-4fff-821c-3c7a84f7bc3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4919c54e-8628-41d1-974e-c906c7aa2a1a"
        },
        {
            "id": "cbdb7591-5875-4d13-afdd-d9cd071935ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4919c54e-8628-41d1-974e-c906c7aa2a1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}