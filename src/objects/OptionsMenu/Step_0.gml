/// @description Insert description here
// You can write your code in this editor

if(!scrolling){
	event_inherited();
}
if(objGenericCanDo()  && menuControl.active_menu == id){
	if(ScaleManager.updated){
		event_user(0)	
	}

	if(!keypress_this_frame){
		var mousex = window_mouse_get_x()
		var mousey = window_mouse_get_y()
		if(mouse_check_button_pressed(mb_left)){
			if(mousex >= MouseHandler.mouse_points_trans[scrollcounter,0] && 
				mousex <= MouseHandler.mouse_points_trans[scrollcounter,2] &&
				mousey >= MouseHandler.mouse_points_trans[scrollcounter,1] && 
				mousey <= MouseHandler.mouse_points_trans[scrollcounter,3]){
					scrolling = true;
				}
		}
		if(mouse_check_button(mb_left)){
			if(scrolling){
				options_scroll(mousex,mousey);	
			}
		}
		if(mouse_check_button_released(mb_left)){
			scrolling = false;	
		}
	}
}

if(menu_countdown <= -1){
	
	if(object_index == OptionsMenu){
		if(global.gamepad_state_array[KEYBOARD_KEY_OPTIONS] == KEY_STATE_PRESS ||
			keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_OPTIONS])){		
			with(menuControl){
				// pop every option menu
				if(ds_exists(menu_stack,ds_type_stack)){
					while(ds_stack_size(menu_stack) > 1 && instance_exists(active_menu) &&
						(object_is_ancestor(active_menu.object_index,OptionsMenu) ||
							active_menu.object_index == OptionsMenu)
						){
						with(active_menu){
							instance_destroy();	
						}
						menu_pop();
					}
					
					if(global.menu_sounds){soundfxPlay(sfx_sound_menu_cancel);}
				}
			}
			instance_destroy();
		}
	}
}

if(menu_countdown > -1){
	menu_countdown-= ScaleManager.timer_diff
}