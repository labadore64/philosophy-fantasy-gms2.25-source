/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
options_createDefault();

// add options

ds_map_add(options_map,"SFX",global.sfxVolume*10)
ds_map_add(options_map,"Music",global.musicVolume*10)

ds_map_add(options_map,"Stick Axis",global.gamepadRightAxis)
ds_map_add(options_map,"Vibration",global.gamepadVibration)
ds_map_add(options_map,"Vibration Rate",global.gamepadVibrationAmount*10)


ds_map_add(options_map,"Mouse",global.mouse_active)
ds_map_add(options_map,"Skip Games",global.skip_duel)
ds_map_add(options_map,"Skip Intro",global.skip_splash)
ds_map_add(options_map,"Replay",global.replay)

// add menu options
option_number[0] = 0;
menu_add_option("Graphics","Modify graphics settings.",-2,-1)
//menu_add_option("Fullscreen", "Enable fullscreen.",-1,0)
//menu_add_option("Enable Shaders","Enable shader effects.",-1,0)
menu_add_option("Skip Intro","Skip intro graphic.",-1,0)
//menu_add_option("Enable Graphics","Enable graphics. Turns on TTS if disabled.",-1,0)
menu_add_option("Appearance","Change game appearance.",options_openAppearance,2)
menu_add_option("Display Options","View Display Options.",options_openDisplayOptions,2)

menu_add_option("","",-2,-1)
option_number[1] = menu_count;
menu_add_option("Sound","Modify Sound settings.",-2,-1)
menu_add_option("SFX","Modify sound effect volume.",-1,1)
menu_add_option("Music","Modify music volume.",-1,1)
menu_add_option("Sound Effects","Customize certain sound effect options.",options_openSoundEffects,2)
menu_add_option("Text-to-Speech","View Text-to-Speech Options.",options_openTTS,2)
menu_add_option("Sound Test","Listen to SFX and Music.",options_soundtest,2)

menu_add_option("","",-2,-1)
option_number[2] = menu_count;
menu_add_option("Controls","Modify Controls",-2,-1)

menu_add_option("Keyboard Binding","Modify keyboard binding.",options_open_keyboard_bind,2)
menu_add_option("Controller Binding","Modify controller binding.",options_open_controller_bind,2)
menu_add_option("Mouse","Enable/Disable mouse controls.",-1,0)
menu_add_option("Stick Axis","Which axis to use for the controller stick.",-1,3)
menu_add_option("Vibration","Enable controller vibration.",-1,0);
menu_add_option("Vibration Rate","Adjust vibration rate.",-1,1);

menu_add_option("","",-2,-1)
option_number[3] = menu_count;
menu_add_option("Game","In-Game Options",-2,-1)
menu_add_option("Battle Options","View Battle Options.",options_openDuel,2)
menu_add_option("Replay","Whether or not to say replays after battle.",-1,6)

menu_add_option("Skip Games","Disable games to continue story mode.",-1,0)
menu_add_option("Tutorial Reset","Click to reset tutorials.",options_reset_tutorial,-1)

options_createFinali();

destroy_options = false;

if(room != MainRoom){
	if(global.menu_sounds){soundfxPlay(sfx_sound_menu_options);}	
	destroy_options = true
}