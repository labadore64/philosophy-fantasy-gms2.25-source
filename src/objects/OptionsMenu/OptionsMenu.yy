{
    "id": "240beec4-ed1d-4b57-bef5-155fe131f7b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OptionsMenu",
    "eventList": [
        {
            "id": "1a0630c9-a3ea-4661-8ba8-78fa6690d907",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "240beec4-ed1d-4b57-bef5-155fe131f7b9"
        },
        {
            "id": "b939f215-9ab5-4d1c-84fb-931f051858b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "240beec4-ed1d-4b57-bef5-155fe131f7b9"
        },
        {
            "id": "890abc0b-dea1-4a09-92c3-eaf52b20751a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "240beec4-ed1d-4b57-bef5-155fe131f7b9"
        },
        {
            "id": "fc4a1a74-2d0e-477f-9593-c89ce8745b10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "240beec4-ed1d-4b57-bef5-155fe131f7b9"
        },
        {
            "id": "3741fb86-9c97-4c9f-834f-9baff2fa91ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "240beec4-ed1d-4b57-bef5-155fe131f7b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}