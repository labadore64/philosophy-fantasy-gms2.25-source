/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

options_complete();

if(ds_exists(options_map,ds_type_map)){
	ds_map_destroy(options_map)
}

with(mainMenu){
	if(global.high_contrast){
		card_alpha = 1
	} else {
		card_alpha = .6;	
	}	
	card_display.image_alpha = card_alpha
	surface_update = true
}
var pos = menupos

with(duelController){
	options_menupos = menupos
	card_surface_update = true;
	for(var i = 0; i < 2; i++){
		if(instance_exists(field[i])){
			field[i].surface_update = true
		}
	}	
}

with(duelField){
	surface_update = true	
}

with(duelHealth){
	surf_updated = true	
}
if(object_index == OptionsMenu){
	
	ini_open("config.ini")
	
	ini_write_string("ax","tts",boolToString(global.voice))
	ini_write_string("ax","tts_details",boolToString(global.speak_details))
	
	ini_write_string("graphics","enabled",boolToString(global.drawing_enabled))
	ini_write_string("graphics","shader_enabled",boolToString(global.enableShader))
	ini_write_string("graphics","fullscreen",boolToString(global.fullscreen))
	ini_write_string("graphics","high_contrast",boolToString(global.high_contrast))
	ini_write_string("graphics","menu_animate",boolToString(global.menu_animate))
	ini_write_real("graphics","fps",game_get_speed(gamespeed_fps))
	ini_write_string("graphics","card_icon",boolToString(global.card_icon))
	ini_write_string("graphics","skip_splash",boolToString(global.skip_splash))
	if(global.menu_index > -1){
		ini_write_string("graphics","menu_bg",string_replace_all(sprite_get_name(global.menu_index),"background_img_",""))
	} else {
		ini_write_string("graphics","menu_bg","none")
	}
	
	
	
	
	ini_write_string("control","stick",boolToString(global.gamepadRightAxis))
	ini_write_string("control","vibration",boolToString(global.gamepadVibration))
	ini_write_real("control","vibration_amount",global.gamepadVibrationAmount)

	ini_write_real("sound","sfx_vol",global.sfxVolume);
	ini_write_real("sound","music_vol",global.musicVolume)
	ini_write_string("sound","menu",boolToString(global.menu_sounds))
	ini_write_string("sound","positional_pitch",boolToString(global.positional_pitch))
	ini_write_string("sound","text_sound",boolToString(global.text_sound))
	ini_write_string("sound","extrasound",boolToString(global.extra_sounds));
	
	ini_write_string("control","mouse",boolToString(global.mouse_active))
	ini_write_real("battle","tooltip",global.tooltip_type);
	ini_write_real("battle","mouse_pan_speed",global.pan_speed)
	ini_write_real("battle","mouse_zoom_speed",global.zoom_speed)
	ini_write_string("battle","mouse_display_right",boolToString(global.mouse_right))
	ini_write_string("battle","fastbattle",boolToString(global.fastmode))
	ini_write_real("battle","difficulty",global.difficulty)
	ini_write_string("battle","skip",boolToString(global.skip_duel))
	ini_write_string("battle","draw_health",boolToString(global.display_hp))
	ini_write_string("battle","coin_toss",boolToString(global.coin_flip))
	ini_write_string("battle","press_continue",boolToString(global.press_continue))
	ini_write_string("battle","invert_cursor",boolToString(global.reverse_pointer))
	ini_write_string("battle","pause_anim",boolToString(global.pause_anim))
	
	ini_write_string("text","auto",boolToString(global.text_auto))
	ini_write_string("text","full",boolToString(global.text_full))
	
	ini_write_string("character","name",global.character_name)

	ini_write_real("battle","replay",global.replay)

	ini_close()
	with(AXManager){
		gamepad_keybind_save("gamepad.json")
		keyboard_keybind_save("keyboard.json")
	}
}

with(menuControl){
	with(active_menu){
		if(menu_mouse_activate != -1){
			script_execute(menu_mouse_activate)	
			event_user(0)
		}
		event_user(2)	
	}
	
	with(mainMenu){
		event_user(2)	
	}
}