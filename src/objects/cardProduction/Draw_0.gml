/// @description Insert description here
// You can write your code in this editor
if(!done){
	for(var i = 0; i < obj_data.cards_total; i++){
		with(card[i]){
			if(!surface_exists(surf)){
				card_reset_size();
				surf = surface_create(card_width,card_height);	
				surface_update = true
			} else {
				if(ScaleManager.updated){
					card_reset_size();
					surface_resize(surf,card_width,card_height)	
					surface_update = true
				}
			}

			if(surface_update){
				card_update_surface();
	
				if(card_sprite != -1){
					sprite_delete(card_sprite);	
				}
				surface_update = false;
			
				surface_save(surf, "card_output\\"+string(i)+".png");
			}
		
		
		}
	}
}
done = true