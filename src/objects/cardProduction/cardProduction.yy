{
    "id": "c3c064cd-c5d4-4cc3-b5a6-0c27f7f268a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "cardProduction",
    "eventList": [
        {
            "id": "a83267e3-a68f-41f6-a424-edea13f682d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3c064cd-c5d4-4cc3-b5a6-0c27f7f268a5"
        },
        {
            "id": "5ade3ba3-4a91-4904-9381-7c51518a5bd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c3c064cd-c5d4-4cc3-b5a6-0c27f7f268a5"
        },
        {
            "id": "a0abff2e-ad7e-4ebe-8ee1-de2ab50cc88b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3c064cd-c5d4-4cc3-b5a6-0c27f7f268a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}