/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(ds_exists(keybind_text,ds_type_list)){
	ds_list_destroy(keybind_text);	
}

if(ds_exists(key_ids,ds_type_list)){
	ds_list_destroy(key_ids)	
}

if(ds_exists(axis_values,ds_type_list)){
	ds_list_destroy(axis_values);	
}
if(ds_exists(axis_strings,ds_type_list)){
	ds_list_destroy(axis_strings);	
}

if(ds_exists(vib_values,ds_type_list)){
	ds_list_destroy(vib_values);	
}
if(ds_exists(vib_strings,ds_type_list)){
	ds_list_destroy(vib_strings);	
}

with(menuControl){
	please_draw_this_frame= true
}