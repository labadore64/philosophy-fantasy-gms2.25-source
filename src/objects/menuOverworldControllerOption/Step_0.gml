/// @description Insert description here
// You can write your code in this editor
//updateTimingAlarm()
// Inherit the parent event
if(ScaleManager.updated){
	event_user(0)	
}

if(!scrolling){
	event_inherited();
}

if(objGenericCanDo() && menuControl.active_menu == id){
	if(!keypress_this_frame){
		var mousex = window_mouse_get_x()
		var mousey = window_mouse_get_y()
		if(mouse_check_button_pressed(mb_left)){
			if(mousex >= MouseHandler.mouse_points_trans[scrollcounter,0] && 
				mousex <= MouseHandler.mouse_points_trans[scrollcounter,2] &&
				mousey >= MouseHandler.mouse_points_trans[scrollcounter,1] && 
				mousey <= MouseHandler.mouse_points_trans[scrollcounter,3]){
					scrolling = true;
				}
		}
		if(mouse_check_button(mb_left)){
			if(scrolling){
				binding_scroll(mousex,mousey);	
			}
		}
		if(mouse_check_button_released(mb_left)){
			scrolling = false;	
		}
	}
}
