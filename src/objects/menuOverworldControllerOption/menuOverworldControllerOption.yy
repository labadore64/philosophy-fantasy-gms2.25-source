{
    "id": "901840d0-15fa-448e-8e53-c87bcb69222c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldControllerOption",
    "eventList": [
        {
            "id": "a1e1f942-27a0-4566-bf8e-df817286fb5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "901840d0-15fa-448e-8e53-c87bcb69222c"
        },
        {
            "id": "a7fb86e9-a109-47b7-b229-21f365414376",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "901840d0-15fa-448e-8e53-c87bcb69222c"
        },
        {
            "id": "fd9a3baa-ecb8-4899-a44d-86cd999f8dfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "901840d0-15fa-448e-8e53-c87bcb69222c"
        },
        {
            "id": "0db4e54a-e81d-4a02-a236-c30e75d69742",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "901840d0-15fa-448e-8e53-c87bcb69222c"
        },
        {
            "id": "7e321fd0-e643-4a84-9580-7b4bcfb0a731",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "901840d0-15fa-448e-8e53-c87bcb69222c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}