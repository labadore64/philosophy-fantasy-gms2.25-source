/// @description Insert description here
// You can write your code in this editor
objGenericStepMain();

if(alpha_adder < alpha_max){
	surface_update=true
	alpha_adder += ScaleManager.timer_diff*alpha_rate	
	if(alpha_adder > alpha_max){
		alpha_adder = alpha_max	
	}
}

if(objGenericCanDo()){
	if(global.drawing_enabled){
		if(!keypress_this_frame){
			if(mouse_check_button_pressed(mb_right)){
				if(!MouseHandler.clicked){
					if(menu_cancel > -1){
						script_execute(menu_cancel)	
						keypress_this_frame = true;
						with(MouseHandler){
							clicked = true
							clicked_countdown = 5	
						}
					}
				}
			} 
				
			if(enable_scrollwheel && !keypress_this_frame){
				if(mouse_wheel_down()){
					if(menu_up > -1){
						option_top+=scrollwheel_skip;
						if(option_top > menu_count - per_line){
							option_top = menu_count - per_line
						}
						if (option_top < 0){
							option_top = 0	
						}
						keypress_this_frame = true;
					}
				} else if(mouse_wheel_up()){
					if(menu_down > -1){
						option_top-=scrollwheel_skip;
						if (option_top < 0){
							option_top = 0	
						}
						keypress_this_frame = true;
					}
				}
			}
		}
	}
}
		
if(!keypress_this_frame){
	mouseHandler_process()
}

if(mouse_check_button_released(mb_left)){
	with(menuControl){
		with(exit_obj){
			if(cancel_pressed){
				script_execute(menu_cancel);	
				cancel_pressed = false
			}
		}
	}
}