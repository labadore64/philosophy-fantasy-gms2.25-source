/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(global.menu_sounds){soundfxPlay(sfx_sound_exit_game);}
menu_draw = endGame_draw
card_id = -1;
menu_selected = 0;

cancel_button_enabled = true; // does this menu use the cancel button
cancel_draw_border = true;
right_click_move = true

option_x_offset = 150
option_y_offset = 80
option_spacer = 50;

menu_height = 180-10
menu_width = 380

x= 280-50
y= 200

card = noone;
parent = noone;

title = "End game?"

menu_up = menuParent_moveUp
menu_down = menuParent_moveDown
menu_cancel = endGame_cancel;

menu_add_option("Yes","Exits the game.",endGame_do,-1)
menu_add_option("No","Resume gameplay.",endGame_cancel,-1)
draw_width = menu_width
mousepos_x = 40
mousepos_y = 10
tts_say(title + " " + menu_name[0])

alpha_max = .65

menuParent_menu_makeMouse();