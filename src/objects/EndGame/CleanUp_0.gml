/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
with(menuControl){
	exit_obj = noone	
}

mouseHandler_clear()

with(menuControl){
	with(active_menu){
		if(menu_mouse_activate != -1){
			script_execute(menu_mouse_activate)	
			event_user(0)
		}
	}
	
	with(mainMenu){
		event_user(2)	
	}
}