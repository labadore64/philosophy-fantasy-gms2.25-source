{
    "id": "b2c123b8-25c8-4cf4-8455-1f4410d1b2cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "EndGame",
    "eventList": [
        {
            "id": "2abac186-b021-4fcf-b063-794ef10acad8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2c123b8-25c8-4cf4-8455-1f4410d1b2cc"
        },
        {
            "id": "bd0f26d1-6a19-46ac-923a-c08e52e3aafe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b2c123b8-25c8-4cf4-8455-1f4410d1b2cc"
        },
        {
            "id": "ec8a7619-517d-4de3-89bb-69094c5c2854",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2c123b8-25c8-4cf4-8455-1f4410d1b2cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}