/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event



if(delay > -1){
	delay -=ScaleManager.timer_diff
} else {
	event_inherited();
	
	rare_color = merge_color(make_color_hsv(floor(color_counter) % 255, 255,255),c_white,.5);
	color_counter+= ScaleManager.timer_diff*2

	if(animate_active){
		for(var i = 0; i < 5; i++){
			if(!animate_card[i]){
				animate_time[i] = animate_time_max
				animate_card[i] = true;
				soundfxPlay(sfx_sound_play_card)
				RUMBLE_CARD_REVEAL_PACK
			}
	
			if(animate_card[i]){
				if(animate_time[i] > -1){
					animate_time[i] -=ScaleManager.timer_diff;
					if(global.menu_animate){
						animate_x[i] -= animate_time_change*ScaleManager.timer_diff;
			
						if(animate_x[i] < 0){
							animate_x[i] = 0;	
						}
					} else {
						animate_x[i] = 0;	
					}
					if(animate_time[i] <= -1){
						animate_time[i] = -1;	
					} else {
						break;	
					}
				}
			}
		
			// if reached last point, finish animate
			if(i == 4){
				animate_active = false;	
				for(var j = 0; j < 5; j++){
					animate_card[j] = true
					animate_time[j] = -1;
					animate_x[j] = 0
				}
			}
		}

	}

	if(cancelled){
		if(!instance_exists(running_object)){
			instance_destroy();	
		}
	}
}