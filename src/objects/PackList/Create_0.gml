/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
tts_stop();
running_object = noone;
cancelled = false;

var list = -1;
var pack_id = -1;

list = global.selected_pack
pack_id = global.selected_pack_id

if(list == -1){
	instance_destroy();	
} else {
	spr_unload_card()
	
	var sizer = ds_list_size(list);
	for(var i = 0; i < sizer; i++){
		var cardd = list[|i]
		menu_add_option(obj_data.card_name[cardd],"",-1,spr_duel_cards)	
		card_sprite_index[i] = selectPack_getCardIndex(cardd);
		card_id[i] = cardd;
		is_new[i] = false;
		is_rare[i] = false;
		if(obj_data.card_in_trunk[cardd] == 0 &&
			ds_list_find_index(obj_data.current_deck,cardd) == -1){
			is_new[i] = true;	
		}
		
		for(var j = 0; j < obj_data.pack_card_total[pack_id]; j++){
			if(obj_data.pack_card[pack_id,j] == card_id[i]){
				if(obj_data.pack_rate[pack_id,j] == 1){
					is_rare[i] = true;	
				}
			}
		}
		
		for(var j = 0; j < i; j++){
			if(j != i){
				if(card_id[i] == card_id[j]){
					is_new[i] = is_new[j];	
				}
			}
		}
		give_card_index(cardd);
		spr_load_card_front(cardd);	
	}
	
}

menu_draw = packList_draw;
menu_up = packCardMoveUp;
menu_down = packCardMoveDown;
menu_select = packList_select
menu_cancel = packList_cancel

menu_surf = -1;
menu_surf_update = false;
selected_draw_index=0
info_x_offset = 20;
info_y_offset = 495

option_spacer = 90;
option_x_offset = 20;
option_y_offset = 75
per_line = 5
option_top = 0;
wrap = false;

delay = 7;

color_counter = 0;
rare_color = c_red

animate_active = true;
animate_time_max = 15;
animate_dist = 500;
animate_time_change = animate_dist/animate_time_max;
for(var i = 0; i < 5; i++){
	animate_card[i] = false;
	animate_time[i] = -1;
	animate_x[i] = animate_dist;
}


if(global.event_set != ""){
	eventSet((global.event_set))
	global.event_set = ""
}

// save the game.
save_game();

draw_width = 800
mousepos_x = 400
mousepos_y = 10

packBoosterAX();

triggered = false;

menu_push(id);

menu_help = "cardview"

ax_title = "Cards pulled"