{
    "id": "495215b7-7f8f-49a1-8e80-a61d88e34918",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PackList",
    "eventList": [
        {
            "id": "598a8fdf-23e0-4199-bb0f-0a2aa768ba77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "495215b7-7f8f-49a1-8e80-a61d88e34918"
        },
        {
            "id": "11114f79-c3db-40ff-a6cc-c68206c1adc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "495215b7-7f8f-49a1-8e80-a61d88e34918"
        },
        {
            "id": "dfc7e02b-6d7a-4a55-9ccc-55701c495899",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "495215b7-7f8f-49a1-8e80-a61d88e34918"
        },
        {
            "id": "6c71cbd9-4d1e-465b-a2be-09f9cb0d8cd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "495215b7-7f8f-49a1-8e80-a61d88e34918"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}