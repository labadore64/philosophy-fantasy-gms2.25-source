/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

name_select_alpha_timer+= ScaleManager.timer_diff
name_select_alpha = .25+sin(name_select_alpha_timer*.085)*.25

if(!keypress_this_frame){
	if(global.inputEnabled){
		if(menu_submit != -1){
			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT2]) ||
			global.gamepad_state_array[KEYBOARD_KEY_SELECT2] == KEY_STATE_PRESS) {

				script_execute(menu_submit);
				
			}
		}
	}
} 

if(destroyed){
	if(destroy_timer > -1){
		 destroy_timer -= ScaleManager.timer_diff	
	} else {
		instance_destroy();
		room_goto(appearanceRoom)
	}
}