/// @description Insert description here
// You can write your code in this editor
event_inherited()

talked_frame = false;
insert_mode = true;

#macro NAMEINPUT_STATE_NAME 0
#macro NAMEINPUT_STATE_ALPHABET 1
#macro NAMEINPUT_STATE_CONTROLS 2

#macro NAMEINPUT_CASE_UPPER 0
#macro NAMEINPUT_CASE_LOWER 1
#macro NAMEINPUT_CASE_SPECIAL_UPPER 2
#macro NAMEINPUT_CASE_SPECIAL_LOWER 3

menu_draw = nameInput_draw
menu_cancel = nameInput_delete
menu_up = nameInput_moveUp
menu_down = nameInput_moveDown
menu_left = nameInput_moveLeft
menu_right = nameInput_moveRight
menu_select = nameInput_select

destroyed = false;

menu_submit = nameInput_submit

current_name = "";
current_name_length = 0;
alpha_menupos = 1

name_length = 7
for(var i = 0; i < name_length; i++){
	name[i] = ""
}
name_spacer = 30
name_select_alpha = 0;
name_select_alpha_timer = 0
uppercase = NAMEINPUT_CASE_UPPER
insert_pos = 0;

columns = 9;
rows = 5;
spacer_x = 60
spacer_y = 60
alpha_x = 160
alpha_y = 200

option_x = 400
option_y = 530

selected_part = NAMEINPUT_STATE_ALPHABET

chara[0] = "a"
chara[1] = "b"
chara[2] = "c"
chara[3] = "d"
chara[4] = "e"
chara[5] = "f"
chara[6] = "g"
chara[7] = "h"
chara[8] = "i"
chara[9] = "j"
chara[10] = "k"
chara[11] = "l"
chara[12] = "m"
chara[13] = "n"
chara[14] = "o"
chara[15] = "p"
chara[16] = "q"
chara[17] = "r"
chara[18] = "s"
chara[19] = "t"
chara[20] = "u"
chara[21] = "v"
chara[22] = "w"
chara[23] = "x"
chara[24] = "y"
chara[25] = "z"

chara[26] = " "
chara[27] = "?"
chara[28] = "!"
chara[29] = ":"
chara[30] = "'"
chara[31] = "¿"

special_upper[0] = "À";
special_upper[1] = "Á";
special_upper[2] = "Â";
special_upper[3] = "Ã";
special_upper[4] = "Ä";
special_upper[5] = "Å";
special_upper[6] = "Æ";
special_upper[7] = "Ç";
special_upper[8] = "È";
special_upper[9] = "É";
special_upper[10] = "Ê";
special_upper[11] = "Ë";
special_upper[12] = "Ì";
special_upper[13] = "Í";
special_upper[14] = "Î";
special_upper[15] = "Ï";
special_upper[16] = "Ð";
special_upper[17] = "Ñ";
special_upper[18] = "Ò";
special_upper[19] = "Ó";
special_upper[20] = "Ô";
special_upper[21] = "Õ";
special_upper[22] = "Ö";
special_upper[23] = "Ø";
special_upper[24] = "Ù";
special_upper[25] = "Ú";
special_upper[26] = "Û";
special_upper[27] = "Ü";
special_upper[28] = "Ý";
special_upper[29] = "Þ";
special_upper[30] = "ß";

special_upper_length = array_length_1d(special_upper);

special_lower[0] = "à";
special_lower[1] = "á";
special_lower[2] = "â";
special_lower[3] = "ã";
special_lower[4] = "ä";
special_lower[5] = "å";
special_lower[6] = "æ";
special_lower[7] = "ç";
special_lower[8] = "è";
special_lower[9] = "é";
special_lower[10] = "ê";
special_lower[11] = "ë";
special_lower[12] = "ì";
special_lower[13] = "í";
special_lower[14] = "î";
special_lower[15] = "ï";
special_lower[16] = "ð";
special_lower[17] = "ñ";
special_lower[18] = "ò";
special_lower[19] = "ó";
special_lower[20] = "ô";
special_lower[21] = "õ";
special_lower[22] = "ö";
special_lower[23] = "ø";
special_lower[24] = "ù";
special_lower[25] = "ú";
special_lower[26] = "û";
special_lower[27] = "ü";
special_lower[28] = "ý";
special_lower[29] = "þ";
special_lower[30] = "ÿ";

special_lower_length = array_length_1d(special_lower);

double_select = false

wrap = false
menupos = 1

chara_length = array_length_1d(chara)

lengy = chara_length

for(var i = 0; i < chara_length; i++){
	if(uppercase == NAMEINPUT_CASE_UPPER){
		display_chara[i] = string_upper(chara[i]);
	} else {
		display_chara[i] = string_lower(chara[i]);
	}
}

var s =instance_create(30,0,fadeIn)
s.time = 30
s.amount_per_cycle = 1/s.time;

ax_title = "Name?"

menu_push(id);

soundPlaySong(-1,0)

menu_update_values = nameInput_updateMove

destroy_timer = -1;

global.appearance_room = startGame