{
    "id": "4adb977b-cc20-429d-b142-3f56050eade1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "nameInput",
    "eventList": [
        {
            "id": "371f307f-42e5-4655-b91e-8d4b487c0f78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4adb977b-cc20-429d-b142-3f56050eade1"
        },
        {
            "id": "9cb37c12-3a05-40e9-8ddb-759c946c7f5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4adb977b-cc20-429d-b142-3f56050eade1"
        },
        {
            "id": "2817d699-f3ca-44c4-8536-ff138351bb5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4adb977b-cc20-429d-b142-3f56050eade1"
        },
        {
            "id": "514e843c-ed3e-414e-a68a-20e7bc0453a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4adb977b-cc20-429d-b142-3f56050eade1"
        },
        {
            "id": "bb8ea9bc-6e14-4a26-960a-d3587351d3c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "4adb977b-cc20-429d-b142-3f56050eade1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}