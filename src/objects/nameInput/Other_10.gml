/// @description Insert description here
// You can write your code in this editor
mouseHandler_clear()

menu_clear_option()

menu_add_option(current_name,"Your name",-1,-1)

lengy = chara_length;
	
if(uppercase == NAMEINPUT_CASE_SPECIAL_UPPER){
	lengy = special_upper_length
} else if (uppercase == NAMEINPUT_CASE_SPECIAL_LOWER){
	lengy = special_lower_length
}

for(var i = 0; i < lengy ; i++){
	if(uppercase == NAMEINPUT_CASE_SPECIAL_UPPER){
		menu_add_option(special_upper[i],"",-1,-1)	
	} else if (uppercase == NAMEINPUT_CASE_SPECIAL_LOWER){
		menu_add_option(special_lower[i],"",-1,-1)	
	} else {
		menu_add_option(display_chara[i],"",-1,-1)	
	}
}

menu_add_option("Confirm","Confirm your name.",nameInput_confirm,-1)
var scrip = nameInput_characters;
var stringer = "lowercase"
if(uppercase == NAMEINPUT_CASE_UPPER){
	menu_add_option(stringer,"lowercase",scrip,-1)
} 
else if(uppercase == NAMEINPUT_CASE_LOWER){
	var stringer = "Extra UPPER"
	menu_add_option(stringer,"Extra uppercase",scrip,-1)
} 
else if(uppercase == NAMEINPUT_CASE_SPECIAL_UPPER){
	var stringer = "Extra lower"
	menu_add_option(stringer,"Extra lowercase",scrip,-1)
} 
else if (uppercase == NAMEINPUT_CASE_SPECIAL_LOWER){
	stringer = "UPPERCASE"
	menu_add_option(stringer,"uppercase",scrip,-1)
}
menu_add_option("Clear","Clear your name.",nameInput_clear,-1)
menu_add_option("Delete","Delete last character.",nameInput_delete,-1)

// ok NOW you can add the mouse options

if(!talked_frame){
	tts_say(menu_name[menupos]);
	talked_frame = true
}

mouseHandler_add(30+245,145-50,30+580-120,145+5,soundTest_mouseName,current_name,true)

var sizerx = 40
var sizery = 60
var offsetx = -25
var offsety = -28

for(var i = 0; i < lengy; i++){
	var xx_shift = i % columns;
	var yy_shift = floor(i / columns)
	
	var posx = alpha_x+xx_shift*spacer_x;
	var posy = alpha_y+yy_shift*spacer_y
	
	mouseHandler_add(posx+offsetx,posy+offsety,
					posx+offsetx+sizerx,posy+sizery+offsety,nameInput_select,display_chara[i],true)	
}
var counter = 0
for(var i = lengy+1; i < menu_count; i++){
	var xx_shift = counter % 2;
	var yy_shift = floor(counter / 2)
	
	var posx = option_x+(xx_shift-.5)*275;
	var posy = option_y+(yy_shift-.5)*40
	mouseHandler_add(posx-100-30,posy-20,
					posx+100,posy+20,menu_script[i],menu_name[i],true)
	counter++
}

ax_string_clear()
access_string[0] = current_name;