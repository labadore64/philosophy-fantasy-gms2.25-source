/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var sizer = ds_list_size(diagrams);

for(var i = 0; i < sizer; i++){
	with(diagrams[|i]){
		instance_destroy();	
	}
}

with(card){
	instance_destroy()	
}

with(menuParent){
	if(active){
		event_user(2)	
	}
}

if(ds_exists(diagrams,ds_type_list)){
	ds_list_destroy(diagrams);	
}
cardInfo_deleteParticle()