/// @description Insert description here
// You can write your code in this editor

#macro CARD_DISPLAY_EFFECT_LINES 19
#macro CARD_DISPLAY_BIO_LINES 5

// Inherit the parent event
event_inherited();

if(global.voice){
	menu_select = cardinfo_read;
} else {
	menu_select = menu_cancel;
}
menu_up = cardInfo_up
menu_down = cardInfo_down
menu_left = menu_up
menu_right = menu_down

diagrams = ds_list_create()
diagram_length = 0;

card = -1;
card_id = -1;
bio = ""
card_text = "";
archetype = "";
archetype2 = "";

menu_draw = cardinfo_draw;
mouse_clear_selection = true
menu_help = "cardview"

RUMBLE_MOVE_MENU

if(global.menu_sounds){
	soundfxPlay(sfx_sound_view_card)
}


for(var i = 0; i < CARD_DISPLAY_EFFECT_LINES; i++){
	card_effect_desc[i] = "";
}

for(var i = 0; i < CARD_DISPLAY_BIO_LINES; i++){
	card_bio_desc[i] = "";
}

option_spacer = 23;
select_percent = 1;

cardInfo_initParticle()