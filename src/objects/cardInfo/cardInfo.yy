{
    "id": "55b90eb3-e4aa-4a85-81dc-9dc5368ad537",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "cardInfo",
    "eventList": [
        {
            "id": "fdfb9474-12f2-4461-9e55-c96fef4486b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55b90eb3-e4aa-4a85-81dc-9dc5368ad537"
        },
        {
            "id": "d7dd1124-1783-4999-9e46-59214917367e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "55b90eb3-e4aa-4a85-81dc-9dc5368ad537"
        },
        {
            "id": "d73d771d-99dc-4f77-8238-c686f7f891fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "55b90eb3-e4aa-4a85-81dc-9dc5368ad537"
        },
        {
            "id": "ae233653-24fd-426b-afb7-129d8059fdaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "55b90eb3-e4aa-4a85-81dc-9dc5368ad537"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}