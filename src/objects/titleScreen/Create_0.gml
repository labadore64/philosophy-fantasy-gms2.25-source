/// @description Insert description here
// You can write your code in this editor
song_play = false;

soundPlaySong(-1,0)

soundplay[0] = false;
soundplay[1] = false;
soundplay[2] = false;

newalarm[0] = TIMER_INACTIVE



newgame = false;
continuegame = false;
running_object = noone;

randomise();
last_card = -1;
card_id = irandom(obj_data.cards_total-1);
while(card_id == last_card || obj_data.card_type[card_id] != CARD_TYPE_PHILOSOPHER){
	card_id = irandom(obj_data.cards_total-1);
}
card_alpha = 1
card_x = 630;
card_y = 300;
card_size = .5
card_counter = 0;

card_display = card_create(card_x,card_y,card_size,card_id);
card_display.image_alpha = card_alpha
card_display.switching = false;
card_display.slappa = false;
variation_counter = 0

variation = .33 + sin(variation_counter*.01)*.15
last_am = 0;
amm = 0;
ammz = 0
// Inherit the parent event
event_inherited();

menu_cancel = -1;
x = 0;
y = 0;

xx = 75;
yy = 75;

option_start_x = 400;
option_start_y = 360
option_spacer = 60;
option_text_size = 3;

menu_draw = titleScreen_draw

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

titleScreen_initParticle();

flip_time = 200;

//newalarm[0] = flip_time

title[0] = "What is"
title[1] = "Philosophy?"
title[2] = ""
for(var i = 0; i < 3; i++){
	title_len[i] = string_length(title[i]);
	for(var j = 1;j < title_len[i]+1; j++){
		title_bounce[i,j-1] = string_char_at(title[i],j);
	}
}

menu_add_option("New Game","Start a new game.",titleScreen_script_newgame,-1)
if(!ds_list_empty(obj_data.current_deck)){
	menu_add_option("Continue","Continue where you left off.",titleScreen_script_continue,-1)
	menupos = 1;
}
menu_add_option("Credits","Credits to those who helped out.",titleScreen_script_credits,-1)
menu_add_option("Quit","Exit the game.",endGame_confirm,-1)

def_count--

mousepos_y = 400-35
draw_width = 350
mousepos_x = 400
ax_title = "Philosophy Fantasy Title Menu"

menu_push(id)

instance_create(0,0,fadeIn)

title_anim = 0;
title_surface = -1;;

con_uni_time = shader_get_uniform(shd_ripple,"time");
con_var_time_var = 0;

con_uni_mouse_pos = shader_get_uniform(shd_ripple,"mouse_pos");
con_var_mouse_pos_x = mouse_x - camera_get_view_x(0);
con_var_mouse_pos_y = mouse_y - camera_get_view_y(0);

con_uni_resolution = shader_get_uniform(shd_ripple,"resolution");
con_var_resolution_x = camera_get_view_width(0);
con_var_resolution_y = camera_get_view_height(0);

con_uni_wave_amount = shader_get_uniform(shd_ripple,"wave_amount");
con_var_wave_amount = 25; //higher = more waves

con_uni_wave_distortion = shader_get_uniform(shd_ripple,"wave_distortion");
con_var_wave_distortion = 50; //higher = less distortion

con_uni_wave_speed = shader_get_uniform(shd_ripple,"wave_speed");
con_var_wave_speed = 0.050; //higher = faster

mouseHandler_clear();
mouseHandler_add(0,0,800,600,title_skip_animation,"Continue");

menu_created = false;
menu_do = false;