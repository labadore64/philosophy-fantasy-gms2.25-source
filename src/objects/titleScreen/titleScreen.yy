{
    "id": "61156f5e-205c-4500-b28e-9b363a42084f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "titleScreen",
    "eventList": [
        {
            "id": "28468959-3c84-4926-b8c9-f4c5576ec1c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61156f5e-205c-4500-b28e-9b363a42084f"
        },
        {
            "id": "d81ef909-8068-49aa-86c0-d3aa397308ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "61156f5e-205c-4500-b28e-9b363a42084f"
        },
        {
            "id": "551f1dcd-ee21-4979-b054-be4cc9c75722",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "61156f5e-205c-4500-b28e-9b363a42084f"
        },
        {
            "id": "3f6cf8c8-55f3-48d9-89b3-4f3857171403",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "61156f5e-205c-4500-b28e-9b363a42084f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ebf24ee-c273-4aa8-b027-e4de66370aaf",
    "visible": true
}