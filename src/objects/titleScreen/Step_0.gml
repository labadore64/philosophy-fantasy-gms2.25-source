/// @description Insert description here
// You can write your code in this editor
if(newalarm[0] > TIMER_INACTIVE){
	newalarm[0] -= ScaleManager.timer_diff;	
	if(newalarm[0] < 0){
		event_perform(ev_alarm,0);
		if(newalarm[0] < 0){
			newalarm[0] = TIMER_INACTIVE;
		}
	}
}
// Inherit the parent event
event_inherited();

repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff
shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;

// do floating effect
card_counter+= ScaleManager.timer_diff;
/*
card_counter++;
var y_adjust = card_y + sin(card_counter*.05)*20;
with(card_display){
	card_y = y_adjust
	card_calculate_points();	
}*/

/*
if(global.menu_animate){
	variation_counter+=ScaleManager.timer_diff
	var curr = variation_counter;
	last_am = amm;
	amm = sin(variation_counter*.035)
	ammz = sin(variation_counter*.02)
	var varzi = amm*.1+1 + ammz*.35
	var varz2 = 1 - ammz*.15
	var varzis = .5+amm*.5

	if(amm > last_am && card_display.slappa){
		card_display.switching = !card_display.switching
		card_display.front = !card_display.front
		card_display.display_front = !card_display.display_front
		card_display.surface_update = true
		card_display.slappa = false
		if(card_display.front){
			last_card = card_id
			card_id = irandom(obj_data.cards_total-1);
			while(card_id == last_card || obj_data.card_type[card_id] != CARD_TYPE_PHILOSOPHER){
				card_id = irandom(obj_data.cards_total-1);
			}

			card_display.image_blend = obj_data.type_color[obj_data.card_element[card_id]];
			card_display.name_display = string_copy(obj_data.card_name[card_id],1,17);

			card_display.card_id = card_id
			with(card_display){
				card_update();		
			}
			card_display.image_alpha = card_alpha
		}
	} else if(amm < last_am && !card_display.slappa){
		card_display.slappa = true
	}

	with(card_display){
		y = ystart-(cos(curr*.025)*.5+.5)*20
		if(switching){
			point_scale_x[0] = varzi;
			point_scale_x[1] = varz2
			point_scale_x[2] = varzi;
			point_scale_x[3] = varz2
			point_scale_y[0] = varz2
			point_scale_y[1] = varzi;
			point_scale_y[2] = varz2
			point_scale_y[3] = varzi;
		} else {
			point_scale_x[1] = varzi;
			point_scale_x[0] = varz2
			point_scale_x[3] = varzi;
			point_scale_x[2] = varz2
			point_scale_y[1] = varz2
			point_scale_y[0] = varzi;
			point_scale_y[3] = varz2
			point_scale_y[2] = varzi;
		}
		x_scale=varzis
		card_reset_sizespecial();
	}
}
*/
if(newgame){
	if(!instance_exists(running_object)){
		obj_data.story_counter = 0;
		global.playerdata = "";
		story_script_execute();
	}
}
if(continuegame){
	if(!instance_exists(running_object)){
		room_goto(MainRoom)	
	}
}

if(image_speed > 0){
	title_anim += ScaleManager.timer_diff;
	if(!song_play){
		if(title_anim > 74){
			soundPlaySong(sfx_music_title,0)
			song_play = true;
			menu_created = true;
		}
	}
	
	if(!soundplay[0]){
		if(title_anim > 15){
			soundfxPlay(sfx_start1)
			soundplay[0] = true;
		}
	}
	
	if(!soundplay[2]){
		if(title_anim > 40){
			soundfxPlay(sfx_start3)
			soundplay[2] = true;
		}
	}
	
	if(!soundplay[1]){
		if(title_anim > 70){
			soundfxPlay(sfx_start2)
			soundplay[1] = true;
		}
	}
	
	if(title_anim > 143){
		title_anim = 143;
		image_speed = 0;
		
	}
}

if(menu_created && !menu_do){
	menu_do = true;
	mouseHandler_clear();
	mouseHandler_menu_default()
}

con_var_time_var+= ScaleManager.timer_diff;