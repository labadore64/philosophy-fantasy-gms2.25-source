{
    "id": "aa31c5a5-88e5-4588-b28f-b835946a5350",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelCursor",
    "eventList": [
        {
            "id": "3baf99cb-eeaf-4733-a3ab-fbfe5ba940ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aa31c5a5-88e5-4588-b28f-b835946a5350"
        },
        {
            "id": "9696aec0-f302-4b1a-bf9f-5ff096029abd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa31c5a5-88e5-4588-b28f-b835946a5350"
        },
        {
            "id": "5b1ea1a8-a85b-404b-9013-4b402be2db4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "aa31c5a5-88e5-4588-b28f-b835946a5350"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}