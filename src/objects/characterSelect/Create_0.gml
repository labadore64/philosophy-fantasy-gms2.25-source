/// @description Insert description here
// You can write your code in this editor
event_inherited()
menu_up = characterSelect_moveUp
menu_down = characterSelect_moveDown
menu_left = characterSelect_moveLeft
menu_right = characterSelect_moveRight
menu_draw = characterSelect_draw
menu_select = characterSelect_select
menu_cancel = -1
surface_update=true

mouse_mode = false;
pan_up = false
pan_down = false
pan_left = false
pan_right = false

newgame = false;
running_object = noone

translation_rate_start = 25

if(global.menu_animate){
	translation_start = 200;
} else {
	translation_start = 0	
}

translation_decay_rate = .90

translation = translation_start
translation_rate = translation_rate_start;

color[0] = "Lightest"
color[1] = "Light"
color[2] = "Middle"
color[3] = "Dark"
color[4] = "Darkest"

gender[0] = "Male"
gender[1] = "Female"

ax_title = "Choose your character appearance. Press select to continue."

menu_push(id)
