{
    "id": "75271612-ca5b-40fe-9b21-cde80caf9846",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "characterSelect",
    "eventList": [
        {
            "id": "3e338cbf-8b89-4d65-90a5-afe1ee01c6c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75271612-ca5b-40fe-9b21-cde80caf9846"
        },
        {
            "id": "7cade145-4fd7-4ee5-a59c-05a30d387924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75271612-ca5b-40fe-9b21-cde80caf9846"
        },
        {
            "id": "2ffe5ace-6867-48aa-9a39-eba771931144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "75271612-ca5b-40fe-9b21-cde80caf9846"
        },
        {
            "id": "8fd973c9-c263-4812-856b-457fe11f2143",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "75271612-ca5b-40fe-9b21-cde80caf9846"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}