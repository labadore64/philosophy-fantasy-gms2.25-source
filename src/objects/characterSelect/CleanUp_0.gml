/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

	ini_open("config.ini")
	
	ini_write_string("ax","tts",boolToString(global.voice))
	ini_write_string("ax","tts_details",boolToString(global.speak_details))
	
	ini_write_string("graphics","enabled",boolToString(global.drawing_enabled))
	ini_write_string("graphics","shader_enabled",boolToString(global.enableShader))
	ini_write_string("graphics","fullscreen",boolToString(global.fullscreen))
	ini_write_string("graphics","high_contrast",boolToString(global.high_contrast))
	
	ini_write_string("control","stick",boolToString(global.gamepadRightAxis))
	ini_write_string("control","vibration",boolToString(global.gamepadVibration))

	ini_write_real("sound","sfx_vol",global.sfxVolume);
	ini_write_real("sound","music_vol",global.musicVolume)
	
	ini_write_string("control","mouse",boolToString(global.mouse_active))
	ini_write_real("battle","mouse_pan_speed",global.pan_speed)
	ini_write_real("battle","mouse_zoom_speed",global.zoom_speed)
	ini_write_string("battle","mouse_display_right",boolToString(global.mouse_right))
	ini_write_string("battle","fastbattle",boolToString(global.fastmode))
	ini_write_real("battle","difficulty",global.difficulty)
	
	ini_write_string("character","name",global.character_name)

	ini_close()
