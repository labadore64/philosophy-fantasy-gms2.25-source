/// @description Insert description here
// You can write your code in this editor
event_inherited()
if(!lock){
	if(global.mouse_active){
		if(MouseHandler.mousepos_x != MouseHandler.mouse_xprevious || MouseHandler.mousepos_y != MouseHandler.mouse_yprevious){
			if(!mouse_mode){
				surface_update=true	
			}
			mouse_mode = true
		}
	}
}


if(translation >= 0){
	surface_update = true;
	translation-=ScaleManager.timer_diff*translation_rate;

	translation_rate=translation_rate*translation_decay_rate;

	if(translation < 0 ){
		translation = 0	
	}
} else {
	translation = 0;
}


if(newgame){
	if(!instance_exists(running_object)){
		obj_data.story_counter = 1;
		story_script_execute();
	}
}