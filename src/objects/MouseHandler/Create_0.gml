/// @description Insert description here
// You can write your code in this editor

// main points
mouse_points[0,0] = 200;
mouse_points[0,1] = 200;
mouse_points[0,2] = 300;
mouse_points[0,3] = 300;

// transformed points
mouse_points_trans[0,0] = 200;
mouse_points_trans[0,1] = 200;
mouse_points_trans[0,2] = 200;
mouse_points_trans[0,3] = 200;
mouse_menupos[0] = false
mouse_hovered[0] = false;
mouse_hovered_last[0] = false;
ignore_menu = false;

mouse_text[0] = "";

mouse_menu_text[0] = ""
mouse_menu_script[0] = -1
mouse_menu_points_trans[0,0] = 200;
mouse_menu_points_trans[0,1] = 200;
mouse_menu_points_trans[0,2] = 200;
mouse_menu_points_trans[0,3] = 200;
mouse_menu_spacer = 1;


mouse_script[0] = mouseHandler_test_script
mouse_obj = noone
mouse_menu_obj = noone;

selected = -1;

mouse_active_position = -1

mouse_count = 0
mouse_menu_count = 0
clicked = false;
debug = true
clicked_countdown = -1;
active = true;

event_perform(ev_step,0)

depth = -100000

clicked_this_frame = false
menu_hover = false;

mouse_xprevious = 0
mouse_yprevious = 0
enabled = true;

mousepos_x =0
mousepos_y = 0
evaluated = false;