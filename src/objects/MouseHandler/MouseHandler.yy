{
    "id": "6397e46f-2e4f-40e3-ad8c-b01daba7af2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MouseHandler",
    "eventList": [
        {
            "id": "44f43e74-f85f-4993-8e92-232de66138e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6397e46f-2e4f-40e3-ad8c-b01daba7af2d"
        },
        {
            "id": "2d16782f-ee43-4f2b-a4cc-cef26e70b9d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6397e46f-2e4f-40e3-ad8c-b01daba7af2d"
        },
        {
            "id": "4ec79d1e-58e9-406f-96cb-27bbe63b4e3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 9,
            "m_owner": "6397e46f-2e4f-40e3-ad8c-b01daba7af2d"
        },
        {
            "id": "4134f874-317d-4b6d-89e6-440764bfcdb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6397e46f-2e4f-40e3-ad8c-b01daba7af2d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}