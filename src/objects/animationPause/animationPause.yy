{
    "id": "799b6e90-9684-4a34-a6e3-02627d67b4d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationPause",
    "eventList": [
        {
            "id": "4ee2b14b-3c33-4efb-85e3-7f2c6393e0a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "799b6e90-9684-4a34-a6e3-02627d67b4d0"
        },
        {
            "id": "1b6511de-1387-446c-9fc0-1e3aa6047f4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "799b6e90-9684-4a34-a6e3-02627d67b4d0"
        },
        {
            "id": "4f208829-1059-4cc6-938e-746b1a6fac8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "799b6e90-9684-4a34-a6e3-02627d67b4d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}