
if(global.inputEnabled){

	if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT]) ||
		keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT2]) ||
		global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
		mouse_check_button_pressed(mb_left)) {

		instance_destroy()
	}
}