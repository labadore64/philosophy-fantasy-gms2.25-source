{
    "id": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuParent",
    "eventList": [
        {
            "id": "594363c3-dafe-4f0f-b26a-b5b9b90eef21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        },
        {
            "id": "8d568845-8278-4947-8b75-81829538180e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        },
        {
            "id": "d1761e68-23d8-4e84-a591-080b72c7bdc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        },
        {
            "id": "f6f15663-f532-4216-a01e-2100909c21e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        },
        {
            "id": "9ca7b8e7-4269-429d-9a4f-16f1a6c3d3ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        },
        {
            "id": "9e5631b6-b209-4a00-89cc-918d170d91c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        },
        {
            "id": "5dcbee9a-8562-462c-b0ac-8ee71a8e8111",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "a776c784-4a84-48b1-b5f1-75afa06dd9fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}