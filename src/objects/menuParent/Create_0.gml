/// @description Insert description here
// You can write your code in this editor
alpha_adder = 0;
alpha_max = .5
alpha_rate = .05

pop_base = true

mouse_clear_selection = false;

// tooltips stored
tooltip[0] = noone;

tooltip_total = 0;

objGenericCreate();
mouseHandler_clear()
menu_width = 100;
menu_height = 100;

// if the menu occupies the whole screen. If it does, its rendered differently.
menu_fullscreen = false;

surf = -1;
menu_draw_surface = menuParent_drawSurface;

menu_draw = menuParent_draw; // this will draw the menu.
menu_step = menuParent_step; // what the menu does every step

menu_cancel = menuParent_cancel;
menu_select = menuParent_select;

menu_up = menuParent_moveUp;
menu_down = menuParent_moveDown;

menu_left = menu_up
menu_right = menu_down

menu_help = "";

menu_name[0] = "";
menu_desc[0] = "";
menu_script[0] = -1;
menu_sprite[0] = -1;

menu_count = 0;

menupos = 0; // pointer position

wrap = true

// only plays sound if tts is enabled
menu_sound_ax = true

textbox = noone;

// input definitions
key_name[0] = "";
key_desc[0] = "";
key_chara[0] = "";

def_count = 0

def_active = true;

default_key_def()

menu_use_generic = false

// values used for mouse
info_x_offset = 0;
info_y_offset = 0

option_spacer = 40;
option_x_offset = 0;
option_y_offset = 0;
per_line = 4;
option_top = 0;
mousepos_x = 0;
mousepos_y = 0
draw_width = 100
draw_height = 20
select_percent = 1

enable_scrollwheel = true

menu_update_values = menuParent_updateSurface

menu_mouse_activate = menuParent_mouse_create

scrollwheel_skip = 1

ax_title = "";

play_select_sound = true

view_card_script = -1;


// menu cancel button shit
#macro MENU_PARENT_CANCEL_DEFAULT_POS -1000000

cancel_button_enabled = false; // does this menu use the cancel button
cancel_button_x = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_y = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_width = 40; // width of the cancel button
cancel_button_height = 40; // height of the cancel button
cancel_sprite = spr_menu_cancel; // the cancel sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
cancel_draw_border = false; // whether to draw a border around the cancel sprite
cancel_pressed = false; // whether the cancel button is pressed
cancel_hover = 1; // the scale of the hovering animation

help_button_enabled = false; // does this menu use the help button
help_button_x = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_y = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_width = 40; // width of the help button
help_button_height = 40; // height of the help button
help_sprite = spr_menu_help; // the help sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
help_draw_border = false; // whether to draw a border around the help sprite
help_pressed = false; // whether the help button is pressed
help_hover = 1; // the scale of the hovering animation

right_click_move = false; // does this menu move with the right click button
right_clicked = false; // are you pressing down the button
left_dragging = false; // are you dragging with the left button
right_clicked_x = 0; // position of x when clicked
right_clicked_y = 0; // position of y when clicked
right_clicked_mx = 0; // position of mousex when clicked
right_clicked_my = 0; // position of mousey when clicked

mouse_menu_script = mouseHandler_menu_default;
render_menu = true;