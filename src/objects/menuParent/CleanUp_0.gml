/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(surface_exists(surf)){
	surface_free(surf);	
}

for(var i = 0; i < tooltip_total; i++){
	with(tooltip[i]){
		instance_destroy();	
	}
}