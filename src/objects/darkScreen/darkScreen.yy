{
    "id": "0f43622e-3adf-4b0b-84d1-e6b55488f7da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "darkScreen",
    "eventList": [
        {
            "id": "9a093de5-62a7-43c6-aa89-9a7dc4a2b860",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f43622e-3adf-4b0b-84d1-e6b55488f7da"
        },
        {
            "id": "dfb1f7f6-52d7-4d3d-8dda-e7a3f66d840f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f43622e-3adf-4b0b-84d1-e6b55488f7da"
        },
        {
            "id": "97efb05c-b368-403f-b091-c04ee5d5c64a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0f43622e-3adf-4b0b-84d1-e6b55488f7da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}