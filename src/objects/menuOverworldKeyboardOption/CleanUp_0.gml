/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(ds_exists(keybind_text,ds_type_list)){
	ds_list_destroy(keybind_text);	
}

if(ds_exists(key_ids,ds_type_list)){
	ds_list_destroy(key_ids)	
}

with(menuControl){
	please_draw_this_frame= true
}