{
    "id": "74425859-e35d-49ec-a3a2-7e851cb4428b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldKeyboardOption",
    "eventList": [
        {
            "id": "523d2c11-bd87-4f82-b2fe-2edc6f2dad0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74425859-e35d-49ec-a3a2-7e851cb4428b"
        },
        {
            "id": "5307c059-2dda-45de-966c-e87e02de0ae0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "74425859-e35d-49ec-a3a2-7e851cb4428b"
        },
        {
            "id": "20dfdb6d-4856-4984-b75b-9794e5861472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "74425859-e35d-49ec-a3a2-7e851cb4428b"
        },
        {
            "id": "b06a7565-cd8b-4f5b-b454-eb1f7340cb16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74425859-e35d-49ec-a3a2-7e851cb4428b"
        },
        {
            "id": "3630192a-bea3-4508-b619-8ba463e6e5e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "74425859-e35d-49ec-a3a2-7e851cb4428b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}