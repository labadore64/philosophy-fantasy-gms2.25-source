/// @description Insert description here
// You can write your code in this editor
with(TextBox){
	instance_destroy()	
}
window_set_cursor(cr_default);
// Inherit the parent event
event_inherited();
open_sound = sfx_sound_duel_details;
menu_draw = duelEnd_replaydraw
card_id = -1;
menu_selected = 0;

option_x_offset = 100-60
option_y_offset = 20;
option_spacer = 50;

title_x = 0;
title_y = 0
title = "";
draw_title = false;

menu_height = 230
menu_width = 450-50

disable_reset_sound = false;

//y+= 30
//x+= 40

card = noone;
parent = noone;

menu_help = "duel"

RUMBLE_MOVE_MENU

mousepos_y = 15;
mousepos_x = 60+45;
draw_width = 300

play_select_sound = false
play_open_sound = true;

execute = false;

menu_push(id);