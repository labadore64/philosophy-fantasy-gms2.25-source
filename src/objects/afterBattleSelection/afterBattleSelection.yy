{
    "id": "de07ee19-be7e-4769-b5ea-3ad85ee9750f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "afterBattleSelection",
    "eventList": [
        {
            "id": "43ae3ddf-c855-42f1-9eb0-e0361ede9045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de07ee19-be7e-4769-b5ea-3ad85ee9750f"
        },
        {
            "id": "6e031b7e-7b09-4624-a1e2-944b7a5fd88d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "de07ee19-be7e-4769-b5ea-3ad85ee9750f"
        },
        {
            "id": "0534fd59-7ebe-4afd-8040-be56d619b045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "de07ee19-be7e-4769-b5ea-3ad85ee9750f"
        },
        {
            "id": "36fa9622-1998-49f9-8ffb-bb2f773e40b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "de07ee19-be7e-4769-b5ea-3ad85ee9750f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}