/// @description Insert description here
// You can write your code in this editor

event_inherited();
//mouseHandler_clear()

if((global.voice || (global.press_continue && !global.dont_record))){
	if(global.menu_sounds){
		soundfxPlay(sfx_sound_menu_cancel)	
	}
}

if(!damage_done){
	var dambage = abs(total_damage);
	if(total_damage > 0){
		with(defending_hp){
			duelHealth_hp_drop(id,dambage,25)
		}
	} else if (total_damage < 0){
		with(attacking_hp){
			duelHealth_hp_drop(id,dambage,25)
		}
	}
}

if(total_damage > 0){
	with(defending_hp){
		duel_animation_move_card(parent.part[DUEL_FIELD_ACTIVE].card_array_size-1,parent.part[DUEL_FIELD_ACTIVE],parent.wait,false,false,power(2,ANIMATION_MOVE_FLAG_CARD_EFFECT))
	}
} else if (total_damage < 0){
	with(attacking_hp){
		duel_animation_move_card(parent.part[DUEL_FIELD_ACTIVE].card_array_size-1,parent.part[DUEL_FIELD_ACTIVE],parent.wait,false,false,power(2,ANIMATION_MOVE_FLAG_CARD_EFFECT))
	}
}

with(attacking_card){
	card_update();	
}
with(defending_card){
	card_update();	
}

menu_pop();
with(attacking_card){
	x = original_x
	y = original_y
	card_scale = original_scale
	text_display_mode = CARD_DISPLAY_MODE_NORMAL;
	surface_update = true
	card_reset_size();
}
with(defending_card){
	x = original_x
	y = original_y
	card_scale = original_scale
	text_display_mode = CARD_DISPLAY_MODE_NORMAL;
	surface_update = true
	card_reset_size();
}

instance_create(120,0,fadeIn)

with(duelController){
	test_victory = true;
	in_battle = false;
	//event_user(0)
}

if(instance_exists(attacking_hp)){
	duel_battle_attack_effect(attacking_hp.parent.part[DUEL_FIELD_ACTIVE],attacking_card.card_id)	
	duel_battle_defend_effect(defending_hp.parent.part[DUEL_FIELD_ACTIVE],defending_card.card_id)	
}
duel_animation_run_script(duelController_complete_battle_script)

// clears boosts
with(duelField){
	att_boost = 0
	def_boost = 0
}

with(duelController){
	battle_complete = true
}
