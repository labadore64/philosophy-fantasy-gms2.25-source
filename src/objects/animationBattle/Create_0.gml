/// @description Insert description here
// You can write your code in this editor
event_inherited();
menu_push(id)
menu_up = -1;
menu_down = -1;
menu_left = -1;
menu_right = -1;
menu_select = -1;
menu_cancel = -1;


attacking_card = -1;
defending_card = -1;

attacking_name = "";
defending_name = "";

attacking_hp = -1;
defending_hp = -1;

instance_create(120,0,fadeIn)

completed = false;

timer = 0;
complete_time = 80

destroy_time = complete_time+20;

shake_time = 40;
shake_sound = false;

menu_draw = duel_draw_battle
done_animate = false;

total_damage = 0;
damage_abs = 0;
damage_done = false;

super_effective = false;

double_select =true
menu_select = duel_anim_message_continue
mouseHandler_clear()
mouseHandler_add(0,0,800,600,duel_anim_message_continue,"")

wait_to_kill = (global.voice || (global.press_continue && !global.dont_record));