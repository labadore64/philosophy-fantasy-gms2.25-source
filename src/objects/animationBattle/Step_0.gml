/// @description Insert description here
// You can write your code in this editor
event_inherited();
timer += ScaleManager.timer_diff*FAST
var ledone_animate = done_animate;
if(!completed){
	if(timer > shake_time){
		if(!shake_sound){
			shake_sound = true;
			var tts_string = "";
			
			if(total_damage > 0){
				tts_string = attacking_card.name_display + " inflicted " + string(total_damage) + " damage to " + defending_name+"!"
				if(super_effective){
					soundfxPlay(sfx_sound_super_effective)
					RUMBLE_MOVE_ATTACK_STRONG
				} else {
					soundfxPlay(sfx_sound_hit)
					RUMBLE_MOVE_ATTACK
				}
			} else if (total_damage < 0){
				tts_string = defending_card.name_display + " reflected " + string(abs(total_damage)) + " damage to " + attacking_name+"!"
				soundfxPlay(sfx_sound_not_effective)
				RUMBLE_MOVE_ATTACK_WEAK
			}
			
			if(total_damage == 0){
				tts_string = "No damage was taken."
			}
			
			tts_say(tts_string)
		}
		
		var damb = total_damage;
		
		if(duelController.active_player == DUEL_PLAYER_ENEMY){
			with(Rumble){
				if(damb > 0){
					rumble_left=1;
					rumble_right=0;
				} else {
					rumble_left=0
					rumble_right=1
				}
			}
		} else {
			with(Rumble){
				if(damb < 0){
					rumble_left=1;
					rumble_right=0;
				} else {
					rumble_left=0
					rumble_right=1
				}
			}
		}
		
		if(total_damage > 0){
			with(defending_card){
				// shake defending dude	
				var h = 575;
				if(duelController.active_player == DUEL_PLAYER_ENEMY){
					h = 225	
				}
				
				adder1 = 0
				adder2 = 0
				
				if(global.menu_animate && !ledone_animate){
					var adder1 = irandom(8)
					var adder2 = irandom(8)
				}
				
				x = h + adder1 
				y = 375 + adder2 
				card_reset_size();
			}
		} else if (total_damage < 0){
			with(attacking_card){
				var h = 225;
				if(duelController.active_player == DUEL_PLAYER_ENEMY){
					h = 575	
				}
				adder1 = 0
				adder2 = 0
				
				if(global.menu_animate && !ledone_animate){
					var adder1 = irandom(8)
					var adder2 = irandom(8)
				}
				
				// shake attacking dude	
				x = h + adder1
				y = 375 + adder2
				card_reset_size();
			}
		}

		// do damage calc
		var dambage = abs(total_damage);
		if(!damage_done){
			if(total_damage > 0){
				with(defending_hp){
					duelHealth_hp_drop(id,dambage,25)
				}
			} else if (total_damage < 0){
				with(attacking_hp){
					duelHealth_hp_drop(id,dambage,25)
				}
			}
			damage_done = true;
		}

	}
	
	if(timer > complete_time ){
		done_animate = true
		if(!wait_to_kill){
			instance_create(0,0,fadeOut)
			completed = true;
		}
	}
} else {
	if(timer > destroy_time ){

		instance_destroy();
	}
}

check_input_select_pressed()
check_input_cancel_pressed()