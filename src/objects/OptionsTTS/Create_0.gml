/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
// Inherit the parent event
options_createDefault();

// add options
ds_map_add(options_map,"Text-to-Speech",global.voice)
ds_map_add(options_map,"TTS Details",global.speak_details)

// add menu options
option_number[0] = 0;
menu_add_option("TTS","Modify Text to Speech settings.",-2,-1)
menu_add_option("Text-to-Speech","Enable Text to Speech.",-1,0)
menu_add_option("TTS Details","Read more information with TTS.",-1,0)
options_createFinali();