{
    "id": "3bc5ab42-4fb2-4858-b43f-026a1874511f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "splashScreen",
    "eventList": [
        {
            "id": "d07e90fe-5e07-470c-aa78-f7d6235b22f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3bc5ab42-4fb2-4858-b43f-026a1874511f"
        },
        {
            "id": "bc85ccad-b226-4e41-bb73-3b9241e2a2f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bc5ab42-4fb2-4858-b43f-026a1874511f"
        },
        {
            "id": "a5772249-1bf7-44e7-bd86-bf5ec96efd2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3bc5ab42-4fb2-4858-b43f-026a1874511f"
        },
        {
            "id": "0061c43a-1c7a-42b3-bbd4-9b5bf4fdef7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3bc5ab42-4fb2-4858-b43f-026a1874511f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}