/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\diagram\\dia";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] =ini_read_string_length("data","name","dia",30);
	alt_text[itemcount] = ini_read_string_length("data","alt_text","Diagram",420);
	mini_text[itemcount] = ini_read_string_length("data","mini_text","",1);
	sprite[itemcount] = asset_get_index(ini_read_string_length("data","sprite","",IMPORT_MAX_SIZE_SPRITE));
	frame[itemcount] = ini_read_real("data","frame",1);
	xscale[itemcount] = ini_read_real("data","x_scale",1);
	yscale[itemcount] = ini_read_real("data","y_scale",1);
	
	xoffset[itemcount] = ini_read_real("data","x_offset",0);
	yoffset[itemcount] = ini_read_real("data","y_offset",0);
	
	r[itemcount] = ini_read_real("data","r",255);
	g[itemcount] = ini_read_real("data","g",255);
	b[itemcount] = ini_read_real("data","b",255);
	
	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;

