/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
tts_stop();
var list = -1;

with(PackSelect){
	list = pack_display[2].pack_id
}

pack_id = global.selected_pack_id
if(list == -1){
	instance_destroy();	
} else {
	var sizer = obj_data.pack_card_total[list]
	for(var i = 0; i < sizer; i++){
		var cardd = obj_data.pack_card[list,i];
		menu_add_option(obj_data.card_name[cardd],"",-1,spr_duel_cards)	
		card_sprite_index[i] = selectPack_getCardIndex(cardd);
		card_id[i] = cardd;
		card_have[i] = false
		if(obj_data.card_in_trunk[cardd] || ds_list_find_index(obj_data.current_deck,cardd) > -1){
			card_have[i] = true;
		}
	}
	
	spr_unload_card()

	for(var i = 0; i < sizer; i++){
		spr_load_card_front(card_id[i]);	
	}
}


menu_draw = packCardList_draw;
menu_up = packCardMoveUp;
menu_down = packCardMoveDown;
menu_select = packCard_select
menu_surf = -1;
menu_surf_update = false;
selected_draw_index=0
info_x_offset = 20;
info_y_offset = 495

option_spacer = 55;
option_x_offset = 20;
option_y_offset = 20+option_spacer;
per_line = 9
option_top = 0;
wrap = false;

menu_help = "cardlist"

draw_width = 800
mousepos_x = 400
mousepos_y = 10

ax_title = "Card list"

menu_push(id);