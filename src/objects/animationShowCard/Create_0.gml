/// @description Insert description here
// You can write your code in this editor

cancel_button_enabled = false; // does this menu use the cancel button
cancel_button_x = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_y = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_width = 40; // width of the cancel button
cancel_button_height = 40; // height of the cancel button
cancel_sprite = spr_menu_cancel; // the cancel sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
cancel_draw_border = false; // whether to draw a border around the cancel sprite
cancel_pressed = false; // whether the cancel button is pressed

help_button_enabled = false; // does this menu use the help button
help_button_x = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_y = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_width = 40; // width of the help button
help_button_height = 40; // height of the help button
help_sprite = spr_menu_help; // the help sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
help_draw_border = false; // whether to draw a border around the help sprite
help_pressed = false; // whether the help button is pressed

right_click_move = false; // does this menu move with the right click button
right_clicked = false; // are you pressing down the button
left_dragging = false; // are you dragging with the left button
right_clicked_x = 0; // position of x when clicked
right_clicked_y = 0; // position of y when clicked
right_clicked_mx = 0; // position of mousex when clicked
right_clicked_my = 0; // position of mousey when clicked

card = noone
timer = 45
alpha_fade_time = 20;
alpha_fade_amount = 1/alpha_fade_time;
RUMBLE_CARD_REVEAL

double_select =true
menu_select = duel_anim_message_continue
menu_cancel = menu_select;
mouseHandler_clear()
mouseHandler_add(0,0,800,600,duel_anim_message_continue,"")

wait_to_kill = (global.voice || global.press_continue) && !global.dont_record;