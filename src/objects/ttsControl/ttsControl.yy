{
    "id": "34bd35fc-4e91-473b-938b-b7c1b97b0e05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ttsControl",
    "eventList": [
        {
            "id": "92e29741-3511-4f02-bd57-0331e5ddf6d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "34bd35fc-4e91-473b-938b-b7c1b97b0e05"
        },
        {
            "id": "78ca0e63-c440-4e2d-8d3b-cffb24da5da9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34bd35fc-4e91-473b-938b-b7c1b97b0e05"
        },
        {
            "id": "abb7c279-198f-49db-ba44-7f94480b37db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 9,
            "m_owner": "34bd35fc-4e91-473b-938b-b7c1b97b0e05"
        },
        {
            "id": "b0f0b8a8-3260-4eef-8f25-a08e8be93f60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34bd35fc-4e91-473b-938b-b7c1b97b0e05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}