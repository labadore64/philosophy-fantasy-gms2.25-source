/// @description Insert description here
// You can write your code in this editor


if(global.inputEnabled){
	if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_TTS_STOP]) ||
		global.gamepad_state_array[KEYBOARD_KEY_TTS_STOP] == KEY_STATE_PRESS) {
			tts_stop()
	}
	if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_TTS_REPEAT])||
		global.gamepad_state_array[KEYBOARD_KEY_TTS_REPEAT] == KEY_STATE_PRESS) {
			tts_say(toRead_last)
	}
}
