/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
// Inherit the parent event
options_createDefault();

// add options
ds_map_add(options_map,"Fast Battles",global.fastmode)
ds_map_add(options_map,"Menu Side",global.mouse_right)
ds_map_add(options_map,"Zoom Speed",global.zoom_speed-1)
ds_map_add(options_map,"Pan Speed",global.pan_speed-1)
ds_map_add(options_map,"Press to Continue",global.press_continue)
ds_map_add(options_map,"Coin Toss",global.coin_flip)
ds_map_add(options_map,"Pause Animations",global.pause_anim);
ds_map_add(options_map,"Difficulty",global.difficulty)
ds_map_add(options_map,"Tooltips",global.tooltip_type);

// add menu options
option_number[0] = 0;
menu_add_option("General","General battle settings.",-2,-1)
menu_add_option("Fast Battles","Enable faster battles.",-1,0)
menu_add_option("Difficulty","Battle difficulty level.",-1,4)
menu_add_option("Pause Animations","Pause between effect animations.",-1,0)
menu_add_option("Press to Continue","Press select down to continue battles.",-1,0)
menu_add_option("Coin Toss","Coin toss to go first. Otherwise, its random.",-1,0)

menu_add_option("","",-2,-1)
option_number[1] = menu_count;
menu_add_option("Appearance","Modify battle appearance.",-2,-1)
menu_add_option("Menu Side","Which side the mouse menu appears.",-1,3)
menu_add_option("Tooltips","Card info tooltip display configuration.",-1,7)
menu_add_option("Zoom Speed","Speed of camera zoom.",-1,1)
menu_add_option("Pan Speed","Speed of camera pan.",-1,1)

options_createFinali();