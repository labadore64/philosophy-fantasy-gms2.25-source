/// @description Insert description here
// You can write your code in this editor
menuParentCreate()
event_inherited();
//initTimingAlarm();
filename = "test_book";

text_string = "";

//for(var i = 0; i < 40; i++){
menu_add_option("Test","A test string.",-1,-1)
//}
display_lines = 13

per_line = display_lines;

menu_draw = menuBookDraw;

menu_down = menuBookMoveRight
menu_up = menuBookMoveLeft

menu_left = menuBookMoveLeft
menu_right = menuBookMoveRight

newalarm[0] = 2;
newalarm[2] = 1;

draw_bg = false;

menu_control = menu_cancel

menu_select = menuBookSelect;

diagrams = ds_list_create()
diagram_length = 0;

ignore_tab = true

enable_scrollwheel = true

menu_cancel = menuParent_cancel

//menu_update_values = menuBookUpdate
wrap = false;

title_x_offset = 10; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 20 //xoffset for subtitle
subtitle_y_offset = 60; //yoffset for subtitle
option_x_offset = 10 //xoffset for options
option_y_offset = 100; //yoffset for options

option_space = 40; //how many pixels between each option
select_alpha = .25; //alpha of current selection
select_color = c_white; //selection color
select_percent = 1; //how much of the menu does the selection graphic occupy

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = c_white; //color of the title of the info box
info_subtitle_color = c_white; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

menu_step = menuHelpStep; // what the menu does every step

menuParent_menu_default_button()

menu_push(id)

selected_draw_index = 0;
option_top = 0;