/// @description Insert description here
// You can write your code in this editor
menu_clear_option();
var filename_id = working_directory + "resources\\book\\"+filename+".txt";
var list = ds_list_create();
var text = "";
var titler = "";
var display_linesize = 45
var queue = -1;
var event_id = ""; //if it has an item then it should be set!
if(file_exists(filename_id)){
	var file = file_text_open_read(filename_id);
	titler = string_replace_all(string_replace_all(file_text_readln(file),"\n",""),"\r","")
	event_id = string_replace_all(string_replace_all(file_text_readln(file),"\n",""),"\r","")
	while(!file_text_eof(file)){
		text = file_text_readln(file);
		text = textForceCharsPerLine(text,display_linesize);
		text = string_replace_all(text,"\n","~");
		
		text_string+= text+"\n";
		queue = split_string(text,"~");
		while(!ds_queue_empty(queue)){
			ds_list_add(list,ds_queue_dequeue(queue));
		}
		ds_queue_destroy(queue);
		
	}
	file_text_close(file);

}

var sizer = ds_list_size(list);

for(var i = 0; i < sizer; i++){
	menu_add_option(list[|i],"","",-1)
}

title = titler;
tts_say(titler)

ds_list_destroy(list);

x = 0;
y = 0;