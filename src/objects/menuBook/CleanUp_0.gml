/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var sizer = ds_list_size(diagrams);

for(var i = 0; i < sizer; i++){
	if(!is_undefined(diagrams[|i])){
		with(diagrams[|i]){
			instance_destroy();	
		}
	}
}
	
ds_list_destroy(diagrams);

with(menuControl){
	with(active_menu){
		event_user(2)
	}
}