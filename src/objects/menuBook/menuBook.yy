{
    "id": "ac9f79ca-6936-45e4-9757-60eb189dc5d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBook",
    "eventList": [
        {
            "id": "b5ca190d-e7c9-492c-b58c-a7ffd78b0e7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac9f79ca-6936-45e4-9757-60eb189dc5d6"
        },
        {
            "id": "0f991605-1937-43cc-9229-a14fb9586a03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "ac9f79ca-6936-45e4-9757-60eb189dc5d6"
        },
        {
            "id": "3a841262-24c5-425b-b12f-d416fe9a3a7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ac9f79ca-6936-45e4-9757-60eb189dc5d6"
        },
        {
            "id": "51fafdb8-432c-4e61-828b-191c8001dd96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ac9f79ca-6936-45e4-9757-60eb189dc5d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}