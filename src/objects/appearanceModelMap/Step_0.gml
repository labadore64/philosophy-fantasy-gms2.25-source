/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(menu_count > 8){
	if(mouse_wheel_down()){
		menupos+=9;
		if(menupos < 0){
			menupos = 0;	
		}
		if(menupos >= menu_count){
			menupos = menu_count-1	
		}
	
		selected_draw_index = menupos % 9;

		if(option_top < 0){
			option_top = 0;	
		}
		page = floor(menupos / 9)
		option_top = page*9
		appearanceModelMap_scrollUpdate();
		scroll_delay = 3
	} else if(mouse_wheel_up()){
		menupos-=9;
		if(menupos < 0){
			menupos = 0;	
		}
		if(menupos >= menu_count){
			menupos = menu_count-1	
		}
	
		selected_draw_index = menupos % 9;
		page = floor(menupos / 9)
		option_top = page*9
		if(option_top < 0){
			option_top = 0;	
		}
		appearanceModelMap_scrollUpdate();
	}
}
var handled = false;
// test buttons
var mousex = window_mouse_get_x()
var mousey = window_mouse_get_y()


if(MouseHandler.mouse_xprevious != MouseHandler.mousepos_x &&
	MouseHandler.mouse_yprevious != MouseHandler.mousepos_y){
	mouse_moved = true	
}

if(objGenericCanDo()){
	if(!keypress_this_frame){
		if(MouseHandler.mouse_menu_obj == id){
			if(mousex >= MouseHandler.mouse_points_trans[9,0] && 
				mousex <= MouseHandler.mouse_points_trans[9,2] &&
				mousey >= MouseHandler.mouse_points_trans[9,1] && 
				mousey <= MouseHandler.mouse_points_trans[9,3]){
				if(mouse_check_button_pressed(mb_left)){
						scrolling = true;
						handled = true
					}
		
				if(mouse_check_button(mb_left)){
					handled = true
					if(scrolling){
						if(global.drawing_enabled){
							if(mousey >= MouseHandler.mouse_points_trans[9,1] && 
								mousey <= MouseHandler.mouse_points_trans[9,3]){
							
								for(var i = 0; i < menu_count; i++){
									if(mousey > MouseHandler.mouse_points_trans[9,1]+global.scale_factor*scroll_increment*(i) &&
										mousey < MouseHandler.mouse_points_trans[9,1]+global.scale_factor*scroll_increment*(i+1)){
										var val = i*9
										if(val > menu_count-1){
											val = menu_count-1	
										}
										if(val != menupos){
											model_surf_update = true	
										}
										selected_draw_index = 0;
										menupos = val;
										page = floor(menupos / 9)
										option_top = page*9
										surf_main_update = true
										break;
									}
								}
							}
						}
					}
				}
			}
			if(mouse_check_button_released(mb_left)){
				scrolling = false;	
			}
		}
	}
}

if(!handled){
for(var i =0; i < 9; i++){
	if(mousex >= MouseHandler.mouse_points_trans[i,0] && 
		mousex <= MouseHandler.mouse_points_trans[i,2] &&
		mousey >= MouseHandler.mouse_points_trans[i,1] && 
		mousey <= MouseHandler.mouse_points_trans[i,3])
		{
			if(mouse_moved){
				var menupos_old = menupos
				var adjusted = false;
				menupos = i + option_top;
				if(menupos > menu_count-1){
					menupos = menu_count-1;
					adjusted = true
				}
				selected_draw_index = menupos % 9
				if(menupos != menupos_old){
					surf_main_update = true
				}
				mouse_moved = false;
			}
			
			if(mouse_check_button_pressed(mb_left)){
				appearanceModelMap_select()	
			}
		}
	}
}