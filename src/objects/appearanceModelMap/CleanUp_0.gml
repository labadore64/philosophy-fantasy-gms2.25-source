/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


for(var i = 0; i < 9; i++){
	if(surface_exists(model_surface[i])){
		surface_free(model_surface[i])	
	}
}
	
	
if(surface_exists(sample_surface)){
	surface_free(sample_surface)	
}

if(surface_exists(surf)){
	surface_free(surf)	
}

with(appearanceSet){
	draw_menu = true
	menu_surf_update = true
}