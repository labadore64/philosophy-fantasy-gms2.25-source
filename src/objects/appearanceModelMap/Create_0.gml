/// @description Insert description here
// You can write your code in this editor
event_inherited();
parent_update = -1;

parent = ds_stack_top(menuControl.menu_stack)

menu_draw = appearanceModelMap_draw;

menu_width = 360
menu_height = 560
title = "Item Select"

menupos = 0;

for(var i = 0; i < 9; i++){
	model_surface[i] = -1	
}

sample_surface = -1;
sample_update = true
model_surf_update = true

selected_draw_index = 0
wrap = false
enable_scrollwheel = false
scroll_delay = -1

menu_right = appearanceModelMap_moveRight
menu_left = appearanceModelMap_moveLeft

menu_up = appearanceModelMap_moveUp
menu_down = appearanceModelMap_moveDown

menu_select = appearanceModelMap_select;

page = 0;
page_total = 0;
scrolling = false;	

appearanceModelMap_scrollUpdate();

with(appearanceSet){
	draw_menu = false	
}

mouse_moved = false;