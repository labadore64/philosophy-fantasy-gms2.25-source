{
    "id": "1c51735c-c176-4e77-83fa-2e9b3671ec76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "appearanceModelMap",
    "eventList": [
        {
            "id": "f89c291c-478e-4e5f-97b2-cc5563a62110",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c51735c-c176-4e77-83fa-2e9b3671ec76"
        },
        {
            "id": "46ad8f51-16dd-4f1f-afad-85d896b673ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1c51735c-c176-4e77-83fa-2e9b3671ec76"
        },
        {
            "id": "8ae763b9-7773-4e6b-831b-7675bd591edf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c51735c-c176-4e77-83fa-2e9b3671ec76"
        },
        {
            "id": "a1104443-0766-40c2-9570-fce2f08c7c1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1c51735c-c176-4e77-83fa-2e9b3671ec76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}