/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
duelist_img_sprite = -1;
// clear duel values since this is for AI duels.
global.duel_player[0] = ""
global.duel_player[1] = ""
global.duel_actions = -1
global.duel_actions[0] = "";
global.duel_random = -1
global.first_player = 0
global.dont_record = false;
global.goto_map = -1

soundPlaySong(-1,0)

Sname = -1;
particle1= -1

// this queue contains the things to run.
load_queue = ds_queue_create();

menu_draw = duelStart_draw

// only progress if this item doesn't exist
running_object = noone;

menu_push(id);

menu_cancel = -1;
menu_select = -1;

//shader
shader_id = -1;
shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

textbox = noone

menuControl.sprite_index = -1
menuControl.x = 120;
menuControl.y = 0;
menuControl.image_xscale = 1;
menu_control.image_yscale = 1

demo_get_duelists()
var spritename = working_directory + "resources\\bg\\" + duelist_bg_image  + ".png";
duelist_img_sprite =  sprite_add(spritename, 0, false, false, 0, 0)

// custom cutscene sprites go here
if(duelist_sprite == spr_portrait_roger){
	duelist_sprite = spr_portrait_roger_cutscene	
}

if(duelist_particle != -1){
	script_execute(duelist_particle)	
}

// test
ds_queue_enqueue(load_queue,
				gameStart_script0a,
				demoDuelist_script0,
				demoDuelist_script1,
				gameStart_script4);