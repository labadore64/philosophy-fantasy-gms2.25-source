/// @description Insert description here
// You can write your code in this editor
if(instance_exists(obj_data)){
	if(surface_exists(surf)){
		surface_free(surf);	
	}

	
	if(ds_exists(load_queue,ds_type_queue)){
		ds_queue_destroy(load_queue);	
	}
	with(menuControl){
		sprite_index = -1
	}
	titleScreen_deleteParticle();

	for(var i = 0; i < tooltip_total; i++){
		with(tooltip[i]){
			instance_destroy();	
		}
	}
	draw_texture_flush()
	room_goto(CointossRoom);
}

if(sprite_exists(duelist_img_sprite)){
	sprite_delete(duelist_img_sprite)	
}