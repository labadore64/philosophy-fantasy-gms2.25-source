{
    "id": "8ef95ad4-a3d9-4021-85ee-13664ffa0f06",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelStart",
    "eventList": [
        {
            "id": "8ffd3609-be9d-4b63-ae2d-8c25111606f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ef95ad4-a3d9-4021-85ee-13664ffa0f06"
        },
        {
            "id": "0be46b0a-8123-46aa-aea2-5566936320bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8ef95ad4-a3d9-4021-85ee-13664ffa0f06"
        },
        {
            "id": "c8350bfc-948c-4366-b44c-b79c9f8b547a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ef95ad4-a3d9-4021-85ee-13664ffa0f06"
        },
        {
            "id": "8c2ac75c-3513-433a-b37a-80570cd54f0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8ef95ad4-a3d9-4021-85ee-13664ffa0f06"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}