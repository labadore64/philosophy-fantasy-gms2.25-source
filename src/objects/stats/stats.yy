{
    "id": "0a547519-4072-4462-aec9-d97633c73c31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "stats",
    "eventList": [
        {
            "id": "cf58b3af-01ff-42d1-8a93-f303e44b2787",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a547519-4072-4462-aec9-d97633c73c31"
        },
        {
            "id": "3584ba0f-440c-4f1a-a19b-3e6af81a1de8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0a547519-4072-4462-aec9-d97633c73c31"
        },
        {
            "id": "dc9100e0-564b-4e95-b2a4-30ab71c4fac7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0a547519-4072-4462-aec9-d97633c73c31"
        },
        {
            "id": "c0764193-b241-4645-a11c-88147ce28463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0a547519-4072-4462-aec9-d97633c73c31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}