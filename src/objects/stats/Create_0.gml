/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_left = -1;
menu_right = -1;

menu_sound_ax = false;

menu_draw = stats_draw;

fave_cando = eventCheck(("fave"));

var counter = 0;
var card_total = 0;
// count total card types
for(var i = 0; i < obj_data.cards_total; i++){
	if(obj_data.card_in_trunk[i] > 0 || 
		ds_list_find_index(obj_data.current_deck,i) != -1){
		counter++;		
	}
	card_total+=obj_data.card_in_trunk[i];
}
card_total+= ds_list_size(obj_data.current_deck);

// collected cards
stat_value[0] = string(counter) + "/" + string(obj_data.cards_total);
stat_name[0] = "Cards Collected: "
// total cards
stat_value[1] = string(card_total);
stat_name[1] = "Total Cards: "

stat_value[2] = ""
stat_name[2] = ""

// game wins and losses
stat_value[3] = string(obj_data.wins);
stat_name[3] = "Wins: "
stat_value[4] = string(obj_data.losses);
stat_name[4] = "Losses: "
stat_value[5] = string(obj_data.draws);
stat_name[5] = "Draws: "

option_y_offset = 195
option_x_offset = 20;
option_space = 30;
draw_width = 800;
mousepos_x = 400

var scrip = menuParent_cancel

if(global.voice){
	scrip = menuParent_ttsread;	
} else{
	menu_select = menu_cancel;	
}

stat_size = array_length_1d(stat_value);
if(fave_cando){
	menu_add_option(obj_data.card_name[obj_data.current_main_card] + " Favorite Philosopher","",-1,-1)
} else {
	menu_add_option("","",-1,-1)	
}
for(var i = 0; i < stat_size; i++){
	menu_add_option(stat_value[i] + " " + stat_name[i], "",scrip,-1)	
}

menu_help = "stats"

ax_title = "Stats"

menuParent_menu_default_button();

surf_main = -1;

menu_push(id);

menuParent_default_accessString();

alpha = 1;
alpha_rate = .17;

if(global.menu_animate){
	alpha = 0;	
}

// load sprite data

var map = load_playerdata();


// used to create the custom portrait
for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
	portrait_property[i] = 0;
}

// used to create the custom portrait
for(var i = 0; i < TOTAL_CHARA_COLORS; i++){
	portrait_color[i] = c_white
}

if(ds_exists(map,ds_type_map)){
	var val = map[? "val"]
	if(!is_undefined(val)){
		var sizer = ds_list_size(val);
		for(var i = 0; i < sizer; i++){
			portrait_property[i] = val[|i];	
		}
	}
	
	var val = map[? "col"]
	if(!is_undefined(val)){
		var sizer = ds_list_size(val);
		for(var i = 0; i < sizer; i++){
			portrait_color[i] = val[|i];	
		}
	}
	
	ds_map_destroy(map);
}