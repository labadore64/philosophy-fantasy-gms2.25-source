{
    "id": "537a4021-3267-41d6-be79-12c5d0609b68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuControl",
    "eventList": [
        {
            "id": "dba0a9c7-6cfb-4ba6-8ba8-9de82bbd15fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "537a4021-3267-41d6-be79-12c5d0609b68"
        },
        {
            "id": "83273ae1-7d72-43c9-8d67-253f3ad271de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "537a4021-3267-41d6-be79-12c5d0609b68"
        },
        {
            "id": "8e20bfdb-86bb-478a-b063-c31850616db5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "537a4021-3267-41d6-be79-12c5d0609b68"
        },
        {
            "id": "4e613a7d-1570-46fc-bc48-231d3d05ba4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "537a4021-3267-41d6-be79-12c5d0609b68"
        },
        {
            "id": "14b651e6-246f-4940-b85e-1cc180cc73ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "537a4021-3267-41d6-be79-12c5d0609b68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}