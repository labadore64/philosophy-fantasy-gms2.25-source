/// @description Insert description here
// You can write your code in this editor
gpu_set_colorwriteenable(true,true,true,true)
if(global.drawing_enabled){
	if(dont_draw_frames < 0){
		with(active_menu ){
			if(menu_draw != -1){
				script_execute(menu_draw);	
				
				with(textbox){
					script_execute(menu_draw);	
				}
			}
		}
	}
	
	if(help_obj > -1){
		with(help_obj ){
			menuHelpDraw();	
		}
	}
	
	if(exit_obj > -1){
		with(exit_obj){
			endGame_draw();	
		}
	}
	
	with(MouseHandler){
		if(visible){
			mouseHandler_draw()
		}
	}
} else {
	draw_set_font(global.normalFont)
	draw_clear(c_black)
	draw_set_color(global.text_fgcolor)
	draw_set_halign(fa_center)
	draw_set_valign(fa_middle)
	
	with(ttsControl){
		draw_text_transformed_ratio(400,300,toRead_preview,2,2,0)
	}
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
}