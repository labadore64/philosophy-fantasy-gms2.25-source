/// @description Insert description here
// You can write your code in this editor
if(pause <= -1){
	with(active_menu){
		if(menu_step > -1){
			script_execute(menu_step);
		}
	}
}

if(pause > -1){
	pause-= ScaleManager.timer_diff;
	
	if(pause < -1){
		pause = -1;	
	}
}

if(dont_draw_frames > 0){
	dont_draw_frames-=  ScaleManager.timer_diff;
	if(dont_draw_frames > 0){
		dont_draw_frames = -1;	
	}
}

// You can exit the game whenever you want!
if(exit_obj < 0){
	if(global.inputEnabled){
		if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_EXIT]) ||
			global.gamepad_state_array[KEYBOARD_KEY_EXIT] == KEY_STATE_PRESS){
			endGame_confirm();
		}
	}
}

// You can get help if the active menu supports it!
if(exit_obj < 0 && help_obj < 0){
	if(global.inputEnabled){
		if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_HELP]) ||
			global.gamepad_state_array[KEYBOARD_KEY_HELP] == KEY_STATE_PRESS){
			if(!instance_exists(fadeIn) && !instance_exists(fadeOut)){
				if(instance_exists(active_menu)){
					if(active_menu.menu_help_action != -1){
						if(active_menu.menu_help != ""){
							with(active_menu){
								script_execute(menu_help_action)	
							}
						}
					} else {
						if(active_menu.menu_help != ""){
							help_obj = help_menu(active_menu.menu_help);
							keypress_this_frame = true;
						}
					}
				}
			}
		}
	}
}