/// @description Insert description here
// You can write your code in this editor
event_inherited();

// The amount of cards on the bench can never go higher than this.
size_limit = 3;

source_name = "Bench"

menu_activate = duel_bench_activate

menu_right = duel_bench_move_right
menu_left = duel_bench_move_left
menu_up = duel_bench_up;
menu_down = duel_bench_down

menu_select = duel_bench_select

// bench counter
// how many times you did a normal
// bench add. you can only add one
// per turn
