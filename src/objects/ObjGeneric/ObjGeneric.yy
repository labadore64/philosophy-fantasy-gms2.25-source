{
    "id": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ObjGeneric",
    "eventList": [
        {
            "id": "30b398bb-2831-4d6c-83c7-d76cb89e90fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40f71446-3930-4b99-90ec-0d26891a5c6b"
        },
        {
            "id": "b18ab3d7-3f9f-4538-b4c4-c8c60f0eb434",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "40f71446-3930-4b99-90ec-0d26891a5c6b"
        },
        {
            "id": "f540612b-4764-4521-89d2-c8e3bb658160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "40f71446-3930-4b99-90ec-0d26891a5c6b"
        },
        {
            "id": "a8ffda3d-1dd2-4620-b447-3b4d7afee2a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "40f71446-3930-4b99-90ec-0d26891a5c6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}