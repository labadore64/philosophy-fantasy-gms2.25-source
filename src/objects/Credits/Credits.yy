{
    "id": "9ada61ea-2179-4af4-b655-561ca4d77214",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Credits",
    "eventList": [
        {
            "id": "da8584c8-7c70-42ff-8424-665558c1a632",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ada61ea-2179-4af4-b655-561ca4d77214"
        },
        {
            "id": "2b0fe5f9-6fa6-4b8c-a48d-f8b8209d1701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9ada61ea-2179-4af4-b655-561ca4d77214"
        },
        {
            "id": "6fad8890-ea54-448c-97ae-882f89a005cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9ada61ea-2179-4af4-b655-561ca4d77214"
        },
        {
            "id": "bfc5424e-dece-4a6b-8ef1-b9d7cf901086",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9ada61ea-2179-4af4-b655-561ca4d77214"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}