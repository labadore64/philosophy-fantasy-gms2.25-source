/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
render_menu = false;

wrap = false;
menu_select = menu_cancel
//menu_up = -1;
//menu_down = -1;
menu_left = -1;
menu_right = -1;

menu_sound_ax = false;

menu_add_option("Programming/Assets By:","",-1,-1)
menu_add_option("PunishedFelix","",-1,-1)
menu_add_option("Special Thanks:","",-1,-1)
menu_add_option("Forthegy\n\n","",-1,-1)
menu_add_option("Anarchomuaddab\n(Machinic Unconscious\nHappy Hour Podcast)\n\n","",-1,-1)
menu_add_option("and all my friends!\n\n","",-1,-1)

menu_draw = draw_credits

//menu_step = credits_step

tts_say(menu_name[0] + " " + menu_name[1])
clear_key_def()
add_key_def("Cancel","Cancel","select")
add_key_def("Cancel","Cancel","cancel")

if(global.voice){
	add_key_def("Up","Move up","up")
	add_key_def("Down","Move down","down")		
}

alpha = 1;
alpha_rate = .17;

if(global.menu_animate){
	alpha = 0;	
}

ax_title = "Credits"


menuParent_menu_default_button();