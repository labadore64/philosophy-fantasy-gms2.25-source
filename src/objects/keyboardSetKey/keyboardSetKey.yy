{
    "id": "8294cf9f-116d-43cb-843c-d5ca09525edd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "keyboardSetKey",
    "eventList": [
        {
            "id": "fa17a8aa-fbe9-40a4-8bab-a920beddfbf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8294cf9f-116d-43cb-843c-d5ca09525edd"
        },
        {
            "id": "160bba51-2f49-4dcc-9624-e23092282b80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8294cf9f-116d-43cb-843c-d5ca09525edd"
        },
        {
            "id": "3a16e8be-c4ed-40bd-a464-28ebd270573c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8294cf9f-116d-43cb-843c-d5ca09525edd"
        },
        {
            "id": "5b02ce38-b683-4442-9557-ee6fe8dd751a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8294cf9f-116d-43cb-843c-d5ca09525edd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}