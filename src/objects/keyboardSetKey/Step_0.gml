/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();
if(active){
	if(keyboard_lastkey != 0){

		var oldbinding = keyboard_bind_get(keybind);
		if(oldbinding > -1){
			keyboard_bind_set(keybind,keyboard_lastkey);
		
			if(keybind_legal_change(key_get_index(keybind))){
				instance_destroy();
				tts_say("Key set for " + keyboardToString(keyboard_lastkey));
				if(global.menu_sounds){
				soundfxPlay(sfx_sound_menu_twinkle)
				}
			} else {
				// reset the binding
				keyboard_bind_set(keybind,oldbinding);
			}
		}
	}
}