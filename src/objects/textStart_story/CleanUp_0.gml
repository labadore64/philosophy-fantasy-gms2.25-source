/// @description Insert description here
// You can write your code in this editor
if(ds_exists(load_queue,ds_type_queue)){
	ds_queue_destroy(load_queue);	
}
with(menuControl){
	sprite_index = -1
}

if(Sname > -1){
	part_emitter_destroy_all(Sname);
}
if(particle1 > -1){
	part_type_destroy(particle1);	
}

if(surface_exists(surf)){
	surface_free(surf);	
}

if(surface_exists(part_surf)){
	surface_free(part_surf)	
}

for(var i = 0; i < tooltip_total; i++){
	with(tooltip[i]){
		instance_destroy();	
	}
}

if(sprite_exists(duelist_img_sprite)){
	sprite_delete(duelist_img_sprite)	
}

if(duel_id != ""){
	if(instance_exists(obj_data)){
		var val = -1;
		var lelel = string_lower(duel_id)
	
		for(var i = 0; i < obj_data.opponents_total; i++){
			if(string_lower(obj_data.opponent_name[i]) == lelel){
				val = i;
				break;
			}
		}
		obj_data.loaded_opponent = i;
		obj_data.loaded_duel = duel_level
		room_goto(CointossRoom);
	}
} else {

	story_goto_next()
}