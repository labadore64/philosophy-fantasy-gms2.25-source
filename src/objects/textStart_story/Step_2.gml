/// @description Insert description here
// You can write your code in this editor
// script execution
if(!instance_exists(running_object)){
	if(ds_queue_empty(load_queue)){
		instance_destroy()	
	} else {
		var script = ds_queue_dequeue(load_queue);
		if(script > -1){
			script_execute(script);	
		}
	}
}