{
    "id": "6be8e5e3-a04b-40dc-93f7-165b54981cca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "textStart_story",
    "eventList": [
        {
            "id": "27e8198a-e0af-4247-af9f-be15adca88f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6be8e5e3-a04b-40dc-93f7-165b54981cca"
        },
        {
            "id": "f027febf-cc1c-40dc-976f-de4a43d6d1a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6be8e5e3-a04b-40dc-93f7-165b54981cca"
        },
        {
            "id": "8f1bd17e-15e2-477c-9bd0-b45c12537877",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6be8e5e3-a04b-40dc-93f7-165b54981cca"
        },
        {
            "id": "ce4b0870-8c52-4d07-8a50-ded7693fe2a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6be8e5e3-a04b-40dc-93f7-165b54981cca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}