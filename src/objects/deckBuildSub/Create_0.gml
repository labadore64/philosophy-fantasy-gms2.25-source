/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

cancel_button_enabled = true; // does this menu use the cancel button
cancel_draw_border = true;
right_click_move = true
mouse_menu_script = deckBuildSub_updateMenu;

menu_draw = deckBuildSub_draw
card_id = -1;
menu_selected = 0;

option_x_offset = 80
option_y_offset = 20+20;
option_spacer = 50;

menu_height = 230
menu_width = 375
if(instance_exists(deckBuild)){
	ax_title = "Do what with " + obj_data.card_name[deckBuild.selected_card.card_id] + "?"
} else if (instance_exists(cardList)){
	ax_title = "Do what with " + obj_data.card_name[cardList.selected_card.card_id] + "?"
}
play_select_sound = false;
menu_push(id)

var axstring6 = ""
var axstring7 = ""
var axstring8 = ""

fave_cando = eventCheck(("fave"));

with(deckBuild){
	if(fave_cando){
		axstring6 = obj_data.card_name[obj_data.current_main_card] + ", favorite philosopher"
	}
	axstring7 = string(deck_total) + " out of 20, deck size"
	if(menu_selected == DECK_BUILD_TRUNK){
		axstring8 = string(trunk_quantity[menupos]) + " quantity"	
	}
}
menu_help="deckbuild"
if(instance_exists(deckBuild)){
	ax_card(deckBuild.selected_card.card_id)
} else if (instance_exists(cardList)){
	ax_card(cardList.selected_card.card_id)
}
access_string[6] = axstring6
access_string[7] = axstring7
access_string[8] = axstring8

mousepos_y = 15;
mousepos_x = 50;
draw_width = 250
