{
    "id": "e727fc22-16fa-4ef7-8f30-de185b2c6260",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "afterBattleWindow",
    "eventList": [
        {
            "id": "121af143-6c79-487a-8a37-cf1d1a5786d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e727fc22-16fa-4ef7-8f30-de185b2c6260"
        },
        {
            "id": "4baaffcb-bb72-4aa5-9013-d657b1b91ac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e727fc22-16fa-4ef7-8f30-de185b2c6260"
        },
        {
            "id": "a3fbdedf-8cb7-47c5-b9ca-6531b021fb5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e727fc22-16fa-4ef7-8f30-de185b2c6260"
        },
        {
            "id": "b531e7b4-3f88-4bbc-9477-a05aca6abad0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e727fc22-16fa-4ef7-8f30-de185b2c6260"
        },
        {
            "id": "aed1fae4-3d73-4b18-9f1d-0fd19acd271d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e727fc22-16fa-4ef7-8f30-de185b2c6260"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}