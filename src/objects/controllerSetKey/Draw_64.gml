/// @description Insert description here
// You can write your code in this editor
draw_set_alpha(.45);
draw_set_color(bg_color);

draw_rectangle(0,0,800,600,false);

draw_set_alpha(1);

draw_set_color(c_white)
draw_rectangle(x1-4,y1-4,x2+4,y2+4,false)

if(global.high_contrast){
	draw_set_color(c_black)
} else {
	draw_set_color(global.menu_bgcolor)
}
draw_rectangle(x1,y1,x2,y2,false)

draw_set_color(global.text_fgcolor)

draw_set_halign(fa_center);
draw_set_valign(fa_middle)
draw_text_transformed_ratio(400,300,draw_msg,2,2,0);

draw_set_halign(fa_left);
draw_set_valign(fa_top)