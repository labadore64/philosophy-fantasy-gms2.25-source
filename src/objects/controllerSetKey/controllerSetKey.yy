{
    "id": "54710bef-2434-47b4-bbc4-08369767bec1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "controllerSetKey",
    "eventList": [
        {
            "id": "9888d8fb-002b-45e5-9013-9d5c6259bcd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54710bef-2434-47b4-bbc4-08369767bec1"
        },
        {
            "id": "c36c711a-fab9-4040-861e-c3c5985e2ad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "54710bef-2434-47b4-bbc4-08369767bec1"
        },
        {
            "id": "5a3be5fa-f85f-4c51-9cc4-85736a48a920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54710bef-2434-47b4-bbc4-08369767bec1"
        },
        {
            "id": "61dd9557-0530-4b8e-a3c2-fba7d4ab9b5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "54710bef-2434-47b4-bbc4-08369767bec1"
        },
        {
            "id": "9aeb0684-4ed4-441a-ae12-164dc1b0ba16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "54710bef-2434-47b4-bbc4-08369767bec1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}