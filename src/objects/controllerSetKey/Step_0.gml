/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
if(active){
	var pressed = -1;

	if(gamepad_button_check_pressed(pad,gp_face1)){
		pressed = gp_face1;
	} else if(gamepad_button_check_pressed(pad,gp_face2)){
		pressed = gp_face2;
	} else if(gamepad_button_check_pressed(pad,gp_face3)){
		pressed = gp_face3;
	} else if(gamepad_button_check_pressed(pad,gp_face4)){
		pressed = gp_face4;
	} else if(gamepad_button_check_pressed(pad,gp_padu)){
		pressed = gp_padu;
	} else if(gamepad_button_check_pressed(pad,gp_padr)){
		pressed = gp_padr;
	} else if(gamepad_button_check_pressed(pad,gp_padl)){
		pressed = gp_padl;
	} else if(gamepad_button_check_pressed(pad,gp_padd)){
		pressed = gp_padd;
	} else if(gamepad_button_check_pressed(pad,gp_select)){
		pressed = gp_select;
	} else if(gamepad_button_check_pressed(pad,gp_start)){
		pressed = gp_start;
	} else if(gamepad_button_check_pressed(pad,gp_shoulderl)){
		pressed = gp_shoulderl;
	} else if(gamepad_button_check_pressed(pad,gp_shoulderlb)){
		pressed = gp_shoulderlb;
	} else if(gamepad_button_check_pressed(pad,gp_shoulderr)){
		pressed = gp_shoulderr;
	} else if(gamepad_button_check_pressed(pad,gp_shoulderrb)){
		pressed = gp_shoulderrb;
	} else if(gamepad_button_check_pressed(pad,gp_stickl)){
		pressed = gp_stickl;
	} else if(gamepad_button_check_pressed(pad,gp_stickr)){
		pressed = gp_stickr;
	}


	if(pressed != -1){
		gamepad_bind_set(keybind,pressed);
		if(global.menu_sounds){
		soundfxPlay(sfx_sound_menu_twinkle)
		}
		instance_destroy();
	}
}