/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(obj_data.last_game_state == 1){
	soundPlaySong(sfx_music_victory,0)
} else {
	soundPlaySong(-1,-1);	
}

Sname = -1;
particle1= -1

// this queue contains the things to run.
load_queue = ds_queue_create();

menu_draw = duelStart_draw

// only progress if this item doesn't exist
running_object = noone;

menu_push(id);

menu_cancel = -1;
menu_select = -1;

//shader

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")
shader_id = -1;
textbox = noone

menuControl.sprite_index = spr_portrait_mystery_man
menuControl.x = 120;
menuControl.y = 0;
menuControl.image_xscale = 1;
menu_control.image_yscale = 1
duelist_img_sprite = -1;
demo_get_duelists()
var spritename = working_directory + "resources\\bg\\" + duelist_bg_image  + ".png";
duelist_img_sprite =  sprite_add(spritename, 0, false, false, 0, 0)

// custom cutscene sprites go here
if(duelist_sprite == spr_portrait_roger){
	duelist_sprite = spr_portrait_roger_cutscene	
}

// test
if(duelist_particle != -1){
	script_execute(duelist_particle)	
}


ds_queue_enqueue(load_queue,
				gameStart_script0a,
				demoDuelist_script0,
				demoDuelist_after_script1,
				gameStart_script4);