{
    "id": "97b0338e-a0f3-47ca-8db2-fb46229d9d0a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelComplete",
    "eventList": [
        {
            "id": "b7c5ebb3-f638-492b-8f2a-959079987874",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97b0338e-a0f3-47ca-8db2-fb46229d9d0a"
        },
        {
            "id": "2d5b03dd-6772-4cee-ad84-61af8066a785",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "97b0338e-a0f3-47ca-8db2-fb46229d9d0a"
        },
        {
            "id": "dc49cdd2-db5f-42f9-a13b-92e793df0955",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "97b0338e-a0f3-47ca-8db2-fb46229d9d0a"
        },
        {
            "id": "7251bf8c-e242-4897-b9c0-b023692980c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "97b0338e-a0f3-47ca-8db2-fb46229d9d0a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}