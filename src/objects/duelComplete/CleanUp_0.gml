/// @description Insert description here
// You can write your code in this editor
if(instance_exists(obj_data)){
	if(surface_exists(surf)){
		surface_free(surf);	
	}


	for(var i = 0; i < tooltip_total; i++){
		with(tooltip[i]){
			instance_destroy();	
		}
	}

	if(ds_exists(load_queue,ds_type_queue)){
		ds_queue_destroy(load_queue);	
	}
	with(menuControl){
		sprite_index = -1
	}
	
	if(sprite_exists(duelist_img_sprite)){
		sprite_delete(duelist_img_sprite)	
	}
	
	titleScreen_deleteParticle();
	draw_texture_flush()
	if(global.force_pack_select || (global.do_pack_select && (obj_data.last_game_state == 1))){
		global.force_pack_select = false;
		room_goto(PackSelectRoom);
	} else {
		if(global.story_next == ""){
			room_goto(global.goto_map)
			global.goto_map = MainRoom
		} else {
			story_goto_next()	
		}
	}
}