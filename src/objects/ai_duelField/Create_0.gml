/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

parent = noone

//newAI_init_ai_field();

part[0] = instance_create(0,0,ai_duelDeck);
part[1] = instance_create(0,0,ai_duelHand);
part[2] = instance_create(0,0,ai_duelActive);
part[3] = instance_create(0,0,ai_duelTheory);
part[4] = instance_create(0,0,ai_duelBench);
part[5] = instance_create(0,0,ai_duelWaiting);
part[6] = instance_create(0,0,ai_duelDiscard);

ai_part_count = array_length_1d(part);

for(var i = 0; i < ai_part_count; i++){
	part[i].part_num = i;
	part[i].parent = id;
}	

att_boost = 0
def_boost = 0