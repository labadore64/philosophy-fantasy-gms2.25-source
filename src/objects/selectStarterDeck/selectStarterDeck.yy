{
    "id": "10de07a9-3437-469a-b3ed-a6f405ba3359",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "selectStarterDeck",
    "eventList": [
        {
            "id": "35397aa8-3af0-43a1-bb82-9cff44fcc423",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10de07a9-3437-469a-b3ed-a6f405ba3359"
        },
        {
            "id": "5d93c1e4-43b3-4da2-9701-17360873acd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "10de07a9-3437-469a-b3ed-a6f405ba3359"
        },
        {
            "id": "c011c1c1-798f-4eaf-9807-713cdceef226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "10de07a9-3437-469a-b3ed-a6f405ba3359"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}