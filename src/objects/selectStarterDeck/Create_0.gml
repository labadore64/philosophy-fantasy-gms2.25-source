/// @description Insert description here
// You can write your code in this editor
play_select_sound = false
// Inherit the parent event
event_inherited();
var spacer = 10
deck[0] = create_spirit_deck(40-spacer,100)
deck[1] = create_mind_deck(300-spacer,100)
deck[2] = create_nature_deck(560-spacer,100)

for(var i = 0; i < 3; i++){
	spr_load_card_front(deck[i].card_sprite)	
}

menu_cancel = -1;
menu_update_values = -1
menu_left = starterDeck_moveUp
menu_right = starterDeck_moveDown

menu_draw = starterDeck_draw;

show_text = "Select a Starter Deck.";

menu_select = starterDeck_select;

menu_up = menu_left;
menu_down = menu_right

pos_counter = 0;
select_move = 0;

menupos = 0;
menu_count = 0
wrap = true;
destroyed = false;

tts_say(show_text);

for(var i = 0; i < 3; i++){
	menu_add_option(deck[i].name + " " + deck[i].subtitle,"",-1,-1);
}

mouseHandler_clear()
mouseHandler_add(0,0,275,600,starterDeck_select,"Greeks",true)
mouseHandler_add(275,0,800-275,600,starterDeck_select,"Psych",true)
mouseHandler_add(800-275,0,800,600,starterDeck_select,"Marxism",true)