{
    "id": "22241f6a-c8b4-4658-9eca-065b9825a2bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "afterBattleQueue",
    "eventList": [
        {
            "id": "b776ccce-4f02-4ee9-8f52-9c7adc8ba9ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22241f6a-c8b4-4658-9eca-065b9825a2bc"
        },
        {
            "id": "38a8c276-9418-41c6-b1de-e45c8170d395",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "22241f6a-c8b4-4658-9eca-065b9825a2bc"
        },
        {
            "id": "eeb0f432-3741-4267-94b2-38fd98874fbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "22241f6a-c8b4-4658-9eca-065b9825a2bc"
        },
        {
            "id": "2e0c1574-e78f-4316-839a-182d53f0a5fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22241f6a-c8b4-4658-9eca-065b9825a2bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}