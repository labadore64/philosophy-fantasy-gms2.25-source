/// @description Insert description here
// You can write your code in this editor
if(queue_execute){
	if(!instance_exists(running_obj)){
		if(countdown < -1){
			
			if(ds_queue_empty(queue)){
				instance_destroy();	
			} else {
				var exec = ds_queue_dequeue(queue);
		
				// generate the object here
				var obj = duelEnd_process(exec);
		
				running_obj = obj;
				countdown = 15;
			}
		} else {
			countdown-= ScaleManager.timer_diff	
		}
	}
}