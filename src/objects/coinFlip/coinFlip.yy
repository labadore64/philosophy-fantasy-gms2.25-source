{
    "id": "e790d420-28bf-47f9-9e22-ef76d2791b88",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "coinFlip",
    "eventList": [
        {
            "id": "2055c57c-7d63-4e3e-93a4-95a07670e923",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e790d420-28bf-47f9-9e22-ef76d2791b88"
        },
        {
            "id": "ae806d96-7ef4-47a0-bdaa-0a99cffdb8d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e790d420-28bf-47f9-9e22-ef76d2791b88"
        },
        {
            "id": "094524ff-d1bd-4fa9-8eea-01d848091e78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e790d420-28bf-47f9-9e22-ef76d2791b88"
        },
        {
            "id": "407289ee-8572-4441-b8fc-06a4c89ec314",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e790d420-28bf-47f9-9e22-ef76d2791b88"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "824223f8-a611-4d03-b7ab-af55668b615c",
    "visible": true
}