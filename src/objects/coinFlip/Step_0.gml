/// @description Insert description here
// You can write your code in this editor
event_inherited()

if(completed){
	if(!instance_exists(fade_obj)){
		if(player_goes_first){
			global.first_player = DUEL_PLAYER_YOU
		} else {
			global.first_player = DUEL_PLAYER_ENEMY	
		}
		room_goto(BattleRoom)	
	}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff
shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;


repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(start_coin_flip){
	if(coin_flip_animate){
		if(!trigger_sound){
			trigger_sound = true;
			soundfxPlay(sfx_sound_coin_flip);
		}
		index += ScaleManager.timer_diff;

		if(index > spr_time){
			coin_flip_animate = false;	
		}
	} else {
		if(!finish_countdown_start){
			if(countdown > -1){
				countdown-=ScaleManager.timer_diff;
			} else {
				// finish countdown	
				if(coin_transition_x > 0){
					coin_transition_x -= coin_transition_rate;

					if(coin_transition_x <= 0){
						coin_transition_x = 0;	
						soundfxPlay(sfx_sound_coin_result)
						finish_countdown_start = true;
					}
				}
			}
		}
	}
}

if(finish_countdown_start){
	if(finish_countdown > -1){
		finish_countdown -= ScaleManager.timer_diff	
	} else {
		if(!display_window){
			display_window = true;
			if(predicted_heads == heads){
				called_wrong = false
				tts_say(coin_string + "!")
				running_object = show_window("You called the coin toss right! Choose to go first or second.")
				if(global.menu_sounds){
					soundfxPlay(sfx_sound_menu_twinkle)
				}
			} else {
				tts_say(coin_string + "!")
				if(player_goes_first){
					running_object = show_window("You called the coin toss wrong! You're going first!")		
				} else {
					running_object = show_window("You called the coin toss wrong! You're going second!")			
				}
				if(global.menu_sounds){
					soundfxPlay(sfx_sound_not_effective)
				}
			}
		} else {
			if(!instance_exists(running_object)){
				if(!completed){
					if(called_wrong){
						// kill
						coinFlip_done();
					} else {
						// do select routine
						calling = true
						if(!menu_choose){
							tts_say("First")
							menu_add_option("First","Go first.",coinFlip_done_choose,-1)
							menu_add_option("Second","Go second.",coinFlip_done_choose,-1)
							menu_choose = true;
							menupos = 0;
							mouseHandler_menu_default();
						}
					}
				}
			}
		}
	}
}