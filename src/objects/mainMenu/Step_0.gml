/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff
shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;


if(deckbuild){
	if(!instance_exists(running_object)){
		spr_unload_card();
		room_goto(DeckBuildRoom)	
	}
}

if(storyduel){
	if(!instance_exists(running_object)){
		spr_unload_card();
		story_load_element("hub");
		room_goto(pointAndClickRoom)
	}
}

if(review){
	if(!instance_exists(running_object)){
		spr_unload_card();
		room_goto(ReviewRoom)
	}
}
if(appearance){
	if(!instance_exists(running_object)){
		spr_unload_card();
		room_goto(appearanceRoom)
	}
}
if(go_back){
	if(!instance_exists(running_object)){
		spr_unload_card();
		room_goto(TitleRoom)
	}
}

// do floating effect

if(global.menu_animate){
	var old_alpha = card_display_alpha;
	card_display_alpha = sin(ScaleManager.spr_counter*card_display_multi)*.5 + .5;
	if(card_display_alpha > old_alpha){
		if(card_display_switch){
			card_display_switch = false;
			var old = card_display_index
			var counter = 0;
			while(card_display_index == old &&
				counter < 5000){
				counter++;
				card_display_index=irandom(card_trans_total-1)
				card_display_x = 50;
			}
			fade_surface_updated = true
		}
	} else {
		if(!card_display_switch){
			card_display_switch = true	
		}
	}

	card_display_x-=ScaleManager.timer_diff*.33
}

if(objGenericCanDo()){
	if(!keypress_this_frame){
		if(MouseHandler.mouse_menu_obj == id){
			var mousex = window_mouse_get_x()
			var mousey = window_mouse_get_y()
			if(mousex >= MouseHandler.mouse_points_trans[0,0] && 
				mousex <= MouseHandler.mouse_points_trans[0,2] &&
				mousey >= MouseHandler.mouse_points_trans[0,1] && 
				mousey <= MouseHandler.mouse_points_trans[0,3]){
				if(mouse_check_button_pressed(mb_left)){
					scrolling = true;
				}
			
				if(mouse_check_button(mb_left)){
					if(scrolling){
						mainMenu_Scroll(mousex,mousey);	
					}
				}
				if(mouse_check_button_released(mb_left)){
					scrolling = false;	
				}
			}
		}
	}
}