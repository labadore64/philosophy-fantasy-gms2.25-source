

/// @description Insert description here
// You can write your code in this editor
soundPlaySong(sfx_music_main_menu,0)
// Inherit the parent event
event_inherited();
spr_unload_card();
mainMenu_initParticle();
selected_draw_index = 0;
appearance = false
go_back = false;
deckbuild = false;
storyduel = false;
review = false;
replay = false
running_object = noone;

//shader
shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

menu_draw = mainMenu_draw
part_surf = -1;

var count_duel = 0;

for(var i = 0; i < obj_data.opponents_total; i++){
	if(obj_data.opponent_encounter[i] > -1){
		count_duel++;	
	}
}

//menu_add_option("Story Mode","Continue the Game Story",-1,-1)
menu_add_option("Continue Story","Continue the Story.",mainMenu_script_story_mode,-1)
if(eventCheck(("freegame"))){
	menu_add_option("Free Game","Play against a CPU opponent.",mainMenu_script_freeDuel,-1)
}
menu_add_option("Deck Build","Build your Deck.",mainMenu_script_deckbuild,-1)

// note: this is going to become a school activity
if(eventCheck(("review"))){
	//menu_add_option("Review","Review philosophy subjects.",mainMenu_script_review,-1)
}
if(PAID_VERSION){
	//&& eventCheck(("replay"))){
	menu_add_option("Replay","View game replays.",mainMenu_script_replay,-1)
}
menu_add_option("Avatar","Update your avatar.",mainMenu_script_appearance,-1)
menu_add_option("Game Stats","Game Stats",mainMenu_script_stats,-1)
menu_add_option("Options","Set game options.",mainMenu_script_options,-1)
menu_add_option("Credits","Credits to those who helped out.",titleScreen_script_credits,-1)
//menu_add_option("Back","Go to the title screen.",mainMenu_script_back,-1)
menu_add_option("Quit","Exit the Game.",endGame_confirm,-1)

option_start_x = 100;
option_start_y = 200-70;

option_spacer = 60;
option_text_size = 3;

menu_cancel = mainMenu_script_back;

instance_create(120,0,fadeIn)

// card

card_id = 0
if(global.high_contrast){
	card_alpha = 1
} else {
	card_alpha = .6;	
}
card_x = 630;
card_y = 350;
card_size = .55
card_counter = 0;

card_display = card_create(card_x,card_y,card_size,obj_data.current_main_card);
card_display.image_alpha = card_alpha

menupos = global.mainMenupos;

menu_up = mainMenu_up
menu_down = mainMenu_down
menu_left = mainMenu_up
menu_right = mainMenu_down

menu_help = "mainmenu";

option_x_offset = 50
option_y_offset = 180
per_line = 7
mousepos_x = 35+20
mousepos_y = 0-50
draw_width = 350
draw_height = 20
select_percent = 1
wrap = false;


ax_title = "Main menu"

menuParent_menu_default_button();

cancel_button_enabled = true;

menu_push(id)

// key def
global.appearance_room = MainRoom
card_transition[0] = -1;
var counter = 0;
for(var i = 0; i < obj_data.cards_total; i++){
	if(ds_list_find_index(obj_data.current_deck,i) > -1){
		card_transition[counter] = i;
		spr_load_card_front(i)
		counter++;
	}
}

card_trans_total = array_length_1d(card_transition)

card_display_x = 0;

card_display_alpha = sin(ScaleManager.spr_counter*.025)*.5 + .5;
card_display_index = 0;
card_display_multi = pi/(30*5);
card_display_switch = false;

fade_surface_updated = false
fade_surface = -1;
scrolling = true
replay_name = "";

event_user(2)

mainMenu_updateScroll()

audio_stop_sound(global.loop_sound_val);	