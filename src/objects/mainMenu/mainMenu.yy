{
    "id": "1ffc200d-ff94-47e0-85ea-d85f7657d675",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mainMenu",
    "eventList": [
        {
            "id": "9520e46b-6eb6-46a4-9b5d-98dc59de038e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ffc200d-ff94-47e0-85ea-d85f7657d675"
        },
        {
            "id": "8455dc13-5eae-48f5-8cb7-87c825f32838",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1ffc200d-ff94-47e0-85ea-d85f7657d675"
        },
        {
            "id": "5987f034-e22d-474e-b7df-ed2fa64a12b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ffc200d-ff94-47e0-85ea-d85f7657d675"
        },
        {
            "id": "03fc1261-4dfd-4660-a092-bce879c4b4e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1ffc200d-ff94-47e0-85ea-d85f7657d675"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}