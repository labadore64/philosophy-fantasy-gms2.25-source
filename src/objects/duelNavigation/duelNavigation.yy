{
    "id": "96ca0954-a591-4280-8f64-6a77a125b9ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelNavigation",
    "eventList": [
        {
            "id": "843bbab4-becc-438f-a5ff-9ba9774e3ed9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96ca0954-a591-4280-8f64-6a77a125b9ec"
        },
        {
            "id": "70b30abd-d250-4ea8-af83-48391ed5346e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "96ca0954-a591-4280-8f64-6a77a125b9ec"
        },
        {
            "id": "75712c3d-c2a9-4306-bc75-9657a1bce93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "96ca0954-a591-4280-8f64-6a77a125b9ec"
        },
        {
            "id": "980f7693-5bd3-449a-9be9-07fbb629fedb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "96ca0954-a591-4280-8f64-6a77a125b9ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}