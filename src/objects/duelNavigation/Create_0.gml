/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

with(duelController){
	if(pop_menu_active){
		duelController_pop_menu();	
	}
}

title = "Navigation"
option_x_offset = 5;
option_y_offset = 40

menu_draw = duelNavigation_draw;
mouse_menu_script = duelNavigation_mouse_make;

menu_right = -1;
menu_left = -1;
menu_up = -1;
menu_down = -1;
menu_cancel = duelNavigation_destroy;
menu_select = menu_cancel;

nav_x = 100;
nav_y = 75

menu_width = 200
menu_height = 230
cancel_button_enabled = true
cancel_draw_border = true
right_click_move = true

zoom_start = false;
zoom_index = 0; //index of mouse button handler

duelNavigation_update_zoom()