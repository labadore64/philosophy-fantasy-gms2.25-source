/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


if(global.mouse_active){
	if(mouse_check_button_released(mb_left)){
		if(cancel_pressed){
			script_execute(menu_cancel);	
			cancel_pressed = false
		}
	}
}

// zoom
if(zoom_start){
	if(mouse_check_button(mb_left)){
		//zoom_index
		var mousex = window_mouse_get_x();

		var mina =  MouseHandler.mouse_points_trans[zoom_index,0] 
		var maxa = MouseHandler.mouse_points_trans[zoom_index,2]
		
		if(mousex < mina){
			duelCamera.scale = duelCamera.max_scale
		} else if (mousex > maxa){
			duelCamera.scale = duelCamera.min_scale
		} else {
			duelCamera.scale = duelCamera.min_scale + (duelCamera.max_scale - duelCamera.min_scale) *
							(1-((mousex - mina)/(maxa-mina)))
		}
		duelNavigation_update_zoom();
	} else if (mouse_check_button_released(mb_left)){
		zoom_start = false;	
	}
} else {

	// drag

	if(	right_click_move ){
					
		var mx = (MouseHandler.mousepos_x - global.display_x)/global.scale_factor
		var my = (MouseHandler.mousepos_y - global.display_y)/global.scale_factor
					
		var start_move = false;
		var end_move = false;
		var move_it = false;

				
		if(left_dragging){
				
			if(mouse_check_button(mb_left)){
				// move the shit
				move_it = true;
					
			} else if(mouse_check_button_released(mb_left)){
				// update the position
					
				// update the shit
				end_move = true
			}
		} else {
			if(mouse_check_button_pressed(mb_left)){
				// check that you're in range:
				if(mx > x-7 && mx < x+menu_width+7 &&
					my > y-7 && my < y+option_y_offset){
					
					left_dragging = true;
					start_move = true;
				}
			} else if(mouse_check_button_pressed(mb_right)){
				// check that you're in range:
				if(mx > x-7 && mx < x+menu_width+7 &&
					my > y-7 && my < y+menu_height+7){
					
					right_clicked = true;
					start_move = true;
				}
			}
	
		}
			
		if(start_move){
			right_clicked_x = x
			right_clicked_y = y
			right_clicked_mx = mx
			right_clicked_my = my
			buttons_rendered = false;
					
			with(MouseHandler){
				mouse_count = 0
				mouse_menu_count = 0
			}
		}
			
		if(end_move){
			right_clicked = false
			left_dragging = false
			menuParent_menu_makeMouse();
			with(MouseHandler){
				clicked_countdown = 5	
			}
		}
			
		if(move_it){
			x = right_clicked_x + (mx - right_clicked_mx)
			y = right_clicked_y + (my - right_clicked_my)

			if(x < 5){
				x = 5;	
			}
			if(y < 5+170){
				y = 5+170;	
			}
			if(x > 800 - menu_width - 5){
				x = 800 - menu_width - 5	
			}
			if(y > 495 - menu_height - 5){
				y = 495 - menu_height - 5	
			}
					
			cancel_button_x = x + menu_width;
			cancel_button_y = y;
		}
		
	}
}