/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

cancel_button_enabled = true; // does this menu use the cancel button
cancel_draw_border = true;
right_click_move = true

if(global.menu_sounds){soundfxPlay(sfx_sound_question);}
menu_draw = confirmDialog_draw//endGame_draw
card_id = -1;
menu_selected = 0;

option_x_offset = 150+50
option_y_offset = 80
option_spacer = 50;

menu_height = 180
menu_width = 380+100

x= 280-70-25
y= 200

card = noone;
parent = noone;

title = "Are you sure?"
ax_title = title;

menu_up = menuParent_moveUp
menu_down = menuParent_moveDown
menu_cancel = menuParent_cancel;

confirm_script = -1

auto_delete = true;

menu_add_option("Yes","",confirmDialog_select,-1)
menu_add_option("No","",menuParent_cancel,-1)

draw_width = menu_width

tts_say(title + " " + menu_name[0])
mousepos_y+=10
mousepos_x+=40

window_set_cursor(cr_default);
menu_push(id)