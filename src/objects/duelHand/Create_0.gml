/// @description Insert description here
// You can write your code in this editor
event_inherited();

// the hand size limit. If its larger than this at certain times you will be forced to return cards to the deck.
hand_size_limit = 5;

size_limit = 99

ai_script = newAI_get_hand_commands;

menu_left = duel_hand_move_left;
menu_right = duel_hand_move_right;
menu_up = duel_hand_move_up;

menu_activate = duel_hand_activate;
menu_select = duel_hand_select

source_name = "Hand"