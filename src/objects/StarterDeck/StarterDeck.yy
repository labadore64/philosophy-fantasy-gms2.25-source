{
    "id": "38e92348-723e-4456-af2d-dee4d4f9e2ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "StarterDeck",
    "eventList": [
        {
            "id": "6457dd31-b28f-49b5-b20b-ac15229cbf15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38e92348-723e-4456-af2d-dee4d4f9e2ee"
        },
        {
            "id": "9f6947e9-8baa-4e04-9066-50a506c1f50d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "38e92348-723e-4456-af2d-dee4d4f9e2ee"
        },
        {
            "id": "6d4a2d90-c705-4dd1-b934-5d3b726a3e2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "38e92348-723e-4456-af2d-dee4d4f9e2ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}