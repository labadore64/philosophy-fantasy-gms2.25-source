/// @description Insert description here
// You can write your code in this editor
image_blend = $7F7FFF
sprite_index = spr_card_starterdeck
card_sprite = obj_data.card_sprite[card_from_name("aristotle")]
type_sprite = spr_typeSpirit

surface_update = false;
surf = -1;

card_width = 325
card_height = 440

name = "Greek\nClassics"
subtitle = "A well-rounded\ndeck made for\nbeginners."

scale = .65