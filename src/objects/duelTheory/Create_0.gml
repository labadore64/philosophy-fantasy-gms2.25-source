/// @description Insert description here
// You can write your code in this editor
event_inherited();

// only one card allowed in the theory zone.
size_limit = 1;

source_name = "Theory"

menu_activate = duel_theory_activate
menu_right = duel_theory_move_right
menu_up =duel_bench_up

menu_select = duel_theory_select;