/// @description Insert description here
// You can write your code in this editor
// macros

#macro TOTAL_CHARA_COLORS 20
#macro TOTAL_CHARA_PARTS 30

#macro CHARA_CUSTOM_COL_HAIR 0
#macro CHARA_CUSTOM_COL_SKIN 1
#macro CHARA_CUSTOM_COL_EYE 2
#macro CHARA_CUSTOM_COL_MOUTH 3
#macro CHARA_CUSTOM_COL_EARRING 4
#macro CHARA_CUSTOM_COL_HAT 5
#macro CHARA_CUSTOM_COL_GLASSES 6
#macro CHARA_CUSTOM_COL_SHIRT 7
#macro CHARA_CUSTOM_COL_NECKLACE 8
#macro CHARA_CUSTOM_COL_BACKGROUND 9
#macro CHARA_CUSTOM_COL_SNOUT 10

#macro CHARA_CUSTOM_VAL_CUTE 0
#macro CHARA_CUSTOM_VAL_BACKHAIR 1
#macro CHARA_CUSTOM_VAL_BODY 2
#macro CHARA_CUSTOM_VAL_NECK 3
#macro CHARA_CUSTOM_VAL_SHIRT 4
#macro CHARA_CUSTOM_VAL_NECKLACE 5
#macro CHARA_CUSTOM_VAL_FACE 6
#macro CHARA_CUSTOM_VAL_NOSE 7
#macro CHARA_CUSTOM_VAL_MOUTH 8
#macro CHARA_CUSTOM_VAL_EYE 9
#macro CHARA_CUSTOM_VAL_EYEBROW 10
#macro CHARA_CUSTOM_VAL_GLASSES 11
#macro CHARA_CUSTOM_VAL_EAR 12
#macro CHARA_CUSTOM_VAL_EARRING 13
#macro CHARA_CUSTOM_VAL_FRONTHAIR 14
#macro CHARA_CUSTOM_VAL_HAT 15
#macro CHARA_CUSTOM_VAL_SNOUT 16

// Inherit the parent event
event_inherited();
menu_push(id);

menu_draw = appearance_draw

menu_up = appearanceSet_moveUp
menu_down = appearanceSet_moveDown

menu_left = appearanceSet_moveLeft
menu_right = appearanceSet_moveRight

menu_tableft = appearance_tab;
menu_tabright = appearance_tab;

menu_cancel = appearance_model_cancel

selected_draw_index = 0
option_spacer = 70+40
option_x_offset = 500-30;
option_y_offset = 120;

// used to create the custom portrait
for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
	portrait_property[i] = 0;
}

wrap = false;

// used to create the custom portrait
for(var i = 0; i < TOTAL_CHARA_COLORS; i++){
	portrait_color[i] = c_white
}

appearance_constrain();
appearance_color_constrain();

portrait_color[CHARA_CUSTOM_COL_HAIR] = make_color_hsv(
											irandom_range(0,55),
											irandom_range(127,190),
											irandom_range(20,245))
portrait_color[CHARA_CUSTOM_COL_SKIN] = make_color_hsv(
											irandom_range(0,31),
											irandom_range(65,127),
											irandom_range(80,255))
portrait_color[CHARA_CUSTOM_COL_EYE] = make_color_hsv(
											irandom_range(0,254),
											irandom_range(200,255),
											irandom_range(127,245))
portrait_color[CHARA_CUSTOM_COL_MOUTH] = make_color_hsv(
											irandom_range(0,15),
											irandom_range(127,190),
											irandom_range(127,127))
portrait_color[CHARA_CUSTOM_COL_EARRING] = make_color_hsv(
											irandom_range(45,55),
											irandom_range(127,190),
											irandom_range(127,127))
portrait_color[CHARA_CUSTOM_COL_HAT] = make_color_hsv(
											irandom_range(0,254),
											irandom_range(200,255),
											irandom_range(127,245))
portrait_color[CHARA_CUSTOM_COL_GLASSES] = c_white
portrait_color[CHARA_CUSTOM_COL_SHIRT] = make_color_hsv(
											irandom_range(0,254),
											irandom_range(200,255),
											irandom_range(127,245))
portrait_color[CHARA_CUSTOM_COL_NECKLACE] = make_color_hsv(
											irandom_range(45,55),
											irandom_range(127,190),
											irandom_range(127,127))
										
portrait_color[CHARA_CUSTOM_COL_BACKGROUND] = $080808
										
portrait_property[CHARA_CUSTOM_VAL_CUTE] = 0
portrait_property[CHARA_CUSTOM_VAL_BACKHAIR] = irandom_range(1,property_part_count[CHARA_CUSTOM_VAL_BACKHAIR]-1)
portrait_property[CHARA_CUSTOM_VAL_BODY]= irandom(property_part_count[CHARA_CUSTOM_VAL_BODY]-1)
portrait_property[CHARA_CUSTOM_VAL_NECK]= irandom(property_part_count[CHARA_CUSTOM_VAL_NECK]-1)
portrait_property[CHARA_CUSTOM_VAL_SHIRT]= irandom(property_part_count[CHARA_CUSTOM_VAL_SHIRT]-1)
portrait_property[CHARA_CUSTOM_VAL_NECKLACE] = 0
portrait_property[CHARA_CUSTOM_VAL_FACE]= irandom(property_part_count[CHARA_CUSTOM_VAL_FACE]-1)
portrait_property[CHARA_CUSTOM_VAL_NOSE]= irandom(property_part_count[CHARA_CUSTOM_VAL_NOSE]-1)
portrait_property[CHARA_CUSTOM_VAL_MOUTH]= irandom(property_part_count[CHARA_CUSTOM_VAL_MOUTH]-1)
portrait_property[CHARA_CUSTOM_VAL_EYE]= 0//irandom(property_part_count[CHARA_CUSTOM_VAL_EYE]-1)
portrait_property[CHARA_CUSTOM_VAL_EYEBROW] =irandom(property_part_count[CHARA_CUSTOM_VAL_EYEBROW]-1)
portrait_property[CHARA_CUSTOM_VAL_GLASSES] = 0
portrait_property[CHARA_CUSTOM_VAL_EAR] = irandom_range(1,property_part_count[CHARA_CUSTOM_VAL_EAR]-1)
portrait_property[CHARA_CUSTOM_VAL_EARRING] = 0
portrait_property[CHARA_CUSTOM_VAL_FRONTHAIR] = portrait_property[CHARA_CUSTOM_VAL_BACKHAIR];
portrait_property[CHARA_CUSTOM_VAL_HAT] = 0
portrait_property[CHARA_CUSTOM_VAL_SNOUT] = 0

var map = load_playerdata()

if(ds_exists(map,ds_type_map)){
	var val = map[? "val"]
	if(!is_undefined(val)){
		var sizer = ds_list_size(val);
		for(var i = 0; i < sizer; i++){
			portrait_property[i] = val[|i];	
		}
	}
	
	var val = map[? "col"]
	if(!is_undefined(val)){
		var sizer = ds_list_size(val);
		for(var i = 0; i < sizer; i++){
			portrait_color[i] = val[|i];	
		}
	}
	
	ds_map_destroy(map);
}

appearance_configure_lists();

appearanceSet_Scrollupdate();

scrolling = false;	
draw_height = 10
draw_width = 280
mousepos_x = 140

event_user(0)

for(var i = 0; i < 4; i++){
	model_surface[i] = -1	
}
model_surf_update = false;
option_top_old = 0

titleScreen_initParticle()

part_type_color1(particle1,c_white);	

surf_main = -1;
surf_main_update = false;

menu_surf = -1;
menu_surf_update = false;
menupos_old = menupos

draw_menu = true;

instance_create(0,0,fadeIn)
exit_counter = -1;
exit_start = false
scrolling = false;