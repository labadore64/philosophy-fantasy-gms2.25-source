{
    "id": "27f34765-8430-4afe-a60c-b3db911b8c45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "appearanceSet",
    "eventList": [
        {
            "id": "7bfe44df-3f25-432e-85b8-18b25a4ae2d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27f34765-8430-4afe-a60c-b3db911b8c45"
        },
        {
            "id": "b7c3358d-cfe3-4707-86c7-438241ebe635",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "27f34765-8430-4afe-a60c-b3db911b8c45"
        },
        {
            "id": "9e113314-de64-4505-8f4e-de956da5dba9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27f34765-8430-4afe-a60c-b3db911b8c45"
        },
        {
            "id": "a8716f7c-a38c-4a14-9e78-236c756305a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "27f34765-8430-4afe-a60c-b3db911b8c45"
        },
        {
            "id": "e852415e-764f-45fc-9e26-5b2f9b1e5ae7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "27f34765-8430-4afe-a60c-b3db911b8c45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}