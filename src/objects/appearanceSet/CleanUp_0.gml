/// @description Insert description here
// You can write your code in this editor
event_inherited();

for(var i = 0; i < 4; i++){
	if(surface_exists(model_surface[i])){
		surface_free(model_surface[i])	
	}
}
	
if(surface_exists(surf_main)){
	surface_free(surf_main);	
}
	
if(surface_exists(menu_surf)){
	surface_free(menu_surf)	
}
	
titleScreen_deleteParticle()
	
draw_texture_flush()

var list1 = ds_list_create();
var list2 = ds_list_create();
var map = ds_map_create();

for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
	ds_list_add(list1,portrait_property[i])
}
for(var i = 0; i < TOTAL_CHARA_COLORS; i++){
	ds_list_add(list2,portrait_color[i])
}
	
ds_map_add_list(map,"val",list1)
ds_map_add_list(map,"col",list2)
	
save_playerdata(map)
ds_map_secure_save(map,"avatar.bin")

ds_map_destroy(map)
ds_list_destroy(list1)
ds_list_destroy(list2)


room_goto(global.appearance_room)