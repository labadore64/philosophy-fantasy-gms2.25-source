/// @description Insert description here
// You can write your code in this editor
repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(menupos != menupos_old){
	menu_surf_update = true	
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

if(objGenericCanDo()){
	if(!keypress_this_frame){
		if(MouseHandler.mouse_menu_obj == id){
			var mousex = window_mouse_get_x()
			var mousey = window_mouse_get_y()
			if(mouse_check_button_pressed(mb_left)){
				if(mousex >= MouseHandler.mouse_points_trans[0,0] && 
					mousex <= MouseHandler.mouse_points_trans[0,2] &&
					mousey >= MouseHandler.mouse_points_trans[0,1] && 
					mousey <= MouseHandler.mouse_points_trans[0,3]){
						scrolling = true;
					}
			}
			if(mouse_check_button(mb_left)){
				if(scrolling){
					appearanceSet_scroll(mousex,mousey);	
				}
			}
			if(mouse_check_button_released(mb_left)){
				scrolling = false;	
			}
		}
	}
}

if(option_top_old != option_top){
	if(mouse_wheel_up() || mouse_wheel_down()){
		model_surf_update = true	
		menu_surf_update = true
	}
}

if(exit_start){
	if(exit_counter > 0){
		exit_counter-=ScaleManager.timer_diff
	} else {
		menu_pop();
		instance_destroy();	
	}
}