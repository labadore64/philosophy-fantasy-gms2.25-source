/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_draw = duelOverlayCardChoose_draw

card[0] = duelController.field[0].active_monster.card_array[1];
card[1] = duelController.field[0].active_monster.card_array[2];

for(var i = 0; i < 2; i++){
	card_display[i] = card_create(250+i*300,300,.6,card[i])
}

menu_add_option(obj_data.card_name[card[0]],"",-1,-1)
menu_add_option(obj_data.card_name[card[1]],"",-1,-1)

menu_push(id);

info_surf= -1
info_surf_update = true

mouse_mode = false

pointer_x = 0
pointer_y = 0

window_set_cursor(cr_none);

menu_up = duelOverlayCardChoose_moveUp;
menu_down = duelOverlayCardChoose_moveDown
menu_left = duelOverlayCardChoose_moveUp;
menu_right = duelOverlayCardChoose_moveDown
menu_select = duelOverlayCardChoose_select

ax_title = "Select card to discard"