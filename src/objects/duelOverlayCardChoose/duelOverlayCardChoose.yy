{
    "id": "5abaacea-885f-4891-936d-360e6ba9fe32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelOverlayCardChoose",
    "eventList": [
        {
            "id": "4bcb402f-72f9-448e-8eb7-d0eaf6cbbcc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5abaacea-885f-4891-936d-360e6ba9fe32"
        },
        {
            "id": "6dc1bc0b-1998-4c81-8ce4-23588b16305e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5abaacea-885f-4891-936d-360e6ba9fe32"
        },
        {
            "id": "9cb17bc4-7833-4891-bb87-8fc01644d801",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5abaacea-885f-4891-936d-360e6ba9fe32"
        },
        {
            "id": "f266c17a-b076-4937-8a1e-ff7698e667a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5abaacea-885f-4891-936d-360e6ba9fe32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}