{
    "id": "7a2785cd-68f9-4c41-922e-8966949c8b6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "replayDuel",
    "eventList": [
        {
            "id": "f7c05897-7594-40f2-b4ac-3584dbb90ad8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a2785cd-68f9-4c41-922e-8966949c8b6f"
        },
        {
            "id": "85aed5eb-822d-4ad6-81a4-f6ceb0dfd1fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7a2785cd-68f9-4c41-922e-8966949c8b6f"
        },
        {
            "id": "7581be58-905e-4e7e-b3ff-b800111965f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7a2785cd-68f9-4c41-922e-8966949c8b6f"
        },
        {
            "id": "0d6c2fd3-7eed-468c-8fe9-8bb0e71ac2b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "7a2785cd-68f9-4c41-922e-8966949c8b6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}