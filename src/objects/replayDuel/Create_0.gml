created = 1;

// Inherit the parent event
event_inherited();

menu_step = -1;

selected_draw_index=0
info_x_offset = 20
info_y_offset = 495

option_spacer = 70;
option_x_offset = 50
option_y_offset = 120
per_line = 7
option_top = 0;
wrap = false

running_object = noone
activated = false;

replay_filename[0] = "[None]"

// display values for replay characters
for(var i = 0; i < 2; i++){
	replay_player[0,i] = "{}";
	replay_char_loaded[i] = -1;
	replay_char_name[i] = "";
	replay_char_surf[i] = -1;
	replay_surf_update[i] = false;
	replay_custom[i] = false;
	replay_sprite[i] = -1;
	replay_chara_id[i] = 0;
}


if(PAID_VERSION){
	created = replayDuel_create();	
}

if(created == 1){
	instance_destroy();
}