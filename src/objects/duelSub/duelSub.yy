{
    "id": "232856d9-8fc8-4eef-a0cb-f2a332349a3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelSub",
    "eventList": [
        {
            "id": "f76c9a79-7f13-44d7-aed2-bbb5d019a0c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "232856d9-8fc8-4eef-a0cb-f2a332349a3b"
        },
        {
            "id": "9f72e014-4a2b-4c46-844d-0471a2761990",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "232856d9-8fc8-4eef-a0cb-f2a332349a3b"
        },
        {
            "id": "db070979-6aff-4bc8-bf52-46a56d03493d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "232856d9-8fc8-4eef-a0cb-f2a332349a3b"
        },
        {
            "id": "f89c97a3-cdd8-41cd-9530-02e5da0f1319",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "232856d9-8fc8-4eef-a0cb-f2a332349a3b"
        },
        {
            "id": "984d2499-fcf9-4e25-872e-10b41b4f5334",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "232856d9-8fc8-4eef-a0cb-f2a332349a3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}