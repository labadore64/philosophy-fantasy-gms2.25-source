/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(disable_reset_sound){
	duelController.speak_tts = false	
}

//duelController.do_the_context_menu = false;
if(!duelController.do_the_context_menu){
	duelController.mouse_card_selected_last = -1
	duelController.mouse_card_selected = -1;
	with(duelController.selected_object){
		selected_object = noone
	}
	duelController.selected_object = noone
}

//duelController_resume_control()
with(card){
	instance_destroy();	
}

if(context_menu){
	duelController.context_menu = noone	
	duelCursor.move_countdown = 15000
}

if(execute){
	duelinput_execute(duelinput_length()-1);	
}

if(!global.dont_record){
	duelController.draw_pointer = true;
}

if(instance_exists(duelController.selected_object)){
	if(instance_exists(duelController.selected_object.selected_object)){
		if(duelController.selected_object.selected_object.object_index != duelDeck &&
			duelController.selected_object.selected_object.object_index != duelWaiting){
			duelField_tts_read();
		}
	}
}
		
duelField_tts_part_read()