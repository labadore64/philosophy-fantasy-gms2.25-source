/// @description Insert description here
// You can write your code in this editor
textbox = noone;
with(TextBox){
	instance_destroy()	
}
window_set_cursor(cr_default);
// Inherit the parent event
event_inherited();
open_sound = sfx_sound_duel_details;
menu_draw = duelSub_draw
card_id = -1;
menu_selected = 0;

option_x_offset = 100-60
option_y_offset = 20;
option_spacer = 50;

title_x = 0;
title_y = 0
title = "";
draw_title = false;

menu_height = 230
menu_width = 450

disable_reset_sound = false;

y+= 30
x+= 40
adjust_me = 0
card = noone;
parent = noone;

menu_help = "duel"

RUMBLE_MOVE_MENU

mousepos_y = 15;
mousepos_x = 60+45;
draw_width = 300

play_select_sound = false
play_open_sound = true;

execute = false;

context_menu = false;

cancel_button_enabled = true; // does this menu use the cancel button
cancel_draw_border = true;
right_click_move = true