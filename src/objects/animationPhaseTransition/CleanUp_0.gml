/// @description Insert description here
// You can write your code in this editor
with(TextBox){
	instance_destroy()	
}
mouseHandler_clear()
var me = id;
with(duelController){
	draw_phase_change = false;	
	if(running_obj == me){
		running_obj = noone
	}
	// check if input is empty
	
	event_user(0)
	input_active = true;
}

with(duelActive){
	duel_activemon_update()
}

with(duelController){
	duelCursor_mouse_selected()
	test_victory = true;
	do_phase = true
	do_phase_timer = 1
}

for(var i = 0; i < 2; i++){
	if(surface_exists(fade_surface[i])){
		surface_free(fade_surface[i]);	
	}
}

if(surface_exists(message_surf)){
	surface_free(message_surf)	
}

part_system_destroy(Sname);
part_type_destroy(particle1);


