/// @description Insert description here
// You can write your code in this editor

cancel_button_enabled = false; // does this menu use the cancel button
cancel_button_x = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_y = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_width = 40; // width of the cancel button
cancel_button_height = 40; // height of the cancel button
cancel_sprite = spr_menu_cancel; // the cancel sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
cancel_draw_border = false; // whether to draw a border around the cancel sprite
cancel_pressed = false; // whether the cancel button is pressed

help_button_enabled = false; // does this menu use the help button
help_button_x = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_y = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_width = 40; // width of the help button
help_button_height = 40; // height of the help button
help_sprite = spr_menu_help; // the help sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
help_draw_border = false; // whether to draw a border around the help sprite
help_pressed = false; // whether the help button is pressed

right_click_move = false; // does this menu move with the right click button
right_clicked = false; // are you pressing down the button
left_dragging = false; // are you dragging with the left button
right_clicked_x = 0; // position of x when clicked
right_clicked_y = 0; // position of y when clicked
right_clicked_mx = 0; // position of mousex when clicked
right_clicked_my = 0; // position of mousey when clicked


menu_up = -1;
menu_down = -1;
menu_left = -1;
menu_right = -1;
menu_select = -1;
menu_select_hold = -1;
menu_cancel = -1;
menu_help = "";
menu_help_action = - 1;

Sname = part_system_create();

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_line);
part_type_size(particle1,0.75,1,0,0);
part_type_scale(particle1,global.scale_factor,global.scale_factor);
part_type_color1(particle1,12632256);
part_type_alpha3(particle1,0,0.35,0);
part_type_speed(particle1,3,6,0.10,0);
part_type_direction(particle1,180,180,0,0);
part_type_gravity(particle1,0,0);
part_type_orientation(particle1,0,0,0,0,1);
//part_type_blend(particle1,1);
part_type_life(particle1,60,240);

emitter1 = part_emitter_create(Sname);
	part_emitter_region(Sname,emitter1, 800*global.scale_factor,
										950*global.scale_factor,
										0,
										600*global.scale_factor,
										ps_shape_rectangle,ps_distr_linear);
part_emitter_stream(Sname,emitter1,particle1,1);

if(duelController.phase_current == DUEL_PHASE_BATTLE){
	repeat(150){
		if(global.particles){part_system_update(Sname)}
	}
}


part_system_automatic_update(Sname,false)
part_system_automatic_draw(Sname,false)

double_select=true

trigger = false;
wait_to_kill = false;


y = 300;
alpha_decrease = .01

bg_height = 0;
bg_height_multi = .12
velocity[0] = 45;
velocity[1] = 2;
vel_flip = false;
image_alpha = 1;

message_surf = -1;
message_surf_update = false

timer = -1;
message = "";
var msg = "";
with(duelController){

	msg = phase_name[phase_current]

	input_active = false
}

with(cardInfo){
	instance_destroy();	
}

if(msg != ""){

	message = msg;
	tts_say(message)

	gamepad_rumble(.5,25)

	with(Rumble){
		rumble_left=1;
		rumble_right=0
		rumble_do_change=false;
	}

	menu_select = duel_anim_message_continue

	var me = id;


	with(animationPhaseTransition){
		if(id != me){
			instance_destroy()	
		}
	}

}

dialog_id[0] = duelController.field[0].active_monster.card_array[0];
dialog_id[1] = duelController.field[1].active_monster.card_array[0];

for(var i = 0; i < 2; i++){
	fade_surface[i] = -1;
	if(dialog_id[i] > -1){
		dialog_sprite[i] = obj_data.card_sprite[dialog_id[i]]
	}
}

// destroys battle if not destroyed for some reason
with(animationBattle){
	instance_destroy();	
}