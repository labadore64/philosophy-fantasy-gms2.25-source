/// @description Insert description here
// You can write your code in this editor
if(message == ""){
	instance_destroy();		
} else {
	
	repeat(ScaleManager.timer_diff_round){
		if(global.particles){part_system_update(Sname)}
	}

	if(!trigger && !instance_exists(TextBox)){
		trigger = true;
		mouseHandler_clear()
		mouseHandler_add(0,0,800,600,duel_anim_message_continue,message)
		if(duelController.phase_current == DUEL_PHASE_BATTLE){
			soundfxPlay(sfx_sound_duel_dialog);
		} else {
			
			soundfxPlay(sfx_sound_duel_phase_change)
		}
	}

	if(trigger){
		timer-=ScaleManager.timer_diff*FAST
		if(timer < -1){
			instance_destroy();	
		}

		if(x < 400){
			var val = velocity[0]*ScaleManager.timer_diff*FAST;
			x+=val
			bg_height+=val*bg_height_multi
			if(x >= 400){
				if(!vel_flip){
					x = 400;
					vel_flip = true;
				}
			}
		} else {
			var val = velocity[1]*ScaleManager.timer_diff*FAST;
			if(!vel_flip){
				x = 400;
				vel_flip = true;
			}
			x+=val
			bg_height+=val*bg_height_multi
			image_alpha-=alpha_decrease;
		}

		with(Rumble){
			var val = 1/rumble_time;
			rumble_left-=val
			rumble_right+=val
		}

		check_input_select_pressed()
	}

}

if(ScaleManager.updated){
	part_emitter_destroy(Sname,emitter1);
	emitter1 = part_emitter_create(Sname);
	part_type_scale(particle1,global.scale_factor,global.scale_factor);
	part_emitter_region(Sname,emitter1, 800*global.scale_factor,
										950*global.scale_factor,
										0,
										600*global.scale_factor,
										ps_shape_rectangle,ps_distr_linear);
	part_emitter_stream(Sname,emitter1,particle1,1);

	repeat(500){
		if(global.particles){part_system_update(Sname)}
	}
	
}