{
    "id": "0e0f9c8a-6d92-4867-8dce-7cc5418da61e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationTimer",
    "eventList": [
        {
            "id": "a9472a38-7f6b-4ff3-91c5-b252550113fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e0f9c8a-6d92-4867-8dce-7cc5418da61e"
        },
        {
            "id": "1bc82ed5-85a9-4e6d-9d59-bb7cee15f1b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e0f9c8a-6d92-4867-8dce-7cc5418da61e"
        },
        {
            "id": "9d33880a-3ec7-4ae5-a11c-220ee5068c6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0e0f9c8a-6d92-4867-8dce-7cc5418da61e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}