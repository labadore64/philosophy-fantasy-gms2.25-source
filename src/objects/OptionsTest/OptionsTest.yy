{
    "id": "1ac237a1-bd61-4fe6-b331-0d2a0a77de3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OptionsTest",
    "eventList": [
        {
            "id": "3c175759-8e79-46c8-af54-2c7d02d806c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ac237a1-bd61-4fe6-b331-0d2a0a77de3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "240beec4-ed1d-4b57-bef5-155fe131f7b9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}