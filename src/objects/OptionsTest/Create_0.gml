/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
// Inherit the parent event
options_createDefault();

// add options
ds_map_add(options_map,"Enable Graphics",global.drawing_enabled)
ds_map_add(options_map,"Enable Shaders",global.enableShader)
ds_map_add(options_map,"Fullscreen",global.fullscreen)
ds_map_add(options_map,"Frame Rate",(game_get_speed(gamespeed_fps)-10)*.1)

// add menu options
option_number[0] = 0;
menu_add_option("Display Options","Modify Display Options.",-2,-1)
menu_add_option("Fullscreen", "Enable fullscreen.",-1,0)
menu_add_option("Enable Graphics","Enable graphics. Turns on TTS if disabled.",-1,0)
menu_add_option("Enable Shaders","Enable shader effects.",-1,0)
menu_add_option("Frame Rate", "Change game frame rate.",-1,5)

options_createFinali();