

// disables auto-scaling
application_surface_draw_enable(false);

updated = false;

base_aspect_ratio = 800/600;

base_width = 800;
base_height = 600;

global.window_width = window_get_width();
global.window_height = window_get_height();
		
global.aspect_ratio = global.window_width/global.window_height;
		
if (global.aspect_ratio > base_aspect_ratio) // window's fatter than display, vertical letterbox
{
	global.letterbox_h = global.window_height;
	global.scale_factor = global.window_height/base_height;
	global.letterbox_w = base_width*global.scale_factor;
	global.display_x = (global.window_width) * 0.5 - global.letterbox_w* 0.5 ;
	global.display_y = 0;
}
else  // window's skinnier than display, horizontal letterbox
{
	global.letterbox_w = global.window_width;
	global.scale_factor = global.window_width/base_width;
	global.letterbox_h = base_height*global.scale_factor;
	global.display_x = 0;
	global.display_y = (global.window_height) * 0.5 - global.letterbox_h* 0.5 ;
}
		
updated = true;

spr_counter = 0;

save_screen = -1


// timing

timer_estimated_fps = 0;
timer_frame_count = 0;
timer_dt = 0;
timer_diff = 0;
timer_diff_round = 1

save_screen_max = 3