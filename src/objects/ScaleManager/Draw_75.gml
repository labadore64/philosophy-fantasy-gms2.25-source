/// @description Insert description here
// You can write your code in this editor

/// @description Insert description here
// You can write your code in this editor

if(save_screen == 0){
	fname = "screenshots\\"+string(current_year)+string(current_month)+string(current_day)+string(current_hour) + string(current_minute)+string(current_second)+string(irandom(255)) + ".png"
	
	screen_save(fname);
}

var framee = timer_frame_count;

timer_dt = timer_dt * 1; /* factor in the numerator of the bias ratio for rolling average */
timer_dt = timer_dt + (delta_time / 1000); /* bring in the newest delta time term */
timer_dt = timer_dt / 2; /* factor in the denominator of the bias ratio */

timer_frame_count += timer_dt * 60 / 1000

timer_diff = timer_frame_count - framee;
timer_diff_round = clamp(round(timer_diff),1,50)

timer_estimated_fps = 1000 / timer_dt; /* rolling estimated frame rate (for reference) */