/// @description Insert description here
// You can write your code in this editor
updated = false;
if ( window_get_width() != global.window_width ||
         window_get_height() != global.window_height )
    {
		if(window_get_width() != 0 && window_get_height() != 0){
	        global.window_width = window_get_width();
	        global.window_height = window_get_height();
   
	        surface_resize(application_surface, global.window_width, global.window_height)    
		
			global.aspect_ratio = global.window_width/global.window_height;
		
			if (global.aspect_ratio > base_aspect_ratio) // window's fatter than display, vertical letterbox
			{
				global.letterbox_h = global.window_height;
				global.scale_factor = global.window_height/base_height;
				global.letterbox_w = base_width*global.scale_factor;
				global.display_x = (global.window_width) * 0.5 - global.letterbox_w* 0.5 ;
				global.display_y = 0;
			}
			else  // window's skinnier than display, horizontal letterbox
			{
				global.letterbox_w = global.window_width;
				global.scale_factor = global.window_width/base_width;
				global.letterbox_h = base_height*global.scale_factor;
				global.display_x = 0;
				global.display_y = (global.window_height) * 0.5 - global.letterbox_h* 0.5 ;
			}
		
	        view_wport[0] = global.letterbox_w
	        view_hport[0] = global.letterbox_h
		
			updated = true;
		}
    }