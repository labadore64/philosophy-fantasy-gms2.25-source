/// @description Insert description here
// You can write your code in this editor

// delay for screenshot
if(save_screen > -1){
	save_screen++;
	if(save_screen == save_screen_max){
		save_screen = -1;
	}
}


// you can take a screenshot whenever you want!
if(global.inputEnabled){
	if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SCREENSHOT]) ||
		global.gamepad_state_array[KEYBOARD_KEY_SCREENSHOT] == KEY_STATE_PRESS){
		if(save_screen == -1){
			save_screen = 0;
			tts_say("Screenshot taken")
		}
	}
}

if(global.menu_animate){
	spr_counter += timer_diff;
} else {
	spr_counter = 0	
}