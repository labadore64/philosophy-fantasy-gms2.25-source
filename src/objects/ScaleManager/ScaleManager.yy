{
    "id": "28fec97c-bac1-4868-aeb7-ba1bd005bc07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ScaleManager",
    "eventList": [
        {
            "id": "3034de96-c671-466f-a594-9595c228c2bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "777658ee-d383-4b07-b70a-86508d909f5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "73f62247-13d7-4e2c-acc1-e85b0680d094",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "2dbeb987-7d6c-4ffa-a6bf-e8cc260cefad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "181cfe36-5145-4689-ada8-2d1f938a135c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}