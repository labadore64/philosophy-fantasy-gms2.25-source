/// @description Insert description here
// You can write your code in this editor
if(global.menu_animate){
	for(var i = 0; i < 2; i++){
		xx[i]-=move_x[i]*ScaleManager.timer_diff*FAST
		yy[i]-=move_y[i]*ScaleManager.timer_diff*FAST
	}
	// do sound position
	var posy = min(max(yy[0],DUEL_SOUND_POSITION_TOP),DUEL_SOUND_POSITION_BOTTOM)-DUEL_SOUND_POSITION_TOP
	var diff = DUEL_SOUND_POSITION_BOTTOM-DUEL_SOUND_POSITION_TOP;
	var amount = POSITION_PITCH_MIN-.1 + (posy / diff)*(+.2+POSITION_PITCH_MAX-POSITION_PITCH_MIN)
	audio_sound_pitch(sound_id,amount )
	
}
timer-=ScaleManager.timer_diff*FAST

duelController.card_surface_update = true;

if(timer < -1){
	for(var i = 0; i < 2; i++){
		if(instance_exists(card_place_location[i])){
			var card_id = card_place_id[i];
			var card_index = card_dest_place[i];
			with(card_place_location[i]){
				card_array[card_index] = card_id
				cards[|card_index] = card_id
			
				parent.selected_object = id;
				if(enemy){
					duelController.selected_object = duelController.field[1];	
				} else {
					duelController.selected_object = duelController.field[0];	
				}
				
				duelController.mouse_cleared=true
				duelController.field[0].surface_update = true;
				duelController.field[1].surface_update = true;
				info_surf_update = true;
			}
		}
	}
	with(duelActive){
		duel_activemon_update();	
	}
	instance_destroy();	
}