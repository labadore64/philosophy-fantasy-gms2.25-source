{
    "id": "e4221d5f-3a3a-4fee-bd85-726632d5bcb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "drawSwapCard",
    "eventList": [
        {
            "id": "43bdcff0-2544-44cb-a685-9d547e73e62a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e4221d5f-3a3a-4fee-bd85-726632d5bcb6"
        },
        {
            "id": "196b61b4-f1ca-4b24-9fa2-55d8d9ae09b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e4221d5f-3a3a-4fee-bd85-726632d5bcb6"
        },
        {
            "id": "4f9448c7-9d77-428d-9594-fa31ef0d5058",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e4221d5f-3a3a-4fee-bd85-726632d5bcb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}