/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(trigger){
	if(!instance_exists(running_object)){
		if(menupos > -1){
			if(sprite_cost[menupos] > 0){
				global.activity_points-= sprite_cost[menupos]	
				if(global.activity_points < 0){
					global.activity_points = 0	
				}
			}
		}
		
		menu_pop()
		instance_destroy()
	}
}

if(sprite_sfx_random[0] != -1){
	sfx_random_count -= ScaleManager.timer_diff;

	if(sfx_random_count < 0){
		var size = array_length_1d(sprite_sfx_random);
		random_sound = soundfxPlay(sprite_sfx_random[irandom(size-1)])
		sfx_random_count = random(500) + 300;
	}
}