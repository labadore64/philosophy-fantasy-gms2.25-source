/// @description Insert description here
// You can write your code in this editor
event_inherited();

menu_draw = pointAndClick_draw;
menu_cancel = pointAndClick_cancel;
menu_select = pointAndClick_select
mouse_menu_script = pointAndClick_mouse
menu_left = pointAndClick_left
menu_up = menu_left
menu_right = pointAndClick_right
menu_down = menu_right
menu_mouse_activate = mouse_menu_script

bg_image = ""
bg_title = "";
img_sprite = -1
part_surf = -1;
surf = -1;
running_object = instance_create(0,0,fadeIn)

sprites[0] = -1
sprite_x[0] = 0
sprite_y[0] = 0
sprite_xscale[0] = 1
sprite_yscale[0] = 1
sprite_alpha[0] = 1
sprite_color[0] = c_white
sprite_title[0] = ""
sprite_desc[0] = ""
sprite_next[0] = ""
sprite_nextroom[0] = ""
sprite_exist[0] = false;
spritewidth[0] =1
spriteheight[0] = 1
sprite_text[0] = ""
sprite_cost[0] = 0
sprite_sfx[0] = sfx_sound_menu_select
sprite_sfx_random[0] = -1;
sprite_show[0] = true
sprite_sfx_loop = -1;

random_sound = -1;
sfx_random_count = random(500) + 300;

play_select_sound = false;

energy_col[0] = $9999FF
energy_col[1] = $99FFFF
energy_col[2] = $99FF99
energy_col[3] = $FFFF99
energy_col[4] = $FF9999
energy_col[5] = $FF99FF

sprite_count = 0;

window_set_cursor(cr_none);

trigger = false;
menuParent_menu_default_button();
with(MouseHandler){
	mouse_active_position  = -1	
}

cancel_button_enabled = true;
menu_push(id)

cur_sprite = spr_duel_cursor
menupos = -1;
