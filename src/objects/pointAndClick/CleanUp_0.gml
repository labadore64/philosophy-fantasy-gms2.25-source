/// @description Insert description here
// You can write your code in this editor
event_inherited();

if(sprite_exists(img_sprite)){
	sprite_delete(img_sprite)	
}


if(surface_exists(surf)){
	surface_free(surf);	
}

if(surface_exists(part_surf)){
	surface_free(part_surf)	
}


for(var i = 0; i < sprite_count; i++){
	if(sprite_exists(sprites[i])){
		sprite_delete(sprites[i])	
	}
	
}

if(audio_exists(random_sound)){
	audio_stop_sound(random_sound);	
}


window_set_cursor(cr_default);

if(trigger){
	story_goto_next()
}