/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
// Inherit the parent event
options_createDefault();

// add options
ds_map_add(options_map,"Health Sound",global.hp_sound)
ds_map_add(options_map,"Positional Pitch",global.positional_pitch);
ds_map_add(options_map,"Menu Sound",global.menu_sounds);
ds_map_add(options_map,"Text Sound",global.text_sound)
ds_map_add(options_map,"Navigation Sounds",global.extra_sounds);

// add menu options
option_number[0] = 0;
menu_add_option("Sound Effects","Customize certain sound effect options.",-2,-1)
menu_add_option("Menu Sound","Enable menu sound effects.",-1,0)
menu_add_option("Text Sound","Enable text box sound effect.",-1,0)
menu_add_option("Health Sound","Plays a sound effect for damage.",-1,0)
menu_add_option("Navigation Sounds","Enable navigational sound effects.",-1,0)
menu_add_option("Positional Pitch","Positions pitch based on selection.",-1,0)
options_createFinali();