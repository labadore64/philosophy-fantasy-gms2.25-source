/// @description Insert description here
// You can write your code in this editor
cards = ds_list_create()

is_ai = false;
ai_script = -1;

card_array[0] = -1;
card_facedown[0] = false;
card_array_size = 0;
part_num = 0

size_limit = 1;

// the info to draw
draw_info = -1;

// which card is selected (may be ignored for stuff like deck, waiting)
menupos = 0;

enemy = false

menu_left = -1;
menu_right = -1;
menu_up = -1;
menu_down = -1;
menu_select = -1;
menu_cancel = -1;
menu_activate = -1;

menu_left_enabled = true;
menu_right_enabled = true;
menu_up_enabled = true;
menu_down_enabled = true;
menu_cancel_enabled = true;
menu_select_enabled = true;

menu_display_info = duel_hand_display_info

info_x_offset = 20;
info_y_offset = 495

info_surf = -1;
info_surf_update = false;
parent = noone;

source_name = "";