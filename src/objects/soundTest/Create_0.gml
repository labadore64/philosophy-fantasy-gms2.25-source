/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

width = 150
height = 80
centerx = 400
centery = 300

menu_draw = soundTest_draw
menu_left = soundTest_moveLeft
menu_right = soundTest_moveRight
menu_select = soundTest_select

ax_title = "Sound test"

menu_push(id)

menu_add_option("Sound Effects","",-1,-1)
menu_add_option("Music","",-1,-1)

sound_menupos = 0
song_menupos = 0
wrap = true
menupos = 0;
menu_count = 2