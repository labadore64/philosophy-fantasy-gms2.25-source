{
    "id": "20ea00d4-4fd7-4052-aa66-1a49baad6794",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "appearanceColorMap",
    "eventList": [
        {
            "id": "749dd6dd-6977-4279-b3bb-7b5f648ecaee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20ea00d4-4fd7-4052-aa66-1a49baad6794"
        },
        {
            "id": "0f0ded56-00b4-4e1c-bd57-92c742f398ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "20ea00d4-4fd7-4052-aa66-1a49baad6794"
        },
        {
            "id": "c8908fca-7054-4c9f-b4b8-6844f2e86d9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20ea00d4-4fd7-4052-aa66-1a49baad6794"
        },
        {
            "id": "722f66db-052b-438a-9b09-a8cab1c0a4c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "20ea00d4-4fd7-4052-aa66-1a49baad6794"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}