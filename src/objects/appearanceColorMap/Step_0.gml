/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
my_selected_counter += ScaleManager.timer_diff
my_selected = make_color_hsv(my_selected_counter % 255,255,255)
if(MouseHandler.clicked_countdown <= -1){
	if(MouseHandler.active && MouseHandler.enabled){
		appearanceColorMap_mouse()
	}
}
appearanceColorMap_number_input()

selected_color_component = make_color_hsv(0,0,255*(.5+sin(.1*ScaleManager.spr_counter)*.5))
menupos_old = menupos