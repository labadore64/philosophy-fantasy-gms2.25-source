/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
mouseHandler_clear();
mouseHandler_clear_menu()
var x_shift = x+20
var y_shift = y+150

// color changers
// value
mouse_script[0] =appearanceColorMap_mouseVal
mouse_script[1] =appearanceColorMap_mouseCol

mouseHandler_add(x_shift+15,y_shift+15,x_shift+menu_width-52,y_shift+30+15,-1,"Value Changer")

// hue/saturation
mouseHandler_add(x_shift+15,y_shift+55,x_shift+menu_width-52,y_shift+55+295,-1,"Hue and Saturation Changer")

var spacer = 30;

var x_hue = x+75
var y_hue = y+60
mouseHandler_add(x_hue,y_hue,x_hue+70,y_hue+spacer,-1,"Red")
mouseHandler_add(x_hue,y_hue+spacer,x_hue+70,y_hue+spacer+spacer,-1,"Green")
mouseHandler_add(x_hue,y_hue+spacer*2,x_hue+70,y_hue+spacer+spacer*2,-1,"Blue")

var x_hue = x+210
mouseHandler_add(x_hue,y_hue,x_hue+70,y_hue+spacer,-1,"Hue")
mouseHandler_add(x_hue,y_hue+spacer,x_hue+70,y_hue+spacer+spacer,-1,"Saturation")
mouseHandler_add(x_hue,y_hue+spacer*2,x_hue+70,y_hue+spacer+spacer*2,-1,"Value")