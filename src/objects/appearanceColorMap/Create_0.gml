/// @description Insert description here
// You can write your code in this editor
event_inherited();
parent_update = -1;


rgb_name[0] = "Red"
rgb_name[1] = "Gre"
rgb_name[2] = "Blu"

hsv_name[0] = "Hue"
hsv_name[1] = "Sat"
hsv_name[2] = "Val"

parent = ds_stack_top(menuControl.menu_stack)

menu_draw = appearanceColorMap_draw;
menu_select = appearanceColorMap_select
menu_cancel = appearanceColorMap_cancel

menu_left = appearanceColorMap_moveLeft
menu_right = appearanceColorMap_moveRight
menu_up = appearanceColorMap_moveUp
menu_down = appearanceColorMap_moveDown

menu_hold_left = appearanceColorMap_holdLeft
menu_hold_right = appearanceColorMap_holdRight
menu_hold_up = appearanceColorMap_holdUp
menu_hold_down = appearanceColorMap_holdDown

menu_release_down = appearanceColorMap_release
menu_release_left = appearanceColorMap_release
menu_release_right = appearanceColorMap_release
menu_release_up = appearanceColorMap_release

menu_width = 360
menu_height = 560
title = "Color Select"

menupos = 0;
keyboard_timer = -1

selected=false;
color_selected = false;
stringer = "";

my_selected_counter = 0
my_selected = make_color_hsv(my_selected_counter % 255,255,255)

menu_add_option("","",-1,-1)
menu_add_option("Red","",-1,-1)
menu_add_option("Green","",-1,-1)
menu_add_option("Blue","",-1,-1)
menu_add_option("Hue","",-1,-1)
menu_add_option("Saturation","",-1,-1)
menu_add_option("Value","",-1,-1)
menu_add_option("Value Slider","",-1,-1)
menu_add_option("Hue/Saturation Slider","",-1,-1)

with(appearanceSet){
	draw_menu = false	
}

wrap = false;

sound[0] = -1
sound[1] = 0;

sound_released = true
menupos_old = 0
mouse_selected = -1

selected_color_component = make_color_hsv(0,0,sin(.01*ScaleManager.spr_counter))