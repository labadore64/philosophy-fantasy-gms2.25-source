{
    "id": "190b186b-23a2-4590-9301-ebea2aad59dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationMessage",
    "eventList": [
        {
            "id": "8ceec805-a827-4eeb-88e3-90cfd3537bad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "190b186b-23a2-4590-9301-ebea2aad59dd"
        },
        {
            "id": "de0a864e-6141-4a39-9fcb-086e7a6b0a85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "190b186b-23a2-4590-9301-ebea2aad59dd"
        },
        {
            "id": "449c8711-50fa-40e4-b929-7100a6b990d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "190b186b-23a2-4590-9301-ebea2aad59dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "63585793-b876-4106-a034-4389c639caa9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}