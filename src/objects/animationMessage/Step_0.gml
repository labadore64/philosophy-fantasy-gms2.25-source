if(!trigger && !instance_exists(TextBox)){
	trigger = true;
	mouseHandler_clear()
	mouseHandler_add(0,0,800,600,duel_anim_message_continue,message)	
}
if(trigger){
	timer-=ScaleManager.timer_diff*FAST
	if(timer < -1){
		instance_destroy();	
	}

	if(x < 400){
		x+=velocity[0]*ScaleManager.timer_diff*FAST
		if(x >= 400){
			if(!vel_flip){
				x = 400;
				vel_flip = true;
			}
		}
	} else {
		if(!vel_flip){
			x = 400;
			vel_flip = true;
		}
		x+=velocity[1]*ScaleManager.timer_diff*FAST	
		image_alpha-=alpha_decrease;
	}

	with(Rumble){
		var val = 1/rumble_time;
		rumble_left-=val
		rumble_right+=val
	}

	check_input_select_pressed()
}