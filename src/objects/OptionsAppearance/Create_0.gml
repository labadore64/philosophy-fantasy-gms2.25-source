/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
// Inherit the parent event
options_createDefault();

// add options
ds_map_add(options_map,"High Contrast",global.high_contrast)
ds_map_add(options_map,"Animate Menus",global.menu_animate)
ds_map_add(options_map,"Particles",global.particles)
ds_map_add(options_map,"Show Health",global.display_hp)
ds_map_add(options_map,"Card Icon",global.card_icon)
ds_map_add(options_map,"Auto Text",global.text_auto);
ds_map_add(options_map,"Fullsize Text",global.text_full);

// for background
option_text[6,0] = "None"
option_text[6,1] = "Ravens"
option_text[6,2] = "Black Hole"
option_text[6,3] = "Hydrogen"
option_text[6,4] = "Dimension"

var inde = 0;

if(global.menu_index > -1){
	inde = string_replace_all(sprite_get_name(global.menu_index),"background_img_","");
	var sizer = array_length_2d(option_text,6);
	for(var i = 0; i < sizer; i++){
		if(string_replace_all(string_lower(option_text[6,i])," ","_") == inde){
			inde = i;
			break;
		}
	}
}

ds_map_add(options_map,"Menu Background",inde)

ds_map_add(options_map,"Cursor Direction",global.reverse_pointer)

// add menu options
option_number[0] = 0;
menu_add_option("General","General appearance settings.",-2,-1)
menu_add_option("Animate Menus", "Enable menu movement animations.",-1,0)
menu_add_option("High Contrast", "Enables high contrast mode.",-1,0)
menu_add_option("Particles", "Enable particle effects.",-1,0)
menu_add_option("Menu Background", "Select menu background.",-1,6)

menu_add_option("","",-2,-1)
option_number[1] = menu_count;
menu_add_option("Text","Modify text appearance.",-2,-1)

menu_add_option("Auto Text","Automatically advances text.",-1,0)
menu_add_option("Fullsize Text","Textbox occupies whole screen.",-1,0)

menu_add_option("","",-2,-1)
option_number[2] = menu_count;
menu_add_option("Sprites","Specific sprite display settings.",-2,-1)

menu_add_option("Card Icon", "Draw card type icons on card sprite.",-1,0)
menu_add_option("Show Health","Show exact amount of health on the screen.",-1,0)
menu_add_option("Cursor Direction","Which direction the cursor points.",-1,3)
options_createFinali();

// for background
option_text[6,0] = "None"
option_text[6,1] = "Ravens"
option_text[6,2] = "Black Hole"
option_text[6,3] = "Hydrogen"
option_text[6,4] = "Dimension"

for(var i = 0; i < menu_count; i++){
	if(menu_sprite[i] > -1 && option_select[i] > -1){
		option_visible_draw[i] = option_text[menu_sprite[i],option_select[i]];	
	} else {
		option_visible_draw[i] = "";	
	}
}