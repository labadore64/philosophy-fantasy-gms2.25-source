/// @description Insert description here
// You can write your code in this editor
event_inherited();

// deck
size_limit = 20

source_name = "Deck"

menu_activate = duel_deck_activate
menu_down = duel_deck_move_down
menu_up = duel_deck_move_up
menu_select = duel_deck_select

menu_display_info = duel_limbo_display_info