{
    "id": "013c94ea-4f22-49ff-a015-b9f6e10f3b15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationWindow",
    "eventList": [
        {
            "id": "de523b7f-f03e-4262-87ab-13ed4389a73c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "013c94ea-4f22-49ff-a015-b9f6e10f3b15"
        },
        {
            "id": "eaae88eb-131b-413b-9f15-cc0a2cd75f3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "013c94ea-4f22-49ff-a015-b9f6e10f3b15"
        },
        {
            "id": "a67409f8-eb6f-4181-8e0b-2fd25a38db4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "013c94ea-4f22-49ff-a015-b9f6e10f3b15"
        },
        {
            "id": "2ae4a3e5-5bc4-420f-95ed-946822009d72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "013c94ea-4f22-49ff-a015-b9f6e10f3b15"
        },
        {
            "id": "687f624f-f777-4d32-8861-da3cd6db8a83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "013c94ea-4f22-49ff-a015-b9f6e10f3b15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}