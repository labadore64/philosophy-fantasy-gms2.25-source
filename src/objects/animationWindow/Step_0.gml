/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(global.dont_record || !global.press_continue){
	if(!countdown_complete){
		countdown-= ScaleManager.timer_diff	
		if(countdown < 0){
			duel_specialCancel()
			countdown_complete = true
		}
	}
}