/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(resume_control){
	if(!duelController.regain_control){
		duelController.resume_control = true;
		duelController.resume_control_counter = DUEL_RESUME_CONTROL_TIME
	}
}

with(duelController){
	test_victory = true;
	event_user(0)
	//duelController_resume_control()
}