{
    "id": "a95413cf-da00-43b7-bc1f-df2c5cb3dfa7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ChapterController",
    "eventList": [
        {
            "id": "be8c96a7-d92d-4ed4-9a20-389c33188307",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a95413cf-da00-43b7-bc1f-df2c5cb3dfa7"
        },
        {
            "id": "dc1cc3a5-9a35-492e-bcc0-697e47ee9c2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a95413cf-da00-43b7-bc1f-df2c5cb3dfa7"
        },
        {
            "id": "c1940d83-acca-494e-8a34-a2a8df4e1746",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a95413cf-da00-43b7-bc1f-df2c5cb3dfa7"
        },
        {
            "id": "cf689e26-b51e-49a1-b966-bbea1863e298",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "a95413cf-da00-43b7-bc1f-df2c5cb3dfa7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}