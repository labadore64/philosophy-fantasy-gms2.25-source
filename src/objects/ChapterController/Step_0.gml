/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

variation_counter+=ScaleManager.timer_diff
var curr = variation_counter;
last_am = amm;
amm = sin(variation_counter*.015)
ammz = sin(variation_counter*.025)
var varzi = amm*.1+1 + ammz*.20
var varz2 = 1 - ammz*.05
var varzis = .5+amm*.5

if(amm > last_am && card.slappa){
	card.switching = !card.switching
	card.front = !card.front
	card.display_front = !card.display_front
	card.surface_update = true
	card.slappa = false
} else if(amm < last_am && !card.slappa){
	card.slappa = true
}

with(card){
	y = ystart-(cos(curr*.025)*.5+.5)*20
	if(switching){
		point_scale_x[0] = varzi;
		point_scale_x[1] = varz2
		point_scale_x[2] = varzi;
		point_scale_x[3] = varz2
		point_scale_y[0] = varz2
		point_scale_y[1] = varzi;
		point_scale_y[2] = varz2
		point_scale_y[3] = varzi;
	} else {
		point_scale_x[1] = varzi;
		point_scale_x[0] = varz2
		point_scale_x[3] = varzi;
		point_scale_x[2] = varz2
		point_scale_y[1] = varz2
		point_scale_y[0] = varzi;
		point_scale_y[3] = varz2
		point_scale_y[2] = varzi;
	}
	x_scale=varzis
	card_reset_sizespecial();
}

if(cancelled){
	if(!instance_exists(running_object)){
		instance_destroy();	
	}
}