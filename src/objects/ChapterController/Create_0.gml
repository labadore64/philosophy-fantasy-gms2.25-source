/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
chapter = obj_data.chapter
chapter_name = obj_data.chapter_title

spr_unload_card()
spr_load_card_front(card_from_name(obj_data.chapter_card))

cancelled = false
running_obj = noone

script_next = -1;

last_am = 0;
amm = 0;
ammz = 0
menu_draw = chapterController_draw

card = card_create_from_name(650,250,obj_data.chapter_card,.5,1)
viewcard_id = card_from_name(obj_data.chapter_card);
card.card_sprite_ref = obj_data.card_sprite[card_from_name(obj_data.chapter_card)]
with(card){
	card_update();	
}
card.switching = false;
card.slappa = false;
variation_counter = 0

variation = .33 + sin(variation_counter*.01)*.15

var varzi = variation
with(card){
	point_scale_x[0] = varzi 
	point_scale_x[1] = 1;
	point_scale_x[2] = varzi 
	point_scale_x[3] = 1;
	point_scale_y[0] = 1;
	point_scale_y[1] = varzi 
	point_scale_y[2] = 1;
	point_scale_y[3] =varzi 
	card_reset_sizespecial();
}
menu_push(id)

var s =instance_create(120,0,fadeIn)
s.time = 120
s.amount_per_cycle = 1/s.time;

menu_select = chapterController_close
menu_cancel = chapterController_close
menu_left = -1;
menu_right = -1;
menu_up = -1;
menu_down = -1;

soundPlaySong(-1,0)

mouseHandler_clear()
mouseHandler_add(0,0,800,600,chapterController_close,chapter + ": " + chapter_name + ".")