/// @description Insert description here
// You can write your code in this editor

if(hp_counter > -1){
	hp_counter-= ScaleManager.timer_diff*FAST;
	//hp_spacer_counter+= ScaleManager.timer_diff;
	if(hp_counter > -1){
		//hp_spacer_counter = hp_spacer_counter % hp_spacer;
		hp_current-= hp_diff*ScaleManager.timer_diff*FAST*hp_spacer;
		duelHealth_update();
	}
	if(sound > -1){
		audio_sound_pitch(sound, (hp_percent*.5)+.75);
	}
	
	if(hp_counter < -1 || hp_current < hp_target){
		hp_current = hp_target;
		hp_counter = -1
		hp_spacer = 1
		hp_spacer_counter = 0;
		if(sound > -1){
			audio_stop_sound(sound);
			sound = -1;
		}
		duelHealth_update();
	}
}