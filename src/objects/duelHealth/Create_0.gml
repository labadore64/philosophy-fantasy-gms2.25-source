/// @description Insert description here
// You can write your code in this editor
portrait_surf = -1;
portrait_chara = -1
portrait_sprite = spr_portrait_player;
surf = -1;
surf_updated = false;

draw_color = c_lime;

hp_max = 20;
hp_current = hp_max
hp_current_display = hp_current

sound = -1;

sound_fx = sfx_sound_HP_left
hp_diff = 0;
hp_percent = 1;

portrait_x = 0;
portrait_y = 0
portrait_custom = false

// used to create the custom portrait
for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
	portrait_property[i] = 0;
}

// used to create the custom portrait
for(var i = 0; i < TOTAL_CHARA_COLORS; i++){
	portrait_color[i] = c_white
}

name = global.character_name

channel_card = -1

hp_time = 15

hp_counter = -1
hp_spacer = 1
hp_spacer_counter = 0;
hp_target = hp_current;
parent = noone


duelHealth_update();