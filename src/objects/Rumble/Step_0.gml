/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();

if(active){
	
	rumble_time_counter-=ScaleManager.timer_diff;
	rumble_amount-= rumble_change*ScaleManager.timer_diff;
	
	if(rumble_amount > rumble_max){
		rumble_amount = rumble_max;	
	}
	
	
	if(rumble_time_counter <= 0 ||
		rumble_amount <= rumble_min){
		gamepad_set_vibration(global.gamepad, 0, 0);
		instance_destroy();	
	} else {
	
		gamepad_set_vibration(global.gamepad, rumble_amount*rumble_left*global.gamepadVibrationAmount, rumble_amount*rumble_right*global.gamepadVibrationAmount);
	
	}
}
