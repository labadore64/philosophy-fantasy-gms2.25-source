{
    "id": "cf482c98-1f2c-4452-99b1-12e19d732bf9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Rumble",
    "eventList": [
        {
            "id": "78b32938-d6d1-4aee-8ae2-da21a2dcbf11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf482c98-1f2c-4452-99b1-12e19d732bf9"
        },
        {
            "id": "da811b8e-f5ca-4f0f-ab55-11e9153542b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cf482c98-1f2c-4452-99b1-12e19d732bf9"
        },
        {
            "id": "411b792c-824d-45ad-b828-9cbdd7540a82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cf482c98-1f2c-4452-99b1-12e19d732bf9"
        },
        {
            "id": "5742a275-e760-4b44-a2d6-f1fbef934e47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "cf482c98-1f2c-4452-99b1-12e19d732bf9"
        },
        {
            "id": "f91ab9de-e877-47e2-8af7-e5061e3d5b29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "cf482c98-1f2c-4452-99b1-12e19d732bf9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}