/// @description Insert description here
// You can write your code in this editor

#macro DUEL_SOUND_POSITION_TOP -150
#macro DUEL_SOUND_POSITION_BOTTOM 800

#macro DUEL_RESUME_CONTROL_TIME 30

global.skip_this_coin = false
mouse_card_updated= false;
game_started = false
game_start_animation = false;
game_complete = false;
ai_mode = false;
ai_side = 0;
ai_resume_point = 0
ai_resume_complete = false

options_menupos = 0

resume_control_counter = -1;
resume_control = false;

extra_card_list =-1

event_inherited();

#macro DUEL_PLAYER_YOU 0
#macro DUEL_PLAYER_ENEMY 1

#macro DUEL_PHASE_DRAW 0
#macro DUEL_PHASE_STANDBY 1
#macro DUEL_PHASE_LIMBO_RETURN 2
#macro DUEL_PHASE_MAIN 3
#macro DUEL_PHASE_BATTLE 4
#macro DUEL_PHASE_RETURN 5
#macro DUEL_PHASE_END 6

#macro FAST duelController.fast


var repeater = 13*current_second + 17*current_hour + 19*current_minute + floor(0.01*31*current_time);

duel_money = 0;
duel_item = 0;

// if no enemy is loaded, load the AI enemy
if(global.duel_player[1] == ""){
	instance_create(0,0,duelSinglePlayerLoad);
}

// decode these and place their data in appropriate places
var map1 = json_decode(global.duel_player[0])
var map2 = json_decode(global.duel_player[1])

if(ds_map_exists(map2,"money")){
	duel_money = map2[? "money"];
}

if(ds_map_exists(map2,"item")){
	duel_item = itemNameToID(map2[? "item"]);
}

var seed = 0;
if(global.duel_random == -1){
	coolrandom_set_seed(irandom(250))
	seed = coolrandom_get_seed();
} else {
	coolrandom_set_seed(global.duel_random)
	seed = coolrandom_get_seed();
}

obj_data.savescum = true
save_game()

subscript_queue = -1;

do_return_overlay = false;

mouse_card[0,0] = 0
mouse_card[0,1] = 0
mouse_card[0,2] = 0
mouse_card[0,3] = 0
mouse_card_element[0] = -1
mouse_card_menupos[0] = 0

mouse_card_count = 0;
mouse_card_selected = -1
mouse_card_selected_last = -1


tooltip_show = true;
context_menu = noone
do_context_menu = false
context_menu_x = 0
context_menu_y = 0
context_hover = false;
context_menu_select = 0
do_the_context_menu = false;

phase_name[0] = "Draw!"
phase_name[1] = ""
phase_name[2] = ""
phase_name[3] = ""
phase_name[4] = "Dialog!"
phase_name[5] = "Return!"
phase_name[6] = ""
draw_phase_change = false;
phase_current = -1
active_player = global.first_player;
turns = 0;

temp_phase_current = phase_current
temp_turns = turns

is_rotating = false
rotate_start = 0;
rotate_start_x = 0;
rotate_start_y = 0
rotate_start_angle = 0;

card_surf_x = 75-50
card_surf_y = -180

card_surf_w = 495+100
card_surf_h = 1000

card_surf = -1
card_surface_update = true;

fast = 1.5
if(global.fastmode){
	fast = 2
}

field[0] = instance_create(0,300,duelField)
duel_hp[0] = instance_create(20,5,duelHealth)
duel_hp[0].parent = field[0]
duel_hp[0].sound_fx = sfx_sound_HP_left

// get your deck
var len = ds_list_size(obj_data.current_deck);

/*
for(var i = 0; i < len; i++){
	duel_deck_addCard(field[0].deck,obj_data.current_deck[|i]);	
	spr_load_card_front(obj_data.current_deck[|i]);
}
*/

with(field[0].deck){
	duelField_randomize();	
}

field[1] = instance_create(660,350,duelField)
field[1].image_angle = 180
field[1].enemy = true;
for(var i = 0; i < 6; i++){
	field[1].part[i].enemy = true;	
}

duelField_reverse_control(field[1])

field[1].ai_field.enemy = true
for(var i = 0; i < field[1].ai_field.ai_part_count; i++){
	field[1].ai_field.part[i].enemy = true
}

fave = -1

demo_get_duelists()

duel_hp[1] = instance_create(500,5,duelHealth)

//duel_hp[1].hp_current = 1;

duel_hp[1].parent = field[1]
duel_hp[1].sound_fx = sfx_sound_HP_right

duel_loadHealth(duel_hp[0],field[0],map1)
duel_loadHealth(duel_hp[1],field[1],map2)

camera = instance_create(325,450,duelCamera);
background = instance_create(0,0,duelBackground);

cursor = instance_create(0,0,duelCursor)
panning = true;

field_alpha = 1;

mouse_mode = false;
disable_mouse_mode = true
mouse_mode_w = 200
mouse_cleared = false;

menu_push(id)

// controls
input_active = true;

global.mainMenupos = 0;

menu_draw = duel_draw;

menu_step = menuParent_step_duelController

menu_cancel = duelController_cancel
if(!global.dont_record){
	menu_select = duelController_select
	menu_left = duelController_left
	menu_right = duelController_right
	menu_down = duelController_down
	menu_up = duelController_up
} else {
	menu_select = -1
	menu_left = -1
	menu_right = -1
	menu_down = -1
	menu_up = -1
}
menu_help_action = duelController_open_help

selected_object = field[0];
last_selected_object = noone
last_selected_thingie = noone
anim_countdown = -1;

pointer_x = 0;
pointer_y = 0;

//duelField_selectObject(field[0],DUEL_FIELD_ACTIVE);

soundPlaySong(-1,0)

animation_stack = ds_stack_create();
animation_stack_run = noone
animation_stack_count = 0;
animation_obj_count = 0
animation_stack_bottom = noone
resumeAnim = false

running_obj = noone;
card_list = -1;
card_list_name = "";
fade = false;

draw_pointer = false


regain_control = false;

var modi = 0;
			
if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
	modi = -135
}

with(duelCamera){
	scale = 5;
	x = 350;
	y = 290+modi
}
if(obj_data.free_duel){
	duel_music = sfx_music_freebattle
} else {
	duel_music = obj_data.duel_music
}

do_battle = -1
do_coinflip = -1;

test_victory = false;
complete = false;

room_timer = -1;

duelController_start_game();

instance_create(0,0,fadeIn)

tts_stop();

menu_help = "duel"

centerx = 330
centery = 320

// used for rumble
card_rumbleset_x =0;
card_rumbleset_y =0;

click_countdown = -1

with(duelController){
	if(selected_object > 0){
		last_selected_object = selected_object
		last_selected_thingie = selected_object.selected_object	
	}
}


do_screenshot = false;
screenshot_delay = -1

pan_left = false
pan_right = false
pan_up = false
pan_down = false
pan_hold_counter = 0
pan_hold_frames = 7
can_tts = true; // whether or not you can read TTS for duel field
speak_tts = true;

instance_create(0,0,fadeIn);

init_mouse_counter = 10
can_mouse_counter = false;
mouse_counter_triggered = false;

phase_transition = noone;

code_loaded[0] = "";
code_length = 0;

code_pointer = 0;
code_start = 0;

code_arguments[0] = "";
code_argument_size = 0;

code_query[0] = "";
code_query_result[0] = 0;
code_query_size = 0;

code_values = ds_map_create();

card_list_destroy = false;
select_card = false;
card_list_dest = noone
card_list_source = noone;
card_list_cond = "";

list_show = -4

code_select_location[0] = noone;
code_select_query = "";
code_select_active = false;
code_select_location_size = 0;

subscript_queue = -1
subscript_queue_pos = 0;

code_this = ds_stack_create();
code_card_id = ds_stack_create();
code_is_ai = false;

card_status = ds_map_create();

code_default_values()

// temp for loading status data
temp_status_name = -1
temp_status_arg = -1
temp_status_card_loc = -1
temp_status_card_loc_index = -1
temp_negated = false;
temp_loaded_block = "";
temp_status_id = "";

in_battle = false;

var teststring = getStatusString("test",2,field[0].deck,0)
loadStatusString(teststring);


// input queue.
// if there are inputs loaded, it should keep running until its done
// seed for random so that it plays back the same way
// thinking about just adding a random number table
input_seed = seed;

// this queue stores inputs that are to be executed.
input_queue = ds_list_create();

// when completed, the input object will kill itself
input_obj = noone;

input_countdown = 0

// whether or not to write inputs to a file
input_write = false;
// file to write to
var name = "temp_" + duel_hp[0].name + "_vs_" + duel_hp[1].name
var date = string(current_year) + string(current_month) + string(current_day) + "_" + string(current_hour) + string(current_minute) + string(current_second)
input_write_file = "replay\\"+name + "_" + date + ".txt";

// create the header
if(!global.dont_record){
	var file = file_text_open_write(input_write_file)

	// write seed
	file_text_write_string(file,string(input_seed))
	file_text_writeln(file)
	// write first
	file_text_write_string(file,string(active_player))
	file_text_writeln(file)
	// write you
	file_text_write_string(file,global.duel_player[0])
	file_text_writeln(file)
	// write enemy
	file_text_write_string(file,global.duel_player[1])
	file_text_writeln(file)
	// write time
	file_text_write_string(file,string(current_year) + "-" + string(current_month) + "-" + string(current_day) + " " + string(current_hour) + ":" + string(current_minute) + ":" + string(current_second))
	file_text_writeln(file)

	file_text_write_string(file,"==========START REPLAY==========")
	file_text_writeln(file)
	file_text_close(file);
}

ds_map_destroy(map1);
ds_map_destroy(map2);

// populate actions

var size = array_length_1d(global.duel_actions);
with(duelController){
	if(global.duel_actions[0] != ""){
		for(var i = 0; i < size; i++){
			ds_list_add(input_queue,global.duel_actions[i]);
		}
	}
}

battle_complete = false
last_this = ""

shuffle_count = 0

do_phase = false;
do_phase_timer = -1;

do_end = false;
do_end_timer = -1;

pop_base = false

do_enemy_end = false;
do_enemy_end_timer = -1;

// stuff

#macro POP_MENU_TOP_LENGTH -135
#macro POP_MENU_RATE 17

pop_menu_active = false;
pop_menu_moving = 0
pop_menu_y = POP_MENU_TOP_LENGTH

// extra buttons
// basically will make a mouse button for each
// maybe have it movable idk
side_button_x = 225
side_button_y = 165
side_button_spacer = 60

side_button_name[0] = "Proceed"
side_sprite[0] = spr_menu_confirm;
side_button_script[0] = duelController_next_phase

side_button_name[1] = "Next Turn"
side_sprite[1] = spr_menu_next;
side_button_script[1] = duelController_do_nextturn

side_button_name[2] = "Forfeit"
side_sprite[2] = spr_menu_forfeit;
side_button_script[2] = duel_do_forfeit

side_button_name[3] = "Screenshot"
side_sprite[3] = spr_menu_screenshot;
side_button_script[3] = duelController_screenshot

side_button_name[4] = "Options"
side_sprite[4] = spr_menu_options;
side_button_script[4] = duelController_open_options

side_button_name[5] = "Close"
side_sprite[5] = spr_menu_cancel;
side_button_script[5] = duelController_pop_menu

side_button_total = array_length_1d(side_button_name)

side_button_right = true;

for(var i = 0; i < side_button_total; i++){

	side_pressed[i] = false; // whether the side button is pressed
	side_hover[i] = 1; // the scale of the hovering animation	
}

pop_menu_pos = 0;

side_sprite_yscale = 1;

help_menu_pressed = false
help_menu_hover = 1;

move_menu_pressed = false
move_menu_hover = 1;

mouse_menu_script = duelController_initialize_mouse

navigation_menu = noone;

nav_pos[0] = -100000
nav_pos[1] = -100000
nav_pos[2] = -100000
nav_pos[3] = -100000

start_navx = 200;
start_navy = 180;