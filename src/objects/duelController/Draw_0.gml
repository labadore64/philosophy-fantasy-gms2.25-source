/// @description Insert description here
// You can write your code in this editor
if(global.drawing_enabled){
	
	if(!surface_exists(card_surf)){
		card_surf = surface_create(495+100,1000);	
		card_surface_update = true;
	}

	if(card_surface_update){
	
		draw_set_alpha(1)
		for(var i = 0; i < 2; i++){
			duelField_draw(field[i]);	
		}

		// draw the field
		
		surface_set_target(card_surf)

		draw_clear_alpha(c_black,0)

		gpu_set_colorwriteenable(true,true,true,true)
		
		draw_surface_ext(field[0].surf,field[0].x-80+50,field[0].y+180,1,1,0,c_white,field[0].image_alpha)	
		draw_surface_ext(field[1].surf,field[1].x-80+50,field[1].y+180,1,1,180,c_white,field[1].image_alpha)	
	
		with(drawMoveCard){
			cardSource_draw_card(x-25,y+180,card_index)	
		}

		with(drawSwapCard){
			for(var i = 0; i < 2; i++){
				cardSource_draw_card(xx[i]-25,yy[i]+180,card_index[i])	
			}
		}

		surface_reset_target();
		card_surface_update = false
	}
	gpu_set_colorwriteenable(true,true,true,true)
	
	draw_surface_ext(card_surf,
					card_surf_x+(card_surf_h*sin_val + card_surf_w*cos1_val)/2 ,//+ card_surf_x*cos_val - card_surf_y*sin_val,
					card_surf_y+(card_surf_h*cos1_val - card_surf_w*sin_val)/2 ,//+ card_surf_x*sin_val + card_surf_y*cos_val,
					field[0].image_xscale,field[0].image_yscale,
					-image_angle,c_white,1)	

	/*
	}
	
	
	nav_pos[0]
	draw_set_alpha(.5)
	draw_set_color(c_lime)
	draw_rectangle(0,DUEL_SOUND_POSITION_TOP,800,DUEL_SOUND_POSITION_BOTTOM,false)
	
	with(duelCursor){
	draw_set_color(c_green)
	draw_rectangle(test_area[0],test_area[1],
					test_area[2],test_area[3],
						false)
	}
	
	draw_set_color(c_blue)
	for(var i = 0; i < mouse_card_count; i++){

		draw_rectangle(mouse_card[i,0],mouse_card[i,1],
							mouse_card[i,2],mouse_card[i,3],false)
	}
	
	draw_set_alpha(1)
	*/
}