/// @description Insert description here
// You can write your code in this editor
event_inherited();

if(ds_exists(code_values,ds_type_map)){
	ds_map_destroy(code_values);	
}

if(ds_exists(animation_stack,ds_type_stack)){
	ds_stack_destroy(animation_stack);	
}

if(ds_exists(code_this ,ds_type_stack)){
	ds_stack_destroy(code_this );	
}

if(ds_exists(code_card_id ,ds_type_stack)){
	ds_stack_destroy(code_card_id);	
}

if(ds_exists(card_status, ds_type_map)){
	ds_map_destroy(card_status);	
}

if(ds_exists(input_queue,ds_type_list)){
	ds_list_destroy(input_queue);	
}

reset_card_used_this_duel();

with(duelBackground){
	instance_destroy();	
}
with(duelField){
	instance_destroy();	
}
with(duelCamera){
	instance_destroy();	
}
with(duelHealth){
	instance_destroy();	
}
with(duelCardSource){
	instance_destroy();	
}

if(surface_exists(card_surf)){
	surface_free(card_surf);	
}

window_set_cursor(cr_default);

// replay shit
if(!global.dont_record){
	if(game_complete){
		if(global.replay == 0){
			// delete the replay file
			file_delete(input_write_file);
		} else if (global.replay == 2){
			// save the replay file
			file_copy(input_write_file,string_replace_all(input_write_file,"temp_",""));
	
			// delete the replay file
			file_delete(input_write_file);
		} else {
			// save filename so you can interact with it later
			global.replay_temp = input_write_file	
		}
	}
}

global.dont_record = false;
