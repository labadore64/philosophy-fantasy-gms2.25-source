/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event

#macro INPUT_COUNTDOWN_TIME 5

// do fast
context_hover = false;
// first update context menu position and state
with(context_menu){
	var xx = (window_mouse_get_x()-global.display_x)/global.scale_factor
	var yy = (window_mouse_get_y()-global.display_y)/global.scale_factor
	// check if position is touching mouse
	for(var i = 0; i < menu_count; i++){
		if(point_in_rectangle(xx,yy,
							x+option_x_offset,-5+y+option_y_offset+option_spacer*(i),
							x+menu_width-option_x_offset,-5+y+option_y_offset+option_spacer*(i+1)) > 0){
			menupos = i
			duelController.context_hover = true
			break;
		}
	}
}

if(game_started && !in_battle && !ds_list_empty(input_queue) && !instance_exists(animationQueue) && !instance_exists(animationPhaseTransition)){
	//if(instance_exists(animation_stack_run)){
		duelController_inputStep()
	//}
} else if(do_phase){
	do_phase_timer-= ScaleManager.timer_diff*FAST
	if(do_phase_timer < -1){
		do_phase = false;
		duelController_phase_do();
	}
} else if(resume_control){
	resume_control_counter-= ScaleManager.timer_diff
	if(resume_control_counter < -1){
		duelController_resume_control()
		resume_control_counter = -1;
		resume_control = false;
	} 
} else {
	if(!resume_control){
		duelController_step();
	}
}

if(battle_complete){
	if(!game_complete){
		if(!instance_exists(animationQueue)){
			battle_complete = false	
			duel_animation_next_phase();
		}
	}
}

sin_val = sin(degtorad(image_angle))
cos_val = cos(degtorad(image_angle))
cos1_val = (1 - cos_val)

// popup value
if(pop_menu_moving != 0){
	
	// receeding
	if(pop_menu_moving == -1){
		pop_menu_y -=POP_MENU_RATE;
		if(pop_menu_y <= POP_MENU_TOP_LENGTH){
			pop_menu_y = POP_MENU_TOP_LENGTH;
			pop_menu_moving = 0;
			pop_menu_active = false;
		}
	// popping out
	} else if (pop_menu_moving == 1){
		pop_menu_y +=POP_MENU_RATE;
		if(pop_menu_y >= 0){
			pop_menu_y = 0;
			pop_menu_moving = 0;
			pop_menu_active = true;
			duelController_pop_menu_open();
		}
	}
}

if(pop_menu_pos > 0){
	var pop_pos = pop_menu_pos;
	with(MouseHandler){
		var mousex = mousepos_x
		var mousey = mousepos_y
		
		for(var i = pop_pos; i < mouse_count; i++){
			if(mousex >= mouse_points_trans[i,0] && 
				mousex <= mouse_points_trans[i,2] &&
				mousey >= mouse_points_trans[i,1] && 
				mousey <= mouse_points_trans[i,3]){
					duelController.side_hover[i-pop_pos] = 1.1; // the scale of the hovering animation	
				} else {
					duelController.side_hover[i-pop_pos] = 1; // the scale of the hovering animation		
				}
		}
	}
}