{
    "id": "0d8d1702-5501-4483-926d-836dcdd3732b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelController",
    "eventList": [
        {
            "id": "335eb0b1-7632-43ac-a74d-7f48feb235df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "57f449e0-d8cc-42cd-8dcd-bfd94a39e915",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "c9d3fdc9-02e8-4829-8f06-b6e795a22226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "a60b3370-394d-4c82-b27c-c56f9487328b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "e8c21315-c3e6-4ced-9bf5-2ff4c40f3a42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "70912ad7-768c-4fec-80d2-c6eedd8bb35f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "18209c9e-865e-435e-92da-45ab3d729cbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        },
        {
            "id": "74105ad3-a853-4cf5-9a6a-bb04cc00c60b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "0d8d1702-5501-4483-926d-836dcdd3732b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}