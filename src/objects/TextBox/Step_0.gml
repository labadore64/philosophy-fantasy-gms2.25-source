/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();
// Inherit the parent event
event_inherited();

if(!keypress_this_frame){
	mouseHandler_process()	
}

if(objGenericCanDo()){
	
	if(global.text_auto){
		if(auto_countdown <= -1){
			var countdown = false
	
			with(display_object){
				if(cutoff == string_lenger){
					countdown = true;
				}
			}
	
			if(countdown){
			
				auto_countdown = TEXTBOX_AUTO_COUNTDOWN;	
			}
		}
	
	}
}

if(auto_countdown > -1){
	auto_countdown -= ScaleManager.timer_diff;
	if(auto_countdown <= -1){
		auto_countdown = -1;
		textboxSelect();
	}
}

if(global.mouse_active){
	if(mouse_check_button_released(mb_left)){
		if(cancel_pressed){
			script_execute(menu_cancel);	
			cancel_pressed = false
		}
		if(help_pressed){
			help_menu_do()
			help_pressed = false
		}
	}
}