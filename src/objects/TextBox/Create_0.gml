//control
#macro TEXTBOX_AUTO_COUNTDOWN 120

name = ""
force_draw_bg = false;

do_not_draw_me = false;

animation_complete = false;
image_speed = 0;
update_image_only_active = true;
drawOtherMenus = true;
objGenericCreate();
initTimingAlarm();
menupos = 0; //represents the position of the menu
option_top = 0; //represents the top displayed item in the menu
menu_count = 1; //represents the full size of the menu
wrap = true; //whether or not if you get to the end of the list, if it wraps around.
skip = 10; //how many points in the list to skip if you're skipping through the list
debug = global.debugMenu; //if in debug mode
draw_bg = true; //whether to draw transparent bg
menu_cancel = textboxCancel;

//text
menu_name = ds_list_create(); //name of all menu items
menu_description = ds_list_create(); //descriptions of all menu items
menu_script = ds_list_create(); //scripts for all menu items

title = "Title"; //title of the menu
subtitle = "Subtitle"; //subtitle of the menu

//other scripts

menu_draw = -1

//keybind scripts

cancelled = false;

//drawing variables
x = 400;
y = 300;

draw_centered = true; //Is the origin in the center of the box, or on the top left? 
draw_width = 300; //width of box in pixels
draw_height = 200; //height of box in pixels

gradient_height = 80; //height of the title
gradient_color1 = c_black; //gradient colour for the title left
gradient_color2 = c_white; //gradient colour for the title right

title_color = c_white; //text color for the title
subtitle_color = c_white; //text color for the subtitle
text_color = c_white

titleFontSize = 4; //title font size
subtitleFontSize = 2; //subtitle font size
text_size = 2; //size of option text

drawTitle = true; //whether or not to draw title
drawSubtitle = true; //whether or not to draw subtitle
drawInfoText = true; //whether or not to draw info text
drawGradient = true; //whether or not to draw gradient

title_x_offset = 10; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 20 //xoffset for subtitle
subtitle_y_offset = 60; //yoffset for subtitle
option_x_offset = 10 //xoffset for options
option_y_offset = 100; //yoffset for options

option_space = 40; //how many pixels between each option
select_alpha = .25; //alpha of current selection
select_color = c_white; //selection color
select_percent = 1; //how much of the menu does the selection graphic occupy

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = c_white; //color of the title of the info box
info_subtitle_color = c_white; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

//following are specifically for the pop up animation
animation_start = 4 //animation length
animation_len = animation_start; //animation counter
anim_width = 0; //animation width size
anim_height = 0; //animation height size
anim_grad = 0; //animation gradient size

// if not an empty string, opens up the help
menu_help = "";

//following represent the corners of the box
x1 = 0;
x2 = 0;
y1 = 0;
y2 = 0;

//following represents sound effects for the box
sound_move = sfx_sound_menu_default;
sound_cancel = sfx_sound_menu_cancel
sound_select = sfx_sound_menu_default;

//if in debug mode, add these objects to allow stretching the box
stretch[0] = noone; //top left corner
stretch[1] = noone; //bottom right corner
stretch[2] = noone; //gradient height


newalarm[2] = 1;
newalarm[3] = 1;
active = false;

gradient1_original = c_black
gradient2_original = c_black

destroy_time = 1;

destroy_script = -1;

clean_script = -1;

ignore_clean_script = false;

// the string to use to read the title
tts_title_string = "";
infokeys_loaded = false;

cancel_button_enabled = true; // does this menu use the cancel button
cancel_button_x = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_y = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_width = 40; // width of the cancel button
cancel_button_height = 40; // height of the cancel button
cancel_sprite = spr_menu_cancel; // the cancel sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
cancel_draw_border = false; // whether to draw a border around the cancel sprite
cancel_pressed = false; // whether the cancel button is pressed
cancel_hover = 1; // the scale of the hovering animation

help_button_enabled = false; // does this menu use the help button
help_button_x = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_y = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_width = 40; // width of the help button
help_button_height = 40; // height of the help button
help_sprite = spr_menu_help; // the help sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
help_draw_border = false; // whether to draw a border around the help sprite
help_pressed = false; // whether the help button is pressed
help_hover = 1; // the scale of the hovering animation

right_click_move = false; // does this menu move with the right click button
right_clicked = false; // are you pressing down the button
left_dragging = false; // are you dragging with the left button
right_clicked_x = 0; // position of x when clicked
right_clicked_y = 0; // position of y when clicked
right_clicked_mx = 0; // position of mousex when clicked
right_clicked_my = 0; // position of mousey when clicked

mouse_menu_script = -1;
render_menu = true;


// DO TEXTBOX SHIT HERE

textboxCreate();
show_chara_name = false;

draw_map = ds_map_create();
draw_array[0] = noone;
draw_count = 0;

menu_select = textboxSelect
parent_lock = noone
braille_thing = false;

auto_countdown = -1;

if(room != BattleRoom){
	menuParent_menu_default_button();

	cancel_button_x -= 10
	cancel_button_y += 10
}