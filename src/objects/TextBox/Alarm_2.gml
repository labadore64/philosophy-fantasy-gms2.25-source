/// @description Insert description here
// You can write your code in this editor

textboxGetVoice();

if(string_pos(".ini",filename_id) == 0){
	textboxStringFromFile(filename_id + text_id + ".ini")
} else {
	textboxStringFromFile(filename_id)
}
if(!destroyed && display_object < 0){
	active = true;
	if(!global.text_sound && global.menu_sounds){
		soundfxPlay(sfx_sound_textbox)	
	}
	textboxSetCurrentText();
	if(tts_speak){
		if(name != ""){
			tts_say(name + " says: " + current_text);
		} else {
			tts_say("You say: " + current_text);	
		}
	}
	if(delay_display = 0){
		if(name != ""){
			show_chara_name = true;	
		}
		
		display_object = textboxDialogCreate(text,portrait,x_align)
		display_object.lineEnd = char_per_line
		display_object.text_sound1 = text_sound1;
		display_object.text_sound2 = text_sound2;
		display_object.text_sound3 = text_sound3;
		display_object.text_sound4 = text_sound4;
		display_object.text_mod_sound = text_mod_sound;
		display_object.pitch_base = pitch_base;
		textboxExecuteScript();
	
		mouseHandler_clear()

		// update this later to update the text...
		if(room == BattleRoom){
			mouseHandler_add(0,0,800,600,textboxMouse,"")
		} else {
			if(global.text_full){
				mouseHandler_add(0,0,500,600,textboxMouse,"")
			} else {
				mouseHandler_add(0,400,800,600,textboxMouse,"")
			}
			event_user(2);
		}
		
		

	} else {
		newalarm[3] = delay_display;	
	}

	if(!is_monster){
		wait = false	
	}
}