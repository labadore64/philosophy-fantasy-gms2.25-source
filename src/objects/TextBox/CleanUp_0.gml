/// @description Insert description here
// You can write your code in this editor

visible = false;
if(!ignore_clean_script){
	if(clean_script != -1){
		script_execute(clean_script);	
	}
}
ds_list_destroy(menu_name);
ds_list_destroy(menu_description);
ds_list_destroy(menu_script);

tts_stop()

// put textbox shit here

// executes any scripts in the text box
if(ds_exists(scripts,ds_type_map)){
	if(!ds_map_empty(scripts)){
		
		var size, i;
		size = ds_list_size(text);
		for (i = 0; i < size; i++;)
		{
			//if the scriptval is positive
			var scriptval = scripts[? i];
			if(!is_undefined(scriptval)){
				scriptval = real(scriptval);
			
				if(script_exists(scriptval)){
					script_execute(scriptval,arguments);	
				}
			}
		
			//if the scriptval is negative
			var scriptval = scripts[? (i-size)];
			if(!is_undefined(scriptval)){
				scriptval = real(scriptval);
			
				if(script_exists(scriptval)){
					script_execute(scriptval,arguments);	
				}
			}
	
		}	
		
	}
}

if(event_id != ""){
	with(parent){
		instance_destroy();	
	}
}

with(parent_lock){
	lock = false;
	textbox = noone;
}

with(menuControl){
	with(active_menu){
		if(menu_mouse_activate != -1){
			script_execute(menu_mouse_activate)	
			event_user(0)
		}
	}	
}

textboxClean();

if(ds_exists(draw_map,ds_type_map)){
	ds_map_destroy(draw_map);
}
