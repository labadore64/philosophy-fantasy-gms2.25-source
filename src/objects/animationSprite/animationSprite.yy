{
    "id": "32f3925f-4e62-4712-8475-8a149dd2aab6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationSprite",
    "eventList": [
        {
            "id": "b71bae01-ca89-4398-bb88-c60125d05bb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "32f3925f-4e62-4712-8475-8a149dd2aab6"
        },
        {
            "id": "cb433863-9d48-41fa-a145-b4ced94c6717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32f3925f-4e62-4712-8475-8a149dd2aab6"
        },
        {
            "id": "43cb4a4a-5e87-4738-bc4e-8b3bb554fbe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32f3925f-4e62-4712-8475-8a149dd2aab6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}