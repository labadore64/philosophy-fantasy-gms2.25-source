{
    "id": "052fb3b0-76e0-425c-aa7e-a1c15bd6f36d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "fadeIn",
    "eventList": [
        {
            "id": "72d1ebc1-0830-4b9f-bc8b-7c73c67f0990",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "052fb3b0-76e0-425c-aa7e-a1c15bd6f36d"
        },
        {
            "id": "2600b9b1-0b91-4db7-88bd-aff4957229c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "052fb3b0-76e0-425c-aa7e-a1c15bd6f36d"
        },
        {
            "id": "da61785f-e660-492a-9c5d-f05793e08bb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "052fb3b0-76e0-425c-aa7e-a1c15bd6f36d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4e6890b2-451e-4077-985c-c8e1a0e56464",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}