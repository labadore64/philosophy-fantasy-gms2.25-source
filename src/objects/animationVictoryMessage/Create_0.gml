/// @description Insert description here
// You can write your code in this editor

cancel_button_enabled = false; // does this menu use the cancel button
cancel_button_x = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_y = MENU_PARENT_CANCEL_DEFAULT_POS // default cancel position. Relative to x/y. if not set will automatically
cancel_button_width = 40; // width of the cancel button
cancel_button_height = 40; // height of the cancel button
cancel_sprite = spr_menu_cancel; // the cancel sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
cancel_draw_border = false; // whether to draw a border around the cancel sprite
cancel_pressed = false; // whether the cancel button is pressed

help_button_enabled = false; // does this menu use the help button
help_button_x = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_y = MENU_PARENT_CANCEL_DEFAULT_POS  // default help position. Relative to x/y. if not set will automatically
help_button_width = 40; // width of the help button
help_button_height = 40; // height of the help button
help_sprite = spr_menu_help; // the help sprite. if -1 it isn't drawn. note it should be centered and cannot be scaled.
help_draw_border = false; // whether to draw a border around the help sprite
help_pressed = false; // whether the help button is pressed

right_click_move = false; // does this menu move with the right click button
right_clicked = false; // are you pressing down the button
left_dragging = false; // are you dragging with the left button
right_clicked_x = 0; // position of x when clicked
right_clicked_y = 0; // position of y when clicked
right_clicked_mx = 0; // position of mousex when clicked
right_clicked_my = 0; // position of mousey when clicked


menu_up = -1;
menu_down = -1;
menu_left = -1;
menu_right = -1;
menu_select = -1;
menu_select_hold = -1;
menu_cancel = -1;
menu_help = "";
menu_help_action = - 1;


timer = 120

y = 300;
alpha_decrease = .01

velocity[0] = .35;
velocity[1] = .025
vel_flip = false;
image_alpha = 1;

message = "";

sizer = 1;
wait_to_kill = false
menu_select = duel_anim_message_continue
double_select=true

RUMBLE_CARD_REVEAL