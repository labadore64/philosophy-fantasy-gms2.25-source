/// @description Insert description here
// You can write your code in this editor
event_inherited()

if(completed){
	if(!instance_exists(fade_obj)){
		instance_destroy();
	}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff
shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;


repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(start_coin_flip){
	if(coin_flip_animate){
		if(!trigger_sound){
			trigger_sound = true;
			soundfxPlay(sfx_sound_coin_flip);
		}
		index += ScaleManager.timer_diff;

		if(index > spr_time){
			coin_flip_animate = false;	
		}
	} else {
		if(!finish_countdown_start){
			if(countdown > -1){
				countdown-=ScaleManager.timer_diff;
			} else {
				// finish countdown	
				if(coin_transition_x > 0){
					coin_transition_x -= coin_transition_rate;

					if(coin_transition_x <= 0){
						if(!done){
							if(global.dont_record){
								do_steppy = true;
							} else {
								coin_transition_x = 0;	
								soundfxPlay(sfx_sound_coin_result)
								tts_say(coin_string)
								finish_countdown_start = true;
								menu_select = coinFlip_duel_done;
								mouseHandler_clear();
								mouseHandler_add(0,0,800,600,coinFlip_duel_done,"")
								done = true
							}
						}
					}
				}
			}
		}
	}
}

if(finish_countdown_start){
	if(finish_countdown > -1){
		finish_countdown -= ScaleManager.timer_diff	* FAST
	}
}

if(do_steppy){
	if(finish_countdown < -1){
		coinFlip_duel_done();	
	}
}