/// @description Insert description here
// You can write your code in this editor
event_inherited();
titleScreen_initParticle();
did_tts = false;

//soundfxPlay(sfx_sound_coin_start)
coin_flip_animate = true
index = 0;
spr_time = 60

countdown = 10;

//animate coin option
//randomize()
heads = coolrandom_choose(true,false); // whether you get heads or tails
player_goes_first = coolrandom_choose(true,false); // whether the player goes first
predicted_heads = true; //whether you called heads. if false you called tails
coin_string = "Heads!"
if(!heads){
	coin_string = "Tails!"	
}
coin_transition_x = 600; // transition
coin_transition_rate = 50; // how many pixels per frame

menu_cancel = -1;
menu_up = -1;
menu_down = -1;
menu_left = -1
menu_right = -1;

menu_draw = coinFlip_draw_duel;

start_coin_flip = true
trigger_sound = false;
called_wrong = true;

option_x_offset = 100
option_y_offset = 40;
option_spacer = 70;

title_x = 0;
title_y = 0
title = "";
draw_title = false;

menu_height = 180
menu_width = 330

x = 250
y = 180

finish_countdown = 10;
finish_countdown_start = false;
display_window = false;
running_obj = noone;
completed = false;
fade_obj = noone;
calling = false;

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

menu_choose = false;
mousepos_y =0
mousepos_x = 60;
draw_width = 300

done = false;

instance_create(0,0,fadeIn)

ax_title = ""
if(global.speak_details){
	ax_title += " If you call it right, you choose to go first or second."
}

menu_select = -1

return_string = "return"

menu_push(id)

do_steppy = false;
