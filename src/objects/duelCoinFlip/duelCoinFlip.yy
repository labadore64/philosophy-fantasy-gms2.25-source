{
    "id": "c1bc0910-6628-46d4-9c3b-be657f5ee33f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "duelCoinFlip",
    "eventList": [
        {
            "id": "b753cd8b-5251-46e1-b81e-ee5766fa2c8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c1bc0910-6628-46d4-9c3b-be657f5ee33f"
        },
        {
            "id": "94ee01d5-37a4-459b-9af7-6aa80aa14090",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c1bc0910-6628-46d4-9c3b-be657f5ee33f"
        },
        {
            "id": "455c91ab-773d-4608-a995-2cf6dd08fe58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c1bc0910-6628-46d4-9c3b-be657f5ee33f"
        },
        {
            "id": "e218fc9a-8b6a-4218-b001-492ca83119c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "c1bc0910-6628-46d4-9c3b-be657f5ee33f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}