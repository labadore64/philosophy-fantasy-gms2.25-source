{
    "id": "91add8d6-893b-41cc-ab62-94a72d0b1f08",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gameStart",
    "eventList": [
        {
            "id": "a7d06988-09bc-4728-951b-8a6d3539c4fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91add8d6-893b-41cc-ab62-94a72d0b1f08"
        },
        {
            "id": "ffd2cdf2-c8a0-4fbb-a033-02fc587994d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "91add8d6-893b-41cc-ab62-94a72d0b1f08"
        },
        {
            "id": "3b368dfa-3a21-489a-9db2-f834be209682",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "91add8d6-893b-41cc-ab62-94a72d0b1f08"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}