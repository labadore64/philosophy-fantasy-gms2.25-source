/// @description Insert description here
// You can write your code in this editor
clear_save_data();
// Inherit the parent event
event_inherited();

spr_unload_card()

shader_id = 0

soundPlaySong(-1,0)

// this queue contains the things to run.
load_queue = ds_queue_create();

menu_draw = gameStart_draw;

// only progress if this item doesn't exist
running_object = noone;

menu_push(id);

gameStart_initParticle();

menu_cancel = -1;
menu_select = -1;
menu_left = -1;
menu_right = -1;
menu_up = -1
menu_down = -1

//shader

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

textbox = noone

menuControl.sprite_index = spr_portrait_mystery_man
menuControl.x = 120;
menuControl.y = 0;
menuControl.image_xscale = 1;
menu_control.image_yscale = 1

duelist_name = ""
duelist_sprite = spr_portrait_mystery_man
duelist_deck = 0;
duelist_say1 = ""
duelist_say2 = ""
duelist_bg_image = ""

duelist_img_sprite = -1;

selected_deck = 0;

// test
ds_queue_enqueue(load_queue,
				gameStart_script0a,
				gameStart_script0,
				gameStart_script1,
				gameStart_script2,
				gameStart_script3,
				gameStart_script4);