/// @description Insert description here
// You can write your code in this editor
with(duelCursor){
	duel_cursor_sprite = spr_duel_cursor	
}
queue = -1;
running_obj = noone;
counter = 0;
args = ds_map_create();
arg_to_script = ds_map_create();
var me = id;

with(duelController){
	draw_pointer = false;
	regain_control = false;
	if(ds_exists(animation_stack,ds_type_stack)){
		/*
		if(!ds_stack_empty(animation_stack)){
			if(!instance_exists(ds_stack_top(animation_stack))){
				ds_stack_pop(animation_stack);		
			}
		}
		*/
		ds_stack_push(animation_stack,me);
		
		running_obj = me;
		animation_stack_run = me;
		if(ds_stack_size(animation_stack) == 1){
			animation_stack_bottom = me
		}

	}
}

total_arg = 0;