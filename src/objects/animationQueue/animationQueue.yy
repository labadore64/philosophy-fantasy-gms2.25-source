{
    "id": "b410c15c-3cf6-453b-a7ba-165e0cf6a820",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "animationQueue",
    "eventList": [
        {
            "id": "17717467-9b76-4f3d-8fc4-38be205fb535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b410c15c-3cf6-453b-a7ba-165e0cf6a820"
        },
        {
            "id": "291e11a9-5f6d-44e1-8560-9dd3d20c1a06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b410c15c-3cf6-453b-a7ba-165e0cf6a820"
        },
        {
            "id": "cdc73762-2b7c-4864-91b4-bbae8e39d189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b410c15c-3cf6-453b-a7ba-165e0cf6a820"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}