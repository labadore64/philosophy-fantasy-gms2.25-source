{
    "id": "e38c59cc-e2c8-49a5-abce-e55f37513781",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "freeDuel",
    "eventList": [
        {
            "id": "1f643e75-8b3f-4dad-a557-48ff047eba55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e38c59cc-e2c8-49a5-abce-e55f37513781"
        },
        {
            "id": "4752f18c-e077-42d8-9858-e903e9351b9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e38c59cc-e2c8-49a5-abce-e55f37513781"
        },
        {
            "id": "0bea89d1-a94a-4a61-bdf0-ec62627daa6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e38c59cc-e2c8-49a5-abce-e55f37513781"
        },
        {
            "id": "19e1e7b1-5c78-40cf-b4bb-113b1d254f7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e38c59cc-e2c8-49a5-abce-e55f37513781"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}