/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
menuParent_menu_default_button();

menu_draw = freeDuel_draw

spr_unload_card()
selected_draw_index=0
info_x_offset = 20;
info_y_offset = 495

option_spacer = 60;
option_x_offset = 20;
option_y_offset = 120
per_line = 8
option_top = 0;
wrap = false

if(instance_exists(obj_data)){
	for(var i = 0; i < obj_data.opponents_total; i++){
		if(obj_data.opponent_encounter[i] > -1){
			duel_opponent[menu_count] = i
			duel_level[menu_count] = obj_data.opponent_encounter[i];
			menu_add_option(obj_data.opponent_name[i],"",obj_data.opponent_philo[i],obj_data.opponent_sprite[i])
			spr_load_card_front(obj_data.opponent_philo[menu_count])
		}
	}
}

menu_up = freeDuel_Up
menu_down = freeDuel_Down
menu_select = freeDuel_select
menu_update_values = freeDuel_update

fade_surface = -1
fade_surface_updated = false;

duel = false;
running_object = noone;

translation_rate_start = 25;
if(global.menu_animate){
	translation_start = 150;
} else {
	translation_start = 0	
}

translation_decay_rate = .975

translation = translation_start
translation_rate = translation_rate_start;

menu_help = "freeplay"

draw_width = 400
mousepos_x = 200
mousepos_y = 10

ax_title = "Select Opponent"

for(var i = 0; i < obj_data.cards_total; i++){
	if(ds_list_find_index(obj_data.current_deck,i) > -1){
		spr_load_card_front(i)
	}
}

menu_push(id)