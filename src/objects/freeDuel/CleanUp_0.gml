/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(surface_exists(fade_surface)){
	surface_free(fade_surface);	
}

if(instance_exists(obj_data)){
	with(mainMenu){
		spr_unload_card()
		var counter = 0;
		for(var i = 0; i < obj_data.cards_total; i++){
			if(ds_list_find_index(obj_data.current_deck,i) > -1){
				spr_load_card_front(i)
			}
		}
	}
}