/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(duel){
	if(!instance_exists(running_object)){
		room_goto(DuelStartRoom)
	}
}

if(translation >= 0){
	surface_update = true;
	translation-=ScaleManager.timer_diff*translation_rate;

	translation_rate=translation_rate*translation_decay_rate;

	if(translation < 0 ){
		translation = 0	
	}
} else {
	translation = 0;
}
