/// @description Insert description here
// You can write your code in this editor
if(global.menu_animate){
	if(animation != CARD_ANIMATION_NONE){
		if(animation == CARD_ANIMATION_FLIP_FACEDOWN ||
			animation == CARD_ANIMATION_FLIP_FACEUP){
			var halver = turn_speed*.5+1
			
			if(animation_time > turn_speed*.5){
				var scaler = (turn_speed-animation_time)/halver;
			
				x_scale = 1-scaler;
				point_scale_y[1] = 1-(scaler)*.35;
				point_scale_y[2] = 1-(scaler)*.35;
			} else {
				if(!animate_flip){
					display_front = !display_front;
					animate_flip = true;	
					surface_update = true
					point_scale_y[1] = 1
					point_scale_y[2] = 1
				}
				var scaler = (animation_time)/halver;
			
				x_scale = 1-scaler;
				point_scale_y[0] = 1-(scaler)*.35;
				point_scale_y[3] = 1-(scaler)*.35;
			}
		} else if (animation == CARD_ANIMATION_ACTIVATE){
			var scaler = (animation_time)/(activate_speed);
			point_scale_y[0] = 1-(scaler)*2;
			point_scale_y[1] = 1-(scaler)*2;
		} else if (animation == CARD_ANIMATION_FLASH){
			card_surface_update = true;	
		}
	
		card_calculate_points();
		animation_time-= ScaleManager.timer_diff
		if(animation_time < 0){
			card_reset_animation();
		}
	}
} else {
	if(!update_no_animate){
		point_scale_x[0] = 1;
		point_scale_x[1] = 1;
		point_scale_x[2] = 1;
		point_scale_x[3] = 1;
		point_scale_y[0] = 1;
		point_scale_y[1] = 1;
		point_scale_y[2] = 1;
		point_scale_y[3] = 1;

		card_calculate_points();
		update_no_animate = true;
	}
}