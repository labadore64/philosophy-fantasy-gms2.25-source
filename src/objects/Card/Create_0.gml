#macro CARD_ELEMENT_MIND 0
#macro CARD_ELEMENT_SPIRIT 1
#macro CARD_ELEMENT_MATTER 2

#macro CARD_TYPE_PHILOSOPHER 0
#macro CARD_TYPE_METAPHYS 1
#macro CARD_TYPE_EFFECT 2

#macro CARD_ANIMATION_NONE 0
#macro CARD_ANIMATION_FLIP_FACEDOWN 1
#macro CARD_ANIMATION_FLIP_FACEUP 2
#macro CARD_ANIMATION_ACTIVATE 3
#macro CARD_ANIMATION_FLASH 4

#macro CARD_DISPLAY_MODE_NORMAL 0
#macro CARD_DISPLAY_MODE_ATTACK 1
#macro CARD_DISPLAY_MODE_DEFENSE 2

card_changed = false;
surf = -1;
update_no_animate = false;
front = true;
display_front = true;
animation = CARD_ANIMATION_NONE
animation_time = -1;
animate_flip = false;

turn_speed = 36
activate_speed = 18

surface_update = false;

x_scale = 1;
y_scale = 1;

point_scale_x[0] = 1;
point_scale_x[1] = 1;
point_scale_x[2] = 1;
point_scale_x[3] = 1;
point_scale_y[0] = 1;
point_scale_y[1] = 1;
point_scale_y[2] = 1;
point_scale_y[3] = 1;

card_width = 400;
card_height = 600;
card_x = 0;
card_y = 0;

card_scale = .5

image_blend = $7F7FFF

// Card Stats
card_id = 0
name_display = "";
name_characters = 21;

card_overlay[0] = -1;
card_overlay[1] = -1;

card_attack_power = 0;
card_defense_power = 0;

card_boost_attack = 0;
card_boost_defense = 0;

card_sprite = -1;
card_surface = -1;
card_surface_update = false

original_x = 0;
original_y = 0;
original_scale = 0;

card_sprite_ref = preview_philosopher

card_reset_size();

text_display_mode = CARD_DISPLAY_MODE_NORMAL

mx = 0;
my = 0;