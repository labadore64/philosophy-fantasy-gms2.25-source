{
    "id": "51bb9c99-f999-449a-ba6f-444208ef1f7b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Card",
    "eventList": [
        {
            "id": "4922ab0d-b987-4c45-b0a9-b5509d2a964e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51bb9c99-f999-449a-ba6f-444208ef1f7b"
        },
        {
            "id": "091fe3f7-1da2-4b28-9b5c-778aa89d4dc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "51bb9c99-f999-449a-ba6f-444208ef1f7b"
        },
        {
            "id": "703ef945-2c0f-4b26-9762-2ad6673b6698",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "51bb9c99-f999-449a-ba6f-444208ef1f7b"
        },
        {
            "id": "7c39d7c6-e5e6-49d1-84ea-988597faf94f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "51bb9c99-f999-449a-ba6f-444208ef1f7b"
        },
        {
            "id": "29d8c4d3-1674-45b3-8385-89a4ee5e2976",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51bb9c99-f999-449a-ba6f-444208ef1f7b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}