/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

Sname = -1;
particle1 = -1;

with(obj_data){
	loaded_cutscene_counter = 0;	
}

soundPlaySong(-1,0)

// this queue contains the things to run.
load_queue = ds_queue_create();

menu_draw = textStart_draw;

// only progress if this item doesn't exist
running_object = noone;

menu_push(id);

duelist_bg_image = obj_data.loaded_background
duelist_img_sprite = -1;
if(duelist_bg_image != -1){
	var spritename = working_directory + "resources\\bg\\" + duelist_bg_image  + ".png";
	duelist_img_sprite =  sprite_add(spritename, 0, false, false, 0, 0)
}


menu_cancel = -1;
menu_select = -1;

//shader
part_surf = -1;

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

textbox = noone

menuControl.sprite_index = -1
menuControl.x = 120;
menuControl.y = 0;
menuControl.image_xscale = 1;
menu_control.image_yscale = 1

demo_get_duelists()

// custom cutscene sprites go here
if(duelist_sprite == spr_portrait_roger){
	duelist_sprite = spr_portrait_roger_cutscene	
}


// test
ds_queue_enqueue(load_queue,
				gameStart_script0a,
				textbox_script0);
				
// add text boxes
repeat(obj_data.loaded_cutscene_count){
	ds_queue_enqueue(load_queue,
	textbox_script1)
}
				
ds_queue_enqueue(load_queue,
				gameStart_script4);
				
if(obj_data.loaded_particle_script > -1){
	script_execute(obj_data.loaded_particle_script)	
}