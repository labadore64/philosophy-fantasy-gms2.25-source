{
    "id": "e84f189b-e9ab-4d9d-95c3-24515a98743a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "textStart",
    "eventList": [
        {
            "id": "7e992f2b-076f-4d70-8f7e-9ff32d01174b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e84f189b-e9ab-4d9d-95c3-24515a98743a"
        },
        {
            "id": "23cb6256-a58d-4c0b-b023-a1e499f0131e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e84f189b-e9ab-4d9d-95c3-24515a98743a"
        },
        {
            "id": "111461ed-0dc0-4662-a079-f06975bc411b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e84f189b-e9ab-4d9d-95c3-24515a98743a"
        },
        {
            "id": "141701a9-7ecc-44f3-8803-ff246e206073",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e84f189b-e9ab-4d9d-95c3-24515a98743a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}