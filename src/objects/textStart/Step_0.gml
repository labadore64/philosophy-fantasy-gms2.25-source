/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// script execution
if(!instance_exists(running_object)){
	if(ds_queue_empty(load_queue)){
		instance_destroy()	
	} else {
		var script = ds_queue_dequeue(load_queue);
		if(script > -1){
			script_execute(script);	
		}
	}
}

if(Sname > -1){
	repeat(ScaleManager.timer_diff_round){
		if(global.particles){part_system_update(Sname)}
	}

	if(ScaleManager.updated){	
		part_emitter_region(Sname,emitter1,
							global.display_x,
							global.display_x+global.letterbox_w,
							global.display_y,
							global.display_y+global.letterbox_h,
							ps_shape_rectangle,
							ps_distr_linear);

				
		part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
	}

	shader_sprite_index+= ScaleManager.timer_diff
	shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;	
}