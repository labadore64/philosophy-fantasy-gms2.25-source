{
    "id": "11405777-f79f-4d29-96b8-1f1bcbf925fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dialogue",
    "eventList": [
        {
            "id": "b1e6b935-5095-43d1-8b02-101bef6a41d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11405777-f79f-4d29-96b8-1f1bcbf925fe"
        },
        {
            "id": "642299c5-3602-4845-bced-b5ff99272410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11405777-f79f-4d29-96b8-1f1bcbf925fe"
        },
        {
            "id": "966eda60-e180-47f9-9e8b-ed2a1db1d4cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "11405777-f79f-4d29-96b8-1f1bcbf925fe"
        },
        {
            "id": "47abc55a-44e5-46f0-9d85-72127d1da0b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "11405777-f79f-4d29-96b8-1f1bcbf925fe"
        },
        {
            "id": "dca99216-faf6-42fb-86d2-f4e2f2ff1771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "11405777-f79f-4d29-96b8-1f1bcbf925fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}