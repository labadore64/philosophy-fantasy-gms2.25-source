/// @description Insert description here
// You can write your code in this editor

repeat(ScaleManager.timer_diff_round*FAST){
	if(global.particles){part_system_update(Sname)}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff*FAST
shader_counter+= ScaleManager.timer_diff*FAST; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31);