/// @description Insert description here
// You can write your code in this editor
Sname = part_system_create();

var scale = .25;

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_square);
part_type_scale(particle1,scale,scale);
if(!global.enableShader){
	part_type_color1(particle1,$7F7F00);
} else {
	part_type_color1(particle1,16776960);	
}
part_type_alpha3(particle1,0,1,0);
part_type_life(particle1,60,120);
part_type_blend(particle1,true)

quantity = 2
part_type_alpha3(particle1,0,.05,0);
part_type_size(particle1,1,1,.01,0);
part_type_speed(particle1,1,1,0,0)
part_type_direction(particle1,270,270,0,0)
part_type_gravity(particle1,.05,90)

emitter1 = part_emitter_create(Sname);
part_emitter_region(Sname,emitter1,
					global.display_x,
					global.window_width,
					global.display_y,
					global.window_height,
					ps_shape_rectangle,
					ps_distr_linear);

part_system_automatic_draw(Sname, false)
part_system_automatic_update(Sname,false)

part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
repeat(500){
	if(global.particles){part_system_update(Sname)}
}

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

surf = -1;
surface_update = false;