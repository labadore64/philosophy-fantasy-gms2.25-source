/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

repeat(ScaleManager.timer_diff_round){
	if(global.particles){part_system_update(Sname)}
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_linear);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= ScaleManager.timer_diff
shader_counter+= ScaleManager.timer_diff; shader_bg_color=make_color_hsv((.25*shader_counter)%255,181,31); part_type_color1(particle1,make_color_hsv(.25*(shader_counter+15)%255,181,151));;

if(animate > -1){
	if(!animate_left){
		animate_x-=animate_dist*ScaleManager.timer_diff
	} else {
		animate_x+=animate_dist*ScaleManager.timer_diff
	}
	
	if(animate_x > 250){
		animate_x = 250;	
	} else if(animate_x < -250){
		animate_x = -250;	
	}
	
	animate -= ScaleManager.timer_diff;
	if(animate <= -1){
		animate_x = 0;
		animate = -1;
		selectPack_displayUpdate();
	}
}