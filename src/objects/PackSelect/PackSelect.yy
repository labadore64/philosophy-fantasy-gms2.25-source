{
    "id": "98be1bf1-b7c9-452a-af76-d0d0a1423924",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PackSelect",
    "eventList": [
        {
            "id": "0e375228-d3fa-4393-8b4b-44fdb5ce57f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "98be1bf1-b7c9-452a-af76-d0d0a1423924"
        },
        {
            "id": "773805b8-3e91-4700-a049-973ba80b77b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "98be1bf1-b7c9-452a-af76-d0d0a1423924"
        },
        {
            "id": "65005476-d41e-479e-a6cc-103c047fda0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "98be1bf1-b7c9-452a-af76-d0d0a1423924"
        },
        {
            "id": "28978b72-e025-412b-95fe-6a049890d657",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "98be1bf1-b7c9-452a-af76-d0d0a1423924"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}