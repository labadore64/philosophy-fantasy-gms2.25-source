/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
tts_stop();
pack_list = generatePackList();

var sizer = ds_list_size(pack_list);

for(var i = 0; i < sizer; i++){
	pack[i] = createCardPack(pack_list[|i],50+250*i,100)	
}

selectPack_loadSprites();

menu_draw = selectPack_draw;

deckBuild_initParticle();

// shader stuff

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

menu_up = selectPack_moveUp;
menu_down = selectPack_moveDown;
menu_left = menu_up;
menu_right = menu_down
menu_select = selectPack_select;

menu_cancel = selectPack_openCardList;



for(var i = 0; i < sizer; i++){
	menu_add_option(obj_data.pack_name[pack[i].pack_id],
					obj_data.pack_desc[pack[i].pack_id],
					-1,-1)
}

animate = -1;
animate_time = 10
animate_dist = 250/animate_time
animate_x = 0;
animate_left = false;

y = 75

selectPack_displayUpdate();

instance_create(120,0,fadeIn)

menu_help = "packselect"
ax_title = "Select a booster pack."
menu_push(id)