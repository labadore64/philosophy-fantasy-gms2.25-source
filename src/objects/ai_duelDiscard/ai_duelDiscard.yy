{
    "id": "fd8856c9-23a5-429b-9bd0-9b98b0cfa255",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ai_duelDiscard",
    "eventList": [
        {
            "id": "609ed4eb-6f7d-4832-ad0c-3f691c1fc119",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd8856c9-23a5-429b-9bd0-9b98b0cfa255"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7f7b1f7b-3ff6-431b-a380-36446bbb3b68",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}