/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
deckBuild_deleteParticle();
with(selected_card){
	instance_destroy();	
}

if(surface_exists(menu_surf)){
	surface_free(menu_surf);	
}

if(sprite_exists(loaded_sprite)){
	sprite_delete(loaded_sprite)	
}

menu_pop();

//