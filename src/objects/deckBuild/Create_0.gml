/// @description Insert description here
// You can write your code in this editor

#macro DECK_BUILD_TRUNK 0
#macro DECK_BUILD_DECK 1

// Inherit the parent event
event_inherited();

archetype_string = "";
// start trunk list
var counter = 0;
for(var i = 0; i < obj_data.cards_total; i++){
	if(obj_data.card_in_trunk[i] > 0){
		trunk_cards[counter] = i
		counter++
	}
}


// draw stuff

// whether in trunk or deck
menu_selected = DECK_BUILD_TRUNK


info_x_offset = 20;
info_y_offset = 495

option_spacer = 90;
option_x_offset = 20;
option_y_offset = 20+option_spacer;
per_line = 4;
option_top = 0;

mousepos_x = 210
draw_width=450

soundPlaySong(sfx_music_deck_construction,0)

menu_draw = deckBuild_draw;

menu_up = deckBuild_moveUp;
menu_down = deckBuild_moveDown;
menu_tableft = deckBuild_moveLeft;
menu_tabright = deckBuild_moveLeft;//deckBuild_moveRight;
menu_cancel = deckBuild_cancel
menu_left = deckBuild_tabLeft
menu_right = deckBuild_tabRight

menu_select = deckBuild_select

menu_update_values = deckBuild_updateSurface

scrolling = false;

wrap = false;

// card stuff

selected_card = card_create(635,275,.6,0);

deck_menupos = -1
trunk_menupos = -1

// data structures

for(var i = 0; i < per_line; i++){
	draw_id[i] = -1;	
}

selected_draw_index = 0;
loaded_sprite = -1

deckBuild_setDisplayArrays();

deckBuild_initParticle();

// shader stuff


shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

instance_create(120,0,fadeIn)

cancelled = false;
running_object = noone;

menu_surf = -1;
menu_surf_update = false;

tts_say("Trunk")

menu_help="deckbuild"


var counter = 0;
var card_total = 0;
// count total card types
for(var i = 0; i < obj_data.cards_total; i++){
	if(obj_data.card_in_trunk[i] > 0 || 
		ds_list_find_index(obj_data.current_deck,i) != -1){
		counter++;		
	}
	card_total+=obj_data.card_in_trunk[i];
}
card_total+= ds_list_size(obj_data.current_deck);

// collected cards
total_cards = string(counter) + "/" + string(obj_data.cards_total);

sort_id[0] = "TYPE"
sort_id[1] = "ABC"
sort_id[2] = "ZYX"
sort_id[3] = "ATT"
sort_id[4] = "DEF"
sort_id[5] = "ARC1"
sort_id[6] = "ARC2"


sort_ax_id[0] = "type"
sort_ax_id[1] = "alphabetical"
sort_ax_id[2] = "reverse alphabetical"
sort_ax_id[3] = "attack highest"
sort_ax_id[4] = "defense highest"
sort_ax_id[5] = "archetype 1"
sort_ax_id[6] = "archetype 2"

sort_script[0] = deckSort_type
sort_script[1] = deckSort_abc
sort_script[2] = deckSort_zyx
sort_script[3] = deckSort_att
sort_script[4] = deckSort_def
sort_script[5] = deckSort_arch1
sort_script[6] = deckSort_arch2

sort_size = array_length_1d(sort_id)

sort_mode_pos = 0;
sort_mode = sort_id[sort_mode_pos]

mouse_menu_script = deckBuild_mouse_menu_script

clear_key_def()
add_key_def("Up","Move up","up")
add_key_def("Down","Move down","down")
add_key_def("Page Up","","left")
add_key_def("Page Down","","right")
add_key_def("Deck/Trunk","","lefttab")
add_key_def("Deck/Trunk","","righttab")
add_key_def("Select","","select")
add_key_def("Cancel","Go back","cancel")
add_key_def("Sort","","sort")

menu_control = deckBuild_sort
deckSort_type();
deckBuild_setDisplayArrays();

deckBuild_updateSprite();

fave_cando = eventCheck(("fave"));

ax_title = "Deck Build"

menuParent_menu_default_button();
cancel_button_x-=20
help_button_x-=20

event_user(2)

menu_push(id);