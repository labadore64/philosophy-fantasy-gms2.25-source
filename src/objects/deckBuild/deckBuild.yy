{
    "id": "ddbc7f0d-6ae7-4dbb-8e49-b6e268c69abc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "deckBuild",
    "eventList": [
        {
            "id": "919996f7-2619-4fc4-a213-482458c05423",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddbc7f0d-6ae7-4dbb-8e49-b6e268c69abc"
        },
        {
            "id": "063ca2a1-5bfa-487f-9e7c-4eb93f285dec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ddbc7f0d-6ae7-4dbb-8e49-b6e268c69abc"
        },
        {
            "id": "1501b5a1-683e-465e-87b6-9a7eb7360951",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ddbc7f0d-6ae7-4dbb-8e49-b6e268c69abc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a776c784-4a84-48b1-b5f1-75afa06dd9fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}