{
    "id": "8d677c12-7a6c-4caa-8805-2a2e4a5c3266",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_data",
    "eventList": [
        {
            "id": "78c0d64f-f68f-473c-ba0a-654338d1ead2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d677c12-7a6c-4caa-8805-2a2e4a5c3266"
        },
        {
            "id": "b8f924c0-b4a2-4dcd-a03d-9851c9a5b760",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8d677c12-7a6c-4caa-8805-2a2e4a5c3266"
        },
        {
            "id": "6dd7bef5-d7da-44f1-bfae-37c870805766",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "8d677c12-7a6c-4caa-8805-2a2e4a5c3266"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}