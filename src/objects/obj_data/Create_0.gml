/// @description Insert description here
// You can write your code in this editor
loadEvents();
objdata_load_types();
objdata_load_cards("default");
objdata_load_packs()
objdata_load_opponent()
objdata_load_songs();
init_random();

for(var i = 0; i < 1000; i++){
	ds_the[i] = i;	
}

//create diagram list
diagrams = instance_create(0,0,objdata_diagram)

// inits story values
story_script_init();


loaded_opponent = -1
loaded_duel = 0;
loaded_background = -1;
free_duel = false;

duel_music = -1;

loaded_textbox[0] = "";
loaded_character[0] = 0;
loaded_music[0] = -1;
loaded_cutscene_count = 0;
loaded_cutscene_counter = 0;
loaded_particle_script = -1;
loaded_shader = -1;

chapter = 1;
chapter_title = "";
chapter_card = "";
story_duel = false;
// 1 = win
// 0 = lose
// 2 = draw
last_game_state = 0;

wins = 0
losses = 0
draws = 0

savescum = false;

card_img[0] = preview_philosopher
card_bg_img[0] = preview_philosopher
card_img_size = 0;
card_bg_img_size = 0;

load_game();