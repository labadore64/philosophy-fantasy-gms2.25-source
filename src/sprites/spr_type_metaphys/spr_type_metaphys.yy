{
    "id": "dab4c019-32f9-4b83-96d4-03037a0ee328",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_type_metaphys",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46a82409-8513-4658-a070-928edd578cf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab4c019-32f9-4b83-96d4-03037a0ee328",
            "compositeImage": {
                "id": "e3f36c56-3fb7-4ca3-95eb-8701ba3aa7f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a82409-8513-4658-a070-928edd578cf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5095c964-2ba2-415f-a811-494e0e231edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a82409-8513-4658-a070-928edd578cf9",
                    "LayerId": "b737b11c-ac7b-4d65-b0bc-b62f3488aeca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b737b11c-ac7b-4d65-b0bc-b62f3488aeca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dab4c019-32f9-4b83-96d4-03037a0ee328",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}