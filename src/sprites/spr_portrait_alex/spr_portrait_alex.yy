{
    "id": "0d62e731-aa23-41e9-b8d0-f420c8ff970f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_alex",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 86,
    "bbox_right": 453,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0419624-1d97-4481-99b0-0bffd66b3a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d62e731-aa23-41e9-b8d0-f420c8ff970f",
            "compositeImage": {
                "id": "1e92e573-cb30-477e-b606-7d48c0ee387d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0419624-1d97-4481-99b0-0bffd66b3a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d00df46-e1fb-47b1-963f-7103fba8ec3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0419624-1d97-4481-99b0-0bffd66b3a56",
                    "LayerId": "58652427-0147-45ae-b9af-f66e9d87dfe8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "58652427-0147-45ae-b9af-f66e9d87dfe8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d62e731-aa23-41e9-b8d0-f420c8ff970f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}