{
    "id": "dd47223c-244a-408b-8e3e-011cdd84781a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 398,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aee6baf6-dffc-40fa-9bef-dbfd76056793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd47223c-244a-408b-8e3e-011cdd84781a",
            "compositeImage": {
                "id": "7da66c6f-e1fc-41fc-89b8-697b4d76c90a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee6baf6-dffc-40fa-9bef-dbfd76056793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84280510-8860-4c94-95ac-089b4d25d731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee6baf6-dffc-40fa-9bef-dbfd76056793",
                    "LayerId": "81559a85-3275-46cc-93d0-668cb0de1ad1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "81559a85-3275-46cc-93d0-668cb0de1ad1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd47223c-244a-408b-8e3e-011cdd84781a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}