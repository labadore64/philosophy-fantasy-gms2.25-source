{
    "id": "40486d29-2969-4c5f-97c1-e3a1a3011104",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_duel_rotate_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 10,
    "bbox_right": 83,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1c37dd1-60cb-4bc1-b4f7-f86c85837b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40486d29-2969-4c5f-97c1-e3a1a3011104",
            "compositeImage": {
                "id": "1221d2f2-7a15-4a4c-85f5-7999d55a3d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c37dd1-60cb-4bc1-b4f7-f86c85837b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72763214-fa66-4c56-98fc-b65a31a717b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c37dd1-60cb-4bc1-b4f7-f86c85837b61",
                    "LayerId": "76b734cf-2849-4157-9ecc-34845a7950e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "76b734cf-2849-4157-9ecc-34845a7950e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40486d29-2969-4c5f-97c1-e3a1a3011104",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 86,
    "yorig": 36
}