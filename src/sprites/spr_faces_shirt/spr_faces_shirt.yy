{
    "id": "b6b198fb-544f-4d36-a178-1128aac870f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_shirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 92,
    "bbox_right": 426,
    "bbox_top": 353,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6687ee71-d695-494d-9e36-7b5c2fd6f682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6b198fb-544f-4d36-a178-1128aac870f7",
            "compositeImage": {
                "id": "a97749f8-f30a-4caf-b5d6-396bab742445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6687ee71-d695-494d-9e36-7b5c2fd6f682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aee00ee-e891-4938-9d53-dfe955133eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6687ee71-d695-494d-9e36-7b5c2fd6f682",
                    "LayerId": "cfb214ef-ad11-4218-be11-aedbb9980c37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "cfb214ef-ad11-4218-be11-aedbb9980c37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6b198fb-544f-4d36-a178-1128aac870f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}