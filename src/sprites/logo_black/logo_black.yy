{
    "id": "da79f0d5-87b3-45f2-9561-06f4866f19ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "logo_black",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 141,
    "bbox_right": 360,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80a6205e-32ff-45e6-8e8a-eec6923cc629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da79f0d5-87b3-45f2-9561-06f4866f19ba",
            "compositeImage": {
                "id": "8f9ff45b-84a6-4d72-b40a-d456f47968ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a6205e-32ff-45e6-8e8a-eec6923cc629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c28de54-d387-47aa-bdf9-7718a1e58861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a6205e-32ff-45e6-8e8a-eec6923cc629",
                    "LayerId": "0b769078-da73-4337-aaf9-7ab04da59cbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "0b769078-da73-4337-aaf9-7ab04da59cbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da79f0d5-87b3-45f2-9561-06f4866f19ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 251,
    "yorig": 263
}