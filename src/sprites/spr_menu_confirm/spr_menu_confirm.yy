{
    "id": "da8f823e-6f07-4116-b17b-07dd8b6fd689",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_confirm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47e26ae2-1fba-4d05-9ec1-f24ed93ec0a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da8f823e-6f07-4116-b17b-07dd8b6fd689",
            "compositeImage": {
                "id": "9269c376-fbe5-414a-a1a0-9995babf18ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e26ae2-1fba-4d05-9ec1-f24ed93ec0a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42014d0-2438-4c15-abda-e1ff4dba5baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e26ae2-1fba-4d05-9ec1-f24ed93ec0a5",
                    "LayerId": "0df8ac05-81a4-41d5-9798-25188f325379"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0df8ac05-81a4-41d5-9798-25188f325379",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da8f823e-6f07-4116-b17b-07dd8b6fd689",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}