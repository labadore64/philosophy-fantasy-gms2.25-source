{
    "id": "d5410bd0-b333-46f2-a99b-933b0f3a4697",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_colormap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0648369c-7abf-493a-8c0e-17936082b7d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5410bd0-b333-46f2-a99b-933b0f3a4697",
            "compositeImage": {
                "id": "48ec2e27-e5ee-487e-a37f-0f5a0fe6a664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0648369c-7abf-493a-8c0e-17936082b7d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a53aa1-0051-4ad3-adaf-57247f4de931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0648369c-7abf-493a-8c0e-17936082b7d1",
                    "LayerId": "56e1ebf8-dc7a-4f98-a1d7-65bec5b6479d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "56e1ebf8-dc7a-4f98-a1d7-65bec5b6479d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5410bd0-b333-46f2-a99b-933b0f3a4697",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a2e48f42-f2d7-4aa4-9195-47cd93a8b9fc",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}