{
    "id": "17a6bb8d-ebb9-4e9f-860f-732601f73aae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_player_nb2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 54,
    "bbox_right": 448,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c94dc47-b67d-4cc9-821b-5572b38fbb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17a6bb8d-ebb9-4e9f-860f-732601f73aae",
            "compositeImage": {
                "id": "3b93727e-99f4-4f47-9013-3289eea53c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c94dc47-b67d-4cc9-821b-5572b38fbb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a865a6-3f49-4f7e-ab2c-28ec487284bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c94dc47-b67d-4cc9-821b-5572b38fbb87",
                    "LayerId": "0d5fa6a6-2a04-4fd2-8cee-1b0b0108b09a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "0d5fa6a6-2a04-4fd2-8cee-1b0b0108b09a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17a6bb8d-ebb9-4e9f-860f-732601f73aae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}