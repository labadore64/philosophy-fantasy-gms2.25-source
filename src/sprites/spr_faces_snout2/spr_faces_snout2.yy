{
    "id": "951ae86b-6271-4dec-8462-6fbfbe6e165b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_snout2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "408aed26-0ae4-451a-98cc-c844c4d00c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "951ae86b-6271-4dec-8462-6fbfbe6e165b",
            "compositeImage": {
                "id": "5a0fe23a-289c-4c09-bc9e-8c907775b4af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "408aed26-0ae4-451a-98cc-c844c4d00c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ade04db-e8e1-405e-ab07-6d34b9fe4fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "408aed26-0ae4-451a-98cc-c844c4d00c04",
                    "LayerId": "52fec3a4-fcd8-407d-bd38-93e606f406c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "52fec3a4-fcd8-407d-bd38-93e606f406c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "951ae86b-6271-4dec-8462-6fbfbe6e165b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}