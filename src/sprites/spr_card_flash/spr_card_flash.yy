{
    "id": "f71b541e-9811-483f-858f-11e94ce19178",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_flash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 596,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf06848c-5fd9-4dee-aaa8-e4d12654e8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f71b541e-9811-483f-858f-11e94ce19178",
            "compositeImage": {
                "id": "c9b2da80-ad46-4302-8b40-807ef958b4d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf06848c-5fd9-4dee-aaa8-e4d12654e8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8db918e-e8ff-4882-9ee6-7e87c648f59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf06848c-5fd9-4dee-aaa8-e4d12654e8f2",
                    "LayerId": "03569c91-537e-4c1c-8250-1d04917dcb4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "03569c91-537e-4c1c-8250-1d04917dcb4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f71b541e-9811-483f-858f-11e94ce19178",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}