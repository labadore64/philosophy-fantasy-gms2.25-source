{
    "id": "7a786542-12d8-4dc2-911c-5c3da8d256fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_forfeit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0635d30a-b8f0-4791-920e-d79e2b6a5649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a786542-12d8-4dc2-911c-5c3da8d256fa",
            "compositeImage": {
                "id": "c936a4a6-cf40-4960-a92d-56a173cf1ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0635d30a-b8f0-4791-920e-d79e2b6a5649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb112e42-2d8e-47de-ad40-cbdef5bc5d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0635d30a-b8f0-4791-920e-d79e2b6a5649",
                    "LayerId": "15d79b08-560d-483c-a72b-b015c14d71d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "15d79b08-560d-483c-a72b-b015c14d71d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a786542-12d8-4dc2-911c-5c3da8d256fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 19,
    "yorig": 21
}