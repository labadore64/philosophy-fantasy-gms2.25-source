{
    "id": "3354b9f6-6970-4a5e-9d7e-087285d99f87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_shine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac6d8c92-6183-4699-bd22-6346e459303c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3354b9f6-6970-4a5e-9d7e-087285d99f87",
            "compositeImage": {
                "id": "f247097a-1f92-4e6a-9fe5-5a40c2174acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac6d8c92-6183-4699-bd22-6346e459303c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649b9248-bc78-4464-b2f3-562c87f0dcae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac6d8c92-6183-4699-bd22-6346e459303c",
                    "LayerId": "cd6c62a4-5580-4e32-a6cc-2611ad054089"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "cd6c62a4-5580-4e32-a6cc-2611ad054089",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3354b9f6-6970-4a5e-9d7e-087285d99f87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}