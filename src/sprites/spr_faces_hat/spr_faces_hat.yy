{
    "id": "3ece733c-3871-4c44-9ea2-0111aab91fc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_hat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1e360fb-abee-4bee-b099-ff3a004e8a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ece733c-3871-4c44-9ea2-0111aab91fc3",
            "compositeImage": {
                "id": "2d7498fd-b53e-4706-baa2-87307eb39764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e360fb-abee-4bee-b099-ff3a004e8a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e7d1bc7-22e3-4c9a-9b08-7dfdafe3a9e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e360fb-abee-4bee-b099-ff3a004e8a8d",
                    "LayerId": "2b749876-0b56-4894-b277-227ec45c0ac4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2b749876-0b56-4894-b277-227ec45c0ac4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ece733c-3871-4c44-9ea2-0111aab91fc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}