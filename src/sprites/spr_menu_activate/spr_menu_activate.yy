{
    "id": "e3729cec-0ee8-4572-b17d-fc8e8ad53aeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_activate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 10,
    "bbox_right": 117,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90dabd9f-96f9-462f-b664-71e07a5663ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3729cec-0ee8-4572-b17d-fc8e8ad53aeb",
            "compositeImage": {
                "id": "d1a3b202-8938-413e-8cc0-8b7c73c8219e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90dabd9f-96f9-462f-b664-71e07a5663ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66b5116-09fd-40c2-8798-a38c1f3e8b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90dabd9f-96f9-462f-b664-71e07a5663ec",
                    "LayerId": "19ee6cc8-b6b1-4fb6-8c96-a8ce7b5d9673"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "19ee6cc8-b6b1-4fb6-8c96-a8ce7b5d9673",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3729cec-0ee8-4572-b17d-fc8e8ad53aeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}