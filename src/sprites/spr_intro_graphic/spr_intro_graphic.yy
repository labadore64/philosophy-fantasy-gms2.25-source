{
    "id": "0ebf24ee-c273-4aa8-b027-e4de66370aaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_intro_graphic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9769894-4c1f-4d76-9431-0ca7a0a3385f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ebf24ee-c273-4aa8-b027-e4de66370aaf",
            "compositeImage": {
                "id": "95828b15-d694-409b-afe4-1a4a6d19491b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9769894-4c1f-4d76-9431-0ca7a0a3385f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e28dd17-a4b2-46bf-a99d-d17d77aced64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9769894-4c1f-4d76-9431-0ca7a0a3385f",
                    "LayerId": "eda368ba-b770-4ee0-91c1-99cff0ab2af8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "eda368ba-b770-4ee0-91c1-99cff0ab2af8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ebf24ee-c273-4aa8-b027-e4de66370aaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}