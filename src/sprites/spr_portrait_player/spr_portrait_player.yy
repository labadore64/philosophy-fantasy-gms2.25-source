{
    "id": "cf013e34-de10-4a2a-9e62-a8784dee3b41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 54,
    "bbox_right": 448,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64076d76-4982-4d92-ad6c-eb43826ecb58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf013e34-de10-4a2a-9e62-a8784dee3b41",
            "compositeImage": {
                "id": "22e05cd6-e673-4099-a92e-4c41db3eb3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64076d76-4982-4d92-ad6c-eb43826ecb58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de34fb02-ed4b-47bc-978f-0c75203f4cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64076d76-4982-4d92-ad6c-eb43826ecb58",
                    "LayerId": "1db97db5-4cf3-411b-ab5d-4a7dae17147a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "1db97db5-4cf3-411b-ab5d-4a7dae17147a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf013e34-de10-4a2a-9e62-a8784dee3b41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}