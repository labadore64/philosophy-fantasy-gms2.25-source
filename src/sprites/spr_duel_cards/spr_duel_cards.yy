{
    "id": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_duel_cards",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 24,
    "bbox_right": 103,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d0431f2-4f32-482c-a48d-26424f13e36f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "compositeImage": {
                "id": "5d9d6887-7ebc-478e-b868-d4f74ae8c095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0431f2-4f32-482c-a48d-26424f13e36f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc10f80c-c4f3-4c46-bbe8-371f022e0603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0431f2-4f32-482c-a48d-26424f13e36f",
                    "LayerId": "541c0f84-955e-4e82-8c2e-1b82d62dc35b"
                }
            ]
        },
        {
            "id": "cdd0a5e0-da77-4198-bdca-25faf091edd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "compositeImage": {
                "id": "33bd0afa-572e-4c38-b9b7-2ae9c12400a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd0a5e0-da77-4198-bdca-25faf091edd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2410e50d-d2c9-4119-bda6-8018111d6bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd0a5e0-da77-4198-bdca-25faf091edd8",
                    "LayerId": "541c0f84-955e-4e82-8c2e-1b82d62dc35b"
                }
            ]
        },
        {
            "id": "b5348271-2c55-4f54-b5d0-fda64844144e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "compositeImage": {
                "id": "a4f5c48b-3f5a-4f18-ae05-c4e94e762510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5348271-2c55-4f54-b5d0-fda64844144e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54de1f0f-3990-4cd8-9317-706927548e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5348271-2c55-4f54-b5d0-fda64844144e",
                    "LayerId": "541c0f84-955e-4e82-8c2e-1b82d62dc35b"
                }
            ]
        },
        {
            "id": "56348a1b-8b14-4dd2-af6b-e2f58d89e419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "compositeImage": {
                "id": "24a052bd-9d36-489a-8e8d-9654924bd433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56348a1b-8b14-4dd2-af6b-e2f58d89e419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c0d85b-da0a-407d-98c9-83d895d7c8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56348a1b-8b14-4dd2-af6b-e2f58d89e419",
                    "LayerId": "541c0f84-955e-4e82-8c2e-1b82d62dc35b"
                }
            ]
        },
        {
            "id": "965d4f51-3d0a-46c0-a5cb-8ddf18d472de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "compositeImage": {
                "id": "76d08520-ccdc-489d-9a03-3d934803275e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "965d4f51-3d0a-46c0-a5cb-8ddf18d472de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b72d80-63f5-4ffe-af90-533a9cc8ebb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "965d4f51-3d0a-46c0-a5cb-8ddf18d472de",
                    "LayerId": "541c0f84-955e-4e82-8c2e-1b82d62dc35b"
                }
            ]
        },
        {
            "id": "5924fe80-88f6-4674-a45a-3b754ef60529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "compositeImage": {
                "id": "c1e6e4f2-412d-40dc-bae5-86ffd9897ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5924fe80-88f6-4674-a45a-3b754ef60529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97b3ac2-f652-441d-b43d-25477defbc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5924fe80-88f6-4674-a45a-3b754ef60529",
                    "LayerId": "541c0f84-955e-4e82-8c2e-1b82d62dc35b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "541c0f84-955e-4e82-8c2e-1b82d62dc35b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fd902e6-94e5-4127-b1d8-083acc7b2262",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}