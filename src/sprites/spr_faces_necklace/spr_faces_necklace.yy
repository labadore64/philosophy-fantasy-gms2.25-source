{
    "id": "e6449509-02a1-4a4c-943e-04b3f89dff84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_necklace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbf77027-3a30-4e45-90b4-4b13afd8eb19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6449509-02a1-4a4c-943e-04b3f89dff84",
            "compositeImage": {
                "id": "206cf421-bd5d-4ed3-8eb3-2ab55f7e1b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf77027-3a30-4e45-90b4-4b13afd8eb19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f2413d-a496-44ca-9f39-eb2164f4c15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf77027-3a30-4e45-90b4-4b13afd8eb19",
                    "LayerId": "09a513d9-43d0-49d4-b1ff-b48cea3d8076"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "09a513d9-43d0-49d4-b1ff-b48cea3d8076",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6449509-02a1-4a4c-943e-04b3f89dff84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}