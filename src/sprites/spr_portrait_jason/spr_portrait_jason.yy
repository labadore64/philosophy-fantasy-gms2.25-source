{
    "id": "c299f13c-042e-49da-ba71-9d1d259ee5ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_jason",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 52,
    "bbox_right": 456,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2122c11c-a7be-4a14-a528-c4a95a344a1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c299f13c-042e-49da-ba71-9d1d259ee5ad",
            "compositeImage": {
                "id": "1383cda2-cdbf-4bbb-9ab9-95d775dacde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2122c11c-a7be-4a14-a528-c4a95a344a1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0d0d41f-32a1-4fec-acea-fa4fbcd1156f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2122c11c-a7be-4a14-a528-c4a95a344a1d",
                    "LayerId": "0c15dfe8-5caa-45ac-8c3b-8a344ec404b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "0c15dfe8-5caa-45ac-8c3b-8a344ec404b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c299f13c-042e-49da-ba71-9d1d259ee5ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}