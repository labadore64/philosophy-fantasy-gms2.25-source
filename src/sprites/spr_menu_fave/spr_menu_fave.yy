{
    "id": "d1e5fc27-d0dc-41b1-9f96-0360c79e6d3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_fave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 14,
    "bbox_right": 110,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e76f179f-0e47-41c7-9f5b-1b2fd96f7f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1e5fc27-d0dc-41b1-9f96-0360c79e6d3b",
            "compositeImage": {
                "id": "742596ed-ac74-48f3-9100-889abe8b541e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e76f179f-0e47-41c7-9f5b-1b2fd96f7f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f52e3f-0a9f-4975-883f-dec01d37473e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e76f179f-0e47-41c7-9f5b-1b2fd96f7f56",
                    "LayerId": "ca7cc7d8-dcfb-4944-8053-8784aae06111"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ca7cc7d8-dcfb-4944-8053-8784aae06111",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1e5fc27-d0dc-41b1-9f96-0360c79e6d3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}