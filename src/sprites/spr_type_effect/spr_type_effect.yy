{
    "id": "bc2ce2f6-33ca-4198-80a2-ceddb216d9ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_type_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bd775cf-f13e-4074-bfff-1bc551b4615e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc2ce2f6-33ca-4198-80a2-ceddb216d9ac",
            "compositeImage": {
                "id": "ba5bb98a-c56a-4198-ba54-599590f194a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd775cf-f13e-4074-bfff-1bc551b4615e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "319bef31-eceb-4f65-a5aa-a2e924b7bf76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd775cf-f13e-4074-bfff-1bc551b4615e",
                    "LayerId": "738be73c-5fef-44a6-b65d-4b759d13871e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "738be73c-5fef-44a6-b65d-4b759d13871e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc2ce2f6-33ca-4198-80a2-ceddb216d9ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}