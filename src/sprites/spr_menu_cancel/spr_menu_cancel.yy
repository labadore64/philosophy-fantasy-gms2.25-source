{
    "id": "211f38d8-574d-4949-b511-d67dea4973a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_cancel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 2,
    "bbox_right": 36,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dee2b913-38b5-4691-97a4-0b643f81beab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "211f38d8-574d-4949-b511-d67dea4973a1",
            "compositeImage": {
                "id": "a6f78d0c-67dd-452e-8365-a41382247a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee2b913-38b5-4691-97a4-0b643f81beab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8e75fb-7c0f-4c16-b7a3-ab5631f11390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee2b913-38b5-4691-97a4-0b643f81beab",
                    "LayerId": "602c2b5b-9a68-49d5-91db-79ab2fde30d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "602c2b5b-9a68-49d5-91db-79ab2fde30d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "211f38d8-574d-4949-b511-d67dea4973a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}