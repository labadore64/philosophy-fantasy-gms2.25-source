{
    "id": "d27ccbc5-5b17-4495-93af-926b6812c21a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_roger_cutscene",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 74,
    "bbox_right": 399,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c629167-0acd-4fe5-b398-7dd9ae93834c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d27ccbc5-5b17-4495-93af-926b6812c21a",
            "compositeImage": {
                "id": "22a1c84b-51cc-4fe8-a641-70f35ffc51f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c629167-0acd-4fe5-b398-7dd9ae93834c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48fb47e-e8cf-4de0-aca6-61544b2119af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c629167-0acd-4fe5-b398-7dd9ae93834c",
                    "LayerId": "32831378-31f5-4e9b-8edc-5ebb00e009f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "32831378-31f5-4e9b-8edc-5ebb00e009f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d27ccbc5-5b17-4495-93af-926b6812c21a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}