{
    "id": "89dbb827-a1c5-441e-8fe1-b0df96603c66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_player_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 54,
    "bbox_right": 448,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cf84858-6e95-47ea-ab05-5dda832849b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89dbb827-a1c5-441e-8fe1-b0df96603c66",
            "compositeImage": {
                "id": "fde31f00-f92e-4cb3-8a2e-e8f2296ee9a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cf84858-6e95-47ea-ab05-5dda832849b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb5ed4f8-b7e6-4022-89ab-a0162f00fe96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cf84858-6e95-47ea-ab05-5dda832849b1",
                    "LayerId": "1278b478-1796-4db7-840a-f9a9ebbd7f6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "1278b478-1796-4db7-840a-f9a9ebbd7f6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89dbb827-a1c5-441e-8fe1-b0df96603c66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}