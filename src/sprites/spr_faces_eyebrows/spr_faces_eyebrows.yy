{
    "id": "0cf2b3d5-9638-40f4-ab7b-d8264ab7fe03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_eyebrows",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 231,
    "bbox_left": 163,
    "bbox_right": 281,
    "bbox_top": 204,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a520d4b-4d19-49db-81ea-72c638426a03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cf2b3d5-9638-40f4-ab7b-d8264ab7fe03",
            "compositeImage": {
                "id": "1264b497-8852-4647-872d-3a07a292e0ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a520d4b-4d19-49db-81ea-72c638426a03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "625c8f20-f9c9-44b9-8158-e9bf44111d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a520d4b-4d19-49db-81ea-72c638426a03",
                    "LayerId": "42ecf1c6-f73c-45cd-a161-81fff957069a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "42ecf1c6-f73c-45cd-a161-81fff957069a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cf2b3d5-9638-40f4-ab7b-d8264ab7fe03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}