{
    "id": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fontLarge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "afa8e26d-2f5d-4e80-bacb-3cd4ccff50d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0388b0c5-1bf8-4e1c-9fbf-51d0349246e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "9b65be17-f3b6-4fd8-8df5-bf52ae713aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a837ee12-96a0-475b-a28f-8785b012cb21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "98c60c96-71ae-4150-8b94-b2634a711687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a837ee12-96a0-475b-a28f-8785b012cb21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b1b6b3-1d79-4314-9c3d-bddcb6c65eb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a837ee12-96a0-475b-a28f-8785b012cb21",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e1be3b50-74df-49ad-87fc-c5a717a017fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a837ee12-96a0-475b-a28f-8785b012cb21",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b95e2175-b01c-4c45-bb8d-faae7c886d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08ea7f0-cbda-4ade-9b85-c435c03810d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "794cf225-c2bc-4232-8b36-3281a3df39e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "362e1614-b346-4b20-b895-4876faa2512b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "df81891b-a24c-470c-b685-7927b6581c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362e1614-b346-4b20-b895-4876faa2512b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e304e443-f352-4c19-987b-403b193937ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362e1614-b346-4b20-b895-4876faa2512b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d5e4fda2-dc49-495b-86d9-058ed267b840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362e1614-b346-4b20-b895-4876faa2512b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "271c2af2-92bd-4e3f-a8e3-5d4616008a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7160c764-548f-4aca-8d49-3ec9f254247b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "30676ba4-582f-483e-9b8b-73de16ea2e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9c09a75e-0056-4d8e-a8ea-8bd69cd13401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4dc9d9-7ad8-4f74-9721-8b41aa9592b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "3c0017f3-2a52-4773-aef1-70ef0d7e3371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9399f2a9-dad2-4185-8997-c4c902dfd7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f7374cc-da83-4739-974d-d20bebb1f40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d57ac1a9-288a-477f-b9d6-417bc9b8f4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "6dfa9b5f-dfb2-4098-8aea-5e87da87ef0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed6bc43-0a00-4f6d-808b-327507e49a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "7f319ac0-04b7-4ffd-9829-d51696625b3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "35bcd022-5ee0-455a-ab45-4035fdb69164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "996fc92f-ae98-4704-8bc6-da8a22c74363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35bcd022-5ee0-455a-ab45-4035fdb69164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40714127-5e81-437a-af38-389e353f3e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bcd022-5ee0-455a-ab45-4035fdb69164",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "535e422b-f5df-468e-ab1a-c8565d50dd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bcd022-5ee0-455a-ab45-4035fdb69164",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f76b9b49-ace0-4e6d-a755-78efd391304f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "be05406b-0838-4bad-b5cd-ea8992510aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76b9b49-ace0-4e6d-a755-78efd391304f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4944a6f-a7f8-4eb2-bbec-074d6324fcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76b9b49-ace0-4e6d-a755-78efd391304f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "8d061eaa-408e-4748-8a26-1be7ddba9e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76b9b49-ace0-4e6d-a755-78efd391304f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b7396498-8c62-4342-8557-3c2c424f59ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f31118-b3d0-41d0-b00f-df4e06ed3df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "94665646-eb3f-48af-b87d-0b25b6de2407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e38b9e27-cc9d-4d04-bd18-c33bca18a99c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d2dab9f-3fc7-42a0-a29e-a4ecfda0267e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "0795e54c-3ce7-4672-aa35-31123335c0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c144a520-3267-42f8-97ba-182d0f198d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "440b9263-4f59-4e9c-a867-1fc0a690b1af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c144a520-3267-42f8-97ba-182d0f198d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ccada93-2e68-4267-80ae-f0f5ac6a8eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c144a520-3267-42f8-97ba-182d0f198d31",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "6c6ca52d-4b77-476c-81e9-f1e92d430143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c144a520-3267-42f8-97ba-182d0f198d31",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c91a5ed1-3bad-429a-908c-e78d646d5649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f22fae3c-c3aa-4e61-a7aa-a9c37e478713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91a5ed1-3bad-429a-908c-e78d646d5649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37310c2b-b2f0-4fed-bb12-10757cdd0f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91a5ed1-3bad-429a-908c-e78d646d5649",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "71620b74-090b-492c-9fc1-f99ad95bb16a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91a5ed1-3bad-429a-908c-e78d646d5649",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "45c36b6a-831b-4618-975a-d348eabe8aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9390ad13-3e4e-4469-9a11-09bedf3fc9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c36b6a-831b-4618-975a-d348eabe8aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf98708-5cb9-45b2-9c73-8bb8ce04e8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c36b6a-831b-4618-975a-d348eabe8aa6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e45f5820-1324-42ec-8dda-feb48c8e81d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c36b6a-831b-4618-975a-d348eabe8aa6",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8389e2b9-7759-43b6-8477-d89d8df11776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "82cb701e-e346-4ec3-b11c-b1ff6edd180a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8389e2b9-7759-43b6-8477-d89d8df11776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5473c01-046c-4685-8b93-1cfbe12fa5c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8389e2b9-7759-43b6-8477-d89d8df11776",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "9789b19b-8c88-408d-a232-622d72c9337c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8389e2b9-7759-43b6-8477-d89d8df11776",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "87fd8ba8-2df0-464a-a923-1d30e53fc63d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c3d1d9-4393-49d0-ac3a-d849f4c5015a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "decd855e-82e3-4769-8901-c694e93b0bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ee49b1b7-f2c0-4173-be49-1ce1e50436aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b320249-b869-4cdf-8ff2-cad53c3478fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "f923b1c5-19d9-4f19-8851-318295f9e4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1101be79-3e98-4a97-8c03-6b0d2ae748c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9945320c-aad8-43e8-8add-da2c48a6d496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "3a528394-a884-4a7f-9982-a13499b0ba98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "878b4584-0896-4893-8bbd-66398acb35af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1307e09-58ff-4351-bd8f-6a1369406755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "92a11527-79f3-4611-b33d-2c163132dec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e916d7d8-3576-49cf-851c-9858bb46357c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "344ac833-4a5c-44f6-af3d-1c3d6eb19f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e916d7d8-3576-49cf-851c-9858bb46357c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b1b1b6-c981-4a0a-bec5-b0f75707805a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e916d7d8-3576-49cf-851c-9858bb46357c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "46e96cdc-31f8-4681-81c6-e7963b8cb723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e916d7d8-3576-49cf-851c-9858bb46357c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "13193fab-4995-4ca8-a7df-5facb2986f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68769296-a310-4968-ac8b-2ee724c515a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "07f06b74-df94-4a08-b415-7edd047a30bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "66fa7325-d991-4369-963e-24df01292e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d6d14281-abcd-43e6-817d-5930b1f5f04a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fa7325-d991-4369-963e-24df01292e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "938f0ae4-d1b3-464a-a6bc-80437dd308c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fa7325-d991-4369-963e-24df01292e08",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e31fc4ac-ab4a-4bf4-960a-7f8f4ae0ab85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fa7325-d991-4369-963e-24df01292e08",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "63227d54-8b3f-4b00-979b-3542c8461d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ec176cd6-ed72-4e71-a55d-e73c54543d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63227d54-8b3f-4b00-979b-3542c8461d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07cbc1e3-141b-4478-a418-d9d360286807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63227d54-8b3f-4b00-979b-3542c8461d4b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "280c4cf5-8ab4-4cb2-9dc9-6a060952c079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63227d54-8b3f-4b00-979b-3542c8461d4b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "65558847-b4aa-4669-8f86-263bf07f3e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85495c8f-1ffb-4c6a-9f11-c0116a8210d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "cdc57618-be09-4c0f-ac60-565dd5cc9eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "adefa744-dadf-430f-b5e1-e72b547305d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70501c66-76f8-4c32-a2fe-c0b4a1cb5268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "31d25c7f-a338-49b3-be65-609697f61496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "bd3de32f-6cd2-4b90-8cef-5c5ba5477854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e050fc8-ea0d-43a9-a1cc-8804b5cd4381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b420201b-0493-4c62-80d6-2c4bfb142848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f01ba076-6033-4335-953d-eff8e3f82058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded5b7fe-c4e6-47b2-80e1-23f61881b2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "f124d4c5-2b8c-4025-b5cc-cffb4618838d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "72b97c5d-be8f-4a7f-bff7-82916665264b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7c1c2f15-bad3-4d04-9cf2-b93067d0d07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72b97c5d-be8f-4a7f-bff7-82916665264b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d93577a4-ccd0-471b-bc35-bc0fee30d27e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b97c5d-be8f-4a7f-bff7-82916665264b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "23f3ef37-6da9-47c5-83d5-bfb18f9b30a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b97c5d-be8f-4a7f-bff7-82916665264b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "bc7dec1e-d9ce-4854-a813-32218e4c5427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb8b0e6-d26d-4614-b582-83a36e780d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e0b7fc0c-0813-47d8-b54e-afc1bae14812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d2f4bb65-aaf1-43e7-a8b6-91a50050b9c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d3be4a-975c-4c91-a540-dbe28abc62c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "396cd661-06e0-4120-aa15-aeedf7e738f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e4f6a643-145a-4899-b5de-8d278a39b448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7eee978-d4f9-4efc-816c-77c3f5c76cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "34905791-cbce-40e8-a524-1378e6ec6903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9338ef49-bf61-4cce-bdb3-9ba67d2ba472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8364a69-9576-4b36-ac55-4ed43ed4cb8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "bd07d5d7-6422-4707-a715-afd70a3ef6b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "874cf113-24e6-4725-b19c-70784b17ddcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b53f8911-ad31-4c6f-a790-f8b4a9d21218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874cf113-24e6-4725-b19c-70784b17ddcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea783973-140a-4703-9266-15d41955f8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874cf113-24e6-4725-b19c-70784b17ddcb",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "1f7e3136-ec30-405e-aae9-35e6d9440677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874cf113-24e6-4725-b19c-70784b17ddcb",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "6504f926-689f-453d-abaf-2a5e9e9d6160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "bc798851-35a3-432c-97ba-057587599944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6504f926-689f-453d-abaf-2a5e9e9d6160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b583e6-301a-4373-8f5d-1eed3aabe5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6504f926-689f-453d-abaf-2a5e9e9d6160",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "15ca6d31-b693-4f49-a821-3624fef2bf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6504f926-689f-453d-abaf-2a5e9e9d6160",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "949a2782-b44a-496b-9c4d-f7f1cf861379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "24a362ed-5332-469c-949f-961f33450979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949a2782-b44a-496b-9c4d-f7f1cf861379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c7372f-6fa5-4a9d-b014-8a39daf585e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949a2782-b44a-496b-9c4d-f7f1cf861379",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "ac6eb7a2-62a7-4c91-a8bb-1a9e0fcea244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949a2782-b44a-496b-9c4d-f7f1cf861379",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "148684f2-2fb5-4399-8567-1f03a7f84a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca0647a-d2b7-4315-94e6-bd612e705244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "879c8471-e368-4360-8740-1b97e57e260c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "89248728-c99d-447d-b6f7-a5044eae490b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "99414337-0a16-444c-83a8-955e80e9467b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89248728-c99d-447d-b6f7-a5044eae490b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ae269e-48ae-411d-8882-eb6c453389e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89248728-c99d-447d-b6f7-a5044eae490b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "25925dbd-7c23-41ca-a711-87aac7f3d8e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89248728-c99d-447d-b6f7-a5044eae490b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "49acab48-77a7-406c-9379-eca923bed749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "161df8c1-79c8-44c9-9fec-d26f20d6c185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49acab48-77a7-406c-9379-eca923bed749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928be747-a593-4118-9614-dab470fba158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49acab48-77a7-406c-9379-eca923bed749",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "41fd20f1-bd55-480e-969a-ae4515a26566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49acab48-77a7-406c-9379-eca923bed749",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "42822745-5135-4a9a-976b-33d61b0694f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e36a633b-ddbf-4bd6-a975-b6ef5cc1ca57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42822745-5135-4a9a-976b-33d61b0694f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0433a209-3975-4650-aee5-258cf97d5b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42822745-5135-4a9a-976b-33d61b0694f0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e23ea613-3c61-4457-bb83-e779e8132426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42822745-5135-4a9a-976b-33d61b0694f0",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f9f4789b-1461-44bd-a442-f2c49e86b125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c367ca-0426-4ddc-9f62-01ea5da19482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "c25450e3-400e-4fc5-a995-aa8721a46920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "8043ec9e-ec63-4203-8971-b001723aca33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5f7234-8420-468c-a81b-e7d95c1ea68a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "cf547595-a26a-4189-b0b9-27a0de813d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "47aa7f55-695f-4dce-a79f-9cc37ff346d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b9ba85-5db6-42f5-b755-d484106f9cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "09b7e810-9813-4a87-a534-e0883a372d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "97f1c75e-31a0-43c2-abdb-27deb56989d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3218689f-ffb4-4376-a402-8d4deb3fc370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "535efb35-79bc-482e-852d-ffd50f52cc82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "dc54520f-0680-4902-9331-a037ba72a499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce1596e-f8b8-48dc-be69-6d66f42a7991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "06a7a4d5-f759-43fa-8160-fd400f377130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b9385cbb-b8e2-42f5-a834-1512a04ec88e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a678098-e725-4bf8-bea1-d4a72d87637e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "2490cf68-b074-447a-bbb9-887bad0c1953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "657c6fdb-febd-4565-a6bd-ffbee3357417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a96deb-ac8b-46f4-900e-90a61babef06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "57c35fab-effa-40b5-9b40-1d365e1fc560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "38ad37ca-df4f-4848-906a-496f76936235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "6a239f81-cb0f-4c87-9c6e-acb32f1b5e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ad37ca-df4f-4848-906a-496f76936235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee40c47-51a3-480e-8922-89a365596bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ad37ca-df4f-4848-906a-496f76936235",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e51fc811-f5c6-43cf-aa8a-dea32c23fca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ad37ca-df4f-4848-906a-496f76936235",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "5d333f4f-3f40-4471-839f-0276a293ebda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "76771879-abb4-4ceb-88d8-df29e6d2e13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d333f4f-3f40-4471-839f-0276a293ebda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ef7e628-a542-4388-8603-a3a7ece9382d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d333f4f-3f40-4471-839f-0276a293ebda",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "cf2a357b-719a-4c09-8942-139e8d8ec51e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d333f4f-3f40-4471-839f-0276a293ebda",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "4c6b0124-74d8-4e07-b8f7-803d726fd8d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061aeac7-9678-47dd-b523-21c382918406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "c3225fca-8d8c-41b2-8934-1c4b06b506c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "19863bed-ca24-444d-8878-2817cf1fe625",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2580aa14-a2cc-4ca8-b61b-7aa1e84b23a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b8d81b73-ea48-40fb-8dd7-9eba27ca31cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7669ac4a-41b0-4aea-907b-963c26f1f360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b6aeb7-f2d4-4dfd-ac06-77b154e2d445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "46b3818f-b7d3-4739-afb3-f39fea252094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "abe7bf06-1658-49de-8498-8646b4b63fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d20a381c-3014-4538-aa74-3dbef136e5dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe7bf06-1658-49de-8498-8646b4b63fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c28801f5-b8cc-4ff3-a2fd-7f7a0a7c332d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe7bf06-1658-49de-8498-8646b4b63fa6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "c30d0488-ddb1-496e-ac44-4e6e487af593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe7bf06-1658-49de-8498-8646b4b63fa6",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "abbf6b96-59d5-4487-b3b2-de9bb9fcfe8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59624008-61f7-4d7b-aa53-2f23f6e94975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "03160cb9-ef40-456c-a185-6fb59a71fe27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ac2884ca-d670-4532-a48b-76d19649cd5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7da04d-24ba-483d-9ce1-b467cff864c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "9eb5e6a9-021a-4e99-87e0-9700aa623721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "15fbbd04-e0e7-46a6-be88-03f90672e932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "653e0ca5-680a-4361-a272-64715361d122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "125eac08-6619-44b9-842f-dc18f063d452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1750bc38-1710-4b35-b726-f8b2dd257bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "912b7bf5-5434-4db7-af14-e4e995d5bf2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "6f14b466-258b-4138-ac1f-de0324824320",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "ac848319-1a38-4234-b50b-600c1ec72730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "215f5aef-f98b-4943-b30f-60b53429235c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac848319-1a38-4234-b50b-600c1ec72730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a45c4e-e34c-455b-bca3-c6383a7bb975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac848319-1a38-4234-b50b-600c1ec72730",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "ea98e1b1-09f2-4ad1-b755-9f5c269e0e07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac848319-1a38-4234-b50b-600c1ec72730",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "93bd620d-6696-4609-9b93-2a29f98aae91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "498a9dca-8315-4c89-b146-657ad88104fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93bd620d-6696-4609-9b93-2a29f98aae91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b86b0e-286f-4f94-9141-4710ccd5c0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bd620d-6696-4609-9b93-2a29f98aae91",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "68e346e5-506c-41b4-8a9a-318186a9f07c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bd620d-6696-4609-9b93-2a29f98aae91",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "604a67f7-2921-4cb2-8562-ab2fda1b30ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3f6f94-95e6-4ee6-8e99-2edbc688e474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "bdc87299-1ab2-4542-b80c-34720d60f548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "475870bb-0ce8-41f3-9376-2671e28bda6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "64a7d9b9-0a05-4c52-a813-63b53da4eff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475870bb-0ce8-41f3-9376-2671e28bda6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b992c5-34ac-4117-9206-f6f332570ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475870bb-0ce8-41f3-9376-2671e28bda6c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "266e12fb-65af-4a9c-b9a3-b2d2acee4c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475870bb-0ce8-41f3-9376-2671e28bda6c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "926e6485-80f5-43f3-80e9-ea90377e4a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf28688-3f85-441a-9dfd-29cf13b20fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "122c7dd1-3cd7-41ad-8241-1e06e64612bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "3e08624b-7f45-4a70-923a-9341fa3818d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ae72bb67-7335-48e7-885c-eb7a7232b89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e08624b-7f45-4a70-923a-9341fa3818d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d35af7a-98fc-4b56-b542-623ebc982083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e08624b-7f45-4a70-923a-9341fa3818d3",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "ae43656e-1019-4d61-942f-8d2254167b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e08624b-7f45-4a70-923a-9341fa3818d3",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "fd3454bb-fce6-4497-bd6a-dd24d138d8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8ef369-d221-4b4a-8886-b6ef9733a875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "ea529499-fc62-4b5b-826b-089c6e299ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ae773464-7e2a-415e-af7f-afa51a2645c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a023ef-3a77-488a-8a83-f0fd2981cb7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "21f14749-1772-4f69-9789-6c14d37fe11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d1b4ce2e-f334-450d-b5d5-d736d4caebe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1124f66f-46c3-4a72-b415-ca7b193ab9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "8b80c877-ffc0-4558-b5da-bc1a0195075c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a36bf504-f103-4cb1-89ac-322e81004804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "044edf02-a256-4d37-8611-e4dd4ad4be57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36bf504-f103-4cb1-89ac-322e81004804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9980189a-5087-4ebb-bd6d-51ecdc0ce37f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36bf504-f103-4cb1-89ac-322e81004804",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "09499577-d621-43f9-8304-5b988f8213d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36bf504-f103-4cb1-89ac-322e81004804",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "6ca0d132-d655-4b22-995f-03f52c5768af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ee0f3ebd-67b2-45e6-8643-6889127bb851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca0d132-d655-4b22-995f-03f52c5768af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59c28a6-0cee-4232-bec2-c1718e914aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca0d132-d655-4b22-995f-03f52c5768af",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "30da47cf-ad6a-4d5d-9f19-02896a2b68a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca0d132-d655-4b22-995f-03f52c5768af",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "d9fbf6d0-127c-44df-8a94-b062e795131f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ff339706-8e92-4d8f-ad98-6cd0e37bad91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9fbf6d0-127c-44df-8a94-b062e795131f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6741d6e6-ed53-4075-abe1-1a8dff723ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9fbf6d0-127c-44df-8a94-b062e795131f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e8934915-cf8f-457c-aba1-a5f7efd544bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9fbf6d0-127c-44df-8a94-b062e795131f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "12a3add5-daa9-4512-9b26-c6bb0629662b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "c9d10542-cf1f-4d4a-a68b-10d63da47a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12a3add5-daa9-4512-9b26-c6bb0629662b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e30d581b-7c54-48f3-9466-18909004f169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12a3add5-daa9-4512-9b26-c6bb0629662b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d201539f-5419-4c8f-9b2a-26f630260152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12a3add5-daa9-4512-9b26-c6bb0629662b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1ab04ab8-ae4d-427f-bc27-be7a438aafa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a6707d4-aa56-4ab7-a1d1-aeb9c4c459eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "bc462ce3-400c-4ada-88ed-b06ea8f117ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e48a82d5-cfc6-4607-9f5f-b3bc4c798764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9dd7d6-13c6-4cee-8fc0-3d96ba360e74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "f5edee83-253f-44e5-9842-034b2c314870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "dc511ca5-12bb-4fb6-88a5-22cf0edbe58c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1543290-de68-4f1a-9b21-67cede6f5e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "527df758-c05f-4114-85ce-133f8d865918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "217e16db-7d91-40a4-9df2-8d0043f9ce22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf1ff24-a71e-45b7-898d-6c25bca0c055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "847f3b54-8790-4f7e-8583-e779ced6ff49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "26892c83-0bba-4e76-ba27-021694cd58fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "73c7ec39-8348-470a-8d0d-d8ff4e739cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26892c83-0bba-4e76-ba27-021694cd58fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8881cd-f14d-4949-a85a-433b31525be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26892c83-0bba-4e76-ba27-021694cd58fa",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e40348cd-da31-4d91-842c-18ac708be7c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26892c83-0bba-4e76-ba27-021694cd58fa",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "8e54e858-915c-4868-8832-ca3f8a796999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7371d56f-9293-463c-a797-e3e99b823c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "76e8e9f8-1f4e-457f-8043-590e26e982d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "04181ce5-7444-463f-91f1-47dd882475e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "06e875f8-c37f-4536-8a34-9257975b3abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04181ce5-7444-463f-91f1-47dd882475e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf26064-e7c0-4ead-99c9-194c0eb95d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04181ce5-7444-463f-91f1-47dd882475e1",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "2297339a-4afc-4364-a17a-578838c12c1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04181ce5-7444-463f-91f1-47dd882475e1",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "75fcd19d-e680-4bb1-a16b-d441fc377634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "88faf19a-6692-424e-bd33-6b4be0594258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75fcd19d-e680-4bb1-a16b-d441fc377634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b84457-ec09-40fd-8e8a-f39c8e996692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75fcd19d-e680-4bb1-a16b-d441fc377634",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "1a986fbe-1a4b-4c97-985c-0d7896a4facb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75fcd19d-e680-4bb1-a16b-d441fc377634",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ebe4e4a1-dd02-497f-87a2-597c62828f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa4e49c3-e431-436f-a120-d9452ec6e373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "a8557552-ead1-4166-a3a4-7c83f56a89fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "d8341155-f1e7-4511-9d16-403712a58a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e79e40c1-7fc3-44f1-b3d6-c14cccfa3a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8341155-f1e7-4511-9d16-403712a58a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a172a07-416a-40b0-8247-600371744df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8341155-f1e7-4511-9d16-403712a58a7b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "01d9f8bf-c125-46ba-ae9a-28ccec890340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8341155-f1e7-4511-9d16-403712a58a7b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "dd72dfcc-f606-41b2-be11-f0d87270b895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "41834685-18b5-4e48-98df-cabfcf183301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd72dfcc-f606-41b2-be11-f0d87270b895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab710c41-f68f-4824-9b1e-9505ed6c8cf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd72dfcc-f606-41b2-be11-f0d87270b895",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "a5e18e18-a342-480c-bb89-34658843581c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd72dfcc-f606-41b2-be11-f0d87270b895",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "6ea6737f-5a1f-4a8f-b96e-b64a50e4a56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45a66db-1534-4a8a-baff-d6b4a6d2c23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "3b819806-9bf6-46f7-9511-21afc5755d57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e879ad83-a521-459b-adfe-933f4a790e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f8f8ea-428a-4f76-ac46-59f24a73d816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "2321b46a-7fce-4813-bdb6-db3976eaeea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "389817e1-3ff3-4e4f-8640-c204125862d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "5c5b2a15-c3f9-4e12-addc-772652f304dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389817e1-3ff3-4e4f-8640-c204125862d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3273bf1b-be0f-401e-aa0f-1af86f6cd86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389817e1-3ff3-4e4f-8640-c204125862d8",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "a6c2d74f-1bdd-415c-8376-66082b67b242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389817e1-3ff3-4e4f-8640-c204125862d8",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "2a7ae38e-2176-4a38-9c05-07eca6b389a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d42feb77-2b7e-48fe-bb5e-0db421bd074d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e2c99bef-1725-462c-b37e-1dfee3b9593e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c842efb6-8049-4a2c-8b86-08736e9413a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "cd282e8e-ded5-4623-a22d-942334565a8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c842efb6-8049-4a2c-8b86-08736e9413a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae10de47-5ff3-4295-a21d-b3c786da1761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c842efb6-8049-4a2c-8b86-08736e9413a6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "faedb575-6e47-40fe-88f0-eca74beb6317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c842efb6-8049-4a2c-8b86-08736e9413a6",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "487f0659-7ae9-4eec-8081-909ddc0d074c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7434b60b-676a-479e-96d4-1ba5de6e78e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "487f0659-7ae9-4eec-8081-909ddc0d074c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32c5aa3e-74a6-445e-8bd7-e57c4381f0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487f0659-7ae9-4eec-8081-909ddc0d074c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "376cd836-0407-4da0-a11b-4d08f13424e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "487f0659-7ae9-4eec-8081-909ddc0d074c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "32d59833-cbd0-4292-a0a9-23643a32a9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "2716d0b0-1ce5-4460-8677-0fbd132f46f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32d59833-cbd0-4292-a0a9-23643a32a9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261fca2f-749e-470a-a1cf-65ee45286951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d59833-cbd0-4292-a0a9-23643a32a9c4",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "52aa6bca-d7c8-44f2-a1ba-31221d963a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d59833-cbd0-4292-a0a9-23643a32a9c4",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "50c059c4-1b94-477a-aaf1-4591470b231f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1725e889-6363-48aa-96cd-447b3253997b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c059c4-1b94-477a-aaf1-4591470b231f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0c8bce-a2ad-40ea-aa42-a073afd8df78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c059c4-1b94-477a-aaf1-4591470b231f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "47191e10-20cf-40e9-89f1-ab11c3ee91a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c059c4-1b94-477a-aaf1-4591470b231f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "b15df472-1b87-4aaa-9b1c-a478e5bebe5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "373b6944-f390-43c9-a9fd-7aca42952cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b15df472-1b87-4aaa-9b1c-a478e5bebe5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5db5606f-7485-4681-b3a8-00ff62423484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15df472-1b87-4aaa-9b1c-a478e5bebe5c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "16742d6e-8a04-4c69-9b9e-c2d850892257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15df472-1b87-4aaa-9b1c-a478e5bebe5c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "24471671-df9c-4230-8208-b4dec22fde2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "258c091b-fddf-4af0-9efd-ae2d7eba44dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24471671-df9c-4230-8208-b4dec22fde2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f1e6af-101e-48d5-aa28-fc48cb9b6dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24471671-df9c-4230-8208-b4dec22fde2f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "4b978b29-895f-40e1-af6d-12b286e3318a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24471671-df9c-4230-8208-b4dec22fde2f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "50fdddae-0451-4b69-a7b9-4ca776f3443f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "913c1185-1f84-4600-9218-c90abe70a5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50fdddae-0451-4b69-a7b9-4ca776f3443f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b6d0c3-9129-4efb-b6d4-595c1ec34573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50fdddae-0451-4b69-a7b9-4ca776f3443f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "42485953-aa79-48c7-a2a3-24a561f41709",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50fdddae-0451-4b69-a7b9-4ca776f3443f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c2ab0cae-f12b-4fc5-bb81-d670735dee80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "a5e5cb66-88b4-427d-9ceb-def150600e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2ab0cae-f12b-4fc5-bb81-d670735dee80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "560524c5-0cf7-4901-ab3b-543a1a32cc21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2ab0cae-f12b-4fc5-bb81-d670735dee80",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "dfb03c7d-b658-4874-a418-adb1ea67f53f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2ab0cae-f12b-4fc5-bb81-d670735dee80",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "fb80b364-1185-41db-9145-4ac5bbe1348b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "a422f005-0587-44ad-8618-52fb76da9945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb80b364-1185-41db-9145-4ac5bbe1348b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adbddcd9-5ce2-4a22-a623-ad102fb35d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb80b364-1185-41db-9145-4ac5bbe1348b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "a9b337b6-92aa-42bf-a14a-2c85ad9ceef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb80b364-1185-41db-9145-4ac5bbe1348b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "6334a335-67ba-406f-b8c0-6e8193e1d721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "0964035f-6816-4681-b871-668d58df3afc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6334a335-67ba-406f-b8c0-6e8193e1d721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eba7e3c-6c7b-40b8-acb4-ab3587602a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6334a335-67ba-406f-b8c0-6e8193e1d721",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "bf7f9476-d51b-4026-8478-7671c3a0f8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6334a335-67ba-406f-b8c0-6e8193e1d721",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a54ca8bd-edd2-4d45-83c7-0ed42642cbe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b94ad0be-e157-46da-88b2-03d2da158c8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a54ca8bd-edd2-4d45-83c7-0ed42642cbe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7f1351-4cf6-41aa-be26-0ae64bdf9eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a54ca8bd-edd2-4d45-83c7-0ed42642cbe7",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "340565cc-13bd-4e99-8fbb-310e1068d528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a54ca8bd-edd2-4d45-83c7-0ed42642cbe7",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "fda3d1eb-7766-4963-9022-0cdb009db86b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "10812e29-0ea9-42a5-b680-0f7b9603452d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda3d1eb-7766-4963-9022-0cdb009db86b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b0eb40-e67a-4586-bf61-e41bd779a5bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda3d1eb-7766-4963-9022-0cdb009db86b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e4b0d6a1-be43-4c22-8776-4f451228d1b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda3d1eb-7766-4963-9022-0cdb009db86b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "4bded1cd-e1aa-4fa5-b240-dcb05a05e3a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9c423ef6-c648-4548-a80d-5601f7505a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bded1cd-e1aa-4fa5-b240-dcb05a05e3a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55698a83-4749-4d8d-b1cc-3e242d6831cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bded1cd-e1aa-4fa5-b240-dcb05a05e3a8",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "fa9e855d-80db-4fee-85cc-31a8063741df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bded1cd-e1aa-4fa5-b240-dcb05a05e3a8",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "82ccae27-1b71-430a-bfd4-1236aad5f709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1d6710bb-46ca-4872-b86f-1bc113b87f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ccae27-1b71-430a-bfd4-1236aad5f709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8674ccf8-05c7-40eb-b6a1-ef5c653c81d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ccae27-1b71-430a-bfd4-1236aad5f709",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "a798c7ae-9732-426d-abaf-1b5331d98d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ccae27-1b71-430a-bfd4-1236aad5f709",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "93e71019-c61b-479b-9d01-6ccd346be416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "698a478d-3ebd-4d2a-97b6-e7d0ee4192e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e71019-c61b-479b-9d01-6ccd346be416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a1e0ce-dbb0-4a48-a530-14200e35792d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e71019-c61b-479b-9d01-6ccd346be416",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "6056cc92-cf26-439a-ac03-bc6bf7203004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e71019-c61b-479b-9d01-6ccd346be416",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "1ec7f671-6d9e-4be5-8838-b4c11251f4a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9dc4f303-27db-4833-b49d-1513b731b355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec7f671-6d9e-4be5-8838-b4c11251f4a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d442fdf5-49ee-41f3-b51a-9a11a86a9147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec7f671-6d9e-4be5-8838-b4c11251f4a8",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "cc0207a9-6f57-4e44-8fa4-c215166d7914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec7f671-6d9e-4be5-8838-b4c11251f4a8",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "d1c4a049-926f-4182-8bb2-b87f477a7556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "24f99184-287e-445e-810e-73eeadfb46c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c4a049-926f-4182-8bb2-b87f477a7556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c773d654-331e-451d-b986-b6fbb8c93f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c4a049-926f-4182-8bb2-b87f477a7556",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d2a0dc51-acda-447e-9a23-c097093d4aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c4a049-926f-4182-8bb2-b87f477a7556",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "78e9de43-5e11-4c8e-a3bb-8130e050fabc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "00182295-379c-48be-80fa-ff07b5a5e79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78e9de43-5e11-4c8e-a3bb-8130e050fabc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "459f644a-c0c5-415d-8d78-58ccfe0ced2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e9de43-5e11-4c8e-a3bb-8130e050fabc",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "028e2b7d-2732-44fd-97f3-42831fc2eb20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78e9de43-5e11-4c8e-a3bb-8130e050fabc",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "25129294-c0ea-4d9e-b549-49a0e2e736e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ab9aaf25-0904-46ff-ac08-aeba8a8a8cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25129294-c0ea-4d9e-b549-49a0e2e736e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4feb19e-9bf4-46b1-9f80-ffea86c793a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25129294-c0ea-4d9e-b549-49a0e2e736e4",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "4d610423-fb75-45d9-b3fa-d39ab3cdfdc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25129294-c0ea-4d9e-b549-49a0e2e736e4",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "b31fcc10-f99b-4800-92c5-cf0831ff26b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f67e9315-3386-4ce4-85a1-40285f5f880c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31fcc10-f99b-4800-92c5-cf0831ff26b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836dafc8-caa4-417c-9860-99773364caf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31fcc10-f99b-4800-92c5-cf0831ff26b0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "846fcffd-c834-4462-91e0-d0d58389dcff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31fcc10-f99b-4800-92c5-cf0831ff26b0",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "4334e829-cc37-4417-87fd-ffd049b04759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "45f93640-685d-48f5-9b87-6f6906d65494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4334e829-cc37-4417-87fd-ffd049b04759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fde7f52-7842-41dd-aff7-906e8863d6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4334e829-cc37-4417-87fd-ffd049b04759",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "10502fc7-37ce-4791-96a8-955b932c90b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4334e829-cc37-4417-87fd-ffd049b04759",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "9a9cef03-2837-4770-bf60-23c721e6b3c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d0754fcf-eb79-40e8-b10e-e62c1a81382d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9cef03-2837-4770-bf60-23c721e6b3c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b5b603c-d2c7-4b05-b87f-4f9112bc4ec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9cef03-2837-4770-bf60-23c721e6b3c1",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b2f519a2-b07a-419e-bee2-d6f9bf78a931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9cef03-2837-4770-bf60-23c721e6b3c1",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e0746cc3-c3a6-4a2c-95a1-7cef786f863d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "eeb73b46-7915-4760-b2d4-d56630934c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0746cc3-c3a6-4a2c-95a1-7cef786f863d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "105cd5c3-8ce7-4961-aca6-72db6100dd17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0746cc3-c3a6-4a2c-95a1-7cef786f863d",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "8a25dc16-8aa6-40fb-8344-e1983cda6e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0746cc3-c3a6-4a2c-95a1-7cef786f863d",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0375679c-bf50-4934-8bf7-f47589654b53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "da11e80e-b217-4a3c-8311-b552acadcf97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0375679c-bf50-4934-8bf7-f47589654b53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f081eb63-73de-4321-88c9-24c892e397a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0375679c-bf50-4934-8bf7-f47589654b53",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b8febe83-f7bc-4d3b-b34e-f5f64550741b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0375679c-bf50-4934-8bf7-f47589654b53",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "31eefb8a-1d10-4055-83c8-d98692b84796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "638d1373-7939-4637-8b59-33b9551e4e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31eefb8a-1d10-4055-83c8-d98692b84796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24ddd4d-6d30-4fde-b08d-2a362a189d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31eefb8a-1d10-4055-83c8-d98692b84796",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "51af4187-8863-4fc4-a423-92aa177dcf34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31eefb8a-1d10-4055-83c8-d98692b84796",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "966f6be2-d4a8-41b1-8dde-4540079d591f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d87a2e4a-d6b5-4e43-90e4-70321109d4b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "966f6be2-d4a8-41b1-8dde-4540079d591f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5255e4-1ecc-4d4c-a409-7ca0bee08269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966f6be2-d4a8-41b1-8dde-4540079d591f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "3b656634-79d7-40a8-a507-27479d788201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966f6be2-d4a8-41b1-8dde-4540079d591f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8e93dd52-a63b-4cc5-ae0e-159783c9e7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "590bb284-eed5-422a-8133-6b4e6e6a94a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e93dd52-a63b-4cc5-ae0e-159783c9e7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9ac93a-3b10-4981-be62-698e82c786e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e93dd52-a63b-4cc5-ae0e-159783c9e7d6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "17e94f1d-7878-4591-8ff1-0d5ff99b947e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e93dd52-a63b-4cc5-ae0e-159783c9e7d6",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "53c73392-ddf8-4d7a-98c1-8342ae9c1032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b2f7e081-ce47-483d-b40f-0e2009509c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53c73392-ddf8-4d7a-98c1-8342ae9c1032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687a4de4-5fb5-4e7e-bb6e-48ced579d2e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53c73392-ddf8-4d7a-98c1-8342ae9c1032",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b4e05d60-9c5f-40c7-bd63-8f2768d2f3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53c73392-ddf8-4d7a-98c1-8342ae9c1032",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e9b08d53-e3d7-4eaf-9491-57e6aca964da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "a6344833-ea63-4308-a2fa-893bd615d4c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b08d53-e3d7-4eaf-9491-57e6aca964da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "565fa630-2a55-4289-90a8-e89a9e2d1534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b08d53-e3d7-4eaf-9491-57e6aca964da",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "25737746-7cec-493c-82d0-7f32eb39c55e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b08d53-e3d7-4eaf-9491-57e6aca964da",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0704b81a-5114-43e4-a3cf-8211a5f10a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "c936d7df-057c-4801-a863-9505baf94cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0704b81a-5114-43e4-a3cf-8211a5f10a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00525542-8e2a-4f54-b40c-901befa940ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0704b81a-5114-43e4-a3cf-8211a5f10a18",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "8b530f34-b983-42c5-89a2-af7d46651ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0704b81a-5114-43e4-a3cf-8211a5f10a18",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "47846d96-7828-4347-a882-2612f8760f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7fc1907e-bbaf-40dd-a96a-bff484e5fbde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47846d96-7828-4347-a882-2612f8760f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051abf4a-2339-41d4-b733-bd100c198ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47846d96-7828-4347-a882-2612f8760f14",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "89d55ea5-773c-4b0f-afcd-e27f155dd496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47846d96-7828-4347-a882-2612f8760f14",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e53eebff-227e-4292-a96a-0aeaa366ccce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "77ac8038-0614-41d8-8f1c-7e05e0516805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e53eebff-227e-4292-a96a-0aeaa366ccce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b70b81-c533-4c92-b4e0-fb97796ef269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e53eebff-227e-4292-a96a-0aeaa366ccce",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "f526b508-b661-4b80-8193-10a746ef4d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e53eebff-227e-4292-a96a-0aeaa366ccce",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "af3819b5-1d34-449c-846b-25615141c13f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e3742fbc-59b1-41d2-a173-531a7e8a935c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3819b5-1d34-449c-846b-25615141c13f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb14df92-586f-42ba-a699-689b5118a4f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3819b5-1d34-449c-846b-25615141c13f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "48a0ce14-8fb6-4215-a4f4-4a6f9f0e0606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3819b5-1d34-449c-846b-25615141c13f",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c6d74a7c-0984-4d28-bd4b-64ade3e71a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "a1b1fb09-8b0e-4ca6-ae86-f403eb106fd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d74a7c-0984-4d28-bd4b-64ade3e71a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc0bc8f-6485-463f-886c-ab311d8762e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d74a7c-0984-4d28-bd4b-64ade3e71a8c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "26f4b35a-579b-4850-b9c1-ab15ad896757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d74a7c-0984-4d28-bd4b-64ade3e71a8c",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "b50ad09d-ffe1-4ab7-9f5a-2affccb62c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d3b86a83-52c8-432a-b947-abce5b1b7f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50ad09d-ffe1-4ab7-9f5a-2affccb62c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8357f7cd-557c-4830-8b8f-1d4e8bf9147f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50ad09d-ffe1-4ab7-9f5a-2affccb62c98",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d82eff16-4ec1-426f-96c0-b58da1f07415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50ad09d-ffe1-4ab7-9f5a-2affccb62c98",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a24a654a-9d4a-4031-9a28-16353b07021a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "602dc001-853a-4b31-ba3b-9225679da485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a24a654a-9d4a-4031-9a28-16353b07021a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c3a1d3-3bbd-4d31-a34e-5e6b67ef490d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a24a654a-9d4a-4031-9a28-16353b07021a",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "27880d1e-1b53-47f9-87c5-4fe32d778ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a24a654a-9d4a-4031-9a28-16353b07021a",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f35022b8-a526-40f9-9442-4d8978c9c1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7b5061e6-51db-4db1-8ea4-89d260acf02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f35022b8-a526-40f9-9442-4d8978c9c1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fc5b2b3-238d-4eb1-ad95-2211baafe4bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f35022b8-a526-40f9-9442-4d8978c9c1a9",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "bc10c455-fea0-4262-adf7-7e49d0b49b79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f35022b8-a526-40f9-9442-4d8978c9c1a9",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "e9c5a4f7-b003-46c5-ac23-68e986c2fab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7dae8131-319e-443f-9b95-32585e36f245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c5a4f7-b003-46c5-ac23-68e986c2fab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa3c6a0c-ccf3-4336-be95-62fb88daf314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c5a4f7-b003-46c5-ac23-68e986c2fab7",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "e3650db1-c055-418b-92b3-afed963e3903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c5a4f7-b003-46c5-ac23-68e986c2fab7",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c05c29e7-b457-4800-a05f-775fe4fee83e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "0ad200a5-3809-4121-8d28-eca3771cbcca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05c29e7-b457-4800-a05f-775fe4fee83e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f33557c-0ff6-4804-bb46-44987e8da43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05c29e7-b457-4800-a05f-775fe4fee83e",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "47486b32-ada0-4116-9925-61e9ac92880c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05c29e7-b457-4800-a05f-775fe4fee83e",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "f4290398-6171-47e3-94b5-de597a7fbf4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "56e73ee8-db98-48d3-9046-a3f2a2efc42c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4290398-6171-47e3-94b5-de597a7fbf4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90161574-7da1-4502-997b-41e36e903e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4290398-6171-47e3-94b5-de597a7fbf4d",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b03101ae-83d4-4767-8a1b-159c4a66c974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4290398-6171-47e3-94b5-de597a7fbf4d",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "264f904c-b688-4c36-84e8-c0be475a2b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ae6f4503-7b67-4779-ba6d-e8c2e18646cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "264f904c-b688-4c36-84e8-c0be475a2b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ad1c74-6dee-47d5-a526-f66a2f47368d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264f904c-b688-4c36-84e8-c0be475a2b75",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d9683e38-4a3c-4015-bd95-8e111b070eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264f904c-b688-4c36-84e8-c0be475a2b75",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a5846bb3-d6a7-4cd8-8362-bb9a1d0a0cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "c78f3f90-3cac-44ec-98b4-f4a09c79d7ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5846bb3-d6a7-4cd8-8362-bb9a1d0a0cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a848820-3320-49d2-b4bf-a9306743b6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5846bb3-d6a7-4cd8-8362-bb9a1d0a0cfd",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "00b51c22-cc4d-4978-b4d6-b671e4ca8fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5846bb3-d6a7-4cd8-8362-bb9a1d0a0cfd",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "3e12fb4f-f878-4933-a98e-f5bca84c2f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "93a30f3d-65a6-46b4-ab69-e265af5742e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e12fb4f-f878-4933-a98e-f5bca84c2f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d433d17-36b4-4769-987e-1632e035b02a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e12fb4f-f878-4933-a98e-f5bca84c2f31",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d3020140-6807-4c59-b3df-a5c7036bed1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e12fb4f-f878-4933-a98e-f5bca84c2f31",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "224260dd-583a-405c-af65-878669000592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "2fb63ff9-ffba-417d-b916-9f085c7727ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224260dd-583a-405c-af65-878669000592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6085edaa-98fc-42cb-978f-4739ec87ad1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224260dd-583a-405c-af65-878669000592",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "67b51fcc-188d-45fc-b15c-8aeb0bb2f9e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224260dd-583a-405c-af65-878669000592",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c2d2c543-a55b-4007-b8c4-d3362285808d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "a5ea361a-1989-4f0a-b4ef-88be3ac4182a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d2c543-a55b-4007-b8c4-d3362285808d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af3229c-671c-4bc1-85d8-088493e7903d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d2c543-a55b-4007-b8c4-d3362285808d",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "eb98623a-ac2e-484a-9b4b-9475505031f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d2c543-a55b-4007-b8c4-d3362285808d",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a00c150e-ebff-4675-9dcf-091816ba2d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "42b3b176-f454-44df-b377-1c2ab69f7579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00c150e-ebff-4675-9dcf-091816ba2d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80b158f-1f2a-42f2-8823-83eb26f81a7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00c150e-ebff-4675-9dcf-091816ba2d9a",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "79a551df-5dc7-42fd-ad75-dc8d1ce599cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00c150e-ebff-4675-9dcf-091816ba2d9a",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "ea93afd8-f82b-4547-a0b4-e12393316a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "0cec3f66-172f-4808-9064-1d86ebb70d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea93afd8-f82b-4547-a0b4-e12393316a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2c48df-9d7c-4c59-921b-65fbd9c8f83f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea93afd8-f82b-4547-a0b4-e12393316a27",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "335c12ff-9f8c-4bee-99bb-33cf7e97fb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea93afd8-f82b-4547-a0b4-e12393316a27",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "3d0d67e3-dd62-49b5-b942-4f7913278e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "82c564bc-0674-444e-8559-81b6d2738993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0d67e3-dd62-49b5-b942-4f7913278e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc369e6a-c4c7-4ed4-b380-34ae81f8c364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0d67e3-dd62-49b5-b942-4f7913278e31",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "d3707c0e-6595-4a90-9de0-57772add3427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0d67e3-dd62-49b5-b942-4f7913278e31",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "1c8fa083-9497-4aa0-b02e-cc7fa71400ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "315f484b-0b6d-4539-97a7-814bbf3e8613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8fa083-9497-4aa0-b02e-cc7fa71400ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f2326f8-c0ec-4eee-90b4-8a3a99156503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8fa083-9497-4aa0-b02e-cc7fa71400ac",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "c86ec1ff-12b8-4310-9c7f-03d32d513177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8fa083-9497-4aa0-b02e-cc7fa71400ac",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "4fc81ca3-23c7-4c43-91a7-cbd9500083fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "feb8936c-a321-4752-aaaf-e4ec99acbb5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fc81ca3-23c7-4c43-91a7-cbd9500083fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a6affbe-d207-4409-931d-a15117e509d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc81ca3-23c7-4c43-91a7-cbd9500083fd",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b66123f2-8f1e-42ee-a534-977ae5446a33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc81ca3-23c7-4c43-91a7-cbd9500083fd",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "a7d36643-b203-4f46-9b79-9e91b3b51674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d4c45665-de42-449b-b207-04d167eab110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d36643-b203-4f46-9b79-9e91b3b51674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f475836-0779-49db-9dbd-ac1993376dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d36643-b203-4f46-9b79-9e91b3b51674",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "1c83c726-1531-4b34-9e17-e1c1d64a9799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d36643-b203-4f46-9b79-9e91b3b51674",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "82b94558-c0ff-4d4a-af34-604ffeca4f17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "fae93860-90f3-41ab-bbb2-055f45ac5cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b94558-c0ff-4d4a-af34-604ffeca4f17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eec800c4-c9eb-4220-ba26-6b24edc6de6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b94558-c0ff-4d4a-af34-604ffeca4f17",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "9a5b2b74-2bfd-45e2-9276-cc3a0454dab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b94558-c0ff-4d4a-af34-604ffeca4f17",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "98197188-9640-48b9-9e02-7617be14b481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9780527f-36de-4ce8-9ddf-1442bdb4c60a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98197188-9640-48b9-9e02-7617be14b481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c1a5e0b-4dbe-45e0-b69c-ff2abdd2e97f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98197188-9640-48b9-9e02-7617be14b481",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "741688b3-d0ec-4cc7-8490-15dd6d4f607d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98197188-9640-48b9-9e02-7617be14b481",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0766766d-3cd6-4eeb-88d3-47a176f1b400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f807faf9-c710-48dc-b32d-40879d160638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0766766d-3cd6-4eeb-88d3-47a176f1b400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a130bfb2-832d-406b-9b0d-0bf48ebe3198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0766766d-3cd6-4eeb-88d3-47a176f1b400",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "5ebdb215-f2a8-4de9-9c3d-23d249f9877b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0766766d-3cd6-4eeb-88d3-47a176f1b400",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "07386a58-1a08-4de8-aaa3-c3d506c81650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "2ac16541-696d-4675-91b3-bbc3ed1cc6b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07386a58-1a08-4de8-aaa3-c3d506c81650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8587ae8e-6e54-48f5-a55c-ce8342fad2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07386a58-1a08-4de8-aaa3-c3d506c81650",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "c0e59f62-5a26-4f6b-af53-7254c3b39501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07386a58-1a08-4de8-aaa3-c3d506c81650",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "c740f59a-1dfe-4e60-a37e-2963828af3d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "915a3794-6594-4ddc-b205-6947c722dfec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c740f59a-1dfe-4e60-a37e-2963828af3d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43f3dff-9729-41b1-ba88-8ff516b8ced5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c740f59a-1dfe-4e60-a37e-2963828af3d3",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "eed9966f-db52-449c-8abd-fe7b70455ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c740f59a-1dfe-4e60-a37e-2963828af3d3",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8920f8df-0b2a-4fbb-ac07-701308148e34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b35ea79f-a3b2-4b1d-9433-275531593134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8920f8df-0b2a-4fbb-ac07-701308148e34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac56e4b-9e70-4dd3-ba4a-703c7dd026a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8920f8df-0b2a-4fbb-ac07-701308148e34",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "0c47212f-370d-4f68-b73c-7bcf683cc956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8920f8df-0b2a-4fbb-ac07-701308148e34",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "3807405d-8f92-4bd6-8a0f-60a8d33a28e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "900ba285-e223-437d-8b67-f100c7a032ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3807405d-8f92-4bd6-8a0f-60a8d33a28e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcbb9092-8459-43ec-967d-59d731f6c0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3807405d-8f92-4bd6-8a0f-60a8d33a28e2",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b6ae1bba-c743-4539-baf5-f2f1e5b616c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3807405d-8f92-4bd6-8a0f-60a8d33a28e2",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "13b65710-c4c5-41d9-b3fb-edcd3ae7e0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "81e2b16a-57ca-4df8-b4b5-207b8e31dd7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13b65710-c4c5-41d9-b3fb-edcd3ae7e0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4196b67c-eacc-4a9e-b4b6-510ea6572f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b65710-c4c5-41d9-b3fb-edcd3ae7e0f6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "16b5f9fe-266b-41e3-bd9b-7e40f015878e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b65710-c4c5-41d9-b3fb-edcd3ae7e0f6",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "0b6bd37b-9bdc-428e-8731-ee8d1dc1d620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "c282be4e-db78-488b-a9bc-3a679fd0b7cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6bd37b-9bdc-428e-8731-ee8d1dc1d620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6fb99e0-0511-44be-b711-20c01b5db831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6bd37b-9bdc-428e-8731-ee8d1dc1d620",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "6bd09b47-44fd-40c3-b184-4aca7c21822c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6bd37b-9bdc-428e-8731-ee8d1dc1d620",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "1ac0bcf7-51ba-466a-98eb-e34aa22b432d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "378a4c38-4193-4dc3-938f-511b4ab53428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac0bcf7-51ba-466a-98eb-e34aa22b432d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae063cb6-6438-4bd2-a225-f41693c00f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac0bcf7-51ba-466a-98eb-e34aa22b432d",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "2cdf1b4e-c283-41ac-b32a-c724aa90d287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac0bcf7-51ba-466a-98eb-e34aa22b432d",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "7a2a3215-7a7f-47e0-8470-aeb223713f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "df199991-7324-4dbb-844e-4ccb3f198e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2a3215-7a7f-47e0-8470-aeb223713f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "227cd72c-7455-4d29-993d-722e7826ad5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2a3215-7a7f-47e0-8470-aeb223713f8b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "b00f47d9-7a5c-4a9f-b5f1-6fdd3434fe64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2a3215-7a7f-47e0-8470-aeb223713f8b",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "8d479be6-941b-4184-9d48-3e9ed8273f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d8eee0e0-0019-4c51-adfd-4f96362692fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d479be6-941b-4184-9d48-3e9ed8273f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbda0368-eedb-4723-9d34-48d93d697fe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d479be6-941b-4184-9d48-3e9ed8273f69",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "ff3cccaf-d275-42f7-9a5d-10c7aacd0940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d479be6-941b-4184-9d48-3e9ed8273f69",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        },
        {
            "id": "6bcec8b2-dc84-4785-b655-378e7b79d851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f698512d-009e-4052-8669-b9dc1f3389b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bcec8b2-dc84-4785-b655-378e7b79d851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ffa84b-9e1e-4cdd-a234-83c44b8678e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bcec8b2-dc84-4785-b655-378e7b79d851",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                },
                {
                    "id": "cc644299-994d-4a31-875a-e5d47dbdfef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bcec8b2-dc84-4785-b655-378e7b79d851",
                    "LayerId": "36eba7db-b4c5-41e9-9241-260e32771afb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "36eba7db-b4c5-41e9-9241-260e32771afb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2) (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "16b8bc86-31a6-42ac-aeab-86247216000d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 47,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}