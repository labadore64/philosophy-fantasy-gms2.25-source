{
    "id": "32ab6b4b-1078-46aa-87d3-877eef6d0ed5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_starterdeck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 439,
    "bbox_left": 0,
    "bbox_right": 324,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03a51547-fdd8-4be2-807d-4e52ee462aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ab6b4b-1078-46aa-87d3-877eef6d0ed5",
            "compositeImage": {
                "id": "502ef58f-2b19-4883-9d92-21113854822f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a51547-fdd8-4be2-807d-4e52ee462aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f14372-6e78-43ed-b355-e5a0e0a04f12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a51547-fdd8-4be2-807d-4e52ee462aac",
                    "LayerId": "0ccfa20a-64a1-41b7-8c3f-ce4fd306c439"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 440,
    "layers": [
        {
            "id": "0ccfa20a-64a1-41b7-8c3f-ce4fd306c439",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32ab6b4b-1078-46aa-87d3-877eef6d0ed5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 325,
    "xorig": 0,
    "yorig": 0
}