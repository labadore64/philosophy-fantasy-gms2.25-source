{
    "id": "645369b5-cee6-4a80-9f3b-bc4e0d44d411",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 314,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff46d8b7-7c1d-4896-9fb4-2b3f2eaa056f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645369b5-cee6-4a80-9f3b-bc4e0d44d411",
            "compositeImage": {
                "id": "8820b72f-b471-4636-bf48-16294e931047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff46d8b7-7c1d-4896-9fb4-2b3f2eaa056f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7ebdc5-1daf-4f92-9c44-4de519ad34f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff46d8b7-7c1d-4896-9fb4-2b3f2eaa056f",
                    "LayerId": "f6942a74-b4f7-412b-83a8-d059684b089c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "f6942a74-b4f7-412b-83a8-d059684b089c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "645369b5-cee6-4a80-9f3b-bc4e0d44d411",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}