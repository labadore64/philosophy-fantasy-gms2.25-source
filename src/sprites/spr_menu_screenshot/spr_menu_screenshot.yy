{
    "id": "c1532089-a377-48ad-839c-cea36417a310",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_screenshot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 3,
    "bbox_right": 35,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7a113f5-1fbd-4ce2-b9ac-d53752a5ccba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1532089-a377-48ad-839c-cea36417a310",
            "compositeImage": {
                "id": "c22e0d18-67e2-4c58-abfa-0f4221b2c574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a113f5-1fbd-4ce2-b9ac-d53752a5ccba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d08cb4-266b-4823-b21d-af2a8dd6b639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a113f5-1fbd-4ce2-b9ac-d53752a5ccba",
                    "LayerId": "2793fc05-d38b-4c10-bca5-43dc22062f2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2793fc05-d38b-4c10-bca5-43dc22062f2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1532089-a377-48ad-839c-cea36417a310",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}