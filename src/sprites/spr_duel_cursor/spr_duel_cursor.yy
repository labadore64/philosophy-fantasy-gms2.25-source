{
    "id": "43e81ae4-efad-4555-a558-b550caaf4475",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_duel_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 10,
    "bbox_right": 86,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13ad30e7-ff32-49c0-a33b-494606f29090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43e81ae4-efad-4555-a558-b550caaf4475",
            "compositeImage": {
                "id": "7ddf50c7-c167-4200-a00a-957ae011ca36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ad30e7-ff32-49c0-a33b-494606f29090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04d83f0d-911a-45ce-835e-5c574513f5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ad30e7-ff32-49c0-a33b-494606f29090",
                    "LayerId": "d412d0e3-24a4-4edf-bf78-2128aa793fbc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d412d0e3-24a4-4edf-bf78-2128aa793fbc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43e81ae4-efad-4555-a558-b550caaf4475",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 86,
    "yorig": 36
}