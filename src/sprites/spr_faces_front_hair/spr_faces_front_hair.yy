{
    "id": "b752211b-6e4f-41fb-901a-300bd221739c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_front_hair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e5d9672-ff3d-4ff2-b98a-13a142c3753e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b752211b-6e4f-41fb-901a-300bd221739c",
            "compositeImage": {
                "id": "a6db8a11-08d3-4cfc-9571-117ea6ae60a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5d9672-ff3d-4ff2-b98a-13a142c3753e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f8a5d1-926d-4296-94d3-4a0982dfd95a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5d9672-ff3d-4ff2-b98a-13a142c3753e",
                    "LayerId": "fd5e0de2-b325-4f40-96e9-47fe87876927"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "fd5e0de2-b325-4f40-96e9-47fe87876927",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b752211b-6e4f-41fb-901a-300bd221739c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}