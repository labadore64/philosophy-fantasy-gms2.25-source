{
    "id": "f4ffc89f-bfa3-4fa4-996f-896362389fed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_john",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 86,
    "bbox_right": 461,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2975ebae-19eb-4ec6-91e5-1a3243d94f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ffc89f-bfa3-4fa4-996f-896362389fed",
            "compositeImage": {
                "id": "7e549262-70bc-4ae0-a010-1f0e0ae0a61a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2975ebae-19eb-4ec6-91e5-1a3243d94f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f6ee96-7d8e-4282-b436-0bea56d4021d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2975ebae-19eb-4ec6-91e5-1a3243d94f50",
                    "LayerId": "87d41b6e-4de9-4af7-a077-b75c700a589d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "87d41b6e-4de9-4af7-a077-b75c700a589d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4ffc89f-bfa3-4fa4-996f-896362389fed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}