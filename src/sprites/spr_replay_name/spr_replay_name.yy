{
    "id": "081d6d67-4440-43d9-ac70-cd2b00830a56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_replay_name",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 82,
    "bbox_right": 445,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1464272b-b499-419a-bd08-b7ed68f66e47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "081d6d67-4440-43d9-ac70-cd2b00830a56",
            "compositeImage": {
                "id": "6c41362e-952f-4f62-8681-2aad99fa6e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1464272b-b499-419a-bd08-b7ed68f66e47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44b36df6-4531-4a26-8a38-cd0e5d6a5a6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1464272b-b499-419a-bd08-b7ed68f66e47",
                    "LayerId": "8fec6d96-cf72-460a-af40-6a860749d91f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8fec6d96-cf72-460a-af40-6a860749d91f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "081d6d67-4440-43d9-ac70-cd2b00830a56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.23275,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 64
}