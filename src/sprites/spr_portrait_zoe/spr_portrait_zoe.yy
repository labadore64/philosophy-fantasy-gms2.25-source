{
    "id": "b502f0b7-a834-4fb6-a0ed-7ee48afe5b38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_zoe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 60,
    "bbox_right": 486,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc270380-da0b-417f-90ea-1fe22c56106c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b502f0b7-a834-4fb6-a0ed-7ee48afe5b38",
            "compositeImage": {
                "id": "1b2077ac-d7ca-4185-8468-67d029941828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc270380-da0b-417f-90ea-1fe22c56106c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99a47f2-c005-4125-a218-ea2e2f7fb8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc270380-da0b-417f-90ea-1fe22c56106c",
                    "LayerId": "207044d9-4b93-4794-8ae6-da9f9fe51a45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "207044d9-4b93-4794-8ae6-da9f9fe51a45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b502f0b7-a834-4fb6-a0ed-7ee48afe5b38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}