{
    "id": "6283961b-8121-4296-b840-51f296b759c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_options",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89809df9-bf38-46a2-8dce-da09e5e4cbec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6283961b-8121-4296-b840-51f296b759c8",
            "compositeImage": {
                "id": "0736c1ee-5964-4c5f-9674-096dd63cdd59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89809df9-bf38-46a2-8dce-da09e5e4cbec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c85ef64-2f09-4779-81c4-b8dcfa9ec236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89809df9-bf38-46a2-8dce-da09e5e4cbec",
                    "LayerId": "ac470e27-ab21-477b-9df1-4f597a9660a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "ac470e27-ab21-477b-9df1-4f597a9660a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6283961b-8121-4296-b840-51f296b759c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}