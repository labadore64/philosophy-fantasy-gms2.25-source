{
    "id": "d018cba1-7869-491e-a6a8-b86450a6bc1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_point_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32b06e72-a527-4c78-9135-4f3e1ec85c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d018cba1-7869-491e-a6a8-b86450a6bc1f",
            "compositeImage": {
                "id": "ec06f210-62c9-4cb0-bb39-c818d394a8e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b06e72-a527-4c78-9135-4f3e1ec85c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d596d7-b9b8-49e2-a849-f4c0211df05b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b06e72-a527-4c78-9135-4f3e1ec85c32",
                    "LayerId": "89534c6c-7981-48b7-9f69-b3ea76e4f256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "89534c6c-7981-48b7-9f69-b3ea76e4f256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d018cba1-7869-491e-a6a8-b86450a6bc1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 59
}