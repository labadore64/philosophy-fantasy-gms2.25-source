{
    "id": "e9ff8cbe-b863-4e1e-8461-0f8e0c101257",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dialog_philosopher",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01073cc5-28de-461a-9c79-646301faa9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ff8cbe-b863-4e1e-8461-0f8e0c101257",
            "compositeImage": {
                "id": "d812f4c2-f7f4-46fd-b076-30be7787f97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01073cc5-28de-461a-9c79-646301faa9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9630be0b-2fec-40ad-a80f-c04b5f0c3b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01073cc5-28de-461a-9c79-646301faa9c3",
                    "LayerId": "17af06cc-587f-463c-a2cf-1dbdbe54dc36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "17af06cc-587f-463c-a2cf-1dbdbe54dc36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9ff8cbe-b863-4e1e-8461-0f8e0c101257",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}