{
    "id": "c92f31ee-a8de-4933-bc51-b8d07cf27b1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_replay_vs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 367,
    "bbox_left": 166,
    "bbox_right": 371,
    "bbox_top": 155,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ca4135f-a663-4017-90da-ef657a675422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c92f31ee-a8de-4933-bc51-b8d07cf27b1c",
            "compositeImage": {
                "id": "bd30b6f8-46fa-49fd-b7e7-a00a5a9a977e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca4135f-a663-4017-90da-ef657a675422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c016168-4f2f-46de-86e1-1e7773ced06e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca4135f-a663-4017-90da-ef657a675422",
                    "LayerId": "abbbdf03-6569-4e75-8f3c-2bd40ffc209f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "abbbdf03-6569-4e75-8f3c-2bd40ffc209f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c92f31ee-a8de-4933-bc51-b8d07cf27b1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}