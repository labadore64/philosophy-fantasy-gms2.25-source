{
    "id": "e35b22bb-dffc-4222-9f35-15edb4332b3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_nose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 300,
    "bbox_left": 188,
    "bbox_right": 208,
    "bbox_top": 276,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4664b6a2-e374-42a3-968c-f5c5a34d7a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e35b22bb-dffc-4222-9f35-15edb4332b3f",
            "compositeImage": {
                "id": "ce8c3a55-8929-4c00-bd17-275dae95b63a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4664b6a2-e374-42a3-968c-f5c5a34d7a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb4d79c5-8f87-407a-a240-13b95ef83f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4664b6a2-e374-42a3-968c-f5c5a34d7a41",
                    "LayerId": "29eb51f9-fd1e-4c5f-89d8-f0383c8f4dcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "29eb51f9-fd1e-4c5f-89d8-f0383c8f4dcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e35b22bb-dffc-4222-9f35-15edb4332b3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}