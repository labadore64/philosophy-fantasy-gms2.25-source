{
    "id": "9f66d59a-b66e-4b77-a8c6-d0914db27501",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_img_ravens",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 141,
    "bbox_right": 360,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0882766a-7c8a-4e10-9026-ed18360c2142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f66d59a-b66e-4b77-a8c6-d0914db27501",
            "compositeImage": {
                "id": "9152757b-9eef-443e-b595-95f3d91faee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0882766a-7c8a-4e10-9026-ed18360c2142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dffd1073-abe6-4853-9ff3-fedbd0db362a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0882766a-7c8a-4e10-9026-ed18360c2142",
                    "LayerId": "a1ee366f-9d9c-4392-a442-50e606545ead"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "a1ee366f-9d9c-4392-a442-50e606545ead",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f66d59a-b66e-4b77-a8c6-d0914db27501",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 250,
    "yorig": 260
}