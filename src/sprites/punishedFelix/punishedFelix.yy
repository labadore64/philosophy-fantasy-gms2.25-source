{
    "id": "5a3ba457-39c7-40db-a119-152f6c0d75e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "punishedFelix",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 419,
    "bbox_left": 188,
    "bbox_right": 630,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d71c681c-5d69-4d8d-a08b-55ff8fae0bfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a3ba457-39c7-40db-a119-152f6c0d75e7",
            "compositeImage": {
                "id": "a1dd1da4-f065-4e59-b328-93709974da55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71c681c-5d69-4d8d-a08b-55ff8fae0bfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7f39864-143c-4d6b-9fd9-2912705aa0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71c681c-5d69-4d8d-a08b-55ff8fae0bfc",
                    "LayerId": "d375d7c7-7434-43fa-ac64-fbcad384ba7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "d375d7c7-7434-43fa-ac64-fbcad384ba7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a3ba457-39c7-40db-a119-152f6c0d75e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}