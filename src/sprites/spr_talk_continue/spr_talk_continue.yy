{
    "id": "ea1bdb1b-4384-4610-80ff-26c8d51d18a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_talk_continue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 12,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8c90a8c-9ab8-47af-ac42-7cf90644fd47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1bdb1b-4384-4610-80ff-26c8d51d18a6",
            "compositeImage": {
                "id": "52c5a4f7-514e-4eda-998f-f2e97cf57a3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8c90a8c-9ab8-47af-ac42-7cf90644fd47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c74aa5-910e-4dad-a6aa-4748d4977582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8c90a8c-9ab8-47af-ac42-7cf90644fd47",
                    "LayerId": "91fe66fe-8a83-4553-9c5a-5ac70e1872ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "91fe66fe-8a83-4553-9c5a-5ac70e1872ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea1bdb1b-4384-4610-80ff-26c8d51d18a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}