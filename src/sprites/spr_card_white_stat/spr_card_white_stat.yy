{
    "id": "5ea1d8aa-321f-4f3b-a75d-642a8dbf8464",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_white_stat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6bbc76e-df9a-466b-aba5-681e855ac1c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ea1d8aa-321f-4f3b-a75d-642a8dbf8464",
            "compositeImage": {
                "id": "16e0ca20-e2f3-4896-ad16-524ee19b3cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6bbc76e-df9a-466b-aba5-681e855ac1c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e944c7-3f1c-4df3-93ac-f515f7d0173e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6bbc76e-df9a-466b-aba5-681e855ac1c2",
                    "LayerId": "7add5ae7-2ea8-46b7-96a8-96b02bf025b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7add5ae7-2ea8-46b7-96a8-96b02bf025b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ea1d8aa-321f-4f3b-a75d-642a8dbf8464",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}