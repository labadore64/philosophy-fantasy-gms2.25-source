{
    "id": "66620bf4-fa93-4346-8ee6-33bcf5bd48b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typeSpirit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd17b632-3deb-451c-97e9-3b79e43688d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66620bf4-fa93-4346-8ee6-33bcf5bd48b7",
            "compositeImage": {
                "id": "b1451aa2-b06c-40cd-a5e0-4fb25a375940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd17b632-3deb-451c-97e9-3b79e43688d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f452d32c-db12-42a9-bf3d-41507f7a9a6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd17b632-3deb-451c-97e9-3b79e43688d8",
                    "LayerId": "c7d251e2-11c4-477c-93b6-1b2656905bef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7d251e2-11c4-477c-93b6-1b2656905bef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66620bf4-fa93-4346-8ee6-33bcf5bd48b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}