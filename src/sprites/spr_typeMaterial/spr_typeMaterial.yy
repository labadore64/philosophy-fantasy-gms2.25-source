{
    "id": "6d312a1d-dfc9-47ee-a88f-89ae1d773c33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typeMaterial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e5bd1ea-abcc-4b84-93f1-8caa3eb029cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d312a1d-dfc9-47ee-a88f-89ae1d773c33",
            "compositeImage": {
                "id": "fc2e72e7-0814-45b5-8051-b0862d7aadb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e5bd1ea-abcc-4b84-93f1-8caa3eb029cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f1ab436-5d38-4fde-9d9e-59cad2bb0f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e5bd1ea-abcc-4b84-93f1-8caa3eb029cd",
                    "LayerId": "e6686cd9-0b10-45f4-81d6-7cd0a12a3b92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e6686cd9-0b10-45f4-81d6-7cd0a12a3b92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d312a1d-dfc9-47ee-a88f-89ae1d773c33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}