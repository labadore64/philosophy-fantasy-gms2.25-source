{
    "id": "3e55e505-b934-4ed6-884a-8042a403bf2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_back_hair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34c546aa-14c8-47a9-a4a9-d5d1a570bb1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e55e505-b934-4ed6-884a-8042a403bf2e",
            "compositeImage": {
                "id": "e33fc5fb-f5b0-437a-9649-c8c670f50abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34c546aa-14c8-47a9-a4a9-d5d1a570bb1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f369f7-215e-4499-89c2-6b4c58780182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34c546aa-14c8-47a9-a4a9-d5d1a570bb1d",
                    "LayerId": "ef92ecc5-e08f-4798-bf78-29e89fd9af63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "ef92ecc5-e08f-4798-bf78-29e89fd9af63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e55e505-b934-4ed6-884a-8042a403bf2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}