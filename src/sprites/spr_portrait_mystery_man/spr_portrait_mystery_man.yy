{
    "id": "2f3f1515-31eb-4f8e-9215-c42b42c707b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_mystery_man",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 74,
    "bbox_right": 399,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44b94d1c-d7da-4879-a964-f29e92c3e31e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3f1515-31eb-4f8e-9215-c42b42c707b2",
            "compositeImage": {
                "id": "b171672b-6bae-4bd7-876f-d1dd10436a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b94d1c-d7da-4879-a964-f29e92c3e31e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb75a23e-82ce-414f-b485-33d562088740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b94d1c-d7da-4879-a964-f29e92c3e31e",
                    "LayerId": "c37c29a7-fca5-434a-81c3-120d6b741b7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c37c29a7-fca5-434a-81c3-120d6b741b7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f3f1515-31eb-4f8e-9215-c42b42c707b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}