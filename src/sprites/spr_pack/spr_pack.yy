{
    "id": "96bfcfed-1073-4e7f-b6d5-227656be2b44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6ea793c-2e73-44d0-85eb-d4ee03671cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96bfcfed-1073-4e7f-b6d5-227656be2b44",
            "compositeImage": {
                "id": "f33c9851-8a20-4596-a189-64d8995d4d63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ea793c-2e73-44d0-85eb-d4ee03671cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2762c082-d807-4716-93b4-a5b3b7fd4322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ea793c-2e73-44d0-85eb-d4ee03671cb3",
                    "LayerId": "fe650575-5bb9-4345-90d4-1c8389e5d864"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fe650575-5bb9-4345-90d4-1c8389e5d864",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96bfcfed-1073-4e7f-b6d5-227656be2b44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}