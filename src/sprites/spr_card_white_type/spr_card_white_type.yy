{
    "id": "b6e1567f-398d-4f90-a8f1-80ebf0bb5ff2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_white_type",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "694499ec-102d-4644-991f-0c797b267a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e1567f-398d-4f90-a8f1-80ebf0bb5ff2",
            "compositeImage": {
                "id": "77209e7e-283f-4d14-b95d-368a7753a915",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "694499ec-102d-4644-991f-0c797b267a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa7c019-b987-4112-99ef-1223c5632fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "694499ec-102d-4644-991f-0c797b267a75",
                    "LayerId": "e103d147-8c35-4cc2-925b-b05c620f0818"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e103d147-8c35-4cc2-925b-b05c620f0818",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6e1567f-398d-4f90-a8f1-80ebf0bb5ff2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}