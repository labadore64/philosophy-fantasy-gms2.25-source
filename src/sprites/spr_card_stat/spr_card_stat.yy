{
    "id": "cb17a24a-5cd3-4fc8-845d-a1726a24fd5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_stat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22f3d1a6-73cf-496d-8924-5e5f52c0d856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb17a24a-5cd3-4fc8-845d-a1726a24fd5b",
            "compositeImage": {
                "id": "fdf7d88c-b4c8-46a7-9e0b-a88850c79ce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f3d1a6-73cf-496d-8924-5e5f52c0d856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbc0201-fc9f-4251-9b0e-c1e6dfa13545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f3d1a6-73cf-496d-8924-5e5f52c0d856",
                    "LayerId": "fae25f9d-6f3b-410b-a937-67ec526d7007"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fae25f9d-6f3b-410b-a937-67ec526d7007",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb17a24a-5cd3-4fc8-845d-a1726a24fd5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}