{
    "id": "8ca5dea4-c083-4bd7-96be-37b5ec76a934",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_pigeon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 4,
    "bbox_right": 509,
    "bbox_top": 76,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6f8be98-1a10-4544-8a07-bfa539c2b09a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ca5dea4-c083-4bd7-96be-37b5ec76a934",
            "compositeImage": {
                "id": "3993fa26-9e54-4457-a20b-e5c99829f68a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f8be98-1a10-4544-8a07-bfa539c2b09a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "035e2903-5300-4431-8a10-fcee6fbc8152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f8be98-1a10-4544-8a07-bfa539c2b09a",
                    "LayerId": "4bc23ce4-63f8-41b4-9ea5-623903a4ade6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "4bc23ce4-63f8-41b4-9ea5-623903a4ade6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ca5dea4-c083-4bd7-96be-37b5ec76a934",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}