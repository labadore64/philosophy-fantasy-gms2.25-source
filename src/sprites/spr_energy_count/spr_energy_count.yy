{
    "id": "f443f14d-586b-4e1d-b6a4-306b4d9a47cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy_count",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d93ca0c0-e1d2-4956-9f04-489e1aa1efbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f443f14d-586b-4e1d-b6a4-306b4d9a47cc",
            "compositeImage": {
                "id": "c07ac1d7-8844-4b44-8911-f9e2ff7685e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93ca0c0-e1d2-4956-9f04-489e1aa1efbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82f7727d-ce3c-4f9a-a9a3-ece44ffd4276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93ca0c0-e1d2-4956-9f04-489e1aa1efbb",
                    "LayerId": "9f03e347-0617-43ae-95d3-3eac0f76301e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f03e347-0617-43ae-95d3-3eac0f76301e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f443f14d-586b-4e1d-b6a4-306b4d9a47cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}