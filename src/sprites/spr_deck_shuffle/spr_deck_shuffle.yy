{
    "id": "b27a6c39-5364-4678-a485-7d5712f16b8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_deck_shuffle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74c5649d-dd0e-4eff-9847-7e3e52a0848a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27a6c39-5364-4678-a485-7d5712f16b8a",
            "compositeImage": {
                "id": "b5d6defd-500c-478e-bb6f-2d8c49c8a3f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c5649d-dd0e-4eff-9847-7e3e52a0848a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66914a90-33c4-4311-a626-1128e2e1561f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c5649d-dd0e-4eff-9847-7e3e52a0848a",
                    "LayerId": "f57515f5-4669-4fa7-a312-42f3df2173a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "f57515f5-4669-4fa7-a312-42f3df2173a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b27a6c39-5364-4678-a485-7d5712f16b8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}