{
    "id": "aff9ad4d-18e7-4c23-bafa-75e5eeb4ff15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_roger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 36,
    "bbox_right": 495,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a82c1963-4ee8-463a-918d-9a1f8caa412e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aff9ad4d-18e7-4c23-bafa-75e5eeb4ff15",
            "compositeImage": {
                "id": "1593993f-3e00-4673-95cc-8da02e0eed2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a82c1963-4ee8-463a-918d-9a1f8caa412e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2fda9a-c76f-419b-a243-dc42db4e56e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a82c1963-4ee8-463a-918d-9a1f8caa412e",
                    "LayerId": "933bb4b9-e535-46dd-9b52-305f625aeb7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "933bb4b9-e535-46dd-9b52-305f625aeb7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aff9ad4d-18e7-4c23-bafa-75e5eeb4ff15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}