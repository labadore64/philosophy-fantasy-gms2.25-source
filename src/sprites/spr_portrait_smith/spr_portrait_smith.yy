{
    "id": "3453bed4-a3b7-4355-9023-9f46efaa2d4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_smith",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 68,
    "bbox_right": 444,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10574d81-af72-416c-9bc3-9f960702c096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3453bed4-a3b7-4355-9023-9f46efaa2d4d",
            "compositeImage": {
                "id": "561c1e48-85c3-4a6f-b51f-dc9b91a972a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10574d81-af72-416c-9bc3-9f960702c096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb741b6-ad49-4120-824f-cc9538b9fd06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10574d81-af72-416c-9bc3-9f960702c096",
                    "LayerId": "d4d19888-2818-484a-8a75-1451afa509d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "d4d19888-2818-484a-8a75-1451afa509d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3453bed4-a3b7-4355-9023-9f46efaa2d4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}