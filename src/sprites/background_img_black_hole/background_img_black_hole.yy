{
    "id": "fb99b2f6-c7ce-48af-b102-2b2d928598ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_img_black_hole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b696d23d-dfa9-473c-8f5f-b843367467f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb99b2f6-c7ce-48af-b102-2b2d928598ea",
            "compositeImage": {
                "id": "85dcbd82-5850-4c4c-b090-722404aa9229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b696d23d-dfa9-473c-8f5f-b843367467f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "625d85a0-2794-42b0-99da-07bbb98c43c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b696d23d-dfa9-473c-8f5f-b843367467f7",
                    "LayerId": "5ad16351-8a28-4f79-a22a-049f5359476c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "5ad16351-8a28-4f79-a22a-049f5359476c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb99b2f6-c7ce-48af-b102-2b2d928598ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.0547502,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}