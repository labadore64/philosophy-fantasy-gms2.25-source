{
    "id": "7229072a-64b9-47ee-90eb-22c29d42bf57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_effect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 398,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9f74396-f4e2-4813-9130-c3ab03640891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7229072a-64b9-47ee-90eb-22c29d42bf57",
            "compositeImage": {
                "id": "e8823225-51cf-4078-82dd-cf3498459ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f74396-f4e2-4813-9130-c3ab03640891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93b8922-56cf-4991-8c6f-30917bbd7378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f74396-f4e2-4813-9130-c3ab03640891",
                    "LayerId": "1fe5df38-81ab-483d-ae44-e1ce9ba24f6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "1fe5df38-81ab-483d-ae44-e1ce9ba24f6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7229072a-64b9-47ee-90eb-22c29d42bf57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}