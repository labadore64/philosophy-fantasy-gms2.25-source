{
    "id": "6d6823cb-eef7-42b2-b339-6804a0570f86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_cute",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e9c2709-e533-4558-b9c4-b6a248c09f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6823cb-eef7-42b2-b339-6804a0570f86",
            "compositeImage": {
                "id": "fc709882-e82c-452d-89dc-8bea5c2de1ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e9c2709-e533-4558-b9c4-b6a248c09f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5784f6-b35e-49c4-ac57-5aff5d023ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e9c2709-e533-4558-b9c4-b6a248c09f88",
                    "LayerId": "7e3d2c5b-21fc-4420-a1c4-32cd9ad5901d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "7e3d2c5b-21fc-4420-a1c4-32cd9ad5901d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d6823cb-eef7-42b2-b339-6804a0570f86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}