{
    "id": "c8e035c5-a6ba-46e4-b6d3-1623187f3d40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_card_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 398,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c5ac7e6-1b6e-4c97-8553-877758d26511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8e035c5-a6ba-46e4-b6d3-1623187f3d40",
            "compositeImage": {
                "id": "4b7fcf99-556b-45e5-94f6-897ef172b1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5ac7e6-1b6e-4c97-8553-877758d26511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73f83d06-7413-4842-a979-81c25aeb3ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5ac7e6-1b6e-4c97-8553-877758d26511",
                    "LayerId": "1874399b-d1fa-44ed-8618-555779ce9337"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "1874399b-d1fa-44ed-8618-555779ce9337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8e035c5-a6ba-46e4-b6d3-1623187f3d40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}