{
    "id": "99ce15ec-0e78-47bd-841e-dc7cc177b1ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 284,
    "bbox_left": 165,
    "bbox_right": 271,
    "bbox_top": 226,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cd25e69-5729-4896-88e3-db4e1c269f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99ce15ec-0e78-47bd-841e-dc7cc177b1ba",
            "compositeImage": {
                "id": "79a60d35-7774-4040-873e-506c683d597f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd25e69-5729-4896-88e3-db4e1c269f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ada553ac-9946-4386-aef6-cec94b0430e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd25e69-5729-4896-88e3-db4e1c269f09",
                    "LayerId": "075b5ac5-6ae3-46a9-a60e-a27d5103e72b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "075b5ac5-6ae3-46a9-a60e-a27d5103e72b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99ce15ec-0e78-47bd-841e-dc7cc177b1ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}