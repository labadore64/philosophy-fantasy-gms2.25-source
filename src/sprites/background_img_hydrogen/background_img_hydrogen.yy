{
    "id": "28f58055-8e0b-4fae-a80b-60a6a37f170b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_img_hydrogen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 503,
    "bbox_left": 23,
    "bbox_right": 498,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69d3ceb0-f5cc-4faf-af40-b851fe6e9a64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28f58055-8e0b-4fae-a80b-60a6a37f170b",
            "compositeImage": {
                "id": "8350f5f7-bc98-47b0-8cdb-0bb1a269aac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d3ceb0-f5cc-4faf-af40-b851fe6e9a64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489a6958-86b0-4dbb-8513-779a87e76a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d3ceb0-f5cc-4faf-af40-b851fe6e9a64",
                    "LayerId": "2e1bce26-deb2-4018-9495-cf6b20ca124a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2e1bce26-deb2-4018-9495-cf6b20ca124a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28f58055-8e0b-4fae-a80b-60a6a37f170b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}