{
    "id": "e27ff85d-f34c-4b99-953b-763e2924e8c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_bench",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 3,
    "bbox_right": 123,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b4e9be7-dae9-4fa0-b923-19c535102584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e27ff85d-f34c-4b99-953b-763e2924e8c2",
            "compositeImage": {
                "id": "28ca4fa4-25b1-4769-a7d8-7b88085a80fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4e9be7-dae9-4fa0-b923-19c535102584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "950b639a-143d-4f51-bc10-a1ffe38d4956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4e9be7-dae9-4fa0-b923-19c535102584",
                    "LayerId": "f6937a78-b38c-41cf-b171-5ec1ca7f8963"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f6937a78-b38c-41cf-b171-5ec1ca7f8963",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e27ff85d-f34c-4b99-953b-763e2924e8c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}