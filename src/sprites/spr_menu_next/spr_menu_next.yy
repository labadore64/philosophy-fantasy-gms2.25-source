{
    "id": "c6b99fb6-b895-4616-a02e-7c986a512829",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_next",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d34371dc-1765-42c8-ac03-1d78c7491a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b99fb6-b895-4616-a02e-7c986a512829",
            "compositeImage": {
                "id": "5f489422-f7f1-4efe-98ae-0885057a927e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d34371dc-1765-42c8-ac03-1d78c7491a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60256183-4d1b-45c0-840c-aeeab0000ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34371dc-1765-42c8-ac03-1d78c7491a96",
                    "LayerId": "2f40884f-e812-4f32-bd0b-3f0c1a6f88bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2f40884f-e812-4f32-bd0b-3f0c1a6f88bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6b99fb6-b895-4616-a02e-7c986a512829",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}