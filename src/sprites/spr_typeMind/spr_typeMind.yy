{
    "id": "76d75dd7-ff87-4620-a555-9cce333843b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typeMind",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75219082-0d6f-469a-ac03-1a66580b8952",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76d75dd7-ff87-4620-a555-9cce333843b5",
            "compositeImage": {
                "id": "492c1a61-cbe9-4076-ba61-d593d87d4d37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75219082-0d6f-469a-ac03-1a66580b8952",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c84c8ebc-d792-4e22-b5c5-d83c8403ff1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75219082-0d6f-469a-ac03-1a66580b8952",
                    "LayerId": "7226525b-f7eb-46d0-a00b-094c1a33fa4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7226525b-f7eb-46d0-a00b-094c1a33fa4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76d75dd7-ff87-4620-a555-9cce333843b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}