{
    "id": "6f467c08-b649-4f82-b9f4-139f8fc59600",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_mouth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 211,
    "bbox_right": 245,
    "bbox_top": 310,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5641501b-0dd0-4df5-8bd9-cd50fbefb65f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f467c08-b649-4f82-b9f4-139f8fc59600",
            "compositeImage": {
                "id": "8d8e1a57-2e87-4397-966a-b53c2564a776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5641501b-0dd0-4df5-8bd9-cd50fbefb65f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff45ed6-855b-4b6c-b2d7-2f6f1b247f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5641501b-0dd0-4df5-8bd9-cd50fbefb65f",
                    "LayerId": "c2f60b7c-4149-4044-8771-8933a8fd05e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c2f60b7c-4149-4044-8771-8933a8fd05e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f467c08-b649-4f82-b9f4-139f8fc59600",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}