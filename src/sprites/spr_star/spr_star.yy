{
    "id": "0f88316e-bfd3-477d-9bd7-21ef2f1c4ff8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_star",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11d669dd-190d-43ea-845c-068bf1d66cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f88316e-bfd3-477d-9bd7-21ef2f1c4ff8",
            "compositeImage": {
                "id": "f1dad1c2-5c95-4edd-827f-eb3a65f79926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11d669dd-190d-43ea-845c-068bf1d66cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a3f5994-c3f7-4943-8d2d-7e9d0e887fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d669dd-190d-43ea-845c-068bf1d66cc4",
                    "LayerId": "7d7e0263-98a7-47e1-8ebb-3aa18538f1a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7d7e0263-98a7-47e1-8ebb-3aa18538f1a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f88316e-bfd3-477d-9bd7-21ef2f1c4ff8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}