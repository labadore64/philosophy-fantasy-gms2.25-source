{
    "id": "a0324c78-7e46-4a06-8685-f66f99fdb423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_player_nb1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 54,
    "bbox_right": 448,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e258e75-9432-49ac-b45b-b6eb9ef98e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0324c78-7e46-4a06-8685-f66f99fdb423",
            "compositeImage": {
                "id": "3142d0d2-088f-414d-9908-2d90bbd5d779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e258e75-9432-49ac-b45b-b6eb9ef98e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36c1dd05-f1ed-40d3-8c2f-b8d03b891b0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e258e75-9432-49ac-b45b-b6eb9ef98e39",
                    "LayerId": "eebeb9a4-a97c-4bbb-9f0e-bfb49aea9679"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "eebeb9a4-a97c-4bbb-9f0e-bfb49aea9679",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0324c78-7e46-4a06-8685-f66f99fdb423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}