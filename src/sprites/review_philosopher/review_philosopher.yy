{
    "id": "0f999ea6-1889-4794-974f-447048cc04e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "review_philosopher",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d632dff-9923-4e83-8282-be40bd4b4e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f999ea6-1889-4794-974f-447048cc04e0",
            "compositeImage": {
                "id": "2a553d91-8e85-43c8-b7e9-1a2774f7c7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d632dff-9923-4e83-8282-be40bd4b4e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f70fff-5988-4c41-adcf-7df75ebfae30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d632dff-9923-4e83-8282-be40bd4b4e18",
                    "LayerId": "63699ffb-7338-4484-8309-f0a39e4294a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "63699ffb-7338-4484-8309-f0a39e4294a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f999ea6-1889-4794-974f-447048cc04e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}