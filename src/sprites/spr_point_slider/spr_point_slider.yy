{
    "id": "0a58a9ac-cbad-430d-a445-3b7e4146ebb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_point_slider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 11,
    "bbox_right": 122,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8511a13d-330a-46b3-9ed7-812daf47f812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a58a9ac-cbad-430d-a445-3b7e4146ebb6",
            "compositeImage": {
                "id": "95351c67-75dd-4d14-bb80-d702494bfe05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8511a13d-330a-46b3-9ed7-812daf47f812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da7d8591-f9fc-464d-8f52-7995dfbed9a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8511a13d-330a-46b3-9ed7-812daf47f812",
                    "LayerId": "ca889a83-0f7b-4065-8bc3-c5cee23206a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ca889a83-0f7b-4065-8bc3-c5cee23206a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a58a9ac-cbad-430d-a445-3b7e4146ebb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 52
}