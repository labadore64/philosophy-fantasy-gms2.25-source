{
    "id": "e2455d50-3dc2-48ab-9445-05a726acabaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 3,
    "bbox_right": 34,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc0c41ee-e368-42b4-a705-e09ac5c9f980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2455d50-3dc2-48ab-9445-05a726acabaf",
            "compositeImage": {
                "id": "d710bbd8-51b0-405b-8703-90fd73ff66c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0c41ee-e368-42b4-a705-e09ac5c9f980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d613ba7c-da77-419e-a47e-7975970ba481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0c41ee-e368-42b4-a705-e09ac5c9f980",
                    "LayerId": "a88bf378-9541-4a67-92dd-0df8d8d52692"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "a88bf378-9541-4a67-92dd-0df8d8d52692",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2455d50-3dc2-48ab-9445-05a726acabaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 19,
    "yorig": 21
}