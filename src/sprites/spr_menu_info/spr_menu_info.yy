{
    "id": "b2b9fbf3-25a3-46af-b3f6-0aca411347fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_info",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 5,
    "bbox_right": 122,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87720e7f-5ed3-4e1a-970c-da477558fd02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2b9fbf3-25a3-46af-b3f6-0aca411347fc",
            "compositeImage": {
                "id": "98c0d8d5-fb9b-46ad-aa87-f2770b508e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87720e7f-5ed3-4e1a-970c-da477558fd02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c77da6b-ea69-48d5-a9e9-879c330c9f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87720e7f-5ed3-4e1a-970c-da477558fd02",
                    "LayerId": "fef57a90-10ee-4d71-be42-8d48ca530edd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "fef57a90-10ee-4d71-be42-8d48ca530edd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2b9fbf3-25a3-46af-b3f6-0aca411347fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}