{
    "id": "790c1d84-13c2-4d57-89f1-c0468a5f292a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_neck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 380,
    "bbox_left": 177,
    "bbox_right": 306,
    "bbox_top": 286,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3baf6ba8-9253-4f41-960d-dbdaeba73583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "790c1d84-13c2-4d57-89f1-c0468a5f292a",
            "compositeImage": {
                "id": "95075596-087f-4439-9a4e-f92f1fba3f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3baf6ba8-9253-4f41-960d-dbdaeba73583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b0d7e32-f906-4d97-860e-58324ed54acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3baf6ba8-9253-4f41-960d-dbdaeba73583",
                    "LayerId": "57c2d51c-8be0-4ded-9502-bcd729af6572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "57c2d51c-8be0-4ded-9502-bcd729af6572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "790c1d84-13c2-4d57-89f1-c0468a5f292a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}