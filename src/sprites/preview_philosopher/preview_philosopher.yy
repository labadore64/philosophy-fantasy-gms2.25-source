{
    "id": "ee136d47-be76-4b1d-88b8-0fe0c14b3fea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "preview_philosopher",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dcf9d49-21c2-40b3-aa45-989fabd1a0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee136d47-be76-4b1d-88b8-0fe0c14b3fea",
            "compositeImage": {
                "id": "b379457d-e3bb-496f-8da2-81c5637ffc19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dcf9d49-21c2-40b3-aa45-989fabd1a0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d6b214-b5c8-4d12-b368-fe324eb58c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dcf9d49-21c2-40b3-aa45-989fabd1a0d2",
                    "LayerId": "8d857c39-982b-4201-bf98-614630145d50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8d857c39-982b-4201-bf98-614630145d50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee136d47-be76-4b1d-88b8-0fe0c14b3fea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5c6d2703-4e2f-460a-b8af-9c3d1d277f6d",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}