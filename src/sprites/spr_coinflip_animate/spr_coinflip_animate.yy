{
    "id": "824223f8-a611-4d03-b7ab-af55668b615c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coinflip_animate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13a88387-8917-42fc-a059-2c2bb6db47a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "824223f8-a611-4d03-b7ab-af55668b615c",
            "compositeImage": {
                "id": "4ec2f85c-9c94-4d34-b24a-2d574033cb72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a88387-8917-42fc-a059-2c2bb6db47a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd2d0e3-10b8-40f9-8427-4a694d03fd1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a88387-8917-42fc-a059-2c2bb6db47a8",
                    "LayerId": "52236f6d-476b-43a3-84e8-f956c6b45732"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "52236f6d-476b-43a3-84e8-f956c6b45732",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "824223f8-a611-4d03-b7ab-af55668b615c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}