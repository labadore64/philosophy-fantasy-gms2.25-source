{
    "id": "9cb70360-b351-4935-a328-7b7ab9399f8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_img_dimension",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b45c2a9-d174-4239-b57e-47c7896f762c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cb70360-b351-4935-a328-7b7ab9399f8d",
            "compositeImage": {
                "id": "588409de-46c3-406c-a30c-79f472526fb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b45c2a9-d174-4239-b57e-47c7896f762c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1359caaa-a765-4f35-b82f-0d9c10ed2a49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b45c2a9-d174-4239-b57e-47c7896f762c",
                    "LayerId": "8872d860-b25b-4b94-bd7e-161f144c1d55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "8872d860-b25b-4b94-bd7e-161f144c1d55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cb70360-b351-4935-a328-7b7ab9399f8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}