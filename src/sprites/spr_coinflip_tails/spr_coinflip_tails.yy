{
    "id": "4d2fcac5-bcaf-4f6d-80b1-06ea9453a6a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coinflip_tails",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 475,
    "bbox_left": 183,
    "bbox_right": 604,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09aca0f2-adee-4f51-92e3-53c75cdb16cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d2fcac5-bcaf-4f6d-80b1-06ea9453a6a7",
            "compositeImage": {
                "id": "86fb4703-27b7-411e-9d5e-10396f931e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09aca0f2-adee-4f51-92e3-53c75cdb16cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69566275-92cf-4a68-aa1d-136ea1736c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09aca0f2-adee-4f51-92e3-53c75cdb16cd",
                    "LayerId": "a8acd149-88f3-428d-a745-f0b73ba81cc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "a8acd149-88f3-428d-a745-f0b73ba81cc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d2fcac5-bcaf-4f6d-80b1-06ea9453a6a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}