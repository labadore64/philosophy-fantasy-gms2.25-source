{
    "id": "53b05321-b293-44e4-8e28-4bc8322bf797",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_overlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 23,
    "bbox_right": 104,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4083fea5-2aa4-4faf-bd7e-5be5f10741ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53b05321-b293-44e4-8e28-4bc8322bf797",
            "compositeImage": {
                "id": "1a67bd5e-68dc-4461-9f26-1d370d45d3a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4083fea5-2aa4-4faf-bd7e-5be5f10741ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca2f32ae-8375-4545-b8a2-9edfb8320184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4083fea5-2aa4-4faf-bd7e-5be5f10741ee",
                    "LayerId": "4b64e266-7cb4-4241-a4cc-4543ed9a6cd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4b64e266-7cb4-4241-a4cc-4543ed9a6cd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53b05321-b293-44e4-8e28-4bc8322bf797",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}