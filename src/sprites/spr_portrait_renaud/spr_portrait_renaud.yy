{
    "id": "49129595-0311-4654-b727-10dd7abdd88a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_renaud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 58,
    "bbox_right": 475,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a5b1ed6-6eb1-46ad-8ac6-c74f78266f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49129595-0311-4654-b727-10dd7abdd88a",
            "compositeImage": {
                "id": "d213eeb4-aae5-4df3-802a-7bcefa3d64c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a5b1ed6-6eb1-46ad-8ac6-c74f78266f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fb5ef80-bb8b-436f-8e48-38c01ba65989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5b1ed6-6eb1-46ad-8ac6-c74f78266f85",
                    "LayerId": "1afc2a3f-f807-4f48-8a13-4310c39282de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "1afc2a3f-f807-4f48-8a13-4310c39282de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49129595-0311-4654-b727-10dd7abdd88a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}