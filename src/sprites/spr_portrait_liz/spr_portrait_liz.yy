{
    "id": "8a36bcda-d6a4-4166-8625-cc1abbb505f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_liz",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 75,
    "bbox_right": 461,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3106834-28c7-4036-a212-803e64eb146c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a36bcda-d6a4-4166-8625-cc1abbb505f6",
            "compositeImage": {
                "id": "b200c4a3-5921-43e6-bec1-8575cefba71e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3106834-28c7-4036-a212-803e64eb146c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdef6f16-489e-4a3e-bbf3-be6ca55d6693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3106834-28c7-4036-a212-803e64eb146c",
                    "LayerId": "c8cd5754-fca1-4303-93aa-84d6adb6aeaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c8cd5754-fca1-4303-93aa-84d6adb6aeaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a36bcda-d6a4-4166-8625-cc1abbb505f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}