{
    "id": "84be22e9-ec0b-4cf2-9f1b-44afab492925",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_ear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99c55435-cd0a-4069-a8e3-36e650d7cd6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84be22e9-ec0b-4cf2-9f1b-44afab492925",
            "compositeImage": {
                "id": "a37975c3-e3da-4b62-9ce6-bca2b9b95b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c55435-cd0a-4069-a8e3-36e650d7cd6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aafb5e04-84af-43e8-bfe9-234468f15e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c55435-cd0a-4069-a8e3-36e650d7cd6d",
                    "LayerId": "6fa6363d-d549-4293-a943-1a3bb69e183a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "6fa6363d-d549-4293-a943-1a3bb69e183a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84be22e9-ec0b-4cf2-9f1b-44afab492925",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}