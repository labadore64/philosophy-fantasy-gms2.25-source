{
    "id": "70771c9f-6657-48a5-be29-06e31d3e5623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dropdown_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 212,
    "bbox_left": 23,
    "bbox_right": 426,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "828cb956-b3e3-436d-9048-a41085ca5bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70771c9f-6657-48a5-be29-06e31d3e5623",
            "compositeImage": {
                "id": "35a6c8e1-057b-41d5-991e-30a92972afa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "828cb956-b3e3-436d-9048-a41085ca5bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a181a4d-a734-4683-b567-4dcd515e790f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "828cb956-b3e3-436d-9048-a41085ca5bf7",
                    "LayerId": "45a2bd70-13d9-44c2-a969-dbc47cc47879"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "45a2bd70-13d9-44c2-a969-dbc47cc47879",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70771c9f-6657-48a5-be29-06e31d3e5623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 450,
    "xorig": 225,
    "yorig": 0
}