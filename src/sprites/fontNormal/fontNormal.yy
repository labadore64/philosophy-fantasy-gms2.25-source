{
    "id": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fontNormal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "414f38f0-84a4-4452-bcbb-03054e717cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "df7bb1d3-a723-4546-a215-2a6579bfcaf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414f38f0-84a4-4452-bcbb-03054e717cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e936573-24f0-4123-b33c-ced6cb906a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414f38f0-84a4-4452-bcbb-03054e717cc3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "d1e517c4-ee85-49c1-b665-5b335d7850d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414f38f0-84a4-4452-bcbb-03054e717cc3",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "96759603-6447-41c6-b2fe-4cfa01543cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "eaba3ced-b433-4d21-9214-94f4ce9881ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96759603-6447-41c6-b2fe-4cfa01543cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123e0a8b-a8b5-4e81-8033-7332ec36c39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96759603-6447-41c6-b2fe-4cfa01543cff",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "a8cc55d2-a570-46ff-9010-b783e68c4fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96759603-6447-41c6-b2fe-4cfa01543cff",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "fc468dbd-64bb-43fe-b909-17a53924259f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "658c243f-14ad-4e2f-af65-dec4613b657e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc468dbd-64bb-43fe-b909-17a53924259f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff24c8c-ee5d-49f8-9284-08407b951f6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc468dbd-64bb-43fe-b909-17a53924259f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "0f7b59b2-d023-4b95-9fa9-1e54b4eaeb08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc468dbd-64bb-43fe-b909-17a53924259f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "66eef2b9-dff3-491b-a765-cc8f7525988e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff43d05-0db9-496c-b065-0027b02c2e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8de54e9a-2907-4225-ae24-7f8beee1ed28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "159f68ad-362d-467f-8e47-ff7c25db8f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "eed060d8-dc00-4291-8952-406def7490a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "159f68ad-362d-467f-8e47-ff7c25db8f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86153f0d-17c5-456d-b265-223ebddd7382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159f68ad-362d-467f-8e47-ff7c25db8f32",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "19d7af48-be16-4ddf-9960-7f98d4153241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159f68ad-362d-467f-8e47-ff7c25db8f32",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "69a0a8d5-087b-47f6-b3e4-4352790578a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3578c013-9ad3-4c87-9928-c3985d1e050e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7a98b90c-2309-47c4-9ee1-5542bebd3990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "18326649-3b43-4094-b638-14b1a46c060b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "38ff6b09-f9ff-452f-91b3-4390beae0ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18326649-3b43-4094-b638-14b1a46c060b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a31ea6f-3bd7-466a-84f1-95fa976e08ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18326649-3b43-4094-b638-14b1a46c060b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "89fb6911-4493-4ef6-982e-fef1a6dda72b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18326649-3b43-4094-b638-14b1a46c060b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bc509196-4522-4c25-bb47-bc46eee5ab03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5341d903-e675-45bb-bf5e-ae16fcde04d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "52a396e4-7b0b-4f92-9819-5ad986704978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "95be07a5-3f09-4240-82ad-00bbd5687de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b292c442-c43a-4e6e-bc05-58d58b586593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "92e9b034-ba30-4f92-8465-171a9d42f1a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "f081018b-fa92-4e83-b891-9f178e7cce66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "5ce60bac-96b1-47b2-bc19-e16daeb1e266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f081018b-fa92-4e83-b891-9f178e7cce66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e11214-e052-4ab6-a091-05a0834b4c61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f081018b-fa92-4e83-b891-9f178e7cce66",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "30a4ee91-52d9-4407-a022-b3385975d6ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f081018b-fa92-4e83-b891-9f178e7cce66",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b6532597-138f-49c7-b4e4-454874212935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "1a6a8aa8-e85b-4be2-ae73-7ba3a47e9cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6532597-138f-49c7-b4e4-454874212935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a91b518f-2cd4-4090-8522-5a1636a56ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6532597-138f-49c7-b4e4-454874212935",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "2e5c6598-ff77-4d8b-b6c7-765eacbdf636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6532597-138f-49c7-b4e4-454874212935",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b6619143-6d77-49f1-97de-ff151edfb98c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4c5ea8e7-482a-4d8e-bf9d-5d58b32cb896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6619143-6d77-49f1-97de-ff151edfb98c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7be0af6-8440-4aef-8e55-c8ad171da8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6619143-6d77-49f1-97de-ff151edfb98c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "c22bb422-9bc5-40d9-9933-4c53931a2d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6619143-6d77-49f1-97de-ff151edfb98c",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "161c0fd1-b49c-4e0f-af53-60754ed28e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ae6bbe-d194-4a7f-ad12-4b80f57ca051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b5979cae-5afe-400c-b88b-2a0b9843047b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "1a5f46d3-9f7f-4a27-9d89-1f274f4743b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6ba922-f8d2-4b30-932e-c69dafb02df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "0ebf7da2-efd2-421f-9c08-9ade90d3dca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "360d78c0-a2d7-4d74-816d-52b652546f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "144367e1-8cfb-4cf0-ba08-4ed3c36e32e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360d78c0-a2d7-4d74-816d-52b652546f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9c3f15-ef53-4e09-b6bf-d0dc5de3e822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360d78c0-a2d7-4d74-816d-52b652546f3d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "d67073a7-d90a-436c-97c8-5369b92172f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360d78c0-a2d7-4d74-816d-52b652546f3d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "95cdcd5a-298e-46fa-b227-c69c6613c926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ceec0d2-ff1e-4a6b-9ab4-fac2c9045080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "e8963a48-6765-401d-8b4b-41245de6a500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "0e6331df-ecda-4fed-88ec-0c700c3fb608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e74821-dd8f-4b41-942d-65264d11f540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "98acfa4b-7faa-4822-9e43-fa162a651edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "71c80e35-5a84-4089-a9d0-c145fd204b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6d82065-e0c3-4e28-9bd0-c1678d58b3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7b1bae95-f24e-4744-8d5a-3031d9ffa57b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ec67a29d-7c0c-4880-8dc2-c7cf1e3c0ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6482a0ad-bb24-425e-b8aa-3af6c61ed39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "3222e616-6ad9-4df6-86b1-9478253a0a91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "885233f3-ba2f-4899-ab2a-ca843759d7dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8beb556b-89fc-4ee5-aaf0-d498b76c5af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "f8b02608-fb9d-49bf-8dab-b6908cf3e0a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a3edf169-3e71-41e9-9658-625eaa693ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f71c0c-2dcd-40aa-a0d0-8ccc2028626b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "2607e4a0-2dab-447f-b1db-7b8f6142344d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f4304e91-0f8e-4919-afa1-6feeed3ccb19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2357b4d-2f1c-48ca-b7f7-dcf814b2f378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "1d080ae2-541d-4fd5-9fc8-00686b51334e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "bbafe227-7d74-41e6-a161-48870c0f994d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "000ec0c0-62e9-4337-a45b-4320edc5a949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbafe227-7d74-41e6-a161-48870c0f994d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb45177-c95a-44f5-8061-7f29053e4ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbafe227-7d74-41e6-a161-48870c0f994d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "eae2e6a1-12d8-4b35-bafd-6a9b38500680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbafe227-7d74-41e6-a161-48870c0f994d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "eab18968-2c95-421d-878a-63889f1978d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "18c6ddc0-be63-4bf6-ac90-f9e1cbad6b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab18968-2c95-421d-878a-63889f1978d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969a09e1-7167-4260-a9c7-458ec0634083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab18968-2c95-421d-878a-63889f1978d3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "98526d5b-285e-4a9b-a103-8fc93e8dacc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab18968-2c95-421d-878a-63889f1978d3",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "61c47459-11f0-4af6-9c47-f87ef5a241dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b01d43-b258-42ab-a0db-0f5e02810ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7e2c4609-c369-4c0f-b198-c11dd3a13e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4cbfeae6-8661-4a3d-8dee-06bccbea66ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72fdd3ac-4e42-488b-90e9-f78c39d59e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "f7e12abb-1b44-47eb-bcc7-2c1295252b73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "96d7faed-36fe-432a-b1af-c6469ba46f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bd5f6951-ac92-4ffb-9ac7-662a8b7fcba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d7faed-36fe-432a-b1af-c6469ba46f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a75e579b-aa3e-4fd8-a7d4-6100e5d0154c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d7faed-36fe-432a-b1af-c6469ba46f25",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "f2822822-c454-4570-814a-b126ed93ba7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d7faed-36fe-432a-b1af-c6469ba46f25",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "17a60aba-8220-41a6-9a92-3953ea63ac0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e7dc96-a6cf-459d-94a1-718536b95d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "0570fffa-1cd6-4320-a3de-8dc10fa7b37a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "ee66136e-6181-4963-939b-ee25e8ba9407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4e088aa1-b864-41e1-937a-c7e4102458dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee66136e-6181-4963-939b-ee25e8ba9407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7530010-02c8-4783-8147-ae7202aeab87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee66136e-6181-4963-939b-ee25e8ba9407",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b97665a0-5f42-4d8b-a992-a14ab161b31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee66136e-6181-4963-939b-ee25e8ba9407",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "4f825655-97d3-4bbe-8de3-81871136f21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "557067ea-d6a9-4f4e-91d6-c4c346ba5e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f825655-97d3-4bbe-8de3-81871136f21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70fee4a-0c62-4cc5-9494-4de142831ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f825655-97d3-4bbe-8de3-81871136f21e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b356af1b-a95d-4346-aa90-0c891778ce6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f825655-97d3-4bbe-8de3-81871136f21e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "444e78df-a032-49c9-8621-83271fbadaa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4d38ffa5-9930-422d-8417-bb8b7ed99537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444e78df-a032-49c9-8621-83271fbadaa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2d69f5-1d9a-478e-ad5c-1d14804e3133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444e78df-a032-49c9-8621-83271fbadaa8",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b113c8f9-e9b8-4634-b9d6-4ac5ad99c788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444e78df-a032-49c9-8621-83271fbadaa8",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "685b2d60-724b-4aee-bb36-cc52c856441e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7b775dce-b122-4ec9-9c4f-92308f0b375a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685b2d60-724b-4aee-bb36-cc52c856441e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1d0f4b-9368-403e-9184-455fa7a9e4e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685b2d60-724b-4aee-bb36-cc52c856441e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "84a92a83-074e-4a01-8971-569ee5c60496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685b2d60-724b-4aee-bb36-cc52c856441e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8d645b55-fd66-46d8-ac59-c3a8adfbaf48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c8e8cb-e433-4edb-8b17-7ebe8ec70f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "11880f49-91ff-4cbc-8951-f2ba0e45346f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f27a9fce-641d-4d5a-afc0-d3f4b1a77a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ef2b04-da2f-4e12-b076-cd88844fa8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7b5aa3c2-654f-4673-8fb2-e547a974862c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "5b2cb454-68db-472e-8bf3-2055f307e732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8453a0cb-b6f5-4998-8080-20fec29f49c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "3e9c53fb-1779-402c-9839-4b650ad9bbae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "c6fd742e-370f-4bf7-a8e6-5733e8c40e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9f64d8c-2af0-45cb-8ff2-02af8231c5a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "05ba568c-9ca4-4251-b12a-66dce2313c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "25104ec9-27e3-4fc5-a1e5-a9a3ede168e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b392e78-ae0a-4b5d-b9f8-e650a2db5b21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "75aff1e4-3fbe-4cca-8695-59e742ee25cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "de660742-ff20-4381-aee8-2eee9c3b92aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d1a7a8-e510-40cf-aaa8-a862951339d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "6f244348-c401-41ff-961d-26359df2114b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d0240b50-1df9-481d-8b8e-427b695d2250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "d1bef301-6897-4cfb-8748-ea25b7a4e69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0240b50-1df9-481d-8b8e-427b695d2250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491d88a3-f5bd-4b6a-9d2d-fcab9f2d5d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0240b50-1df9-481d-8b8e-427b695d2250",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "758d6bf5-59f1-4120-92e5-33c03d6d3760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0240b50-1df9-481d-8b8e-427b695d2250",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "03c061b4-f6bb-4a3f-8226-ccd2efb73299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62aa93b5-9c9f-4ce7-9a38-a4e8c28d6966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b1939c0c-f7db-4a3a-873c-4c2f7cb3218e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ec51b1dd-0388-49ff-9fb2-1d110284e673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30644fde-c571-4014-a859-d539bea7556d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7726e192-d92b-49f7-8f9f-edfd63a537a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a2d222a6-8c17-4e28-9795-7e351d588110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eca81d3-e70f-43b1-a86a-e7c5a2b5a2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "58c1abd8-14cd-4477-a237-ccb22e9437da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "090527eb-bbc8-4820-8b90-137f7ae0635f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "84c5e54f-e0cb-4d88-9d54-f316722fe12f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090527eb-bbc8-4820-8b90-137f7ae0635f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "216d0142-71c0-404e-a8be-63070a6e5f22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090527eb-bbc8-4820-8b90-137f7ae0635f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "127adb1d-2a48-4a72-b195-aa76deac7546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090527eb-bbc8-4820-8b90-137f7ae0635f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "6784b0c7-2a19-49f7-aa52-59e8436541d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28ed432-5078-49a2-82f2-5ca87aee0e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8c461bd4-90df-4fc3-ae0b-1d52498e8b2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "82c09c26-919d-4482-9827-6bfe6a7ff860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3913ba97-036d-44f4-8b82-0bbee76b12ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "d8206adc-38fa-4a83-a203-6e09ef60627f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "76d1a733-6894-412b-9e7b-df0934e632ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "20d6e4af-6676-4a45-98f9-09e748527fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d1a733-6894-412b-9e7b-df0934e632ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001781b1-20e0-4c5d-88a3-52edbcd6d648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d1a733-6894-412b-9e7b-df0934e632ca",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "302f6a82-21ae-4f6b-9f9b-31bb4f389b4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d1a733-6894-412b-9e7b-df0934e632ca",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "905f2863-854a-4d2f-8f83-ae566001ec36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7f3f65a-c706-4852-8e9e-5f8a2ce348ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "e24c5638-be1b-41c6-a64e-3fa7125bc757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ff800a40-603c-4479-9028-f8b06e56001c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74640699-0290-4fa4-8cf6-6b516f078efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7441e7d6-661a-4ceb-a74d-2d5c78f6dfed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "06eaadb5-3613-4942-bcf4-6d8a51a4a2e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7866dbeb-960e-45dc-b8d6-8e02c76cd0fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "2991c33d-18d7-442d-9dbd-f25d986ff18d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "72fe3477-3e47-416d-837d-e738fe5b4f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed237a9b-ab0d-458e-8498-a780d12151e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "5b9bfbc0-bff3-47e8-8c13-9b8acc986a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "d9d281cd-9fe0-4624-aa91-ccda765645dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f3e46ac-cf70-4b61-8167-de0698ff432a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "cec9e404-8ece-46e2-96b2-d334999bf638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "cdf4ad54-1eaa-4b7b-be35-1b44e665754a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8597f5-8a19-4b1b-941d-129ce64604b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b7e10691-8c43-42c5-b6fd-e7d0bf4df07a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "3e6cbfbe-96df-46a6-bee0-a0de872fcc33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7422547-af9a-488c-9249-e828728c546b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "739f254a-deae-484f-9524-6961f7ca4029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "152358de-b5d4-42a5-9477-1aac61e55f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb3e9510-5ddc-4b94-a0b4-e9c946d98ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "ee8e4ceb-5ec8-4adf-8b3a-10c9f91403c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "3553e832-f408-444e-a407-706984b2aa80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "fd32b110-556c-4ca6-8d98-875451c8077c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3553e832-f408-444e-a407-706984b2aa80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7e4a69-adc7-4286-b923-d92b37b445ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3553e832-f408-444e-a407-706984b2aa80",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b8d0bef9-9776-4db8-a0c0-f04d67a8ff09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3553e832-f408-444e-a407-706984b2aa80",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e73e4d96-604d-4eb2-b45f-7ec066bae497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "777e35a1-9497-49ad-ad2b-6490d68aa5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "fd76a024-4fbc-425e-8a34-1fa1abbf9ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "77ece326-f1ed-4298-bb3a-5ec99c7116f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b85d45-8542-42e8-86a2-fee58b51f496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "c05a230d-456f-49d6-a047-c3e398f681b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d485a962-f87b-402f-a805-7ad96a85a67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "490e17ea-05be-4072-87d4-042f99049e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d485a962-f87b-402f-a805-7ad96a85a67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fad585b-7e75-4d58-925c-1667940b70cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d485a962-f87b-402f-a805-7ad96a85a67f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "e5c85976-dae4-45ad-a253-9fa9797c9488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d485a962-f87b-402f-a805-7ad96a85a67f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "798fad61-8256-4976-90be-a96a7a67190e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "fd56bc03-1adb-43a4-8353-ec7f0a89d566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798fad61-8256-4976-90be-a96a7a67190e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "153ed927-d2df-4e62-b144-92b4be5956fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798fad61-8256-4976-90be-a96a7a67190e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "48929123-abb1-460d-b33c-aeb810c59363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798fad61-8256-4976-90be-a96a7a67190e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "cf4eef31-b003-4379-a8fa-bf7c5e198277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da79593-1e82-4a4b-8bf3-cdbea866f9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "15a70d27-8823-428e-8b44-69048e227c20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "3d26ea54-81b5-4a35-958d-14223ea931bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "21cd4f3a-d795-4806-9ce4-58a0adf90339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d26ea54-81b5-4a35-958d-14223ea931bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095a6049-2909-4499-9789-6241e708fa41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d26ea54-81b5-4a35-958d-14223ea931bd",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "f3809e07-06d6-48f4-be7b-3138126ffc27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d26ea54-81b5-4a35-958d-14223ea931bd",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "136278d2-2ca1-4d1f-9d61-9a4e9959b271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a77df8da-12b3-425a-98bc-e5ffed29060d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "fc7c9355-905b-452f-8a01-b0759b55b622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ae75c9eb-7644-4797-b8fe-ee13ec913cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be1eaaff-268b-4fd3-baf1-297aecc486fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "933883a2-054d-44aa-9e3f-75ac062886c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4a313577-c42b-40ac-b527-e70c64b20308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2ae494-3935-443f-8cb2-b582d6866a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "45d41849-736e-434b-b53d-e4efbfa62c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "9657e363-7f5e-4109-95a3-5b2d31295795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "afccb4dc-5b02-4592-a09d-54c095d5dd9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9657e363-7f5e-4109-95a3-5b2d31295795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4f1d51-ad5d-4948-99af-cd8842ea45d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9657e363-7f5e-4109-95a3-5b2d31295795",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "34b8753d-b28c-458f-8f1e-483c97ed0a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9657e363-7f5e-4109-95a3-5b2d31295795",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "db95036e-efd3-4bc9-ae47-6e0c312b2243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24fc226a-653e-463d-9cb1-f09c83e870f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "579a59e0-aab2-4c98-9468-0c5f51818e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d2d8e140-e4ce-45de-940f-3848223f1a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "0fb4a6a8-8ec8-4294-b990-8a8b24861d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2d8e140-e4ce-45de-940f-3848223f1a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4f2aab-9bb0-4f99-9453-f0f70f73ce50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2d8e140-e4ce-45de-940f-3848223f1a68",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "638aaf77-422d-4f29-b599-f163bf1d35eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2d8e140-e4ce-45de-940f-3848223f1a68",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "beaabd0f-2b8a-4ef6-96e3-89f9b3eb810e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ad9a2b-863d-431e-9235-839fba41ef95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "0fea3cac-3d33-48b6-9b14-81d028f60c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "41591d60-6797-41aa-aeee-49928cd81643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96a9e03-16d6-4ef2-b736-d6a519165e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "96185d10-d4e7-41c3-b581-870c57a273db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "3611edad-5353-4f21-a533-bdbc9919bb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8219feb2-9f61-4444-95fb-8161d5ca49bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3611edad-5353-4f21-a533-bdbc9919bb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27cf89e-be77-4e86-a4b1-6f9e0c5294fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3611edad-5353-4f21-a533-bdbc9919bb3f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7f449282-ff7c-4216-9985-8e0696a8e22e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3611edad-5353-4f21-a533-bdbc9919bb3f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ee37900f-1bc2-4fb8-880f-68a73a25e065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a30623a-613e-4f5b-83f5-210b04ec04f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "98b30cda-acb4-432f-bd3c-28c3a0d42763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8c5c8d80-dfd1-4d5b-8371-2caa49711440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13c631b-997a-4cae-a27e-841f1961847e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8801eee1-11b5-4331-b5ba-10c1482b4619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "12f12d72-175e-46b6-a1b4-e59586e7297f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220e7be2-ab61-4d5c-b35d-1c3e1ac5a95f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8fd0ed59-df75-446a-b553-8bb962147390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "5ce508e2-5604-4892-ac56-80c26356c39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ccb4b214-6827-4259-b2a0-52f0800f1898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce508e2-5604-4892-ac56-80c26356c39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96c6d5b-68e8-4766-b7b5-5b78ad0068ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce508e2-5604-4892-ac56-80c26356c39b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "ca263df6-be81-4c3c-ae49-0ab6bb97e7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce508e2-5604-4892-ac56-80c26356c39b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e1af8bf3-f821-4028-aabb-0931ab3aee94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a67991-a67c-4169-8065-89e546960482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "55776820-b6d9-4006-992c-deb3e9f55b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ebf2e6c9-818b-48ca-84f0-9ed379672c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38677c7-d36d-4826-926e-a5c765fb3719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "c170ddce-b84d-4ebb-8a82-1dfe2730c730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "833c2b76-f2db-4179-a7f9-90470dc98965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e26a1b34-cbd9-4083-a99c-5ae14e578694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833c2b76-f2db-4179-a7f9-90470dc98965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40806269-895d-4d73-bc8a-62b7784e7b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833c2b76-f2db-4179-a7f9-90470dc98965",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "2530cb31-390c-4837-84fa-6ebb95bf8c8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833c2b76-f2db-4179-a7f9-90470dc98965",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "256b82ad-4f2d-432a-9f11-a2e47029a109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7289a8d9-d459-42b7-a14d-1b58fcf79095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256b82ad-4f2d-432a-9f11-a2e47029a109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d5ef59f-45ec-44c0-bad7-2ebc11486191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256b82ad-4f2d-432a-9f11-a2e47029a109",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "3aa2e1c9-3ccf-40ce-8ba5-3823b7f3f21a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256b82ad-4f2d-432a-9f11-a2e47029a109",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "e58f7347-14e0-422d-866d-91194d6f30b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "11fd3d67-17e3-4718-8ddd-ac5af7a96e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e58f7347-14e0-422d-866d-91194d6f30b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58bc310c-9a7c-46c3-985a-b95afd873b5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e58f7347-14e0-422d-866d-91194d6f30b3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "40feba70-6774-4b2a-8af2-b0a067941bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e58f7347-14e0-422d-866d-91194d6f30b3",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7bc8d77b-c38f-4172-924f-b5b88c6db620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4727eb93-6f96-41a3-b616-8a18e6b74092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "ed0b6400-2933-4ef7-91f6-d26b6209623b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bde9c031-daac-492c-87ed-e97367afaca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4cea2d-9628-46b3-9217-aa2106a4268d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "320abb22-99cc-4398-a34a-e740871cf000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "ab729e3d-68f3-4907-994d-23ae430453ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bdca6fe0-2bc1-4e23-97e5-245fde0abfc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab729e3d-68f3-4907-994d-23ae430453ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ccc838-3758-45c7-82c2-45fe368cc573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab729e3d-68f3-4907-994d-23ae430453ac",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "352b5062-2ce7-468e-b09d-110f0304f158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab729e3d-68f3-4907-994d-23ae430453ac",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "8342820d-7e71-4ebc-927e-51d5674df89f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "84e2f0de-cefb-4ec4-849f-db4c415cd05a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8342820d-7e71-4ebc-927e-51d5674df89f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcbeda8f-2989-472b-a5e5-d63223ea319c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8342820d-7e71-4ebc-927e-51d5674df89f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "252de41e-486c-46f2-b542-a0047f95e199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8342820d-7e71-4ebc-927e-51d5674df89f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "9c5f8251-1fa5-400b-82c1-c7cc5fac390c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9abbe37d-b985-46e4-b31a-c31463c8e2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "12189003-b9fe-4db1-84c8-c3f84b4e98ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "668ea954-4ef1-477a-8db8-37b920f7d819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bb5a6564-92e6-42ff-bcab-604881284bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668ea954-4ef1-477a-8db8-37b920f7d819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36a7ed5-136c-4200-9195-3695fc5329fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668ea954-4ef1-477a-8db8-37b920f7d819",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "cf876747-cb13-49c1-8396-f9c9a1fdd2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668ea954-4ef1-477a-8db8-37b920f7d819",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "567650f6-764d-48ee-89ba-b0b3ea7ae910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "374572b4-d4e7-4ef8-be72-0e8a42754f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567650f6-764d-48ee-89ba-b0b3ea7ae910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b87c097-f00a-4edd-bc9f-d174e6340885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567650f6-764d-48ee-89ba-b0b3ea7ae910",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "5f646722-b602-494a-8868-3d26233ba402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567650f6-764d-48ee-89ba-b0b3ea7ae910",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "acee3492-3399-4f07-abc0-11013647a68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7b9303ab-d533-44b0-87c8-a76dac567727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acee3492-3399-4f07-abc0-11013647a68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edbcdd7d-a8a4-4f1d-81b8-36731786ef00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acee3492-3399-4f07-abc0-11013647a68f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "4cac9db9-5f26-48bf-b205-1ab7ed90fbe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acee3492-3399-4f07-abc0-11013647a68f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "438d2c4a-fec0-4c5d-9372-11b0f13f0e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7a7bdc65-cc2e-47ca-b5e8-a6b54ad660be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "438d2c4a-fec0-4c5d-9372-11b0f13f0e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f8ca71-a5dd-48dd-ab97-7cce1c4168e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "438d2c4a-fec0-4c5d-9372-11b0f13f0e72",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "caa24b9b-bd70-4205-9ce9-6bd8e9b0103b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "438d2c4a-fec0-4c5d-9372-11b0f13f0e72",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "4b3bcd49-9b45-49d0-8ddd-4ec68e5ef783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "19fdbaaf-5252-4125-a8c8-1e5be5579576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b3bcd49-9b45-49d0-8ddd-4ec68e5ef783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a9f359-45d1-49f7-9f31-9da93da90a0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3bcd49-9b45-49d0-8ddd-4ec68e5ef783",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "951a1825-1645-443e-8ff7-6442db8e65ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3bcd49-9b45-49d0-8ddd-4ec68e5ef783",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "fba9e227-5226-4a6f-9045-754236ca651d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "6327ca89-e686-4c1f-8afa-c29f72c769a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba9e227-5226-4a6f-9045-754236ca651d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf793f2-fde9-40fd-b17e-a712698c4094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba9e227-5226-4a6f-9045-754236ca651d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "cac4f649-01e2-4df4-bdec-6d091c5f153a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba9e227-5226-4a6f-9045-754236ca651d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c6da7c8a-900c-4319-9dcf-90647c96b90a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "c43bb63b-fc2f-46ff-a0d9-a7ff3ff076e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6da7c8a-900c-4319-9dcf-90647c96b90a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bc2dc7e-084d-43f5-af6f-d22bae2b529f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6da7c8a-900c-4319-9dcf-90647c96b90a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "5fb6e5f8-7bd4-499e-8886-baac6ed1f9d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6da7c8a-900c-4319-9dcf-90647c96b90a",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "e611d963-9a0a-4cd0-bcf7-81ca25ae507b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "394ef57e-6464-41be-a9c7-326e99ae10aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e611d963-9a0a-4cd0-bcf7-81ca25ae507b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cb8031a-017d-4902-9eac-869f5a362c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e611d963-9a0a-4cd0-bcf7-81ca25ae507b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b74c7eb5-f8b2-49a5-a13c-3500ffc44781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e611d963-9a0a-4cd0-bcf7-81ca25ae507b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "dea1dd51-ef78-4f5d-814e-94d2ade6d022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8cf441ad-f9b5-42b5-b2ec-38e6b6c38dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea1dd51-ef78-4f5d-814e-94d2ade6d022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2912011-c061-4abb-a2f5-9c5733bd6a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea1dd51-ef78-4f5d-814e-94d2ade6d022",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "3dc1301a-2c3f-4a53-b942-7b1526fba75f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea1dd51-ef78-4f5d-814e-94d2ade6d022",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "0131f97c-b649-4027-a5c8-4176cd9f337b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ee6dc73d-1149-479e-b98f-2cc6ca99adf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0131f97c-b649-4027-a5c8-4176cd9f337b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849d4863-0d9f-495d-b408-cdcd546378c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0131f97c-b649-4027-a5c8-4176cd9f337b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "e3e8bd25-21cd-47f2-a153-b7ca7f6ea268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0131f97c-b649-4027-a5c8-4176cd9f337b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "2afdc935-6f63-4db6-9b1b-6fb54c7a2975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "3476cacc-b251-46db-aeb8-814f508124dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2afdc935-6f63-4db6-9b1b-6fb54c7a2975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fdefa34-bf9e-49af-8eaa-41bea9bbe2d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2afdc935-6f63-4db6-9b1b-6fb54c7a2975",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "d8a9a444-f187-4605-8408-c8a0258c6299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2afdc935-6f63-4db6-9b1b-6fb54c7a2975",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c92396e9-48f4-4b38-89b6-fbb36745e935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "6bc68ade-5080-4fe9-8403-32fd7f7963a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92396e9-48f4-4b38-89b6-fbb36745e935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d88dfe7-7f6a-4b8f-8ab4-a99b70caaf99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92396e9-48f4-4b38-89b6-fbb36745e935",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "63cf2daa-6b25-4f6b-9c36-ff83f7947351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92396e9-48f4-4b38-89b6-fbb36745e935",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "689cf515-507a-4a13-b211-a2b2a52773e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "3fd01422-e11e-481c-9f3d-26f2e121116a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "689cf515-507a-4a13-b211-a2b2a52773e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9412c4-c75d-4d31-b411-a0fb7fbfd598",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "689cf515-507a-4a13-b211-a2b2a52773e8",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "69910ded-4fef-46ab-93ca-b24f8ee2dc3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "689cf515-507a-4a13-b211-a2b2a52773e8",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "1b2cb76d-6b8b-4a67-8e61-5511287bd3b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "88671f6e-2181-4510-8bf8-e95286bc088b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b2cb76d-6b8b-4a67-8e61-5511287bd3b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7244b587-3fe8-4ff4-80db-c63559b946e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2cb76d-6b8b-4a67-8e61-5511287bd3b8",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "395d0510-4cb0-411b-a3e3-70a57497dad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2cb76d-6b8b-4a67-8e61-5511287bd3b8",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d27bf20f-bb2c-48d9-9631-b14346faa909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4d382a36-bd07-475b-805d-78802d337199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d27bf20f-bb2c-48d9-9631-b14346faa909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbf7ec6-bfaf-48ea-af4d-1c27923b1271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d27bf20f-bb2c-48d9-9631-b14346faa909",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "336b6427-347c-4bc4-80ba-0e2053c5df32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d27bf20f-bb2c-48d9-9631-b14346faa909",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "f15fd94d-073c-4a7e-9d4d-278a121fe374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "87d59101-ba92-403a-b322-d489d94750b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15fd94d-073c-4a7e-9d4d-278a121fe374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be57f5ae-f926-4252-885b-8c8b36561c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15fd94d-073c-4a7e-9d4d-278a121fe374",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "c90d11b7-ea4b-4cea-a111-e51557fdeb84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15fd94d-073c-4a7e-9d4d-278a121fe374",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "4d736a83-d4b8-48dd-a5ee-a81c4fc80215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ad9f8167-444f-4df1-b23a-943996f4bf5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d736a83-d4b8-48dd-a5ee-a81c4fc80215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e7ba5ca-b007-4fe9-805d-344c3bbd714a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d736a83-d4b8-48dd-a5ee-a81c4fc80215",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8e8617a6-3575-491b-b9b4-6365a7d8512a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d736a83-d4b8-48dd-a5ee-a81c4fc80215",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "bbdec4a4-e40b-4f7f-b904-b7997a494628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "eee576de-385d-4dde-8c8c-28693f619f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbdec4a4-e40b-4f7f-b904-b7997a494628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "460cf6fa-37c0-4f66-b296-7704e47a69b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdec4a4-e40b-4f7f-b904-b7997a494628",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8b4e3d83-eee5-4531-80b8-526a69c537f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdec4a4-e40b-4f7f-b904-b7997a494628",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "610ffa97-996c-4904-89ce-81e6749b9cb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "99c10324-a20c-466c-a064-ac523ae577c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "610ffa97-996c-4904-89ce-81e6749b9cb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc41b883-18b5-4c45-9abc-912b78546efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "610ffa97-996c-4904-89ce-81e6749b9cb5",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "62307d47-4e7f-4aba-b55a-0ab684bef950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "610ffa97-996c-4904-89ce-81e6749b9cb5",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "a1f1c1ea-4bc6-4a91-8d1f-626e0b94cc96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "3f89a1ec-4c83-4986-b85b-15adaf03c9ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f1c1ea-4bc6-4a91-8d1f-626e0b94cc96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b59f7c3-0ba3-4cc4-ada2-7730078822a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f1c1ea-4bc6-4a91-8d1f-626e0b94cc96",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "fc9c6950-3db9-4beb-84a4-d260f83b2943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f1c1ea-4bc6-4a91-8d1f-626e0b94cc96",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "08d59901-67dc-4ab9-9cca-10450dd1a7b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7d7530ae-dbdc-4e7e-907a-12447581a108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d59901-67dc-4ab9-9cca-10450dd1a7b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd44998a-40e7-4b75-87af-842ea65252d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d59901-67dc-4ab9-9cca-10450dd1a7b0",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "197a6919-4360-4ad0-81f9-1f9f988d3c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d59901-67dc-4ab9-9cca-10450dd1a7b0",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "fd3a9728-81ea-444e-a898-6fa3ddb056a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "61b6bfdd-8d09-4873-9cba-852a08978943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3a9728-81ea-444e-a898-6fa3ddb056a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef7e15ac-8a48-4054-9f61-d2b1bc861349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3a9728-81ea-444e-a898-6fa3ddb056a2",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "be8d067a-96bf-4e7f-89c1-4407aea0f8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3a9728-81ea-444e-a898-6fa3ddb056a2",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "df751446-323b-4614-bdb6-e3b2a583bed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "53d8bb17-b1df-441f-aa9b-d2e35fb488ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df751446-323b-4614-bdb6-e3b2a583bed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5524ae97-82fe-41d8-94d6-b1703785e912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df751446-323b-4614-bdb6-e3b2a583bed9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "24991b2d-231c-45a3-afed-bd250aebf7fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df751446-323b-4614-bdb6-e3b2a583bed9",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "809f5af4-0699-43da-a3bd-94299e322669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f340eeaa-1de4-4c76-8cc9-e1d218ecc3e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "809f5af4-0699-43da-a3bd-94299e322669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1089212-7128-40d9-bdaf-959028e7b5a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809f5af4-0699-43da-a3bd-94299e322669",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "db8c7d8c-6903-43df-8ffd-ca876b491eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809f5af4-0699-43da-a3bd-94299e322669",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "78593819-94ac-4397-8678-0e3192a4ecfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "782c992a-059b-4b48-bc74-56de45780c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78593819-94ac-4397-8678-0e3192a4ecfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7caddcf-fbb8-4bb5-beb1-6658bfe15389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78593819-94ac-4397-8678-0e3192a4ecfe",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "97e7eda3-c110-421f-b5e5-7038e5a5c168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78593819-94ac-4397-8678-0e3192a4ecfe",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c2fe4630-57ee-481c-9807-83acb6f80737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "629af838-81cb-42be-a3c1-d07eb52b59d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fe4630-57ee-481c-9807-83acb6f80737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acfa1c62-d603-4a8c-9779-ba02c502d94a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fe4630-57ee-481c-9807-83acb6f80737",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8fd27cae-f46f-428c-9e6e-75ed9084931c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fe4630-57ee-481c-9807-83acb6f80737",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "be15ded2-7d61-497e-9602-86a9530322cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "13daf33c-18cd-444e-80fb-2ea11ca5df88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be15ded2-7d61-497e-9602-86a9530322cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df44e36f-5b1b-476a-9ba6-4872bb56b953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be15ded2-7d61-497e-9602-86a9530322cf",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "4a58c32e-140e-48a3-9260-c62b765a11be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be15ded2-7d61-497e-9602-86a9530322cf",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "03f21b94-c4c3-4323-a425-3aa0c196499c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "9d3643cc-7dc8-4221-af25-776af268961a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f21b94-c4c3-4323-a425-3aa0c196499c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c81d85-f38d-4629-a313-4cf75de98e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f21b94-c4c3-4323-a425-3aa0c196499c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "87d23e14-fb69-445e-afd9-359ecf3567b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f21b94-c4c3-4323-a425-3aa0c196499c",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "19527311-3de3-4294-a7ef-18fa0bbf653d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8872b63f-1f26-4f26-a052-70a055fe8c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19527311-3de3-4294-a7ef-18fa0bbf653d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a81a83-c789-4427-9b7c-ae0360b09dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19527311-3de3-4294-a7ef-18fa0bbf653d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "6a37c23e-182d-453c-b7c3-9e6b976f393f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19527311-3de3-4294-a7ef-18fa0bbf653d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "65bf1127-9073-4dd7-a6d9-2ad050211615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "2785010d-74aa-40b6-b8f7-71f96186c58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65bf1127-9073-4dd7-a6d9-2ad050211615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c160c0-e18b-451a-97f5-3f3834de1f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bf1127-9073-4dd7-a6d9-2ad050211615",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "3f1c2205-0cd4-4233-9f75-d3ee0a083832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bf1127-9073-4dd7-a6d9-2ad050211615",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b059f515-f609-454a-9f16-7c63161d4780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "2c2b60de-6004-4f0d-b01f-291c34a42651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b059f515-f609-454a-9f16-7c63161d4780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b90f0357-7265-48f4-ab95-28b6702b2fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b059f515-f609-454a-9f16-7c63161d4780",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "c3a7084a-33c7-4c69-b9f5-2e5e5d164be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b059f515-f609-454a-9f16-7c63161d4780",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c964cade-860e-4cea-a020-b17476c29097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "004afb84-87be-4fb7-9249-c1023849ca6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c964cade-860e-4cea-a020-b17476c29097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c7d94af-d274-4b22-8e43-0d42c0edc4da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c964cade-860e-4cea-a020-b17476c29097",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "f828e607-0081-4847-916b-de66df0002f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c964cade-860e-4cea-a020-b17476c29097",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "26d4d182-3e82-429a-b74d-3831aea77662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f0a321f1-b659-440d-8b1d-8bc86efc8be0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26d4d182-3e82-429a-b74d-3831aea77662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967c7a5e-043a-4a72-8079-73d5b6bc912f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26d4d182-3e82-429a-b74d-3831aea77662",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "25548c17-a55d-4f9c-9cd6-9032536ba7d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26d4d182-3e82-429a-b74d-3831aea77662",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "18f23d5d-a4f4-4ba0-bb4a-e6d17267fbec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "57d25de8-96b4-4fdb-af70-e553785b918a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f23d5d-a4f4-4ba0-bb4a-e6d17267fbec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dab1d94-b788-40d5-a8be-2e434dc22939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f23d5d-a4f4-4ba0-bb4a-e6d17267fbec",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "c9e24fd9-29cf-48a6-b044-5ee4474346d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f23d5d-a4f4-4ba0-bb4a-e6d17267fbec",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b464530c-556a-4cdc-a265-4708b8f7f68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e1d575b9-f3be-48f1-ab32-c0f11ca49d7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b464530c-556a-4cdc-a265-4708b8f7f68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9f6878-e8ad-4516-bdef-adf0241ad4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b464530c-556a-4cdc-a265-4708b8f7f68d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "7251c5d5-a16e-40e9-9aa3-1ab1a946630a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b464530c-556a-4cdc-a265-4708b8f7f68d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "191fc902-63e2-44f0-8813-4beb23c9ff01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "39ce862e-0414-494a-bb55-a3d07d087254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "191fc902-63e2-44f0-8813-4beb23c9ff01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd12b48-1f7e-4ec1-b5d3-a60b03fe1abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "191fc902-63e2-44f0-8813-4beb23c9ff01",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "4b358028-6d26-4c49-bc6a-026a1f1917ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "191fc902-63e2-44f0-8813-4beb23c9ff01",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c027dc59-cfec-45cb-b312-4c88ce757b1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "50e97d5a-f8c4-4e3a-8e1a-8a6c48d5793e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c027dc59-cfec-45cb-b312-4c88ce757b1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46328896-7754-4a76-8175-27a8656a37bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c027dc59-cfec-45cb-b312-4c88ce757b1b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "0a4869b9-55f5-40d2-af83-f26724ec9440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c027dc59-cfec-45cb-b312-4c88ce757b1b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "37116f0d-1d23-4eec-9cb1-a068f3940021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "360db5fe-fc3d-477a-9f3d-8b0821147d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37116f0d-1d23-4eec-9cb1-a068f3940021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a9cece-655b-44c2-8843-4304c38f9d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37116f0d-1d23-4eec-9cb1-a068f3940021",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "18c703b8-5928-4af6-83a6-3411ce8bc97a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37116f0d-1d23-4eec-9cb1-a068f3940021",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "ec6c5927-3d90-4542-b329-d7b30ca50c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "9d153f24-62a0-440f-80c3-721e864a2fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6c5927-3d90-4542-b329-d7b30ca50c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea8921e-86b3-4c1d-adef-50d5429cf66c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6c5927-3d90-4542-b329-d7b30ca50c7a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "62f9212f-474e-481f-8327-6f38cb304fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6c5927-3d90-4542-b329-d7b30ca50c7a",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "090cacaa-8596-482e-b570-d5fe648c512e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a3093159-9178-4d51-b9a6-eb29d2e18c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090cacaa-8596-482e-b570-d5fe648c512e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ddfb94-b7d5-4f7e-900f-985ca0a4ca41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090cacaa-8596-482e-b570-d5fe648c512e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "cd5a8c74-b2cb-419f-a7e4-77b1e0e909db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090cacaa-8596-482e-b570-d5fe648c512e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "94f0d8ef-fa0f-49ac-8f87-0d07195aac59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "246cff9f-b12e-4cf4-a1c8-ec1b543ca975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f0d8ef-fa0f-49ac-8f87-0d07195aac59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87ba45b-e1c0-4913-8a29-838d1c01dc0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f0d8ef-fa0f-49ac-8f87-0d07195aac59",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "35c0160a-97f1-47a2-9139-7a65d25734b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f0d8ef-fa0f-49ac-8f87-0d07195aac59",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "37c4079c-5e93-4b50-9cdc-8094df04136c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8be94ec5-04b5-4588-9c0b-4a4f8d3d1128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c4079c-5e93-4b50-9cdc-8094df04136c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f9e6d2-7130-4bf2-ac2d-f42f3dca0aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c4079c-5e93-4b50-9cdc-8094df04136c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "1f06030b-9348-4a0a-ac1c-b31691f5989e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c4079c-5e93-4b50-9cdc-8094df04136c",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "58a56916-19eb-4b21-b888-a1d835b30b3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "afcdbc55-955f-4590-8f41-5644f2aa8c75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58a56916-19eb-4b21-b888-a1d835b30b3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e29605d-f3d3-4c12-bd50-b16fc6a37a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58a56916-19eb-4b21-b888-a1d835b30b3d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "26951994-19f4-4ce9-8ee6-b4bd7cc3d53f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58a56916-19eb-4b21-b888-a1d835b30b3d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "8485b211-d8ba-49d0-8aa1-57ec5cab6edd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ffde3907-a16c-41f4-8596-7d818c2fa56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8485b211-d8ba-49d0-8aa1-57ec5cab6edd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54fa056-cb4b-4831-b4d7-4b4957a2daa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8485b211-d8ba-49d0-8aa1-57ec5cab6edd",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "98dfc152-b860-40b5-8432-49558602fd29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8485b211-d8ba-49d0-8aa1-57ec5cab6edd",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "f1f75323-60f7-4887-a4d9-d84332db692c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "fb79cabe-6fa5-4386-89b5-4cde065a876c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f75323-60f7-4887-a4d9-d84332db692c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20613da5-7a1b-4e03-ac9b-8517107b01bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f75323-60f7-4887-a4d9-d84332db692c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "29e6d424-a765-4231-92ef-674db7909ece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f75323-60f7-4887-a4d9-d84332db692c",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "bf6e54d0-c913-4e7c-bb7c-6e20162f98ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "49ecd728-38a1-42b3-a802-2553f7af2189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6e54d0-c913-4e7c-bb7c-6e20162f98ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e46552c6-19c0-48e4-a6bd-9e4632bdfd2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6e54d0-c913-4e7c-bb7c-6e20162f98ca",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "35c77aeb-9577-4e4f-89a7-a37ca5767a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6e54d0-c913-4e7c-bb7c-6e20162f98ca",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d34f8bd7-c561-400e-bcfa-de5d1d129f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "08ce109e-a592-4c40-9489-c2c332f178d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d34f8bd7-c561-400e-bcfa-de5d1d129f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "185cce62-b14c-46f8-9ebc-0f33ef32bb53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34f8bd7-c561-400e-bcfa-de5d1d129f78",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b07d3494-1a94-48f7-83e3-1e34c98c6911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34f8bd7-c561-400e-bcfa-de5d1d129f78",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c4a7b14a-69a3-4918-a99d-8821f22fa04c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "b694f0dc-a006-44d8-b132-19090c194f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a7b14a-69a3-4918-a99d-8821f22fa04c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "addbddd4-2449-4497-9e3c-56dcf95782bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a7b14a-69a3-4918-a99d-8821f22fa04c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "4ac1b57d-8fa9-422d-9ea9-8d8c7bbdacba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a7b14a-69a3-4918-a99d-8821f22fa04c",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "42d74736-8680-4909-8c66-b28fbd93a035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "aceac62c-9bff-4836-bc95-c028f004f8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42d74736-8680-4909-8c66-b28fbd93a035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3675b4-356d-4888-b66f-26db5a8dca6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42d74736-8680-4909-8c66-b28fbd93a035",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "fab07e1f-2782-4fed-8de7-d3bfc2892f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42d74736-8680-4909-8c66-b28fbd93a035",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "ca7b5e28-9443-4d0e-bba4-64866bc6dad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "655d5763-7ce6-4747-9f54-36f25a286e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7b5e28-9443-4d0e-bba4-64866bc6dad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e1e53d-d7b0-4788-8e7b-c33500ca80b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7b5e28-9443-4d0e-bba4-64866bc6dad2",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "aea3a8ab-7baa-4aac-bd7b-b146591ac0b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7b5e28-9443-4d0e-bba4-64866bc6dad2",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "bb2bf67c-c190-4383-8c69-a2678bd41110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "07ac1e1f-ab3b-47c0-9070-410d01c6a846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2bf67c-c190-4383-8c69-a2678bd41110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7366384d-ff65-41d2-b28c-d795b901c982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2bf67c-c190-4383-8c69-a2678bd41110",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "4535e287-36ff-4944-9d4d-2c679c98f014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2bf67c-c190-4383-8c69-a2678bd41110",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "1d48441c-f0fa-4e15-a9af-79f05f4490e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "915a515d-7e24-4d02-8f99-3829d93df58c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d48441c-f0fa-4e15-a9af-79f05f4490e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d68a1ae-ed39-4815-9793-b737944da4bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d48441c-f0fa-4e15-a9af-79f05f4490e0",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "4ee5e2e7-6770-4aa0-aa9a-13fec0368f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d48441c-f0fa-4e15-a9af-79f05f4490e0",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "c11b00a4-72ed-446a-8946-347368b33944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7b63625a-d515-43b4-b6b7-5c98353b76f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c11b00a4-72ed-446a-8946-347368b33944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d661ce74-afc8-460e-81f5-c3e14f135579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11b00a4-72ed-446a-8946-347368b33944",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "ee7a2787-cb63-4d98-b587-c4fdc12bd533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11b00a4-72ed-446a-8946-347368b33944",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "a3a19b2b-7d01-43ab-a09a-17fbd969b548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "295359d9-131f-4560-bbaf-24077d0c64e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a19b2b-7d01-43ab-a09a-17fbd969b548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d189ba70-b8a4-4e42-80fe-7e947f5d6889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a19b2b-7d01-43ab-a09a-17fbd969b548",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "63d0a845-6c80-4c0c-bfef-df9860534b33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a19b2b-7d01-43ab-a09a-17fbd969b548",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "03b7183e-502a-4006-b719-f0b659a0242f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "341a0272-890f-44c5-9250-dfe811a3b9eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b7183e-502a-4006-b719-f0b659a0242f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d48f948-29c9-47bf-9ccd-9234c028c819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b7183e-502a-4006-b719-f0b659a0242f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "d50a5aa6-edc3-4660-a489-75ab9c7715eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b7183e-502a-4006-b719-f0b659a0242f",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "76aac748-908f-46fa-a075-85b5cf6aee85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "3d686f05-a792-47aa-b927-93abfd281ecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76aac748-908f-46fa-a075-85b5cf6aee85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269c3c07-8c7c-4295-8e05-9ca8be261d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76aac748-908f-46fa-a075-85b5cf6aee85",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "36e09dba-e211-4b85-bd59-6fddad54b0ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76aac748-908f-46fa-a075-85b5cf6aee85",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "71f92da1-0b1e-4ce2-924c-0b2e5438ab7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "d4d59b9f-0036-41f2-8758-2377fb780d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71f92da1-0b1e-4ce2-924c-0b2e5438ab7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7102d71-d85c-4f01-ac6e-5d0900f38978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f92da1-0b1e-4ce2-924c-0b2e5438ab7e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "e700210e-ab30-4802-89ea-307e2509ed4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f92da1-0b1e-4ce2-924c-0b2e5438ab7e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "d7fa6ece-6712-4164-b309-4e6c631ddbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a6047db0-ed8b-4f38-8b96-0583a4adcb26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7fa6ece-6712-4164-b309-4e6c631ddbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a90add7-73d2-4d92-a981-52f748717667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7fa6ece-6712-4164-b309-4e6c631ddbd6",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "3883fc1f-3900-4b84-a4c1-7dc1acb693d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7fa6ece-6712-4164-b309-4e6c631ddbd6",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "5a961441-b0cb-4869-a264-53e5fc19d6d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e12a98a0-2039-42d4-a633-7a554b32ad5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a961441-b0cb-4869-a264-53e5fc19d6d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d520979-580d-49b5-a9fc-f51a41318506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a961441-b0cb-4869-a264-53e5fc19d6d6",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "cadf83d7-8394-42a4-bfff-65e2ba54cb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a961441-b0cb-4869-a264-53e5fc19d6d6",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "43936d4c-28ff-4d3a-bf2d-e5f6e2332e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a4023bc9-2943-409c-a7d9-7a0130f08960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43936d4c-28ff-4d3a-bf2d-e5f6e2332e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11189773-68d8-4c08-99eb-4390fc04d50d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43936d4c-28ff-4d3a-bf2d-e5f6e2332e8d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "8fc68143-e217-4767-987e-fa45c25bee30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43936d4c-28ff-4d3a-bf2d-e5f6e2332e8d",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "779ca47f-b20a-4547-b058-6896cc1a1560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "5f36d67b-4904-41dc-94b6-77cfc16e42e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "779ca47f-b20a-4547-b058-6896cc1a1560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c891e4c1-fe52-4602-8ad5-1984d7643808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "779ca47f-b20a-4547-b058-6896cc1a1560",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "b6dae335-795e-489d-a72a-f758989208c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "779ca47f-b20a-4547-b058-6896cc1a1560",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "27a48a78-7d61-4c65-8942-c5d1db6c262b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "2d208194-0aa7-4371-ad8c-6779eda3bdf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27a48a78-7d61-4c65-8942-c5d1db6c262b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1960e5d9-56c4-46d3-be48-0b13d637d190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a48a78-7d61-4c65-8942-c5d1db6c262b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "a47cd4e2-0d5a-4e47-b7f7-22ea2e72f56b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a48a78-7d61-4c65-8942-c5d1db6c262b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "021dccf9-6092-47f2-b9b8-160ca569a75b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "b16993aa-cd65-4915-adf6-b159dc568528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021dccf9-6092-47f2-b9b8-160ca569a75b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c84cd959-a33f-46bf-972b-1ff6e7820095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021dccf9-6092-47f2-b9b8-160ca569a75b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "eb16a323-62eb-4a0c-8b31-d7e73af43a33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021dccf9-6092-47f2-b9b8-160ca569a75b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "3ce076ab-a91a-46c9-b254-479bcaf596bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "c6593087-5d1a-4d41-a82e-dd3a0102598b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce076ab-a91a-46c9-b254-479bcaf596bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07045b49-9fff-473b-8204-d0c36f25359c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce076ab-a91a-46c9-b254-479bcaf596bb",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "780b398b-c358-48d4-bbcd-90b545f6d62f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce076ab-a91a-46c9-b254-479bcaf596bb",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "39d11edd-2334-4b2b-ae43-317d44f1f80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "0d782408-1871-4efb-9155-b7ffaa0257f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39d11edd-2334-4b2b-ae43-317d44f1f80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d780b2e-5884-4614-980a-1ee27d413b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39d11edd-2334-4b2b-ae43-317d44f1f80b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                },
                {
                    "id": "96422550-1084-49cd-9ff0-3e34fab2aa44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39d11edd-2334-4b2b-ae43-317d44f1f80b",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                }
            ]
        },
        {
            "id": "b22eadbc-265e-47f6-ada3-9ad35f339aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "704062b1-80cd-48a0-b329-f4a640c1efad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22eadbc-265e-47f6-ada3-9ad35f339aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7ee9f3-a12f-47be-b7af-f9c2e4585c90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22eadbc-265e-47f6-ada3-9ad35f339aa9",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                },
                {
                    "id": "6999b676-ac17-42bb-8f4c-fc23e33bf633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22eadbc-265e-47f6-ada3-9ad35f339aa9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "c966831d-0f4a-4548-b82d-1ce3cfc089b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "1fd350e3-b516-44f7-b28a-ad64c1780240",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c966831d-0f4a-4548-b82d-1ce3cfc089b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb0a846-1dc0-4b98-9b95-fb6aaab8c855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c966831d-0f4a-4548-b82d-1ce3cfc089b7",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                },
                {
                    "id": "f11560b1-5791-43c7-b016-2dca84e7f9ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c966831d-0f4a-4548-b82d-1ce3cfc089b7",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "ceeba2ef-499a-4fcc-bd70-dc23781594c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e3291177-ef50-4eff-87a5-d3328058a5a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceeba2ef-499a-4fcc-bd70-dc23781594c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dcd5282-29e4-408a-98a6-63044b602791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceeba2ef-499a-4fcc-bd70-dc23781594c3",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                },
                {
                    "id": "cc027e90-8e0a-4467-acbf-2594bb80631c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceeba2ef-499a-4fcc-bd70-dc23781594c3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "7dad7b6d-c4e4-40d3-a567-b910bc238329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "1b3b49a3-367a-4a26-bd6f-53c45359a099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dad7b6d-c4e4-40d3-a567-b910bc238329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd5455a-8845-453f-b713-a935c216dedf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dad7b6d-c4e4-40d3-a567-b910bc238329",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                },
                {
                    "id": "a8b3df6b-f88c-4228-bcce-cfa7d1bc3c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dad7b6d-c4e4-40d3-a567-b910bc238329",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "65c0edc7-74ea-4d13-b7af-c6eb6210776e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f74429d9-91c7-44ec-985d-47dbf8aee578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65c0edc7-74ea-4d13-b7af-c6eb6210776e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b0601c-5a5d-4693-a5e9-ed80aae0c8c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c0edc7-74ea-4d13-b7af-c6eb6210776e",
                    "LayerId": "7974c091-631d-4a01-b153-27f96264e549"
                },
                {
                    "id": "3606438f-960e-4be0-9a89-eeae56a36ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c0edc7-74ea-4d13-b7af-c6eb6210776e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "7974c091-631d-4a01-b153-27f96264e549",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "50d94fb2-b853-4446-95b9-39c8bc2ac501",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 33,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}