{
    "id": "49a84014-cca6-45d8-b24d-c2854555a0fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_help",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 8,
    "bbox_right": 32,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1001d5ac-d85e-4184-ab8d-5e91af3049d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49a84014-cca6-45d8-b24d-c2854555a0fe",
            "compositeImage": {
                "id": "b06cb1ca-5403-4102-9e4f-4664282e1ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1001d5ac-d85e-4184-ab8d-5e91af3049d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f045b9bd-4aa1-457a-be7d-c70be9800505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1001d5ac-d85e-4184-ab8d-5e91af3049d0",
                    "LayerId": "ccec6b69-7560-407a-9a13-25a8f126ca54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "ccec6b69-7560-407a-9a13-25a8f126ca54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49a84014-cca6-45d8-b24d-c2854555a0fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.767750263,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}