{
    "id": "602cbb50-81d9-4410-926f-f0be384e5a6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_glasses",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f4b3509-117b-4ac7-a32e-93af330e100e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "602cbb50-81d9-4410-926f-f0be384e5a6b",
            "compositeImage": {
                "id": "ec74bfb1-96eb-4c11-b4a2-ded5178f75e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4b3509-117b-4ac7-a32e-93af330e100e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a87bd4fc-c5a0-4b2c-8949-86f2990a60c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4b3509-117b-4ac7-a32e-93af330e100e",
                    "LayerId": "63808b66-400b-4698-8c78-2428ee4812db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "63808b66-400b-4698-8c78-2428ee4812db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "602cbb50-81d9-4410-926f-f0be384e5a6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}