{
    "id": "6c4f2f6a-d459-4fb3-98cb-d6195cc9fa56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_earring",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb382325-5588-4a06-943a-37bd1a1dd98a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c4f2f6a-d459-4fb3-98cb-d6195cc9fa56",
            "compositeImage": {
                "id": "bc4918aa-eb95-434c-9b6b-0fe7578e92d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb382325-5588-4a06-943a-37bd1a1dd98a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeae3de9-7511-4a8e-853d-a93391f7100c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb382325-5588-4a06-943a-37bd1a1dd98a",
                    "LayerId": "298045e8-9424-4c73-87af-21ea115a875e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "298045e8-9424-4c73-87af-21ea115a875e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c4f2f6a-d459-4fb3-98cb-d6195cc9fa56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}