{
    "id": "15e3cc72-ba8a-4ad8-b653-0fc5ec165e91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_snout1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b8ed92f-588e-4b2e-a8c0-009a10526eb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15e3cc72-ba8a-4ad8-b653-0fc5ec165e91",
            "compositeImage": {
                "id": "8648e839-2357-4c96-9e7d-241227bd13aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8ed92f-588e-4b2e-a8c0-009a10526eb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354f2ec3-f1c2-421a-8ed6-d3399a1a7cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8ed92f-588e-4b2e-a8c0-009a10526eb0",
                    "LayerId": "95739afe-1f63-40ab-bde2-28b26db4ba64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "95739afe-1f63-40ab-bde2-28b26db4ba64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15e3cc72-ba8a-4ad8-b653-0fc5ec165e91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}