{
    "id": "1e008ad8-b384-465a-be6e-6ea6f0539293",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_thomas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 66,
    "bbox_right": 484,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9cbcd86-420d-4eb5-995a-e35f882e05e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e008ad8-b384-465a-be6e-6ea6f0539293",
            "compositeImage": {
                "id": "5ec7544a-0cbe-4e5e-9564-fc4f0f13411f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9cbcd86-420d-4eb5-995a-e35f882e05e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb2b7ef4-918b-4c3b-b2a2-c21c743d1452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9cbcd86-420d-4eb5-995a-e35f882e05e4",
                    "LayerId": "ce559de0-f1cc-4b42-b7be-08aeeb71841b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "ce559de0-f1cc-4b42-b7be-08aeeb71841b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e008ad8-b384-465a-be6e-6ea6f0539293",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}