{
    "id": "637f2be5-91af-4e80-a780-1cc14b3af757",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_eyes_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 290,
    "bbox_left": 156,
    "bbox_right": 271,
    "bbox_top": 225,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c374aa2-0244-4b2d-9d49-d62d0665748c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "637f2be5-91af-4e80-a780-1cc14b3af757",
            "compositeImage": {
                "id": "be8f07ca-e8b6-4580-a737-d1a52483187d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c374aa2-0244-4b2d-9d49-d62d0665748c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79dcdf9a-96ee-415d-8618-116956642383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c374aa2-0244-4b2d-9d49-d62d0665748c",
                    "LayerId": "7af20430-ea73-425e-9139-e3187b4a8ac0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "7af20430-ea73-425e-9139-e3187b4a8ac0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "637f2be5-91af-4e80-a780-1cc14b3af757",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}