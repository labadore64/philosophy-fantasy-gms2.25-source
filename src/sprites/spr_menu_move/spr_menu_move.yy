{
    "id": "873d317c-c094-42a0-9bad-e4c41cf93749",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44521259-93a5-4218-b5cd-9684f7d41fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873d317c-c094-42a0-9bad-e4c41cf93749",
            "compositeImage": {
                "id": "685dbd96-3e83-42da-b0ee-37d76160d91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44521259-93a5-4218-b5cd-9684f7d41fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b74359-298e-4ff4-a600-4ab25ae2ffcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44521259-93a5-4218-b5cd-9684f7d41fd0",
                    "LayerId": "d5eb6882-0231-4735-87d0-cdca14066e2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "d5eb6882-0231-4735-87d0-cdca14066e2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "873d317c-c094-42a0-9bad-e4c41cf93749",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 40,
    "xorig": 19,
    "yorig": 19
}