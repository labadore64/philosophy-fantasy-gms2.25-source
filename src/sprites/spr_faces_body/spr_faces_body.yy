{
    "id": "2baa0c72-fff3-4411-a60e-eb4e94e9780f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faces_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 96,
    "bbox_right": 436,
    "bbox_top": 346,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12160457-2793-4e76-a3b8-d21403676a02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baa0c72-fff3-4411-a60e-eb4e94e9780f",
            "compositeImage": {
                "id": "0887f29f-ee30-43b6-bff2-4ccda9317ad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12160457-2793-4e76-a3b8-d21403676a02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4674bb0-2a44-45e9-a6f9-915bc39805a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12160457-2793-4e76-a3b8-d21403676a02",
                    "LayerId": "1ffa9201-1969-434f-86e4-7b7596799141"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "1ffa9201-1969-434f-86e4-7b7596799141",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2baa0c72-fff3-4411-a60e-eb4e94e9780f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}