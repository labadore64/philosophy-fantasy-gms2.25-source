{
    "id": "0b24555a-e493-4dee-be3f-281aa1538d26",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "OpenDyslexicLarge",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "OpenDyslexic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "27d5f120-948d-4631-9541-7c64d6257ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e0d50baa-ea84-4873-9fa6-a9c94d933f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "52bcaaa9-a75e-4a0b-b061-2cb524327f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 244,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0b08be7f-f3e9-4bc6-930f-2d38f080e01a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 229,
                "y": 72
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1386ab96-7b15-4101-8e18-c112140e0496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 218,
                "y": 72
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7c863165-ea78-46b1-812d-9f61493e1ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 72
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "806275de-5c2b-4d92-9d1a-591adb1e7c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 185,
                "y": 72
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a0d1e838-c71d-4306-a967-94103a362af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 181,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ae29f299-f667-457b-875a-b7de76e0b247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 174,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "95a0cc97-ac17-436f-aee2-c315143e2900",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 167,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a5dd463e-59d6-4090-beda-8ebada6304a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 7,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9e4a857f-688f-4c3b-99d3-c0bff644987c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 153,
                "y": 72
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3d653ea5-c2af-4320-b899-b28ab3f8f9ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 136,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "dfa07100-33e9-4ff0-8f3b-6094ba9e3a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 129,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "49d42fe8-5dc0-41d1-9c2d-e2ea8fff7cc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 124,
                "y": 72
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3b3e7d6d-2a94-4e1a-9a7a-df2c22a37299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 115,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1f653907-719d-40e3-955c-6eb0b0c51b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "75fb4e71-7fdd-404f-9e81-8be48249bf8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 93,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "20204e1f-472f-4dca-b63d-aed7f159c145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 82,
                "y": 72
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "82d9e075-6473-4cd8-9fe5-035c5712eb2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 71,
                "y": 72
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "554f3bdc-0099-4661-b22b-3c3ed6832c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 59,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7f943867-26f0-4b91-a43d-ce4ed9e87be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 142,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5a87451a-896c-4ddf-b9f8-dd42fb9d7a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f204c0a9-fc6a-4208-b0a2-1dafad269a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 30,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4d5e3eb8-0036-4269-b776-074110027530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 107
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "83ac5856-0150-4e1f-9611-af4d3cfb3a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 142
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5bae293d-4b1a-4006-89eb-81d5f3916e5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 36,
                "y": 142
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2ca61e4b-4214-4147-b8c2-46b0dd5fe5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 31,
                "y": 142
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0d5b3e61-785f-44a7-ae15-98b78512156d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 16,
                "y": 142
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d7d32689-ec73-4bfe-a12f-e4d3caa63e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "88164b9a-c446-4762-9a0f-82291890a36a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 236,
                "y": 107
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b9584795-c7dc-4a23-a37b-e5a8fa9ed5da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 226,
                "y": 107
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "550c90d4-5ae4-42ab-804d-1a328e8d7e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 207,
                "y": 107
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a73fcad2-e7d2-4066-9057-5b8e5511b17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 192,
                "y": 107
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e37c551e-011d-4bb9-a844-f739861a70e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 180,
                "y": 107
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d369880f-4651-4b4c-8624-5acb9665edbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 167,
                "y": 107
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "40730f40-f5fb-449e-a5aa-1f783d797fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 153,
                "y": 107
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2f8aa731-82fd-40d6-86a0-a3caa45be019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 142,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "90ca3900-f45b-4807-80ab-af2b1f3a0b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 132,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e6fa60dc-b036-4414-8296-ae7c004cd97d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 118,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a0f250bb-c63a-451c-bfc8-b4f3daf6eadd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 105,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f9d7f986-85b2-41f8-b923-c36f23135a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 100,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f6008a02-00ea-483d-90cd-42ed5295caae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 107
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dc991e0f-9e27-445e-8e4b-a7526cabbcdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 79,
                "y": 107
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a7c718ce-e352-4d7a-b35c-9f03043f7a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ec062ca0-0690-40b8-bd8d-cc89c9489d4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 53,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f646bf6e-f4c8-4d66-9fdf-9d9a0765bdcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 46,
                "y": 72
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3b6e9ac1-022e-49c1-99c3-527085fe63d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 31,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c916fd83-f4d5-4669-aa84-998e216ca942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 19,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "25bb7980-a3c2-4cf9-ade3-1cd6696d5172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 8,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6bdf5c26-7e11-4ab1-9974-750ebddcb79b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3b50799a-fc9f-4ef3-b45b-559a844ab40f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "99427c5e-a537-47ab-a892-d03723f79e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a4c82753-671e-4c96-a46a-649603028213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2ac8708f-c360-4ae3-8aed-3e9e08cc5e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3e025af2-b33d-40bc-ab31-943639326f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2fb68948-a439-4d9f-9929-ade8f89a4542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0c22fbe0-652a-47b3-b572-706c1eac1bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "02792602-c046-4cbd-8046-94d4011a66d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d4cfae33-7326-4349-80f1-5271f44cb08b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3214123e-1b5b-4a49-b794-a60e9053c9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0dce20ab-4df9-4049-a49c-1eb592af1992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c0b3ed37-0794-4e4e-95a8-9eefad0baa39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "220ac922-e682-49b6-9548-ea9e9b4de82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1f206889-a2a5-4e95-bcd5-ee16c2608351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4af86875-9826-46e3-a062-b92fa8148f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bc1a82ca-135e-41e5-89a4-8cf9cf6b0004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3836cd16-84f0-4993-8ec3-e60d8c509bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fc908b7c-112b-4ab4-b057-fa0ef99b7acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1fd6af74-156e-46ea-ae9e-6e33e025c996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5823f69d-e805-4d74-8666-73bb25658633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fd97bb90-3837-405c-98b5-4b88ced78c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 24,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "52c0a57e-2a42-4b68-9821-753bd973e278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 140,
                "y": 37
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4825be10-4208-4135-8524-1f614124ff8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 36,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e3c98bbf-38e4-4a8e-a584-96a75d976a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7f65dd9b-4c60-4353-8214-9aef9967716d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 37
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9f85a4ed-da32-4e5e-bd54-465205d53b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 236,
                "y": 37
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b9a57c98-10c4-4966-a585-112a0b00989d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 218,
                "y": 37
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "db5e1384-cdec-4e74-b051-ecb683c6456d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 37
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1f6f4416-3d74-483f-8d08-6211b0b6d558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 37
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "201754ed-fb99-482e-a8c1-6aa101ae5fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 37
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "30578743-397f-4169-8775-61f52261740e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 169,
                "y": 37
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "03a0173d-4d30-4700-a834-384881bda124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 160,
                "y": 37
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "01d4cbd8-ace1-47f8-81ca-7fc386961c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 9,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6242b0e8-95c1-4edf-9a71-8b05e101fc99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 37
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9df4bd08-6c94-4f24-9aea-3e2573a434c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 129,
                "y": 37
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1dc9a12d-ac8a-4b45-aade-109408e5ffb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 117,
                "y": 37
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "90e7a7ad-37e8-4bac-9b4b-2a26ccf66b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 101,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "968b5df6-b2d8-44e2-8f46-0574cb1e7c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 88,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6e97e9c2-e856-494f-a3ce-cc0ba29feb83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 76,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b2bae8d3-64d5-48cb-b81f-8ff74a29aa43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 65,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e069bd19-edb7-4d71-9bd6-eef42fe451f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 55,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "07f2bf69-3d49-45d8-bccf-8c934af1f46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 50,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cd17b858-162d-44df-bcfa-ff0bc46f8fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 41,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "01ea068c-b6f8-41b1-9dc6-3667bec7534d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 53,
                "y": 142
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1a689952-7bf0-4942-85b2-ce18b90760b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 67,
                "y": 142
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}