{
    "id": "0b24555a-e493-4dee-be3f-281aa1538d26",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "OpenDyslexicLarge",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "OpenDyslexic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2eaa01ff-d013-486d-9eb6-67e6f33834b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 56,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "317f1c55-e6ba-4716-bf31-939289247dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 56,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 173,
                "y": 118
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b2d4495f-4ca8-4df1-975b-5cc7d373d613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 56,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 161,
                "y": 118
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ca1f4d36-85d0-45b6-9545-57b3a60bf304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 137,
                "y": 118
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a548b697-41b1-455a-9021-d32fe4b6a4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 120,
                "y": 118
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0beb50f0-592c-4e58-bcf5-f5326aa365e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 91,
                "y": 118
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7680fcb5-3165-4e65-b97b-5e2705c26c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 67,
                "y": 118
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9b43a360-b2f2-4b63-be91-4e809dd53183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 56,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 62,
                "y": 118
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "56932e16-194d-437d-bab8-39417bbee042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 56,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 51,
                "y": 118
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "61173619-9eb2-4a6b-87a7-bba64def803a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 56,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 41,
                "y": 118
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ec8cfad2-783d-4264-8d74-3b10133046ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 180,
                "y": 118
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9b33d64b-06a0-45d8-a57e-f4f03d2c45f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 19,
                "y": 118
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8ec419a6-6107-4c84-8d09-ecd477d09072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 56,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 487,
                "y": 60
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e97016c9-83a6-4737-94e1-4aa21c70e2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 56,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 476,
                "y": 60
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "489f01d9-24e8-4be6-a438-6ca551b49492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 56,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 469,
                "y": 60
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d9ab39bc-05d0-457f-82df-5c4b26d5475f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 56,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 455,
                "y": 60
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "23550600-5993-4399-a534-5211419d23f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 437,
                "y": 60
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c99310d7-3c87-475e-9618-2b9fe4b1c37f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 421,
                "y": 60
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "38af8cf1-f50e-49a5-9709-be9f3609e1b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 404,
                "y": 60
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8f4e8c49-82d3-4d50-a4b6-cd42124083f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 386,
                "y": 60
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "fc56a5b6-d5e0-4a7b-a964-325c1200aed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 367,
                "y": 60
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b4c0921b-044a-4432-9ce0-979b06aa7f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1c77cd17-52c3-4e10-9c8b-ee563d0e1e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 198,
                "y": 118
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "de46cca1-7e1c-4372-b5a6-50b0c30cc2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 216,
                "y": 118
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d4338f50-60c7-44fe-b09e-03859cc82e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 233,
                "y": 118
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "47e0e960-5a0f-4921-98e3-474f362273f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 128,
                "y": 176
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "485ad4d2-f36a-4344-a75f-c9b554f98e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 56,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 122,
                "y": 176
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "23f83bcb-2efb-43b5-9afa-ed839289c8ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 56,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 115,
                "y": 176
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8dcb865d-7274-47e6-957e-4d1d2b25053d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 21,
                "x": 92,
                "y": 176
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ebf1a05d-d173-416d-aa06-f4bfbe82ee63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 70,
                "y": 176
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f9eef455-a1cf-416d-b098-df9dab88d46e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 21,
                "x": 47,
                "y": 176
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "30951c19-dbaf-4328-93c7-fa82c093d1f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 56,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 32,
                "y": 176
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "df19b90b-731b-4eed-8b0d-271b15b2647c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c6774f46-1531-4879-921d-989544b4f2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 470,
                "y": 118
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "28e53f29-df95-4109-96c8-5116fbffdc20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 451,
                "y": 118
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "81cc16b6-abe2-4e1f-b8a7-33dc92108150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 430,
                "y": 118
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8110f9e4-a423-4523-be59-2c350bc0c6a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 408,
                "y": 118
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "58d9f1a7-c52f-4756-83f6-a448b1e792f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 391,
                "y": 118
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "eb37c3a2-add2-4139-aed3-91491ff1f66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 56,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 375,
                "y": 118
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dc03c7f2-577f-4aac-96d6-431341ccfc24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 352,
                "y": 118
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bc9808b4-c318-4d95-8f26-f0433a0ea84c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 332,
                "y": 118
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9e13cb1e-24b4-4a36-b59d-5a5dc31944e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 56,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 326,
                "y": 118
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6a716907-2334-4feb-803d-c02e536a07f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 56,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 314,
                "y": 118
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "157416ad-a8c8-4135-a0ad-1897d3265f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 293,
                "y": 118
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "553c02cb-73c9-4e4a-ae23-8050f6f3a47b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 276,
                "y": 118
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "65e70a22-9de2-4365-9bea-1f56d717c1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 252,
                "y": 118
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c550f0d9-8051-4199-a6df-34f6b3f1ada9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 347,
                "y": 60
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6e1e77d0-de00-4708-9162-8d58d594bb8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 323,
                "y": 60
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "faaa98b6-80d2-46a2-8b12-30af3551d6b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 305,
                "y": 60
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8191a363-09a3-4b5b-88db-b328ec06a6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f0e3530-3aa3-42e0-818c-48c70127ec2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 369,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4b0d7a89-dc1e-4dff-afcc-699dc419f487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 350,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "68a6a1bd-92d4-4b63-9e7e-155aabeca040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e4b61cfa-73bd-4ee9-8063-3b4ee9b4360e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "498abdc4-a85b-4373-9f44-8b196df9a22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5fbeb44f-957b-4742-8509-fdfe0c3cefb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 56,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 252,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "92c226fb-2280-4397-99c3-976ba210d0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3583645a-35fc-40ba-ae36-a2dc4c364658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "270b4ec7-5f46-4f4f-aeee-e6c842c9a6e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "706b6177-d446-4c42-8715-bbacee7bc0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 56,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "aed92e14-08b2-4f96-813b-8a56cde46d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "563fc85d-e0ea-45b9-9ab8-a2fda7e26bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 56,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e67a2ce0-88ea-4525-947c-e9505d5f920a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ea37bcc3-d1be-4e58-8cbb-c6ba7f0c00f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b93785c2-d1da-4503-bff6-f68dc393647e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 56,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "487a82e0-8d41-46e1-b12c-44c07d9ae42d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "016f97dc-9128-4263-95b2-28d33f56bc03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 56,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cd4ad7ad-d5b3-4d79-9f5b-0a0110d715d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ea4c4150-88e0-4dd2-baf1-b2f6012f2ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3c16e53b-f2bb-4126-b53d-ecebde2e97d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bc10a2fc-80ab-46e5-8534-0b1b70e483bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "896ebf90-991e-4c1a-8045-ac7784a596b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0fe8da58-6881-4f36-a283-9c7548ef0330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 103,
                "y": 60
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f8f58a4b-b759-4796-ae16-118f91361c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 56,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "70405216-7a51-431e-b050-151789d712ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 56,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 279,
                "y": 60
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c14ce84b-7b83-4d51-83ce-972fc7ca068b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 261,
                "y": 60
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cedbc95c-b361-4c42-bbf8-87e8f3b6d772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 56,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 252,
                "y": 60
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6977e83a-645e-493e-a1d5-882018a68787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 223,
                "y": 60
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c7eca788-810f-4eaf-ad11-a21b0f68c234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 205,
                "y": 60
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "eca99b15-078d-44b5-ae46-a3897440232c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 187,
                "y": 60
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "04321bf8-0f8c-4823-bbd4-66cb4fe0d22b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 168,
                "y": 60
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9d12946b-bc2b-4ccf-aaf1-06ff330904b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 147,
                "y": 60
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "232ed0f9-be83-4027-aef9-09a8f9847b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 134,
                "y": 60
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ec81758b-4d04-44a8-a277-3648a2a62a56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 56,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 289,
                "y": 60
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "944ac0ef-0c26-44bc-9b24-46124cf34fa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 56,
                "offset": 2,
                "shift": 16,
                "w": 11,
                "x": 121,
                "y": 60
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b8a4237e-c86d-4e58-9f91-ab97f664d2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 86,
                "y": 60
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1349a000-9843-4a66-bcea-8ec95a65de0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 67,
                "y": 60
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "76ed0354-b0ca-49a1-8682-e443f53511cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 56,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 41,
                "y": 60
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4daf077f-70c7-4207-85f7-7b098356d8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 21,
                "y": 60
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b9fbfc6a-0223-4b55-98ab-599025a93c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0f922fcb-5dbe-4271-8aa3-64a84b36d65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1c28cdc9-fb3b-45f5-adbb-afe0097593c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 56,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1a4f46d2-e5ba-42af-a5ee-8469591c257b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 56,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "92a7cd31-529f-4fce-bb3f-1ce7dda1c896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 56,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dac89670-dece-455e-9789-c640e609af39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 146,
                "y": 176
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4c3e4d08-20af-4ba5-bb59-5806ef69c706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 56,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 168,
                "y": 176
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}