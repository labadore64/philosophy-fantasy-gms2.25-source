{
    "id": "20786b2a-5473-4700-b365-3a4f4f3d5952",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "OpenDyslexicSmall",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "OpenDyslexic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "77e41ba1-5343-49f5-b40f-f89df29648c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a806e79f-a06e-4928-ac65-3642c2cce8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 7,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "69923b3d-80bb-48e7-a4c4-01ba224dac4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "276f9f56-dd92-441b-94ce-22828d5cff83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 243,
                "y": 25
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b44d1817-7d32-49da-b90c-d1d165f1a39f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 235,
                "y": 25
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e62d93f1-e5ed-438e-a618-c4ee2504ec1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ae293f46-4a97-4694-85e7-70037af0e896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 213,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4242c1a6-740d-48a0-868d-20b57e65818c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 209,
                "y": 25
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f0888d8b-05be-42eb-8c0f-3ed1354c5053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 25
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "79f9ba4d-4585-45db-80d8-bd569e7932bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 199,
                "y": 25
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "42788fa6-c309-4467-9837-b91e2083d727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d1ec56c3-5fe6-43f0-9844-b9a9d8a4c508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 190,
                "y": 25
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ec271389-4db9-4086-8963-ad61ee114469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "36f23bb0-c65d-41aa-a77d-e9f3f37f6de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 173,
                "y": 25
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2e20600a-ce14-402f-a856-7b3b908f9ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 169,
                "y": 25
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "776dd3a6-73cb-424f-b7ab-a2f10e48dbb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 163,
                "y": 25
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "44acb889-d3a8-48bf-9a4d-9e8c6c2e3733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 155,
                "y": 25
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "49a22afb-5e4c-4e11-bcf3-9797dbc53ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 148,
                "y": 25
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fdee1335-1130-4758-afa1-f2147ddbf1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 141,
                "y": 25
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "29f2fe71-f3c6-4092-a604-3eb8be9dfebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 133,
                "y": 25
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "650720b3-70b9-42c9-ad79-dffcf45665ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 125,
                "y": 25
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a8653260-52fd-4aba-af73-6618b68d3256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 182,
                "y": 25
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0c042e6d-4312-4634-93c7-78f2b68d564d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "29f61140-0a60-4043-81eb-50a23659926a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 26,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "42049620-c3f4-4c1c-a725-c1ece3c04283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 33,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9f487228-a60d-4a9e-b05d-44e83c2654fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 200,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a3a91c33-f1e2-4070-b138-7a7e534959f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 197,
                "y": 48
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9c578a61-7a38-42b8-a0ef-36ea0193ccab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 193,
                "y": 48
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bb532bb1-ffd9-4ba8-b407-ec3d514b70ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 183,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9e0d46dc-1ef1-4a1c-84b5-27209a9ba21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 174,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6d8a43f3-f73d-4447-a11b-e5cb63d5080a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 164,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6001fe4a-c05d-4bca-9054-f8c643d47842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 158,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6f2f58d1-8283-40cd-9752-9bad0d1e7f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bd52a901-dd60-461f-86bf-6d48d1e789dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 136,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "25a072c5-eadc-43fe-b3d9-84b06e8bb2de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 128,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1b7205d3-cceb-4d07-bd9b-711421529122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c353e5fc-c14d-4941-b658-78474f0456f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a080550c-8a16-47ae-ab5f-656b70eb8215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fca5f5f8-72e9-4bd5-bdfc-642059da0405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 95,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "204121ab-3e68-4ff4-97b9-67397e64eb8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 85,
                "y": 48
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d364b886-c866-4eb2-9efd-b7ffd8607957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 76,
                "y": 48
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f00522b2-3f3f-4b1b-ad05-ad0967008116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 73,
                "y": 48
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e3d70264-00ee-4959-a4db-49175e37b479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 67,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9456d369-f290-4f3a-9346-0c2da48a0d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 58,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "14344bcd-6692-4070-9379-39c4305de92f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5e73cf62-a0cf-48fa-8012-deafabbf8a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 48
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6fd1df77-9e1f-4d1c-88c4-e30e682b9834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 116,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e5ea76a9-9a0c-46c0-892b-8c1caab8c41b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "441e9aee-f5c6-4ba1-b192-c21763cf41e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "79caeea6-8a8e-4950-abc5-0f56dce23f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0a9d645c-24a5-48e8-8e84-601f9205a4fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2bab7282-f707-4022-b78b-445e8d344262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "41b57752-5f18-498b-bed4-b2a4360abc2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "202fdac8-1004-4caa-afa7-3e7b7e4c9320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6c8701ba-02b3-4672-98ee-5b1a7927874f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c7071b8a-9675-4ee3-9056-e78bcb96281a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fbc59995-3491-44f5-ad50-0e76e1cc972d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e7c4d64c-363d-43c1-9866-df447ba31b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "962d0d59-7cca-4bc9-b08b-bca02575c644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "89a0d0fa-03f4-4852-95b7-02a73a358828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0675580b-b151-4eef-9f9b-8a83ce7ba675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8a199a0d-293c-45da-9961-46d5cbf2ae3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "84b27605-5631-473c-ae4e-51d0160d2a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "91b86892-fe86-4fcc-a8e5-2187ba5c2082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "72cec3f3-4860-49d6-9f69-44c3f1fb7502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "49d182c7-72bd-40c7-83c4-ba1521b9e4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c87d4391-d9ec-47a7-95f6-ac4b12ed9a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "dcafa63c-766f-4dac-8bce-d069867a85bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "05a7b7af-2107-45e6-bff9-0df320c36ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "76c59668-5f59-41b7-9b42-1838979cdd3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eb232c35-de07-4001-bd30-068a2d521ce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1399b4df-4931-4f55-874f-ceb97b1f656d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5b13c436-1149-4aa2-a8a9-409a99c11e0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4e6a78bc-e531-4dd1-84e7-ef9e1b11a001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c771f89a-c570-4768-a343-ce6f30904da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "23925b83-5237-42e4-b1bd-9408b73ac90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 78,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "09ad384b-c14d-4783-b99e-a9cb12383c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 74,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fd75ca11-4b13-4c51-8fd9-4a62b24703d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "666ec00a-6a6a-4095-9422-a50ae409b2f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 54,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "28bf0719-e38f-4123-87d2-e4b648592b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "06920a96-0f14-46e1-8d45-34a468828eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 38,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7ccbc8a6-778c-4836-898f-2367b14ee9eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "07dae16e-ba7b-49e8-b376-f7a3494a6b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 23,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3e8ef86b-c21b-4993-966a-8f6d3da10d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 91,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7aabd524-958b-43d2-8b80-bf328c77eff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 17,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3ef66feb-1753-4b44-ade5-e744e7a9feea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9d97788f-fd3a-43c1-a925-7226a1e48c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ebaaa09a-6f3f-4ef0-8098-319a26ba0f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "054ff15e-da2c-4142-80f4-5d63d9c3277e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f4da1bc1-c535-4022-88cb-102eb050c6d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e1aa592d-3e17-4459-b7cc-66de62dde668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5da02a97-b5f8-49f5-9841-124527338f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b19c124b-0d3f-4e46-ad80-ba2820dee857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "41c55291-3e17-4622-8497-2c7523ef274f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5d8dc21e-a3be-4f99-b3bc-ae7991c43eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 208,
                "y": 48
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "14ff5d9b-a599-4596-93c8-3dda7e124caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 217,
                "y": 48
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}