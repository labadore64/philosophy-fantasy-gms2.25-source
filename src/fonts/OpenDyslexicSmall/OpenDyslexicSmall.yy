{
    "id": "20786b2a-5473-4700-b365-3a4f4f3d5952",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "OpenDyslexicSmall",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "OpenDyslexic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "852621df-8c1a-4655-8f55-fb2d0c06f938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bb3281d1-4b91-4881-9822-dd0ff573335a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 7,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5d3b3a5f-176f-45cc-8423-1f9614b5c5ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6e3d4dbe-7c3b-430d-8e87-433c88bb9748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 243,
                "y": 25
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "326160f6-878e-4566-87c3-9cf454230df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 235,
                "y": 25
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5056bbd0-e4de-4c84-a2e8-81d03e832392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1d30bec4-96a8-4ff5-ab20-6b957793e227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 213,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "aa744345-a102-46f5-b744-9ecabee70c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 209,
                "y": 25
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7d5551be-9d41-49d2-b447-9c73adea7c4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 25
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ea24cc82-126a-4881-a551-0de62e246623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 199,
                "y": 25
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "288739a2-f724-47f4-b6b1-612c6df84006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6cfa8abe-05f4-445d-b399-22dde01a78cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 190,
                "y": 25
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "aac2a154-36bc-4eb5-8aa1-7470e791ee73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "07a2c4a4-0f00-4825-8599-4d1c6e6ee67f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 173,
                "y": 25
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5f196e08-bd12-4dd1-9738-83c1b3e388eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 169,
                "y": 25
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "14e935cb-ca4b-45dc-bde2-5e0cb13abb4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 163,
                "y": 25
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cf9fb2b8-55b3-41f5-ade4-c2719ba92192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 155,
                "y": 25
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cb73ecae-e28b-49a7-97c5-bd9b8b51ddaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 148,
                "y": 25
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f76050f4-9cf0-411c-baf2-a99703eb8463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 141,
                "y": 25
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4a17067a-84d5-4be2-83d7-cd715a9afa82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 133,
                "y": 25
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "092ee587-fdcb-47d5-bf43-d764811a4068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 125,
                "y": 25
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "45165143-6a0f-4214-99df-334a380a2364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 182,
                "y": 25
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "55efe531-e003-4bbf-8980-c8a3441dae9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5b780f6a-8497-4d9c-923a-1f207e535629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 26,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c1a8db6f-45a7-42c8-bd27-273768f2ffaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 33,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e1a66e76-27c8-44d8-97d7-11dc0c2f9e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 200,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e535efe9-c051-410d-a654-d3629fb69570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 197,
                "y": 48
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a9497f3a-004b-41a5-b785-b17f262bd9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 193,
                "y": 48
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a2b27187-ca80-4525-ad12-a18e986ad11d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 183,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cee49a37-7037-46e9-be19-43da48f5a005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 174,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "046f160f-b194-4ea4-b480-795d69d15752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 164,
                "y": 48
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bf7ecf3f-8e61-44eb-9d7b-1e585d68e6eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 158,
                "y": 48
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "445048db-58e1-4b9f-9827-add136e6ac30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c2055ed8-882a-47dc-a821-4ab40fcfa8d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 136,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "eb9ba6f3-543d-4313-828a-8455629740bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 128,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7d5bdf85-7a7f-4958-9052-b4638ac0b3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bf7dfe7c-1292-45e6-b51f-3624bf5b8d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "794586eb-08e0-4fff-924d-f55061eda77a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "53842266-d98d-45d3-8620-354562bfeac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 95,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c318d72a-c79b-480f-ba9f-152ac502db43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 85,
                "y": 48
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3f771998-3725-45bf-b97e-cb5c10b646e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 76,
                "y": 48
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "56489726-3cda-404c-9c4d-6aec9773ad41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 73,
                "y": 48
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e81ed0bb-2f75-489a-8bc0-f5dffe252c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 67,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "873899c3-04b9-4a45-a7ec-5b79b4ff5c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 58,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2667f86f-3d52-4e56-ad52-b165e8f4eae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e50320c3-e796-4db2-b521-8858533dcfdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 48
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0c6cca4b-0674-496f-8375-0eaa9dd2db64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 116,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b6e22367-88f4-4177-92cc-73eaed31b020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0e3c53d1-715a-4043-8537-66184e0aa9f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "81e6f2c3-c26d-40c3-959a-08ce7d7a33a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c04fa1c1-78db-49b3-bcf8-f44eabcaeea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6550d10b-9b89-4071-b0a9-f37e2de92dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d9578b9c-c20b-4626-b7a9-b99f9c8a2bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "259109dc-149c-4e87-9601-a8e5b0bd0972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5a478189-eccf-4dcd-a8a7-219555b7f174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "dcd4578c-84f7-497b-9317-783ad534f961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "370f6295-f12c-4ace-9b6b-18a1d866c80b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "298628f4-0b7a-4ddf-8dd7-8c077f2dcde9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8443e51b-ed2e-42b9-a1de-897e2c685df4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8b81333b-f49f-47ce-85e7-807094a5d733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "79098fcb-24a8-497d-a855-851b0921a96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "416df52d-fa43-4ff6-9b23-08a7af939eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ddc2eb8c-8f3b-44a2-84d7-bb097526292f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c62cb91d-6af7-4d2d-a4b3-3d7f76ad1294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c9076892-f1ac-4994-ab11-0e67841eaddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9eecbc18-a1c7-40de-8a60-bbba937ba17b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0842cdf6-51f3-4256-aee0-70b00c8f542c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bba36dc7-301f-46d8-88a5-a4ed1c5d93e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "51ef5fe6-7a05-4310-ae00-135f5c2dac03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e7fb07e1-d48a-47cd-a839-f1348cc5e000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "376be347-6a70-4145-a3d4-ec5f3a4e8a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f659dfc0-f99b-4efe-b3fa-e02b131b16e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "569c08c6-5ff2-4f0b-a3b4-fcd3e0a1e861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3f743162-790a-4092-bb62-028d94f97b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "707efcd7-236a-4812-bee6-76bd919befa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "25a92a04-d297-4ad8-bab0-d07ffefb7171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 78,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b49fe9fc-0164-4bc8-868e-debd8b12e578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 74,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3c604092-8a13-4381-a812-9ac1bd0014a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "696d59c8-3fc6-41ae-819b-b72ac23d45cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 54,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7e348979-649b-4686-9f57-25f5040a539f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d44a0304-61c7-487d-9169-8b40a241baa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 38,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "941c0bf9-63f9-41b7-9e95-2719738fd619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6244e49d-9d2c-445d-a9c3-179913323c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 23,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6fe8eb66-3ae9-4385-8188-81cb9beb7c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 91,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "204a98df-7dfd-4b3b-9896-c435576d8928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 17,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b4af72e5-c745-42db-96be-4eff17c8e97b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8dc00f10-bedb-440b-b1f1-65a7cc3ef267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b16b6078-57ea-4f2a-8d86-bf601f7ff60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "18721cb9-032e-460d-874c-b12e780fb46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cde45952-06fa-4722-93c2-d3aa162e000f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5b0c28c6-2f07-4ed7-9f94-ece34f69008a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b809e5a6-f389-4821-9722-6ae24753092e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "29044d66-a530-448d-8cf2-741d723070ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f2403f75-e1e6-45a2-b8cc-a32e2b3ad3c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5c03fc59-163e-4efc-9e3c-6c9294ecf343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 208,
                "y": 48
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "044a8ec5-85fc-4577-9489-30853cc28856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 217,
                "y": 48
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}