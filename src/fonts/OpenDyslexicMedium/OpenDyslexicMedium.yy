{
    "id": "0da4b666-464c-47ca-a968-80082565d1ef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "OpenDyslexicMedium",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "OpenDyslexic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f46d7afe-20b9-41b5-94ee-c021269b6205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ff5619d7-7db1-4949-ba79-101b30cffb09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "54fac169-faed-446a-ad55-3f2407a7d6e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 247,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5e76ae4a-accc-438c-8153-f1f39a6d78ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 232,
                "y": 72
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1e848d37-5cea-45cf-a2d2-28517bec7008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 221,
                "y": 72
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "05927b34-b577-4d12-9497-9ac8282faccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 203,
                "y": 72
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "26d8f87c-d74d-44a9-b4cc-3fb51225402e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 188,
                "y": 72
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9940c8b0-716d-457a-940f-a08dd47bb8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 184,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fb02c8dc-43db-42ad-9c1f-fcaff90493b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 177,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a2ba702e-a20d-4c67-a0ab-ea46f3f34bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 170,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "88f5ecdc-fabe-4ecb-9060-a5e5b0a0fd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 7,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a38fefe2-3e67-407c-9511-9d86678ed0e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 72
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "971d746a-1918-4404-bcd6-797aa5379579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 139,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f35af3b2-a25f-4ee4-9882-59570601faa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 132,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1f0084f4-6f93-4fd7-aa24-e247b128d183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 127,
                "y": 72
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f806022f-f7fc-4e0b-8990-b24e6ffcb95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "af73de88-035b-4214-9d77-6a54db7fbcf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 106,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f023512a-b594-4e4f-bd10-c4813ff80aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 96,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f8727172-8a54-499f-a28b-7f45276b4cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 85,
                "y": 72
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d64fadde-a882-4934-b06d-d304ff3639bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 74,
                "y": 72
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3882091d-581e-4a12-84ef-bae3b6910f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 62,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b33f52b5-22bc-47e5-a512-48f80ac57968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 145,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7d5461b4-b2cb-49a2-bc31-c2c1f1c25b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 31,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "92596338-1d69-4545-9fa2-806d12fee7bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 154,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "910d4b0e-ca8c-4bb8-9101-fd54b6b2acc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 43,
                "y": 107
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "135c75c0-e863-4c70-9984-f44340c38279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 27,
                "y": 142
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9952eace-9524-402f-9521-dc70b8adc894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 22,
                "y": 142
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7de1f8fa-04e7-4740-a5de-c511e4c682bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 17,
                "y": 142
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5bde89dc-0506-4720-a5af-20d190a9a64e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f24fceb5-adda-4106-adb6-0a6ae6262538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 237,
                "y": 107
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1b57583f-90ff-4d1f-a5bb-b34e6f92bbe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 222,
                "y": 107
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0fcfd422-cecc-4fbb-930e-da291308c798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 107
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3e68ea72-d3f7-4e34-a425-f41763c3e073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 193,
                "y": 107
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bda0ba38-3ae1-49a5-b11f-bff9f99e9a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 178,
                "y": 107
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b6afb622-82fb-4d24-91eb-688b33211909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 39,
                "y": 142
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f13b23e7-7bc3-4bc0-8f16-5983588a29a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 165,
                "y": 107
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b00dbe5d-72c2-4876-915d-2912d7271cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 107
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "44909526-3047-43e0-a0a2-20f5428ec439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 129,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "79d26cff-3295-4e1b-9c27-d0c1ade3e43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 119,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "99718b5f-460d-4ffa-92f4-0c6e44417930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 105,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d0a287a0-c172-4988-80c2-29aa2bbdbf44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 92,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b2762a41-64e7-4e95-b982-9f401e86b475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 87,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "75251c6e-93f7-4e46-858a-9c4576c55456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 79,
                "y": 107
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "eddbda8b-3d75-4798-9277-2e58ec187927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 66,
                "y": 107
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "adf582ef-2425-42e4-9915-e4322aeaf103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 55,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8439c8f4-7f01-49aa-89be-c57b7d592d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 47,
                "y": 72
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "497b39a8-107e-4ed8-89bf-82dcadc6da45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "48145a6a-738b-4912-83b0-e56d1486545f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ae4efd2e-6078-46f3-82af-93264a2d1526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 15,
                "y": 37
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "99cc0f36-e7a7-475d-ad4b-3d8189a0d20a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "264ee8de-42a9-4738-bbf6-4579b743cad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e843712f-2249-420a-b4bf-a33ea26fd643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d2683825-4de7-4f78-bbde-f0c9b1309f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "71b1abc5-4199-4f88-a8dc-4c8e7d562932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4735afea-e47b-4682-9988-fdbb3cbbbd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c60bb044-6ba4-41b2-bbee-b21e2cc07b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7534cf98-9f16-491b-afeb-1a9976227fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bcbbaa9e-f5ad-43ad-aff0-12d44f00d75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cf29b2c8-77d7-4597-b442-2c4c3f64e684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "84cda005-be35-4ab5-8695-21f3f0cf5d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fc5e2fbf-8be3-43b4-82ac-cfe71dc197b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0b4cd648-f3c3-40fb-8768-23174381f66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "19f46b4f-8443-49f0-a77a-9ff93f5515ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b89e4f63-35a0-4ee1-a398-72459d239377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "88fd9537-2a85-4376-ad56-02351f498651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c6f2f3d8-354e-4246-85ae-0fbfa55770d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4bac2ea4-8f8f-4ba3-88f1-77f46abf76bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2cae0a53-d373-4beb-92bc-3c60d022795b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c2a8e45a-cc83-4955-8b80-4dae0b4d82a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2a200b11-495f-4e6c-ba2e-b5d79e69750f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4fcd1206-2904-4651-af66-0622aafc26c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 27,
                "y": 37
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6d98b7fe-b02f-4aad-b8e7-6b4cb2ed45c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 145,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "92875252-f7e9-4dc3-b884-80427468817b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 35,
                "y": 37
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f5aa8ea8-0ad3-427b-b817-fc8be74fa06a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 9,
                "y": 72
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3858ee7b-7f21-404f-b24d-4c5d7d7d8eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "37a8447c-5ee9-434c-928e-738e157055a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 240,
                "y": 37
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b82a94c2-df39-47b9-84ca-81e8aa8b6f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 234,
                "y": 37
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "21a4b91c-96a2-4aca-99a3-a8197a45fb83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 216,
                "y": 37
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "72ce66e2-5503-4802-84dd-524c20bf4488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 204,
                "y": 37
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4ccc1e54-9f80-432c-b151-7a0ff01b406c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 192,
                "y": 37
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d573f4d3-359e-459b-948d-8cb93ecfe288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 180,
                "y": 37
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2980e4c8-626a-4356-9973-d17744c48654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 167,
                "y": 37
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a58ed062-20bb-4acb-ab29-637ef491f7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 14,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "107dba1b-cd84-45e9-b070-b248e3f888ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 157,
                "y": 37
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f1e74d89-a5c4-4493-9007-f0261e06c537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 136,
                "y": 37
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "eb7beece-dde4-4e64-8ef8-bdfd3f90a932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 125,
                "y": 37
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e444104d-d5f7-4e85-96c0-74e7070b1940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 113,
                "y": 37
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "45e632f3-9111-4583-958e-e229ee1bb7c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 97,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a5443dca-5101-40fd-995c-ced7ab0f88ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 84,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "57d10c2f-198e-4fb7-a822-163ad7763267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 72,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "da288f23-ffd0-40ef-ba73-f00a6d2ad47e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 61,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1ef59c1d-6251-48cb-8598-5108206e4d89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 51,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "17bf97c8-7aed-4be4-820f-9b71615e3387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 46,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2febf925-25c3-4589-88da-95be8882249b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 23,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "835c2cb2-0908-48ee-9467-0a4b6288c339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 51,
                "y": 142
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}