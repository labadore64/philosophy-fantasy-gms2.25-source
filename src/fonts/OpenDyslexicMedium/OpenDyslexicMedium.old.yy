{
    "id": "0da4b666-464c-47ca-a968-80082565d1ef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "OpenDyslexicMedium",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "OpenDyslexic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cec05f61-8c98-487b-aee3-f34237d43bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7588239d-647d-41fe-8435-265531907047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1e61d05d-6d4e-4f45-88c5-4a6a3cb99284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 244,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "704bc4e5-068d-40d7-a273-a9a04b40d73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 229,
                "y": 72
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fdc18cbc-387c-4b74-9c4a-eba352aa9338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 218,
                "y": 72
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e3b36feb-7118-4743-afee-b4935738d2c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 72
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fdf0e685-2f9e-413d-aca5-9eb43c22ec3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 185,
                "y": 72
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "925fdb73-0faa-4f64-9ed7-0bea98e9c57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 181,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f70fdfab-f8d4-4dee-a491-cea9be2bdcf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 174,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "76321383-bd98-4b0d-adb4-8e288094aa9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 167,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "27ece45f-a09f-4320-b9d2-97a4ed9a442b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 7,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1f46bbbb-3714-4b42-a9a1-4b006bbffed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 153,
                "y": 72
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b1cbca8a-805f-465d-8353-987cd4f3a082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 136,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "153ef320-d183-4726-862f-2c6e7fa4c651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 129,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ef24b919-543e-4a36-bf2b-585b69a798e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 124,
                "y": 72
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "868fa5df-c4e0-4455-80ee-0278bdd2530c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 115,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fe33d364-8cd0-49e3-aa23-c7a77e5abc68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4c8b1060-6b4c-43f1-88f0-95799444b15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 93,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cd638aa9-562f-4028-a09d-e3d59942244f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 82,
                "y": 72
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "30d870e5-8670-4896-b204-ddcbb063bdb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 71,
                "y": 72
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d8032e73-ad7d-40de-9963-eb6842e78997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 59,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "530b7c23-f284-41e7-88fe-ce8a208c7fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 142,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "aa4f7b75-fda2-4f65-9e4f-919e8afb1a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ed4ef3bc-17a9-457e-a138-4f21a6f36c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 30,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a05b3b2f-922f-4ea5-baf4-acfe9301a253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 107
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "859a51d8-87d1-4e78-8feb-2c9e0a4df046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 142
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "58eb6d2a-3f77-4437-a09f-13b0e703eff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 36,
                "y": 142
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "95b462f2-754f-4ffb-89d6-c439e8ef326a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 31,
                "y": 142
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "07b685a9-3f8b-4b2d-b95d-c55e09d24e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 16,
                "y": 142
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9f5560e7-f259-4c40-a261-10ca01c33eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fd8925ab-ab73-40f6-b989-515d79dace5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 236,
                "y": 107
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e542cc64-84da-474d-96b9-ac351d5a9494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 226,
                "y": 107
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4db5f68e-73cd-48b0-9b71-22e207101be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 207,
                "y": 107
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9c4ae8fa-e825-4782-8f51-7d92f23b5582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 192,
                "y": 107
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e0fcf3ae-1cf3-48ec-9630-c3e04f8a0285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 180,
                "y": 107
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e13330ed-3a41-497d-95fa-ea7255db537d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 167,
                "y": 107
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a07c5030-d096-4a9f-a9bb-8a567b7c2c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 153,
                "y": 107
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6e27a90c-7055-4ef4-adc0-6f55309bc59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 142,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9b2d8833-dd0d-4b51-849e-2242456870e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 132,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2113c186-60cb-418a-bf87-9622d231eb71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 118,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5e213f24-dc94-422b-a2a9-42b6811f2fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 105,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "cf02e7fe-aa0a-4c53-a5ce-c5e2b645867b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 100,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "53f58473-02c6-4cc9-bd9a-5e6cbe826dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 107
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8254e64a-e774-4dfc-9231-c26a18c9a1ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 79,
                "y": 107
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "243b5a3c-3361-41c3-9b95-7969a4fa7826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "edeaf6f3-9104-4bfc-9b5e-49d078a2e3aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 53,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d168bbbe-6832-4626-9987-459598f60ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 46,
                "y": 72
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "85be1bd3-2487-4fb7-8f1f-3ac3178d905e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 31,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "964d4d69-ef6f-4c41-aa98-1f948166276d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 19,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2cb94a3e-f64c-4d27-bcb7-33f6a7a96df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 8,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "edccafce-b651-4741-8de7-d0fc03fa003a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bda9aa8c-f11d-48ed-ac65-03fe269d0344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "380ef925-7d1d-4b11-81cc-dfc1a9b056b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "212ab8e2-69d4-4384-8401-746155c3f1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5b326402-6bdb-4c06-bd80-b4ad38d6be8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c895d222-7eb8-4f01-a483-14c2f3bd4849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9f4d43b2-1132-4ff8-b982-df294f485177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8be0c562-8cdc-4d46-8013-d082b61fc98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cc4a5396-eed9-4f54-9bee-056f6621d22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "add9cf8e-7926-478f-a6b2-b696b3ac7be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "50cbfd7b-751f-40f3-961c-7571c3ffe543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d142a52a-56c5-469d-8367-988a590daaeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a310f8f5-3791-4dee-bb4c-6a58dfe15246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e5905fd5-ed7f-40c4-a16e-eabe3882daff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "323fc3c1-9a0b-42ce-bdb3-02788267c836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5320cc17-345a-4ee7-abec-696a31aaf993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b71e1b07-ec73-4600-85f1-454f63817060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9898541e-e6ef-42ec-a39c-430dfc550adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bb4e86a3-4423-4099-9a10-a02b779052a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ebd8107d-52a0-468a-92dd-642bb4a24373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "249be7ee-15e7-4167-bf82-b5297836c82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fbcd3841-0c23-4c33-805a-13e02a331c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 24,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9101be10-c805-4c63-885f-761fb17c62a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 140,
                "y": 37
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "186847bd-c684-45ab-a58e-520d947109bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 36,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "60eb3b46-51d4-4840-b2b7-a37cf1b41da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8f31b9ed-a1e5-4aab-8e00-047632b02eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 37
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "767643d5-d22c-4c20-9760-e07e3c061192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 236,
                "y": 37
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "962fc216-2a77-4e71-b1eb-6e22c3261d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 218,
                "y": 37
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8acaedd7-4186-4931-ad4d-0d042f90d044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 37
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "27fa6ea8-cdac-4e88-ba3c-0c999f4ff02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 37
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "27262c5c-28aa-49d5-9651-ffbee75b6a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 37
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1bc2e684-9782-4abe-bf54-8e129d001354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 169,
                "y": 37
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1451cf9b-91eb-41c5-83e2-8a5af4221737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 160,
                "y": 37
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9161f7a2-75fb-4d53-a6a4-0f8525487bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 9,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3e0bbb9c-f1b5-4c2e-864f-02ceb99d58ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 37
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7200f26f-f50f-4bd1-a9ef-e50482cbb3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 129,
                "y": 37
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b69d482d-1411-4a5d-a266-9195911c7560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 117,
                "y": 37
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d94707c8-0a93-4378-b3c9-d7263bcf8a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 101,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fac8add9-33ca-4e71-8fd5-b73d427dc3d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 88,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4a1f9164-17ec-4624-9129-0857908da21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 76,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e1416554-86b1-4723-b978-1b1488d37d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 65,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b20d03dd-dc02-4906-871c-da522e4b9864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 55,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "363ca81a-a799-465b-9ae8-6d27d5b4a91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 50,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b1969e2f-a9d9-4247-844b-2b0093cc3749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 41,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "40db8c4d-1fbf-432d-b648-e5f22da9fec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 53,
                "y": 142
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "143677bf-94d1-4a35-a7a1-a399be5bc6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 67,
                "y": 142
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}