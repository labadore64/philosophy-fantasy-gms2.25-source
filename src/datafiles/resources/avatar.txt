id:Background
name:Background
x:0
y:0
scale:1
sprite:cute
None
Birds
Hearts
Diamonds
Clubs
Spades
Two Arrows
LGBT Flag
Trans Flag
Bubbles
===============
id:Back Hair
name:Back Hair
x:0
y:5
scale:1.4
sprite:back_hair
None
Medium Straight Messy
Short Straight Clean
Long Straight Clean
Thick Straight Dreadlocks
Medium Curvy Straight
Short Curly Messy
===============
id:Body
name:Body
x:0
y:-15
scale:1.2
sprite:body
Thin
Thick
===============
id:Neck
name:Neck
x:10
y:-20
scale:2.5
sprite:neck
Curved
Narrow
Thin
Straight
Thick
===============
id:Shirt
name:Shirt
x:0
y:0
scale:1
sprite:shirt
T-Shirt
T-Shirt Breasts
Sweater
Sweater Breasts
===============
id:Necklace
name:Necklace
x:0
y:0
scale:1
sprite:necklace
None
Long Scarf
Short Scarf
Pearls
Solid
Choker
Bow
===============
id:Face
name:Face
x:5
y:3
scale:1.75
sprite:face
Round
Rigid
Big Cheek
Pointed
===============
id:Mouth
name:Mouth
x:20
y:-30
scale:4.5
sprite:mouth
Smile
Neutral
Sad
Grin
Cute
Small Frown
Small Smile
Tongue Sticking Out
Happy Open
Grin with Teeth
Lipstick
Squiggle
Unsure
Whistling
Small Round Frown
Smirk
===============
id:Nose
name:Nose
x:35
y:-12
scale:5
sprite:nose
Small Pointed
Small Round
Medium Pointed
Medium Round
Medium Hooked
Medium Flat
Large and Pronounced
Large Round
Large Flat
Large Pointed
Puppy
===============
id:Eyes
name:Eyes
x:15
y:0
scale:2.6
sprite:eyes_white,eyes
Neutral
Tired
Angry
Sad
Excited
Pensive
Closed
Happy
Swirly
Neutral Eyelash
Tired Eyelash
Angry Eyelash
Sad Eyelash
Excited Eyelash
Pensive Eyelash
Happy Eyelash
Dots
Small Black
Large Black
Empty
Cat
Spider
===============
id:Eyebrows
name:Eyebrows
x:10
y:10
scale:2.5
sprite:eyebrows
Angry Thin
Relaxed Thin
Angry Thick
Relaxed Thick
Angry Small
Relaxed Small
Angry Hairy
Relaxed Hairy
Angry Bushy
Relaxed Bushy
===============
id:Glasses
name:Glasses
x:0
y:0
scale:1
sprite:glasses
None
Thin
Big Round
Sunglasses
===============
id:Ears
name:Ears
x:-35
y:-3
scale:3.5
sprite:ear
None
Medium Round
Small Round
Small Lobed
Pointed
Elf
Cat
Hole
===============
id:Earrings
name:Earrings
x:0
y:0
scale:1
sprite:earring
None
Basic
Ring
Large Crescent
Long Earring
===============
id:Front Hair
name:Front Hair
x:7
y:15
scale:2
sprite:front_hair
None
Medium Straight Messy
Short Straight Clean
Long Straight Clean
Thick Straight Dreadlocks
Medium Curvy Straight
Short Curly Messy
===============
id:Hat
name:Hat
x:0
y:0
scale:1
sprite:hat
None
Dark Top Hat
Light Top Hat
Baseball Cap
Antlers
Antennae
Bunny Ears
Feather
Small Devil Horns
Long Devil Horns
Bow
===============
id:Snout
name:Snout
x:0
y:0
scale:1
sprite:snout1,snout2
None
Puppy
Finch
Heron
Crow
Parrot
Eagle
Crocodile
Jagged Snout
===============