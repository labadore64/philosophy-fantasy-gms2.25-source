1. Low Vision Mode:

To set up the defaults for low vision mode, move the file config_lv_default.ini to

%appdata%/WhatIsPhilosophyAnyways

And rename it to config.ini, before starting your game.

2. Customization

a. Cards

Cards are stored as individual .ini files in resources/card/default. The game will automatically load all of the cards that are numbered into the game. It will also load sprites in resources/card/default/sprites.

You can modify a card's sprite by setting the "sprite" key in the card data file. Then, name the sprite file "spr_card_[the sprite name].png" and it will load. Make sure your sprite is 256x256 pixels and a png file.

For example, "Freud, Sigmund" has "s_freud" as his card name and his sprite name is "spr_card_s_freud".

b. Deck

Decks are stored in resources/deck . The first 3 decks are the starter decks, and are also used for the first 3 opponents by default. The rest of the decks represent decks used by opponents. 

"main" - is mostly unused. This card gets added to your trunk when you pick your starter deck, so Deck Build always has at least one card in your deck or trunk at all times.
"card" - is numbered and each card will be added to the deck. Name the card in lowercase. You can use part of the name, but there isn't a guarentee that it will pick the card you want if they share the same substring (for example "Weil" or "Calvin")

c. Booster Packs

Booster packs are obtained after victorious battles, in story mode or in free game. 

"cover" - The cover image of the booster pack. Name the philosopher you want to add.
"event" - This is the event that needs to be set in order to see this booster pack on the selection screen. make sure to edit resources/event/event.ini to include the event so that it can trigger/read.
"name" - The display name.
"desc" - The description.
"r", "g", "b" - Red/Green/Blue values used for tinting the booster pack's color.
"card[#]" - Each card in the booster pack list.
"rate[#]" - The rate each card will be pulled. (note: I'm going to change this since it kinda sucks right now.)