//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float seed;
uniform float counter;
uniform vec2 u_uv;

void main()
{
    vec4 colour = texture2D(gm_BaseTexture, v_vTexcoord);
    vec3 sepia = vec3(0.0);
	
	float uv = (v_vTexcoord.x)/(u_uv.x);
	
	float change = 0.25;
	
	vec3 red = vec3(cos(uv+v_vTexcoord.y+counter*.0029*change)*0.25 + 0.25);
	vec3 green = vec3(cos(uv+v_vTexcoord.y+counter*.037*change)*0.25 + 0.25);
	vec3 blue = vec3(cos(uv+v_vTexcoord.y+counter*.051*change)*0.25 + 0.25);
	
    sepia.r = dot(colour.rgb,red);
    sepia.g = dot(colour.rgb, green);
    sepia.b = dot(colour.rgb, blue);
    gl_FragColor.rgb = mix(colour.rgb,sepia.rgb, 1.0);
    gl_FragColor.a = colour.a;
}
