trunk_display[0] = -1
trunk_quantity[0] = 1;
trunk_total = 0

deck_display[0] = -1
deck_total = 0

// start trunk list
var counter = 0;
var sizer = ds_list_size(obj_data.current_deck);
for(var i = 0; i < obj_data.cards_total; i++){
	deck_display[i] = all_cards[|i];
}

if(menupos > menu_count-1){
	menupos = menu_count-1;
}

if(menupos < 0){
	menupos = 0	
}

if(option_top > menu_count-per_line){
	option_top = menu_count-per_line;
} 

if (option_top < 0){
	option_top = 0	
}

var counter = 0;

var sizer = array_length_1d(trunk_cards);

for(var i = 0; i < sizer; i++){
	trunk_display[i] = trunk_cards[i]
	trunk_quantity[i] = 0
}

trunk_total = sizer 

deck_total = array_length_1d(deck_display);

// sets draw stuff

if(menu_selected == DECK_BUILD_TRUNK){
	menu_count = trunk_total;
	
	if(menupos >= menu_count){
		menupos = 0	
	}
	
	for(var i = 0; i < per_line; i++){
		if(option_top + i < trunk_total){
			draw_id[i] = trunk_display[option_top+i] 
		} else {
			draw_id[i] = -1;	
		}
	}
	selected_card.card_id = trunk_display[menupos]
} else {
	menu_count = deck_total;
	
	if(menupos >= menu_count){
		menupos = 0	
	}
	
	for(var i = 0; i < per_line; i++){
		if(option_top + i < deck_total){
			draw_id[i] = deck_display[option_top+i] 
		} else {
			draw_id[i] = -1;	
		}
	}
	selected_card.card_id = deck_display[menupos]
}

menu_surf_update = true;
if(selected_card.card_id > -1){
	selected_card.name_display = obj_data.card_name[selected_card.card_id]
	selected_card.surface_update = true;
	selected_card.image_blend = obj_data.type_color[obj_data.card_element[selected_card.card_id]];
	with(selected_card){
		if(card_id > -1){
			card_attack_power = obj_data.card_attack[card_id]
			card_defense_power = obj_data.card_defense[card_id]

			surface_update = true
		}
		card_reset_size();	
	}
}

menu_clear_option()

	if(menu_selected == DECK_BUILD_TRUNK){
		for(var i = 0; i < trunk_total; i++){
		menu_add_option(obj_data.card_name[trunk_display[i]],"",-1,-1) 
		}
	} else {
		for(var i = 0; i < deck_total; i++){
		menu_add_option(obj_data.card_name[deck_display[i]],"",-1,-1) 
		}
	}
	
if(selected_card.card_id > -1){
	ax_card(selected_card.card_id)
	if(obj_data.current_main_card > -1){
		access_string[6] = obj_data.card_name[obj_data.current_main_card] + ", favorite philosopher"
	}
	access_string[7] = string(deck_total) + "out of 20, deck size"

	archetype_string = obj_data.card_archetype1[selected_card.card_id]

	if(string_lower(obj_data.card_archetype2[selected_card.card_id]) == "none"){
		access_string[8] = obj_data.card_archetype1[selected_card.card_id] + " archetype"
	} else {
		archetype_string = obj_data.card_archetype1[selected_card.card_id] + "/" + obj_data.card_archetype2[selected_card.card_id]
		access_string[8] = obj_data.card_archetype1[selected_card.card_id] + " " + obj_data.card_archetype2[selected_card.card_id] + " archetypes"
	}
	if(menu_selected == DECK_BUILD_TRUNK){
		access_string[9] = string(trunk_quantity[menupos]) + " quantity"	
	}
}

// scroll display
scroll_increment = (510-20)/menu_count //increment size

// event user
event_user(0)