with(argument[0]){
	var key = argument[1]
	if(!is_undefined(status_effects[? key])){
		status_effects[? key] -= argument[2];
		if(status_effects[? key] < 0){
			status_effects[? key] = 0;	
		}
	}
}