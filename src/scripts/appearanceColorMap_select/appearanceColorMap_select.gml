if(menupos < 7){
	var pos = menupos-1;
	var val = string_digits(stringer);
	
	if(selected && val == "" && argument_count == 0){
		soundfxPlay(sfx_sound_menu_cancel)		
	}
	if(selected){
		selected = false;
	
		if(val != ""){
			soundfxPlay(sfx_sound_menu_twinkle)
			val = clamp(real(val),0,255)
			if(pos < 3){
				color_rgb[pos] = val
				appearanceColorMap_update_rgb()
			} else {
				pos-=3;
				color_hsv[pos] = val
				appearanceColorMap_update_hsv()
			}
			stringer = ""
			soundfxPlay(sfx_sound_menu_twinkle)
		}
	} else {
		appearanceColorMap_mouseSelect(menupos+1)
	}
} else {
	if(argument_count == 0){
		if(menupos == 8){
			if(!color_selected){
				selected = true
				color_selected = true	
				soundfxPlay(sfx_sound_menu_keyboard)
			} else {
				selected = false
				color_selected = false
				soundfxPlay(sfx_sound_menu_cancel)
			}
		} else {
			if(!selected){
				selected = true	
				soundfxPlay(sfx_sound_menu_keyboard)
			} else {
				selected = false
				soundfxPlay(sfx_sound_menu_cancel)
			}
		}
	}
}
surf_main_update = true