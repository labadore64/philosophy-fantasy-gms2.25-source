var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

with(mainMenu){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		mainMenu_DrawBackground()	
	}
}
with(duelController){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		draw_duelbackground()
	}
}
with(deckBuild){
	if(global.high_contrast){
		draw_clear(c_black)	
	} else {
		deckBuild_drawBackground();
	}	
}

if(!surface_exists(surf)){
	surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	surface_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h);
		surface_update = true;
	}
}

var bla = global.menu_bgcolor;

if(global.high_contrast){
	bla = c_black;	
}

if(surface_update){
	surface_set_target(surf)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)

	draw_set_color(bla)
	draw_set_alpha(1)
	draw_rectangle_noratio(760,option_y_offset-10,800,600,false)
	draw_set_alpha(.75)
	draw_rectangle_noratio(0,0,800,600,false)
	
	draw_set_alpha(1)
	draw_set_color(col)
	draw_set_font(global.largeFont)
	draw_set_halign(fa_center)
	draw_text_transformed_noratio(400,30-5,"Options",4,4,false)
	draw_set_halign(fa_left)
	draw_set_font(global.normalFont)
	
	draw_set_color(col)
	if(global.high_contrast){
		draw_set_alpha(.75)
	} else {
		draw_set_alpha(.45)	
	}
	draw_rectangle_noratio(761,option_y_offset+scroll_increment_draw*(option_top)-10,800,min(560,option_y_offset+scroll_increment_draw*(option_top+per_line)-1),false)
	draw_set_alpha(1)

	
	// draw actual options
	var counter = 0;
	for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
		if(draw_option_bg[i]){
			/*
			if(counter == selected_draw_index){
				if(global.high_contrast){
					draw_set_alpha(.6)	
				} else {
					draw_set_alpha(.4)
				}
			} else {
				if(global.high_contrast){
					draw_set_alpha(.25)
				} else {
					draw_set_alpha(.1)
				}
			}
			draw_rectangle_noratio(0,option_y_offset+3-10 + option_spacer*counter,
								760,option_y_offset-3-10 + option_spacer*(counter+1),
								false)
			*/
			gpu_set_colorwriteenable(true,true,true,true)
			if(counter == selected_draw_index){
				draw_set_color(c_white)
				if(global.menu_animate){
					draw_set_alpha(alpha_adder);
				} else {
					draw_set_alpha(alpha_max)
				}
				if(global.high_contrast){
					draw_rectangle_noratio(0,option_y_offset+3-10 + option_spacer*counter,
								760,option_y_offset-3-10 + option_spacer*(counter+1),
								false)
				} else {
					draw_rectangle_color_noratio(0,option_y_offset+3-10 + option_spacer*counter,
								760,option_y_offset-3-10 + option_spacer*(counter+1),
						global.menu_selcolor[1],global.menu_selcolor[1],
						global.menu_selcolor[0],global.menu_selcolor[0],
					false)
				}
			} else {
				draw_set_color(bla)
				draw_set_alpha(.75)
				draw_rectangle_noratio(0,option_y_offset+3-10 + option_spacer*counter,
										760,option_y_offset-3-10 + option_spacer*(counter+1),
										false)
			}
		} else if(draw_title_bg[i]){
			draw_set_alpha(1)
			draw_set_color(bla)
			draw_rectangle_noratio(0,option_y_offset-10 + option_spacer*counter,
								760,option_y_offset-10 + option_spacer*(counter+1),
								false)
			gpu_set_colorwriteenable(true,true,true,false)
		}
		draw_set_color(col)
		draw_set_alpha(1)
		draw_text_transformed_noratio(option_x_offset,-7+2+option_y_offset + option_spacer*counter,
							string_copy(menu_name[i],1,30),3,3,0)
		
		draw_set_halign(fa_center)
		draw_text_transformed_noratio(700-20,-7+2+option_y_offset + option_spacer*counter,
							option_visible_draw[i],3,3,0)
		gpu_set_colorwriteenable(true,true,true,true)
		draw_set_halign(fa_left)
		counter++;
	}
	
	// draw thingie
	draw_set_color(global.text_bgcolor[0])
	draw_set_alpha(1)
	draw_rectangle_noratio(0,560,800,600,false)
	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_color(col)
	draw_set_halign(fa_center)
	draw_text_transformed_noratio(400,570,menu_desc[menupos],2,2,0)
	draw_set_halign(fa_left)
	surface_reset_target();
}

draw_surface(surf,global.display_x,global.display_y);
	

menuParent_draw_cancel_button()

draw_letterbox();