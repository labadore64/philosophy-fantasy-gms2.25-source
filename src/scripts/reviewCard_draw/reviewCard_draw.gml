if(global.high_contrast){
	draw_clear(c_black)	
} else {
	deckBuild_drawBackground();
}

var fadescale = 2

if(!surface_exists(fade_surface)){
	fade_surface = surface_create_access(fadescale*245*global.scale_factor,fadescale*256*global.scale_factor)	
	fade_surface_updated=true;
} else {
	if(ScaleManager.updated){
		surface_resize(fade_surface,fadescale*245*global.scale_factor,fadescale*256*global.scale_factor);
		fade_surface_updated=true;
	}
}

if(fade_surface_updated){
	//preview_philosopher	
	surface_set_target(fade_surface)
	draw_clear_alpha(c_black,0)
	gpu_set_colorwriteenable(true,true,true,true)
	if(draw_the_menu){
	draw_sprite_extended_noratio(preview_philosopher,0,30,0,
								fadescale,fadescale,0,c_white,1)
	} else {
	draw_sprite_extended_noratio(review_philosopher,0,30,0,
								fadescale,fadescale,0,c_white,1)	
	}

	gpu_set_colorwriteenable(true,true,true,false)
	if(obj_data.card_sprite[selected_card.card_id] > -1){
		draw_sprite_extended_noratio(obj_data.card_sprite[selected_card.card_id],0,30,0,
									fadescale,fadescale,0,c_white,1)
	}
	gpu_set_colorwriteenable(true,true,true,true)

	surface_reset_target();
	fade_surface_updated = false;
	surface_update=true
}


// draw RARE CARD

if(!surface_exists(menu_surf)){
	menu_surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	menu_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(menu_surf,global.letterbox_w,global.letterbox_h);
		menu_surf_update = true;
	}
}

if(menu_surf_update){
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	surface_set_target(menu_surf)
	
	draw_clear_alpha(c_black,0)
	
	gpu_set_colorwriteenable(true,true,true,true)

	var bla = global.menu_bgcolor;

	if(global.high_contrast){
		bla = c_black;	
	}

	draw_set_color(bla)
	draw_set_alpha(.5)
	draw_rectangle_noratio(10,0,455,510,false)
	draw_rectangle_noratio(475,0,790,510,false)

	// infobox
	draw_set_color(global.menu_bgcolor)
	draw_set_alpha(1)
	//draw_rectangle_noratio(0,485,800,600,false)

	// text


	draw_set_font(global.largeFont)
	draw_set_color(global.text_fgcolor)
	if(menu_selected == DECK_BUILD_TRUNK){
		draw_text_transformed_noratio(30,15,
							"Owned",3,3,0)		
	} else {
		draw_text_transformed_noratio(30,15,
							"All",3,3,0)			
	}

	draw_set_color(c_white)
	if(global.high_contrast){
		draw_set_alpha(.75)
	} else {
		draw_set_alpha(.65)	
	}
	draw_rectangle_noratio(455+1,10+scroll_increment*(option_top),475-1,min(510,10+scroll_increment*(option_top+per_line)-1),false)
	draw_set_alpha(1)

	draw_set_color(global.text_fgcolor)
	draw_set_font(global.normalFont)
	for(var i = 0; i < per_line; i++){
		if(draw_id[i] > -1){
		
			if(i == selected_draw_index){
				if(global.high_contrast){
					draw_set_alpha(.6)	
				} else {
					draw_set_alpha(.4)
				}
			} else {
				if(global.high_contrast){
					draw_set_alpha(.25)	
				} else {
					draw_set_alpha(.1)
				}
			}
			draw_rectangle_noratio(10,option_y_offset+3-10 + option_spacer*i,
								455,option_y_offset-3-10 + option_spacer*(i+1),
								false)
		
			draw_set_alpha(1)
			draw_text_transformed_noratio(option_x_offset,-7+option_y_offset + option_spacer*i,
								string_copy(obj_data.card_name[draw_id[i]],1,18),3,3,0)
							
			var index = 3;
			if(obj_data.card_type[draw_id[i]] == CARD_TYPE_PHILOSOPHER){
				if(obj_data.card_element[draw_id[i]] == CARD_ELEMENT_MIND){
					index = 2
				} else if(obj_data.card_element[draw_id[i]] == CARD_ELEMENT_MATTER){
					index = 1
				} else {
					index = 0;	
				}
			} else if(obj_data.card_type[draw_id[i]] == CARD_TYPE_METAPHYS){
				index = 4
			}
			/*				
			draw_sprite_extended_noratio(spr_card_white_type,index,
									option_x_offset+15,55+option_y_offset + option_spacer*i,
									1,1,0,c_white,1)

		if(obj_data.card_type[draw_id[i]] == CARD_TYPE_PHILOSOPHER){
			draw_sprite_extended_noratio(spr_card_white_stat,0,
									option_x_offset+90,55+option_y_offset + option_spacer*i,
									.9,.9,0,c_white,1)
								
			draw_sprite_extended_noratio(spr_card_white_stat,1,
									option_x_offset+230,55+option_y_offset + option_spacer*i,
									.9,.9,0,c_white,1)
								
			draw_text_transformed_noratio(option_x_offset+120,45+option_y_offset + option_spacer*i,
								string(obj_data.card_attack[draw_id[i]]) + "/+"
								+ string(obj_data.card_attack_overlay[draw_id[i]])
								,2,2,0)
							
			draw_text_transformed_noratio(option_x_offset+260,45+option_y_offset + option_spacer*i,
								string(obj_data.card_defense[draw_id[i]]) + "/+"
								+ string(obj_data.card_defense_overlay[draw_id[i]])
								,2,2,0)
		}	
		*/
		}
	}

	// draw info text
	/*
	if(menu_selected == DECK_BUILD_TRUNK){
		draw_set_alpha(1)
			draw_text_transformed_noratio(info_x_offset,info_y_offset ,
								obj_data.card_name[trunk_display[menupos]],4,4,0)
				
			var index = 3;
			if(obj_data.card_type[trunk_display[menupos]] == CARD_TYPE_PHILOSOPHER){
				if(obj_data.card_element[trunk_display[menupos]] == CARD_ELEMENT_MIND){
					index = 2
				} else if(obj_data.card_element[trunk_display[menupos]] == CARD_ELEMENT_MATTER){
					index = 1
				} else {
					index = 0;	
				}
			} else if(obj_data.card_type[trunk_display[menupos]] == CARD_TYPE_METAPHYS){
				index = 4
			}
	/*
			draw_sprite_extended_noratio(spr_card_white_type,index,
									info_x_offset+15,70+info_y_offset ,
									1.2,1.2,0,c_white,1)
	if(obj_data.card_type[trunk_display[menupos]] == CARD_TYPE_PHILOSOPHER){
			draw_sprite_extended_noratio(spr_card_white_stat,0,
									info_x_offset+90,70+info_y_offset ,
									1,1,0,c_white,1)
								
			draw_sprite_extended_noratio(spr_card_white_stat,1,
									info_x_offset+230,70+info_y_offset ,
									1,1,0,c_white,1)
								
			draw_text_transformed_noratio(info_x_offset+120,60+info_y_offset ,
								string(obj_data.card_attack[trunk_display[menupos]]) + "/+"
								+ string(obj_data.card_attack_overlay[trunk_display[menupos]])
								,2,2,0)
							
			draw_text_transformed_noratio(info_x_offset+260,60+info_y_offset ,
								string(obj_data.card_defense[trunk_display[menupos]]) + "/+"
								+ string(obj_data.card_defense_overlay[trunk_display[menupos]])
								,2,2,0)
		}
	*/	
	/*
	} else {
		draw_set_alpha(1)
			draw_text_transformed_noratio(info_x_offset,info_y_offset ,
								obj_data.card_name[deck_display[menupos]],4,4,0)
				
			var index = 3;
			if(obj_data.card_type[deck_display[menupos]] == CARD_TYPE_PHILOSOPHER){
				if(obj_data.card_element[deck_display[menupos]] == CARD_ELEMENT_MIND){
					index = 2
				} else if(obj_data.card_element[deck_display[menupos]] == CARD_ELEMENT_MATTER){
					index = 1
				} else {
					index = 0;	
				}
			} else if(obj_data.card_type[deck_display[menupos]] == CARD_TYPE_METAPHYS){
				index = 4
			}
	
			
			draw_sprite_extended_noratio(spr_card_white_type,index,
									info_x_offset+15,70+info_y_offset ,
									1.2,1.2,0,c_white,1)
			
			if(obj_data.card_type[deck_display[menupos]] == CARD_TYPE_PHILOSOPHER){
				draw_sprite_extended_noratio(spr_card_white_stat,0,
										info_x_offset+90,70+info_y_offset ,
										1,1,0,c_white,1)
								
				draw_sprite_extended_noratio(spr_card_white_stat,1,
										info_x_offset+230,70+info_y_offset ,
										1,1,0,c_white,1)
								
				draw_text_transformed_noratio(info_x_offset+120,60+info_y_offset ,
									string(obj_data.card_attack[deck_display[menupos]]) + "/+"
									+ string(obj_data.card_attack_overlay[deck_display[menupos]])
									,2,2,0)
							
				draw_text_transformed_noratio(info_x_offset+260,60+info_y_offset ,
									string(obj_data.card_defense[deck_display[menupos]]) + "/+"
									+ string(obj_data.card_defense_overlay[deck_display[menupos]])
									,2,2,0)
			}
	}
	*/
	draw_set_halign(fa_right)
	draw_text_transformed_noratio(445,15,
						"Sort:"+sort_mode,2,2,0)
	draw_set_halign(fa_left)
	draw_text_transformed_noratio(40,75,
						"Collected: " + total_cards,2,2,0)
	draw_set_color(bla)
	draw_set_alpha(1)
	draw_rectangle_noratio(0,510,800,600,false)
	if(selected_card.card_id > -1){
		draw_set_color(global.text_fgcolor)
		gpu_set_colorwriteenable(true,true,true,false)
		draw_set_halign(fa_center)
		draw_text_transformed_noratio(400,520-7,textForceCharsPerLine(obj_data.card_bio[selected_card.card_id],45),2,2,0)
		draw_set_halign(fa_left)
		gpu_set_colorwriteenable(true,true,true,true)
	}
	
	surface_reset_target();
	menu_surf_update=false

}
if(draw_the_menu){
gpu_set_blendmode(bm_add)
}
draw_surface(fade_surface,global.display_x+313*global.scale_factor,global.display_y+0*global.scale_factor)
gpu_set_blendmode(bm_normal)
if(draw_the_menu){
	draw_surface(menu_surf,global.display_x,global.display_y)
}

with(fadeIn){
	draw_fade()	
}
with(fadeOut){
	draw_fade()	
}

draw_letterbox();