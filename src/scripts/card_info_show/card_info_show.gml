#macro CARD_INFO_CHAR_LINE 22

var use_sprite = -1

if(argument_count > 1){
	use_sprite = argument[1];
}

if(argument[0] > -1){
	var dia = ds_map_create();
	
	var obj = instance_create(0,0,cardInfo);
	obj.card_id = argument[0];
	var scaler = .75
	obj.card = card_create(610,330-40,scaler,argument[0]);
	if(use_sprite > -1){
		obj.card.card_sprite_ref = use_sprite
		with(obj.card){
			card_update();	
		}
	}
	
	obj.ax_title = obj_data.card_name[argument[0]] +" view"
	obj.bio = textForceCharsPerLine(obj_data.card_bio[argument[0]],40);
	var vale = keyword_generate(obj_data.card_keywords[argument[0]])+ "\n";
	var valelen = string_length(vale)
	obj.card_text = menuBookDiagramReplace(vale + textForceCharsPerLineKeepNewline(obj_data.card_effect_desc[argument[0]],CARD_INFO_CHAR_LINE),dia,CARD_INFO_CHAR_LINE);
	
	// add card tooltips
	var stringer = obj_data.card_keywords[argument[0]];
	var pos = string_pos(",",stringer);

	var returner = "";

	with(obj){
		menu_add_option(obj.ax_title,"",-1,-1)
		menuAddEmptyTooltip();
		
		if(global.voice){
			menu_add_option("Card Stats","",-1,-1)
			menuAddEmptyTooltip();
		}
		var counterz = 0;
		while(pos != 0){
			var pos = string_pos(",",stringer);
			var keyword=  get_keyword_id(string_copy(stringer,1,pos-1));
			if(keyword > -1){
				menu_add_option(obj_data.keyword_name[keyword],obj_data.keyword_desc[keyword],-1,-1)
				menuAddTooltip(150,107+20*tooltip_total,obj_data.keyword_desc[keyword],25);
				stringer= string_delete(stringer,1,pos);
				if(pos == 0){
					var keyword=  get_keyword_id(string_copy(stringer,1,pos-1));
					if(keyword > -1){
						menuAddTooltip(150,107+20*tooltip_total, obj_data.keyword_desc[keyword],25);
						menu_add_option(obj_data.keyword_name[keyword],obj_data.keyword_desc[keyword],-1,-1)
					}
					stringer= "";
					break;	
				}
			} else {
				stringer= string_delete(stringer,1,pos);	
			}
			
		counterz++;
		if(counterz > 1000000){
			break;	
		}
		}
		
	}

	
	
	//menuAddTooltip
	
	for(var i = 0; i < CARD_DISPLAY_EFFECT_LINES; i++){
		obj.card_effect_desc[i] = "";
	}

	for(var i = 0; i < CARD_DISPLAY_BIO_LINES; i++){
		obj.card_bio_desc[i] = "";
	}

	obj.card_effect_length = 0;

	for(var i = 0; i < CARD_DISPLAY_EFFECT_LINES; i++){
		var pos = string_pos("\n",obj.card_text);
		obj.card_effect_desc[i] = string_copy(obj.card_text,1,pos-1);
		obj.card_text= string_delete(obj.card_text,1,pos);
		obj.card_effect_length++;
		if(pos == 0){
			obj.card_effect_desc[i] = obj.card_text
			obj.card_text= "";
			break;	
		}
	}
	
	// do diagrams
	
	with(obj){
		var size, key, i;
		size = ds_map_size(dia);
		key = ds_map_find_first(dia);
		display_lines = CARD_DISPLAY_EFFECT_LINES
		for (i = 0; i < size; i++;)
		{
			var char_index = key;
			var lenny;
			var counter = 0;
			var column_count = 0;
			var pagenum = 0;
			var strinlen =0;
		
			var row_count = 0;
			for(var j = 0; j < CARD_DISPLAY_EFFECT_LINES; j++){
				strinlen = string_length(card_effect_desc[j])
				lenny = counter+ strinlen;
				if(char_index > lenny){
					counter = lenny+1;
				} else {
					row_count = j;
					column_count = char_index - counter
					break;	
				}
			}
		
			var xx = 20-9+(string_width(string_copy(card_effect_desc[j],1,column_count))*2);
			var yy = +120+25*row_count;
		
			menuBookCreateDiagram(xx,yy,dia[? key],0)
			key = ds_map_find_next(dia, key);
		}
	
	}
	// end diagram
	
	for(var i = 0; i < CARD_DISPLAY_BIO_LINES; i++){
		var pos = string_pos("\n",obj.bio);
		obj.card_bio_desc[i] = string_copy(obj.bio,1,pos-1);
		obj.bio= string_delete(obj.bio,1,pos);
		if(pos == 0){
			obj.card_bio_desc[i] = obj.bio
			obj.bio= "";
			break;	
		}
	}
	
	obj.archetype = "";
	
	if(obj_data.card_type[argument[0]] == CARD_TYPE_PHILOSOPHER){
		if(string_lower(obj_data.card_archetype1[argument[0]]) != "none"){
		// only one archetype
			if(string_lower(obj_data.card_archetype2[argument[0]]) == "none"){
				obj.archetype = "- " + obj_data.card_archetype1[argument[0]]
			} else {
				
				obj.archetype = "- " + obj_data.card_archetype1[argument[0]]
				obj.archetype2= "- " + obj_data.card_archetype2[argument[0]] 
			}
		}
	} else if(obj_data.card_type[argument[0]] == CARD_TYPE_METAPHYS){
		obj.archetype = "- Theory"
	} else {
		obj.archetype = "- Praxis"
	}
	with(obj){
		ax_card_detailed(card_id);
	}
	
	with(obj){
		mousepos_x = 150;
		mousepos_y = 100
		
		menu_push(id)	
	}
}

ds_map_destroy(dia);

// whether or not the card should shine when viewing it
var shine = false

// if the pack list says its new
with(PackList){
	
	for(var i = 0; i < 5; i++){
		if(card_id[i] == obj.card_id){
			if(is_new[i]){
				shine = true
			}
			break;
		}
	}
}

if(shine){
	with(obj.card){
		card_flash()	
	}
}

return noone;