var savefile = ds_map_secure_load("cards.bin")

if(ds_exists(savefile,ds_type_map)){
	var data = savefile[? "cards"]
	if(!is_undefined(data)){
		for(var i = 0; i < obj_data.cards_total; i++){
			var test = data[? string(i)];
			if(!is_undefined(test)){
				obj_data.card_in_trunk[i] = test;
			}
		}
	}
	
	
	var data = savefile[? "events"]
	if(!is_undefined(data)){
		var sizer = ds_list_size(data);
		ds_list_clear(obj_data.events);
	
		for(var i = 0; i< sizer; i++){
			ds_list_add(obj_data.events,data[| i]);	
		}
	}
	
	
	data = savefile[? "duel_history"]
	if(!is_undefined(data)){
		for(var i = 0; i < obj_data.opponents_total; i++){
			var test = data[? string(i)];
			if(!is_undefined(test)){
				obj_data.opponent_encounter[i] = test;
			}
		}
	}

	data = savefile[? "deck"]
	if(!is_undefined(data)){
		var sizer = ds_list_size(data);
	
		for(var i = 0; i< sizer; i++){
			ds_list_add(obj_data.current_deck,data[| i]);	
		}
	}
	
	data = savefile[? "wins"]
	if(!is_undefined(data)){
		obj_data.wins = data;	
	}
	
	data = savefile[? "losses"]
	if(!is_undefined(data)){
		obj_data.losses = data;	
	}
	
	data = savefile[? "draws"]
	if(!is_undefined(data)){
		obj_data.draws = data;	
	}
	
	data = savefile[? "main"]
	if(!is_undefined(data)){
		obj_data.current_main_card = data;	
	}

	// Hey asshole. you tried to save scum
	// fuck you
	data = savefile[? "savescum"]
	if(!is_undefined(data)){
		if(data){
			obj_data.losses++;	
		}
	}
	
	data = savefile[? "knowledge"]
	if(!is_undefined(data)){
		 global.max_knowledge = data;	
	}
	
	data = savefile[? "story"]
	if(!is_undefined(data)){
		obj_data.story_counter = data;	
	}

	ds_map_destroy(savefile);
}

// load pack data

var savefile = ds_map_secure_load("packs.bin")

if(ds_exists(savefile,ds_type_map)){
	var counter = 0;
	
	while(ds_map_exists(savefile,string(counter))){
		var nom = string(counter);
		data = savefile[? nom]
		if(!is_undefined(data)){
		
			var sizer = ds_list_size(data);
	
			for(var i = 0; i< sizer; i++){
				obj_data.pack_amount[counter,i] = data[|i];
			}
		}
		counter++;
	}
	
	ds_map_destroy(savefile);
}