duel_animation_wait(60)


var modi = 0;
var scale = 2.75;
			
if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
	modi = -135
	scale = TOOLTIP_CAMERA_ADJUST_SCALE
}
		

duel_animation_do_draw_until_5(duelController.field[duelController.active_player])
duel_animation_do_draw_until_5(duelController.field[!duelController.active_player])
duel_animation_move_camera(350,290+modi,5,10)
duel_animation_play_sound(sfx_sound_duel_start)
duel_animation_display_message("Game\nStart!")
duel_animation_play_song(duelController.duel_music)
duel_animation_wait(15)
duel_animation_goto_part(duelController.field[duelController.active_player],DUEL_FIELD_HAND)
duel_animation_play_sound(sfx_sound_duel_end_turn)
var name = duelController.duel_hp[duelController.active_player].name
duel_animation_display_message(name+"'s\nTurn")
duel_animation_wait(15)
duel_animation_start_duel();
//duelController.phase_current = DUEL_PHASE_DRAW;
duel_animation_next_phase();