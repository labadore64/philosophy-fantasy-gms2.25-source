var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}


if(global.high_contrast){
	draw_clear(c_black)	
} else {
	with(titleScreen){
		titleScreen_DrawBackground()
	}

	with(mainMenu){
		mainMenu_DrawBackground();
	}
}

draw_set_halign(fa_center)

draw_set_alpha(alpha)
draw_set_color(col)
gpu_set_blendmode(bm_add)
draw_set_font(global.largeFont)
draw_text_transformed_ratio(400,75-30,"Credits",4,4,0)

draw_text_transformed_ratio(400,170-30,menu_name[0],2,2,0)

draw_text_transformed_ratio(400,280-30,menu_name[2],2,2,0)

draw_set_font(global.normalFont)
draw_text_transformed_ratio(400,220-30,menu_name[1],2,2,0)

draw_text_transformed_ratio(400,330-30,menu_name[3]+
							menu_name[4]+
							menu_name[5]
							,2,2,0)
gpu_set_blendmode(bm_normal)
draw_set_halign(fa_left)

menuParent_draw_cancel_button()

draw_letterbox();