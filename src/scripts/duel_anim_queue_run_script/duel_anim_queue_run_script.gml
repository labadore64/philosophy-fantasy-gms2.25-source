var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _time = map[? "script"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_time)){
			
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		/*
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
		*/
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		//}
		
		// exeucte the script
		var returner = noone
		with(obj){
			returner = script_execute(_time);
		}
		
		if(returner > 0){
			return returner;	
		}
		
		return noone;
	}

}

return noone;