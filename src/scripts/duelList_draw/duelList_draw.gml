deckBuild_drawBackground();

// draw RARE CARD

if(!surface_exists(menu_surf)){
	menu_surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	menu_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(menu_surf,global.letterbox_w,global.letterbox_h);
		menu_surf_update = true;
	}
}

if(menu_surf_update){
	surface_set_target(menu_surf)
	
	draw_clear_alpha(c_black,0)
	
	gpu_set_colorwriteenable(true,true,true,true)

	var bla = global.menu_bgcolor;

	if(global.high_contrast){
		bla = c_black;	
	}

	draw_set_color(bla)
	draw_set_alpha(.5)
	draw_rectangle_noratio(10,0,455,485,false)
	draw_rectangle_noratio(475,0,790,485,false)
	draw_set_alpha(1)
	// infobox
	if(global.color_gradient){
		draw_rectangle_color_noratio(0,485,800,600,
									global.text_bgcolor[0],global.text_bgcolor[0],
									global.text_bgcolor[1],global.text_bgcolor[1],
									false)
	} else {
		draw_set_color(global.text_bgcolor[0])
		draw_rectangle_color_noratio(0,485,800,600,
									false)
	}


	// text


	draw_set_font(global.largeFont)
	draw_set_color(global.text_fgcolor)

	draw_text_transformed_noratio(30,30,
						title,3,3,0)		

	draw_set_color(global.text_fgcolor)
	draw_set_font(global.normalFont)
	for(var i = 0; i < per_line; i++){
		if(draw_id[i] > -1){
		
			if(i == selected_draw_index){
				draw_set_alpha(.4)
			} else {
				draw_set_alpha(.1)
			}
			draw_rectangle_noratio(10,option_y_offset+3-10 + option_spacer*i,
								455,option_y_offset-3-10 + option_spacer*(i+1),
								false)
		
			draw_set_alpha(1)
			
			var display_text = obj_data.card_name[draw_id[i]]
			while(string_width(display_text)*3 > 440){
				display_text = string_copy(display_text,1,string_length(display_text)-1)	
			}
			
			
			draw_text_transformed_noratio(option_x_offset,option_y_offset + option_spacer*i,
								display_text,3,3,0)
							
			var index = 3;
			if(obj_data.card_type[draw_id[i]] == CARD_TYPE_PHILOSOPHER){
				if(obj_data.card_element[draw_id[i]] == CARD_ELEMENT_MIND){
					index = 2
				} else if(obj_data.card_element[draw_id[i]] == CARD_ELEMENT_MATTER){
					index = 1
				} else {
					index = 0;	
				}
			} else if(obj_data.card_type[draw_id[i]] == CARD_TYPE_METAPHYS){
				index = 4
			}
							
			draw_sprite_extended_noratio(spr_card_white_type,index,
									option_x_offset+15,55+option_y_offset + option_spacer*i,
									1,1,0,global.text_fgcolor,1)
		if(obj_data.card_type[draw_id[i]] == CARD_TYPE_PHILOSOPHER){
			draw_sprite_extended_noratio(spr_card_white_stat,0,
									option_x_offset+90,55+option_y_offset + option_spacer*i,
									.9,.9,0,global.text_fgcolor,1)
								
			draw_sprite_extended_noratio(spr_card_white_stat,1,
									option_x_offset+230,55+option_y_offset + option_spacer*i,
									.9,.9,0,global.text_fgcolor,1)
								
			draw_text_transformed_noratio(option_x_offset+120,45+option_y_offset + option_spacer*i,
								string(obj_data.card_attack[draw_id[i]]) + "/+"
								+ string(obj_data.card_attack_overlay[draw_id[i]])
								,2,2,0)
							
			draw_text_transformed_noratio(option_x_offset+260,45+option_y_offset + option_spacer*i,
								string(obj_data.card_defense[draw_id[i]]) + "/+"
								+ string(obj_data.card_defense_overlay[draw_id[i]])
								,2,2,0)
		}	

		}
	}

	// draw info text

		draw_set_alpha(1)
			draw_text_transformed_noratio(info_x_offset,info_y_offset ,
								obj_data.card_name[trunk_display[menupos]],4,4,0)
				
			var index = 3;
			if(obj_data.card_type[trunk_display[menupos]] == CARD_TYPE_PHILOSOPHER){
				if(obj_data.card_element[trunk_display[menupos]] == CARD_ELEMENT_MIND){
					index = 2
				} else if(obj_data.card_element[trunk_display[menupos]] == CARD_ELEMENT_MATTER){
					index = 1
				} else {
					index = 0;	
				}
			} else if(obj_data.card_type[trunk_display[menupos]] == CARD_TYPE_METAPHYS){
				index = 4
			}
	
			draw_sprite_extended_noratio(spr_card_white_type,index,
									info_x_offset+15,70+info_y_offset ,
									1.2,1.2,0,global.text_fgcolor,1)
	
	if(obj_data.card_type[trunk_display[menupos]] == CARD_TYPE_PHILOSOPHER){
			draw_sprite_extended_noratio(spr_card_white_stat,0,
									info_x_offset+90,70+info_y_offset ,
									1,1,0,global.text_fgcolor,1)
								
			draw_sprite_extended_noratio(spr_card_white_stat,1,
									info_x_offset+230,70+info_y_offset ,
									1,1,0,global.text_fgcolor,1)
								
			draw_text_transformed_noratio(info_x_offset+120,60+info_y_offset ,
								string(obj_data.card_attack[trunk_display[menupos]]) + "/+"
								+ string(obj_data.card_attack_overlay[trunk_display[menupos]])
								,2,2,0)
							
			draw_text_transformed_noratio(info_x_offset+260,60+info_y_offset ,
								string(obj_data.card_defense[trunk_display[menupos]]) + "/+"
								+ string(obj_data.card_defense_overlay[trunk_display[menupos]])
								,2,2,0)
		}
	
	surface_reset_target();
}

draw_surface(menu_surf,global.display_x,global.display_y)

card_draw(selected_card);

with(fadeIn){
	draw_fade()	
}
with(fadeOut){
	draw_fade()	
}

draw_letterbox();