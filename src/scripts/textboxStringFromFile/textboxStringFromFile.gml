/// @description - gets a string from a text file
/// @param filename
/// @param string_section
/// @param string_id
/// @param text_list


//add beforetext

var beforetext_size = ds_list_size(add_beforetext);
for(var i = 0; i < beforetext_size; i++){
	ds_list_add(text,add_beforetext[|i]);	
}

i = 0;

ini_open(argument[0]);
ds_list_clear(text);

event_id = ini_read_string(text_section, "event_id","")

while(true){
	if(!ini_key_exists(text_section, text_name + string(i))){
	    break;
	}
	i++
}


var strrr = "";

for(var j = 0; i > j; j++){
    strrr = newline(ini_read_string(text_section, text_name + string(j),""));
	ds_list_add(text,textReplaceVars(strrr));
	ds_map_add(draw_map,j,ini_read_string(text_section, "graphics" + string(j),""));
}

ini_close();

//add aftertext

var aftertext_size = ds_list_size(add_aftertext);
for(var i = 0; i < aftertext_size; i++){
	ds_list_add(text,add_aftertext[|i]);	
}

// check if it should kill itself
if(event_id != ""){
	if(eventCheck(event_id)){
		destroyed = true;
		instance_destroy();
	}
}