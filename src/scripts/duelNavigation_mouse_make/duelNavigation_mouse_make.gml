// main duel surface
if(instance_exists(duelNavigation)){
	var mr_x = duelNavigation.x+duelNavigation.nav_x
	var mr_y = duelNavigation.y+duelNavigation.nav_y
	
	mouseHandler_add(mr_x-20,-35+mr_y,mr_x+20,mr_y+10,duelCamera_pan_start_up,"Pan Up")
	mouseHandler_add(mr_x-20,-35+mr_y+105,mr_x+20,mr_y+5+105,duelCamera_pan_start_down,"Pan Down")
	
	mouseHandler_add(mr_x-65,10+25-20+mr_y,mr_x-25,10+35+20+mr_y,duelCamera_pan_start_left,"Pan Left")
	mouseHandler_add(mr_x-65+100,10+25-20+mr_y,mr_x-25+100,10+35+20+mr_y,duelCamera_pan_start_right,"Pan Right")
	
	mouseHandler_add(mr_x-20,mr_y+20,mr_x+25,-35+mr_y+105,duelCamera_center_camera,"Center")
	
	duelNavigation.zoom_index = MouseHandler.mouse_count;
	mouseHandler_add(mr_x-20-35,10+75+30+mr_y,mr_x+25+40,mr_y+95+50,duelNavigation_start_zoom,"Zoom")

	// cancel
	if(!global.mouse_active){
		cancel_button_enabled = false;	
	}

	with(duelNavigation){
		if(cancel_button_enabled){
			// if either aren't set, default to top right corner
			if(cancel_button_x == MENU_PARENT_CANCEL_DEFAULT_POS
				|| cancel_button_y == MENU_PARENT_CANCEL_DEFAULT_POS){
	
				cancel_button_x = x + menu_width;
				cancel_button_y = y;
			}
			
			// create button
			mouseHandler_add(cancel_button_x - cancel_button_width*.5,
							cancel_button_y - cancel_button_height*.5,
							cancel_button_x + cancel_button_width*.5,
							cancel_button_y + cancel_button_height*.5,
							duelNavigation_destroy, "Cancel")
		}
	}

	with(duelController){
		if(instance_exists(navigation_menu)){

			nav_pos[0] = duelCamera.cam_x+(duelCamera.pan_x-navigation_menu.x)*duelCamera.scale*.5
			nav_pos[1] = duelCamera. cam_y+(duelCamera.pan_y-navigation_menu.y)*duelCamera.scale*.5
			nav_pos[2] = duelCamera.cam_x+(duelCamera.pan_x-navigation_menu.x+ navigation_menu.menu_width)*duelCamera.scale*.5
			nav_pos[3] = duelCamera. cam_y+(duelCamera.pan_y-navigation_menu.y + navigation_menu.menu_height)*duelCamera.scale*.5
		}
	}
}