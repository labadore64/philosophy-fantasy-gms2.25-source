if(duelController.phase_current != DUEL_PHASE_STANDBY){
	if(duelController.turns != 0){
		if(duelController.phase_current == DUEL_PHASE_MAIN){
			duelController_next_phase();
		}
	}
	if(duelController.turns == 0 ||
		duelController.phase_current == DUEL_PHASE_RETURN){
		duelController_do_nextturn();
	}
}