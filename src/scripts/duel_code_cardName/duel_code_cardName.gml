if(!argument[0]){
	
} else {
	var target_index = -1; // the card index to move
	var target_origin = -1; // the location of the target
	var target_destination = -1; // where to move the target
	var target_side = duelController.active_player; // which side the target is on
	var test_value = "";

	var this = code_get_this();
	var this_enemy = false;
	if(string_pos("pos_enemy",this) > 0){
		this_enemy = true;
	}

	var return_value = 0;

	var return_variable = "return"

	var stringer = "";
	for(var i = 0; i < code_argument_size; i++){
		if(string_pos("name",code_arguments[i]) > 0){
			test_value = string_replace_all(string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i])),
									"\"","");
		}
	
		if(string_pos("return",code_arguments[i]) > 0){
			return_variable = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		
			// the variable must exist. this forces uses of the predefined registers
			// to prevent any funny business
			if(!ds_map_exists(duelController.code_values,return_variable)){
				return_variable = "return";	
			}
		}
	
	
		// parse targets
		if(string_pos("target:",code_arguments[i]) > 0){
			var targe = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		
			var targe_variable = duelController.code_values[? targe];
			if(!is_undefined(targe_variable)){
				targe = targe_variable	
			}
		
			// if target is this just use the character stored in this
			var is_this = false;
			if(targe == "this"){
				targe = this
				is_this = true;
			}

			var stringe = targe;
			var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			var side = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			
			//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		
			if(is_this){
				if(this_enemy){
					target_side = DUEL_PLAYER_ENEMY	
				} else {
					target_side = DUEL_PLAYER_YOU	
				}
			} else {
				if(side == "enemy"){
					if(!this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}
				} else {
					if(this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}	
				}
			}
			// if the code is a location, parse as location.
			if(code_is_location(ori)){
				target_index = string_copy(stringe,string_pos(":",stringe)+1,
											string_length(stringe) - string_pos(":",stringe))
			
				// string == variable
				if(string_length(string_digits(target_index)) != string_length(target_index)){
					var value = duelController.code_values[? target_index];
					if(is_undefined(value) || !is_real(value)){
						value = 0;	
					}
				
					target_index = value;
				} else {
					target_index = real(target_index);	
				}
				
				if(ori == "overlay"){
					target_index++;	
				}
				
				target_origin = code_get_location(ori,target_side)
			} else {
				// otherwise, track the search terms of the card.
				// note: you're going to need to update how you track this location
				// after doing this... damb.
				
				// anyways target_index is going to contain a list of targets
				// listed in brackets :) like so:
				// cardMove("target:[type:mind+name:Deleuze+archetype:psychoanalysis]","dest:hand");
			}
		}
	
	}

	// selective move
	if(target_index > -1 &&
		target_origin > -1 &&
		target_index < target_origin.card_array_size &&
		test_value != ""){
		var le_card = target_origin.card_array[target_index];
	
		if(string_pos(string_lower(test_value),string_lower(obj_data.card_name[le_card])) > 0){
			return_value = 1;
		}
	}

	// set the value here

	ds_map_replace(duelController.code_values,return_variable,return_value)
}