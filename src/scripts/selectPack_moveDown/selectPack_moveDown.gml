if(!instance_exists(fadeIn)){
	if(animate <= -1){
		
		menupos++;

		if(menupos >= menu_count){
			if(!wrap){
				menupos = menu_count-1;	
			} else {
				menupos = 0;	
			}
		}

		if(global.menu_sounds){soundfxPlay(sfx_sound_pack_change);}


		RUMBLE_MOVE_MENU


		with(fadeIn){
			instance_destroy()	
		}
		with(fadeOut){
			instance_destroy()	
		}

		menuParent_default_accessString()
		menuParent_ttsread();

		with(MouseHandler){
			ignore_menu = true	
		}

		surface_update = true

		animate = animate_time;
		animate_left = false;
	}
}