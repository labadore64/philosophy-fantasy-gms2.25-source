

with(duelController){
	if(selected_object > 0){
		last_selected_object = selected_object
		last_selected_thingie = selected_object.selected_object	
	}
}

with(argument[0]){
	changed = false;
	if(selected_object != part[argument[1]]){
		changed = true;	
	}
	
	selected_object = part[argument[1]];
	
	// corrects the selected object's menu position
	if(argument_count > 2){
		selected_object.menupos = argument[2]	
		changed = true;	
	}
	
	var size = selected_object.card_array_size;
	
	for(var i = 0; i < selected_object.card_array_size; i++){
		if(selected_object.card_array[i] == -1){
			size = i;
			break;
		}
	}
	
	if(size < 1){
		size = 1	
	}
	
	if(size <= selected_object.menupos){
		selected_object.menupos = size-1;
	}
	if(selected_object.menupos < -1){
		selected_object.menupos = 0;	
	}
	
	with(selected_object){
		if(menu_activate != -1){
			script_execute(menu_activate);
		}	
	}
}