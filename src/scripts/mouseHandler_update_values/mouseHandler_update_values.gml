	for(var i = 0; i < mouse_count; i++){
		for(var j = 0; j < 4; j++){
			if(j % 2 == 0){
				mouse_points_trans[i,j] = global.display_x + mouse_points[i,j]*global.scale_factor
			} else {
				mouse_points_trans[i,j] = global.display_y + mouse_points[i,j]*global.scale_factor	
			}
		}
	}
	
	var val1 = 0;
	var val2 = 0;
	var val3 = 0;
	var val4 = 0;
	var counter = 0;
	var spacer = 1;
	for(var i = 0; i < mouse_menu_count; i++){
		val1 = -1000;
		val2 = -1000;
		val3 = -1000;
		val4 = -1000;
		
		with(mouse_menu_obj){
			spacer = option_spacer*global.scale_factor;
			val1 = mousepos_x+x+option_x_offset-draw_width*select_percent*.5;	
			val2 = mousepos_y+y+option_y_offset-draw_height*.5+(option_spacer)*(counter)-10;
			val3 = mousepos_x+x+option_x_offset+draw_width*select_percent*.5;
			val4 = mousepos_y+y+option_y_offset-draw_height*.5+(option_spacer)*(counter+1)-10-1;
		}
		
		counter++;
		
		mouse_menu_points_trans[i,0] = global.display_x + val1*global.scale_factor
		mouse_menu_points_trans[i,1] = global.display_y + val2*global.scale_factor
		mouse_menu_points_trans[i,2] = global.display_x + val3*global.scale_factor
		mouse_menu_points_trans[i,3] = global.display_y + val4*global.scale_factor
		mouse_menu_spacer = spacer;
	}