var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

var adjust = 20;

if(bg_title == ""){
	adjust = 0;	
}

draw_set_alpha(1)
if(global.high_contrast){
	draw_set_color(c_black)
	draw_rectangle_ratio(0,40,275+75,110+adjust,
								false)
} else {
	if(global.color_gradient){
		draw_rectangle_color_ratio(0,40,275+75,110+adjust,
									global.text_bgcolor[0],global.text_bgcolor[0],
									global.text_bgcolor[1],global.text_bgcolor[1],
									false)
	} else {
		draw_set_color(global.text_bgcolor[0])
		draw_rectangle_ratio(0,40,275,110,
									false)
	}
}

draw_sprite_extended_ratio(spr_energy_bar,0,0,0,1.25,1,0,global.text_bgcolor[0],1)

draw_set_color(col)
draw_set_halign(fa_left)

draw_text_transformed_ratio(15,7,"Energy:",2,2,0)

if(bg_title == ""){
	draw_text_transformed_ratio(15,50,global.month_names[global.game_month] + " " + string(global.game_year),2,2,0)
	draw_text_transformed_ratio(15,75,"Week " + string(global.game_week+1),2,2,0)

} else {
	draw_text_transformed_ratio(15,50,bg_title,2,2,0)
	draw_text_transformed_ratio(15,55+20,global.month_names[global.game_month] + " " + string(global.game_year),2,2,0)
	draw_text_transformed_ratio(15,80+20,"Week " + string(global.game_week+1),2,2,0)

}

	
for(var i = 0; i < global.activity_points; i++){
	draw_sprite_extended_ratio(spr_energy_count,0,325-32*i,19,1,1,0,energy_col[i],1)
}