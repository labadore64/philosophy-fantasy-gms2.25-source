var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _status = map[? "status"];
	var _arg = map[? "arg"];
	var _target = map[? "target"];
	var _quantity = map[? "quantity"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_status) &&
		!is_undefined(_target) &&
		!is_undefined(_quantity) &&
		!is_undefined(_arg)){

		var targe = duelController.field[0];
		if(string_pos("enemy",_target)){
			targe = duelController.field[1];
		}
		
		duel_status_set(targe,_status + ":" + _arg,_quantity)
		
		return noone;
	}

}

return noone;