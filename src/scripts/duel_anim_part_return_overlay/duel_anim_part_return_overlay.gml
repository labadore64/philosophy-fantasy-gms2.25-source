if(global.dont_record){
	return noone;	
}

var argu = argument[0]; // args as json string

var obj = instance_create(200,150,duelSub)
obj.parent = id;
var me = id;
with(obj){
	menu_add_option("Yes","",returnPhase_overlay,-1)	
	menu_add_option("No","",returnPhase_continue,-1)	
	menu_cancel = returnPhase_continue;
	option_y_offset = 100;
	option_x_offset = 200;
	title = "Overlay a card?"
	menu_width+=40
	draw_title = true;
	title_y = 20;
	title_x = 225
	open_sound  = sfx_sound_question
}
	
menu_push(obj)

//duel_animation_resume_control();
return obj