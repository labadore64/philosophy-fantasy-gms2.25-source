var obj = instance_create(400,275,afterBattleSelection);
// set text
with(obj){
	menu_add_option("Yes","",duelEnd_recordReplay_yes,-1)	
	menu_add_option("No","",duelEnd_recordReplay_no,-1)	
	menu_cancel = duelEnd_recordReplay_no;
	menu_help = "";
	option_y_offset = 100;
	option_x_offset = 200;
	title = "Record replay?"
	menu_width+=40
	x-=menu_width*.5
	y-=menu_height*.5
	
	draw_title = true;
	title_y = 20;
	title_x = 225
	soundfxPlay(sfx_sound_question)
	select_percent=1
	draw_width = menu_width
	mousepos_x=15
	mouseHandler_clear()
	mouseHandler_menu_default()

	tts_say(title)
}

return obj;