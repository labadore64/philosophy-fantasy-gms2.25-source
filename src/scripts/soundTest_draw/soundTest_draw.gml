var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

with(OptionsMenu){
	options_draw()	
}

draw_set_alpha(.75)
draw_set_color(c_black)
draw_rectangle_ratio(0,0,800,600,false)

draw_set_alpha(1)

draw_set_color(c_white)
draw_rectangle_ratio(centerx-width-5,centery-height-5,centerx+width+5,centery+height+5,false)

if(global.high_contrast){
	draw_rectangle_ratio(centerx-width-5,centery-height-5,centerx+width+5,centery+height+5,false)
} else {

	if(global.color_gradient){
		draw_rectangle_color_ratio(centerx-width-7,centery-height-7,centerx+width+7,centery+height+7,
		global.menu_grcolor[0],global.menu_grcolor[0],
		global.menu_grcolor[1],global.menu_grcolor[1],
		false)
	
		
	} else {
		draw_set_color(global.menu_grcolor[0])
		draw_rectangle_ratio(centerx-width-7,centery-height-7,centerx+width+7,centery+height+7,
		false)
	
	}
}

if(global.high_contrast){
	draw_set_color(c_black)
} else {
	draw_set_color(global.menu_bgcolor)
}
draw_rectangle_ratio(centerx-width,centery-height,centerx+width,centery+height,false)

gpu_set_colorwriteenable(true,true,true,false)

draw_set_color(c_white)
if(global.menu_animate){
	draw_set_alpha(alpha_adder);
} else {
	draw_set_alpha(alpha_max)
}
if(global.high_contrast){
	draw_rectangle_ratio(centerx-width+10,centery-height-10+80+40*menupos,
					centerx-width+10+100,centery-height+30+80+40*menupos,
				false)
} else {
	draw_rectangle_color_ratio(centerx-width+10,centery-height-10+80+40*menupos,
					centerx-width+10+100,centery-height+30+80+40*menupos,
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
	false)
}

draw_set_halign(fa_left)
draw_set_alpha(1)

draw_set_color(col)
draw_text_transformed_ratio(centerx-width+20,centery-height+80-5,"SFX",2,2,0)
draw_text_transformed_ratio(centerx-width+20,centery-height+120-5,"Music",2,2,0)
draw_set_halign(fa_center)
draw_text_transformed_ratio(centerx,centery-height+20-7,"Sound Test",3,3,0)

draw_sprite_extended_ratio(spr_point_arrow,0,
							centerx+75-30,centery-height+80+8+40,
							.33,.33,90,col,1)
draw_sprite_extended_ratio(spr_point_arrow,0,
							centerx+75-30+55,centery-height+80+8+40,
							.33,.33,270,col,1)
							
draw_sprite_extended_ratio(spr_point_arrow,0,
							centerx+75-30,centery-height+80+8,
							.33,.33,90,col,1)
draw_sprite_extended_ratio(spr_point_arrow,0,
							centerx+75-30+55,centery-height+80+8,
							.33,.33,270,col,1)

draw_text_transformed_ratio(centerx+75,centery-height+80-5,string(sound_menupos),2,2,0)
draw_text_transformed_ratio(centerx+75,centery-height+120-5,string(song_menupos),2,2,0)
/*
draw_set_alpha(.5)
draw_rectangle_ratio(centerx-width+10,centery-height-10+80+40*menupos,
					centerx-width+10+100,centery-height+30+80+40*menupos,false)
draw_set_alpha(1)
*/
gpu_set_colorwriteenable(true,true,true,true)