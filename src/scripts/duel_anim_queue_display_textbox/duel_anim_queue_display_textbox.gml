var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _msg = map[? "msg"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_msg)){
			
		var obj = battleTutorialShow(_msg)
		
		return obj;
	}

}

return noone;