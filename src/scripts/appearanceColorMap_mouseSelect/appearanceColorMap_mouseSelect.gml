menupos = argument[0]-1
color_selected = false

if(stringer != ""){
	selected = true
	appearanceColorMap_select(false)	
}

if(!selected){
	
	stringer="";
	var pos = menupos-1;
	var val = string_digits(stringer);
	
	soundfxPlay(sfx_sound_menu_keyboard)	
	
	if(val != ""){
		val = clamp(real(val),0,255)
		
		if(pos < 3){
			color_rgb[pos] = val
			appearanceColorMap_update_rgb()
		} else {
			pos-=3;
			color_hsv[pos] = val
			appearanceColorMap_update_hsv()
		}

	}
	selected=true
}

surf_main_update = true