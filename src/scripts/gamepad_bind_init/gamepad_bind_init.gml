/// @function gamepad_bind_init()
/// @description Initializes the game's default gamepad binding state.

//gamepad binds
ds_map_clear(global.gamepad_map)
global.gamepad_size = 0
gamepad_bind_set("left",gp_padl);
gamepad_bind_set("right",gp_padr);
gamepad_bind_set("up",gp_padu);
gamepad_bind_set("down",gp_padd);

gamepad_bind_set("lefttab",gp_shoulderl);
gamepad_bind_set("righttab",gp_shoulderr);

gamepad_bind_set("cancel",gp_face2);
gamepad_bind_set("select",gp_face1);
gamepad_bind_set("select2",gp_start);

//overworld gamepadbinds


gamepad_bind_set("sort",gp_face4);
gamepad_bind_set("speed",gp_face3);

gamepad_bind_set("cam_up",-1);
gamepad_bind_set("cam_down",-1);
gamepad_bind_set("cam_left",-1);

gamepad_bind_set("keydisplay", -1);
gamepad_bind_set("cam_right",-1);

gamepad_bind_set("tts_stop",-1)
gamepad_bind_set("tts_repeat",-1);

// additional keys for accessibility
gamepad_bind_set("access1",-1);
gamepad_bind_set("access2",-1);
gamepad_bind_set("access3",-1);
gamepad_bind_set("access4",-1);
gamepad_bind_set("access5",-1);
gamepad_bind_set("access6",-1);
gamepad_bind_set("access7",-1);
gamepad_bind_set("access8",-1);
gamepad_bind_set("access9",-1);
gamepad_bind_set("access0",-1);

// new keys
gamepad_bind_set("exit",gp_select);
gamepad_bind_set("help",gp_shoulderlb);
gamepad_bind_set("options",gp_shoulderrb);
gamepad_bind_set("screenshot",gp_stickl)

gamepad_bind_set("view_card", gp_stickr);

// duel navigation keys
gamepad_bind_set("view_hand", -1);
gamepad_bind_set("view_deck", -1);
gamepad_bind_set("view_active", -1);
gamepad_bind_set("view_overlay", -1);
gamepad_bind_set("view_bench", -1);
gamepad_bind_set("view_theory",-1);
gamepad_bind_set("view_limbo", -1);

gamepad_bind_set("view_switch", -1);

for(var i = 0; i < global.gamepad_size; i++){
	global.gamepad_state_array[i] = KEY_STATE_NONE;
}



//axis stuff
axis = 0;

axisv = 0;
axish = 0;
axis_angle = 0;
axis_amount = 0;

axis_angle_last = 0;
axis_amount_last = 0;

axis_changed = false;

right_axis = global.gamepadRightAxis;

axis_start = false;

axis_min = .5

axis_direction = AXIS_LEFT;

axis_dead_angle = 0.2;

axis_left_pressed = false;
axis_right_pressed = false;
axis_up_pressed = false;
axis_down_pressed = false;