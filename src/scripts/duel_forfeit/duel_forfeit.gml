/*
menu_pop()
instance_destroy();

with(duelSub){
	instance_destroy();
	menu_pop();
}
*/

if(instance_exists(duelSub)){
	//keep popping until the menu count is 1
	with(menuControl){
		while(ds_stack_size(menu_stack) > 1){
			var val = ds_stack_pop(menu_stack);
			with(val){
				instance_destroy();	
			}
		}
		
		active_menu = ds_stack_top(menu_stack);
	}
}

	duelinput_insert("forfeit",duelController.field[duelController.active_player].hand,0);