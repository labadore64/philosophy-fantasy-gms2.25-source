// argument0 = selected card
// argument1 = target card

var card1 = true;
var card2 = true;
var card3 = true;

if(argument[0] < 0){
	return false;	
}

if(argument[0] > -1){
	if(card_has_keyword(argument[0],"timid")){
		card1 = false;	
	}
}

if(argument[0] > -1){
	if(card_has_keyword(argument[0],"sacrifice") &&
		parent.part[DUEL_FIELD_ACTIVE].ai_card_count[argument[2]] < 1){
		card3 = false;	
	}
}


if(argument[1] > -1){
	if(card_has_keyword(argument[1],"block")){
		card2 = false;	
	}
}


	
return card1 && card2 && card3;

return false;