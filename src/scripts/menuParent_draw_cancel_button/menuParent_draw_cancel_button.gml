if(cancel_button_enabled){
	draw_set_alpha(1);
	if(cancel_draw_border){
						
		var ax1 = cancel_button_x - cancel_button_width*.5;
		var ay1 = cancel_button_y - cancel_button_height*.5;
		var ax2 = cancel_button_x + cancel_button_width*.5;
		var ay2 = cancel_button_y + cancel_button_height*.5;
						
		draw_set_alpha(1)
		draw_set_color(c_white)
		if(global.high_contrast){
			draw_rectangle_ratio(ax1-5,ay1-5,ax2+5,ay2+5,false)
		} else {
	
			if(global.color_gradient){
				draw_rectangle_color_ratio(ax1-7,ay1-7,ax2+7,ay2+7,
				global.menu_grcolor[0],global.menu_grcolor[0],
				global.menu_grcolor[1],global.menu_grcolor[1],
				false)
		
			} else {
				draw_set_color(global.menu_grcolor[0])
				draw_rectangle_ratio(ax1-7,ay1-7,ax2+7,ay2+7,
				false)
			}
		}


		if(global.high_contrast){
			draw_set_color(c_black)
		} else {
			draw_set_color(global.menu_bgcolor)
		}
		draw_set_alpha(1)
		draw_rectangle_ratio(ax1,ay1,ax2,ay2,false)			
	}
	
	var col = global.text_fgcolor

	if(global.high_contrast){
		col = c_white;
	}

	
	if(cancel_sprite > -1){
		if(cancel_pressed){
			draw_sprite_extended_ratio(cancel_sprite,0,
										cancel_button_x, cancel_button_y,
										.85,.85,
										0,col,1);
		} else {
			draw_sprite_extended_ratio(cancel_sprite,0,
										cancel_button_x, cancel_button_y,
										cancel_hover,cancel_hover,
										0,col,1);
		}
	}	
}

// help button

if(help_button_enabled){
	draw_set_alpha(1);
	if(help_draw_border){
						
		var ax1 = help_button_x - help_button_width*.5;
		var ay1 = help_button_y - help_button_height*.5;
		var ax2 = help_button_x + help_button_width*.5;
		var ay2 = help_button_y + help_button_height*.5;
						
		draw_set_alpha(1)
		draw_set_color(c_white)
		if(global.high_contrast){
			draw_rectangle_ratio(ax1-5,ay1-5,ax2+5,ay2+5,false)
		} else {
	
			if(global.color_gradient){
				draw_rectangle_color_ratio(ax1-7,ay1-7,ax2+7,ay2+7,
				global.menu_grcolor[0],global.menu_grcolor[0],
				global.menu_grcolor[1],global.menu_grcolor[1],
				false)
		
			} else {
				draw_set_color(global.menu_grcolor[0])
				draw_rectangle_ratio(ax1-7,ay1-7,ax2+7,ay2+7,
				false)
			}
		}


		if(global.high_contrast){
			draw_set_color(c_black)
		} else {
			draw_set_color(global.menu_bgcolor)
		}
		draw_set_alpha(1)
		draw_rectangle_ratio(ax1,ay1,ax2,ay2,false)			
	}
	
	var col = global.text_fgcolor

	if(global.high_contrast){
		col = c_white;
	}

	
	if(help_sprite > -1){
		if(help_pressed){
			draw_sprite_extended_ratio(help_sprite,0,
										help_button_x, help_button_y,
										.85,.85,
										0,col,1);
		} else {
			draw_sprite_extended_ratio(help_sprite,0,
										help_button_x, help_button_y,
										help_hover,help_hover,
										0,col,1);
		}
	}	
}
