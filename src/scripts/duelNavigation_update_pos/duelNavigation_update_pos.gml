with(duelController){
	if(instance_exists(navigation_menu)){

		nav_pos[0] = duelCamera.left+(navigation_menu.x/800)*(duelCamera.origin_width)
		nav_pos[1] = duelCamera.top+(navigation_menu.y/600)*(duelCamera.origin_height)
		nav_pos[2] = duelCamera.left+((navigation_menu.x+ navigation_menu.menu_width)/800)*(duelCamera.origin_width)
		nav_pos[3] = duelCamera.top+((navigation_menu.y + navigation_menu.menu_height)/600)*(duelCamera.origin_height)
	}
}