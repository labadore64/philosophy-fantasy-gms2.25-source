// should be in all scripts
var index = argument[1];
var origin = argument[0];

with(origin){

	duel_animation_move_card(index,id,parent.part[DUEL_FIELD_LIMBO])

	duel_animation_card_effect(card_array[index],"discard",code_set_cardstate(parent.part[DUEL_FIELD_LIMBO],parent.part[DUEL_FIELD_LIMBO].card_array_size-1));

		with(duelController){
			do_end = true;
			do_end_timer = 2	
		}

}