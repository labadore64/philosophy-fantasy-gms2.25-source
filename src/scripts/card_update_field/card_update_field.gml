if(!card_changed){
	if(instance_exists(obj_data)){
		if(card_id > -1){
			var adder1 = 0;
			var adder2 = 0;

			for(var i = 0; i < 2; i++){
				if(card_overlay[i] > -1){
					adder1 += obj_data.card_attack_overlay[card_overlay[i]]
					adder2 += obj_data.card_defense_overlay[card_overlay[i]]
				}
			}
		
		

			card_attack_power = adder1 + obj_data.card_attack[card_id] + card_boost_attack + argument[0].att_boost
			card_defense_power = adder2 + obj_data.card_defense[card_id] + card_boost_defense + argument[0].def_boost

			surface_update = true
		}
	}
}