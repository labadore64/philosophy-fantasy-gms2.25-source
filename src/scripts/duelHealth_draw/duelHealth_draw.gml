with(argument[0]){
	/// @description Insert description here
	// You can write your code in this editor

	// update portrait

	if(!surface_exists(portrait_surf)){
		portrait_surf = surface_create_access(128*global.scale_factor,128*global.scale_factor)	
		portrait_update=true;
	} else {
		if(ScaleManager.updated){
			surface_resize(portrait_surf,128*global.scale_factor,128*global.scale_factor);
			portrait_update=true;
		}
	}
		
	if(portrait_update){
		gpu_set_colorwriteenable(true,true,true,true)
		surface_set_target(portrait_surf);
		draw_clear_alpha(c_black,0);
		var scale = .25
		if(!portrait_custom){
			var bgcolor = $222222

			draw_set_color(bgcolor)
			var adjust = 0;
			draw_circle_noratio((256-adjust)*scale,(256-adjust)*scale,60,false)
			gpu_set_colorwriteenable(true,true,true,false)
			draw_sprite_extended_noratio(portrait_sprite,0,0,0,scale,scale,0,c_white,1)
		} else {
			
			var bgcolor = portrait_color[CHARA_CUSTOM_COL_BACKGROUND]

			draw_set_color(bgcolor)
			var adjust = 0;
			draw_circle_noratio((256-adjust)*scale,(256-adjust)*scale,60,false)
			gpu_set_colorwriteenable(true,true,true,false)
			
			var hair_color = portrait_color[CHARA_CUSTOM_COL_HAIR]
			var skin_color = portrait_color[CHARA_CUSTOM_COL_SKIN]
			var eye_color = portrait_color[CHARA_CUSTOM_COL_EYE]
			var mouth_color = portrait_color[CHARA_CUSTOM_COL_MOUTH]
		
			var earring_color = portrait_color[CHARA_CUSTOM_COL_EARRING]
			var hat_color = portrait_color[CHARA_CUSTOM_COL_HAT]
			var glasses_color = portrait_color[CHARA_CUSTOM_COL_GLASSES]
			var shirt_color = portrait_color[CHARA_CUSTOM_COL_SHIRT]
			var necklace_color = portrait_color[CHARA_CUSTOM_COL_NECKLACE]
			
				var scale = .3
				custom_face_draw1(scale,-15-adjust-450,-35-adjust,
					hair_color,skin_color,shirt_color,necklace_color,
					portrait_property[CHARA_CUSTOM_VAL_CUTE],
					portrait_property[CHARA_CUSTOM_VAL_BACKHAIR],
					portrait_property[CHARA_CUSTOM_VAL_BODY],
					portrait_property[CHARA_CUSTOM_VAL_NECK],
					portrait_property[CHARA_CUSTOM_VAL_SHIRT],
					portrait_property[CHARA_CUSTOM_VAL_NECKLACE],true,true)
				custom_face_draw2(scale,-15-adjust-450,-35-adjust,
					skin_color,eye_color,hair_color,mouth_color,
					portrait_property[CHARA_CUSTOM_VAL_FACE],
					portrait_property[CHARA_CUSTOM_VAL_NOSE],
					portrait_property[CHARA_CUSTOM_VAL_MOUTH],
					portrait_property[CHARA_CUSTOM_VAL_EYE],
					portrait_property[CHARA_CUSTOM_VAL_EYEBROW],
					portrait_property[CHARA_CUSTOM_VAL_SNOUT],true)
				custom_face_draw3(scale,-15-adjust-450,-35-adjust,
					hair_color,skin_color,earring_color,hat_color,glasses_color,
					portrait_property[CHARA_CUSTOM_VAL_GLASSES],
					portrait_property[CHARA_CUSTOM_VAL_EAR],
					portrait_property[CHARA_CUSTOM_VAL_EARRING],
					portrait_property[CHARA_CUSTOM_VAL_FRONTHAIR],
					portrait_property[CHARA_CUSTOM_VAL_HAT],true);
			}
		gpu_set_colorwriteenable(true,true,true,true)
		portrait_update=false
		surface_reset_target();
	}
	gpu_set_colorwriteenable(true,true,true,false)
	

	// update surface

	if(!surface_exists(surf)){
		surf = surface_create_access(300*global.scale_factor,150*global.scale_factor)	
		surf_updated=true;
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,300*global.scale_factor,150*global.scale_factor);
			surf_updated=true;
		}
	}

	if(surf_updated || surface_update){
		var shift = 20;
	
		surface_set_target(surf);
	
		draw_clear_alpha(c_black,0);
		gpu_set_colorwriteenable(true,true,true,true)
	
		var barsize_w = 280-30
		var barsize_h = 16;
	
		var barshift = 55

		var bgcolor = $222222

		draw_set_color(bgcolor)

		if(x > 400){
			if(channel_card > -1){
				draw_rectangle_noratio(0,30+shift-6,64,128-30+shift,false)
			}

			draw_rectangle_noratio(40,40+shift+barshift-80,barsize_w-60,barsize_h+shift+barshift-5-80,false)
			draw_rectangle_noratio(30,40+shift+barshift,barsize_w-60,barsize_h+shift+barshift-5,false)
			//gpu_set_colorwriteenable(true,true,true,false)
			draw_surface_ext(portrait_surf,167*global.scale_factor-10+3*global.scale_factor,portrait_y+shift-25,1,1,0,c_black,.75)
			draw_surface(portrait_surf,167*global.scale_factor-10,portrait_y+shift-25)
			gpu_set_colorwriteenable(true,true,true,true)
			draw_set_color(global.text_fgcolor)
			draw_set_halign(fa_right)
			draw_text_transformed_noratio(170,-5+15+shift+barshift-77,name,2,2,0)
			if(global.display_hp){
				draw_text_transformed_noratio(160,-5+15+shift+barshift,string(hp_current_display)+" HP",2,2,0)
			}
			draw_set_halign(fa_left)
			if(channel_card > -1){
				draw_sprite_extended_noratio(obj_data.card_sprite[channel_card],0,
						3+0,97-30-shift,
						.23,.23,
						0,c_white,1)
			}
		} else {
			if(channel_card > -1){
				draw_rectangle_noratio(barsize_w+3,24+shift,barsize_w-64+3,80+shift-6,false)
			}
	
			draw_rectangle_noratio(55,40+shift+barshift-80,barsize_w-60+35,barsize_h+shift+barshift-5-80,false)
			draw_rectangle_noratio(30,40+shift+barshift,barsize_w+3+30-30,barsize_h+shift+barshift-5,false)
			//gpu_set_colorwriteenable(true,true,true,false)
			draw_surface_ext(portrait_surf,5+10-15-3*global.scale_factor,portrait_y+shift-25,1,1,0,c_black,.75)
			draw_surface(portrait_surf,5+10-15,portrait_y+shift-25)
			gpu_set_colorwriteenable(true,true,true,true)
			draw_set_color(global.text_fgcolor)
			draw_set_halign(fa_left)
			draw_text_transformed_noratio(125,-4+15+shift+barshift-77,name,2,2,0)
			if(global.display_hp){
				draw_text_transformed_noratio(125,-4+15+shift+barshift, string(hp_current_display) +" HP",2,2,0)
			}
			if(channel_card > -1){
				draw_sprite_extended_noratio(obj_data.card_sprite[channel_card],0,
						barsize_w-64+6,97-30-shift,
						.23,.23,
						0,c_white,1)
			}
		}


		draw_set_color(bgcolor)

		var movedown = 90

		if(x > 400){
			draw_rectangle_noratio(0+30,-5+3+movedown+shift,barsize_w+3+30,barsize_h+3+movedown+shift,false)
			if(hp_current > 0){
				var col = c_white;
				if(!global.high_contrast){
					col = draw_color
					draw_rectangle_color_noratio(3+barsize_w-(barsize_w*hp_percent)+30,
												movedown+shift,barsize_w+30,barsize_h+movedown+shift,
												$222222,$222222,
												col,col,
											false)
				
				} else {
					draw_set_color(col)
					draw_rectangle_noratio(3+barsize_w+30-(barsize_w*hp_percent),
												movedown+shift,barsize_w+30,barsize_h+movedown+shift,
												false)
				}
			}
		} else {
			draw_rectangle_noratio(0,-5+3+movedown+shift,barsize_w+3,barsize_h+3+movedown+shift,false)
			if(hp_current > 0){
				var col = c_white;
				if(!global.high_contrast){
					col = draw_color
					draw_rectangle_color_noratio(3,movedown+shift,barsize_w*hp_percent,barsize_h+movedown+shift,
												$222222,$222222,
												col,col,
											false)
				
				} else {
					draw_set_color(col)
					draw_rectangle_noratio(3,movedown+shift,barsize_w*hp_percent,barsize_h+movedown+shift,
												false)
				}
			}
			
			/*
			if(hp_current > 0){
				if(global.high_contrast){
					draw_set_color(c_white);
				} else {
					draw_set_color(draw_color);
				}

				draw_rectangle_noratio(0,movedown+shift,barsize_w*hp_percent,barsize_h+movedown+shift,false)
			}
			*/
		}
		surface_reset_target();
		surface_update = false;
		surf_updated = false;
	}

	draw_surface(surf,global.display_x+x*global.scale_factor,global.display_y+y*global.scale_factor);	
}