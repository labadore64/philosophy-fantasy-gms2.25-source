
		// update input queue	
		var element = argument[0];
		var ind = argument[1];
		var counter = 0;

		var base  = element;

		// parse target, command
		var pos = string_pos(":",element)
		var command = string_copy(element,1,pos-1);
		element = string_delete(element,1,pos);
	
		// now extract the target
		var target_side = 0;
		var target_index = 0;
		var target_origin = noone;
	
		var stringe = element
		var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
		var side = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
			
		//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		

		if(side == "enemy"){
			target_side = DUEL_PLAYER_ENEMY
		} else {
			target_side = DUEL_PLAYER_YOU	
		}
			
		// if the code is a location, parse as location.
		if(code_is_location(ori)){
				
			if( string_length(string_digits(stringe)) == string_length(stringe)){
				
				target_index = string_copy(stringe,string_pos(":",stringe)+1,
										string_length(stringe) - string_pos(":",stringe))

				// string == variable
				if(string_length(string_digits(target_index)) != string_length(target_index)){
					var value = duelController.code_values[? target_index];
					if(is_undefined(value) || !is_real(value)){
						value = 0;	
					}
				
					target_index = value;
				} else {
					target_index = real(target_index);	
				}
				
				if(ori == "overlay"){
					target_index++;	
				}
			} else {
				// check if variable	
				// get number from variables
				if(ds_map_exists(duelController.code_values,stringe)){
					var def = duelController.code_values[? stringe]
					if(!is_undefined(def)){
						target_index = real(def);	
					}
				}
			}
				
			target_origin = code_get_location(ori,target_side)
		}

		// pass the target args to the command
		var script = asset_get_index("duel_input_" + command);

		if(script > -1){
	
			// execute here 
			if(ind > -1){
				global.ai_step = ind;
			}
			script_execute(script,target_origin,target_index,ind);
	
		}

		

		
	//}
