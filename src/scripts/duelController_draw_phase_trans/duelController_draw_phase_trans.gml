

if(!surface_exists(message_surf)){
	message_surf= surface_create_access(global.letterbox_w,global.letterbox_h)	
	message_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(message_surf,global.letterbox_w,global.letterbox_h);
		message_surf_update = true;
	}
}

if(message_surf_update){
	surface_set_target(message_surf)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)
	draw_set_font(global.largeFont)
	draw_set_halign(fa_center)
	draw_set_valign(fa_middle)
	draw_set_color(global.menu_bgcolor)
	draw_set_alpha(1)
	
	if(global.menu_animate){

		draw_set_color(c_black)
		draw_set_alpha(1)

		if(duelController.phase_current == DUEL_PHASE_BATTLE){
			
			// get the philosophers
			var fadescale = .8

			for(var i = 0; i < 2; i++){
				if(!surface_exists(fade_surface[i])){
					fade_surface[i] = surface_create_access(fadescale*245*global.scale_factor,fadescale*256*global.scale_factor)	
					fade_surface_updated=true;
				} else {
					if(ScaleManager.updated){
						surface_resize(fade_surface[i],fadescale*245*global.scale_factor,fadescale*256*global.scale_factor);
						fade_surface_updated=true;
					}
				}

				if(fade_surface_updated){
					//preview_philosopher	
					surface_set_target(fade_surface[i])
					draw_clear_alpha(c_black,0)
					gpu_set_colorwriteenable(true,true,true,true)
					draw_sprite_extended_noratio(dialog_philosopher,0,0,0,
												fadescale,fadescale,0,c_white,1)
					if(dialog_id[i] > -1){
						gpu_set_colorwriteenable(true,true,true,false)
						draw_sprite_extended_noratio(dialog_sprite[i],0,0,0,
													fadescale,fadescale,0,c_white,1)
						gpu_set_colorwriteenable(true,true,true,true)
					}
					surface_reset_target();
					fade_surface_updated = false;
					surface_update=true
				}
			}
			
			
			// draw the thing
			
			if(bg_height > 0){
				draw_rectangle_noratio(0,300-bg_height,800,300+bg_height,false)
			}
			gpu_set_colorwriteenable(true,true,true,false)
			//-bg_height 
			draw_surface(fade_surface[0],
						(125)*global.scale_factor-bg_height,
						210*global.scale_factor)
			// bg_height +
			draw_surface(fade_surface[1],
						(475)*global.scale_factor+bg_height,
						210*global.scale_factor)
			if(global.particles){part_system_drawit(Sname);}
			
			gpu_set_colorwriteenable(true,true,true,true)
		}

		draw_text_transformed_noratio(x-4,y,message,
									4,4,0)
		draw_set_color(c_white)
		draw_text_transformed_noratio(x,y,message,
									4,4,0)
	} else {
		draw_text_transformed_noratio(400-4,300,message,
									4,4,0)
		draw_set_color(c_white)
		draw_text_transformed_noratio(400,300,message,
									4,4,0)
	}
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	draw_set_alpha(1)	
	surface_reset_target();
}

draw_surface_ext(message_surf,global.display_x,global.display_y,
				1,1,0,c_white,
				animationPhaseTransition.image_alpha)
	