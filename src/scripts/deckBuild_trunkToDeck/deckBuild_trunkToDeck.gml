if(argument[0] > -1){
	if(obj_data.card_in_trunk[argument[0]] > 0){
		// count how many times it appears in the deck.
		// You cannot add more than 3 of a card to a deck
		
		var counter = 0;
		var sizer = ds_list_size(obj_data.current_deck);
		for(var i = 0; i < sizer; i++){
			if(obj_data.current_deck[|i] == argument[0]){
				counter++;	
			}
		}
		
		if(counter < 3){
			obj_data.card_in_trunk[argument[0]]--;
			ds_list_add(obj_data.current_deck,argument[0]);
		}
		
	}
}