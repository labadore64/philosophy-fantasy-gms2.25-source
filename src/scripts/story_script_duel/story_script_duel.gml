var event = "";
if(argument_count > 2){
	event = argument[2];
}

if(global.skip_duel){
	var counter = 0;
	while(story_counter< story_size &&
		string_pos("_duel_",script_get_name(story_script[story_counter])) > 0){
		
		obj_data.losses++;
		// increment the story counter
		story_counter++;
		counter++;
		if(counter > 1000000){
			break;	
		}
	}
	if(story_counter > story_size-1){
		story_counter = story_size-1;	
	} else {
		story_counter--;	
	}
	
	if(event != ""){
		eventSet((event))
		global.event_set = ""
	}
	
	// save the game.
	save_game();
	room_goto(MainRoom)
} else {
	global.event_set = event;
	obj_data.loaded_opponent = argument[0]
	obj_data.loaded_duel = argument[1]

	if(obj_data.opponent_encounter[obj_data.loaded_opponent] < obj_data.loaded_duel){
		obj_data.opponent_encounter[obj_data.loaded_opponent] =	obj_data.loaded_duel
	}

	// reduces this. you have to win to get it back.
	obj_data.loaded_cutscene_count--;
	obj_data.story_counter--;
	obj_data.loaded_cutscene_counter = obj_data.loaded_cutscene_count;
	obj_data.story_duel = true;

	room_goto(DuelStartRoom)
}