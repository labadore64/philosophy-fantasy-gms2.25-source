var posxx = argument[2]*global.scale_factor;
var posyy = argument[3]*global.scale_factor;
var scalex = argument[4]*global.scale_factor;
var scaley = argument[5]*global.scale_factor;

draw_sprite_ext(
	argument[0],
	argument[1],
	global.display_x + posxx,
	global.display_y + posyy,
	scalex,
	scaley,
	argument[6],
	argument[7],
	argument[8])