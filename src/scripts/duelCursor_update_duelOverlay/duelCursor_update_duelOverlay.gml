/// @description Insert description here
// You can write your code in this editor
if(global.mouse_active ){
	with(duelOverlayCardChoose){
		if(menuControl.active_menu == id){
			
		with(duelCursor){
			x = MouseHandler.mousepos_x
			y = MouseHandler.mousepos_y
		}
		if(duelCursor.xprevious != MouseHandler.mousepos_x||
			duelCursor.yprevious != MouseHandler.mousepos_y){
			duelOverlayCardChoose.mouse_mode = true;		
		}
		
		if(mouse_mode){
			var last_selected = menupos;
			menupos = -1;
			for(var i = 0; i < 2; i++){
				if(MouseHandler.mouse_points_trans[i,0] < duelCursor.x &&
					MouseHandler.mouse_points_trans[i,2] > duelCursor.x &&
					MouseHandler.mouse_points_trans[i,1] < duelCursor.y &&
					MouseHandler.mouse_points_trans[i,3] > duelCursor.y){
					menupos = i;
					break
				}
			}

			if(last_selected != menupos){
				info_surf_update = true;	
			}
		}
			
		}
	}
}