/// @description - play a new song
/// @param song
/// @param time

with(MusicPlayer){
	if(current_audio != argument[0]){
		audio_stop_sound(current_audio);
		current_audio = argument[0]
		last_song = current_song;
		
		current_song = audio_play_sound(argument[0],1,true);
		audio_sound_gain(current_song,volume,0)
	}
}