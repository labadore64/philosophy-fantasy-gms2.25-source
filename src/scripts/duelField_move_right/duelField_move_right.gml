with(selected_object){
	if(menu_right != -1){
		if(menu_right_enabled){
			script_execute(menu_right)	
		}
	}
}

with(selected_object){
			
	var sound = -1;
	if(global.menu_sounds){sound =soundfxPlay(sfx_sound_menu_default);}

	if(global.positional_pitch){
		if(sound > -1){
			if(object_index == duelHand || object_index == duelBench ||
				object_index == duelActive){
				var diff = POSITION_PITCH_MAX - POSITION_PITCH_MIN;
				audio_sound_pitch(sound,POSITION_PITCH_MIN + diff*(max(menupos,0) / max(card_array_size,1)));
			} else {
				var diff = POSITION_PITCH_MAX - POSITION_PITCH_MIN;
				var valuez = 1;
				audio_sound_pitch(sound,POSITION_PITCH_MIN + diff*(valuez));
			}
		}
	}	
}