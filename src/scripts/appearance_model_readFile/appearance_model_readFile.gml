/*
property_names[CHARA_CUSTOM_VAL_CUTE] = "Background"
property_names[CHARA_CUSTOM_VAL_BACKHAIR] = "Back Hair"
property_names[CHARA_CUSTOM_VAL_BODY]= "Body"
property_names[CHARA_CUSTOM_VAL_NECK]= "Neck"
property_names[CHARA_CUSTOM_VAL_SHIRT]= "Shirt"
property_names[CHARA_CUSTOM_VAL_NECKLACE] = "Necklace"
property_names[CHARA_CUSTOM_VAL_FACE]= "Face"
property_names[CHARA_CUSTOM_VAL_NOSE]= "Nose"
property_names[CHARA_CUSTOM_VAL_MOUTH]= "Mouth"
property_names[CHARA_CUSTOM_VAL_EYE]= "Eyes"
property_names[CHARA_CUSTOM_VAL_EYEBROW] = "Eyebrows"
property_names[CHARA_CUSTOM_VAL_GLASSES] = "Glasses"
property_names[CHARA_CUSTOM_VAL_EAR] = "Ears"
property_names[CHARA_CUSTOM_VAL_EARRING] = "Earings"
property_names[CHARA_CUSTOM_VAL_FRONTHAIR] = "Front Hair"
property_names[CHARA_CUSTOM_VAL_HAT] = "Hat"
property_names[CHARA_CUSTOM_VAL_SNOUT] = "Snout"
*/

var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\avatar.txt";
var testname = "";

testname = filename//+string(itemcount)+ext;

var sizer = array_length_1d(property_names_id)


if(file_exists(testname)){
	//loads data
	// read the code variable cuz ini doesn't like it for some reason
	var file = file_text_open_read(testname)
	var ss;
	var len;
	while (!file_text_eof(file))
	{
		var idz = string_replace(text_file_readln_correct(file),"id:","");
		var counter = -1;
		// get id value
		for(var i =0; i < sizer; i++){
			if(property_names_id[i] == idz){
				counter = i;
				break;
			}
		}
		if(counter > -1){
			property_names[counter] = string_replace(text_file_readln_correct(file),"name:","")
			property_x[counter] = real((string_replace(text_file_readln_correct(file),"x:","")));
			property_y[counter] = real((string_replace(text_file_readln_correct(file),"y:","")));
			property_scale[counter] = real(string_replace(text_file_readln_correct(file),"scale:",""));
			var sprite  = string_replace(text_file_readln_correct(file),"sprite:","");
			if(string_pos(",",sprite) > 0){
				var queue = split_string(sprite);
				var size = ds_queue_size(queue);
				for(var i = 0; i < size; i++){
					sprite = ds_queue_dequeue(queue);
					property_sprite[counter,i] = asset_get_index("spr_faces_" + sprite)		
				}
				ds_queue_destroy(queue);
			} else {
				property_sprite[counter,0] = asset_get_index("spr_faces_" + sprite)	
			}
			
			// now add all the names.
			var namesize = sprite_get_number(property_sprite[counter,0])
			var stop = false;
			var val = "";
			for(var i = 0; i < namesize; i++){
				if(!stop){
					val = text_file_readln_correct(file);
				}
				if(string_pos("=====",val) > 0){
					stop = true;	
				}
				if(!stop){
					property_part_names_unmod[counter,i] = val;
					property_part_names[counter,i] = textForceCharsPerLine(val,15);
				}
			}
			
			// if stop hasn't been reached for some reason, just keep going until you do
			if(!stop){
				while(!file_text_eof(file) &&
					string_pos("=====",text_file_readln_correct(file)) == 0){
						
				}
			}
		}
	}
	
	file_text_close(file);
	
}
