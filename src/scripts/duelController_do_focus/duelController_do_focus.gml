// here you can focus on different parts of the field with a button combo!
var enemy = DUEL_PLAYER_YOU
var field = -1;
var pos = -1;

if(running_obj < 0 || code_select_active){
	if(active_player == DUEL_PLAYER_YOU || code_select_active){
		if(!duel_camera_moving() || duelController.code_select_active){
			
			if(!keypress_this_frame){
	
				if(keyboard_check(global.keybind_value_array[KEYBOARD_KEY_VIEW_SWITCH]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_SWITCH] == KEY_STATE_HOLD){	
					enemy = DUEL_PLAYER_ENEMY;	
				}
			}

			// hand
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_HAND]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_HAND] == KEY_STATE_PRESS){
					field = DUEL_FIELD_HAND
					keypress_this_frame = true;
				}	
			}

			// active
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_ACTIVE]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_ACTIVE] == KEY_STATE_PRESS){
					field = DUEL_FIELD_ACTIVE
					pos = 0;
					// select the
					keypress_this_frame = true;
				}	
			}

			// overlay
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_OVERLAY]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_OVERLAY] == KEY_STATE_PRESS){
					field = DUEL_FIELD_ACTIVE
					pos = 1
					// select the
					keypress_this_frame = true;
				}	
			}

			// bench
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_BENCH]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_BENCH] == KEY_STATE_PRESS){
					field = DUEL_FIELD_BENCH
					// select the
					keypress_this_frame = true;
				}	
			}

			// theory
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_THEORY]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_THEORY] == KEY_STATE_PRESS){
					field = DUEL_FIELD_THEORY
					// select the
					keypress_this_frame = true;
				}	
			}

			// limbo
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_LIMBO]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_LIMBO] == KEY_STATE_PRESS){
					field = DUEL_FIELD_LIMBO
					// select the
					keypress_this_frame = true;
				}	
			}

			// deck
			if(!keypress_this_frame){
				if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEW_DECK]) ||
					global.gamepad_state_array[KEYBOARD_KEY_VIEW_DECK] == KEY_STATE_PRESS){
					field = DUEL_FIELD_DECK
					// select the
					keypress_this_frame = true;
				}	
			}


			if(field > -1){
				mouse_mode = false;
				
				if(pos > -1){
					duelField_selectObject(duelController.field[enemy],field,pos)	
				} else {
					duelField_selectObject(duelController.field[enemy],field)	
				}
				duelController.selected_object = duelController.field[enemy]
				with(duelController.field[enemy].selected_object){
					info_surf_update =true;
				}
				//duelController_resume_control()
				if(!duelController.regain_control){
					duelController.resume_control = true;
					duelController.resume_control_counter = DUEL_RESUME_CONTROL_TIME
				}
			}
			
}}}