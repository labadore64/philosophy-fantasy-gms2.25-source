	if(card_id > -1){
		surface_set_target(surf);
		draw_set_halign(fa_left);
		if(display_front){
			if(obj_data.card_type[card_id] == CARD_TYPE_PHILOSOPHER){
				card_draw_philosopher();
			} else if(obj_data.card_type[card_id] == CARD_TYPE_EFFECT){
				card_draw_effect();
			} else {
				card_draw_metaphys();	
			}
		} else {
			draw_clear_alpha(c_black,0)
			var space = 20;
			var space2 = 3;
			gpu_set_texfilter(false);
			gpu_set_colorwriteenable(true,true,true,true)
			draw_set_alpha(1)
			draw_sprite_extended_noratio(spr_card_back,0,0,0,1,1,0,c_white,1)
		}
		surface_reset_target();
	}