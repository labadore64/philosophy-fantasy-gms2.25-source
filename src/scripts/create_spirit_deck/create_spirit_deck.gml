var obj = instance_create(argument[0],argument[1],StarterDeck);
with(obj){
	image_blend = $7F7FFF
	card_sprite = card_from_name("aristotle")
	bg_sprite = obj_data.card_sprite_bg[card_from_name("aristotle")]
	type_sprite = spr_typeSpirit

	surface_update = false;
	surf = -1;

	card_width = 325
	card_height = 440

	name = "Greek\nClassics"
	subtitle = "A deck with\nlots of strategies\nand options."

	scale = .65
}
return obj;