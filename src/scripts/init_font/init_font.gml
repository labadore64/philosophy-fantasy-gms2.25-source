//global.standardFont = font_add_sprite_ext(sprite_font, "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890[].!?,", true, 3);
global.normalFont = font_add_sprite_ext(fontNormal, "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890[].!?,/+-\':%^()$@\"#;*“”¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ†‡Š‹™", true, 3);
global.mediumFont = global.normalFont;
global.largeFont = font_add_sprite_ext(fontLarge,"AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890[].!?,/+-\':%^()$@\"#;*“”¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ", true, 3);
