// arg0 = x
// arg1 = y
// arg2 = diagram index
// arg3 = page number


var index = argument[2]

if(index > -1){

	var obj = instance_create(argument[0]+objdata_diagram.xoffset[index],
						argument[1]+objdata_diagram.yoffset[index],
						menuDiagram);

	obj.sprite_index = objdata_diagram.sprite[index];
	obj.image_index = objdata_diagram.frame[index]
	obj.image_speed = 0
	obj.image_xscale = objdata_diagram.xscale[index];
	obj.image_yscale = objdata_diagram.yscale[index];
	obj.image_blend = make_color_rgb(
						objdata_diagram.r[index],
						objdata_diagram.g[index],
						objdata_diagram.b[index]
						)
	obj.alt_text = objdata_diagram.alt_text[index];

	obj.min_index = argument[3]*display_lines;
	obj.max_index = (argument[3]+1)*display_lines-1;

	ds_list_add(diagrams,obj);
	diagram_length = ds_list_size(diagrams);

}