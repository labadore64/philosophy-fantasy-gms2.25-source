var returner = ""

var ai_counter = -1;

if(argument_count > 0){
	ai_counter = argument[0]	
}

for(var i = 0; i < part_size; i++){
	var parts = part[i];
	if(parts.ai_test_script > -1){
		for(var j = 0; j < parts.card_array_size; j++){
			if(parts.ai_test_script > -1){
				returner += script_execute(parts.ai_test_script,j,parts,ai_counter);
			}
		}
	}
}

return returner;