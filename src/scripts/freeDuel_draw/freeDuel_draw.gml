var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}


with(mainMenu){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		mainMenu_DrawBackground()	
	}
}

if(!surface_exists(surf)){
	surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	surface_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h);
		surface_update = true;
	}
}

var fadescale = 1.9

if(!surface_exists(fade_surface)){
	fade_surface = surface_create_access(fadescale*245*global.scale_factor,fadescale*256*global.scale_factor)	
	fade_surface_updated=true;
} else {
	if(ScaleManager.updated){
		surface_resize(fade_surface,fadescale*245*global.scale_factor,fadescale*256*global.scale_factor);
		fade_surface_updated=true;
	}
}

if(fade_surface_updated){
	//preview_philosopher	
	surface_set_target(fade_surface)
	draw_clear_alpha(c_black,0)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_sprite_extended_noratio(preview_philosopher,0,30,0,
								fadescale,fadescale,0,c_white,1)
	if(menu_sprite[menupos] > -1){
		gpu_set_colorwriteenable(true,true,true,false)
		draw_sprite_extended_noratio(obj_data.card_sprite[menu_script[menupos]],0,30,0,
									fadescale,fadescale,0,c_white,1)
		gpu_set_colorwriteenable(true,true,true,true)
	}
	surface_reset_target();
	fade_surface_updated = false;
	surface_update=true
}

if(surface_update){
	surface_set_target(surf)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)

	var bla = global.menu_bgcolor;

	if(global.high_contrast){
		bla = c_black;	
	}

	draw_set_color(bla)
	draw_set_alpha(.75)
	draw_rectangle_noratio(20,0,780,option_y_offset+2-10,false)
	draw_rectangle_noratio(20,option_y_offset+3-10,400,600,false)
	draw_set_alpha(1)
	draw_set_color(col)
	draw_set_font(global.largeFont)
	draw_text_transformed_noratio(40,30,"Free Game",4,4,false)
	draw_set_font(global.normalFont)
	
	// draw actual options
	var counter = 0;
	for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
		/*
		if(counter == selected_draw_index){
			if(global.high_contrast){
				draw_set_alpha(.6)	
			} else {
				draw_set_alpha(.4)
			}
		} else {
			if(global.high_contrast){
				draw_set_alpha(.25)
			} else {
				draw_set_alpha(.1)
			}
		}
		draw_rectangle_noratio(20,option_y_offset+3-10 + option_spacer*counter,
							400,option_y_offset-3-10 + option_spacer*(counter+1),
							false)
		*/
		gpu_set_colorwriteenable(true,true,true,true)
		if(counter == selected_draw_index){
			draw_set_color(c_white)
			if(global.menu_animate){
				draw_set_alpha(alpha_adder);
			} else {
				draw_set_alpha(alpha_max)
			}
			if(global.high_contrast){
				draw_rectangle_noratio(20,option_y_offset+3-10 + option_spacer*counter,
							400,option_y_offset-3-10 + option_spacer*(counter+1),
							false)
			} else {
				draw_rectangle_color_noratio(20,option_y_offset+3-10 + option_spacer*counter,
							400,option_y_offset-3-10 + option_spacer*(counter+1),
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
				false)
			}
		} else {
			draw_set_color(bla)
			draw_set_alpha(.85)
			draw_rectangle_noratio(20,option_y_offset+3-10 + option_spacer*counter,
									400,option_y_offset-3-10 + option_spacer*(counter+1),
									false)
		}
					
		//gpu_set_colorwriteenable(true,true,true,false)
		draw_set_alpha(1)
		draw_set_color(col)
		
		draw_set_alpha(1)
		draw_text_transformed_noratio(10+option_x_offset,2+option_y_offset + option_spacer*counter,
							string_copy(menu_name[i],1,30),3,3,0)
		
		draw_set_halign(fa_left)
		
		counter++;
	}
	gpu_set_blendmode(bm_add)
	draw_surface_ext(fade_surface,335*global.scale_factor,115*global.scale_factor,1,1,0,c_white,.75)
	gpu_set_blendmode(bm_normal)
	draw_sprite_extended_noratio(menu_sprite[menupos],0,455+translation,120,1,1,0,c_white,1)
	
	// draw thingie
	surface_reset_target();
}

draw_surface(surf,global.display_x,global.display_y);
	
with(fadeOut){
	draw_fade();	
}
	
menuParent_draw_cancel_button()

draw_letterbox();