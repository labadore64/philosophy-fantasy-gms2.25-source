var deck = argument[0];
var list = -1;
if(argument_count > 1){
	list = argument[1]
}

var filename = working_directory + "resources\\deck\\deck"+string(argument[0]);;
var ext = ".ini";
var testname = "";

testname = filename+ext;

if(file_exists(testname)){
	ini_open(testname);
	var card;
	
	for(var i = 0; i < 20; i++){
		card = card_from_name(ini_read_string_length("card","card" + string(i),"",40));
		if(card > -1){
			if(list < 0){
				duel_deck_addCard(field[1].deck,card);
				spr_load_card_front(card);
			} else {
				ds_list_add(list,obj_data.card_name[card]);
			}
		}
	}
	
	ini_close();
}