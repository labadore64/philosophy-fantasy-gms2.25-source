if(!argument[0]){
	// animation
	
	var status = "";
	var arg = "";
	var target = "";
	var quantity = 0;
	
	// to do = parse status, arg and target for a status effect.
	// you will store the status string and apply it to the side that correlates with the target.
	
	for(var i = 0; i < code_argument_size; i++){

		// parse targets
		if(string_pos("target:",code_arguments[i]) > 0){
			var target = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
		// parse targets
		if(string_pos("args:",code_arguments[i]) > 0){
			var arg = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
		// parse destination
		if(string_pos("status:",code_arguments[i]) > 0){
			status = string_copy(code_arguments[i],string_pos(":",code_arguments[i])+1,
											string_length(code_arguments[i]) - string_pos(":",code_arguments[i]))
		}
		
		
		// parse quantity
		if(string_pos("quantity:",code_arguments[i]) > 0){
			quantity = real(string_digits(string_copy(code_arguments[i],string_pos(":",code_arguments[i])+1,
											string_length(code_arguments[i]) - string_pos(":",code_arguments[i]))))
		}
	}
	
	if(status != "" &&
		target != "" &&
		quantity > 0){
			duel_animation_code_cardStatus(status,arg,target,quantity);
	}
}