var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

if(global.high_contrast){
	draw_clear(c_black)
} else {
	cardInfo_DrawBackground()
}

	var adjust = -20;
	var move = 80
	var xxx = 400
	var space = 25;

if(!surface_exists(surf)){
	surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	surf_updated = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h);
		surf_updated = true;
	}
}

if(!global.high_contrast){
	gpu_set_blendmode(bm_subtract)
	draw_set_alpha(.1)
	draw_set_color($555555)
	draw_rectangle_ratio(0,59,400-10+25,600,false)
			
			
	draw_set_alpha(1)
	gpu_set_blendmode(bm_normal)
}

if(card > -1){
	

	if(surf_updated){
		surface_set_target(surf)
	
		draw_clear_alpha(c_black,0);
	
		draw_set_color(col)
		draw_set_font(global.normalFont)
	
		draw_text_transformed_noratio(20,5,obj_data.card_name[card_id],4,4,0)
	
		draw_text_transformed_noratio(30,60,archetype,2,2,0)
		draw_text_transformed_noratio(30,80,archetype2,2,2,0)
		//draw_text_transformed_ratio(20,120,card_text,2,2,0)
	
		for(var i = 0; i < CARD_DISPLAY_EFFECT_LINES; i++){
			draw_text_transformed_noratio(20,-7+120+space*i,card_effect_desc[i],2,2,0)
		}
	
		menuBookDrawDiagramsSpecial();
	
		// draw trivia
		// year
	
		draw_set_halign(fa_right)
	
		if(obj_data.card_type[card_id] == CARD_TYPE_PHILOSOPHER){
			adjust = 0;
			draw_text_transformed_noratio(765,460+move,"("+obj_data.card_years[card_id]+")",1,1,0)
		}
	
		// bio
		//draw_text_transformed_ratio(20,480+adjust,bio,2,2,0)
		var space = 12
	
		for(var i = 0; i < CARD_DISPLAY_BIO_LINES; i++){
			draw_text_transformed_noratio(765,475+move+space*i,card_bio_desc[i],1,1,0)
		}
		draw_set_halign(fa_left)
	
		surface_reset_target();
		surf_updated = false;
	}
	draw_surface(surf,global.display_x,global.display_y);
	
	gpu_set_blendmode(bm_add)
	if(!global.high_contrast){
		draw_rectangle_color_ratio(430,448+move,800,450+move,c_black,$222222,$222222,c_black,false)
	} else {
		draw_rectangle_color_ratio(430,448+move,770,450+move,c_black,$222222,$222222,c_black,false)
	}
	gpu_set_blendmode(bm_normal)
	
	card_draw(card);
	
	menuDrawTooltip()
	
	draw_letterbox();
}
