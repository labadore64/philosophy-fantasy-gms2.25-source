with(titleScreen){
	script_execute(menu_draw)	
}
with(duelController){
	if(!instance_exists(duelSub)){
		script_execute(menu_draw)	
	}
}
with(duelSub){
	script_execute(menu_draw)	
}

var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

draw_set_alpha(.75)
draw_set_color(c_black)
draw_rectangle_ratio(0,0,800,600,false)

draw_set_font(global.normalFont)
draw_set_alpha(1)

	draw_set_color(c_white)
	if(global.high_contrast){
		draw_rectangle_ratio(x-5,y-5,x+menu_width+5,y+menu_height+5,false)
	} else {

		if(global.color_gradient){
			draw_rectangle_color_ratio(x-7,y-7,x+menu_width+7,y+menu_height+7,
				global.menu_grcolor[1],global.menu_grcolor[1],
				global.menu_grcolor[0],global.menu_grcolor[0],
			false)
		
		} else {
			draw_set_color(global.menu_grcolor[0])
			draw_rectangle_ratio(x-7,y-7,x+menu_width+7,y+menu_height+7,
			false)
		}

	}

	if(global.high_contrast){
		draw_set_color(c_black)
	} else {
		draw_set_color(global.menu_bgcolor)
	}
	draw_set_alpha(1)
	draw_rectangle_ratio(x,y,x+menu_width,y+menu_height,false)

draw_set_color(c_white)
if(global.menu_animate){
	draw_set_alpha(alpha_adder);
} else {
	draw_set_alpha(alpha_max)
}
if(global.high_contrast){
	draw_rectangle_ratio(x,y-10+option_y_offset+option_spacer*(menupos),
					x+menu_width,y-10+option_y_offset+option_spacer*(menupos+1),
				false)
} else {
	draw_rectangle_color_ratio(x,y-10+option_y_offset+option_spacer*(menupos),
					x+menu_width,y-10+option_y_offset+option_spacer*(menupos+1),
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
	false)
}
draw_set_alpha(1)
draw_set_color(col)
draw_set_halign(fa_center)
draw_text_transformed_ratio(x+250,y+15,title,3,3,0)	
for(var i = 0; i < menu_count; i++){
	draw_text_transformed_ratio(x+option_x_offset+40,-5+y+option_y_offset+option_spacer*i,menu_name[i],3,3,0)	
}
draw_set_halign(fa_left)

menuParent_draw_cancel_button();

draw_letterbox();

//draw_sprite_extended_ratio(spr_talk_continue,ScaleManager.spr_counter+15, x+option_x_offset-100,y-10+option_y_offset+option_spacer*menupos,1.5,1.5,0,c_white,1)	