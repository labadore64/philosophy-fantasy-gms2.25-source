if(duelController.temp_turns == 0){
	var adder = 0;
	if(part[DUEL_FIELD_ACTIVE].ai_card_array[argument[0],0] > -1){
		adder += obj_data.card_defense[part[DUEL_FIELD_ACTIVE].ai_card_array[argument[0],0]]
	}
	for(var i = 1; i < 3; i++){
		if(part[DUEL_FIELD_ACTIVE].ai_card_array[argument[0],i] > -1){
			adder += obj_data.card_defense_overlay[part[DUEL_FIELD_ACTIVE].ai_card_array[argument[0],i]]
		}
	}
	
	adder += def_boost
	return adder + ai_hp[argument[0]]*.5 + (20-ai_hp_enemy[argument[0]])*.25;
} else {
	var returner = 0;
	
	
	if(duelController.temp_phase_current != DUEL_PHASE_END){
		
		returner = 40 - ai_hp_enemy[argument[0]]	
		
	} else {
		var hand_success = true;
		
		// if the discarded card doesn't decrease the hand count, strongly oppose discarding
		if(part[DUEL_FIELD_HAND].ai_card_count[argument[0]] >= part[DUEL_FIELD_HAND].original_count){
			hand_success = false;
		}
		
		// check some things about the moved card, if applicable.
		if(part[DUEL_FIELD_LIMBO].ai_card_count[argument[0]] > part[DUEL_FIELD_LIMBO].original_count){
			var card_id = part[DUEL_FIELD_LIMBO].ai_card_array[argument[0],part[DUEL_FIELD_LIMBO].original_count]
			
			// lower weight of permadeath cards
			if(card_has_keyword(card_id,"permadeath")){
				returner -= 35
			}
			if(card_has_keyword(card_id,"exiled")){
				returner -= 75
			}
		}
		
		if(hand_success){
			// checks the cards left in the hand
			// the hands with the highest stats are favored
			for(var i = 0; i < part[DUEL_FIELD_HAND].ai_card_count[argument[0]]; i++){
				if(part[DUEL_FIELD_HAND].ai_card_array[argument[0],i] > -1){
					returner += obj_data.card_attack[part[DUEL_FIELD_HAND].ai_card_array[argument[0],i]]
					returner += obj_data.card_defense[part[DUEL_FIELD_HAND].ai_card_array[argument[0],i]]
				}
			}
		}
		
	}
	
	return returner;
}