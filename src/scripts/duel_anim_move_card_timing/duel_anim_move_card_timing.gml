var argu = argument[0]; // args as json string
var cando = false;

var _card_id,_card_origin,_card_dest,_time,_facedown,_camera_move,_block,flag;
	

// ai
if(argument_count > 1){
	cando = true
	
	_card_id = argument[0]
	_card_origin = argument[1]
	_card_dest = argument[2]
	_time = argument[3]
	_facedown = argument[4]
	_camera_move = argument[5]
	_block = argument[6]
	flag = argument[7]
} else {
	var map = json_decode(argu);

	if(!ds_map_empty(map)){
		// do your normal code down here
		_card_id = map[? "card_id"];
		_card_origin = map[? "origin"];
		_card_dest = map[? "dest"];
		_time = map[? "time"];
		_facedown = map[? "facedown"];
		_camera_move = map[? "movecamera"];
		_block = map[? "block"];
		flag = map[? "flag"];
	
		if(!is_undefined(_card_id) &&
			!is_undefined(_card_origin) &&
			!is_undefined(_card_dest) &&
			!is_undefined(_time) &&
			!is_undefined(_facedown) &&
			!is_undefined(_block) &&
			!is_undefined(flag) &&
			!is_undefined(_camera_move)){
				cando = true
			}
		
	}
}

if(cando){
	// test if the camera needs to be moved.
	
	if(global.camera_track == CAMERA_TRACK_ALL){

		if(_card_origin > -1 && _card_id > -1){
			if(_card_origin.card_array_size > _card_id){
				if(_card_origin.card_array[_card_id] > -1){
					if(instance_exists(_card_dest)){
						with(duelCamera){
							if(duelController.mouse_mode){
								var modi = 0;
								var scale = 2.75;
			
								if(duelController.active_player = DUEL_PLAYER_ENEMY){
									modi = -550
								}
					
								if(_card_dest.object_index == duelActive){
									modi = -(290+290-240)
								}
							
								if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
									modi -= TOOLTIP_CAMERA_ADJUST_Y
									scale = TOOLTIP_CAMERA_ADJUST_SCALE
								}
			

								duel_animation_move_camera(225+110,290+290+modi,scale,10)

							}
						}
					}
				}
			}
		}
	}	
	
	// do the move animation
	if(duelController.ai_mode){

		duel_anim_queue_move_card(_card_id,_card_origin,_card_dest,_time,_facedown,_camera_move,_block,flag)
	} else {
		var json = map
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
		
		duel_animation_pack(obj,queue,json,duel_anim_queue_move_card)
	}
}

if(ds_exists(map,ds_type_map)){
	ds_map_destroy(map);	
}