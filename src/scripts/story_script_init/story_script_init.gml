story_counter = 0; //where you are in the story

// use this list so that you can insert the story events and not have to constantly
// rearrange the array.
var list = ds_list_create();

// start game
ds_list_add(list,story_script_input_name)
ds_list_add(list,story_script_prologue);
ds_list_add(list,story_script_start);

// test text
//ds_list_add(list,story_script_text1)

// chapter 1 intro
ds_list_add(list,story_script_chapter1);

// chapter 1 start text
ds_list_add(list,story_script_text1)
ds_list_add(list,story_script_text2)
// chapter 1 meet thomas
ds_list_add(list,story_script_duel_thomas1);


ds_list_add(list,story_script_duel_liz1);
ds_list_add(list,story_script_duel_jason1);

// first duel with roger
/*
ds_list_add(list,story_script_text3)
ds_list_add(list,story_script_text4)
ds_list_add(list,story_script_text5)
ds_list_add(list,story_script_duel_roger1);
*/

var sizer = ds_list_size(list);

for(var i = 0; i < sizer; i++){
	story_script[i] = list[|i];	
}

story_size = sizer;

ds_list_destroy(list);