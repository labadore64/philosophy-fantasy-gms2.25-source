// particles

Sname = part_system_create();

var scale = 4;

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_pixel);
part_type_scale(particle1,scale,scale);
if(!global.enableShader){
	part_type_color1(particle1,$7F7F00);
} else {
	part_type_color1(particle1,16776960);	
}
part_type_alpha3(particle1,0,1,0);
part_type_life(particle1,60,60);
part_type_blend(particle1,true)

quantity = 2
part_type_alpha3(particle1,0,.5,0);
part_type_size(particle1,1,2,.01,0);
part_type_speed(particle1,.5,1,0,0)
part_type_direction(particle1,335,335,0,3)
part_type_gravity(particle1,.05,45)

emitter1 = part_emitter_create(Sname);
part_emitter_region(Sname,emitter1,
					global.display_x,
					global.window_width,
					global.display_y,
					global.window_height,
					ps_shape_rectangle,
					ps_distr_linear);

part_system_automatic_draw(Sname, false)
part_system_automatic_update(Sname,false)

part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
repeat(500){
	if(global.particles){part_system_update(Sname)}
}