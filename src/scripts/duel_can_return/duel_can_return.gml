// argument0 = selected card
// argument1 = target card

var card1 = true;
var card2 = true;

/*
if(argument[0] > -1){
	if(card_has_keyword(argument[0],"timid")){
		card1 = false;	
	}
}
*/

if(argument[1] > -1){
	if(card_has_keyword(argument[1],"determined")){
		card2 = false;	
	}
}

	
return duel_can_swap(argument[0],argument[1]) && card1 && card2;