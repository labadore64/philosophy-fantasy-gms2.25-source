

var obj = instance_create(0,0,animationBattle);

var attackingfield = duelController.field[duelController.active_player];
var defendingfield = duelController.field[!duelController.active_player];

obj.attacking_card = attackingfield.active_monster.card;
obj.defending_card = defendingfield.active_monster.card;

if(obj.attacking_card.card_id > -1){
	obj.attacking_card.card_sprite_ref = obj_data.card_sprite[obj.attacking_card.card_id]
}
if(obj.defending_card.card_id > -1){
	obj.defending_card.card_sprite_ref = obj_data.card_sprite[obj.defending_card.card_id]
}

obj.attacking_card.card_boost_attack = attackingfield.att_boost;
obj.defending_card.card_boost_defense = defendingfield.def_boost;

if(!instance_exists(obj.attacking_card)){

	obj.attacking_card = card_create(650,290,.5,attackingfield.active_monster.card_array[0])
	if(attackingfield.active_monster.card_array_size > 1){
		attackingfield.active_monster.card.card_overlay[0] = attackingfield.active_monster.card_array[1]
		if(attackingfield.active_monster.card_array_size > 2){
			attackingfield.active_monster.card.card_overlay[1] = attackingfield.active_monster.card_array[2]	
		}
	}
}
if(!instance_exists(obj.defending_card)){
	obj.defending_card = card_create(650,290,.5,defendingfield.active_monster.card_array[0])
	if(defendingfield.active_monster.card_array_size > 1){
		defendingfield.active_monster.card.card_overlay[0] = defendingfield.active_monster.card_array[1]
		if(defendingfield.active_monster.card_array_size > 2){
			defendingfield.active_monster.card.card_overlay[1] = defendingfield.active_monster.card_array[2]	
		}
	}
}
with(obj.attacking_card){
	card_update();	
}
with(obj.defending_card){
	card_update();	
}

with(obj.attacking_card){
	original_x = x
	original_y = y
	original_scale = card_scale
	x = 225
	y = 375
	if(duelController.active_player == DUEL_PLAYER_ENEMY){
		x = 575
	}
	card_scale = .6
	text_display_mode = CARD_DISPLAY_MODE_ATTACK
	surface_update = true
	card_reset_size();
}
with(obj.defending_card){
	original_x = x
	original_y = y
	original_scale = card_scale
	x = 575
	y = 375
	if(duelController.active_player == DUEL_PLAYER_ENEMY){
		x = 225
	}
	card_scale = .6
	text_display_mode = CARD_DISPLAY_MODE_DEFENSE
	surface_update = true
	card_reset_size();
}

obj.attacking_name = duelController.duel_hp[duelController.active_player].name;
obj.defending_name = duelController.duel_hp[!duelController.active_player].name;

obj.attacking_hp = duelController.duel_hp[duelController.active_player]
obj.defending_hp = duelController.duel_hp[!duelController.active_player]

// if the active has piercing, defense is 0
var status = status_card_check(code_set_cardstate(attackingfield.active_monster,0),"pierce");
if(status != ""){
	obj.defending_card.card_defense_power = 0;
	obj.defending_card.card_changed = true
	decrementStatusString(status)
}

// if the active has guard, attack is 0
status = status_card_check(code_set_cardstate(defendingfield.active_monster,0),"guard")
if(status != ""){
	obj.attacking_card.card_attack_power = 0;
	obj.attacking_card.card_changed = true
	decrementStatusString(status)
}

obj.total_damage = obj.attacking_card.card_attack_power - obj.defending_card.card_defense_power

// test the types
if(obj.attacking_card.card_id > -1 && obj.defending_card.card_id > -1 &&
	!card_has_keyword(obj.attacking_card.card_id,"neutral") && !card_has_keyword(obj.defending_card.card_id,"neutral")){

	var type1 = obj_data.card_element[obj.attacking_card.card_id]
	var type2 = obj_data.card_element[obj.defending_card.card_id]

	// if the attacking type is strong against the defending type
	if(obj_data.type_weak[type2] == type1){
		obj.total_damage += 1;	
		obj.super_effective = true
	}

	// if the defending type is strong against the attacking type
	if(obj_data.type_weak[type1] == type2){
		obj.total_damage -= 1;	
	}
}

obj.damage_abs = abs(obj.total_damage);

with(obj){
	duelController_display_battle_ax();	
}

return obj;