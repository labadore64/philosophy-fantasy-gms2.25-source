if(global.inputEnabled){
	if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SPEED]) ||
		global.gamepad_state_array[KEYBOARD_KEY_SPEED] == KEY_STATE_PRESS){	
			if(fast == 1.5){
				fast = 2;	
				global.fastmode = true;
			} else {
				fast = 1.5	
				global.fastmode = false
			}
		}
}

if(do_battle > -1){
	do_battle-= ScaleManager.timer_diff;
	if(do_battle < -1){
		do_battle = -1;
		duelController_display_battle();
	}
}
else if (do_coinflip > -1){
	do_coinflip-= ScaleManager.timer_diff;
	if(do_coinflip < -1){
		do_coinflip= -1;
		duelController_display_coinflip();
	}
} else if (list_show > -1){
	if(!instance_exists(list_show)){
		if(card_list > -1){
			var obj = instance_create(0,0,cardList);
			with(animationQueue){
				running_obj = obj;	
			}
			obj.list_reference = card_list;
			obj.extra_card_list = extra_card_list
			obj.title = card_list_name
			obj.ax_title = obj.title;
			obj.select_card = select_card
			if(select_card && !code_select_active){
				obj.menu_cancel = -1;	
			}
			obj.card_list_destroy = card_list_destroy;
			obj.card_list_source = card_list_source;

			with(obj){
				duelList_setDisplayArrays();
				if(list_reference > -1){
					selected_card.card_sprite_ref = obj_data.card_sprite[trunk_display[menupos]]
					with(selected_card){
						card_update()	
					}
				}
			}
			select_card = false;
			card_list_destroy = false;
		}
		list_show = noone;
	}
}else {
	if(running_obj < 0 && menuControl.active_menu == id){
		if(duelController.click_countdown <= -1){
			can_viewcard = true;
			event_inherited();
			can_viewcard = false;
		}
	}
	if(menuControl.active_menu == id){
		if(active_player == DUEL_PLAYER_YOU){
			if(global.inputEnabled){
				if(!keypress_this_frame){
					if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_OPTIONS]) ||
						global.gamepad_state_array[KEYBOARD_KEY_OPTIONS] == KEY_STATE_PRESS){	
							// if the options key is pressed 
							if(!instance_exists(OptionsMenu)){
								if(!instance_exists(fadeIn)){
									if(!instance_exists(fadeOut)){
										if(running_obj <= -1){
											running_obj = instance_create(0,0,OptionsMenu)
										}
									}
								}
							}
						}
						
					duelController_do_focus();	
				}
			}
		}
	
		if(running_obj > -1){
			if(!instance_exists(running_obj)){
				card_list = -1;
				running_obj = noone
				fade = false;
			}
		}
	}
}



	while(!instance_exists(animation_stack_run) && animation_stack_run != noone){
		if(ds_exists(animation_stack,ds_type_stack)){
			if(!ds_stack_empty(animation_stack)){
				animation_stack_run = ds_stack_pop(animation_stack);	
				//ds_stack_pop(animation_stack);
				animation_stack_count = ds_stack_size(animation_stack)	
		
				if(!ds_stack_empty(animation_stack)){
					var me = ds_stack_top(animation_stack);
					running_obj = me;
					animation_stack_run = me
			
				}
			} else {
				// to do: move this to when the queue is fully destroyed and control
				// is regained
				tooltip_show = true;
				if(!duelController.regain_control){
					duelController.resume_control = true;
					duelController.resume_control_counter = DUEL_RESUME_CONTROL_TIME
				}
				//duelController_resume_control()
				duelController.resumeAnim = false
				animation_stack_run = noone
			}
		}
	}

	with(animation_stack_run){
		animationQueue_update()	
	}

if(phase_transition < 0){
	if(animation_stack_run < 0){
		if(ds_list_empty(input_queue) && !instance_exists(animationQueue) && !instance_exists(animationPhaseTransition)){
			
			if(active_player == DUEL_PLAYER_ENEMY){
				// uncomment this when you are ready to work with AI again
				/*
				// buffer for the end phase so the enemy can discard cards
				if(do_enemy_end){
					do_enemy_end_timer -= ScaleManager.timer_diff	
					if(do_enemy_end_timer < -1){
						do_enemy_end = false;
						do_enemy_end_timer = -1;
						if(field[active_player].part[DUEL_FIELD_HAND].card_array_size <= field[active_player].part[DUEL_FIELD_HAND].hand_size_limit){
							phase_current--;
							duel_animation_wait(15)
							duel_animation_end_turn()
							duel_animation_wait(15)
						}
					}
				} else {
					
					// this is the normal AI execution
					var val = newAI_start(DUEL_PLAYER_ENEMY);	
					// the resume point must be 0, meaning the AI is completed 
					// its batch cycle
					if(duelController.ai_resume_point == 0){
						show_debug_message("chosen command: " + val);
						// test if this value is a valid choice right now. If it is, do it!
						if(val == ""){
							duel_animation_run_script(duel_next_phase_)
						} else if (val == AI_END_PHASE){
							duel_animation_run_script(duel_next_phase_)
						} else if (val == AI_END_TURN){
							duel_animation_run_script(duel_end_turn_)	
						} else {
							if(newAI_test_action(val)){
								duelinput_insertnumber(val);
						
								if(phase_current != DUEL_PHASE_BATTLE){
									duel_animation_run_script(duelCamera_center_camera_special);	
								}
							} else {
								duel_animation_run_script(duel_next_phase_)
							}
						}
					
				
						if(phase_current >= DUEL_PHASE_END){
							do_enemy_end = true;
							do_enemy_end_timer = 2;
						}
					}
				}
				*/
			} else {
				// this is so you can't just pitch deleuze in the end phase
				// it will wait for animations to play THEN it will test.
				if (do_end){
					do_end_timer -= ScaleManager.timer_diff;
					if(do_end_timer < -1){
						do_end = false;
						
						if(field[active_player].part[DUEL_FIELD_HAND].card_array_size <= 5){
							duel_animation_center_camera(field[active_player],"");
							//duel_animation_next_phase();
							duel_animation_wait(15)
							duel_animation_end_turn()
							duel_animation_wait(15)
						}
					}
				}	
			}
		}
	}
}


{

	is_rotating = false
	
	if(test_victory && !complete){
		test_victory = false
		// test victory
		if((duelController.duel_hp[1].hp_current <= 0)
			|| (duelController.duel_hp[0].hp_current <= 0)){
			duelController.running_obj = noone
			with(animationQueue){
				instance_destroy();	
			}
			with(animationPhaseTransition){
				instance_destroy();	
			}
			complete = true;
			soundPlaySong(-1,0)
			spr_unload_card();
			duel_animation_victory();
			disable_mouse_mode = true
			mouse_mode = false;
		}
	}


	if(room_timer > -1){

		room_timer-= ScaleManager.timer_diff;
		if(room_timer <= -1){
			if(global.dont_record){
				room_goto(MainRoom)	
			} else {
				duelController_generateEndText()
			}
		}
	}

	if(click_countdown > -1){
		click_countdown-= ScaleManager.timer_diff;
		if(click_countdown < -1){
			click_countdown =-1;	
		}
	}

	key_name[5] = "Menu" 

	// mouse mode

	init_mouse_counter -= ScaleManager.timer_diff;
	if(init_mouse_counter < -1){
		if(!mouse_counter_triggered){
			mouse_counter_triggered = true;
			can_mouse_counter = true;
		}
	}

	if(can_mouse_counter){
		if(MouseHandler.mousepos_x != MouseHandler.mouse_xprevious || MouseHandler.mousepos_y != MouseHandler.mouse_yprevious){
			if(!mouse_mode){
				surface_update=true	
			}
			mouse_mode = true
		}
	}

	if(duelController.phase_transition != noone){
		if(!instance_exists(duelController.phase_transition)){
			duelController.phase_transition = noone;	
			with(animationQueue){
				with(running_obj){
					instance_destroy();	
				}
			}
		}
	}

	if(global.mouse_active && active_player == DUEL_PLAYER_YOU){
		if((running_obj < 0 || code_select_active)  && menuControl.active_menu == id){
			if(duelController.click_countdown <= -1){
				
		var contextd = true;
		if(mouse_check_button_pressed(mb_left)){
			if(context_hover){
				with(context_menu){
					if(!keypress_this_frame){
						if(menu_select != -1){
							duelController.context_menu_obj.menupos = duelController.context_menu_select;
							//contextd = true
							script_execute(menu_select)	
						}
					}	
				}
			} else {
			
				if(instance_exists(duelSub)){
					with(duelSub){
						if(context_menu){
							duelController.do_the_context_menu = true	
						}
					}
				}
			}
			
			with(duelSub){
				instance_destroy();	
			}
			with(menuTooltip){
				instance_destroy()	
			}
		}
		
		var cantdo = (rectangle_in_rectangle(cursor.test_area[0],cursor.test_area[1],
														cursor.test_area[2],cursor.test_area[3],
														nav_pos[0],nav_pos[1],
														nav_pos[2],nav_pos[3]) == 0);
				
		if(context_menu < 0 && !pop_menu_active){
			mouse_card_selected_last = mouse_card_selected
			mouse_card_selected = -1
			
			for(var i = 0; i < mouse_card_count; i++){

				if(rectangle_in_rectangle(cursor.test_area[0],cursor.test_area[1],
									cursor.test_area[2],cursor.test_area[3],
									mouse_card[i,0],mouse_card[i,1],
									mouse_card[i,2],mouse_card[i,3]) > 0){

						mouse_card_selected = i					
						break;

				}
			}

				
			if(mouse_card_selected > -1 && cantdo){
				if(duelController_is_not_panning()){
					duelCursor_mouse_selected()
					do_the_context_menu = false;
					can_mouse_counter = false
				}
			} else if(mouse_card_selected == -1){
				with(tooltip[0]){
					instance_destroy();	
				}
			
				duelController.viewcard_id = -1;
				if(
					mouse_card_selected != mouse_card_selected_last){

					with(selected_object){
						selected_object = noone
					}
					selected_object = noone
				} else {
					panning = !panning	
				}
			}
		}

		if(screenshot_delay > -1){
			screenshot_delay-= ScaleManager.timer_diff
			if(screenshot_delay < -1){
				screenshot_delay = -1;	
			}
		}

		if(pan_up){
			duelCamera_pan_up()
		}

		if(pan_down){
			duelCamera_pan_down()
		}

		if(pan_left){
			duelCamera_pan_left()
		}					

		if(pan_right){
			duelCamera_pan_right()
		}

		if(mouse_check_button(mb_left)){
			pan_hold_counter++	
		}

		if(mouse_check_button_released(mb_left) && pan_hold_counter > pan_hold_frames){
			pan_up = false;
			pan_down = false;
			pan_left = false;
			pan_right = false;
		}
	}
	}
	}
}