
with(argument[0]){
	draw_set_halign(fa_left)
	draw_set_alpha(1)
	if(global.high_contrast){

		draw_set_color(c_white)
		draw_rectangle_ratio(x-3,y-3,x+menu_width+3,y+menu_height+3,false)	
		draw_set_color(c_black)
	} else {
		draw_set_color(global.menu_bgcolor)
		draw_set_alpha(.85)
		draw_rectangle_ratio(x-3,y-3,x+menu_width+3,y+menu_height+3,false)	
		draw_set_alpha(1)
	}
		
	draw_rectangle_ratio(x,y,x+menu_width,y+menu_height,false)	
		
	draw_set_alpha(1)
		
	gpu_set_colorwriteenable(true,true,true,false)
					
	draw_set_color(c_white)
	draw_set_alpha(alpha_max)


	if(global.high_contrast){
		draw_rectangle_ratio(x+option_x_offset-5,-5+y+option_y_offset+option_spacer*(menupos),
				x+menu_width-option_x_offset+5,-5+y+option_y_offset+option_spacer*(menupos+1)-3,
					false)
	} else {
		draw_rectangle_color_ratio(x+option_x_offset-5,-5+y+option_y_offset+option_spacer*(menupos),
				x+menu_width-option_x_offset+5,-5+y+option_y_offset+option_spacer*(menupos+1)-3,
		global.menu_selcolor[1],global.menu_selcolor[1],
		global.menu_selcolor[0],global.menu_selcolor[0],
		false)
	}
	
	if(menu_sprite[menupos] > -1){
		var xx = x-75
		var yy = y+4;
		var width = 60
		draw_set_alpha(1)
		draw_set_color(c_white)
		if(global.high_contrast){
			draw_rectangle_ratio(xx-5,yy-5,xx+width+5,yy+width+5,false)
		} else {
	
			if(global.color_gradient){
				draw_rectangle_color_ratio(xx-7,yy-7,xx+width+7,yy+width+7,
				global.menu_grcolor[0],global.menu_grcolor[0],
				global.menu_grcolor[1],global.menu_grcolor[1],
				false)
		
			} else {
				draw_set_color(global.menu_grcolor[0])
				draw_rectangle_ratio(xx-7,yy-7,xx+width+7,yy+width+7,
				false)
			}
		}


		if(global.high_contrast){
			draw_set_color(c_black)
		} else {
			draw_set_color(global.menu_bgcolor)
		}
		draw_set_alpha(1)
		draw_rectangle_ratio(xx,yy,xx+width,yy+width,false)
		
		draw_sprite_extended_ratio(menu_sprite[menupos],0,xx+30,yy+30,.4,.4,0,c_white,1)
	}
	
	
	draw_set_alpha(1)
	gpu_set_colorwriteenable(true,true,true,true)	
					
	draw_set_color(global.text_fgcolor)
	
	for(var i = 0; i < menu_count; i++){
		draw_text_transformed_ratio(x+option_x_offset,-5+y+option_y_offset+option_spacer*i,menu_name[i],3,3,0)	
	}
	
}