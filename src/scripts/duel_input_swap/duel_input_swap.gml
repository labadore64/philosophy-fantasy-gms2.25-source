// should be in all scripts
var index = argument[1]
var origin = argument[0]

with(origin){
	var mon = card_array[index]
	var card = parent.part[DUEL_FIELD_ACTIVE].card_array[0]
	if(mon > -1){
		tts_say( obj_data.card_name[card] + " switched with " + obj_data.card_name[mon]);	
	}

	
	duel_anim_switch_benched(id,parent.part[DUEL_FIELD_ACTIVE],card_array[index],index)
	
	var this = code_set_cardstate(parent.part[DUEL_FIELD_ACTIVE],parent.part[DUEL_FIELD_ACTIVE].card_array_size)
	duel_animation_card_effect(card,"switch",this);
	
	if(card_has_keyword(mon,"recoil")){
		repeat(5){
			var card = parent.part[DUEL_FIELD_BENCH].card_array[0];
			duel_animation_move_card(0,parent.part[DUEL_FIELD_BENCH],parent.wait);
		}
		repeat(5){
			var card = parent.part[DUEL_FIELD_THEORY].card_array[0];
			duel_animation_move_card(0,parent.part[DUEL_FIELD_THEORY],parent.wait);
		}
	}
	
	if(duelController.phase_current == DUEL_PHASE_RETURN){
		// do the next thingie	
		// actually create the confirm box
		duel_animation_return_menu()
	}
		duelController.speak_tts = false;
		//////duel_animation_resume_control()		
}

return noone