

	var filename = working_directory + "resources\\story\\";
	var ext = ".json";
	var text = "";

	var file = file_text_open_read(filename + argument[0] + ext)

	while(!file_text_eof(file)){
		text+= text_file_readln_correct(file)	
	}

	file_text_close(file);

	var map = json_decode(text);

	// if the json is valid load the map
	if(ds_exists(map,ds_type_map)){
		var val = map[? "type"]
	
		// tests which type of story element is to be loaded
		if(!is_undefined(val)){
			show_debug_message(argument[0])
			global.story_last[1] = global.story_last[0];
			global.story_last[0] = argument[0];
			var script = asset_get_index("story_do_" + val);
		
				if(script > -1){
					script_execute(script,map,argument[0]);	
				}
		
				// if activity points are deducted, do it at this time
		
				if( global.activity_points < 0){
					global.activity_points = 0;	
				}
		
				// set the event

				eventSet(argument[0]);


		}
	
		ds_map_destroy(map);
	} else {
		room_goto(MainRoom)	
	}
