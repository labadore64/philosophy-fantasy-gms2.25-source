if(!instance_exists(duelController.animation_stack_run) || duelController.animation_stack_run.object_index == animationQueue){
	var card_id = argument[0];
	var card_source_loc = argument[1];
	var goto_loc = argument[2];
	var time = argument[3]
	var facedown = argument[4]
	var movecamera = argument[5]
	// get start position
	
	var field_x = 200;
	var field_y = 200

	var start_x = 0
	var end_x = 0
	var spacer = 0
	var posy = 0
	var yspacer = 0;
	var menuposs = 0;
	
	// activate card effects when a card is moved somewhere.
	
	with(card_source_loc){
		
		if(enemy){
			var field_x = 204;
			var field_y = 199
		}
		
		if(object_index == duelHand){
			if(!enemy){
				var start_x = -60;
				var end_x = 420;
				var spacer = (end_x-start_x)/5;
				var posy = 340+240
			} else {
				var start_x = 420-105;
				var end_x = -60-105
				var spacer = (end_x-start_x)/5;
				var posy = -330
			}
			
			if(card_array_size > 5){
				spacer = (end_x-start_x)/(card_array_size+1);
			}
			
			menuposs = card_id;
			if(menuposs > card_array_size-1){
				menuposs = card_array_size-1;	
			}
			
		} else if(object_index == duelDeck){
			if(!enemy){
				var start_x = 310
				var posy = 450
			} else {
				var start_x = -55
				var posy = -200
			}
			
		} else if(object_index == duelWaiting){
			if(!enemy){
				var start_x = 310
				var posy = 340
			} else {
				var start_x = -55
				var posy = -90
			}
			
		} else if(object_index == duelTheory){
			if(!enemy){
				var start_x = 310-370
				var posy = 340
			} else {
				var start_x = -55+370
				var posy = -90
			}
			
		} else if (object_index == duelActive){
			if(!enemy){
				var start_x = 130
				var posy = 230
	
				var yspacer = 20;
			} else {
				var start_x = 0
				var posy = 200
	
				var yspacer = 20;
			}
			menuposs = card_id;
			if(menuposs > card_array_size-1){
				menuposs = card_array_size-1;	
			}
			
		} else if (object_index == duelBench){
			
			if(enemy){
				var start_x = 205
	
				var posy = -90
	
				var spacer = -75;
			} else {
				var start_x = 50
				var posy = 340
	
				var spacer = 75;
				
			}
			
			if(card_array_size == 0){
				startx = 150
			}
			
			menuposs = card_id;
			if(menuposs > card_array_size-1){
				menuposs = card_array_size-1;	
			}
			
		}
	
	}
	
	var start_xx = field_x+start_x+menuposs*spacer
	var start_yy = field_y+posy+menuposs*yspacer
	
	
	// end get start position
	
	// start get end position
	var field_x = 200;
	var field_y = 200

	var start_x = 0
	var end_x = 0
	var spacer = 0
	var posy = 0
	var yspacer = 0;
	var menuposs = 0;
	with(goto_loc){
		var le_card = -1
		if(card_id > -1 && card_id < card_source_loc.card_array_size){
			le_card = card_source_loc.card_array[card_id]
		}
		
		if(enemy){
			var field_x = 204;
			var field_y = 199
		}
		
		if(object_index == duelHand){
			if(!enemy){
				var start_x = -60;
				var end_x = 420;
				var spacer = (end_x-start_x)/5;
				var posy = 340+240
			} else {
				var start_x = -60+350+10;
				var end_x = 420+350+10;
				var spacer = (end_x-start_x)/5;
				var posy = -330
			}
			
			if(card_array_size > 5){
				spacer = (end_x-start_x)/(card_array_size+1);
			}
			
			menuposs = card_array_size;
			
		} else if(object_index == duelDeck){
			if(!enemy){
				var start_x = 310
				var posy = 450
			} else {
				var start_x = -55
				var posy = -200
			}
			
		} else if(object_index == duelWaiting){
			if(!enemy){
				var start_x = 310
				var posy = 340
			} else {
				var start_x = -55
				var posy = -90
			}
			
		} else if(object_index == duelTheory){
			if(!enemy){
				var start_x = 310-370
				var posy = 340
			} else {
				var start_x = -55+370
				var posy = -90
			}
			
		} else if (object_index == duelActive){
			if(!enemy){
				var start_x = 130
				var posy = 210
	
				var yspacer = -20;
			} else {
				var start_x = 130
				var posy = 0
	
				var yspacer = 20;
			}
			menuposs = card_array_size-1;
			
		} else if (object_index == duelBench){
			
			if(enemy){
				var start_x = 205+75
	
				var posy = -90
	
				var spacer = -75;
			} else {
				var start_x = 125
				var posy = 340
	
				var spacer = 75;
				
			}
			
			if(card_array_size == 0){
				startx = 150
			}
			
			menuposs = card_array_size-1;
		}
	
	}
	
	if(goto_loc.enemy){
		var dest_x = field_x+start_x-menuposs*spacer
		var dest_y = field_y+posy-menuposs*yspacer	
	} else {
		var dest_x = field_x+start_x+menuposs*spacer
		var dest_y = field_y+posy+menuposs*yspacer
	}
	// end get end position

	if(card_id > -1 && card_id < card_source_loc.card_array_size){
	var le_card = card_source_loc.card_array[card_id]

	if(le_card > -1){

		// if its AI just calculate the move
		if(duelController.ai_mode){
			if(card_source_loc.card_array_size > 0){
				for(var i = 0; i < goto_loc.card_array_size; i++){
					if(goto_loc.card_array[i] == -1){
						break;
					}
				}
			
				goto_loc.card_array[i] = card_source_loc.card_array[card_id];

				card_source_loc.card_array[card_id] = -1;
				
				goto_loc.card_array_size++;
				card_source_loc.card_array_size--;
				
			}
			
		} else {
			// otherwise do this shit
			var obj = instance_create(start_xx,start_yy,drawMoveCard)

			obj.card_place_number = card_id
			obj.card_place_id = le_card;
			obj.card_place_source = card_source_loc
			obj.card_place_location = goto_loc;
			obj.card_place_facedown = facedown
			obj.card_index = 5;	
			if(!facedown){
				if(obj_data.card_type[le_card] == CARD_TYPE_PHILOSOPHER){
					obj.card_index = obj_data.card_element[le_card]
				} else if (obj_data.card_type[le_card] == CARD_TYPE_METAPHYS){
					obj.card_index = 4
				}else if (obj_data.card_type[le_card] == CARD_TYPE_EFFECT){
					obj.card_index = 3
				} else {
					obj.card_index = 5;	
				}
			}
			obj.goto_x = dest_x;
			obj.goto_y = dest_y;
			obj.move_x = (start_xx-dest_x)/(time+1)
			obj.move_y = (start_yy-dest_y)/(time+1)
			obj.timer = time;
			obj.move_camera = movecamera
		
			// since you know which card is moving, check to see if it has a status effect.
			// if it does, remove it.
			var target_string = code_set_cardstate(card_source_loc,card_id);
		
			with(duelController){
				var size = ds_map_size(card_status)
				var key = ds_map_find_first(card_status)
				for (var i = 0; i < size; i++)
				{
					if(!is_undefined(key)){
						var value = card_status[? key]

						if(string_pos(target_string,key) > 0){
							ds_map_delete(card_status,key);	
						}

						key = ds_map_find_next(card_status, key);
					}
				}
			}

			duelController.card_rumbleset_x =dest_x
			duelController.card_rumbleset_y =dest_y;

			RUMBLE_CARD_SELECT

			if(movecamera){
				duelCamera.x = start_xx;
				duelCamera.y = start_yy;
				duel_camera_focus_object(dest_x,dest_y,time,duelCamera.scale);
			}

			if(!instance_exists(duelController.running_obj)  || duelController.running_obj.object_index != animationQueue){
				duelController.running_obj = obj;
			}
	
			card_source_loc.card_array[card_id] = -1

			with(duelField){
				surface_update = true;	
			}
		
	
			return obj;
		}
	}
	}
}

return noone;