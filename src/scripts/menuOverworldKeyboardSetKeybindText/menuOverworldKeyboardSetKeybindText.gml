var list = argument[0];

ds_list_clear(list);
if(object_index == menuOverworldKeyboardOption){
	ds_list_add(list," ");
	ds_list_add(list,keyboardToString(keyboard_bind_get("select")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("select2")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("cancel")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("up")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("down")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("left")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("right")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("lefttab")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("righttab")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("sort")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_card")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("speed")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("cam_up")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("cam_down")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("cam_left")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("cam_right")));
	
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_hand")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_deck")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_active")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_overlay")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_bench")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_theory")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_limbo")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("view_switch")));
	
	ds_list_add(list,keyboardToString(keyboard_bind_get("help")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("options")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("screenshot")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("keydisplay")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("exit")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("tts_repeat")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("tts_stop")));
	for(var i = 0; i < 10; i++){
		ds_list_add(list,keyboardToString(keyboard_bind_get("access"+string(i))));
	}
	var sizer = ds_list_size(list);
	for(var i = 0; i < sizer; i++){
		display_key[i] = list[|i]	
	}
} else {
	ds_list_add(list," ");
	ds_list_add(list,gamepadToString(gamepad_bind_get("select")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("select2")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("cancel")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("up")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("down")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("left")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("right")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("lefttab")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("righttab")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("sort")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("view_card")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("speed")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("help")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("options")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("screenshot")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("keydisplay")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("exit")));	
	var sizer = ds_list_size(list);
	for(var i = 0; i < sizer; i++){
		display_key[i] = list[|i]	
	}
}