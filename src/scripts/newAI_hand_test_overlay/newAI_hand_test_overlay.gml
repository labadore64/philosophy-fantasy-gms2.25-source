// argument0 = selected card
// argument1 = target card

var card1 = true;
var card2 = true;

var field = argument[2];

if(argument[0] > -1){
	if(card_has_keyword(argument[0],"unfriendly")){
		card1 = false;	
	}
}

if(argument[1] > -1){
	if(card_has_keyword(argument[1],"solo")){
		card2 = false;	
	}
}

// if stunned, you can't overlay cards.
if(duel_status_active(field.parent,"stun")){
	return false;
}
	
return card1 && card2;