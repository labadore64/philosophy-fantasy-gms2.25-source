var input = argument[0];

var pos = string_pos(":",input);
var script = asset_get_index("duel_input_" + string_copy(input,1,pos-1));

input = string_delete(input,1,pos)
pos = string_pos(":",input);

var ori = real(string_copy(input,1,pos-1));

input = string_delete(input,1,pos)
pos = string_pos(":",input);

var side = real(string_copy(input,1,pos-1));

input = string_delete(input,1,pos)
pos = string_pos(":",input);

var ind = real(input);

// now that you have the stuff get the values

// now extract the target
var target_origin = noone;
			
// if the code is a location, parse as location.
		
target_origin = duelController.field[side].ai_field.part[ori]

// now execute the script

input_obj = script_execute(script,target_origin,ind);