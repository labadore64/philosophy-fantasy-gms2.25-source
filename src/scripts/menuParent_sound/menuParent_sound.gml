var sound = -1;
if(global.menu_sounds){sound =soundfxPlay(sfx_sound_menu_default);}

if(global.positional_pitch){
	if(sound > -1){
		if(object_is_ancestor(object_index,duelCardSource)){
			if(size_limit > 1){
				var diff = POSITION_PITCH_MAX - POSITION_PITCH_MIN;
				var maxsize = max(card_array_size,1)-1;
				audio_sound_pitch(sound,POSITION_PITCH_MIN + diff*((maxsize-max(menupos,0)) / (maxsize+1)));
			} else {
				var diff = POSITION_PITCH_MAX - POSITION_PITCH_MIN;
				var valuez = 1;
				audio_sound_pitch(sound,POSITION_PITCH_MIN + diff*(valuez));
			}
		} else {
			var maxsize = max(menu_count,1)-1;
			var diff = POSITION_PITCH_MAX - POSITION_PITCH_MIN;
			audio_sound_pitch(sound,POSITION_PITCH_MIN + diff*((maxsize-max(menupos,0)) / (maxsize+1)));	
		}
	}
}