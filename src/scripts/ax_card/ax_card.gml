if(argument[0] > -1){
	for(var i = 0; i < 10; i++){
		access_string[i] = "";	
	}
	
	access_string[0] = obj_data.card_name[argument[0]];
	
	access_string[1] = string(obj_data.card_attack[argument[0]]) + " attack "
	access_string[2] = string(obj_data.card_attack_overlay[argument[0]]) + " overlay attack"
	access_string[3] = string(obj_data.card_defense[argument[0]]) + " defense "
	access_string[4] = string(obj_data.card_defense_overlay[argument[0]]) + " overlay defense"

	stringer = "mind"
	var stringstrong = "matter"
	var stringweak = "spirit"
	if(obj_data.card_element[argument[0]] == CARD_ELEMENT_MATTER){
		stringer = "matter"	
		stringstrong = "spirit"
		stringweak = "mind"
	} else if(obj_data.card_element[argument[0]] == CARD_ELEMENT_SPIRIT){
		stringer = "spirit"
		stringstrong = "mind"
		stringweak = "matter"
	}
	
	access_string[5] = stringer + " Type"
}