var obj = instance_create(0,0,afterBattleWindow);

// set text
var windowCharPerLine = 22

obj.message = textForceCharsPerLine(argument[0],windowCharPerLine);
obj.height = string_height(obj.message)*2 + 40;
tts_say(obj.message)

with(obj){
	mouseHandler_clear()
	mouseHandler_add(0,0,800,600,menu_cancel,message)	
}

return obj;