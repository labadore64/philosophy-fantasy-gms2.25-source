// argument0 field
// argument1 card

var field = argument[0]
var card = argument[1]

if(instance_exists(field)){
	if(card > -1){
			
		// if card has return, return this card to the deck
		if(card_has_keyword(argument[1],"return")){
			
			duel_battle_showcard(card);
			
			repeat(field.parent.part[DUEL_FIELD_BENCH].card_array_size-1){
				duel_animation_move_card(1,field,field.parent.wait);
			}
			duel_animation_move_card(0,field,field.parent.part[DUEL_FIELD_DECK]);
			duel_animation_shuffle(field.parent.part[DUEL_FIELD_DECK])
		}
	}
}