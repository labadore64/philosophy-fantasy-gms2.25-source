var xx = argument[0]*global.scale_factor;
var yy = argument[1]*global.scale_factor;
var textval = argument[2];
var scalex = argument[3]*global.scale_factor;
var scaley = argument[4]*global.scale_factor;

var angle = argument[5];

if(scalex > 1 || scaley > 1){
	draw_text_transformed(global.display_x + xx,global.display_y+yy,textval,scalex,scaley,angle)
} else {
	draw_text(global.display_x + xx,global.display_y+yy,textval)
}