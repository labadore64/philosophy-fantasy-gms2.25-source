	#macro BATTLE_DEFAULT_YOU_STATE "active:you:"
	#macro BATTLE_DEFAULT_THEM_STATE "active:enemy:"
	

		
	if(argument_count == 0){
		var you = duelController.field[duelController.active_player].part[DUEL_FIELD_ACTIVE]
		var them = duelController.field[!duelController.active_player].part[DUEL_FIELD_ACTIVE]
	} else {
		var you = argument[0].part[DUEL_FIELD_ACTIVE]
		var them = argument[1].part[DUEL_FIELD_ACTIVE]
	}
		
		
	// do some optimization
		
	if(you.card_array[0] > -1){
		if(obj_data.card_code_count[you.card_array[0]] > 0){
			duel_animation_card_effect(you.card_array[0],"dialog_end",BATTLE_DEFAULT_YOU_STATE + "0");
			if(you.card_array_size > 1){
				duel_animation_card_effect(you.card_array[1],"dialog_end_overlay",BATTLE_DEFAULT_YOU_STATE + "1");
			}
			if(you.card_array_size > 2){
				duel_animation_card_effect(you.card_array[2],"dialog_end_overlay",BATTLE_DEFAULT_YOU_STATE + "2");
			}

			duel_animation_card_effect(you.card_array[0],"dialog_end_attack",BATTLE_DEFAULT_YOU_STATE + "0");

			if(you.card_array_size > 1){
				duel_animation_card_effect(you.card_array[1],"dialog_end_overlay_attack",BATTLE_DEFAULT_YOU_STATE + "1");
			}
			if(you.card_array_size > 2){
				duel_animation_card_effect(you.card_array[2],"dialog_end_overlay_attack",BATTLE_DEFAULT_YOU_STATE + "2");
			}
		}
	}
		
	if(them.card_array[0] > -1){
		if(obj_data.card_code_count[them.card_array[0]] > 0){
			duel_animation_card_effect(them.card_array[0],"dialog_end",BATTLE_DEFAULT_THEM_STATE + "0");
			if(them.card_array_size > 1){
				duel_animation_card_effect(them.card_array[1],"dialog_end_overlay",BATTLE_DEFAULT_THEM_STATE + "1");
			}
			if(them.card_array_size > 2){
				duel_animation_card_effect(them.card_array[2],"dialog_end_overlay",BATTLE_DEFAULT_THEM_STATE + "2");
			}
			duel_animation_card_effect(them.card_array[0],"dialog_end_defend",BATTLE_DEFAULT_THEM_STATE + "0");
			if(them.card_array_size > 1){
				duel_animation_card_effect(them.card_array[1],"dialog_end_overlay_defend",BATTLE_DEFAULT_THEM_STATE + "1");
			}
			if(them.card_array_size > 2){
				duel_animation_card_effect(them.card_array[2],"dialog_end_overlay_defend",BATTLE_DEFAULT_THEM_STATE + "2");
			}
		}
	}
	