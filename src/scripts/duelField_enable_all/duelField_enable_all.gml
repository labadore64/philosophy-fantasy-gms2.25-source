with(duelField){
	var sizer = array_length_1d(part);
	
	for(var i = 0; i < sizer; i++){
		with(part[i]){
			menu_left_enabled = true;
			menu_right_enabled = true;
			menu_up_enabled = true;
			menu_down_enabled = true;
			menu_cancel_enabled = true;
			menu_select_enabled = true;	
		}
	}
}