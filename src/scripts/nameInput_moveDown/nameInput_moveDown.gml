if(selected_part == NAMEINPUT_STATE_NAME){
	// do name modification stuff ( usually used for changing the pointer)
	menupos = alpha_menupos;
	menuParent_sound()
	
} else if (selected_part == NAMEINPUT_STATE_CONTROLS){
	// do control stuff (confirm, backspace, clear ect)
	if(menupos+2 < menu_count){
		menupos+=2;
	}
	menuParent_sound()
} else {
	if(selected_part == NAMEINPUT_STATE_ALPHABET){
		alpha_menupos = menupos	
	}
	
	// do alpha stuff
	menupos+=columns;

	if(menupos >= chara_length+1){
		menupos = chara_length+1
	}

	menuParent_sound()

	RUMBLE_MOVE_MENU


	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}

	with(MouseHandler){
		ignore_menu = true	
	}

	surface_update = true
	
}
nameInput_updateMove()