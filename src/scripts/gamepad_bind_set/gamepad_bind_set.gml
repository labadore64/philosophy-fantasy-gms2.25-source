/// @function gamepad_bind_set(id, gp_button)
/// @description Adds an id-gamepad button pair to the gamepad keybinding definition.
/// @param {string} id The reference ID.
/// @param {real} gp_button The gamepad button.

if(!is_undefined(global.gamepad_map[? argument[0]])){
	ds_map_replace(global.gamepad_map,argument[0],argument[1]);
	for(var i = 0; i < global.gamepad_size; i++){
		if(global.gamepad_array[i] == argument[0]){
			global.gamepad_value_array[i] = argument[1];	
		}
	}
} else {
	global.gamepad_array[global.gamepad_size] = argument[0];
	global.gamepad_value_array[global.gamepad_size] = argument[1];
	ds_map_add(global.gamepad_map,argument[0],argument[1]);
	global.gamepad_size = ds_map_size(global.gamepad_map);
}