var obj;
var menup = option_top

var spacer = 40;

var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

for(var i = 0; i < diagram_length; i++){
	obj = diagrams[|i];
	if(!is_undefined(obj)){
		with(obj){
			var space = y+(min_index-menup)*spacer;
			if(space >= 80 && space <= 540){
				if(draw){
					if(sprite_index > -1){
						draw_sprite_extended_noratio(sprite_index,image_index,x,space,image_xscale,image_yscale,image_angle,col,1)
					}
				}
			}
		}
	}
}