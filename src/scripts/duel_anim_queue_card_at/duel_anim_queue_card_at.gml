var argu = argument[0]; // args as json string

var map = json_decode(argu);

var target_index = -1; // the card index to move
var target_origin = -1; // the location of the target
var target_destination = -1; // where to move the target
var target_side = duelController.active_player; // which side the target is on
var test_value = "";

var this = code_get_this();
var this_enemy = false;
if(string_pos("pos_enemy",this) > 0){
	this_enemy = true;
}

var return_value = 0;

var stringer = "";

if(!ds_map_empty(map)){
	// do your normal code down here
	var _src = map[? "source"];
	var return_variable = map[? "return"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_src) &&
		!is_undefined(return_variable)){
			
			var targe = _src
								
			var targe_variable = duelController.code_values[? targe];
			if(!is_undefined(targe_variable)){
				targe = targe_variable	
			}						
			// parse target
			// if target is this just use the character stored in this
			var is_this = false;
			if(targe == "this"){
				targe = this
				is_this = true;
			}

			var stringe = targe;
			var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			var side = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			
			//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		
			if(is_this){
				if(this_enemy){
					target_side = DUEL_PLAYER_ENEMY	
				} else {
					target_side = DUEL_PLAYER_YOU	
				}
			} else {
				if(side == "enemy"){
					if(!this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}
				} else {
					if(this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}	
				}
			}
			// if the code is a location, parse as location.
			if(code_is_location(ori)){
				target_index = string_copy(stringe,string_pos(":",stringe)+1,
											string_length(stringe) - string_pos(":",stringe))
			
				// string == variable
				if(string_length(string_digits(target_index)) != string_length(target_index)){
					var value = duelController.code_values[? target_index];
					if(is_undefined(value) || !is_real(value)){
						value = 0;	
					}
				
					target_index = value;
				} else {
					target_index = real(target_index);	
				}
				
				if(ori == "overlay"){
					target_index++;	
				}
				
				target_origin = code_get_location(ori,target_side)
			} else {
				// otherwise, track the search terms of the card.
				// note: you're going to need to update how you track this location
				// after doing this... damb.
				
				// anyways target_index is going to contain a list of targets
				// listed in brackets :) like so:
				// cardMove("target:[type:mind+name:Deleuze+archetype:psychoanalysis]","dest:hand");
			}
			
			// card id at target
			if(target_index > -1 &&
				target_origin > -1 &&
				target_index < target_origin.card_array_size){
				return_value = target_origin.card_array[target_index];
	
			}

			// set the value here

			ds_map_replace(duelController.code_values,return_variable,return_value)
		}
	}


return noone;