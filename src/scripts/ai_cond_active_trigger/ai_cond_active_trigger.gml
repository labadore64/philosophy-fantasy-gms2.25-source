var me = argument[0];

var pos = me.menupos;

if(argument_count > 1){
	pos = argument[1];	
}

var ai_val = -1

if(argument_count > 2){
	ai_val = argument[2]
}


var menu_selected = me.card_array[pos]

if(ai_val > -1){
	menu_selected = me.ai_card_array[ai_val,pos]
}

if(!duelController.code_select_active){
	if(duelController.phase_current == DUEL_PHASE_MAIN){
		if(menu_selected > -1){
			// trigger effect for active card
			if(code_calculate_condition(menu_selected,"trigger",code_set_cardstate(me,me.menupos))){
				return true;
			}
		}
	}
}

return false;