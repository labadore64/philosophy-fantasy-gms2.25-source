
if(menu_count > 0){

	var menupos_old = menupos


	menupos+=per_line

	if(menupos >= menu_count){
		if(!wrap){
			menupos = menu_count-1;	
		} else {
			menupos = 0;	
		}
	}

	if(global.menu_sounds){soundfxPlay(sfx_sound_menu_default);}

	RUMBLE_MOVE_MENU


	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}

	if(global.voice){
		tts_say(menu_name[menupos])	
	
		if(global.speak_details){
			tts_say(menu_desc[menupos])	
		}
	}

	surface_update = true


	// do option top stuff
	if(menupos_old != menupos){
		option_top+=per_line
		if(option_top > menu_count-per_line){
			option_top = menu_count-per_line;
		}
		
		//calculate selected from option top
		var counter = 0;
		for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
			if(i == menupos){
				selected_draw_index = counter;
				break;
			}
			counter++;
		}
	}
	surface_update=true
}