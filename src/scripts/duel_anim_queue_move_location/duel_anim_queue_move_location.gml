var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _field = map[? "field"];
	var _part = map[? "part"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_field) &&
		!is_undefined(_part)){
			
		duelField_selectObject(_field,_part)
		with(duelController){
			selected_object = _field	
		}
		
		var obj = instance_create(0,0,animationTimer);
		obj.timer = 5;
		
		return obj;
	}

}

return noone;