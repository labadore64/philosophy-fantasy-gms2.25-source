var xx = argument[0];
var yy = argument[1];

selected_color = make_color_hsv(color_hsv[0],color_hsv[1],ceil(xx*255))

color_rgb[0] = color_get_red(selected_color)
color_rgb[1] = color_get_green(selected_color)
color_rgb[2] = color_get_blue(selected_color)

color_hsv[0] = color_hsv[0]
color_hsv[1] = color_hsv[1]
color_hsv[2] = ceil(xx*255)

col_pos[2] = color_hsv[2]/255;
color_hsv[2] = round(color_hsv[2]);

color_map = make_color_hsv(color_hsv[0],color_hsv[1],255); // this represents the color selected in the color map. value is ignored
color_value = color_hsv[2]; // this represents the value of the color selected

selected_color_display = dec_to_hex(color_rgb[0]) + dec_to_hex(color_rgb[1]) + dec_to_hex(color_rgb[2])