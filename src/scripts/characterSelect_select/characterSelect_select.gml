with(characterSelect){
	if(!lock){
		newgame = true;
		lock = true;
		running_object = instance_create(0,0,fadeOut)
		running_object.time = 75
		running_object.amount_per_cycle = 1/running_object.time;
		surface_update=true
		mouse_mode = false;
		if(global.menu_sounds){soundfxPlay(sfx_sound_confirm);}
		mouseHandler_clear()
	}
}