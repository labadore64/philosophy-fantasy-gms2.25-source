var scale = argument[0]
var xx = argument[1]
var yy = argument[2]

var hair_color = argument[3];
var skin_color = argument[4];
var shirt_color = argument[5];
var necklace_color = argument[6];

if(argument_count < 13){
	if(argument[13]){
		draw_sprite_extended_noratio(spr_faces_cute,argument[7],scale*(256+xx),scale*(256+yy),scale,scale,0,c_white,1)
	}
}

var flip = 1;
if(argument_count > 14){
	if(argument[14]){
		var flip = -1;	
	}
}

show_debug_message(string(scale*(256+xx)*flip))

draw_sprite_extended_noratio(spr_faces_back_hair,argument[8],scale*(256+xx)*flip,scale*(256+yy),scale*flip,scale,0,hair_color,1)
draw_sprite_extended_noratio(spr_faces_body,argument[9],scale*(256+xx)*flip,scale*(256+yy),scale*flip,scale,0,skin_color,1)
draw_sprite_extended_noratio(spr_faces_neck,argument[10],scale*(256+xx)*flip,scale*(256+yy),scale*flip,scale,0,skin_color,1)
draw_sprite_extended_noratio(spr_faces_shirt,argument[11],scale*(256+xx)*flip,scale*(256+yy),scale*flip,scale,0,shirt_color,1)
draw_sprite_extended_noratio(spr_faces_necklace,argument[12],scale*(256+xx)*flip,scale*(256+yy),scale*flip,scale,0,necklace_color,1)