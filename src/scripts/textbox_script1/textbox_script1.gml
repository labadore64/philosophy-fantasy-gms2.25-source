var i = obj_data.loaded_cutscene_counter
textbox_get_duelists()
if(duelist_sprite == spr_portrait_roger){
	duelist_sprite = spr_portrait_roger_cutscene	
}
obj_data.loaded_cutscene_counter++;

textboxShow(obj_data.loaded_textbox[i],duelist_name)

textbox.portrait_sprite = duelist_sprite
running_object = textbox;

textbox.newalarm[2] = TIMER_INACTIVE
textbox.newalarm[3] = TIMER_INACTIVE

soundPlaySong(obj_data.loaded_music[i],0)

with(textbox){
	event_perform(ev_alarm,2)
	event_perform(ev_alarm,3)
	with(obj_dialogue){
		newalarm[0] = TIMER_INACTIVE
		event_perform(ev_alarm,0)
	}
}