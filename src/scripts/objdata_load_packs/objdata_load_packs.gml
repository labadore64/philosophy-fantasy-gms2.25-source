/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\pack\\pack";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	pack_cover[itemcount] = card_from_name(ini_read_string_length("pack","cover","",IMPORT_MAX_SIZE_CARDNAME));
	pack_name[itemcount] = ini_read_string_length("pack","name","",IMPORT_MAX_SIZE_CARDNAME);
	pack_desc[itemcount] = ini_read_string_length("pack","desc","",IMPORT_MAX_SIZE_PACKDESC);
	pack_event[itemcount] = ini_read_string_length("pack","event","",IMPORT_MAX_SIZE_EVENT);
	
	pack_sprite[itemcount] = -1;
	
	if(pack_cover[itemcount] != -1){
		pack_sprite[itemcount] = pack_cover[itemcount];	
	}
	
	
	var r = ini_read_real("pack","r",0);
	var g = ini_read_real("pack","g",0);
	var b = ini_read_real("pack","b",0);
	
	pack_color[itemcount] = make_color_rgb(r,g,b)
	
	//cards and rates
	var i = 0;

	while(true){
		if(!ini_key_exists("pack","card"+ string(i))){
			break;
	    }
		i++
	}

	pack_card[itemcount,0] = -1;
	for(var j = 0; i > j; j++){
		pack_card[itemcount,j] = card_from_name(ini_read_string_length("pack","card"+ string(j),"",IMPORT_MAX_SIZE_CARDNAME))
		pack_rate[itemcount,j] = ini_read_real("pack","rate"+ string(j),1)
		pack_amount[itemcount,j] = pack_rate[itemcount,j] // this is overwriteen when a save file is loaded
	}
	pack_card_total[itemcount] = j;

	ini_close();
	
	// limit to 7 packs
	if(PAID_VERSION == 0 && itemcount > 7){
		break;	
	}
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

packs_total = itemcount;