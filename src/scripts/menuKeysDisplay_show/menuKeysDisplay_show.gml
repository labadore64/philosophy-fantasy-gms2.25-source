
if(def_count > 0){
	if(!instance_exists(menuKeysDisplay)){
		var obj = instance_create(0,0,menuKeysDisplay)
		obj.parent = id;
		var defs = def_count;

		with(obj){
			for(var i = 0; i < defs; i++){
				menuKeysDisplay_add_def(parent.key_name[i],parent.key_desc[i],parent.key_chara[i])
			}
			
			draw_size = ceil(defs/3)*60
		}
	}
}