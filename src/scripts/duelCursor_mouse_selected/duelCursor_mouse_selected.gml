
if(mouse_mode){
	var trigger_select = false;
	var clearshit = false;
	if(mouse_card_selected != mouse_card_selected_last){
		if(mouse_card_selected > -1){
			if(mouse_card_menupos[mouse_card_selected] > -1){
				// if the context menu is open you should select instead.
				if(do_the_context_menu){
					trigger_select = true	
					do_the_context_menu = false;
					context_menu_select = mouse_card_menupos[mouse_card_selected]
					context_menu_obj = mouse_card_element[mouse_card_selected]
					mouse_card_element[mouse_card_selected].menupos = mouse_card_menupos[mouse_card_selected]
					with(mouse_card_element[mouse_card_selected]){
						info_surf_update = true;
					}
				} else {
					mouse_card_element[mouse_card_selected].menupos = mouse_card_menupos[mouse_card_selected]
					with(mouse_card_element[mouse_card_selected]){
						with(duelController){
							if(selected_object > 0){
								last_selected_object = selected_object
								last_selected_thingie = selected_object.selected_object	
							}
					
							// create tooltip
						}
						duelController_make_tooltip(id,menupos);
				
						parent.selected_object = id;
						duelController.selected_object = parent
						if(menupos > -1){
							//duelController.viewcard_id = card_array[menupos]
						} else {
							duelController.viewcard_id = -1;	
						}
						info_surf_update = true;
					}
				}
			} else {
				clearshit = true;	
			}
		} else {
			clearshit = true	
		}
	} else {
		trigger_select = true;
	}
	
	if(trigger_select){
		do_the_context_menu = false;
		if(instance_exists(menuControl)){
			if(menuControl.active_menu == id){
				if(mouse_check_button_pressed(mb_left)){
					
					if(!keypress_this_frame){
						if(menu_select != -1){
							script_execute(menu_select)	
						}
					}
				}
			}
		}	
	}
	
	if(clearshit){
		duelController.mouse_card_selected_last = -1
		duelController.mouse_card_selected = -1;
		with(duelController.selected_object){
			selected_object = noone
		}
		duelController.selected_object = noone
	}
}