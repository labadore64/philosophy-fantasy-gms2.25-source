var textbox_id = "startgame002";

if(selected_deck == 1){
	textbox_id = "startgame003";
} else if (selected_deck == 2){
	textbox_id = "startgame004";
}

textboxShow(textbox_id,"Mystery Man")
textbox.portrait_sprite = spr_portrait_mystery_man
running_object = textbox;

textbox.newalarm[2] = TIMER_INACTIVE
textbox.newalarm[3] = TIMER_INACTIVE

with(textbox){
	event_perform(ev_alarm,2)
	event_perform(ev_alarm,3)
}