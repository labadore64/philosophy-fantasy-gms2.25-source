update_image_only_active = false;
var script_for_update = menuOverworldKeyboardKeybindStart

menu_add_option("Default","Reset to default keys.",menuOverworldKeyboardKeybindSetDefault,-1)
menu_add_option("Select","Select options in menus.",script_for_update,-1) // select
menu_add_option("Submit","Submits input and also selects on menus.",script_for_update,-1) // select 2
menu_add_option("Cancel","Cancel menus or selections.",script_for_update,-1) // cancel
menu_add_option("Up","Move Up.",script_for_update,-1) // up
menu_add_option("Down","Move Down.",script_for_update,-1) // down
menu_add_option("Left","Move Left.",script_for_update,-1) // left
menu_add_option("Right","Move Right.",script_for_update,-1) // right
menu_add_option("Shift Left","Shift Left.",script_for_update,-1) // left Tab
menu_add_option("Shift Right","Shift Right.",script_for_update,-1) // right Tab
menu_add_option("Sort","Sort a list.",script_for_update,-1)  // card deets
menu_add_option("View Card","View card details.",script_for_update,-1)  // sort
menu_add_option("Speed Up","Hold to speed up the game.",script_for_update,-1) //speed
menu_add_option("Camera Up","Pans battle camera up.",script_for_update,-1) // camup
menu_add_option("Camera Down","Pans battle camera down.",script_for_update,-1) // camup
menu_add_option("Camera Left","Pans battle camera left.",script_for_update,-1) // camup
menu_add_option("Camera Right","Pans battle camera right.",script_for_update,-1) // camup

menu_add_option("Go to Hand","In duels, moves to your hand.",script_for_update,-1) // view hand
menu_add_option("Go to Deck","In duels, moves to your deck.",script_for_update,-1) // view deck
menu_add_option("Go to Active","In duels, moves to your active card.",script_for_update,-1) // view active
menu_add_option("Go to Overlay","In duels, moves to your overlayed cards.",script_for_update,-1) // view overlay
menu_add_option("Go to Bench","In duels, moves to your bench.",script_for_update,-1) // view bench
menu_add_option("Go to Theory","In duels, moves to your theory.",script_for_update,-1) // view theory
menu_add_option("Go to Limbo","In duels, moves to your limbo.",script_for_update,-1) // view limbo

menu_add_option("Go to Enemy","If held down, goes to enemy field.",script_for_update,-1) // view switch

menu_add_option("Help","Opens help menu.",script_for_update,-1) // help
menu_add_option("Options","Opens Options menu.",script_for_update,-1) // options
menu_add_option("Screenshot","Takes a screenshot!",script_for_update,-1) // Screenshot
menu_add_option("View Controls","View a screen's control scheme.",script_for_update,-1) // Controls
menu_add_option("Exit","Exits the game.",script_for_update,-1) // exit
menu_add_option("TTS Repeat","Repeats the Text-to-Speech.",script_for_update,-1) // tts repeat
menu_add_option("TTS Stop","Stops the Text-to-Speech.",script_for_update,-1) // tts stop
for(var i = 0; i < 10; i++){
	menu_add_option("Access"+string(i),"Accessibility HUD Info Key.",script_for_update,-1);
}

keybind_text = ds_list_create();

key_ids = ds_list_create();

ds_list_add(key_ids,"","select","select2","cancel","up","down","left","right","lefttab","righttab","sort","view_card","speed")
ds_list_add(key_ids,"cam_up","cam_down","cam_left","cam_right")

ds_list_add(key_ids,"view_hand","view_deck","view_active","view_overlay","view_bench","view_theory","view_limbo","view_switch")
ds_list_add(key_ids,"help","options","screenshot","keydisplay","exit","tts_repeat","tts_stop")

for(var i = 0; i < 10; i++){
	ds_list_add(key_ids,"access"+string(i));
}

menuOverworldKeyboardSetKeybindText(keybind_text);

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

title = "Keyboard Configuration"

menuParentUpdateBoxDimension();

menu_draw = menuOverworldOptionKeyboardDraw

//menu_up = menuOverworldOptionRealUp;
//menu_down = menuOverworldOptionRealDown;

//menu_left = menu_up;
//menu_right = menu_down;