var old = menupos;

menuParent_moveUp();

// update option_top

if(old != menupos){
	selected_draw_index = menupos % 9;

	if(floor(menupos / 9) != floor(old / 9)){
		page = floor(menupos / 9)
		option_top = page*9
		appearanceModelMap_updateSurface();
	}

	if(option_top < 0){
		option_top = 0;	
	}
}
surf_main_update = true