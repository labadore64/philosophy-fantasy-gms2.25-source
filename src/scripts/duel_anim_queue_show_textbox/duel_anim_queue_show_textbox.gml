var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _msg = map[? "msg"];
	
	
	if(!is_undefined(_msg)){
			
		var obj = duelController_show_message(_msg)
		
		if(!is_undefined(map[? "resume"])){
			obj.resume_control = true
		}

		return obj;
	}
	
	ds_map_destroy(map)

}

return noone;