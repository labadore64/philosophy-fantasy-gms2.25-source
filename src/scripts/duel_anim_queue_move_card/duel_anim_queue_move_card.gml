var argu = argument[0]; // args as json string
var cando = false;

var _card_id,_card_origin,_card_dest,_time,_facedown,_camera_move,_block,flag;
	

// ai
if(argument_count > 1){
	cando = true
	
	_card_id = argument[0]
	_card_origin = argument[1]
	_card_dest = argument[2]
	_time = argument[3]
	_facedown = argument[4]
	_camera_move = argument[5]
	_block = argument[6]
	flag = argument[7]
} else {
	var map = json_decode(argu);

	if(!ds_map_empty(map)){
		// do your normal code down here
		_card_id = map[? "card_id"];
		_card_origin = map[? "origin"];
		_card_dest = map[? "dest"];
		_time = map[? "time"];
		_facedown = map[? "facedown"];
		_camera_move = map[? "movecamera"];
		_block = map[? "block"];
		flag = map[? "flag"];
	
		if(!is_undefined(_card_id) &&
			!is_undefined(_card_origin) &&
			!is_undefined(_card_dest) &&
			!is_undefined(_time) &&
			!is_undefined(_facedown) &&
			!is_undefined(_block) &&
			!is_undefined(flag) &&
			!is_undefined(_camera_move)){
				cando = true
			}
		
	}
	
	ds_map_destroy(map)
}

if(cando){
		
	if(duelController.mouse_mode){
		_camera_move = false;	
	}
	var le_card = -1;
	if(_card_id > -1 && _card_id < _card_origin.card_array_size){
		le_card = _card_origin.card_array[_card_id];
	}

	var obj = duel_anim_move_card(_card_id,_card_origin,_card_dest,_time,_facedown,_camera_move)
		
	// describe
	if(!duelController.ai_mode){
		if(bits_contain(flag, ANIMATION_MOVE_FLAG_CARD_EFFECT)){
			if(le_card > -1){
				duel_animation_pause(obj_data.card_name[le_card] + " moved to " + _card_dest.source_name);
			}
		}
	}
		
	if(bits_contain(flag, ANIMATION_MOVE_FLAG_CARD_SHUFFLE)){
		duel_animation_shuffle(_card_origin);
	}
		
	if(obj > -1){
		if(bits_contain(flag, ANIMATION_MOVE_FLAG_IGNORE_SOUND)){
			obj.ignore_sound = true;	
		}
		
		if(bits_contain(flag, ANIMATION_MOVE_FLAG_CAMERA_ADJUST)){
			obj.camera_adjust = true;	
		}
	}
							
	// do effect
	if(!_block){
		if(_card_id > -1){
			
			var this = code_set_cardstate(_card_dest,_card_dest.card_array_size)
			with(_card_dest){

				if(object_index == duelHand){
					duel_animation_card_effect(le_card,"hand",this,true,_card_origin.is_ai);	
			
				} else if(object_index == duelDeck){
			
					duel_animation_card_effect(le_card,"deck",this,true,_card_origin.is_ai);	
			
				} else if(object_index == duelWaiting){
			
					duel_animation_card_effect(le_card,"limbo",this,true,_card_origin.is_ai);	
			
				} else if(object_index == duelTheory){
					duel_animation_card_effect(le_card,"theory",this,true,_card_origin.is_ai);	
			
				} else if (object_index == duelActive){
					if(card_array_size == 0){
						duel_animation_card_effect(le_card,"active",this,true,_card_origin.is_ai);		
					} else {
						duel_animation_card_effect(le_card,"overlay",this,true,_card_origin.is_ai);	
					}
			
				} else if (object_index == duelBench){
					duel_animation_card_effect(le_card,"bench",this,true,_card_origin.is_ai);	
				}
	
			}

			with(_card_origin){
			// leave cases. if its moved to Limbo.
				if(_card_dest.object_index == duelWaiting){
					if(object_index == duelActive){
						if(_card_id == 0){
							duel_animation_card_effect(le_card,"active_limbo",this,true,_card_origin.is_ai);	
						} else {
							duel_animation_card_effect(le_card,"overlay_limbo",this,true,_card_origin.is_ai);
						}
					} else if(object_index == duelBench){
						duel_animation_card_effect(le_card,"bench_limbo",this,true,_card_origin.is_ai);	
					} else if(object_index == duelHand){
						duel_animation_card_effect(le_card,"hand_limbo",this,true,_card_origin.is_ai);	
					} else if(object_index == duelDeck){
						duel_animation_card_effect(le_card,"deck_limbo",this,true,_card_origin.is_ai);	
					} else if(object_index == duelTheory){
						duel_animation_card_effect(le_card,"theory_limbo",this,true,_card_origin.is_ai);	
					}		
				}
			}
		
		}
	}
		
	if(obj > -1){
		obj.sound_id = soundfxPlay(sfx_sound_play_card);
	}
		
	return obj;
}

return noone;