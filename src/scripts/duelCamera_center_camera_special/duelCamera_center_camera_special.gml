#macro TOOLTIP_CAMERA_ADJUST_Y 65
#macro TOOLTIP_CAMERA_ADJUST_SCALE 2.15

with(duelCamera){
		if(duelController.mouse_mode){
			var modi = 0;
			var scale = 2.75;
			
			if(duelController.active_player = DUEL_PLAYER_ENEMY){
				modi = -550
			}
			
			if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
				modi -= TOOLTIP_CAMERA_ADJUST_Y
				scale = TOOLTIP_CAMERA_ADJUST_SCALE
			}
			

			duel_animation_move_camera(225+110,290+290+modi,scale,10)

		}
}