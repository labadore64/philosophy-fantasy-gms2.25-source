// trigger effects based on phase
with(duelController){
	if(instance_exists(field[active_player])){
		// first the active cards
		var phase;
		phase[0] = "draw"
		phase[1] = "standby"
		phase[2] = "limbo"
		phase[3] = "main"
		phase[4] = "dialog"
		phase[5] = "return"
		phase[6] = "end"
	
		var active_you = field[active_player].active_monster;
	
		if(active_you.card_array_size > 0){
			duel_animation_card_effect(active_you.card_array[0],phase[phase_current]+"_start",code_set_cardstate(active_you,0));
			if(active_you.card_array_size > 1){
				duel_animation_card_effect(active_you.card_array[1],phase[phase_current]+"_overlay_start",code_set_cardstate(active_you,1));
				if(active_you.card_array_size > 2){
					duel_animation_card_effect(active_you.card_array[2],phase[phase_current]+"_overlay_start",code_set_cardstate(active_you,2));
				}
			}
		}
	
		active_you = field[active_player].bench;
	
		if(active_you.card_array_size > 0){
			duel_animation_card_effect(active_you.card_array[0],phase[phase_current]+"_bench_start",code_set_cardstate(active_you,0));
			if(active_you.card_array_size > 1){
				duel_animation_card_effect(active_you.card_array[1],phase[phase_current]+"_bench_start",code_set_cardstate(active_you,1));
				if(active_you.card_array_size > 2){
					duel_animation_card_effect(active_you.card_array[2],phase[phase_current]+"_bench_start",code_set_cardstate(active_you,2));
				}
			}
		}
		active_you = field[active_player].theory
	
		if(active_you.card_array_size > 0){
			duel_animation_card_effect(active_you.card_array[0],phase[phase_current]+"_theory_start",code_set_cardstate(active_you,0));
		}
	}
}