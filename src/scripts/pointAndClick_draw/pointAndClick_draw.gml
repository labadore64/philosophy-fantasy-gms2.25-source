
if(!instance_exists(darkScreen)){
	
	// background
	if(!surface_exists(part_surf)){
		part_surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){
			surface_resize(part_surf,global.window_width,global.window_height);
		}
	}
	
	surface_set_target(part_surf)
	draw_clear_alpha(c_black,1);

	draw_set_alpha(1)
	if(img_sprite > -1){
		draw_sprite_extended_ratio(img_sprite,0,0,0,1,1,0,c_white,1)
	} else {
		if(global.color_gradient){
			draw_rectangle_color_ratio(0,0,800,600,
									global.screen_bgcolor[0],global.screen_bgcolor[0],
									global.screen_bgcolor[1],global.screen_bgcolor[1],
											false)
		} else {
			draw_set_color(global.screen_bgcolor[0])
			draw_rectangle_ratio(0,0,800,600,
										false)
		}
	

		gpu_set_colorwriteenable(true,true,true,false)
		if(global.menu_index > -1){
			gpu_set_blendmode(bm_add)
			if(global.menu_animate){
				draw_sprite_extended_ratio(global.menu_index,ScaleManager.spr_counter,400,300,2,2,0,$181818,1)
			} else {
				draw_sprite_extended_ratio(global.menu_index,75,400,300,2,2,0,$181818,1)
			}
			gpu_set_blendmode(bm_normal)
		}
	}

	surface_reset_target();
	draw_surface(part_surf,0,0);	
	
	for(var i = 0; i < sprite_count; i++){
		var scale = 1;
		
		if(menupos == i){
			if(MouseHandler.menu_hover || MouseHandler.ignore_menu){
				scale = 1.05
			}
		}
		if(sprite_exist[i]){
			draw_sprite_extended_ratio(sprites[i],0,
							sprite_x[i],sprite_y[i],
							sprite_xscale[i]*scale,sprite_yscale[i]*scale,
							0,sprite_color[i],sprite_alpha[i])
		}
	}

	if(!instance_exists(TextBox)){
		menuParent_draw_cancel_button();
	}

	draw_sprite_ext(cur_sprite,0,MouseHandler.mousepos_x,MouseHandler.mousepos_y,global.scale_factor,global.scale_factor,0,c_white,1)
	
	if(menupos > -1){
		if(sprite_show[menupos]){
			if(!trigger){
				if(menu_count > 0){
					menuDrawTooltip();
				}
			}
		}
	}
	
	draw_activity_energy()
	
		with(fadeOut){
			draw_fade();	
		}
	
		with(fadeIn){
			draw_fade();	
		}
}
draw_letterbox();