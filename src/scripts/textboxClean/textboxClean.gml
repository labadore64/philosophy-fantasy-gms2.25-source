if(ds_exists(text,ds_type_list)){
	ds_list_destroy(text)	
}

if(ds_exists(add_beforetext,ds_type_list)){
	ds_list_destroy(add_beforetext)	
}

if(ds_exists(add_aftertext,ds_type_list)){
	ds_list_destroy(add_aftertext)	
}

with(display_object){
	instance_destroy();	
}

if(ds_exists(scripts,ds_type_map)){
	ds_map_destroy(scripts);	
}

if(ds_exists(arguments,ds_type_map)){
	ds_map_destroy(arguments);	
}

with(parent){
	lock = false;	
}

audio_stop_sound(text_sound1);
audio_stop_sound(text_sound2);
audio_stop_sound(text_sound3);
audio_stop_sound(text_sound4);

if(end_script != -1){
	script_execute(end_script);	
}

tts_stop();


if(event_id != ""){
	eventSet(event_id);
}
with(menuParent){
	if(textbox == id){
		textbox = noone;	
	}
}