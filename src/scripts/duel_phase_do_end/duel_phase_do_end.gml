

if(active_player == DUEL_PLAYER_YOU){
	// if you have more than 6 cards, you must discard until you only have 6 cards.
	if(field[0].hand.card_array_size > 5){
		duelCamera_center_camera_special()
		duel_animation_show_textbox("Discard from your hand until you only have 5 cards remaining.")
	} else {
		// end turn animation
		duelCamera_center_camera_special()
		duel_animation_wait(15)
		duel_animation_end_turn()	
		duel_animation_wait(15)
	}
} else {
	//duel_phase_do_end_enemy()
}


//duel_animation_next_phase();