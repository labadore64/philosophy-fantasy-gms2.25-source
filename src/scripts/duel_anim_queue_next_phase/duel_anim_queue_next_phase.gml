var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _time = map[? "time"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_time)){
			
		if(!duelController.complete){
			
			with(duelController){
				draw_phase_change = true;
				phase_current++;
				// skip the standby phase now.
				if(phase_current == DUEL_PHASE_STANDBY){
					phase_current++;	
				}
				
				do_return_overlay = false;
				if(array_length_1d(phase_name) <= phase_current){
					duel_start_turn();
				}
	
				if(phase_current == DUEL_PHASE_DRAW){
					duelCamera_center_camera_special()
				}
	
			}
			
			duelController.phase_transition = instance_create(0,0,animationPhaseTransition);
			duelController.phase_transition.timer = 120
			//var obj = instance_create(0,0,animationTimer);
			//obj.timer = 120

			return duelController.phase_transition
		}
	}

}

return noone;