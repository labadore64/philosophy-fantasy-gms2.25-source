var args = string_copy(argument[0],2,string_length(argument[0])-2);
var len = string_length(args);
var obj = argument[1]; // card target object
var index = argument[2]; // card target

var charr;

with(duelController){
	// process args
	var target = ""; // what this negate can trigger. if empty negates all cards
	var effect = ""; // what effect this card negates. if empty negates any effect
	
	// parse arguments into target and effect
	var holder = args;
	var pos = string_pos(",",holder);
	target = string_copy(holder,1,pos-1);
	
	holder = string_copy(holder,pos+1,string_length(holder))
	pos = string_pos(",",holder);
	
	if(pos > -1){
		effect = string_copy(holder,1,pos-1);
	} else {
		effect = holder;	
	}
	
	// check if the effect is correct, if not exit false
	if(effect != "" && effect != "any"){
		if(string_pos(effect,duelController.temp_loaded_block) == 0){
			return false;	
		}
	}
	
	// then compare the target with the current "current_this" position
	
	// if its all, then its any damn card.
	var success = false;
	if(target == "all"){
		success = true;	
	} else {
		var this = code_get_this();
		
		if (string_pos("enemy",target) == 1){
			success = status_effect_negate_get_enemy(target,this);
		} else if (string_pos("you",target) == 1){
			success = status_effect_negate_get_you(target,this);
		} else {
			success = status_effect_negate_get_range(target,this);	
		}
	}
	
	if(success){
		duelController.temp_negated = true;	
	}
	
	return success;
}