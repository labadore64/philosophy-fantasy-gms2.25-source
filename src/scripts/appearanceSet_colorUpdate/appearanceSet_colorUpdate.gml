var color = selected_color

with(parent){
	portrait_color[color_count[menupos]] = color
	
	color_id[menupos] = color
	color_hex[menupos] = dec_to_hex(color_get_red(color)) +
					dec_to_hex(color_get_green(color)) +
					dec_to_hex(color_get_blue(color))
	color_rgb[menupos] = string(round(color_get_red(color))) + ", " +
					string(round(color_get_green(color))) + ", " +
					string(round(color_get_blue(color)))
	color_hsv[menupos] = string(round(color_get_hue(color))) + ", " +
					string(round(color_get_saturation(color))) + ", " +
					string(round(color_get_value(color)))	
	surf_main_update = true
}