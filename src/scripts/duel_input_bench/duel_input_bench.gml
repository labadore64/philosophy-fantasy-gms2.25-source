// should be in all scripts
var index = argument[1]
var origin = argument[0]

with(origin){
	
	duelController.speak_tts = false;
	var mon = card_array[index]
	if(mon > -1){
		tts_say(obj_data.card_name[mon] + " Benched");	
	}
	duel_animation_play_sound(sfx_sound_duel_bench_card)
	duel_animation_activate_card(mon)
	duel_animation_move_card(index,id,parent.part[DUEL_FIELD_BENCH],
										false,false,power(2,ANIMATION_MOVE_FLAG_IGNORE_SOUND))
	//duel_animation_wait(20)
	////duel_animation_resume_control()
	parent.bench_counter++;
}

return noone