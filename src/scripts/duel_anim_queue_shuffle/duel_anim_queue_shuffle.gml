var argu = argument[0]; // args as json string
var cando = false;

var _loc;

if(argument_count > 1){
	_loc = argument[1];
} else {
	var map = json_decode(argu);
	
	if(!ds_map_empty(map)){
		// do your normal code down here
		_loc = map[? "loc"];
	
		if(!is_undefined(_loc)){
			cando = true
		}
	}
	ds_map_destroy(map)
}

if(cando){
	with(_loc){
		duelField_randomize();
		var name = "Your";
		if(enemy){
			name = "Enemy"	
		}
		
		if(!duelController.ai_mode){
			duel_animation_pause(name + " " + source_name + " shuffled.");
		}
	}
	
	if(!duelController.ai_mode){
		with(duelController){
			shuffle_count++;
			show_debug_message("shuffle: " +string(shuffle_count))
		}
		var obj = instance_create(0,0,animationTimer);
		obj.timer = 1;
		duel_animation_play_sound(sfx_sound_deck_shuffle)
		duel_animation_show_sprite(spr_deck_shuffle)
		duel_animation_wait(20);
		return obj;
	}
	
}

return noone;