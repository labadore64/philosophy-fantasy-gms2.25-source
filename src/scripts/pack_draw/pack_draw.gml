/// @description Insert description here
// You can write your code in this editor
with(argument[0]){
	if(pack_id > -1){
		var w = 128*image_xscale
		var h = 256*image_yscale
	
		if(!surface_exists(surface)){
			surface = surface_create(w*global.scale_factor,h*global.scale_factor);	
			surface_update = true
		} else {
			if(ScaleManager.updated){
				surface_resize(surface,w*global.scale_factor,h*global.scale_factor)	
				surface_update = true
			}
		}
	
		if(surface_update){
			surface_set_target(surface)
			if(!global.high_contrast){
				draw_sprite_ext(spr_pack,0,0,0,global.scale_factor*image_xscale,global.scale_factor*image_yscale,0,obj_data.pack_color[pack_id],1)	
			} else {
				draw_sprite_ext(spr_pack,0,0,0,global.scale_factor*image_xscale,global.scale_factor*image_yscale,0,merge_color(obj_data.pack_color[pack_id],c_white,.25),1)	
			}

			gpu_set_blendmode(bm_add)
			draw_sprite_ext(obj_data.card_sprite[obj_data.pack_sprite[pack_id]],0,-55*global.scale_factor,10*global.scale_factor,.9*global.scale_factor*image_xscale,.9*global.scale_factor*image_yscale,0,c_white,.25)
			gpu_set_blendmode(bm_normal)
			surface_reset_target()
		}
	
		draw_surface_ext(surface,
					global.display_x+ argument[1]*global.scale_factor,
					global.display_y+ argument[2]*global.scale_factor,
					1,1,0,
					argument[3],
					argument[4])
	}
}