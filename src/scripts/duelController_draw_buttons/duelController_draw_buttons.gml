var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}


for(var i = 0; i < side_button_total; i++){

	if(side_pressed[i]){
		draw_sprite_extended_ratio(side_sprite[i],0,
			side_button_x+side_button_spacer*(i)+25,side_button_y+25+pop_menu_y,
			.85,.85,0,col,1)
	} else {
		draw_sprite_extended_ratio(side_sprite[i],0,
			side_button_x+25+side_button_spacer*(i),side_button_y+25+pop_menu_y,
			side_hover[i],side_hover[i],0,col,1)
	}
	
}