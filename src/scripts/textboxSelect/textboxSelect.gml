textbox_hold_counter = 0;
auto_countdown = -1;
if(!wait){
	if(ds_exists(text,ds_type_list)){
		if(display_object != -1){
			var size = ds_list_size(text);
			if(line < size){
				var cando = false;
				
				if(MouseHandler.clicked_this_frame || global.textSpeed == -1){
					with(display_object){
						message_current++;
						cutoff = 0;
						cando = true;
						if(global.textSpeed == -1){
							if(array_length_1d(message) > message_current){
								string_lenger = string_length(message[message_current]);
								last_message = message_current;
								surface_draw = true;	
								cutoff = string_lenger;
							}
						}
					}
				}
				with(display_object){
					if(cutoff != string_lenger){
						cutoff = string_lenger;
						audio_stop_sound(text_sound1);
						audio_stop_sound(text_sound2);
						audio_stop_sound(text_sound3);
						audio_stop_sound(text_sound4);
						surface_draw = true;
					} else {
						message_current++;
						cutoff = 0;
						cando = true;
					}
				}
	
				if(cando){
					line++;
					if(line < size){
						textboxExecuteScript();
						textboxSetCurrentText();
						if(tts_speak){
							tts_say(current_text);
						}	
						if(!global.text_sound && global.menu_sounds){
							soundfxPlay(sfx_sound_menu_default)	
						}
						
						for(var i = 0; i < draw_count; i++){
							with(draw_array[i]){
								instance_destroy();	
							}
						}

						// resets the array
						draw_array = -1;
						draw_array[0] = noone;
						draw_count = 0;
					} else {
						if(!global.text_sound && global.menu_sounds){
							soundfxPlay(sfx_sound_menu_default)	
						}
						instance_destroy();	
					}
				}
	
			} else {
				instance_destroy();	
			}
		}
	}
} else {
	wait = false;	
}
RUMBLE_MOVE_MENU