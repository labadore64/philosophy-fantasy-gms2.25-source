var scale = argument[0]
var xx = argument[1]
var yy = argument[2]

var hair_color = argument[3];
var skin_color = argument[4];
var shirt_color = argument[5];
var necklace_color = argument[6];

draw_sprite_extended_ratio(spr_faces_cute,argument[7],scale*(256+xx),scale*(256+yy),scale,scale,0,c_white,1)
draw_sprite_extended_ratio(spr_faces_back_hair,argument[8],scale*(256+xx),scale*(256+yy),scale,scale,0,hair_color,1)
draw_sprite_extended_ratio(spr_faces_body,argument[9],scale*(256+xx),scale*(256+yy),scale,scale,0,skin_color,1)
draw_sprite_extended_ratio(spr_faces_neck,argument[10],scale*(256+xx),scale*(256+yy),scale,scale,0,skin_color,1)
draw_sprite_extended_ratio(spr_faces_shirt,argument[11],scale*(256+xx),scale*(256+yy),scale,scale,0,shirt_color,1)
draw_sprite_extended_ratio(spr_faces_necklace,argument[12],scale*(256+xx),scale*(256+yy),scale,scale,0,necklace_color,1)