var deleted = "";

if(current_name_length > 0){
	if(insert_pos > name_length-1){
		insert_pos = name_length-1	
	}
	
	var repeatcount = 1;
	if(name[insert_pos] == ""){
		repeatcount = 2;	
	}

	repeat(repeatcount){
		deleted = name[insert_pos]
		for(var i = insert_pos; i < name_length-1; i++){
			name[i] = name[i+1] 
		}

		name[name_length-1] = "" 

		insert_pos--;
	}

	if(repeatcount == 2){
		insert_pos++	
	}

	if(insert_pos < 0){
		insert_pos = 0;	
	}
	soundfxPlay(sfx_sound_menu_backspace)	

}
nameInput_updateMove()
if(deleted != ""){
	tts_clear();
	tts_say(deleted + " deleted")
}