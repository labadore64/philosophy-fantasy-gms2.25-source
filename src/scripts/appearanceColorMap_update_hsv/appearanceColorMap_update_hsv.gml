for(var i = 0; i < 3; i++){
	color_hsv[i] = clamp(round(color_hsv[i]),0,255);
}

selected_color = make_color_hsv(color_hsv[0],color_hsv[1],color_hsv[2]);

color_rgb[0] = color_get_red(selected_color)
color_rgb[1] = color_get_green(selected_color)
color_rgb[2] = color_get_blue(selected_color)

for(var i = 0; i < 3; i++){
	col_pos[i] = color_hsv[i]/255;
}

// invert saturation
col_pos[1] = 1-col_pos[1];

color_map = make_color_hsv(color_hsv[0],color_hsv[1],255); // this represents the color selected in the color map. value is ignored
color_value = color_hsv[2]; // this represents the value of the color selected

selected_color_display = dec_to_hex(color_rgb[0]) + dec_to_hex(color_rgb[1]) + dec_to_hex(color_rgb[2])

if(parent_update > -1){
	script_execute(parent_update);	
}

surf_main_update=true