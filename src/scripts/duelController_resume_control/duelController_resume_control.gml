if(duelController.active_player == DUEL_PLAYER_YOU){
	
	if(!global.dont_record){
		duelController.draw_pointer = true
		duelController.regain_control = true
		duelController.speak_tts = true;
		duelController.resumeAnim = false;
		
		if(global.menu_sounds){
			soundfxPlay(sfx_sound_duel_return_control)	
		}
		if(instance_exists(duelController.selected_object)){
			if(instance_exists(duelController.selected_object.selected_object)){
				if(duelController.selected_object.selected_object.object_index != duelDeck &&
					duelController.selected_object.selected_object.object_index != duelWaiting){
					duelField_tts_read();
				}
			}
		}
		
		duelField_tts_part_read()
		
		duel_animation_center_camera_resume(duelController.field[duelController.active_player],"");
	}
}