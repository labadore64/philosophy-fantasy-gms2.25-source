
if(card_sprite > -1){
	//draw_sprite_extended_ratio(sprite2,0,10,10,card_scale,card_scale,0,c_white,1)
	
	if(animation == CARD_ANIMATION_FLASH){
		if(!surface_exists(card_surface)){
			card_reset_size();
			card_surface = surface_create(global.window_width,global.window_height);	
			card_surface_update = true
		} else {
			if(ScaleManager.updated){
				card_reset_size();
				surface_resize(card_surface,global.window_width,global.window_height)	
				card_surface_update = true
			}
		}
		
		if(card_surface_update){
			
			surface_set_target(card_surface)
			draw_clear_alpha(c_black,0);
			gpu_set_colorwriteenable(true,true,true,true)
			draw_sprite_pos_ratio(card_sprite,0,
							xx[0],yy[0],
							xx[1],yy[1],
							xx[2],yy[2],
							xx[3],yy[3],
							c_white,
							image_alpha)	
	

			gpu_set_colorwriteenable(true,true,true,false)
			gpu_set_blendmode(bm_add)
			draw_sprite_extended_ratio(spr_card_flash,44-animation_time,xx[0],yy[0],
										card_scale,card_scale,0,c_white,1)
			gpu_set_blendmode(bm_normal)
			gpu_set_colorwriteenable(true,true,true,true)

			card_surface_update =false;
			surface_reset_target();
		}
		
		gpu_set_colorwriteenable(true,true,true,true)
		draw_surface(card_surface,0,0)
	} else {
		draw_sprite_pos_ratio(card_sprite,0,
						xx[0],yy[0],
						xx[1],yy[1],
						xx[2],yy[2],
						xx[3],yy[3],
						c_white,
						image_alpha)	
	}
}