var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _att = map[? "att"];
	var _def = map[? "def"];
	var _enemy = map[? "enemy"];
	//var this = map[? "this"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_att) &&
		!is_undefined(_def)){
			
		var target_index = -1; // the card index to move
		var target_origin = -1; // the location of the target
		var target_destination = -1; // where to move the target
		var facedown = false;

		var block = false;

		var att = 0;
		var def = 0;
		var enemy = _enemy//DUEL_PLAYER_YOU
		
		/*
		if(string_pos("pos_enemy",this) > 0){
			enemy = DUEL_PLAYER_ENEMY;
		}
		
		if(_enemy){
			if(enemy == DUEL_PLAYER_YOU){
				enemy = DUEL_PLAYER_ENEMY
			} else {
				enemy = DUEL_PLAYER_YOU	
			}
		}
		*/
		
		var stringer = "";

		if(string_length(_att) > 0){
			var targe = _att
			// if its a number
			if(string_length(string_digits(targe)) == string_length(targe)){
				att = real(targe);	
			} else {
				// get number from variables
				if(ds_map_exists(duelController.code_values,targe)){
					att = duelController.code_values[? targe]
					if(!is_undefined(att)){
						att = real(att);	
					}
				}
			}
		}
	
		if(string_length(_def) > 0){
			var targe = _def
			// if its a number
			if(string_length(string_digits(targe)) == string_length(targe)){
				def = real(targe);	
			} else {
				// get number from variables
				if(ds_map_exists(duelController.code_values,targe)){
					def = duelController.code_values[? targe]
					if(!is_undefined(def)){
						def = real(def);	
					}
				}
			}
		}

		with(duelController){
			var name = duel_hp[enemy].name
			
			with(field[enemy]){
				att_boost += att
				def_boost += def
				
				if(att > 0){
					duel_animation_pause(name + 
						"'s attack boosted by " + string(att_boost) +
						" this turn!")
				} else if (def > 0){
					duel_animation_pause(name + 
						"'s defense boosted by " + string(def_boost) +
						" this turn!")
				}
			}
		}

	}

}

return noone;