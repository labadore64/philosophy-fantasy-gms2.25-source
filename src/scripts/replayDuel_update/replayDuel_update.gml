// updates when you view a new replay
for(var i = 0; i < 2; i++){
	if(ds_exists(replay_char_loaded[i], ds_type_map)){
		ds_map_destroy(replay_char_loaded[i]);
	}

	replay_char_loaded[i] = json_decode(replay_player[menupos,i]);
	var mape = replay_char_loaded[i];
	replay_char_name[i] = mape[? "name"];
	replay_surf_update[i] = true;
	
	// load the properties for custom sprites
	if(ds_exists(replay_char_loaded[i],ds_type_map)){
		var map = replay_char_loaded[i];
	
		var val = map[? "val"]
		if(!is_undefined(val)){
			var sizer = ds_list_size(val);
			for(var j = 0; j < sizer; j++){
				portrait_property[i,j] = val[|j];	
			}
		}
		
		var val = map[? "col"]
		if(!is_undefined(val)){
			var sizer = ds_list_size(val);
			for(var j = 0; j < sizer; j++){
				portrait_color[i,j] = val[|j];	
			}
		}
		replay_chara_id[i] = map[? "chara"]
		
		var val = map[? "sprite"]
		if(!is_undefined(val)){
			if(val == "custom"){
				replay_custom[i] = true;
			} else {
				replay_custom[i] = false
				replay_sprite[i] = asset_get_index(val)
			}
		}
	}
		
	
}

return 0;
