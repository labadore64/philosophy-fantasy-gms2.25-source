var do_camera = true;

with(drawMoveCard){
	do_camera = move_camera;	
}
if(!duelController.mouse_mode){
	if(do_camera){
		if(enemy){
			var posx = 150
			var posy = 125


			duel_camera_focus_object(posx,posy,5,1.5);
			duelController.pointer_x = 130+75
			duelController.pointer_y = 130+50
		} else {
			var posx = 500
			var posy = 550

			duel_camera_focus_object(posx,posy,5,1.5);
			duelController.pointer_x = 500+75
			duelController.pointer_y = 550+50
		}
	}
}