// should be in all scripts
var index = argument[0];
var origin = argument[1];

duelController.phase_current = DUEL_PHASE_RETURN
// do the rest here.
if(duelController.active_player == DUEL_PLAYER_YOU){
	duelController_next_phase();
} else {
	duelController.phase_current = DUEL_PHASE_END
	//duel_animation_end_turn();	
	do_enemy_end = true;
	do_enemy_end_timer = 2;
}
with(duelController){
	if(pop_menu_active){
		duelController_pop_menu();	
	}
}

return noone;