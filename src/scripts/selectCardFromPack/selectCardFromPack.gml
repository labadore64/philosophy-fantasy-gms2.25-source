// argument[0] = pack to select from

if(argument[0] > -1){

	var i = argument[0];

	var rand = irandom(obj_data.pack_card_total[i]-1)
	var abort = 0;
	while(obj_data.pack_amount[i,rand] < 0 && abort < 5000){
		rand = irandom(obj_data.pack_card_total[i]-1);
		abort++
	}
	obj_data.pack_amount[i,rand]--;
	// count how many are left, if 0 then reset values
	var total = 0;
	for(var j = 0; j < obj_data.packs_total; j++){
		for(var k = 0; k < obj_data.packs_total; k++){
			total+= obj_data.pack_amount[j,k]
		}
	}
	
	if(total == 0){
		for(var j = 0; j < obj_data.packs_total; j++){
			for(var k = 0; k < obj_data.packs_total; k++){
				obj_data.pack_amount[j,k] = obj_data.pack_rate[j,k]
			}
		}
	}
	
	return obj_data.pack_card[i,rand];
}

return -1;