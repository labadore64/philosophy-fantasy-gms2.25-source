var do_camera = true;

with(drawMoveCard){
	do_camera = move_camera;	
}

if(!duelController.mouse_mode){
	if(do_camera){
		if(enemy){
			var posx = 150
			var posy = 20


			duel_camera_focus_object(posx,posy,5,1.5);
			duelController.pointer_x = 130+75
			duelController.pointer_y = 10+50
		} else {
			var posx = 500
			var posy = 670


			duel_camera_focus_object(posx,posy,5,1.5);
			duelController.pointer_x = 500+75
			duelController.pointer_y = 550+110+50
		}
	}
}