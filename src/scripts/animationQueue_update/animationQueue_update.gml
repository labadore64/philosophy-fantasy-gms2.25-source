if(!instance_exists(fadeIn) && !instance_exists(fadeOut)){
	if(ds_exists(queue,ds_type_list)){
		if(!instance_exists(running_obj)){
			if(!ds_list_empty(queue)){
				with(duelController){
					if(!regain_control){
						draw_pointer = false
					}
				}
				var script = -1;
				var myid = "";
				// gets script
				while(myid == "" && !ds_list_empty(queue) && script == -1){
					myid = queue[|0];
					ds_list_delete(queue,0)
					if(is_undefined(myid)){
						myid = "";	
					}
					
					// gets args for script
					var argu = args[? myid];
					if(is_undefined(argu)){
						argu = -1;	
					}
			
					// gets the script
					script = arg_to_script[? myid]
					if(is_undefined(script)){
						script = -1;
						myid = "";
					}
				}
			
				if(script == -1){
					instance_destroy();	
				} else {
					// initiate the subscript queue
					with(duelController){
						subscript_queue = -1
						subscript_queue_pos = 0;
						tooltip_show = false
					}
				
					running_obj = script_execute(script,argu)
					
					// clear args
					ds_map_delete(args, myid)
					ds_map_delete(arg_to_script, myid)

					// add the subscript queue and destroy it
					/*
					with(duelController){
						if(ds_exists(subscript_queue,ds_type_list)){
							sizer = ds_list_size(subscript_queue);
							for(var i = sizer-1; i >= 0; i--){
								ds_list_insert(qu,0,subscript_queue[|i])
							}
							ds_list_destroy(subscript_queue);
						}
						subscript_queue = -1;
					}
					*/
					var me = id;
					with(duelController){
						animation_obj_count = ds_list_size(me.queue)	
					}
		
					counter++;
					
				}
			} else {
				instance_destroy();	
			}
		}
	} else {
		instance_destroy();	
	}
}