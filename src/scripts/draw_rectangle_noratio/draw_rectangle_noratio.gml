var posx = argument[0]*global.scale_factor;
var posy = argument[1]*global.scale_factor;
var posx2 = argument[2]*global.scale_factor;
var posy2 = argument[3]*global.scale_factor;


draw_rectangle(posx,
				posy,
				posx2,
				posy2,
				argument[4]);