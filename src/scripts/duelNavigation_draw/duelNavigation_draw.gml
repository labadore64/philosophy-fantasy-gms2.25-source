
	draw_set_color(c_white)
	if(global.high_contrast){
		draw_rectangle_ratio(x,y,x+menu_width,y+menu_height,false)
	} else {

		if(global.color_gradient){
			draw_rectangle_color_ratio(x,y,x+menu_width,y+menu_height,
				global.menu_grcolor[1],global.menu_grcolor[1],
				global.menu_grcolor[0],global.menu_grcolor[0],
			false)
		
		} else {
			draw_set_color(global.menu_grcolor[0])
			draw_rectangle_ratio(x,y,x+menu_width,y+menu_height,
			false)
		}

	}

	var bg_col = global.menu_bgcolor;
	if(global.high_contrast){
		bg_col = c_black
		draw_set_color(c_black)
	}
	draw_set_color(bg_col)
	draw_set_alpha(1)
	draw_set_halign(fa_left)
	draw_rectangle_ratio(x+option_x_offset,y+option_y_offset,x+menu_width-option_x_offset,y+menu_height-option_x_offset,false)
	
	
	var colx = global.text_fgcolor

	if(global.high_contrast){
		colx = c_white;
	}
				
	draw_set_color(colx)
	draw_set_alpha(1)
	draw_set_font(global.normalFont)
	
	draw_text_transformed_ratio(x+option_x_offset+5,y+5,title,2,2,0)

	// draw nav buttons
	var col = colx
	if(duelController.pan_up){
		col = c_red	
	}
	draw_sprite_extended_ratio(spr_point_arrow,0,x+nav_x,y+nav_y,
								.5,.5,0,col,1)
	var col = colx
	if(duelController.pan_down){
		col = c_red	
	}
	draw_sprite_extended_ratio(spr_point_arrow,0,x+nav_x,y+nav_y+75,
								.5,-.5,0,col,1)
	var col = colx
	if(duelController.pan_right){
		col = c_red	
	}					
	draw_sprite_extended_ratio(spr_point_arrow,0,x+nav_x+38,y+nav_y+38,
								.5,.5,270,col,1)
	var col = colx
	if(duelController.pan_left){
		col = c_red	
	}
	draw_sprite_extended_ratio(spr_point_arrow,0,x+nav_x-38,y+nav_y+38,
								.5,.5,90,col,1)
		
	// draw scale
	draw_sprite_extended_ratio(spr_point_slider,0,x+nav_x+5,y+nav_y+38+95,
								1,1,0,colx,1)
								
	draw_set_alpha(1)
	draw_set_color(bg_col)
	draw_rectangle_ratio(x+nav_x-55+scale_amount*113-2,y+nav_y+113-2,
						x+7+nav_x-55+scale_amount*113+2,y+nav_y+20+113+2,false)
	draw_set_color(colx)
	draw_circle_ratio(x+nav_x,y+nav_y+40,20,false)
	draw_rectangle_ratio(x+nav_x-55+scale_amount*113,y+nav_y+113,
						x+7+nav_x-55+scale_amount*113,y+nav_y+20+113,false)
	draw_set_halign(fa_left)

	menuParent_draw_cancel_button()
	