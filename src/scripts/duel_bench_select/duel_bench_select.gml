if(!card_facedown[menupos] || !enemy){

	if(card_array_size > 0){
		var obj = instance_create(50,150,duelSub);
		obj.menu_selected = card_array[menupos];
		obj.parent = id;
		var me = id;
		
		if(duelController.do_context_menu){
			obj.context_menu = true;
			soundfxPlay(sfx_sound_duel_ax_select)
			obj.option_spacer = 40
			// set the coordinates
			menupos = duelController.context_menu_select
		}
		
		with(obj){
			var windowCharPerLine = 30
			if(menu_selected > -1){
				card = card_create(650,290,.5,menu_selected);
				card.card_sprite_ref = obj_data.card_sprite[menu_selected]
				with(card){
					card_update()	
				}
			}
			menu_add_option("View","View this card.",duelSub_view,spr_menu_info)
			
			code_menu_select_option(me);
			
			if(!duelController.code_select_active){
				if(duelController.do_return_overlay
					&& !card_has_keyword(menu_selected,"unfriendly")
					&& !card_has_keyword(me.parent.part[DUEL_FIELD_ACTIVE].card_array[0],"solo")
					){
				
					menu_add_option("Overlay","Overlay this card.",duel_hand_script_overlay_return,spr_menu_overlay)	
				} else {
					
					if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size > 0){
						if(duelController.phase_current == DUEL_PHASE_RETURN){
							if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size > 1){
								if(duel_can_return(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])){
									menu_add_option("Switch","Switch this card out.",duel_bench_swap,-1)
								}
							}
						} else {
							if(duelController.phase_current == DUEL_PHASE_MAIN && me.parent.switch_counter == 0){
								if(duel_can_swap(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])){
									menu_add_option("Switch","Switch this card out.",duel_bench_swap,-1)		
								}
							}
						}

						if(duelController.phase_current == DUEL_PHASE_MAIN){
							if(me.parent.overlay_counter == 0 // you haven't done your normal overlay
								&& me.parent.part[DUEL_FIELD_ACTIVE].card_array_size != 0 // there is at least one active philosopher
								&& duel_can_overlay(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])
								){
									// there is room for an overlay
									if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size < me.parent.part[DUEL_FIELD_ACTIVE].card_limit ){
										menu_add_option("Overlay","Overlay this card.",duel_hand_script_overlay,spr_menu_overlay)	
									}
									// there is no room for an overlay, but there's one overlay
									else if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size == 2){
										menu_add_option("Overlay","Replace overlay card.",duel_hand_script_overlay,spr_menu_overlay)	
									}
									// there is no room for an overlay, but there's more than one overlay
									else if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size == 3){
										menu_add_option("Overlay","Replace overlay card.",duel_hand_script_overlay_override_choose,spr_menu_overlay)	
									}
							}
						} else {
				
						}
					
					} else {
						if(duelController.phase_current != DUEL_PHASE_RETURN){
							if (me.parent.switch_counter == 0){
								if(duel_can_activate_philosopher(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])){
									menu_add_option("Activate","Set as Main Philosopher.",duel_hand_set_main,spr_menu_activate)
								}
							}
						}
					}
				}
			}
			
			//event_user(0)
			menu_height = 20 + option_spacer * menu_count;
			
			y = -menu_height*.5 + 300
			
			//event_user(0)
			if(!duelController.do_context_menu){
				if(menu_count == 1){
					menu_width -= 50;
					x+=30
				}
				if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
					y+=50;
					card.y+=50;
					adjust_me = 50;
					
					with(card){
						card_update()	
					}
				}
				menu_push(id)
			} else {
				duelController_context_menu_adjust();
			}
		}
	}
}