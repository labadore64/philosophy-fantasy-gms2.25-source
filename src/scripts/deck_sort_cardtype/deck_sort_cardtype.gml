var value = argument[0];

if(value > -1){
	if(obj_data.card_type[value] == CARD_TYPE_PHILOSOPHER){
		if(obj_data.card_element[value] == CARD_ELEMENT_MIND){
			return 1	
		} else if(obj_data.card_element[value] == CARD_ELEMENT_SPIRIT){
			return 0	
		}
		return 2;
	} else if(obj_data.card_type[value] == CARD_TYPE_METAPHYS){
		return 3;
	} else{
		return 4
	}
}

return 6;