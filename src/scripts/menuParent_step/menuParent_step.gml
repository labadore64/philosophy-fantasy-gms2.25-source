if(!lock){
if(textbox == noone){

	if(alpha_adder < alpha_max){
		surface_update=true
		alpha_adder += ScaleManager.timer_diff*alpha_rate	
		if(alpha_adder > alpha_max){
			alpha_adder = alpha_max	
		}
	}
	
	if(menuControl.exit_obj < 0 &&
		(object_index == menuHelp || menuControl.help_obj < 0)){
		if(!right_clicked){
			objGenericStepMain();
		}
		
		if(!keypress_this_frame){
			if(global.inputEnabled){
					if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_KEYDISPLAY]) ||
						global.gamepad_state_array[KEYBOARD_KEY_KEYDISPLAY] == KEY_STATE_PRESS) {
							keypress_this_frame = true;
							if(object_index == menuKeysDisplay){
								menuParent_cancel();	
							} else {
								menuKeysDisplay_show();
							}
					}
			}
		}
		if(objGenericCanDo()){
			if(!keypress_this_frame){
				if(!cancel_button_enabled && !right_click_move){
					if(global.drawing_enabled){
					if(mouse_check_button_pressed(mb_right)){
						if(!MouseHandler.clicked){
							if(menu_cancel > -1){
								script_execute(menu_cancel)	
								keypress_this_frame = true;
								with(MouseHandler){
									clicked = true
									clicked_countdown = 5	
								}
							}
						}
					} 
				}
				}
				
				if(enable_scrollwheel && !keypress_this_frame){
					if(mouse_wheel_down()){
						if(menu_up > -1){
							option_top+=scrollwheel_skip;
							if(option_top > menu_count - per_line){
								option_top = menu_count - per_line
							}
							if (option_top < 0){
								option_top = 0	
							}
							keypress_this_frame = true;
						}
					} else if(mouse_wheel_up()){
						if(menu_down > -1){
							option_top-=scrollwheel_skip;
							if (option_top < 0){
								option_top = 0	
							}
							keypress_this_frame = true;
						}
					}
				}
			}
		}
		
		if(!keypress_this_frame){
			mouseHandler_process()
		}
		
		if(global.mouse_active){
			if(mouse_check_button_released(mb_left)){
				with(menuControl){
					with(active_menu){
						if(cancel_pressed){
							script_execute(menu_cancel);	
							cancel_pressed = false
						}
						if(help_pressed){
							help_menu_do()
							help_pressed = false
						}
					}
				}
			}
		}
	}
	
}
}