if(!wait_to_kill){
	if(duelController.click_countdown <= -1){
		duelController.click_countdown = 2
		with(animationMessage){
			instance_destroy()
		}
		with(animationPhaseTransition){
			instance_destroy()
		}
		with(animationVictoryMessage){
			instance_destroy()
		}
		instance_destroy()
	}
}
wait_to_kill=false