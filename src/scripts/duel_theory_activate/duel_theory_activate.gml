var do_camera = true;

with(drawMoveCard){
	do_camera = move_camera;	
}

if(do_camera){
	if(enemy){
		var posx = 500
		var posy = 125


		duel_camera_focus_object(posx,posy,5,1.5);
		duelController.pointer_x = 510+75
		duelController.pointer_y = 120+50
	} else {
		var posx = 150
		var posy = 550


		duel_camera_focus_object(posx,posy,5,1.5);
		duelController.pointer_x = 130+75
		duelController.pointer_y = 550+50
	}
}