#macro MAX_AI_PASS 250
#macro MAX_ACTION_BUFFER_SIZE 15

// this opens up the AI testing space.
// basically, once the MAX_AI_PASS is reached, it prunes the trees.

with(argument[0]){
	// set the action buffer which is used to spawn child instances
	for(var i = 0; i < MAX_ACTION_BUFFER_SIZE; i++){
		ai_card_action_buffer[i] = "";
	}
	var quantity = 0;
	if(object_index == duelHand ||
		object_index == duelWaiting){
		// hand can hold up to the same amount of card in the deck.
		quantity = parent.deck.size_limit;
	} else if(object_index != duelDeck){
		quantity = size_limit;	
	}
	
	for(var i = 0; i < MAX_AI_PASS; i++){ 
		for(var j = 0; j < quantity; j++){
			// represents the location of cards in simulations
			// i = simulation number
			// j = card at that position
			ai_card_array[i,j] = -1
		}
		ai_card_command[i] = ""; // which commands correlate with which simulation.
		// hp variables
		ai_card_hp[i,DUEL_PLAYER_YOU] = 0
		ai_card_hp[i,DUEL_PLAYER_ENEMY] = 0
		
		ai_card_total[i] = quantity;
		
		ai_card_eval[i] = 0; // evaluation value, the higher the better graded the move is.
	}
	
	// set initial state.
	// set initial HP
	ai_card_hp[0,DUEL_PLAYER_YOU] = duelController.duel_hp[DUEL_PLAYER_YOU].hp_current;
	ai_card_hp[0,DUEL_PLAYER_ENEMY] = duelController.duel_hp[DUEL_PLAYER_ENEMY].hp_current;
	
	// this sets the current cards in play
	for(var j = 0; j < card_array_size; j++){
		ai_card_array[0,j] = card_array[j];
	}
	
	// start algorithm
	//ai_new_do(0);

}