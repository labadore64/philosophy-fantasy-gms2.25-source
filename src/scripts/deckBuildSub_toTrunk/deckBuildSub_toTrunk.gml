var cando = false;
with(deckBuild){
	if(deck_total > 1){
		deckBuild_deckToTrunk(deck_display[menupos])

		menupos--;
		if(menupos < 0){
			menupos = 0	
		} else {
			if(selected_draw_index > 0){
				selected_draw_index--;	
			} else {
				option_top--
			}
		}

		deckBuild_setDisplayArrays()

		gamepad_rumble(.5,15)
		with(Rumble){
			rumble_left = 1
			rumble_right = 0
		}
		
		deckBuild_updateSprite();

		soundfxPlay(sfx_sound_deck_remove)
		cando = true
	}
}

event_user(0)