var query = argument[1]
var card = argument[0]

var part1 = ""
var part2 = "";
var part3 = "";
var part_counter = 0;

var literal = false;

var counter = 1;
var len = string_length(query);

var current_bracket = "";
var result = 0;
var counterz = 0;
while(counter <= len){
	
	
	char = string_char_at(query,counter);
	if(char == "\"" && counter != len){
		literal = !literal;
	} else if(char == " " || counter == len){
		if(counter == len && char != " "){
			current_bracket += char;
			literal = false;
		}
		if(!literal){
			if(part_counter == 0){
				part1 = code_card_query_do(card,current_bracket);
				current_bracket = "";
				// if its the last thing and there is no part 2 just set return to part 1
				if(counter == len){
					result = part1
				}
			} else if (part_counter == 1){
				part2 = current_bracket;
				current_bracket = "";
			} else if (part_counter == 2){
				part3 = code_card_query_do(card,current_bracket);
				current_bracket = "";
				result = code_process_expr(part1, part2, part3);
				part1 = result;
				part_counter = 0
			}
			
			part_counter++;
		} else {
			//add to the current bracket
			current_bracket += char;
		}
	} else {
		//add to the current bracket
		current_bracket += char;
	}
	
	counter++;	
	
	counterz++;
	if(counterz > 1000000){
		break;	
	}
}

return result;