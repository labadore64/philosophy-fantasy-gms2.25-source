if(!argument[0]){
	var target_index = -1; // the card index to move
	var target_origin = -1; // the location of the target
	var target_destination = -1; // where to move the target
	var target_side = duelController.active_player; // which side the target is on
	var facedown = false;
	var target = "";

	var block = false;

	var stringer = "";
	for(var i = 0; i < code_argument_size; i++){

		if(string_pos("blockeffect",code_arguments[i]) > 0){
			block=true	
		}
		
		
	
		// parse targets
		if(string_pos("target:",code_arguments[i]) > 0){
			var target = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
		// parse destination
		if(string_pos("dest:",code_arguments[i]) > 0){
			target_destination = string_copy(code_arguments[i],string_pos(":",code_arguments[i])+1,
											string_length(code_arguments[i]) - string_pos(":",code_arguments[i]))
		}
	}

	// selective move
	if(target != ""){

		var flags = power(2,ANIMATION_MOVE_FLAG_CARD_EFFECT)

		duel_animation_code_card_move(target,block,target_destination,flags)
		
	}
}
// do search move here