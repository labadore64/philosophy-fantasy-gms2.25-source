// if you have more than 6 cards, you must discard until you only have 6 cards.
if(instance_exists(field[1])){
	if(field[1].hand.card_array_size > 5){
		duelCamera_center_camera()
		var dotimes = field[1].hand.card_array_size - 5;
		with(field[1].hand){
			repeat(dotimes){
				// for now just randomly select a card to remove
				duel_animation_move_card(coolrandom_inext(card_array_size)-1,id,parent.wait)
			}
		if(card_array_size <= 6){
			duel_animation_center_camera(parent,"enemy");
			//duel_animation_next_phase();
		}
		}
	}


	// end turn animation
	duel_animation_wait(15)
	duel_animation_end_turn()
	duel_animation_wait(15)
}