var index = argument[0];
var source = argument[1]
var pos = argument[2];

var flag = ANIMATION_MOVE_FLAG_DEFAULT
var block = false;
var facedown = false;
if(argument_count > 4){
	block = argument[4]
}
if(argument_count > 3){
	facedown = argument[3]
}
if(argument_count > 5){
	flag = argument[5]	
}

if(duelController.ai_mode){

	// just execute the script
	duel_anim_queue_move_card(index,source,pos,7,facedown,false,block,flag)
	
} else {
	if(!instance_exists(duelController.animation_stack_run) ||
		duelController.animation_stack_run.object_index == animationQueue){
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	
	
		if(queue > -1 && ds_exists(queue,ds_type_list)){
			duelController.running_obj = obj;

			var json = ds_map_create();
			ds_map_add(json,"origin",source)
			ds_map_add(json,"dest",pos)
			ds_map_add(json,"card_id",index)
			ds_map_add(json,"time",7)
			ds_map_add(json,"block",block)
			ds_map_add(json,"facedown",facedown);
			ds_map_add(json,"movecamera",true)
			ds_map_add(json,"flag",flag);
		
			duel_animation_pack(obj,queue,json,duel_anim_queue_move_card)
		}
	}
}