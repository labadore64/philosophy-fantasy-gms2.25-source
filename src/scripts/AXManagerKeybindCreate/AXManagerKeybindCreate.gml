/// @function AXManagerKeybindCreate()
/// @description Creates the Keybind Data for the AX Manager.

global.keybind_map = ds_map_create(); // represents the keyboard keybinds.
global.gamepad_map = ds_map_create(); // represents the controller keybinds.
global.keybind_array = -1; // if not -1, is a pointer to an array that contains all the data in keybind_map in an array, for efficiency
global.keybind_value_array = -1; //if not -1, is a pointer to an array that contains all the values in keybind_map, for efficiency
global.gamepad_array = -1; // if not -1, is a pointer to an array that contains all the data in gamepad_map in an array, for efficiency
global.keybind_value_array = -1; //if not -1, is a pointer to an array that contains all the values in keybind_map, for efficiency
global.keybind_size = 0; // number of keybinds defined.
global.gamepad_size = 0; // number of gamepad bindings defined.

global.gamepad = -1; // the gamepad, if detected will be >= 0

global.key_state_array = -1; // if not -1, is a pointer to an array that contains states for the keybound keyboard keys
global.gamepad_state_array = -1; // if not -1, is a pointer to an array that contains states for the keybound gamepad


// initialize the keybinds
keyboard_bind_init();
gamepad_bind_init();