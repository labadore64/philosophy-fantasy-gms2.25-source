var stringer = argument[0];
var pos = string_pos(",",stringer);

var returner = "";

while(pos != 0){
	var pos = string_pos(",",stringer);
	returner += keyword_get_string(string_copy(stringer,1,pos-1));
	stringer= string_delete(stringer,1,pos);
	if(pos == 0){
		returner += keyword_get_string(string_copy(stringer,1,pos-1));
		stringer= "";
		break;	
	}
}

return returner;