if(active){
	draw_set_color(c_black)
	draw_set_alpha(1)	

	if(global.display_y == 0){
		draw_rectangle(0,0,global.display_x,global.window_height,false)
		draw_rectangle(global.window_width-global.display_x,0,global.window_width,global.window_height,false)
	} else if (global.display_x == 0){
		draw_rectangle(0,0,global.window_width,global.display_y,false)
		draw_rectangle(0,global.window_height-global.display_y,global.window_width,global.window_height,false)
	}
}