// draw color selection list
var counter = 0;

for(var i = option_top; i < option_top+per_line; i++){
	
	var size = option_spacer-40
	
	var x_start = 200;
	var y_start = option_y_offset+10;
	var len = 275


	draw_set_font(global.normalFont);
	draw_set_color(global.text_fgcolor)
	draw_set_alpha(1)
	draw_text_transformed_noratio(15,option_y_offset+ option_spacer*counter,color_title[i],2,2,0)
	draw_text_transformed_noratio(15,option_y_offset+30+ option_spacer*counter,"#" + color_hex[i],2,2,0)
	draw_text_transformed_noratio(15,option_y_offset+58+ option_spacer*counter,"RBG: " + color_rgb[i],1,1,0)
	draw_text_transformed_noratio(15,option_y_offset+73 + option_spacer*counter,"HSV: " + color_hsv[i],1,1,0)

	draw_set_color(color_id[i]);
	draw_rectangle_noratio(x_start,y_start+option_spacer*counter,
					x_start+size,size+y_start+option_spacer*counter,false)
	counter++
}