//this function forces the text to be adjusted manually
//Note: modified from code from Diestware's textbox asset. Does not include any of the textbox/drawing stuff from his asset!

var stringer = string_replace_all(argument[0],"\n","~");
var lenger = argument[1];

var returner = "";
var string_size = string_length(stringer);

var linecounter = 0;

var char = "";

for(var i = 1; i <= string_size; i++){
	char = string_char_at(stringer,i);
	if(char == "~"){
		returner +="\n";
		linecounter = 0;
	} else {
	
		returner += char;
		linecounter++;
	
		if(linecounter > lenger){
			if(char != " "){
				while(char != " " ||
					linecounter == 0){
					linecounter--;
					returner = string_delete(returner,i,1)
					i--;
					char = string_char_at(stringer,i)
				}
				if(linecounter == 0){
				
					returner+=string_copy(stringer,i,linecounter);
					i+=linecounter;
				}
			}
			returner = string_delete(returner,i,1)
			returner+= "\n"
		
			linecounter = 0;
		}
	}
}

return returner;