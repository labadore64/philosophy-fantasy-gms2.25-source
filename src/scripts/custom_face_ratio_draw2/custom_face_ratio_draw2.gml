var scale = argument[0]
var xx = argument[1]
var yy = argument[2]

var skin_color = argument[3];
var eye_color = argument[4];
var hair_color = argument[5];

var mouth_color = argument[6];
var snout_color = argument[13]

draw_sprite_extended_ratio(spr_faces_face,argument[7],scale*(256+xx),scale*(256+yy),scale,scale,0,skin_color,1)
draw_sprite_extended_ratio(spr_faces_nose,argument[8],scale*(256+xx),scale*(256+yy),scale,scale,0,skin_color,1)
draw_sprite_extended_ratio(spr_faces_mouth,argument[9],scale*(256+xx),scale*(256+yy),scale,scale,0,mouth_color,1)
draw_sprite_extended_ratio(spr_faces_eyes_white,argument[10],scale*(256+xx),scale*(256+yy),scale,scale,0,c_white,1)
draw_sprite_extended_ratio(spr_faces_eyes,argument[10],scale*(256+xx),scale*(256+yy),scale,scale,0,eye_color,1)
draw_sprite_extended_ratio(spr_faces_eyebrows,argument[11],scale*(256+xx),scale*(256+yy),scale,scale,0,hair_color,1)

draw_sprite_extended_ratio(spr_faces_snout1,argument[12],scale*(256+xx),scale*(256+yy),scale,scale,0,skin_color,1)
draw_sprite_extended_ratio(spr_faces_snout2,argument[12],scale*(256+xx),scale*(256+yy),scale,scale,0,snout_color,1)