// replaces a single string in the hud controller
var stringer = argument[0];
var map = argument[1];

var replacer = -1;

if(argument_count > 2){
	replacer = argument[2];	
}

var alt_text = false;

if(argument_count > 3){
	alt_text = argument[3]
}

var tempstring;

var str_size = string_length(stringer);
var position = string_pos("{{",stringer);
var initpos = position;
if(position > 0){
	while(position <= str_size && position > 0){
		while(!(
				string_char_at(stringer,position) == "}" &&
				string_char_at(stringer,position+1) == "}"
			)){
			position++;
		}

		var val = string_replace_all(string_copy(stringer,initpos+2,(position-initpos-2))," ","");

		var diagram = objdataToIndex(objdata_diagram,val);
		var replacestring = " ";
		if(diagram > -1){
			if(global.voice || alt_text){
				if(alt_text){
					replacestring = objdata_diagram.mini_text[diagram]
				} else {
					replacestring = objdata_diagram.alt_text[diagram]
				}
			} else {
				if(ds_exists(map,ds_type_map)){
					ds_map_replace(map,initpos,diagram)
				}
			}
		}
		stringer=string_delete(stringer,initpos,((position-initpos+2)));
		stringer=string_insert(replacestring,stringer,initpos);
		var sizer = string_length(replacestring);
			
		// do fixing if replacer > -1
		if(!alt_text){
			if(replacer > -1){
				// save what is before the replaced value
				tempstring = string_copy(stringer,1,initpos+sizer);
				
				// take everything else
				var rest = string_replace(stringer,tempstring,"");
				
				// now we need to know how many characters are on the last line.
				var counter = string_length(tempstring);
				var poss = 4;
				if(string_char_at(tempstring,counter) == "\n"){
					poss = replacer
				} else {
					while(string_char_at(tempstring,counter) != "\n"){
						poss++;	
						counter--
					}
				}
				
				var stri = string_copy(rest,1,string_length(rest));
				var pos = string_pos("\n",stri)
				// copy the first part of the string
				tempstring +=string_copy(stri,1,pos-1);
				stri = string_delete(stri,1,pos);
				rest = string_delete(rest,1,pos);
				
				// how many characters to grab
				var diff = replacer-poss;
				
				var newlines = string_copy(stri,1,diff);
				var len = string_length(newlines);
				if(string_char_at(newlines,len) != "\n" &&
					string_char_at(newlines,len) != " " &&
					string_char_at(newlines,len) != "." &&
					string_char_at(newlines,len) != "," &&
					string_char_at(newlines,len) != ":" &&
					string_pos(" ",stri) > 0){
				
					// keep removing until string is empty or
					// you have a space
					while(string_length(newlines) > 0 &&
						string_char_at(newlines,string_length(newlines)) != " "){
						if(string_length(newlines) == 0){
							
							newlines = "";	
						} else {
							newlines = string_delete(newlines,string_length(newlines),1);	
						}
					}
				}
				
				var counter = string_length(newlines);
				
				if(counter == string_length(rest)){
					rest = "";
				} else {
					rest = string_copy(rest,counter,string_length(rest)-counter+1);
				}
				
				if(string_char_at(rest,1) == " "){
					rest = string_delete(rest,1,1)	
				}
				
				if(string_char_at(tempstring,string_length(tempstring)) != "\n"){
					tempstring += " "
				}
				
				var lenz = string_length(tempstring);
				
				if(string_char_at(tempstring,lenz) == "\n"){
					tempstring = string_copy(tempstring,1,lenz-1);	
				}
				
				tempstring += newlines
				// now the temp string is corrected.
				// correct the rest of the string
				
				var fixstring = string_replace_all(string_replace_all(rest,"\n\n","~~"),"\n"," ");
				
				stringer = tempstring + "\n" + textForceCharsPerLine(fixstring,replacer);
				stringer = string_replace_all(stringer,"~~","\n\n");
			}
		}
			
		position = string_pos("{{",stringer);
		initpos = position;
		str_size = string_length(stringer);
	}
}
	
if(ds_exists(map,ds_type_map)){
	diagram_length = ds_map_size(map)
}


return stringer;