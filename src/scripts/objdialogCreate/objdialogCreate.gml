

/// @description HOW TO USE
//diestware.tumblr.com

/*///////////////////////////////
////      Start a chat      /////
/////////////////////////////////

// First create an array with your desired messages. for example:
message[0] = "Hello this is the first message!";
message[0] = "This is the second message!";

// Secondly, if you have a portrait sprite, write the name of the sprite
// as the second parameter. The default sprite size is 50x50.
// If you don't want a portrait just write "none"

// Then when you're ready call this script to start the chat in game:
newChat(message, portrait);

*/




/*///////////////////////////////
////    Effects in chat     /////
/////////////////////////////////

// If you'd like to have effects in your message then you must put the
// correct modifier code. For example:
message[0] = "\4Wavy text";

// The modifier code is "\" followed by the number for the corresponding effect.
// The modifiers are as follows:
\0 - Normal text
\1 - Shaky text
\2 - Color text
\3 - Color & shake text
\4 - Wavy text
\5 - Gradient text
\6 - Gradiest & wavy text

// We can mix and match the effects by doing the following:
message[0] = "\0Normal \1Shaky \2Color \4Wavy";

*/




/*///////////////////////////////
////      Editing Chat      /////
/////////////////////////////////

// WARNING
// Not recommended unless you're an advanced user

// Go to the listed line number in the Draw GUI event to edit

// To move around the chat box go to line 19
// To move around the portrait go to line 21
// To move around the text starting position go to line 38 for the Y and 40-41 for the X
// To edit the chat sound go to line 75
// To edit any of the modifiers they start at line 109
// To add a new modifier add a new case in the switch statement starting at line 109
// To edit how many characters fit on each line go to line 28-29
// To edit space between characters go to line 26
// To edit time between characters go to line 35

*/

/* */
///Initialize Variables
is_monster = false;

message_current = 0;
timer = 0;
cutoff = 0
portrait = -1;
text_scale = 2;
my_color = c_yellow;

fontz = global.textFont

t = 0;
//Amplitude & Freq for Wavy Text
amplitude = 3;
freq = 2.5;

done = false;
depth = -10000000
message[0] = "default"
var mecolor = my_color

continue_draw = true;

my_color = mecolor;
message_end = 1
string_lenger = 0;
length = 0;
modifier_last = 0;

//index of message
for(var k =0; k < 2; k++){
	//index of character
	for(var j = 0; j < 300;j++){
		char_array[k,j] = "";
	}
}
newalarm[0] = 1
random_this_step = false;
random_val = 0;
random_valy = 0;
jitter_amount = 1;

i_value_mod = 0;
i_value_mody = 0;

sprite_counter = 0;

//modifier_last = 0;

/* */
/*  */
    //This is for the effects
    modifier = 0;
    //Character Width
    charWidth = 7*text_scale;
    //Variables for spacing characters
    line = 0;
    space = 0;
    i = 1;
	line_spacer = 35
	
   
    
    //Text Position
	if(global.text_full && room != BattleRoom){
	    //How many characters allowed in each line
	    lineEnd = 25;
		tY = 150
	} else {
	    //How many characters allowed in each line
	    lineEnd = 42;
		tY = 495;
	}
    //Change the horizontal position if there is a portrait
    //if (portrait == "none") var tX = 10;sfdsfsd
    tX = x;
    //Delay time between printing each character
    delay = global.textSpeed;
	
surf = -1;
background_alpha = 1;

surface_draw = false;
finish_draw = false;

clear_surf = false;

last_line = 0;
last_space = 1;

last_message = 0;

text_sound1 = -1;
text_sound2 = -1;
text_sound3 = -1;
text_sound4 = -1;

current_sound = -1;

text_mod_sound = false; //whether or not current sound should mod in pitch

flux_rate = 1;
flux = 1;
pitch_base = 1;

talking_sound = true;

force_draw = false;

no_progress = false;