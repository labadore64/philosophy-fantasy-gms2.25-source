var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

	//menuParentDrawBox();
	
if(!surface_exists(surf)){
	surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	surface_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h);
		surface_update = true;
	}
}

if(surface_update){
	surface_set_target(surf)
	draw_set_halign(fa_left)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear(c_black)
	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_color(col)
	draw_set_halign(fa_center)
	draw_text_transformed_noratio(400,30-7,title,4,4,0)
	draw_set_halign(fa_left)
	//draw_set_color(subtitle_color)

	gpu_set_colorwriteenable(true,true,true,true)
	
	draw_set_halign(fa_left)
	menuBookDrawText();
	menuBookDrawDiagrams();
	gpu_set_colorwriteenable(true,true,true,true)
	surface_reset_target();
	surface_update = false;
}
	
draw_surface(surf,global.display_x,global.display_y);
	
menuParent_draw_cancel_button()
	
draw_letterbox();