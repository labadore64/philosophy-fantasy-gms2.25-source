var trailer = argument[0]; // trailing tester
var test = argument[1]; //which value you want to check
while (test > 0)
{
  trailer = floor(trailer / 2);
  test--;
}
return (trailer & 1) == 1;