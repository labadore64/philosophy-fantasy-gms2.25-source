	var arg = string_lower(argument[0]);
	
	if(arg != "none"){
		var located = false;
		for(var i = 0; i < archetype_count; i++){
			if(archetype_name[i] == arg){
				located = true;
				break;
			}
		}
	
		if(!located){
			archetype_name[archetype_count] = arg;
			archetype_count++;
		}
	}