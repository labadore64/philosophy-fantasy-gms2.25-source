// argument[0] = argument list

if(!argument[0]){
	with(duelController){

		var target_index = -1; // the card index to move
		var target_origin = -1; // the location of the target
		var target_destination = -1; // where to move the target
		var target_side = duelController.active_player; // which side the target is on
		var test_value = "";

		var this = code_get_this();
		var this_enemy = false;
		if(string_pos("pos_enemy",this) > 0){
			this_enemy = true;
		}

		var stringer = "Activate &this's effect?"

		// custom message
		if(code_argument_size > 0){
			if(code_arguments[0] != ""){
				stringer = code_arguments[0];
				if(string_char_at(stringer,1) == "\""){
					stringer = string_copy(stringer,2,string_length(stringer)-2);	
				}
			}
		}
	
		// get this
	
		var stringe = this;
		var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
		var side = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));


		if(this_enemy){
			target_side = DUEL_PLAYER_ENEMY	
		} else {
			target_side = DUEL_PLAYER_YOU	
		}

	
		// if the code is a location, parse as location.
		if(code_is_location(ori)){
			target_index = string_copy(stringe,string_pos(":",stringe)+1,
										string_length(stringe) - string_pos(":",stringe))
			
			// string == variable
			if(string_length(string_digits(target_index)) != string_length(target_index)){
				var value = duelController.code_values[? target_index];
				if(is_undefined(value) || !is_real(value)){
					value = 0;	
				}
				
				target_index = value;
			} else {
				target_index = real(target_index);	
			}
				
			if(ori == "overlay"){
				target_index++;	
			}
				
			target_origin = code_get_location(ori,target_side)
		} 
	
		if(target_index > -1){
			var card_id = target_origin.card_array[target_index];
	
			if(card_id > -1){
				// replace variables
				stringer = string_replace_all(stringer,"&this",obj_data.card_name[card_id]);
			}
	
		}

		// now that you have the display string, trigger the animation
		duel_animation_choice(stringer,target_side); 
	}

}