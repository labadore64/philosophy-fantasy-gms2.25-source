var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

with(draw_obj){
	script_execute(menu_draw)	
}

draw_set_color(c_black)
draw_set_alpha(.75)
draw_rectangle_ratio(0,0,800,600,false)

	gpu_set_colorwriteenable(true,true,true,true)

	draw_set_alpha(1)
	draw_set_font(global.normalFont)
	draw_set_alpha(1)
	draw_set_color(c_white)
	if(global.high_contrast){
		draw_rectangle_ratio(x-4,y-4,x+4+menu_width,y+4+menu_height,false)
	} else {
		
	
		if(global.color_gradient){
			draw_rectangle_color_ratio(x-6,y-6,x+6+menu_width,y+6+menu_height,
			global.menu_grcolor[0],global.menu_grcolor[0],
			global.menu_grcolor[1],global.menu_grcolor[1],
			false)
		
		} else {
			draw_set_color(global.menu_grcolor[0])
			draw_rectangle_ratio(x-6,y-6,x+6+menu_width,y+6+menu_height,
			global.menu_grcolor[0],global.menu_grcolor[0],
			global.menu_grcolor[1],global.menu_grcolor[1],
			false)	
		}
	
	}
	
	//gpu_set_colorwriteenable(true,true,true,false)
	if(global.high_contrast){
		draw_set_color(c_black)
	} else {
		draw_set_color(global.menu_bgcolor)
	}
	draw_rectangle_ratio(x,y,x+menu_width,y+menu_height,false)

	draw_set_color(col)

	draw_set_halign(fa_left)
	gpu_set_colorwriteenable(true,true,true,false)
	
	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(x + draw_width*.5,y+25,title,4,4,0)
	draw_set_halign(fa_left)
	//draw_set_color(subtitle_color)

	gpu_set_colorwriteenable(true,true,true,true)
	
	draw_set_color(c_white);
	
	// draw actual options
	var counter = 0;
	for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
	gpu_set_colorwriteenable(true,true,true,false)
		if(counter == selected_draw_index){
			draw_set_color(c_white)
			if(global.menu_animate){
				draw_set_alpha(alpha_adder);
			} else {
				draw_set_alpha(alpha_max)
			}
			if(global.high_contrast){
				draw_rectangle_ratio(10+x,option_y_offset+y+3-10 + option_spacer*counter,
							x+menu_width-10,option_y_offset+y-3-10 + option_spacer*(counter+1),
							false)
			} else {
				draw_rectangle_color_ratio(10+x,option_y_offset+y+3-10 + option_spacer*counter,
							x+menu_width-10,option_y_offset+y-3-10 + option_spacer*(counter+1),
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
				false)
			}
		}
		
		draw_set_color(col)
		draw_set_alpha(1)
		draw_text_transformed_ratio(10+x+10,-3+option_y_offset+y + option_spacer*counter,
							string_copy(menu_name[i],1,30),2,2,0)
		counter++;
	}
	gpu_set_colorwriteenable(true,true,true,true)

	draw_set_alpha(1)
	draw_set_color(c_white);
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)

	gpu_set_colorwriteenable(true,true,true,true)

	//menuParentDrawInfo();
	draw_set_halign(fa_left)


draw_set_alpha(1)

menuParent_draw_cancel_button()
	
draw_letterbox();