var fullstring = argument[0];
var cond_mode = argument[1];
var is_ai = false;

if(argument_count > 2){
	is_ai = argument[2]	
}
// do special cases

if(string_pos("if",fullstring) == 1){
	// do if condition.
	var if_condition = "";
	var then_condition = "";
	var else_condition = "";
	
	var start_pos = string_pos("(",fullstring);
	var end_pos = string_pos(")",fullstring);
	
	if_condition = string_copy(fullstring,start_pos+1,end_pos-start_pos-1);
	fullstring = string_delete(fullstring,1,end_pos+1);
	
	start_pos = string_pos("(",fullstring);
	end_pos = string_pos(")",fullstring);
	
	then_condition = string_copy(fullstring,start_pos+1,end_pos-start_pos-1);
	fullstring = string_delete(fullstring,1,end_pos+1);
	
	start_pos = string_pos("(",fullstring);
	end_pos = string_pos(")",fullstring);
	
	if(start_pos > 0){
		else_condition = string_copy(fullstring,start_pos+1,end_pos-start_pos-1);
	}

	duel_animation_code_condition(if_condition,then_condition,else_condition,is_ai);

} else {

	var startfunc = string_pos("(",fullstring);
	var endfunc = string_length(fullstring)-2-startfunc;

	var funcName = string_copy(fullstring,1,startfunc-1);
	var args = string_copy(fullstring,startfunc+1,endfunc);

	var script = asset_get_index("duel_code_" + funcName);

	if(script > -1){
		code_populate_args(args)
		script_execute(script,cond_mode,is_ai);	
	}
}