var charperline = 20;

if(argument_count > 3){
	charperline = argument[3]
}
draw_set_font(global.normalFont)
tooltip[tooltip_total] = instance_create(argument[0],argument[1],menuTooltip);
tooltip[tooltip_total].text = textForceCharsPerLineKeepNewline(argument[2],charperline);
tooltip[tooltip_total].height = 20 + string_height(tooltip[tooltip_total].text)*2;
tooltip[tooltip_total].width = 20 + string_width(tooltip[tooltip_total].text)*2;

tooltip_total++;

return tooltip[tooltip_total-1]