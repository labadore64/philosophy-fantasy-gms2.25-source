for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
	property_part_count[i] = 1	
}

// names
property_names_id[CHARA_CUSTOM_VAL_CUTE] = "Background"
property_names_id[CHARA_CUSTOM_VAL_BACKHAIR] = "Back Hair"
property_names_id[CHARA_CUSTOM_VAL_BODY]= "Body"
property_names_id[CHARA_CUSTOM_VAL_NECK]= "Neck"
property_names_id[CHARA_CUSTOM_VAL_SHIRT]= "Shirt"
property_names_id[CHARA_CUSTOM_VAL_NECKLACE] = "Necklace"
property_names_id[CHARA_CUSTOM_VAL_FACE]= "Face"
property_names_id[CHARA_CUSTOM_VAL_NOSE]= "Nose"
property_names_id[CHARA_CUSTOM_VAL_MOUTH]= "Mouth"
property_names_id[CHARA_CUSTOM_VAL_EYE]= "Eyes"
property_names_id[CHARA_CUSTOM_VAL_EYEBROW] = "Eyebrows"
property_names_id[CHARA_CUSTOM_VAL_GLASSES] = "Glasses"
property_names_id[CHARA_CUSTOM_VAL_EAR] = "Ears"
property_names_id[CHARA_CUSTOM_VAL_EARRING] = "Earrings"
property_names_id[CHARA_CUSTOM_VAL_FRONTHAIR] = "Front Hair"
property_names_id[CHARA_CUSTOM_VAL_HAT] = "Hat"
property_names_id[CHARA_CUSTOM_VAL_SNOUT] = "Snout"

// default color
property_default_color[CHARA_CUSTOM_VAL_CUTE,0] = c_white
property_default_color[CHARA_CUSTOM_VAL_BACKHAIR,0] = CHARA_CUSTOM_COL_HAIR
property_default_color[CHARA_CUSTOM_VAL_BODY,0] = CHARA_CUSTOM_COL_SKIN
property_default_color[CHARA_CUSTOM_VAL_NECK,0] = CHARA_CUSTOM_COL_SKIN
property_default_color[CHARA_CUSTOM_VAL_SHIRT,0] = CHARA_CUSTOM_COL_SHIRT
property_default_color[CHARA_CUSTOM_VAL_NECKLACE,0] = CHARA_CUSTOM_COL_NECKLACE
property_default_color[CHARA_CUSTOM_VAL_FACE,0] = CHARA_CUSTOM_COL_SKIN
property_default_color[CHARA_CUSTOM_VAL_NOSE,0] = CHARA_CUSTOM_COL_SKIN
property_default_color[CHARA_CUSTOM_VAL_MOUTH,0] = CHARA_CUSTOM_COL_MOUTH
property_default_color[CHARA_CUSTOM_VAL_EYE,0] = c_white
property_default_color[CHARA_CUSTOM_VAL_EYE,1] = CHARA_CUSTOM_COL_EYE
property_default_color[CHARA_CUSTOM_VAL_EYEBROW,0] = CHARA_CUSTOM_COL_HAIR
property_default_color[CHARA_CUSTOM_VAL_GLASSES,0] = CHARA_CUSTOM_COL_GLASSES
property_default_color[CHARA_CUSTOM_VAL_EAR,0] = CHARA_CUSTOM_COL_SKIN
property_default_color[CHARA_CUSTOM_VAL_EARRING,0] = CHARA_CUSTOM_COL_EARRING
property_default_color[CHARA_CUSTOM_VAL_FRONTHAIR,0] = CHARA_CUSTOM_COL_HAIR
property_default_color[CHARA_CUSTOM_VAL_HAT,0] = CHARA_CUSTOM_COL_HAT
property_default_color[CHARA_CUSTOM_VAL_SNOUT,0] = CHARA_CUSTOM_COL_SKIN
property_default_color[CHARA_CUSTOM_VAL_SNOUT,1] = CHARA_CUSTOM_COL_SNOUT

appearance_model_readFile();

var sizer = array_length_1d(property_names_id)

for(var i = 0; i < sizer; i++){
	property_part_count[i] = array_length_2d(property_part_names,i);
}


portrait_property_total = array_length_1d(property_names);

for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
	for(var j = 0; j < property_part_count[i]; j++){
		property_enabled[i] = false
		if(j > 0){
			property_cando[i,j] = false
		} else {
			property_cando[i,j] = true	
		}
	}
}

// for the following property_part_count, enable every part. This is for shit like eyes, ears ect
var i = CHARA_CUSTOM_VAL_BACKHAIR;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_BODY;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_NECK;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_FACE;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_NOSE;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_MOUTH;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_EYE;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_EYEBROW;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_EAR;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}
i = CHARA_CUSTOM_VAL_FRONTHAIR;
for(var j = 1; j < property_part_count[i]; j++){
	property_cando[i,j] = true;	
	property_enabled[i] = true
}