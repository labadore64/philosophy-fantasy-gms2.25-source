with(argument[0]){
/// @description Insert description here
// You can write your code in this editor

	if(!surface_exists(surf)){
		surf = surface_create(card_width*global.scale_factor,card_height*global.scale_factor);	
		surface_update = true
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,card_width*global.scale_factor,card_height*global.scale_factor)	
			surface_update = true
		}
	}

	if(surface_update){
		draw_set_halign(fa_left)
		surface_set_target(surf)
		gpu_set_colorwriteenable(true,true,true,true)
		draw_sprite_extended_noratio(bg_sprite,0,30,20,1.10,1.10,0,c_white,1)
		draw_sprite_extended_noratio(obj_data.card_sprite[card_sprite],0,30,20,1.10,1.10,0,c_white,1)
		draw_sprite_extended_noratio(sprite_index,0,0,0,1,1,0,image_blend,1)
		
		// text
		gpu_set_colorwriteenable(true,true,true,false)
		draw_sprite_extended_noratio(type_sprite,0,250,360,8,8,0,c_black,.75)
		
		
		gpu_set_blendmode(bm_add)
		draw_set_font(global.largeFont)
		draw_set_color($AAAAAA)
		draw_text_transformed_noratio(45,25,"Starter Deck",1.5,1.5,0)
		draw_set_font(global.normalFont)
		draw_set_halign(fa_center)
		draw_text_transformed_noratio(170,250-7,name,3,3,0)
		draw_text_transformed_noratio(170,360-7-15,subtitle,2,2,0)
		draw_set_halign(fa_left)
		
		gpu_set_blendmode(bm_normal)
		gpu_set_colorwriteenable(true,true,true,true)
		surface_reset_target();
		surface_update = false;
	}

	draw_surface_ext(surf,global.display_x+x*global.scale_factor,global.display_y+y*global.scale_factor,scale,scale,0,c_white,1);

}