
var json = ds_map_create();

// add enemy details here.
demo_get_duelists()

var deck = ds_list_create();

duelController_enemyDeck(duelist_deck,deck)

ds_map_add(json,"channel",fave);
ds_map_add(json,"chara",obj_data.loaded_opponent);
ds_map_add(json,"type","comdefault");
ds_map_add(json,"sprite",sprite_get_name(duelist_sprite));
ds_map_add(json,"name",duelist_name);
ds_map_add(json,"money",money_paid);
ds_map_add(json,"item",item_dropped);
ds_map_add_list(json,"deck",deck);

var json_string = json_encode(json);

ds_map_destroy(json);
ds_list_destroy(deck);

return json_string;