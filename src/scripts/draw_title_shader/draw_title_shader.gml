

if(!surface_exists(title_surface)){
	title_surface = surface_create_access(global.window_width,global.window_height)	
} else {
	if(ScaleManager.updated){
		surface_resize(title_surface,global.window_width,global.window_height);
	}
}

//SHADER SETUP

	surface_set_target(title_surface)
	
	draw_clear_alpha(c_white,0)
	
	draw_sprite_extended_ratio(sprite_index,title_anim,0,-15,1,1,0,c_white,1)
	
	surface_reset_target();
	
	
    // Display setup ---------------------------------------------------------------------------
	shader_set(shd_ripple);
	    shader_set_uniform_f(con_uni_time, con_var_time_var);
	    shader_set_uniform_f(con_uni_mouse_pos, global.window_width*.5,global.window_height*.5);
	    shader_set_uniform_f(con_uni_resolution, global.window_width,global.window_height);
	    shader_set_uniform_f(con_uni_wave_amount, con_var_wave_amount);
	    shader_set_uniform_f(con_uni_wave_distortion, con_var_wave_distortion );
	    shader_set_uniform_f(con_uni_wave_speed, con_var_wave_speed);

        draw_surface(title_surface,0,0);
    shader_reset();
