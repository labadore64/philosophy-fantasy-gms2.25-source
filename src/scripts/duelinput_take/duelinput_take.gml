var index = argument[0]
				
with(duelController){
	var returner = input_queue[|index];	
	ds_list_delete(input_queue,index);
	return returner;
}