// set up array. by default all keys are restricted from each other
for(var i = 0; i < KEYBOARD_KEY_TOTAL; i++){
	for(var j = 0; j < KEYBOARD_KEY_TOTAL; j++){
		global.keybind_restrict[i,j] = true;
	}
}

// set up definitions
// format:
// global.keybind_restrict[KEY,KEY_TO_COMPARE]
// if true, it means the two cannot share the same binding.

// outside of the exception keys, these keys
// are basically bindable to anything.
for(var i = 0; i < KEYBOARD_KEY_TOTAL; i++){

	

		// these keys can be bound to anything that is higher than the Open Menu key
		if(i > KEYBOARD_KEY_SELECT2){
			keybind_restrict_value(i,KEYBOARD_KEY_SPEED,false)
			keybind_restrict_value(i,KEYBOARD_KEY_KEYDISPLAY,false)
			keybind_restrict_value(i,KEYBOARD_KEY_SORT,false)
		}
		
		// cam keys cant be bound to each other
		// access keys and function keys can't be bound to each other
		if((i >= KEYBOARD_KEY_CAMERA_DOWN && i <= KEYBOARD_KEY_CAMERA_UP) ||
			i >= KEYBOARD_KEY_CAMERA_LEFT && i <= KEYBOARD_KEY_CAMERA_RIGHT){
			for(var j = KEYBOARD_KEY_CAMERA_DOWN; j <= KEYBOARD_KEY_CAMERA_RIGHT; j++){
				if(j != KEYBOARD_KEY_KEYDISPLAY){
					keybind_restrict_value(i,j,true)	
				}
			}
			for(var j = KEYBOARD_KEY_CAMERA_DOWN; j <= KEYBOARD_KEY_CAMERA_RIGHT; j++){
				if(j != KEYBOARD_KEY_KEYDISPLAY){
					keybind_restrict_value(i,j,true)	
				}
			}
		}
		
		// access keys and function keys can't be bound to each other
		if((i >= KEYBOARD_KEY_ACCESS1 && i <= KEYBOARD_KEY_ACCESS0) ||
			i >= KEYBOARD_KEY_EXIT && i <= KEYBOARD_KEY_SCREENSHOT){
			for(var j = KEYBOARD_KEY_ACCESS1; j <= KEYBOARD_KEY_ACCESS0; j++){
				keybind_restrict_value(i,j,true)	
			}
			for(var j = KEYBOARD_KEY_EXIT; j <= KEYBOARD_KEY_SCREENSHOT; j++){
				keybind_restrict_value(i,j,true)	
			}
		}
}
	
