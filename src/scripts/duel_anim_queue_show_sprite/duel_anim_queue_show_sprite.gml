var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _sprite = map[? "sprite"];
	var _x = map[? "x"];
	var _y = map[? "y"];
	var _x_scale = map[? "xscale"];
	var _y_scale = map[? "yscale"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_sprite) &&
		!is_undefined(_x) &&
		!is_undefined(_y) &&
		!is_undefined(_x_scale) &&
		!is_undefined(_y_scale)){
			
		var obj = instance_create(0,0,animationSprite);
		obj.sprite_index = _sprite;
		obj.x = _x
		obj.y = _y;
		obj.image_xscale = _x_scale;
		obj.image_yscale = _y_scale;
		obj.frame_count = sprite_get_number(obj.sprite_index)
		
		return obj;
	}

}

return noone;