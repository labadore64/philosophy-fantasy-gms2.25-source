if(!ds_list_empty(obj_data.current_deck)){
	var cards = ds_map_create();
	var lee = ds_map_create();
	var eve = ds_map_create();
	
	for(var i = 0; i < obj_data.cards_total; i++){
		ds_map_add(cards,i,obj_data.card_in_trunk[i]);
	}
	
	for(var i = 0; i < obj_data.opponents_total; i++){
		ds_map_add(lee,i,obj_data.opponent_encounter[i]);
	}
	
	for(var i = 0; i < obj_data.event_total; i++){
		ds_map_add(eve,i,obj_data.event_set[i]);
	}


	var savefile = ds_map_create();
	
	file_delete("cards.bin")
	
	ds_map_add_map(savefile,"cards",cards);
	var list1 = ds_list_create();
	ds_list_copy(list1,obj_data.events)
	ds_map_add_list(savefile,"events",list1)
	// ds_map_add_map(savefile,"events",eve);
	var list = ds_list_create();
	ds_list_copy(list,obj_data.current_deck)
	
	ds_map_add_list(savefile,"deck",list);
	ds_map_add(savefile,"main",obj_data.current_main_card)
	
	ds_map_add(savefile,"story",obj_data.story_counter);
	
	ds_map_add(savefile,"wins",obj_data.wins);
	ds_map_add(savefile,"losses",obj_data.losses);
	ds_map_add(savefile,"draws",obj_data.draws);
	
	ds_map_add(savefile,"savescum",obj_data.savescum);
	
	ds_map_add(savefile,"knowledge", global.max_knowledge)

	ds_map_add_map(savefile,"duel_history",lee)

	ds_map_secure_save(savefile,"cards.bin")

	ds_map_destroy(savefile);

	ds_map_destroy(cards);
	
	if(ds_exists(list,ds_type_list)){
		ds_list_destroy(list)	
	}
	if(ds_exists(list1,ds_type_list)){
		ds_list_destroy(list1)	
	}
	if(ds_exists(lee,ds_type_map)){
		ds_map_destroy(lee)	
	}
	if(ds_exists(lee,ds_type_map)){
		ds_map_destroy(eve)	
	}
	
	// do cardpacks
		
	var savefile = ds_map_create();
	
	file_delete("packs.bin")
	var list_of_lists = ds_list_create();
	for(var i = 0; i < obj_data.packs_total; i++){
		var this = ds_list_create();
		ds_list_add(list_of_lists,this);
		for(var j = 0; j < obj_data.pack_card_total[i]; j++){
			ds_list_add(this,obj_data.pack_amount[i,j]);
		}
		
		ds_map_add_list(savefile,string(i),this);
	}

	ds_map_secure_save(savefile,"packs.bin")

	ds_map_destroy(savefile);
	for(var i = 0; i < obj_data.packs_total; i++){
		if(ds_exists(list_of_lists[|i],ds_type_list)){
			ds_list_destroy(list_of_lists[|i]);	
		}
	}
	
	ds_list_destroy(list_of_lists);

}