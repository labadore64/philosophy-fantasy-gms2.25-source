if(!duelController.ai_mode){
	var sprite = argument[0]
	var x_pos = 0;
	var y_pos = 0;
	var x_scale = 1;
	var y_scale = 1;

	if(argument_count > 1){
		x_pos = argument[1]
	}
	if(argument_count > 2){
		y_pos = argument[2]
	}
	if(argument_count > 3){
		x_scale = argument[3];
	}
	if(argument_count > 4){
		y_scale = argument[4]
	}


	if(!instance_exists(duelController.animation_stack_run) ||
		duelController.animation_stack_run.object_index == animationQueue){
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
		if(queue > -1 && ds_exists(queue,ds_type_list)){
			duelController.running_obj = obj;
	
			var json = ds_map_create();
			ds_map_add(json,"sprite",sprite)
			ds_map_add(json,"xscale",x_scale)
			ds_map_add(json,"yscale",y_scale)
			ds_map_add(json,"x",x_pos)
			ds_map_add(json,"y",y_pos)
		
			duel_animation_pack(obj,queue,json,duel_anim_queue_show_sprite)
		}
	}
}