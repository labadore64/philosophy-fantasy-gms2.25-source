menupos = 0

if(menu_selected == DECK_BUILD_TRUNK){
	menu_selected = DECK_BUILD_DECK
	tts_say("Deck")
} else {
	menu_selected = DECK_BUILD_TRUNK
	tts_say("Trunk")
}

if(global.menu_sounds){soundfxPlay(sfx_sound_menu_default);}

selected_draw_index = 0;
option_top = 0

deckBuild_setDisplayArrays();
deckBuild_updateSprite();
RUMBLE_MOVE_MENU