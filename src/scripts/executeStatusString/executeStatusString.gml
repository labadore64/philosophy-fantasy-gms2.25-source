var key = argument[0];

// loads the status stuff
loadStatusString(key);
var ids = duelController.temp_status_name;
var success = false
var script = asset_get_index("status_effect_" + ids);
if(script > -1){
	with(duelController){
		// runs the status effect, with arguments, card location and card location index in that order
		success = script_execute(script,temp_status_arg,temp_status_card_loc,temp_status_card_loc_index);
	}
}

if(success){
	decrementStatusString(key);
}