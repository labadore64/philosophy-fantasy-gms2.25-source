

	//input_countdown-= ScaleManager.timer_diff;
	//if(input_countdown < -1){
		input_countdown = -1;	
		
		// update input queue	
		var element = "";

		var inde = 0;
		if(argument_count > 0){
			inde = argument[0];	
		}
		var counter = 0;
		while(string_pos(":",element) == 0 &&
			!ds_list_empty(input_queue)){
			element = input_queue[|inde];
			ds_list_delete(input_queue,inde);
			
			counter++;
			if(counter > 1000000){
				break;	
			}
		}
		var base  = element;
		show_debug_message(string(element))
		input_countdown = ds_list_size(input_queue)
		// parse target, command
		var pos = string_pos(":",element)
		var command = string_copy(element,1,pos-1);
		element = string_delete(element,1,pos);
	
		// now extract the target
		var target_side = 0;
		var target_index = 0;
		var target_origin = noone;
	
		var stringe = element
		var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
		var side = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
			
		//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		

		if(side == "enemy"){
			target_side = DUEL_PLAYER_ENEMY
		} else {
			target_side = DUEL_PLAYER_YOU	
		}
			
		// if the code is a location, parse as location.
		if(code_is_location(ori)){
				
			if( string_length(string_digits(stringe)) == string_length(stringe)){
				
				target_index = string_copy(stringe,string_pos(":",stringe)+1,
										string_length(stringe) - string_pos(":",stringe))

				// string == variable
				if(string_length(string_digits(target_index)) != string_length(target_index)){
					var value = duelController.code_values[? target_index];
					if(is_undefined(value) || !is_real(value)){
						value = 0;	
					}
				
					target_index = value;
				} else {
					target_index = real(target_index);	
				}
				
				if(ori == "overlay"){
					target_index++;	
				}
			} else {
				// check if variable	
				// get number from variables
				if(ds_map_exists(duelController.code_values,stringe)){
					var def = duelController.code_values[? stringe]
					if(!is_undefined(def)){
						target_index = real(def);	
					}
				}
			}
				
			target_origin = code_get_location(ori,target_side)
		}

		// pass the target args to the command
		var script = asset_get_index("duel_input_" + command);

		if(script > -1){
			with(duelController){
				regain_control = false;
			}	
			
			with(target_origin){
				menupos = target_index;
				script_execute(menu_activate);
		
		
				var para = parent;
				var me = id;
				with(duelController){
					selected_object = para
					selected_object.selected_object = me
				}
			}
	
			// write the base command to a file if in write mode.
			input_obj = script_execute(script,target_origin,target_index);
	
			// write the replay to file
	
			if(!global.dont_record){
				// create the header
				var file = file_text_open_append(input_write_file)
				// write seed
				file_text_write_string(file,base)
				file_text_writeln(file)
				file_text_close(file);
			}
		}

		

		
	//}
