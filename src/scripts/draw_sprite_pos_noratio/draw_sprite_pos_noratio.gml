/// @description  draw_sprite_pos_raio(sprite,subimg,x1,y1,x2,y2,x3,y3,x4,y4,colour,alpha);
/// @param sprite
/// @param subimg
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param x3
/// @param y3
/// @param x4
/// @param y4
/// @param colour
/// @param alpha

draw_sprite_pos_fixed(argument[0],argument[1],

				argument[2]*global.scale_factor,argument[3]*global.scale_factor,
				argument[4]*global.scale_factor,argument[5]*global.scale_factor,
				argument[6]*global.scale_factor,argument[7]*global.scale_factor,
				argument[8]*global.scale_factor,argument[9]*global.scale_factor,
				
				argument[10],argument[11])