
var json = ds_map_create();

var deck = ds_list_create();

var sizer = ds_list_size(obj_data.current_deck);

for(var i = 0; i < sizer; i++){
	var card = obj_data.current_deck[|i]
	if(card > -1){
		ds_list_add(deck,obj_data.card_name[card]);
	}
}

var vals = ds_list_create();
var cols = ds_list_create();

var mapper = load_playerdata();

if(ds_exists(mapper,ds_type_map)){
	ds_list_copy(vals,mapper[? "val"]);
	ds_list_copy(cols,mapper[? "col"]);
}


ds_map_add(json,"type","custom");
ds_map_add(json,"chara",-1);
ds_map_add(json,"channel",obj_data.current_main_card);
ds_map_add(json,"sprite","custom");

ds_map_add(json,"name",global.character_name);
ds_map_add_list(json,"val",vals);
ds_map_add_list(json,"col",cols);
ds_map_add_list(json,"deck",deck);

var json_string = json_encode(json);

ds_map_destroy(json);
ds_list_destroy(deck);
ds_list_destroy(vals);
ds_list_destroy(cols);

return json_string;