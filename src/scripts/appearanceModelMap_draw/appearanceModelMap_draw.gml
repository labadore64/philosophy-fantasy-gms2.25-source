
// draw parent
	
with(parent){
	script_execute(menu_draw);	
}

// draw you
//surf
if(!surface_exists(surf)){
	surf = surface_create(global.letterbox_w,global.letterbox_h);	
	surf_main_update = true
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h)	
		surf_main_update = true
	}
}

var xxx = x+20
var yyy = y+225

if(surf_main_update){
	
	surface_set_target(surf)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)
	
	draw_set_color(c_white)
	draw_set_alpha(1)
	if(global.high_contrast){
		draw_rectangle_noratio(x-5,y-5,x+menu_width+5,y+menu_height+5,false)
	} else {

		if(global.color_gradient){
			draw_rectangle_color_noratio(x-7,y-7,x+menu_width+7,y+menu_height+7,
			global.menu_grcolor[0],global.menu_grcolor[0],
			global.menu_grcolor[1],global.menu_grcolor[1],
			false)
	
		
		} else {
			draw_set_color(global.menu_grcolor[0])
			draw_rectangle_noratio(x-7,y-7,x+menu_width+7,y+menu_height+7,
			false)
		}
		
	}
	
	if(global.high_contrast){
		draw_set_color(c_black)
	} else {
		draw_set_color(global.menu_bgcolor)
	}

	draw_set_alpha(1)
	draw_rectangle_noratio(x,y,x+menu_width,y+menu_height,false)

	draw_set_color(global.text_fgcolor)
	draw_set_font(global.largeFont)
	draw_set_halign(fa_center)
	draw_text_transformed_noratio(x+menu_width*.5,y+10,title,2,2,0)
	draw_set_halign(fa_left)
	draw_set_font(global.normalFont)

	//draw color map


	if(global.high_contrast){
		draw_rectangle_noratio(xxx-15,yyy-7-80,30+xxx+310,yyy+310-40,false)
	} else {
		draw_rectangle_color_noratio(xxx-7,yyy-7-20,20+xxx+310,yyy+310-20,
		$BBBBBB,$BBBBBB,
		global.menu_grcolor[1],global.menu_grcolor[1],
		false)
	}
	draw_set_color(global.menu_bgcolor)
	draw_rectangle_noratio(xxx+305,yyy-7,xxx+320,yyy+275,false)
	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_color(global.text_fgcolor)
	draw_set_alpha(.5)
		draw_rectangle_noratio(xxx+305,yyy-7+scroll_increment*(page),xxx+320,min(yyy+275,yyy-7+scroll_increment*(page+1)),false)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_set_alpha(1)
	// draw color
	var size = 110
	var x_shift = xxx+230-65
	var y_shift = yyy-150-5

	if(global.high_contrast){
		draw_set_color($AAAAAA)
		draw_rectangle_noratio(40+x_shift-5,y_shift-5,
						40+5+x_shift+size,5+size+y_shift,
						false)
	} else {
		draw_rectangle_color_noratio(40+x_shift-5,y_shift-5,
						40+5+x_shift+size,5+size+y_shift,
						$AAAAAA,$7F7F7F,
						$444444,$7F7F7F,
						false)
	}

	var hextext_x = x+menu_width*.5
	var hextext_y = y+menu_height-30
					
	draw_set_color(global.text_fgcolor)
	var modid = 0;
	var pos = menupos;
	with(parent){
		draw_set_halign(fa_center)
		draw_set_font(global.largeFont)
		var modid = model_id[menupos];
		draw_text_transformed_noratio(hextext_x,hextext_y,property_part_names_unmod[modid,pos],1,1,0)
		draw_set_halign(fa_left)
		draw_text_transformed_noratio(hextext_x-160,hextext_y-440,property_part_names[modid,portrait_property[modid]],1,1,0)
	}

	// draw selected model

	var size = 110
	if(!surface_exists(sample_surface)){
		sample_surface = surface_create(size*global.scale_factor,size*global.scale_factor);	
		sample_update = true
	} else {
		if(ScaleManager.updated){
			surface_resize(sample_surface,size*global.scale_factor,size*global.scale_factor)	
			sample_update = true
		}
	}
	var col = c_white

	if(sample_update){
		surface_set_target(sample_surface)
	
		with(parent){
			draw_clear(portrait_color[CHARA_CUSTOM_COL_BACKGROUND]);
	
			for(var j = 0; j < model_sprite_size[menupos]; j++){
				col = property_default_color[modid,j];
				if(col != c_white){
					col = portrait_color[property_default_color[modid,j]];	
				}
				draw_sprite_extended_noratio(property_sprite[modid,j],portrait_property[modid],
											property_x[modid]*1.25+size*.5,
											property_y[modid]*1.25+size*.5,
											.23*property_scale[modid],.23*property_scale[modid],
											0,col,1)
			}
		}
	
		surface_reset_target()
	}

	draw_surface(sample_surface,
					global.scale_factor*645,
					global.scale_factor*90)

	// draw list

	var counter = 0;
	var opti = option_top
	for(var i = 0; i < 9; i++){
		var size = 90
		if(!surface_exists(model_surface[i])){
			model_surface[i] = surface_create(size*global.scale_factor,size*global.scale_factor);	
			model_surf_update = true
		} else {
			if(ScaleManager.updated){
				surface_resize(model_surface[i],size*global.scale_factor,size*global.scale_factor)	
				model_surf_update = true
			}
		}
		

		if(model_surf_update){
			surface_set_target(model_surface[i])
				with(parent){
					draw_clear(portrait_color[CHARA_CUSTOM_COL_BACKGROUND]);
	
					if(opti+i < property_part_count[modid]){
						for(var j = 0; j < model_sprite_size[menupos]; j++){
							col = property_default_color[modid,j];
							if(col != c_white){
								col = portrait_color[property_default_color[modid,j]];	
							}
							draw_sprite_extended_noratio(property_sprite[modid,j],opti+i,
														property_x[modid]+size*.5,
														property_y[modid]+size*.5,
														.15*property_scale[modid],.15*property_scale[modid],
														0,col,1)
						}
					}
				}
			surface_reset_target()
		}
		
		// draw the surface
		var xx = (i % 3)*100
		var yy = floor(i / 3)*100
		draw_surface(model_surface[i],
			global.scale_factor*(xxx+xx+3),
			global.scale_factor*(yyy+yy-10))
		
		
	}
	
	surface_reset_target()

	sample_update = false;
	model_surf_update = false
	surf_main_update = false;
}

draw_set_alpha(1)
draw_surface(surf,global.display_x,global.display_y)
var size = 90
xx = (selected_draw_index % 3) * 100;
yy = floor(selected_draw_index/3)*100;

//draw_set_color(c_white)
if(global.menu_animate){
	draw_set_alpha(alpha_adder);
} else {
	draw_set_alpha(alpha_max)
}
gpu_set_blendmode(bm_add)
if(global.high_contrast){
	draw_rectangle_ratio(
				xxx+xx+3,
				yyy+yy-10,
				xxx+xx+3+size,
				yyy+yy-10+size,
				false)
} else {
	
	if(global.color_gradient){
		draw_rectangle_color_ratio(
		xxx+xx+3,
		yyy+yy-10,
		xxx+xx+3+size,
		yyy+yy-10+size,
				global.menu_grcolor[1],global.menu_grcolor[1],
				global.menu_grcolor[0],global.menu_grcolor[0],
		false)
		
	} else {
		draw_set_color(global.menu_grcolor[0])
		draw_rectangle_ratio(
		xxx+xx+3,
		yyy+yy-10,
		xxx+xx+3+size,
		yyy+yy-10+size,
		false)
	}
	
}
gpu_set_blendmode(bm_normal)

