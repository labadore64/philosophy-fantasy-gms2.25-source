if(card_array_size > 0){
	if(!card_facedown[menupos] || !enemy){
		
		with(duelSub){
			instance_destroy();	
		}
		
		var obj = instance_create(50,150,duelSub);
		obj.menu_selected = card_array[menupos];
		var menpos = menupos;
		obj.parent = id
		
		if(duelController.do_context_menu){
			obj.context_menu = true;
			soundfxPlay(sfx_sound_duel_ax_select)
			obj.option_spacer = 40
			// set the coordinates
			menupos = duelController.context_menu_select
		}

		var me = id;

		with(obj){
			if (duelController.phase_current == DUEL_PHASE_MAIN){
				textboxShow("tutorial004")
			}
			
			var windowCharPerLine = 30
			if(menu_selected > -1){
				card = card_create(650,290,.5,menu_selected);
				card.card_sprite_ref = obj_data.card_sprite[menu_selected]
				with(card){
					card_update()	
				}
				menu_add_option("View","View this card.",duelSub_view,spr_menu_info)
		
				code_menu_select_option(me);
			
				
				if(!duelController.code_select_active){
					if(duelController.phase_current == DUEL_PHASE_END){
						menu_add_option("Discard","Send this card to Limbo.",duelSub_discard,spr_menu_info)
					} else {
				
						// trigger effect for active card
						if(code_calculate_condition(menu_selected,"discard",code_set_cardstate(me,menpos))){
							menu_add_option("Discard","Send this card to Limbo.",duelSub_discard_effect,spr_menu_activate)
						}

						if(duelController.phase_current == DUEL_PHASE_MAIN){
							if(obj_data.card_type[menu_selected] == CARD_TYPE_METAPHYS &&
								parent.parent.part[DUEL_FIELD_THEORY].card_array_size == 0){
								// if the card has an activation effect
								menu_add_option("Activate","Activate this card.",duel_hand_activate_theory,spr_menu_activate)
							}
						}
						
						
					
						if(duelController.phase_current != DUEL_PHASE_RETURN){
							if (me.parent.switch_counter == 0){
								if(duel_can_activate_philosopher(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])){
										menu_add_option("Activate","Set as Main Philosopher.",duel_hand_set_main,spr_menu_activate)
								}
							}
						}
				
						if(duelController.do_return_overlay){
							if(duel_can_overlay(menu_selected,-1)){
								menu_add_option("Overlay","Overlay this card.",duel_hand_script_overlay_return,spr_menu_overlay)	
							}
						} else {
							if(!parent.enemy){
								if(obj_data.card_type[menu_selected] == CARD_TYPE_PHILOSOPHER){
									if(duelController.phase_current == DUEL_PHASE_MAIN){
										if(me.parent.overlay_counter == 0 // you haven't done your normal overlay
											&& me.parent.part[DUEL_FIELD_ACTIVE].card_array_size != 0 // there is at least one active philosopher
											&& duel_can_overlay(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])
											// there is room within the limit 
											){
											// there is room for an overlay
											if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size < me.parent.part[DUEL_FIELD_ACTIVE].card_limit ){
												menu_add_option("Overlay","Overlay this card.",duel_hand_script_overlay,spr_menu_overlay)	
											}
											// there is no room for an overlay, but there's one overlay
											else if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size == 2){
												menu_add_option("Overlay","Replace overlay card.",duel_hand_script_overlay,spr_menu_overlay)	
											}
											// theres no room for an overlay but there's two so you have to select one
											else if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size == 3){
												menu_add_option("Overlay","Replace overlay card.",duel_hand_script_overlay_override_choose,spr_menu_overlay)	
											}
										}
										// active philoospher must be set
										if(me.parent.part[DUEL_FIELD_ACTIVE].card_array_size > 0){
											if(me.parent.bench_counter == 0 // you haven't done your normal overlay
												&& me.parent.part[DUEL_FIELD_BENCH].card_array_size < me.parent.part[DUEL_FIELD_BENCH].size_limit // there is room within the limit 
												&& duel_can_bench(menu_selected,me.parent.part[DUEL_FIELD_ACTIVE].card_array[0])
												){
												menu_add_option("Bench","Bench this card.",duel_hand_script_bench,spr_menu_bench)	
											}
										}
									}
	
								}
							}
						}
					}
				}

			}
			menu_height = 20 + option_spacer * menu_count;
			
			y = -menu_height*.5 + 300
			
			//event_user(0)
			if(!duelController.do_context_menu){
				if(menu_count == 1){
					menu_width -= 50;
					x+=30
				}
				if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
					y+=50;
					card.y+=50;
					adjust_me = 50;
					
					with(card){
						card_update()	
					}
				}
				menu_push(id)
			} else {
				duelController_context_menu_adjust();
			}
		}
	}
}