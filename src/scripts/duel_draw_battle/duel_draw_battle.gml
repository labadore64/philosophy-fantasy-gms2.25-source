draw_set_alpha(1)
draw_set_font(global.normalFont)
draw_set_color(global.text_fgcolor)

var you = 225;
var enemy = 575

if(duelController.active_player == DUEL_PLAYER_ENEMY){
	you = 575
	enemy = 225
}

draw_set_halign(fa_center)
draw_text_transformed_ratio(you,150,attacking_name,3,3,0)
draw_text_transformed_ratio(enemy,150,defending_name,3,3,0)

with(duelController){
	// draw health
	for(var i = 0; i < 2; i++){
		duelHealth_draw(duel_hp[i]);
	}	
}
draw_set_color(c_white)
card_draw(attacking_card)
card_draw(defending_card)
draw_set_font(global.largeFont)

if(!done_animate){
	if(timer > shake_time){
		draw_set_halign(fa_left);
		if(global.menu_animate){
			if(total_damage > 0){
				draw_set_color(c_black)
				draw_text_transformed_ratio(enemy-25+5,5+400-timer*2,"-" + string(damage_abs),4,4,0)	
				draw_set_color(c_white)
				draw_text_transformed_ratio(enemy,400-timer*2,"-" + string(damage_abs),4,4,0)	
			} else if (total_damage < 0){
				draw_set_color(c_black)
				draw_text_transformed_ratio(you-25+5,5+400-timer*2,"-" + string(damage_abs),4,4,0)	
				draw_set_color(c_white)
				draw_text_transformed_ratio(you-25,400-timer*2,"-" + string(damage_abs),4,4,0)	
			}
		} else {
			if(total_damage > 0){
				draw_set_color(c_black)
				draw_text_transformed_ratio(enemy-25+5,5+400,"-" + string(damage_abs),4,4,0)	
				draw_set_color(c_white)
				draw_text_transformed_ratio(enemy,400,"-" + string(damage_abs),4,4,0)	
			} else if (total_damage < 0){
				draw_set_color(c_black)
				draw_text_transformed_ratio(you-25+5,5+400,"-" + string(damage_abs),4,4,0)	
				draw_set_color(c_white)
				draw_text_transformed_ratio(you-25,400,"-" + string(damage_abs),4,4,0)	
			}	
		}
	}
}

draw_set_halign(fa_left)