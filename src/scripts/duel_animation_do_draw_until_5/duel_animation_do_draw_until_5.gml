var field = argument[0];
var flag = 0
if(argument_count > 1){
	flag = argument[1]	
}
if(instance_exists(field)){
	if(!instance_exists(duelController.animation_stack_run) ||
		duelController.animation_stack_run.object_index == animationQueue){
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	
		if(queue > -1 && ds_exists(queue,ds_type_list)){
	
			duelController.running_obj = obj;
			var counter = ds_list_size(queue);
			// add to the queue
			var add = 5-(field.hand.card_array_size); 
	
			if(field.hand.card_array_size >= 5){
				add = 1;	
			}
			
			var modi = 0;
			var scale = 2.75;
			
			if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
				modi -= TOOLTIP_CAMERA_ADJUST_Y
				scale = TOOLTIP_CAMERA_ADJUST_SCALE
			}
	
			if(!duelController.mouse_mode){
				if(field.enemy){
					var json = ds_map_create();
					ds_map_add(json,"x",350)
					ds_map_add(json,"y",-65+modi)
					ds_map_add(json,"scale",scale)
					ds_map_add(json,"time",20)
		
					duel_animation_pack(obj,queue,json,duel_anim_queue_move_camera)
			
				} else {
					var json = ds_map_create();
					ds_map_add(json,"x",350)
					ds_map_add(json,"y",650+modi)
					ds_map_add(json,"scale",scale)
					ds_map_add(json,"time",20)
		
					duel_animation_pack(obj,queue,json,duel_anim_queue_move_camera)
				}
			}
	
			var counter = ds_list_size(queue);

			var stringg = "";

			for(var i = 0; i < add; i++){
				var json = ds_map_create();
				ds_map_add(json,"origin",field.deck)
				ds_map_add(json,"dest",field.hand)
				ds_map_add(json,"card_id",0)
				ds_map_add(json,"time",12)
				ds_map_add(json,"facedown",field.enemy);
				ds_map_add(json,"movecamera",false)
				ds_map_add(json,"block",true)
				ds_map_add(json,"flag",flag);
				if(i < field.deck.card_array_size){
					var le_card = field.deck.card_array[i];
					if(le_card > -1){
						stringg += obj_data.card_name[le_card] + ", "
					}
				}
				duel_animation_pack(obj,queue,json,duel_anim_queue_move_card)
			}
			
			stringg = string_delete(stringg,string_length(stringg)-1,2);
			if(!field.enemy){
				if(duelController.game_started){
					duel_animation_pause(string_replace("Drew @0.","@0",stringg));
				}
			}
		}
	}
}
