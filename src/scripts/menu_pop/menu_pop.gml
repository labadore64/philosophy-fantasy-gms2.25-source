with(menuControl){
	//mouseHandler_clear();
	var menuz = ds_stack_top(menu_stack);
	var cando = false;
	
	
	if(!is_undefined(menuz) && instance_exists(menuz)){
		if(menuz.pop_base){
			cando = true;		
		}
	} else {
		cando = true;	
	}
	
	if(cando){
		var menu = ds_stack_pop(menu_stack);
	
		
		while(!is_undefined(menu) && !instance_exists(menu) && ds_stack_size(menu_stack) > 1){
			menu = ds_stack_pop(menu_stack);
		}
		active_menu = ds_stack_top(menu_stack);
		if(is_undefined(active_menu)){
			active_menu = noone;	
		}
	
		with(active_menu){
			if(menu_mouse_activate != -1){
				if(!instance_exists(TextBox)){

				
					script_execute(menu_mouse_activate)	
					event_user(0)
				}
			}
			if(mouse_menu_script != -1){
				if(!instance_exists(TextBox)){
					script_execute(mouse_menu_script)	
				}
			}
		}
		pause = 2;
		return menu;
	} else {
		return menuz	
	}
}