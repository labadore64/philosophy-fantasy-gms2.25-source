// should be in all scripts
var index = argument[1]
var origin = argument[0]

duelController.speak_tts = false;
with(origin){
	
	var mon = card_array[index]
	
	if(!duelController.ai_mode){
		if(mon > -1){
			duel_animation_pause(obj_data.card_name[mon] + " Activated");	
		}
	}
	
	if(card_has_keyword(parent.part[DUEL_FIELD_ACTIVE].card_array[0],"unfriendly")){
		repeat(parent.part[DUEL_FIELD_ACTIVE].card_array_size){
			var card = parent.part[DUEL_FIELD_ACTIVE].card_array[0];
			duel_animation_move_card(0,parent.part[DUEL_FIELD_ACTIVE],parent.wait);
		}
	}
	
	for(var i = parent.part[DUEL_FIELD_ACTIVE].card_array_size-1; i >= 0; i--){
		var card = parent.part[DUEL_FIELD_ACTIVE].card_array[i];
		duel_animation_move_card(i,parent.part[DUEL_FIELD_ACTIVE],parent.part[DUEL_FIELD_LIMBO])
	}
	
	if(!duelController.ai_mode){
		duel_battle_showcard(mon,sfx_sound_duel_play_card);
	}
	var this = code_set_cardstate(parent.part[DUEL_FIELD_ACTIVE],0)
	
	duel_animation_card_effect(mon,"initial_activate",this)
	duel_animation_move_card(index,id,parent.part[DUEL_FIELD_ACTIVE],
										false,false,power(2,ANIMATION_MOVE_FLAG_IGNORE_SOUND) + power(2,ANIMATION_MOVE_FLAG_CAMERA_ADJUST))
	
	//duel_animation_wait(20)
	////duel_animation_resume_control()
	if(duelController.ai_mode){
		parent.ai_switch_counter[parent.ai_counter]++;
	} else {
		parent.switch_counter++;
	}
}
// do the animation for showing card!

return noone