// does an expression
if(!argument[0]){
	// parse condition
	if(duelController.code_argument_size > 0){
		var result = 0;
		var _if = duelController.code_arguments[0];
	
	
		var result_name = "return";
		if(duelController.code_argument_size > 1){
			result_name = duelController.code_arguments[1];
		
			// the variable must exist. this forces uses of the predefined registers
			// to prevent any funny business
			if(!ds_map_exists(duelController.code_values,result_name)){
				result_name = "return";	
			}
		}
		
		duel_animation_expr(_if,result_name)
	}
} else {
	duel_code_cexpr();	
}