var obj = instance_create(50,150,deckBuildSub);
obj.menu_selected = menu_selected;
if(menu_selected == DECK_BUILD_DECK){
	obj.card_id = deck_display[menupos]
} else {
	obj.card_id = trunk_display[menupos]
}

var favee = fave_cando;

with(obj){
	var windowCharPerLine = 30
	
	menu_add_option("View",textForceCharsPerLine("View this card.",windowCharPerLine),deckBuildSub_script_view,spr_menu_info)
	if(favee){
		if(obj_data.card_type[card_id] == CARD_TYPE_PHILOSOPHER){
			menu_add_option("Fave",textForceCharsPerLine("Set this card as your favorite.",windowCharPerLine),deckBuildSub_script_fave,spr_menu_fave)
		}
	}
	if(menu_selected != DECK_BUILD_DECK){
		menu_add_option("Deck",textForceCharsPerLine("Move this card to the deck.",windowCharPerLine),deckBuildSub_toDeck,-1)
	} else {
		menu_add_option("Trunk",textForceCharsPerLine("Move this card to the trunk.",windowCharPerLine),deckBuildSub_toTrunk,-1)
	}
	menu_add_option("Cancel",textForceCharsPerLine("Go Back.",windowCharPerLine),menuParent_cancel,-1)	

	tts_say(menu_name[0])
	mouseHandler_clear()
	menuParent_menu_makeMouse();
	if(global.menu_sounds){
		soundfxPlay(sfx_sound_menu_select)	
	}
}