var i = argument[0]

with(duelController){
	if(!game_complete){
		if(instance_exists(duel_hp[0])){
		access_string[i] = string(duel_hp[0].hp_current) + " " + duel_hp[0].name + "'s HP"
		access_string[i+1] = string(duel_hp[1].hp_current) + " " + duel_hp[1].name + "'s HP"
		access_string[i+2] = duel_hp[active_player].name + "'s turn, turn " + string(turns)
		if(instance_exists(selected_object) && instance_exists(selected_object.selected_object)){
			var charaname = "Your";
	
			if(selected_object.enemy){
				charaname = duel_hp[1].name +"'s";	
			}
		
			var stringe =  "";
	
			if(selected_object.selected_object.object_index == duelActive){
				if(selected_object.selected_object.menupos == 0 ||
					selected_object.selected_object.card_array_size == 0){
					stringe = charaname + " " + selected_object.selected_object.source_name
				} else {
					stringe = charaname + " " + "Overlay"
				}
			} else {
				stringe = charaname + " " + selected_object.selected_object.source_name
			}
		
			if(selected_object.selected_object.card_array_size > 0){
				stringe += ", Cards: " + string(selected_object.selected_object.card_array_size);	
			}
		
			access_string[i+3] = stringe;
		}
		}
	}
}