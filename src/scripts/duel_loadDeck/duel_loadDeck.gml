var map = argument[1]

with(argument[0]){
	var list = map[? "deck"]
	
	if(ds_exists(list,ds_type_list)){
		// get your deck
		var len = ds_list_size(list);

		for(var i = 0; i < len; i++){
			duel_deck_addCard(deck,card_from_name(list[|i]));	
			spr_load_card_front(card_from_name(list[|i]));
		}

		with(deck){
			duelField_randomize();	
		}
	}
}
