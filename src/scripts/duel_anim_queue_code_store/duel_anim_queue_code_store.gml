var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _value = map[? "value"];
	var _store = map[? "store"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_value) &&
		!is_undefined(_store)){
		
		if(!is_undefined(duelController.code_values[? _store])){
			// set return value to the result
			duelController.code_values[? _store] = _value
		}
		
		return noone;
	}

}

return noone;