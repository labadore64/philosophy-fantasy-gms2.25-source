
// NOTE, these are base 2 so you can quickly do bitmasking ops
#macro ANIMATION_MOVE_FLAG_DEFAULT 0
#macro ANIMATION_MOVE_FLAG_CARD_EFFECT 1
#macro ANIMATION_MOVE_FLAG_IGNORE_SOUND 2
#macro ANIMATION_MOVE_FLAG_CAMERA_ADJUST 3
#macro ANIMATION_MOVE_FLAG_CARD_SHUFFLE 4

var target_index = argument[0]
var block = argument[1];
var dest = argument[2]
var flag = ANIMATION_MOVE_FLAG_DEFAULT;

if(argument_count > 3){
	flag = argument[3];	
}

if(duelController.ai_mode){
	duel_anim_queue_code_move_card(target_index,block,dest,flag)
} else {

	if(!instance_exists(duelController.animation_stack_run) ||
		duelController.animation_stack_run.object_index == animationQueue){
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	
		duelController.running_obj = obj;
	
		if(queue > -1 && ds_exists(queue,ds_type_list)){
			var counter = ds_list_size(queue);


			var json = ds_map_create();
			ds_map_add(json,"target",target_index )
			ds_map_add(json,"block",block)
			ds_map_add(json,"dest",dest)
			ds_map_add(json,"flag",flag)
		
			duel_animation_pack(obj,queue,json,duel_anim_queue_code_move_card)
		}
	}

}