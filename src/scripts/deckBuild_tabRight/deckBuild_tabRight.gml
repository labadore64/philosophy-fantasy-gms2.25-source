
if(menu_count > 0){

	var menupos_old = menupos



fade_surface_updated=true
menupos+=per_line

if(menupos >= menu_count){
	if(!wrap){
		menupos = menu_count-1;	
	} else {
		menupos = 0;	
	}
}
deckBuild_updateSprite();
menuParent_sound()

RUMBLE_MOVE_MENU


with(fadeIn){
	instance_destroy()	
}
with(fadeOut){
	instance_destroy()	
}

if(global.voice){
	tts_say(menu_name[menupos])	
	
	if(global.speak_details){
		tts_say(menu_desc[menupos])	
	}
}

surface_update = true
	
	// do option top stuff
	if(menupos_old != menupos){
		option_top+=per_line
		if(option_top > menu_count-per_line){
			option_top = menu_count-per_line;
		}
		
		//calculate selected from option top
		var counter = 0;
		for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
			if(i == menupos){
				selected_draw_index = counter;
				break;
			}
			counter++;
		}

	}
	tts_clear()
	tts_say(obj_data.card_name[selected_card.card_id]);
	deckBuild_setDisplayArrays();

}

/*
if(menu_selected == DECK_BUILD_TRUNK){
	if(!(trunk_total == 1 && obj_data.card_in_trunk[trunk_display[0]] == 1) && deck_total < 20){
		deckBuild_trunkToDeck(trunk_display[menupos])

		if(obj_data.card_in_trunk[trunk_display[menupos]] == 0){
			menupos--;
			if(menupos < 0){
				menupos = 0	
			} else {
				if(selected_draw_index > 0){
					selected_draw_index--;	
				} else {
					option_top--
				}
			}
		}
		
		deckBuild_setDisplayArrays()
		
		soundfxPlay(sfx_sound_deck_add)
		
		gamepad_rumble(.5,15)
		with(Rumble){
			rumble_left = 0
			rumble_right = 1
		}
	}
}
*/