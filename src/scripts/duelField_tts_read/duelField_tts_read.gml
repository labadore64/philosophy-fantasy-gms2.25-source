if(instance_exists(duelController.selected_object)){
	with(duelController.selected_object.selected_object){
		duelController.viewcard_id = -1;
		if(card_array_size > 0){
			if(menupos > -1){
				if(card_array[menupos] > -1){
					if(!(card_facedown[menupos])){
						duelController.viewcard_id = card_array[menupos]
							
						if(duelController.can_tts){
							ax_string_clear()
							var cardo = card_array[menupos]
							if(object_index == duelActive &&
								menupos == 0){
								with(duelActive){
									duel_activemon_update();	
								}
								if(instance_exists(card)){
									var att = card.card_attack_power
									var def = card.card_defense_power
							
									with(duelController){
										ax_card_values(cardo,att,def)
									}
							
									duelField_access_update(6)
								}
							} else {
								duel_set_card_data(cardo);
								duelField_access_update(6)
							}
					
							tts_say(obj_data.card_name[card_array[menupos]])
						}
					} else {
						if(duelController.can_tts){
							if(global.speak_details){
								tts_say("Card")	
							}	
							duelField_access_update(0)
						}
					}
				}
			}
		}
	}
}