/// @function stringToKeyboard(name)
/// @description Turns a string into a virtual keyboard key.
/// @param {string} name The name of the key.

	if(is_undefined(argument[0])){
		argument[0] = ""
	}
	var key = string_lower(argument[0]);

		if(string_length(key) == 1){
			if(string_lettersdigits(key) != ""){
				return ord(string_upper(key));
			}
		}
	
		switch(key){
			case "num 0":
			return vk_numpad0
			
			case "num 1":
			return vk_numpad1
			
			case "num 2":
			return vk_numpad2
			
			case "num 3":
			return vk_numpad3
			
			case "num 4":
			return vk_numpad4
			
			case "num 5":
			return vk_numpad5
			
			case "num 6":
			return vk_numpad6
			
			case "num 7":
			return vk_numpad7
			
			case "num 8":
			return vk_numpad8
			
			case "num 9":
			return vk_numpad9
			
			case "f1":
			return vk_f1
			
			case "f2":
			return vk_f2
			
			case "f3":
			return vk_f3
			
			case "f4":
			return vk_f4
			
			case "f5":
			return vk_f5
			
			case "f6":
			return vk_f6
			
			case "f7":
			return vk_f7
			
			case "f8":
			return vk_f8
			
			case "f9":
			return vk_f9
			
			case "f10":
			return vk_f10
			
			case "f11":
			return vk_f11
			
			case "f12":
			return vk_f12
			
			case "ctrl":
			return vk_control
		
			case "tab":
			return vk_tab;
	
			case "caps lock":
			return 20;
	
			case "shift":
			return vk_shift
	
			case "l-ctrl":
			return vk_lcontrol;

			case "left ctrl":
			return vk_lcontrol;
	
			case "r-ctrl":
			return vk_rcontrol;
			
			case "right ctrl":
			return vk_rcontrol;

			case "l-alt":
			return vk_lalt;
			
			case "r-alt":
			return vk_ralt;
			
			case "left alt":
			return vk_lalt;
	
			case "right alt":
			return vk_ralt;
	
			case "space":
			return vk_space
			
			case "spacebar":
			return vk_space
	
			case "left":
			return vk_left
	
			case "up":
			return vk_up
	
			case "right":
			return vk_right
	
			case "down":
			return vk_down
	
			case "enter":
			return vk_enter
	
			case "-":
			return 189
	
			case "+":
			return 187
	
			case "backspace":
			return vk_backspace
	
			case "<":
			return 188
	
			case ">":
			return 190
	
			case "?":
			return 191
	
			case ":":
			return 186
	
			case "\"":
			return 222
	
			case "{":
			return 219;
	
			case "}":
			return 221
	
			case "|":
			return 220
	
			case "~":
			return 192
	
			case "esc":
			return vk_escape
		
			case "num" + " /":
			return 111
		
			case "num" + " *":
			return 106
		
			case "num" + " -":
			return 109
		
			case "num" + " +":
			return 107
		
			case "num" + " .":
			return 110
		}
	
