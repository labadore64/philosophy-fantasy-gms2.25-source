
var _if = argument[0]
var _then = argument[1]
var _else = argument[2]

var is_ai = false;
if(argument_count > 3){
	is_ai = argument[3];
}

if(is_ai){
	duel_anim_queue_code_condition(_if,_then,_else);
} else {
	if(!instance_exists(duelController.animation_stack_run) ||
		duelController.animation_stack_run.object_index == animationQueue){
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	
		duelController.running_obj = obj;
	
		if(queue > -1 && ds_exists(queue,ds_type_list)){
			var counter = ds_list_size(queue);


			var json = ds_map_create();
			ds_map_add(json,"if",_if)
			ds_map_add(json,"then",_then)
			ds_map_add(json,"else",_else)
		
			duel_animation_pack(obj,queue,json,duel_anim_queue_code_condition)
		}
	}
}
