var pack = argument[0];

var obj = instance_create(argument[1],argument[2],CardPack);
obj.pack_id = pack;
obj.card_list = generateCardsForPack(obj.pack_id);

return obj;