
with(PackSelect){
	deckBuild_drawBackground()	
}

if(!surface_exists(menu_surf)){
	menu_surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	menu_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(menu_surf,global.letterbox_w,global.letterbox_h);
		menu_surf_update = true;
	}
}

if(menu_surf_update){
	surface_set_target(menu_surf)
	
	draw_clear_alpha(c_black,0)
	
	gpu_set_colorwriteenable(true,true,true,true)

	var bla = global.menu_bgcolor;

	if(global.high_contrast){
		bla = c_black;	
	}

	draw_set_color(bla)
	draw_set_alpha(.75)
	draw_rectangle_noratio(10,0,780,600,false)

	// text
	
	draw_set_font(global.normalFont)
	var counter = 0;
	for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
		draw_set_color(c_white)
		/*
		if(counter != selected_draw_index || animate_active){
			if(global.high_contrast){
				draw_set_alpha(.25)
			} else {
				draw_set_alpha(.1)
			}
		} else {
			if(global.high_contrast){
				draw_set_alpha(.6)
			} else {
				draw_set_alpha(.4)
			}
		}
		draw_rectangle_noratio(10,option_y_offset+3-10 + option_spacer*counter,
							780,option_y_offset-3-10 + option_spacer*(counter+1),
							false)
		*/
		gpu_set_colorwriteenable(true,true,true,true)
		if(!(counter != selected_draw_index || animate_active)){
			draw_set_color(c_white)
			if(global.menu_animate){
				draw_set_alpha(alpha_adder);
			} else {
				draw_set_alpha(alpha_max)
			}
			if(global.high_contrast){
				draw_rectangle_noratio(10,option_y_offset+3-10 + option_spacer*counter,
							780,option_y_offset-3-10 + option_spacer*(counter+1),
							false)
			} else {
				draw_rectangle_color_noratio(10,option_y_offset+3-10 + option_spacer*counter,
							780,option_y_offset-3-10 + option_spacer*(counter+1),
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
				false)
			}
		} else {
			draw_set_color(global.menu_bgcolor)
			draw_set_alpha(.85)
			draw_rectangle_noratio(10,option_y_offset+3-10 + option_spacer*counter,
							780,option_y_offset-3-10 + option_spacer*(counter+1),
									false)
		}

		draw_set_color(global.text_fgcolor)
		draw_set_alpha(1)	
		if(animate_card[i]){
			
			if(is_new[i] && animate_x[i] <= 0){
				draw_set_color(global.text_fgcolor)
				draw_set_halign(fa_right)
				draw_text_transformed_noratio(780,2+option_y_offset + option_spacer*counter,
					"NEW!",3,3,0)
				draw_set_halign(fa_left)
			}
			
			var draw_color = c_white;
			
			if(is_rare[i]){
				draw_color = rare_color;
				draw_sprite_extended_noratio(spr_star,0,
											animate_x[i]+10+option_x_offset,55+option_y_offset + option_spacer*counter,
											.75,.75,0,draw_color,1)
			}
			
			draw_set_color(draw_color)
			
			draw_text_transformed_noratio(animate_x[i]+10+40+option_x_offset,2+option_y_offset + option_spacer*counter,
								string_copy(menu_name[i],1,30),3,3,0)
		
			if(obj_data.card_in_trunk[card_id[i]] > 0){
				draw_sprite_extended_noratio(menu_sprite[i],card_sprite_index[i],
											animate_x[i]+10+option_x_offset,16+option_y_offset + option_spacer*counter,
											.33,.33,0,c_white,1)
			
			packDrawCard(animate_x[i]+10+option_x_offset,16+option_y_offset + option_spacer*counter,card_sprite_index[i])
										
			// philosopher stats
			if(obj_data.card_type[card_id[i]] == CARD_TYPE_PHILOSOPHER){
				draw_sprite_extended_noratio(spr_card_white_stat,0,
										animate_x[i]+option_x_offset+90,55+option_y_offset + option_spacer*i,
										.9,.9,0,draw_color,1)
								
				draw_sprite_extended_noratio(spr_card_white_stat,1,
										animate_x[i]+option_x_offset+230,55+option_y_offset + option_spacer*i,
										.9,.9,0,draw_color,1)
								
				draw_text_transformed_noratio(animate_x[i]+option_x_offset+120,45+option_y_offset + option_spacer*i,
									string(obj_data.card_attack[card_id[i]]) + "/+"
									+ string(obj_data.card_attack_overlay[card_id[i]])
									,2,2,0)
							
				draw_text_transformed_noratio(animate_x[i]+option_x_offset+260,45+option_y_offset + option_spacer*i,
									string(obj_data.card_defense[card_id[i]]) + "/+"
									+ string(obj_data.card_defense_overlay[card_id[i]])
									,2,2,0)
				}	
			}
		}
		counter++;
	}

	surface_reset_target();
}

draw_surface(menu_surf,global.display_x,global.display_y)

with(fadeIn){
	draw_fade()	
}
with(fadeOut){
	draw_fade()	
}


draw_letterbox();