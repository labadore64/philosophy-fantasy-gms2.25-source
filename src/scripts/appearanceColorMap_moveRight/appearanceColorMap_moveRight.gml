if(!color_selected){
	if(menupos < 7){
		selected = true
		appearanceColorMap_select(false)	
	}
	//appearanceColorMap_mouseSelect(menupos+1)
	stringer = "";
	if(menupos < 7){
		if(menupos != 0){
			menupos+=3
		} else {
			menupos = 1;	
		}
	}

	if(menupos >= menu_count){
		menupos = menu_count-1;	
	}
	randomise();
	if(menupos < 7){
		menuParent_sound()
	}

	RUMBLE_MOVE_MENU


	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}

	menuParent_default_accessString()
	menuParent_ttsread();

	with(MouseHandler){
		ignore_menu = true
		menu_hover = true
	}

	if(global.menu_animate){
		alpha_adder = 0
	}

	surface_update = true
	surf_main_update=true
}