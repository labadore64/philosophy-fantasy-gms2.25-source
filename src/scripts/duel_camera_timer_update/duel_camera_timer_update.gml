if(goto_timer > -1){
	goto_timer-=ScaleManager.timer_diff*FAST
	
	// tests overshooting
	var scalee = (scale-goto_scale)/goto_timer;
	if(sign(goto_x_change) > 0 && x < goto_x){
		goto_timer = -1;
	} else if (sign(goto_x_change) < 0 && x > goto_x){
		goto_timer = -1;
	} else if(sign(goto_y_change) > 0 && y < goto_y){
		goto_timer = -1;
	} else if (sign(goto_y_change) < 0 && y > goto_y){
		goto_timer = -1;
	} else if (sign(scalee) < 0 && scale > goto_scale){
		goto_timer = -1;	
	} else if (sign(scalee) > 0 && scale < goto_scale){
		goto_timer = -1;	
	}
	
	if(goto_timer < 0){
		x = goto_x;
		y = goto_y;
		scale = goto_scale;
		goto_x = 0;
		goto_y = 0;
		goto_timer = -1;
		goto_x_change = 0;
		goto_y_change = 0;
		goto_scale = scale;
		current_scaler = 0;
		last_scaler = 0;
		moving = true
		/*
		with(duelController){
			if(game_started){

				with(selected_object){
					if(changed){
						if(enemy){
							with(selected_object){
								tts_say(duelController.duel_hp[1].name + source_name)
							}
						} else {
							with(selected_object){
								tts_say("Your " + source_name)	
							}
						}
					}
					changed = false;
				}
			}
		}
		*/
	} else {
		if(global.menu_animate){
			var multi = max(0,ScaleManager.timer_diff)*FAST
			x-=	goto_x_change*multi
			y-= goto_y_change*multi
			moving = true
			current_scaler = scale_change*multi
			if(abs(current_scaler) > 2){
				scale = goto_scale;
				goto_timer = 0
				x = goto_x;
				y = goto_y;
			} else {
				scale-=	current_scaler
			}
		}
	}
}