/// @description Insert description here
// You can write your code in this editor
with(duelController){
	draw_set_font(global.normalFont)
	if(global.high_contrast){
		draw_clear($333333)
	} else {
		draw_duelbackground();
	}

	gpu_set_colorwriteenable(true,true,true,false)
	
	draw_surface_ext(application_surface,global.display_x,global.display_y,
					global.scale_factor*800/global.window_width,global.scale_factor*600/global.window_height,0,c_white,field_alpha)

	draw_set_alpha(1)
	
	// draw popout menu
	if(!do_screenshot){
		draw_sprite_extended_ratio(spr_dropdown_menu,0,400,25+pop_menu_y,1,1,0,global.text_bgcolor[0],1)
	
		duelController_draw_buttons();
	}
	
	if(global.high_contrast){
		draw_set_color(c_black)
		draw_rectangle_ratio(0,0,800,145,false)
	} else {
		if(global.color_gradient){
			draw_rectangle_color_ratio(0,0,800,80,
				global.text_bgcolor[1],global.text_bgcolor[1],
				global.text_bgcolor[0],global.text_bgcolor[0],
			false)
			draw_set_color(global.text_bgcolor[0])
			draw_rectangle_ratio(0,80,800,145,false)
		} else {
			draw_set_color(global.text_bgcolor[0])
			draw_rectangle_ratio(0,0,800,145,false)
		}
	}
	
	// draw current phase
	draw_set_color(global.text_fgcolor)
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(400,10,duel_hp[active_player].name,3,3,0)
	draw_text_transformed_ratio(400,50,"Turns:"+string(turns),2,2,0)
	//draw_text_transformed_ratio(400,85,"Phase:",2,2,0)
	//if(phase_current > -1){
	//	draw_text_transformed_ratio(400,110,phase_name[phase_current],2,2,0)
	//}
	
	// draw health
	
	for(var i = 0; i < 2; i++){
		duelHealth_draw(duel_hp[i]);
	}

	var colx = global.text_fgcolor

	if(global.high_contrast){
		colx = c_white;
	}
	
	// dropdown button
	
	draw_sprite_extended_ratio(spr_menu_arrow,0,400,110,1,side_sprite_yscale,0,colx,1)
	
	// help button

	if(side_button_right){
		if(help_menu_pressed){
			draw_sprite_extended_ratio(spr_menu_help,0,770,175,.85,.85,0,colx,1)
		} else {
			draw_sprite_extended_ratio(spr_menu_help,0,770,175,help_menu_hover,help_menu_hover,0,colx,1)
		}
	
		// move button

		if(help_menu_pressed){
			draw_sprite_extended_ratio(spr_menu_move,0,720,175,.85,.85,0,colx,1)
		} else {
			draw_sprite_extended_ratio(spr_menu_move,0,720,175,help_menu_hover,help_menu_hover,0,colx,1)
		}
	} else {
		if(help_menu_pressed){
			draw_sprite_extended_ratio(spr_menu_help,0,30,175,.85,.85,0,colx,1)
		} else {
			draw_sprite_extended_ratio(spr_menu_help,0,30,175,help_menu_hover,help_menu_hover,0,colx,1)
		}
	
		// move button

		if(help_menu_pressed){
			draw_sprite_extended_ratio(spr_menu_move,0,80,175,.85,.85,0,colx,1)
		} else {
			draw_sprite_extended_ratio(spr_menu_move,0,80,175,help_menu_hover,help_menu_hover,0,colx,1)
		}
	}


	if(!global.dont_record){
		
		if(!do_screenshot){
			if(mouse_mode && game_started){
				
			with(navigation_menu){
				script_execute(menu_draw);	
			}

			if(context_menu > -1){
				duelController_context_menu_draw(context_menu);
			} 
			
			draw_set_alpha(1)
			
			if(!game_complete){
				if(draw_pointer && !resume_control){ // 
					var multi = 1;
					if(!global.reverse_pointer){
						multi = -1;	
					}
					var multii = 1;
					if(global.camera_track == CAMERA_TRACK_NONE){
						multii = .5	
					}
				
					if(!mouse_mode){
						draw_sprite_extended_ratio(spr_duel_cursor,ScaleManager.spr_counter*FAST,(pointer_x+50)-duelCamera.left,(20+pointer_y)-duelCamera.top,multi,1,0,c_white,1)
					} else {
						with(duelCursor){
							draw_sprite_ext(duel_cursor_sprite,0,x,y,multii*multi*global.scale_factor,multii*global.scale_factor,0,c_white,1)
						}
					}
				}
			}
		}
	}
	}
	

	draw_set_color(c_black)
	with(duelSub){
		if(!context_menu){
			draw_set_alpha(.5)
			draw_rectangle_ratio(0,0,800,600,false)
			draw_set_alpha(1)	
		}
	}

	draw_set_alpha(1)	
	gpu_set_colorwriteenable(true,true,true,true)

	draw_set_halign(fa_left)
	if(global.tooltip_type != TOOLTIP_TYPE_ONLY){
		with(drawMoveCard){
			draw_set_alpha(1)
			if(global.color_gradient){
				draw_rectangle_color_ratio(0,485,800,600,
											global.text_bgcolor[0],global.text_bgcolor[0],
											global.text_bgcolor[1],global.text_bgcolor[1],
											false)
			} else {
				draw_set_color(global.text_bgcolor[0])
				draw_rectangle_ratio(0,485,800,600,
											false)
			}
		}
		with(drawSwapCard){
			draw_set_alpha(1)
			if(global.color_gradient){
				draw_rectangle_color_ratio(0,485,800,600,
											global.text_bgcolor[0],global.text_bgcolor[0],
											global.text_bgcolor[1],global.text_bgcolor[1],
											false)
			} else {
				draw_set_color(global.text_bgcolor[0])
				draw_rectangle_ratio(0,485,800,600,
											false)
			}
		}
	}


	// draw infobox
	if(global.tooltip_type != TOOLTIP_TYPE_ONLY){
		draw_set_alpha(1)
		if(global.color_gradient){
			draw_rectangle_color_ratio(0,485,800,600,
										global.text_bgcolor[0],global.text_bgcolor[0],
										global.text_bgcolor[1],global.text_bgcolor[1],
										false)
		} else {
			draw_set_color(global.text_bgcolor[0])
			draw_rectangle_ratio(0,485,800,600,
										false)
		}

		draw_set_color(c_white);

		if(duelController.game_start_animation){
			if(selected_object > -1){
				if(selected_object.selected_object > -1){
					if(selected_object.selected_object.menu_display_info > -1){
						with(selected_object.selected_object){
							script_execute(menu_display_info);	
						}
					}
				}
			}
		}
	}
	
	with(animationSprite){
		draw_sprite_extended_ratio(sprite_index,image_index,x,y,image_xscale,image_yscale,
									image_angle,image_blend,image_alpha)
	}
	

	// phase change
	with(animationPhaseTransition){
		if(trigger){
			duelController_draw_phase_trans();	
		}
	}
	
	with(animationVictoryMessage){
		duelController_draw_victory_message();
	}
	
	with(animationEndTurnMessage){
		duelController_draw_victory_message();
	}

	with(animationWindow){
		duelController_draw_message()	
	}
	
	with(animationShowCard){
		card_draw(card)	
	}
	
	//with(TextBox){

	//	script_execute(menu_draw);	
	//}
	draw_set_font(global.normalFont)

	if(pop_menu_active){
		for(var i = 0; i < tooltip_total; i++){
			if(side_hover[i] > 1){
				drawToolTip(tooltip[i]);
			}
		}
	} else {
		if(context_menu < 0){
			if(global.tooltip_type > 0){
				if(tooltip_show){
					if(running_obj < 0 && menuControl.active_menu == id){
						if(tooltip_total > 0){
							drawToolTip(tooltip[0])
						}
					}
				}
			}
		}	
	}

	with(fadeIn){
		draw_fade()	
	}
	with(fadeOut){
		draw_fade()	
	}

	draw_letterbox()
}