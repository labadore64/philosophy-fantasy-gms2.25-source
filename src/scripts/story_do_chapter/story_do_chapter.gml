var map = argument[0]

var chapnum = 0;
var title = "";
var card = "aristotle"

global.story_next = "";
val = map[? "next"]
if(!is_undefined(val)){
	global.story_next = string_lower(val)	
}

val = map[? "id"]
if(!is_undefined(val)){
	chapnum = real(val)	
}

val = map[? "title"]
if(!is_undefined(val)){
	title = val
}

val = map[? "card"]
if(!is_undefined(val)){
	card = string_lower(val)	
}


obj_data.chapter = "Chapter " + string(chapnum)
obj_data.chapter_title = title
obj_data.chapter_card = card

room_goto(ChapterRoom);