// if starts with enemy, you will check if the current user is an enemy card.

var target = argument[0];
var this = argument[1];

var thiss = code_get_this();

// if it is enemy, its always true.
if(target == "enemy" && string_pos("pos_enemy",thiss) > 0 ){
	return true;	
} else {

	if((string_pos("pos_enemy",thiss) > 0 && duelController.active_player == DUEL_PLAYER_YOU)
		|| (string_pos("pos_you",thiss) > 0 && duelController.active_player == DUEL_PLAYER_ENEMY)){
	
			return status_effect_negate_get_range(target,this);	
	}
}

return false;