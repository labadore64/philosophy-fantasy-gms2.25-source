/// @description Insert description here
// You can write your code in this editor

with(duelCoinFlip){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		titleScreen_DrawBackground()
	}

	
	if(!calling){
		if(coin_flip_animate){
			draw_sprite_extended_ratio(spr_coinflip_animate,index,0,0,1,1,0,c_white,1)
		} else {
			if(countdown <= -1){
				if(heads){
					draw_sprite_extended_ratio(spr_coinflip_heads,0,0-coin_transition_x,0,1,1,0,c_white,1)
				} else {
					draw_sprite_extended_ratio(spr_coinflip_tails,0,0-coin_transition_x,0,1,1,0,c_white,1)
				}
		
				if(coin_transition_x <= 0){
					draw_set_color(global.text_fgcolor)
					draw_set_alpha(1)
					draw_set_font(global.largeFont)
					draw_set_halign(fa_center)
					draw_text_transformed_ratio(400,500,coin_string,3,3,0)
					draw_set_halign(fa_left)
			
				}
			}
		}
	}
}

with(fadeIn){
	draw_fade()	
}

with(fadeOut){
	draw_fade()	
}

draw_letterbox()