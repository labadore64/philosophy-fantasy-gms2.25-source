var obj = noone
if(list_selected == 0){
	obj = instance_create(420,20,appearanceModelMap);
	obj.parent_update = appearanceSet_modelUpdate;
	var mode = model_id[menupos]
	var count = property_part_count[mode];
	
	with(obj){
		for(var i = 0; i < count; i++){
			menu_add_option(parent.property_part_names_unmod[mode,i],"",-1,-1)	
		}
		page_total = floor(menu_count/9)+1
		appearanceModelMap_scrollUpdate();
	}
} else {
	obj = instance_create(420,20,appearanceColorMap);
	obj.parent_update = appearanceSet_colorUpdate;
	var color = color_id[menupos]
	with(obj){
		appearanceColorMap_updateColor(color)	
	}
}

if(obj != noone){
	menu_push(obj);
}