draw_set_alpha(.5)
draw_set_color(c_black)
draw_rectangle_ratio(0,0,800,600,false)
draw_set_alpha(1)
draw_set_halign(fa_left)
draw_set_color(c_white)
draw_set_alpha(1)
draw_set_color(c_white)
if(global.high_contrast){
	draw_rectangle_ratio(x-3,y-3,x+width+3,y+height+3,false)
} else {

	if(global.color_gradient){
		draw_rectangle_color_ratio(x-5,y-5,x+width+5,y+height+5,
		global.menu_grcolor[0],global.menu_grcolor[0],
		global.menu_grcolor[1],global.menu_grcolor[1],
		false)
		
	} else {
		draw_set_color(global.menu_grcolor[0])
		draw_rectangle_ratio(x-5,y-5,x+width+5,y+height+5,
		global.menu_grcolor[0],global.menu_grcolor[0],
		global.menu_grcolor[1],global.menu_grcolor[1],
		false)
	}

}

if(global.high_contrast){
	draw_set_color(c_black)
} else {
	draw_set_color(global.menu_bgcolor)
}
draw_set_font(global.normalFont)
draw_rectangle_ratio(x,y,x+width,y+height,false)
draw_set_color(global.text_fgcolor)
draw_text_transformed_ratio(x+15,y+15,message,2,2,0)