with(parent){
	duelController.speak_tts = false;
	var mon = card_array[menupos]
	if(mon > -1){
		tts_say(obj_data.card_name[mon] + " Overlayed");	
	}
	duel_animation_move_card(menupos,parent.part[DUEL_FIELD_BENCH],parent.part[DUEL_FIELD_ACTIVE])
	duel_animation_wait(20)
	duel_animation_play_sound(sfx_sound_duel_overlay_card)
	duel_animation_activate_card(mon)
	
	parent.overlay_counter++;
}
menu_pop();
instance_destroy()
////duel_animation_resume_control()	