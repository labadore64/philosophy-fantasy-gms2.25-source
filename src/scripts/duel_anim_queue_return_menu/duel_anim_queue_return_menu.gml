// first delete all instances of duel_anim_queue_resume_control in the queue
// then, check if this is the last item in the queue
// if it is, execute. otherwise, push it to the end.


	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	
	
	
	if(queue > -1 && ds_exists(queue,ds_type_list)){

		var argu = argument[0]; // args as json string
		
		// remove all instances of resume control
		var len = ds_list_size(queue)-1;
		
		for(var i = len-1; i >= 0; i--){
			var current = queue[| i];
			var val = obj.arg_to_script[? current];
			if(!is_undefined(val)){
				if(val == duel_anim_queue_resume_control){
					ds_list_delete(queue,i);
					ds_map_delete(obj.args,current);
					ds_map_delete(obj.arg_to_script,current);	
				}
			}
		}

		var len = ds_list_size(queue)-1;

		if(ds_list_empty(queue)){
			return duel_anim_part_return_overlay(argument[0]);
		} else {
			// delete the current thing
			var current = queue[| 0];
			if(obj.arg_to_script[? current] == duel_anim_queue_resume_control){
				ds_list_delete(queue,0);
				ds_map_delete(obj.args,current);
				ds_map_delete(obj.arg_to_script,current);
			}
			
			// push to the end
			duel_animation_return_menu();	
		}

	}

return noone;
