var val = -1;

with(deckBuild){
	val = selected_card.card_sprite_ref;	
}

if(val == -1){
	with(cardList){
		val = selected_card.card_sprite_ref;	
	}	
}

var obj = card_info_show(card_id,val)