var obj = instance_create(0,0,animationWindow)

var windowCharPerLine = 22

obj.message = textForceCharsPerLine(argument[0],windowCharPerLine);
if(object_is_ancestor(object_index,menuParent) || object_index == menuParent){
	obj.menu_draw = menu_draw;
}

var counter = string_count("\n",obj.message)+1
obj.height = 30+30 + counter*20

tts_say(obj.message)

return obj;