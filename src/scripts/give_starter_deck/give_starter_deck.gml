
// gets the count of the items
var filename = working_directory + "resources\\deck\\deck"+string(argument[0]);;
var ext = ".ini";
var testname = "";

testname = filename+ext;

if(file_exists(testname)){
	ini_open(testname);
	var card = give_card(ini_read_string_length("card","main","",40));
	obj_data.current_main_card = -1
	
	for(var i = 0; i < 20; i++){
		card = card_from_name(ini_read_string_length("card","card" + string(i),"",40));
		if(card > -1){
			ds_list_add(obj_data.current_deck,card)
		}
	}
	
	ini_close();
}