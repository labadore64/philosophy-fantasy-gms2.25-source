var command = "";
var step = argument[0];
var le_card = -1;
var active_card = parent.part[DUEL_FIELD_ACTIVE].card_array[0];

for(var i = 0; i < card_array_size; i++){
	le_card = card_array[i];
	
	if(parent.ai_phase[step] == DUEL_PHASE_MAIN){
		if(parent.ai_switch_counter[step] == 0){
			if(newAI_hand_test_activate(le_card,active_card,step)){
				command += "activate" + ":" +code_set_cardstate(id,i,true,true) + ","
			}
		}
	
		if(parent.ai_overlay_counter[step] == 0){
			if(parent.part[DUEL_FIELD_ACTIVE].card_array_size > 0){
				if(newAI_hand_test_overlay(le_card,active_card,parent)){
					command += "overlay" + ":" +code_set_cardstate(id,i,true,true) + ","
				}
			}
		}
	}
	else if(parent.ai_phase[step] == DUEL_PHASE_END){ 
		if(card_array_size > hand_size_limit){
			command += "discard_end" + ":" +code_set_cardstate(id,i,true,true) + ","
		}
	}
	
}

return command;