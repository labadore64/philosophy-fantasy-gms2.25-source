var do_camera = true;

with(drawMoveCard){
	do_camera = move_camera;	
}

if(do_camera){
	if(enemy){
		var posx = 325
		var posy = 445-200
	
		var spacer = 20;

		duel_camera_focus_object(posx,posy+spacer*menupos,5,1.45);
		duelController.pointer_x = 320+75
		duelController.pointer_y = 430-200+50+spacer*menupos
	} else {
		var posx = 325
		var posy = 420
	
		var spacer = 20;

		duel_camera_focus_object(posx,posy-spacer*menupos,5,1.45);
		duelController.pointer_x = 320+75
		duelController.pointer_y = 430+50-spacer*menupos
	}
}
duel_activemon_update();
duel_activemon_display_info();
info_surf_update = true;