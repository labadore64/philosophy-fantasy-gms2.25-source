
	gml_pragma("forceinline");

	if(destroy > 0){
		destroy--;
	} else if(destroy == 0){
		instance_destroy();
	}

	if(objGenericCanDo()){
		keypress_this_frame = false;
		//get axis data
		

		if(!check_input_select_pressed()){
			check_input_cancel_pressed();	
		}
			
		check_input_select();

	
		check_input_control_pressed();

		check_input_left_pressed();
		check_input_right_pressed();
		check_input_up_pressed();
		check_input_down_pressed();
	
		check_input_left();
		check_input_right();
		check_input_up();
		check_input_down();

		check_input_left_released();
		check_input_right_released();
		check_input_up_released();
		check_input_down_released();
		
		check_input_lefttab()
		check_input_righttab()

		if(global.inputEnabled){
			if(!keypress_this_frame){
				for(var i = 0; i < 10; i++){
					if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_ACCESS1+i]) ||
						global.gamepad_state_array[KEYBOARD_KEY_ACCESS1+i] == KEY_STATE_PRESS) {
							tts_say(access_string[i])
							keypress_this_frame= true
					}
				}
			}
		
			if(!keypress_this_frame){
				if(can_viewcard){
					if(viewcard_id > -1){
						if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_VIEWCARD]) == KEY_STATE_PRESS ||
							global.gamepad_state_array[KEYBOARD_KEY_VIEWCARD] == KEY_STATE_PRESS){
							viewCard_menu();	
							keypress_this_frame= true
						}
					}
				}
			}
		}
		
		if(keypress_this_frame){
			global.le_mouse = false;
		}

	}

