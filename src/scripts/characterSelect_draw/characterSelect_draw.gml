	if(!surface_exists(surf)){
		surf = surface_create_access(global.window_width,global.window_height)	
		surface_update=true
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,global.window_width,global.window_height);
			surface_update=true
		}
	}
	
draw_set_color(global.text_fgcolor)
draw_set_alpha(1)
if(surface_update){
	surface_set_target(surf)

	draw_clear_alpha($000000,1);

	draw_set_font(global.normalFont)
	draw_set_halign(fa_center)
	if(!newgame){
		draw_text_transformed_noratio(600,40,global.character_name,3,3,0)
		draw_text_transformed_noratio(600,100,"Select Appearance\nwith directional keys.",2,2,0)
		draw_text_transformed_noratio(600,150,"Press select\nto continue.",2,2,0)
	}
	
	
	if(mouse_mode){
		var xx = 600
		var yy = 300
		
			// draw nav buttons
			var col = c_white;
			if(pan_up){
				col = c_red	
			}
			draw_sprite_extended_noratio(spr_point_arrow,0,xx,yy,
										1,1,0,col,1)
			var col = c_white;
			if(pan_down){
				col = c_red	
			}
			draw_sprite_extended_noratio(spr_point_arrow,0,xx,yy+125,
										1,-1,0,col,1)
			var col = c_white;
			if(pan_right){
				col = c_red	
			}					
			draw_sprite_extended_noratio(spr_point_arrow,0,xx+62,yy+62,
										1,1,270,col,1)
			var col = c_white;
			if(pan_left){
				col = c_red	
			}
			draw_sprite_extended_noratio(spr_point_arrow,0,xx-62,yy+62,
										-1,1,90,col,1)
										
			draw_text_transformed_noratio(xx,yy+250-15,"Confirm",3,3,0)
	}
	draw_set_halign(fa_left)
	surface_reset_target()
	surface_update = false;
}

draw_surface(surf,global.display_x,global.display_y);	

with(fadeOut){
	draw_fade()	
}
with(fadeIn){
	draw_fade()	
}