if(!argument[0]){
	var target_index = -1; // the card index to move
	var target_origin = -1; // the location of the target
	var target_destination = -1; // where to move the target
	//var target_side = duelController.active_player; // which side the target is on
	var dest_value = -1;
	var dest_string = "";
	var query_value = "";

	var return_value = 0;


	var this = code_get_this();

	var this_enemy = false;
	var target_side = DUEL_PLAYER_YOU
	if(string_pos("pos_enemy",this) > 0){
		this_enemy = true;
		target_side = DUEL_PLAYER_ENEMY
	}

	var return_variable = "return"

	var stringer = "";
	for(var i = 0; i < code_argument_size; i++){
		// parse destination
		if(string_pos("dest:",code_arguments[i]) > 0){
			dest_string = string_copy(code_arguments[i],string_pos(":",code_arguments[i])+1,
											string_length(code_arguments[i]) - string_pos(":",code_arguments[i]));
			dest_value = code_get_location(dest_string,target_side);
		}
	
		// parse search conditions
		if(string_pos("query:",code_arguments[i]) > 0){
			query_value = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
		if(string_pos("return",code_arguments[i]) > 0){
			return_variable = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		
			// the variable must exist. this forces uses of the predefined registers
			// to prevent any funny business
			if(!ds_map_exists(duelController.code_values,return_variable)){
				return_variable = "return";	
			}
		}
	}

	// 
	if(dest_string != "" || dest_value > 0){
		
		duel_animation_wait(30)
		
		if(global.camera_track == CAMERA_TRACK_ALL){

			if(dest_value > -1){

				if(instance_exists(dest_value)){
					with(duelCamera){
						if(duelController.mouse_mode){
							var modi = 0;
							var scale = 2.75;
			
			
							if(dest_value.enemy){
								modi = -550
							}
					
							if(dest_value.object_index == duelActive){
								modi = -(290+290-240)
							}
							
							if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
								modi -= TOOLTIP_CAMERA_ADJUST_Y
								scale = TOOLTIP_CAMERA_ADJUST_SCALE
							}
			

							duel_animation_move_camera(225+110,290+290+modi,scale,10)

						}
					}
				}
			}
		}	
		
		// if limbo or deck, you want to open up the card list.
		if(dest_value > 0){
			
			if(duelController.active_player == DUEL_PLAYER_YOU){
				duel_animation_pause("Choose a card from your " + dest_value.source_name);
			} else {
				duel_animation_pause("Selecting a card from the " + dest_value.source_name);
			}
			
			if(dest_value.object_index == duelDeck ||
				dest_value.object_index == duelWaiting){
				// opens the menu.
				// simple and will only open the menu and not allow you to exit.
				duel_animation_card_list(dest_value,target_side,query_value);
			}
		} else {
			// otherwise select the card on the field.	
			// note, you can support multiple locations with this method
			// including limbo/deck.
			// it will temporarily change the function of both to enter select
			// mode.
		
			// get the list of valid locations added
			// also add the query.
			duel_animation_card_select(string_replace_all(dest_string,"\"",""),target_side,query_value);
		}
	}
}
// set the value here

//ds_map_replace(duelController.code_values,return_variable,return_value)