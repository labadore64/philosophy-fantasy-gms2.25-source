var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _x = map[? "x"];
	var _y = map[? "y"];
	var _scale = map[? "scale"];
	var _card_id = map[? "card_id"];
	var _anim = map[? "anim"]
	var _time = map[? "time"]
	
	ds_map_destroy(map)
	
	if(!is_undefined(_x) &&
		!is_undefined(_y) &&
		!is_undefined(_scale) &&
		!is_undefined(_card_id) &&
		!is_undefined(_anim) &&
		!is_undefined(_time)){
		var obj = noone;
		if(_card_id > -1){
			var obj = instance_create(0,0,animationShowCard);
			obj.card = card_create(_x,_y,_scale,_card_id)
			obj.card.card_sprite_ref = obj_data.card_sprite[_card_id]
			obj.timer = _time;
			with(obj.card){
				card_update()	
				script_execute(_anim);	
			}
		} else {
			show_debug_message("fyi test for desync")	
		}
		
		
		
		return obj;
	}

}

return noone;