var returner = -1;

with(obj_data){
	if(card_sprite[argument[0]] == review_philosopher){
		var spritename = working_directory + "resources\\card\\" + global.cardpack + "\\sprite\\fg\\spr_card_" + card_sprite_name[argument[0]] + ".png";
		card_img[card_img_size] = sprite_add(spritename, 0, false, false, 0, 0)
		card_sprite[argument[0]] = card_img[card_img_size]
		returner = card_img[card_img_size]
		card_img_size++;
	}
}

return returner;