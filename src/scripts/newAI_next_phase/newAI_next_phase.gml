var field_obj = argument[0];
var i = argument[1];
var curplay = argument[2]

ai_phase[i] = duelController.phase_current
ai_turn[i] = duelController.turns

newAI_load_state(i);

var state = power(2,ANIMATION_MOVE_FLAG_IGNORE_SOUND);
				
// add a card to the players hand
if(duelController.phase_current == DUEL_PHASE_DRAW){
	var count = field_obj.part[DUEL_FIELD_HAND].card_array_size;
					
	if(count >= 5){
		duel_animation_move_card(0,field_obj.part[DUEL_FIELD_DECK],field_obj.part[DUEL_FIELD_HAND],
										false,false,state)
	} else {
		repeat(5-count){
			duel_animation_move_card(0,field_obj.part[DUEL_FIELD_DECK],field_obj.part[DUEL_FIELD_HAND],
																	false,false,state)	
		}
	}
}
		
// add limbo cards to the deck
if(duelController.phase_current == DUEL_PHASE_LIMBO_RETURN){
	var size = field_obj.part[DUEL_FIELD_LIMBO].card_array_size;
			
	var counter = 0;
			
	for(var j = size-1; j >= 0; j--){
		if(!card_has_keyword(field_obj.part[DUEL_FIELD_LIMBO].card_array[j],"permadeath")){
			counter++;
			duel_animation_move_card(0,field_obj.part[DUEL_FIELD_LIMBO],field_obj.part[DUEL_FIELD_DECK],
							false,false,state)
		}
	}
	
	if(counter > 0){
		duel_animation_shuffle(field_obj.part[DUEL_FIELD_DECK])
	}
}
				
if(duelController.phase_current == DUEL_PHASE_BATTLE){
	// calculate battle then move forward.
	newAI_calculate_battle(curplay)
}
		
newAI_copy_state(i);