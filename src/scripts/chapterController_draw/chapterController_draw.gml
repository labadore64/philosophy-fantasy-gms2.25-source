draw_set_alpha(1)
// draw text
var hei = 90;
var push = 130;
draw_rectangle_color_ratio(20,push+hei,100,push+hei+5,c_black,c_white,c_white,c_black,false)
draw_rectangle_color_ratio(100,push+hei,500,push+hei+5,c_white,c_black,c_black,c_white,false)

draw_set_color(global.text_fgcolor)
draw_set_font(global.largeFont)
draw_text_transformed_ratio(40,push+40,chapter,2,2,0)
draw_set_font(global.normalFont)
draw_text_transformed_ratio(40,push+120,chapter_name,2,2,0)


with(card){
	if(card_id > -1){
		if(!surface_exists(surf)){
			card_reset_size();
			surf = surface_create(card_width,card_height);	
			surface_update = true
		} else {
			if(ScaleManager.updated){
				card_reset_size();
				surface_resize(surf,card_width,card_height)	
				surface_update = true
			}
		}

		if(surface_update){
			card_update_surface();
	
			if(card_sprite != -1){
				sprite_delete(card_sprite);	
			}
			card_sprite = sprite_create_from_surface(surf, 0, 0, card_width, card_height, false, false, card_width*.5, card_height*.5);
			surface_update = false;
		}

		if(global.menu_index > -1){
			draw_rectangle_color_ratio(0,490,800,550,$444444,$444444,c_black,c_black,false)

			gpu_set_blendmode(bm_add)
			if(card_sprite > -1){
				var shadow = 380
				draw_sprite_pos_ratio(card_sprite,0,
								spec_xx[0],spec_yy[0]+shadow,
								spec_xx[1],spec_yy[1]+shadow,
								spec_xx[2],spec_yy[2]+shadow,
								spec_xx[3],spec_yy[3]+shadow,
								$aaaaaa,
								.15)	
		
			}
		}
		gpu_set_blendmode(bm_normal)
		draw_set_color(c_black)
		draw_rectangle_ratio(0,400,800,490,false)
		//draw_sprite_extended_ratio(sprite2,0,10,10,card_scale,card_scale,0,c_white,1)	
		draw_sprite_pos_ratio(card_sprite,0,
						xx[0],yy[0],
						xx[1],yy[1],
						xx[2],yy[2],
						xx[3],yy[3],
						c_white,
						image_alpha)	
	}
}

with(fadeIn){
	draw_fade()	
}
with(fadeOut){
	draw_fade()	
}
