	if(global.story_next == "main"){
		room_goto(MainRoom);
	} else if(global.story_next == "last"){
		var filename = working_directory + "resources\\story\\"+global.story_last[1]+".json";
		if(file_exists(filename)){
			story_load_element(global.story_last[1]);
		} else {
			room_goto(MainRoom);
		}
	} else {
		var filename = working_directory + "resources\\story\\"+global.story_next+".json";
		if(file_exists(filename)){
			story_load_element(global.story_next);
		} else {
			room_goto(MainRoom);
		}
	}