	if(!surface_exists(bg_surf)){
		bg_surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){
			surface_resize(bg_surf,global.window_width,global.window_height);
		}
	}
	var duel = instance_exists(duelController);
	surface_set_target(bg_surf)
	gpu_set_colorwriteenable(true,true,true,true)

	draw_set_alpha(1)
	if(global.color_gradient){
		draw_rectangle_color_ratio(0,0,800,600,
								global.screen_bgcolor[0],global.screen_bgcolor[0],
								global.screen_bgcolor[1],global.screen_bgcolor[1],
										false)
	} else {
		draw_set_color(global.screen_bgcolor[0])
		draw_rectangle_ratio(0,480,800,600,
									false)
	}
	

	gpu_set_colorwriteenable(true,true,true,false)
	if(global.menu_index > -1){
		gpu_set_blendmode(bm_add)
		if(global.menu_animate){
			draw_sprite_extended_ratio(global.menu_index,ScaleManager.spr_counter,400,300,2,2,0,$181818,1)
		} else {
			draw_sprite_extended_ratio(global.menu_index,75,400,300,2,2,0,$181818,1)
		}
		gpu_set_blendmode(bm_normal)
	}
	if(!duel){
		draw_set_color($191919);
		draw_rectangle_color_ratio(400-10+25,448+80,800,600,$191919,$282828,$191919,$191919,false)
		draw_rectangle_color_ratio(0,0,800,58,$080808,$080808,$080808,$282828,false)
	} else {
		draw_set_color($111111);
		draw_rectangle_color_ratio(400-10+25,448+80,800,600,$111111,c_black,c_black,$111111,false)
	}
	

	if(global.particles){part_system_drawit(Sname);}
	gpu_set_colorwriteenable(true,true,true,true)
	surface_reset_target();
	
	/*
	var counter = shader_counter
	with(deckBuild){
		counter = shader_counter
	}
	
	with(duelBackground){
		counter = shader_counter
	}
	
	with(PackSelect){
		counter = shader_counter
	}
	*/

	draw_surface(bg_surf,0,0);	

	
	if(duel){
		draw_set_color(c_black)
		draw_rectangle_ratio(0,0,800,59,false)	
	}