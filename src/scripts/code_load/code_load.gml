with(duelController){
	var fullstring = argument[0];
	var index = string_pos(":{",fullstring);
	if(index == 0){
		index = string_pos(" {",fullstring);
	}
	fullstring = string_copy(fullstring,index+2,string_length(fullstring)-index-2);
	code_loaded[0]="";
	var counter = 0;
	var len = 0;
	var pos = 1;
	var startpos = -1;
	
	while(pos > 0){
		// cut out any spaces first
		var space = 0;
		while(string_char_at(fullstring,space) == " "){
			space++;
		}

		pos = string_pos(";",fullstring);
		if(pos > 0){
			len = string_length(fullstring);
			code_loaded[counter] += string_copy(fullstring,space,pos-space+1);

			fullstring = string_delete(fullstring,1,pos+space+1)

			if(pos < len){
				counter++;	
				code_loaded[counter]="";
			}
		}
	}
	code_length = counter+1;
}