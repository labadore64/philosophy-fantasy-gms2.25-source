var returner = "";

with(duelController.field[argument[0]]){
	// clear ai objects
	if(duelController.ai_resume_point == 0){
		duelController.ai_mode = true
		duelController.ai_side = argument[0];
		with(duelController){
			temp_phase_current = phase_current
			temp_turns = turns
		}
	
		with(ai_duelField){
			ai_depth = 0;
		
			newAI_init_ai_field();
			for(var i = 0; i < ai_part_count; i++){
				newAI_init_ai_obj_inside(part[i]);	
			}	
		
			current_hp = duelController.duel_hp[argument[0]].hp_current
			current_hp_enemy = duelController.duel_hp[!argument[0]].hp_current
		}
	}
	
	with(ai_field){
		// start AI exectuion
		returner = newAI_exec(argument[0]);
		
		// clear fields
	}
	
	if(duelController.ai_resume_complete){
		show_debug_message("AI complete!")
		duelController.ai_resume_point = 0
		duelController.ai_resume_complete = false
		with(ai_duelField){
			for(var i = 0; i < ai_part_count; i++){
				newAI_close_ai_obj(part[i]);	
			}	
		}
		
		with(duelController){
			phase_current = temp_phase_current
			turns = temp_turns
		}
		duelController.ai_mode = false
		
		return returner;
	}
}

return "";