/// @function gamepadToString(gp_button)
/// @description Turns a gamepad button into a string.
/// @param {real} gp_button The gamepad button.

	var key = argument[0];
	if(key != 0){

		switch(key){
			case gp_face1:
			return "Button1"
		
			case gp_face2:
			return "Button2";
	
			case gp_face3:
			return "Button3";
	
			case gp_face4:
			return "Button4";
	
			case gp_padd:
			return "D-Down";
	
			case gp_padl:
			return "D-Left";
	
			case gp_padr:
			return "D-Right";
	
			case gp_padu:
			return "D-Up";
	
			case gp_start:
			return "Start";
	
			case gp_select:
			return "Select"
	
			case gp_shoulderl:
			return "L-Shoulder 1"
	
			case gp_shoulderlb:
			return "L-Shoulder 2"
	
			case gp_shoulderr:
			return "R-Shoulder 1"
	
			case gp_shoulderrb:
			return "R-Shoulder 2"
	
			case gp_stickl:
			return "L-Stick"
	
			case gp_stickr:
			return "R-Stick"
		}
	}