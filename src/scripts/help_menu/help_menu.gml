if(argument[0] != ""){
	var name = argument[0];

	var obj = instance_create(0,0,menuHelp);
	obj.help_file = name;

	with(obj){
		event_perform(ev_alarm,2)
	}
	
	soundfxPlay(sfx_sound_help_open)
}