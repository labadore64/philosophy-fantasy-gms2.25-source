if(!instance_exists(navigation_menu)){
	ax_string_clear();
	with(duelSub){
		instance_destroy();	
	}
	with(menuTooltip){
		instance_destroy()	
	}
	// check the special case where its a right click that is in the space of a card
	var cando = false

	var selected = -1

	if(mouse_check_button_pressed(mb_right)){
		/*
		if(global.mouse_right){
			if(MouseHandler.mousepos_x < global.window_width - mouse_mode_w*global.scale_factor){
				cando = true	
			}
		} else {
			if(MouseHandler.mousepos_x > mouse_mode_w*global.scale_factor){
				cando = true	
			}
		}
		*/
		if(cando){
			for(var i = 0; i < mouse_card_count; i++){

				if(rectangle_in_rectangle(cursor.test_area[0],cursor.test_area[1],
									cursor.test_area[2],cursor.test_area[3],
									mouse_card[i,0],mouse_card[i,1],
									mouse_card[i,2],mouse_card[i,3]) > 0){
					selected = i
					break;
				}
			}
		}
	}

	if(selected > -1){
		do_context_menu = true;
		context_menu_x = (MouseHandler.mousepos_x/global.scale_factor) -global.display_x
		context_menu_y = ((MouseHandler.mousepos_y)/global.scale_factor) - global.display_y
		context_menu_select = mouse_card_menupos[mouse_card_selected]
		context_menu_obj = mouse_card_element[mouse_card_selected]
	
		if(!keypress_this_frame){
			if(menu_select != -1){
				script_execute(menu_select)	
			}
		}
		do_context_menu = false
	} else {
		if(input_active && !resume_control){
			if(!disable_mouse_mode){
				if(running_obj < 0){
					if(duelController.click_countdown <= -1){
					if(active_player == DUEL_PLAYER_YOU){
						if(!duel_camera_moving()){
							if(instance_exists(selected_object)){
								with(selected_object){
									if(menu_cancel != -1){
										script_execute(menu_cancel);	
										RUMBLE_MOVE_MENU
									} else {
										if(duelController.phase_current == DUEL_PHASE_MAIN ||
											duelController.phase_current == DUEL_PHASE_RETURN ||
											duelController.phase_current == DUEL_PHASE_STANDBY){
											if(duelController.active_player == DUEL_PLAYER_YOU){
												duelController_continue_menu();
												RUMBLE_MOVE_MENU
											}
										}
									}
								}
							} else {
								duelController_continue_menu();	
							}
							duelController.click_countdown = 2
						}
					}
					}
				}
			}
		}

	}
}