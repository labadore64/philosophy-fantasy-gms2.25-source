// save the replay file
file_copy(global.replay_temp,string_replace_all(global.replay_temp,"temp_",""));
	
// delete the replay file
file_delete(global.replay_temp);

soundfxPlay(sfx_sound_confirm)
instance_destroy();