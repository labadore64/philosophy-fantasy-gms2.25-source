with(argument[0]){
	var key = argument[1]
	var value = status_effects[? key]
	if(!is_undefined(value)){
		return value > 0;	
	}
}

return false;