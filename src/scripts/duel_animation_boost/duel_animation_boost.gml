
var _target = argument[2]
var _att = argument[0]
var _def = argument[1]

if(!instance_exists(duelController.animation_stack_run) ||
	duelController.animation_stack_run.object_index == animationQueue){
		
	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	
	duelController.running_obj = obj;
	
	if(queue > -1 && ds_exists(queue,ds_type_list)){
		var counter = ds_list_size(queue);


		var json = ds_map_create();
		ds_map_add(json,"att",_att)
		ds_map_add(json,"def",_def)
		ds_map_add(json,"enemy",_target)
		//ds_map_add(json,"this",code_get_this())
		
		duel_animation_pack(obj,queue,json,duel_anim_queue_code_boost)
	}
}
