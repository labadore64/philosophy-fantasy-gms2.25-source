/// @description Insert description here
// You can write your code in this editor
if(global.mouse_active ){
	with(duelController){
		if(menuControl.active_menu == id){
			
		with(duelCursor){
			x = MouseHandler.mousepos_x
			y = MouseHandler.mousepos_y
		}
		if(!disable_mouse_mode){
			if(duelCursor.xprevious != MouseHandler.mousepos_x||
				duelCursor.yprevious != MouseHandler.mousepos_y){
				duelController.mouse_mode = true;		
			}
			
		if(running_obj < 0 || code_select_active){
		if(duelController.click_countdown <= -1){
			with(duelCursor){
				if(!MouseHandler.ignore_menu){
					if(menuControl.active_menu > 0){
						if(menuControl.active_menu.object_index == duelController){
						
							var scaled = false;
							with(duelCamera){
								if(duelController.active_player == DUEL_PLAYER_YOU){
									if(mouse_wheel_down()){
										with(duelSub){
											if(context_menu){
												instance_destroy();	
											}
										}
										scale+=scale_counter*global.zoom_speed;
										scaled = true;
									} else if (mouse_wheel_up()){
										with(duelSub){
											if(context_menu){
												instance_destroy();	
											}
										}
										scale-=scale_counter*global.zoom_speed;	
										scaled = true;
									}
									if(scale > max_scale){
										scale = max_scale	
									} else if(scale < min_scale){
										scale = min_scale	
									}	
								}
							}
						
							if(scaled){
								with(duelNavigation){
									duelNavigation_update_zoom()
								}
							}
						
							if(!scaled && duelController_is_not_panning()){
								// do pan
								if(mouse_check_button_pressed(mb_left)){
									if(duelController.mouse_card_selected < 0 &&
										duelController.context_menu < 0 &&
										duelController.navigation_menu < 0){
										var cantdo = false;
										with(duelController){
											cantdo = (rectangle_in_rectangle(cursor.test_area[0],cursor.test_area[1],
																							cursor.test_area[2],cursor.test_area[3],
																							nav_pos[0],nav_pos[1],
																							nav_pos[2],nav_pos[3]) == 0);	
										}
	
										if(cantdo){
											move_countdown = 3;
											with(duelSub){
												if(context_menu){
													instance_destroy()	
												}
											}
											with(menuTooltip){
												instance_destroy()	
											}
											pan_x = window_mouse_get_x()
											pan_y = window_mouse_get_y()
											cam_x = duelCamera.x
											cam_y = duelCamera.y
											duel_cursor_sprite = spr_duel_rotate_cursor
										}
									}
								} else if(mouse_check_button(mb_left)){
									if(duelController.mouse_card_selected < 0 &&
										duelController.context_menu < 0 &&
										duel_cursor_sprite == spr_duel_rotate_cursor){
										if(move_countdown > -1){
											move_countdown -= ScaleManager.timer_diff
											if(move_countdown < -1){
												move_countdown = -1;	
											}
										} else {
											duelCamera.x = cam_x+(pan_x-window_mouse_get_x())*duelCamera.scale*.5
											duelCamera.y = cam_y+(pan_y-window_mouse_get_y())*duelCamera.scale*.5
										}
									}
								} else {
									move_countdown = -1;
									if(mouse_check_button_released(mb_left)){
										
										duel_cursor_sprite = spr_duel_cursor	
									}
									
									/*
									if(xprevious != mouse_x ||
										yprevious != mouse_y){
										duelController.mouse_mode = true;		
									}
									*/
									var moverx = duelCamera.left+((window_mouse_get_x()-global.display_x)/global.letterbox_w)*(duelCamera.origin_width)
									var movery = duelCamera.top+((window_mouse_get_y()-global.display_y)/global.letterbox_h)*(duelCamera.origin_height)
								
									test_area[0] = test_xoffset+ moverx
									test_area[1] = test_yoffset+ movery
									test_area[2] = test_xoffset+moverx+test_width
									test_area[3] = test_yoffset+movery+test_height

									duelNavigation_update_pos()

									if(global.inputEnabled){
										if(keyboard_check(global.keybind_value_array[KEYBOARD_KEY_CAMERA_LEFT]) ||
											global.gamepad_state_array[KEYBOARD_KEY_CAMERA_LEFT] == KEY_STATE_HOLD){
											duelCamera_pan_left()
										} else if(keyboard_check(global.keybind_value_array[KEYBOARD_KEY_CAMERA_RIGHT]) ||
											global.gamepad_state_array[KEYBOARD_KEY_CAMERA_RIGHT] == KEY_STATE_HOLD){
											duelCamera_pan_right()
										} else if(keyboard_check(global.keybind_value_array[KEYBOARD_KEY_CAMERA_UP]) ||
											global.gamepad_state_array[KEYBOARD_KEY_CAMERA_UP] == KEY_STATE_HOLD){
											duelCamera_pan_up()
										} else if(keyboard_check(global.keybind_value_array[KEYBOARD_KEY_CAMERA_DOWN]) ||
											global.gamepad_state_array[KEYBOARD_KEY_CAMERA_DOWN] == KEY_STATE_HOLD){
											duelCamera_pan_down()
										}			
									}
								}
							}

							}
						}
					}
				}
			}
		}
		}
		}
	}
}