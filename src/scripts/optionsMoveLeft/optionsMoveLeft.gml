
if(menu_count > 0){

	if(option_number_count > 1){
		var menupos_old = menupos
		option_top = 0;

		for(var i = 0;i < option_number_count; i++){
			if(menupos_old == option_number[i]){
				if(i > 0){
					menupos = option_number[i-1]
					break;
				}
			}
		
			if(i == option_number_count-1){
			
				if(menupos_old >= option_number[i] &&
					menupos_old <= menu_count-1){
						menupos = option_number[i-1]
						break;
				}
			} else {
				if(menupos_old >= option_number[i] &&
					menupos_old < option_number[i+1]){
						menupos = option_number[i]
						break;
				}
			}
		}


		if(menupos < 0){
			menupos = 0;	
		}
	} else {
		menupos = 0;
		option_top = 0;
		selected_draw_index  = 0
	}
	var posi = menupos;
	with(MouseHandler){
		mouse_active_position = posi 	
		ignore_menu = true	
	}
	
	if(option_number_count > 1){
		option_top = menupos
	
		if(option_top > menu_count-per_line){
			option_top = menu_count-per_line;
		}
	}

	menuParent_sound()

	RUMBLE_MOVE_MENU


	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}

	if(global.voice){
		tts_say(menu_name[menupos])	
	
		if(global.speak_details){
			tts_say(menu_desc[menupos])	
		}
	}

	surface_update = true


	if(option_number_count > 1){
		// do option top stuff
		if(menupos_old != menupos){
			if(option_top < 0){
				option_top = 0
			}
		
			//calculate selected from option top
			var counter = 0;
			for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
				if(i == menupos){
					selected_draw_index = counter;
					break;
				}
				counter++;
			}
		}
	}
	surface_update=true
}