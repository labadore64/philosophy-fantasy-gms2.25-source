// should be in all scripts
var index = argument[1];
var origin = argument[0];

with(origin){

	var mon = card_array[index]
	//if there's already a card in the overlay zone
	if(parent.part[DUEL_FIELD_ACTIVE].card_array_size > 1){
		var card = parent.part[DUEL_FIELD_ACTIVE].card_array[1];
		duel_animation_move_card(1,parent.part[DUEL_FIELD_ACTIVE],parent.part[DUEL_FIELD_LIMBO])	
	}
	
	if(!duelController.ai_mode){
		duel_animation_play_sound(sfx_sound_duel_overlay_card)
		duel_animation_activate_card(mon)
		if(mon > -1){
			tts_say(obj_data.card_name[mon] + " Overlayed");
		}
	}
	duel_animation_move_card(index,id,parent.part[DUEL_FIELD_ACTIVE],
										false,false,power(2,ANIMATION_MOVE_FLAG_IGNORE_SOUND))
	//duel_animation_wait(20)
	////duel_animation_resume_control()
	//duel_animation_play_sound(sfx_sound_duel_overlay_card)
	if(duelController.ai_mode){
		parent.ai_overlay_counter[parent.ai_counter]++;
	} else {
		parent.overlay_counter++;
	}
	duelController.speak_tts = false;

}