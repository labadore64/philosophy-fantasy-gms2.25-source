if(card_array_size > 0){
	if(!card_facedown[menupos] || !enemy){
		var obj = instance_create(50,150,duelSub);
		obj.menu_selected = card_array[menupos];
		var me = id;

		if(duelController.do_context_menu){
			obj.context_menu = true;
			soundfxPlay(sfx_sound_duel_ax_select)
			obj.option_spacer = 40
			// set the coordinates
			menupos = duelController.context_menu_select
		}
		
		with(obj){
			var windowCharPerLine = 30
			if(menu_selected > -1){
				card = card_create(650,290,.5,menu_selected);
				card.card_sprite_ref = obj_data.card_sprite[menu_selected]
				with(card){
					card_update()	
				}
			}
			menu_add_option("View","View this card.",duelSub_view,spr_menu_info)
			
			code_menu_select_option(me);
			
			//event_user(0)
			//event_user(0)
			menu_height = 20 + option_spacer * menu_count;
			
			y = -menu_height*.5 + 300
			
			//event_user(0)
			if(!duelController.do_context_menu){
				if(menu_count == 1){
					menu_width -= 50;
					x+=30
				}
				if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
					y+=50;
					card.y+=50;
					adjust_me = 50;
					
					with(card){
						card_update()	
					}
				}
				menu_push(id)
			} else {
				duelController_context_menu_adjust();
			}
		}
	}
}