var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _x = map[? "x"];
	var _y = map[? "y"];
	var _scale = map[? "scale"];
	var _time = map[? "time"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_x) &&
		!is_undefined(_y) &&
		!is_undefined(_scale) &&
		!is_undefined(_time)){
			
		duel_camera_focus_object(_x,_y,_time,_scale);
		var obj = instance_create(0,0,animationTimer);
		obj.timer = _time;
		
		return obj;
	}

}

return noone;