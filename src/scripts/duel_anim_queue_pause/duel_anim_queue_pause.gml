var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _time = map[? "msg"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_time)){
		var obj = noone;
		if(global.pause_anim || global.voice){
			obj = instance_create(0,0,animationPause);
		}
		if(_time != ""){
			tts_say(_time);	
		}
		// wait 5 cycles so that the sound effect has buffer
		duel_animation_wait(5)
		return obj;
	}

}

return noone;