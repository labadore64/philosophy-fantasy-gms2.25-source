if(cancel_button_enabled){
	draw_set_alpha(1);
	if(cancel_draw_border){
		draw_rectangle_noratio(cancel_button_x - cancel_button_width*.5,
						cancel_button_y - cancel_button_height*.5,
						cancel_button_x + cancel_button_width*.5,
						cancel_button_y + cancel_button_height*.5,
						false)
	}
	if(cancel_sprite > -1){
		draw_sprite_extended_noratio(cancel_sprite,0,
									cancel_button_x, cancel_button_y,
									1,1,
									0,c_white,1);
	}	
}
