var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _card_id = map[? "card_id"];
	var _card_origin = map[? "origin"];
	var _card_dest = map[? "dest"];
	var _card_dest_id = map[? "dest_card_id"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_card_id) &&
		!is_undefined(_card_origin) &&
		!is_undefined(_card_dest) &&
		!is_undefined(_card_dest_id)){
			
						
		var le_card = -1;
		if(_card_id > -1){
			le_card = _card_origin.card_array[_card_id];
		}
		
		var le_card2 = -1;
		if(_card_dest_id > -1){
			le_card2 = _card_dest.card_array[_card_dest_id];
		}
		
		var obj = duel_anim_swap_card(
						_card_id,_card_dest_id,
						_card_origin,_card_dest,
						7)
		// do effect
		if(_card_id > -1){
			
			var this = code_set_cardstate(_card_dest,_card_dest_id)
			with(_card_dest){
				
				if(object_index == duelHand){
					duel_animation_card_effect(le_card,"hand",this);	
			
				} else if(object_index == duelDeck){
			
					duel_animation_card_effect(le_card,"deck",this);	
			
				} else if(object_index == duelWaiting){
			
					duel_animation_card_effect(le_card,"limbo",this);	
			
				} else if(object_index == duelTheory){
					duel_animation_card_effect(le_card,"theory",this);	
			
				} else if (object_index == duelActive){
					if(_card_dest_id == 0){
						duel_animation_card_effect(le_card,"active",this);		
					} else {
						duel_animation_card_effect(le_card,"overlay",this);	
					}
			
				} else if (object_index == duelBench){
					duel_animation_card_effect(le_card,"bench",this);	
				}
	
			}
		}
		
		// do effect
		if(_card_dest_id > -1){
			
			var this = code_set_cardstate(_card_origin,_card_id)
			with(_card_origin){

				if(object_index == duelHand){
					duel_animation_card_effect(le_card2,"hand",this);	
			
				} else if(object_index == duelDeck){
			
					duel_animation_card_effect(le_card2,"deck",this);	
			
				} else if(object_index == duelWaiting){
			
					duel_animation_card_effect(le_card2,"limbo",this);	
			
				} else if(object_index == duelTheory){
					duel_animation_card_effect(le_card2,"theory",this);	
			
				} else if (object_index == duelActive){
					if(_card_id == 0){
						duel_animation_card_effect(le_card2,"active",this);		
					} else {
						duel_animation_card_effect(le_card2,"overlay",this);	
					}
			
				} else if (object_index == duelBench){
					duel_animation_card_effect(le_card2,"bench",this);	
				}
	
			}
		
		}
						
		if(obj > -1){
			obj.sound_id = soundfxPlay(sfx_sound_play_card);
		}
		return obj;
	}

}

return noone;