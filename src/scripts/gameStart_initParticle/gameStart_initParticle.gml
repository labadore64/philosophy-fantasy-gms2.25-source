// particles

Sname = part_system_create();

var scale = 1;

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_pixel);
part_type_scale(particle1,scale,scale);
if(!global.enableShader){
	part_type_color1(particle1,$7F7F00);
} else {
	part_type_color1(particle1,$333333);	
}
part_type_alpha3(particle1,0,1,0);
part_type_life(particle1,15,120);
part_type_blend(particle1,true)

quantity = 2
part_type_alpha3(particle1,0,.25,0);
part_type_size(particle1,10,10,.2,0);
part_type_speed(particle1,.025,.5,0,0)
part_type_direction(particle1,0,0,0,5)
part_type_gravity(particle1,.02,180)

emitter1 = part_emitter_create(Sname);
part_emitter_region(Sname,emitter1,
					global.display_x,
					global.window_width,
					global.display_y,
					global.window_height,
					ps_shape_rectangle,
					ps_distr_linear);

part_system_automatic_draw(Sname, false)
part_system_automatic_update(Sname,false)

part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
repeat(500){
	if(global.particles){part_system_update(Sname)}
}

draw_letterbox();