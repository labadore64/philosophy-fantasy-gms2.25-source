/// @function keyboard_keybind_load(file)
/// @description Loads the state of the keyboard keybind from a JSON file.
/// @param {string} file The file to load from.
if(file_exists(argument[0])){
	var file = file_text_open_read(argument[0]);
	var stringer = "";

	while (!file_text_eof(file))
	{
		stringer+= file_text_readln(file);
	}
	file_text_close(file);

	var save_map = json_decode(stringer);

	// clears the current map
	// when setting the binding, it will just clear out the map
	ds_map_clear(global.keybind_map);
	
	var size = global.keybind_size;

	for(var i = 0; i < size; i++){
		keyboard_bind_set(global.keybind_array[i],stringToKeyboard(save_map[? global.keybind_array[i]]))
	}

	ds_map_destroy(save_map);
}