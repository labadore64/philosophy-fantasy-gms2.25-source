var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _src = map[? "source"];
	var _cond = map[? "cond"];
	var _turn = map[? "turn"]
	
	ds_map_destroy(map)
	
	if(!is_undefined(_src) 
		&& !is_undefined(_cond)
		&& !is_undefined(_turn)){
			
		var obj = instance_create(0,0,animationTimer);
		obj.timer = 120
		var list = ds_list_create();

		var listname = _src.source_name;

		// populate the list.
		ds_list_copy(list,_src.cards);
		var sizer = ds_list_size(list);
		var extra_list = ds_list_create()
		for(var i = 0; i < sizer; i++){
			extra_list[|i] = i;
		}
		
		for(var i = sizer-1; i >= 0; i--){
			var idea = code_card_query(list[|i],_cond);
			if(idea == 0){
				ds_list_delete(list,i);	
				ds_list_delete(extra_list,i);
			}
		}
		if(global.dont_record){
			// check what the argument on the next input is
			// then delete it
			
			var input = duelinput_take(0);
			// calculate the index
			while(string_pos(":",input) > 0){
				var poss = string_pos(":",input)
				input = string_copy(input,poss+1,string_length(input)-poss);
			}
			
			input = string_digits(input);
			
			if(input == ""){
				input = "0";	
			}
			
			var ind = real(input)
			if(_src > 0){
				var cardee = _src.card_array[ind]
				duel_animation_show_selected(cardee,0)
				duelinput_insert("cardselect",_src,ind);
			}
			duelinput_execute(duelinput_length()-1);
		} else {
			if(!ds_list_empty(list)){
				if(_turn == DUEL_PLAYER_YOU){
					with(duelController){
						card_list = list
						extra_card_list = extra_list
						list_show = instance_create(0,0,fadeOut)
						card_list_name = listname
						card_list_source = _src
						card_list_destroy = true;
						select_card = true;
					}
				} else {
					// add AI for this. just selects a card at random
					var card_selected = coolrandom_inext(ds_list_size(list)-1);
					if(card_selected > -1){
						var cardee = list[|card_selected];
						var index = ds_list_find_index(_src.cards,cardee);
					
						duel_animation_show_selected(cardee,0)
						duelinput_insert("cardselect",_src,index);
						duelinput_execute(duelinput_length()-1);
					}
				}
			} else {
				ds_list_destroy(list);
			}
		}

		return obj;
	}

}

return noone;