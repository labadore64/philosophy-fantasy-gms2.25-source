with(argument[0]){
	if(card_id > -1){
		if(!surface_exists(surf)){
			card_reset_size();
			surf = surface_create(card_width,card_height);	
			surface_update = true
		} else {
			if(ScaleManager.updated){
				card_reset_size();
				surface_resize(surf,card_width,card_height)	
				surface_update = true
			}
		}

		if(surface_update){
			card_update_surface();
	
			if(card_sprite != -1){
				sprite_delete(card_sprite);	
			}
			card_sprite = sprite_create_from_surface(surf, 0, 0, card_width, card_height, false, false, card_width*.5, card_height*.5);
			surface_update = false;
		}

		card_render();
	}
}