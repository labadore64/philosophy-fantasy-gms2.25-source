var list = argument[0];
var reverse = false
if(argument_count > 1){
	reverse = true;	
}
if(ds_exists(list,ds_type_list)){
	// create priority list based on this list
	var sizer = ds_list_size(list);
	
	for(var i = 0 ; i < sizer; i++){
		for(var j = i; j < sizer; j++){
			if(i != j){
				var result = deck_list_sort_whobigger(list[|i],list[|j]);
				if(reverse){
					if(!result){
						var listi = list[|i]
						var listj = list[|j]
						list[|i] = listj
						list[|j] = listi
					}
				} else {
					if(result){
						var listi = list[|i]
						var listj = list[|j]
						list[|i] = listj
						list[|j] = listi
					}
				}
			}
		}
	}
}

return list;