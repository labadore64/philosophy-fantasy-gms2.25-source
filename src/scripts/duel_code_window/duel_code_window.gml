// argument[0] = argument list
if(!argument[1]){
	if(!argument[0]){
		with(duelController){

			var stringer = ""

			var this = code_get_this();
			var this_enemy = false;
			if(string_pos("pos_enemy",this) > 0){
				this_enemy = true;
			}

			if(!this_enemy){
				// custom message
				if(code_argument_size > 0){
					if(code_arguments[0] != ""){
						stringer = string_copy(code_arguments[0],2,string_length(code_arguments[0])-2);
					}
				}

				// now that you have the display string, trigger the animation
				duel_animation_show_textbox(stringer,true); 
			}
		}
	}
}