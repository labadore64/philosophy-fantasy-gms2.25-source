with(parent){
	duelController.speak_tts = false;
	var mon = card_array[menupos]
	if(mon > -1){
		tts_say(obj_data.card_name[mon] + " Activated");	
	}
	
	duel_animation_move_card(menupos,parent.part[DUEL_FIELD_HAND],parent.part[DUEL_FIELD_THEORY])
	duel_animation_wait(20)
	duel_animation_play_sound(sfx_sound_duel_play_card)
	duel_animation_activate_card(mon)
}
// do the animation for showing card!

if(!context_menu){
	menu_pop();
}
instance_destroy()