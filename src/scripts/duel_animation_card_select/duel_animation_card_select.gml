var source = argument[0]
var turn = argument[1]
var cond = "";
if(argument_count > 2){
	cond = argument[2]	
}
if(!instance_exists(duelController.animation_stack_run) ||
	duelController.animation_stack_run.object_index == animationQueue){
		
	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	if(queue > -1 && ds_exists(queue,ds_type_list)){
		//duelController.running_obj = obj;
	
		var json = ds_map_create();
		ds_map_add(json,"source",source)
		ds_map_add(json,"cond",cond)
		ds_map_add(json,"turn",turn)
		
		duel_animation_pack(obj,queue,json,duel_anim_queue_card_select)
	}
}
