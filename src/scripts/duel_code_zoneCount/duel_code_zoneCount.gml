

var target_index = -1; // the card index to move
var target_origin = -1; // the location of the target
var target_destination = -1; // where to move the target
var target_side = duelController.active_player; // which side the target is on
var test_value = "";

var this = code_get_this();
var this_enemy = false;
if(string_pos("pos_enemy",this) > 0){
	this_enemy = true;
}

var return_value = 0;

var return_variable = "return"

var stringer = "";
for(var i = 0; i < code_argument_size; i++){
	if(string_pos("query",code_arguments[i]) > 0){
		test_value = string_copy(code_arguments[i],
								string_pos(":",code_arguments[i])+1,
								string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
	}
	
	if(string_pos("return",code_arguments[i]) > 0){
		return_variable = string_copy(code_arguments[i],
								string_pos(":",code_arguments[i])+1,
								string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		
		// the variable must exist. this forces uses of the predefined registers
		// to prevent any funny business
		if(!ds_map_exists(duelController.code_values,return_variable)){
			return_variable = "return";	
		}
	}
	
	
	// parse targets
	if(string_pos("target:",code_arguments[i]) > 0){
		var targe = string_copy(code_arguments[i],
								string_pos(":",code_arguments[i])+1,
								string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
								
	if(argument[0]){
	
			var targe_variable = duelController.code_values[? targe];
			if(!is_undefined(targe_variable)){
				targe = targe_variable	
			}						
			// parse target
			// if target is this just use the character stored in this
			var is_this = false;
			if(targe == "this"){
				targe = this
				is_this = true;
			}

			var stringe = targe;
			var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			var side = stringe;
			
			//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		
			if(is_this){
				if(this_enemy){
					target_side = DUEL_PLAYER_ENEMY	
				} else {
					target_side = DUEL_PLAYER_YOU	
				}
			} else {
				if(side == "enemy"){
					if(!this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}
				} else {
					if(this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}	
				}
			}
			// if the code is a location, parse as location.
			if(code_is_location(ori)){

				target_origin = code_get_location(ori,target_side)
			}
		}
	
	}
	
}

if(!argument[0]){
	
	duel_animation_zoneCount(targe,return_variable,test_value)
} else {
	var counter = 0;
	// selective move
	if(target_origin > -1){
	
		if(test_value != ""){
			var start = 0;
			var total = target_origin.card_array_size;
		
			if(target_origin.object_index == duelActive){
				if(ori == "overlay"){
					start = 1;
				} else {
					if(target_origin.card_array_size > 0){
						total = 1;	
					}
				}
			}
		
			for(var i = start; i < total; i++){
				if(code_card_query(target_origin.card_array[i],test_value)){
					counter++;	
				}
			}
		} else {
			if(target_origin.object_index == duelActive){
				if(ori == "overlay"){
					counter = max(target_origin.card_array_size-1,0);	
				} else {
					if(target_origin.card_array_size > 0){
						counter = 1;	
					}
				}
			} else {
				counter = target_origin.card_array_size;
			}
		}
	}

	// set the value here

	ds_map_replace(duelController.code_values,return_variable,counter)

}