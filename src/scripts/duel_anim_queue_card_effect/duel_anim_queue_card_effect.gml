var argu = argument[0]; // args as json string
var cando = false;
var _card, _loc, _this, _opt;

var ai = false;

if(argument_count > 1){
	cando = true
	var _card = argument[0]
	var _loc = argument[1]
	var _this = argument[2]
	var _opt = argument[3]
	ai = true
} else {
	
	var map = json_decode(argu);

	if(!ds_map_empty(map)){
		// do your normal code down here
		var _card = map[? "card"];
		var _loc = map[? "loc"];
		var _this = map[? "this"];
		var _opt = map[? "onceperturn"];
		
		if(!is_undefined(_card) &&
			!is_undefined(_this) &&
			!is_undefined(_loc) &&
			!is_undefined(_opt)){
			cando = true
		}
	}

	ds_map_destroy(map)
}

if(cando){	
	// reset the code position (this should only be changed inside a code execution)
	duelController.code_start = 0;
	duelController.code_pointer = 0;
	duelController.code_end = false
	duelController.code_is_ai = ai;
	if(_card > -1){
		// if the card isn't once-per-turn and has been already activated once
		if(!(_opt && card_has_keyword(_card,"once-per-turn") && obj_data.card_used_this_turn[_card] > 0)){
					
			duelController.code_start = 0;
			duelController.code_pointer = 0;
			duelController.code_end = false
			duelController.code_is_ai = ai;
			if(code_calculate_condition(_card,_loc,_this)){
					
					
					duelController.code_start = 0;
					duelController.code_pointer = 0;
					duelController.code_end = false
					duelController.code_is_ai = ai;
					var codeblock = code_find_block(_card,_loc)
					code_set_thisdirect(_this);
					code_set_card_id(_card);
					//code_default_values()
						
					// also check if a status effect applies to nullify the effect.
					duelController.temp_loaded_block = _loc;	
					passThroughStatus("negate")
		
					if(!ai){
						var queue = ds_list_create();
						var obj = instance_create(0,0,animationQueue);
						obj.queue = queue;
					}
						
					if(!duelController.temp_negated){

						duel_animation_pause(obj_data.card_name[_card] + "'s effect activated.");
						duel_battle_showcard(_card,sfx_sound_duel_activate_card,card_flash);
						code_load(obj_data.card_code[_card,codeblock]);	
				
						// now that the code is loaded, run the code
						// execute the code
						code_execute(false,true);
					
						// wait before continuing
						//if(!ai){
						duel_animation_wait(20);
						//}
					
						//duel_animation_code_complete();
						if(!ai){
							obj_data.card_used_this_turn[_card]++
							obj_data.card_used_this_game[_card]++
							
							with(duelActive){
								var pare = parent;
								with(card){
									card_update_field(pare);	
								}
							}
						} else {
							with(ai_duelField){
								card_used_this_turn[_card]++
								card_used_this_game[_card]++
							}
							card_update_field_ai();	
						}
					} else {
						// do negate animation code
						// for now it just shows a window cuz im fucking lazy
						//if(!ai){
							duel_animation_show_textbox(obj_data.card_name[_card] + "'s effect was negated!")
						//}
					}
					duelController.temp_negated = false;
				}
		}
		
	}
		
	return noone;
}

return noone;