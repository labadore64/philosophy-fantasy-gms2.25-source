if(!argument[1]){
	if(!argument[0]){
		// does a coin flip and sets "return" to the results
		var return_name = "return"

		for(var i = 0; i < code_argument_size; i++){
			if(string_pos("return",code_arguments[i]) > 0){
				return_name = string_copy(code_arguments[i],
										string_pos(":",code_arguments[i])+1,
										string_length(code_arguments[i])-string_pos(":",code_arguments[i]));
								
				// the variable must exist. this forces uses of the predefined registers
				// to prevent any funny business
				if(!ds_map_exists(duelController.code_values,return_variable)){
					return_variable = "return";	
				}
				break;
			}
		}
		duel_animation_pause("Flipping a coin.");
		duel_animation_coinflip(return_name);
	}
} else {
	with(duelController){
		ds_map_replace(code_values,"return",choose(true,false));
	}	
}