// arg0 = key to check

// if the keybinds are the same
if(argument[0] > -1){
	for(var i = 0; i < KEYBOARD_KEY_TOTAL; i++){
		if(argument[0] != i){
			if(global.keybind_value_array[argument[0]] == global.keybind_value_array[i]){
				if(global.keybind_restrict[argument[0],i]){
					return false;	
				}
			}
		}
	}
}
return true;