if(menu_select != -1){
	if(global.inputEnabled){

		if(double_select){
			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT]) ||
				keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT2]) ||
				global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

				script_execute(menu_select);
				return true;
			}
		} else {
			if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_SELECT]) ||
				global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

				script_execute(menu_select);
				return true;
			}
		}
	}
} 

return false;