init_global();
init_font();
init_format();

instance_create(0,0,ScaleManager)
instance_create(0,0,AXManager);
instance_create(0,0,MusicPlayer)
instance_create(0,0,obj_data);
instance_create(0,0,ttsControl);
instance_create(0,0,menuControl);
instance_create(0,0,MouseHandler)

// when completed, go to next room
alarm[0] = 1;