// these are the variables for ai stuff.

var overlay_c = 0;
var switch_c = 0;

if(parent > -1){
	overlay_c = parent.overlay_counter;
	switch_c = parent.switch_counter;
}
	ai_weight[AI_MAX_PASS-1] = 0;
	ai_turn[AI_MAX_PASS-1] = 0;
	ai_command_depth[AI_MAX_PASS-1] = 0;
	
	ai_att[AI_MAX_PASS-1] = 0;
	ai_def[AI_MAX_PASS-1] = 0;
	ai_card_depth[AI_MAX_PASS-1] = 0;
	
	original_count = 0;
	
	for(var i =0; i < AI_MAX_PASS; i++){
		ai_hp[i] = -1;
		ai_hp_enemy[i] = -1;
		ai_card_status[i] = ""
	
		ai_overlay_counter[i] = overlay_c
		ai_switch_counter[i] = switch_c
	
		ai_command[i] = ""
		ai_full_command[i] = "";
	
		ai_command_depth[i] = 0
	
		ai_att[i] = 0;
		ai_def[i] = 0;
	
		ai_phase[i] = -1
		ai_turn[i] = duelController.temp_turns
		ai_card_depth[i] = 0;
		ai_processed[i] = false;
		

		for(var k = 0; k < ai_part_count; k++){
			if(k == DUEL_FIELD_HAND){
				for(var j = 0; j < 5; j++){
					part[k].ai_card_array[i,j] = -1;
				}
			} else {
				for(var j = 0; j < part[k].size_limit; j++){
					part[k].ai_card_array[i,j] = -1;
				}
			}
		}
	}

	for(var i = 0; i < obj_data.cards_total; i++){
		card_used_this_turn[i]=obj_data.card_used_this_turn[i];
		card_used_this_game[i]=obj_data.card_used_this_game[i];	
	}

	current_hp = 0;
	current_hp_enemy = 0
	ai_counter = 0;
	att_boost = 0
	def_boost = 0

	card_attack_power = 0
	card_defense_power = 0

	current_player = duelController.active_player
