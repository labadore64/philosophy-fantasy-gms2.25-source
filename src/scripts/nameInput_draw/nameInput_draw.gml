draw_set_color(global.text_fgcolor)
draw_set_alpha(1)
draw_set_font(global.largeFont)
draw_set_halign(fa_center)
draw_set_valign(fa_middle)

draw_text_transformed_ratio(400,50,"Your name?",2,2,0)

draw_rectangle_ratio(30+220,145,30+580-90,145+5,false)

if(selected_part == NAMEINPUT_STATE_ALPHABET){
	var i = menupos-1
	var xx_shift = i % columns;
	var yy_shift = floor(i / columns)
	
	var posx = alpha_x+xx_shift*spacer_x;
	var posy = alpha_y+yy_shift*spacer_y
		
	draw_set_color(global.text_fgcolor)
	draw_rectangle_ratio(posx-18-4-5,posy-25-5,posx+18-4+5,posy+25+5,false)
	draw_set_color(c_black)
	draw_rectangle_ratio(posx-18-4,posy-25,posx+18-4,posy+25,false)
	draw_set_color(c_white)
}
draw_set_color(global.text_fgcolor)
draw_set_font(global.normalFont)
for(var i = 0; i < lengy; i++){
	var xx_shift = i % columns;
	var yy_shift = floor(i / columns)
	
	var posx = alpha_x+xx_shift*spacer_x;
	var posy = alpha_y+yy_shift*spacer_y
	
	draw_text_transformed_ratio(posx,posy,menu_name[i+1],3,3,0)	
}

for(var i = 0; i < name_length; i++){
	draw_text_transformed_ratio(30+270+name_spacer*i,120,name[i],3,3,0)	
}

if(name_length > insert_pos){
	draw_set_alpha(name_select_alpha)
	draw_rectangle_ratio(30-24+270+name_spacer*insert_pos, -25+120,
						30-24+270+36+name_spacer*insert_pos, -25+120+50,
						false)	
	draw_set_alpha(1)
}

// draw options

var counter = 0
for(var i = lengy+1; i < menu_count; i++){
	var xx_shift = counter % 2;
	var yy_shift = floor(counter / 2)
	
	var posx = option_x+(xx_shift-.5)*275;
	var posy = option_y+(yy_shift-.5)*40
	var width = string_width(menu_name[i])*3*.5
	
	if(menupos == i){
		gpu_set_colorwriteenable(true,true,true,true)
		draw_set_color(c_white)
		if(global.menu_animate){
			draw_set_alpha(alpha_adder);
		} else {
			draw_set_alpha(alpha_max)
		}
		if(global.high_contrast){
			draw_rectangle_ratio(posx-width-75-10,posy-18,
								posx-width-75+10,posy-18+yy_shift,
						false)
		} else {
			if(global.color_gradient){
				draw_rectangle_color_ratio(posx-width-10,posy-22,
									posx+width,posy-22+40,
				global.menu_grcolor[1],global.menu_grcolor[1],
				global.menu_grcolor[0],global.menu_grcolor[0],
				false)
		
			} else {
				draw_set_color(global.menu_grcolor[0])
				draw_rectangle_ratio(posx-width-10,posy-22,
									posx+width,posy-22+40,
				false)
			}
		}
	//draw_sprite_extended_ratio(spr_talk_continue,ScaleManager.spr_counter+15,
	//							posx-width-75,posy-18,1,1,0,c_white,1)
	}
	draw_set_alpha(1)
	
	draw_text_transformed_ratio(posx,posy,menu_name[i],3,3,0)	
	
	counter++
}

draw_set_halign(fa_left)
draw_set_valign(fa_top)

with(fadeOut){
	draw_fade()	
}
with(fadeIn){
	draw_fade()	
}