var map = argument[0]

var obj = instance_create(0,0,pointAndClick);

val = map[? "bg"]
if(!is_undefined(val)){
	obj.bg_image = "bg_" + val;
	var spritename = working_directory + "resources\\bg\\" + obj.bg_image  + ".png";
	obj.img_sprite =  sprite_add(spritename, 0, false, false, 0, 0)

}
global.story_next = "";
val = map[? "next"]
if(!is_undefined(val)){
	global.story_next = string_lower(val)	
	
}
val = map[? "objects"]

							
// play music
							
var mus = map[? "music"]

if(!is_undefined(mus)){
	mus = asset_get_index("sfx_music_"+mus)
	soundPlaySong(mus,0)
}

var mus = map[? "title"]
if(!is_undefined(mus)){
	obj.bg_title = mus;	
}

var list = map[? "sfx_random"]
var comple = false;
							
if(!is_undefined(list)){
	if(ds_exists(list,ds_type_list)){
		var sizer = ds_list_size(list);
		for(var j = 0; j < sizer; j++){
			obj.sprite_sfx_random[j] = asset_get_index("sfx_sound_" + list[| j]);
		}
									
		comple = true;
	}
}
							
if(!comple){
	obj.sprite_sfx_random[0] = -1;
								
}

var did = false;
var last_sound = global.loop_sound;
							
list=map[? "sfx_loop"]
if(!is_undefined(list)){
	obj.sprite_sfx_loop = asset_get_index("sfx_sound_" + list);
	if(obj.sprite_sfx_loop > -1){
		

		if((obj.sprite_sfx_loop != global.loop_sound_val)){
			audio_stop_sound(global.loop_sound_val);	
			global.loop_sound = soundfxPlayLoop(obj.sprite_sfx_loop);	
			global.loop_sound_val = obj.sprite_sfx_loop;
		}

	} else {
		audio_stop_sound(global.loop_sound_val);	
		global.loop_sound_val = -1
	}
} else {
	audio_stop_sound(global.loop_sound_val);
	global.loop_sound_val = -1
}

var counter = 0;
if(!is_undefined(val)){
	if(ds_exists(val,ds_type_list)){
		var size = ds_list_size(val);
		
		for(var i = 0; i < size; i++){
			var data = val[|i];
			if(!is_undefined(data)){
				var title = data[? "title"]
				if(!is_undefined(title)){
					if(ds_exists(data,ds_type_map)){
						var value = data[? "sprite"]
						var spritename = ""
						var xx = 0;
						var yy = 0
						var xx_scale = 1
						var yy_scale = 1
						var origin_x = 0;
						var origin_y = 0
						var alpha = 1
						var color = c_white;
						var desc = ""
						var next = ""
						var nextroom = ""
						var cando = true;
						var width = 10;
						var height = 20
						var text = "";
						var sfx = sfx_sound_menu_select
						var show = true;
					
						if(!is_undefined(value)){
							spritename = value;
						}
						
						value = data[? "desc"]
						if(!is_undefined(value)){
							desc = value;	
						}
						
						value = data[? "show"]
						if(!is_undefined(value)){
							show = value
						}
						
						value = data[? "text"]
						if(!is_undefined(value)){
							text = value;	
						}
						
						value = data[? "next"]
						if(!is_undefined(value)){
							next = value;	
						}
						
						value = data[? "nextroom"]
						if(!is_undefined(value)){
							nextroom = value;	
						}
						
						value = data[? "x"]
						if(!is_undefined(value)){
							xx = real(value);	
						}
						value = data[? "y"]
						if(!is_undefined(value)){
							yy = real(value);	
						}
						
						value = data[? "xscale"]
						if(!is_undefined(value)){
							xx_scale = real(value);	
						}
						value = data[? "yscale"]
						if(!is_undefined(value)){
							yy_scale = real(value);	
						}
						
						value = data[? "width"]
						if(!is_undefined(value)){
							width = real(value);	
						}
						value = data[? "height"]
						if(!is_undefined(value)){
							height = real(value);	
						}
						
						value = data[? "x_offset"]
						if(!is_undefined(value)){
							origin_x = real(value);	
						}
						value = data[? "y_offset"]
						if(!is_undefined(value)){
							origin_y = real(value);	
						}
						value = data[? "alpha"]
						if(!is_undefined(value)){
							alpha = real(value)*0.01;	
						}
						value = data[? "color"]
						if(!is_undefined(value)){
							color = hex_to_dec(value);	
						}
						value = data[? "sfx"]
						if(!is_undefined(value)){
							sfx = asset_get_index("sfx_sound_" + value);
						}
						
						
						value = data[? "required_events"]
						if(!is_undefined(value)){
							if(ds_exists(value,ds_type_list)){
								var sizer = ds_list_size(value);
								for(var j = 0; j  < sizer; j++){
									var test = value[| j];
									if(!is_undefined(test)){
										if(!eventCheck(test)){
											cando = false;
											break;
										}
									}
								}
							}
						}
					
						value = data[? "required_events_false"]
						if(!is_undefined(value)){
							if(ds_exists(value,ds_type_list)){
								var sizer = ds_list_size(value);
								for(var j = 0; j  < sizer; j++){
									var test = value[| j];
									if(!is_undefined(test)){
										if(eventCheck(test)){
											cando = false;
											break;
										}
									}
								}
							}
						}
						
						if(cando){

							
							var spritenamez = working_directory + "resources\\sprite\\" + spritename + ".png";
						
							obj.sprites[counter] = sprite_add(spritenamez, 0, false, false, origin_x, origin_y)
							obj.sprite_x[counter] = xx;
							obj.sprite_y[counter] = yy;
							obj.sprite_xscale[counter] = xx_scale;
							obj.sprite_yscale[counter] = yy_scale;
							obj.sprite_alpha[counter] = alpha
							obj.sprite_color[counter] = color
							obj.sprite_title[counter] = title
							obj.sprite_desc[counter] = desc
							obj.sprite_next[counter] = next
							obj.sprite_nextroom[counter] = nextroom
							obj.sprite_exist[counter] = sprite_exists(obj.sprites[counter])
							obj.spritewidth[counter] = width;
							obj.spriteheight[counter] = height
							obj.sprite_text[counter] = text
							obj.sprite_sfx[counter] = sfx
							obj.sprite_show[counter] = show
							
							var cost = data[? "cost"]
							if(!is_undefined(cost)){
								if(global.activity_points-real(cost) >= 0){
									obj.sprite_cost[counter] = real(cost);
								} else {
									cando = false	
								}
							}
		
							if(cando){
							
								obj.sprite_count++;
								with(obj){
									menu_add_option(title,desc,-1,-1)	
									var tooltipz = menuAddTooltipNewline(xx+width*.5,yy+height*.5,title + "\n" + desc);
									with(tooltipz){
										if(x < 20){
											x = 20
										} else if(x+self.width > 800- 20){
											x = 800 - 20 - self.width
										}
		
										if(global.tooltip_type == 2){
											if(y < 20){
												y = 20
											} else if(y+self.height > 600 -  20){
												y = 495 - 20 - self.height
											}
										} else {
											if(y < 20){
												y = 20
											} else if(y+self.height > 600 -  20){
												y = 600 - 20 - self.height
											}
										}
									}
								
								}
								counter++;
							}
						}
					}
				}
			}
		}
		
	}
}
with(obj){
	pointAndClick_mouse()	
}