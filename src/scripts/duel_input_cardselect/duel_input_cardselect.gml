// should be in all scripts
var index = argument[1]
var origin = argument[0]

with(duelController){
	if(ds_exists(code_values,ds_type_map)){
		var this = code_set_cardstate(origin,index)
		code_values[? "selection"] = this;

	}
}

return noone