if(!instance_exists(duelController.animation_stack_run) || duelController.animation_stack_run.object_index == animationQueue){
	var card_id = argument[0];
	var card_goto_id = argument[1]
	var card_source_loc = argument[2];
	var goto_loc = argument[3];
	var time = argument[4]
	// get start position
	
	var field_x = 200;
	var field_y = 200

	var start_x = 0
	var end_x = 0
	var spacer = 0
	var posy = 0
	var yspacer = 0;
	var menuposs = 0;
	with(card_source_loc){
		
		if(enemy){
			var field_x = 204;
			var field_y = 199
		}
		
		if(object_index == duelHand){
			if(!enemy){
				var start_x = -60;
				var end_x = 420;
				var spacer = (end_x-start_x)/5;
				var posy = 340+240
			} else {
				var start_x = 420-105;
				var end_x = -60-105
				var spacer = (end_x-start_x)/5;
				var posy = -330
			}
			
			if(card_array_size > 5){
				spacer = (end_x-start_x)/(card_array_size+1);
			}
			
			menuposs = card_id;
			if(menuposs > card_array_size-1){
				menuposs = card_array_size-1;	
			}
		} else if(object_index == duelDeck){
			if(!enemy){
				var start_x = 310
				var posy = 450
			} else {
				var start_x = -55
				var posy = -200
			}
		} else if(object_index == duelWaiting){
			if(!enemy){
				var start_x = 310
				var posy = 340
			} else {
				var start_x = -55
				var posy = -90
			}
		} else if(object_index == duelTheory){
			if(!enemy){
				var start_x = 310-370
				var posy = 340
			} else {
				var start_x = -55+370
				var posy = -90
			}
		} else if (object_index == duelActive){
			if(!enemy){
				var start_x = 130
				var posy = 230
	
				var yspacer = 20;
			} else {
				var start_x = 0
				var posy = 200
	
				var yspacer = 20;
			}
			menuposs = card_id;
			if(menuposs > card_array_size-1){
				menuposs = card_array_size-1;	
			}
			
		} else if (object_index == duelBench){
			
			if(enemy){
				var start_x = 205
	
				var posy = -90
	
				var spacer = -75;
			} else {
				var start_x = 50
				var posy = 340
	
				var spacer = 75;
				
			}
			
			if(card_array_size == 0){
				startx = 150
			}
			
			menuposs = card_id;
			if(menuposs > card_array_size-1){
				menuposs = card_array_size-1;	
			}
		}
	
	}
	
	var start_xx = field_x+start_x+menuposs*spacer
	var start_yy = field_y+posy+menuposs*yspacer
	
	// end get start position
	
	// start get end position
	var field_x = 200;
	var field_y = 200

	var start_x = 0
	var end_x = 0
	var spacer = 0
	var posy = 0
	var yspacer = 0;
	var menuposs = 0;
	with(goto_loc){
		
		if(enemy){
			var field_x = 204;
			var field_y = 199
		}
		
		if(object_index == duelHand){
			if(!enemy){
				var start_x = -60;
				var end_x = 420;
				var spacer = (end_x-start_x)/5;
				var posy = 340+240
			} else {
				var start_x = 420-105;
				var end_x = -60-105
				var spacer = (end_x-start_x)/5;
				var posy = -330
			}
			
			if(card_array_size > 5){
				spacer = (end_x-start_x)/(card_array_size+1);
			}
			
			menuposs = card_goto_id;
		} else if(object_index == duelDeck){
			if(!enemy){
				var start_x = 310
				var posy = 450
			} else {
				var start_x = -55
				var posy = -200
			}
		} else if(object_index == duelWaiting){
			if(!enemy){
				var start_x = 310
				var posy = 340
			} else {
				var start_x = -55
				var posy = -90
			}
		} else if(object_index == duelTheory){
			if(!enemy){
				var start_x = 310-370
				var posy = 340
			} else {
				var start_x = -55+370
				var posy = -90
			}
		} else if (object_index == duelActive){
			if(!enemy){
				var start_x = 130
				var posy = 210
	
				var yspacer = -20;
			} else {
				var start_x = 0
				var posy = 180
	
				var yspacer = 20;
			}
			menuposs = card_goto_id;
		} else if (object_index == duelBench){
			
			if(enemy){
				var start_x = 205
	
				var posy = -90
	
				var spacer = -75;
			} else {
				var start_x = 125-75
				var posy = 340
	
				var spacer = 75;
				
			}
			
			if(card_array_size == 0){
				startx = 150
			}
			
			menuposs = card_goto_id;
		}
	
	}
	
	var dest_x = field_x+start_x+menuposs*spacer
	var dest_y = field_y+posy+menuposs*yspacer
	// end get end position

	var obj = instance_create(0,0,drawSwapCard)
	var le_card = card_source_loc.card_array[card_id]
	
	obj.xx[0] = start_xx
	obj.yy[0] = start_yy
	obj.card_place_id[0] = le_card;
	obj.card_place_location[0] = goto_loc;
	obj.card_dest_place[0] = card_goto_id
	obj.card_index[0] = 5;	

		if(obj_data.card_type[le_card] == CARD_TYPE_PHILOSOPHER){
			obj.card_index[0] = obj_data.card_element[le_card]
		} else if (obj_data.card_type[le_card] == CARD_TYPE_METAPHYS){
			obj.card_index[0] = 4
		}else if (obj_data.card_type[le_card] == CARD_TYPE_EFFECT){
			obj.card_index[0] = 3
		} else {
			obj.card_index[0] = 5;	
		}

	obj.goto_x[0] = dest_x;
	obj.goto_y[0] = dest_y;
	obj.move_x[0] = (start_xx-dest_x)/(time+1)
	obj.move_y[0] = (start_yy-dest_y)/(time+1)

	duelController.card_rumbleset_x =dest_x
	duelController.card_rumbleset_y =dest_y;

	RUMBLE_CARD_SELECT

	// dest card
	var le_card = goto_loc.card_array[card_goto_id]

	obj.xx[1] = dest_x;
	obj.yy[1] = dest_y;
	obj.card_place_id[1] = le_card;
	obj.card_dest_place[1] = card_id
	obj.card_place_location[1] = card_source_loc
	obj.card_index[1] = 5;	

		if(obj_data.card_type[le_card] == CARD_TYPE_PHILOSOPHER){
			obj.card_index[1] = obj_data.card_element[le_card]
		} else if (obj_data.card_type[le_card] == CARD_TYPE_METAPHYS){
			obj.card_index[1] = 4
		}else if (obj_data.card_type[le_card] == CARD_TYPE_EFFECT){
			obj.card_index[1] = 3
		} else {
			obj.card_index[1] = 5;	
		}

	obj.goto_x[1] = start_xx
	obj.goto_y[1] = start_yy
	obj.move_x[1] = (dest_x-start_xx)/(time+1)
	obj.move_y[1] = (dest_y-start_yy)/(time+1)
	
	obj.timer = time;
	
	if(duelController.mouse_mode){
		with(drawSwapCard){
			camera_move = false;	
		}
	}

	if(!instance_exists(duelController.running_obj)  || duelController.running_obj.object_index != animationQueue){
		duelController.running_obj = obj;
	}
	

	with(card_source_loc){
		card_array[card_id] = -1
	}
	with(goto_loc){
		card_array[card_goto_id] = -1
	}
	
	return obj;
}

return noone;