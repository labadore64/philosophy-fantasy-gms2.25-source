	fade_surface_updated=true
	
	translation_rate_start = 25;
	
	if(global.menu_animate){
		translation_start = 150;
	} else {
		translation_start = 0	
	}

	translation_decay_rate = .975

	translation = translation_start
	translation_rate = translation_rate_start;

	//calculate selected from option top
	var counter = 0;
	for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
		if(i == menupos){
			selected_draw_index = counter;
			break;
		}
		counter++;
	}