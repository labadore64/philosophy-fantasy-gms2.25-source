var map = argument[2]

with(argument[0]){

	name = map[? "name"]
	portrait_chara = map[? "chara"]
	portrait_sprite = asset_get_index(map[? "sprite"])
	if(map[? "sprite"] == "custom"){
		portrait_custom = true;
	}
	var val = map[? "val"]
	if(!is_undefined(val)){
		var sizer = ds_list_size(val);
		for(var i = 0; i < sizer; i++){
			portrait_property[i] = val[|i];	
		}
	}
	
	var val = map[? "col"]
	if(!is_undefined(val)){
		var sizer = ds_list_size(val);
		for(var i = 0; i < sizer; i++){
			portrait_color[i] = val[|i];	
		}
	}
	channel_card = map[? "channel"]
}

duel_loadDeck(argument[1],map)