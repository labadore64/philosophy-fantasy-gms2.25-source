update_image_only_active = false;
var script_for_update = menuOverworldKeyboardGamepadStart

menu_add_option("Default","Reset to default keys.",menuOverworldKeyboardGamepadbindSetDefault,-1)
menu_add_option("Select","Select options in menus.",script_for_update,-1) // select
menu_add_option("Submit","Submits input and also selects on menus.",script_for_update,-1) // submit
menu_add_option("Cancel","Cancel menus or selections.",script_for_update,-1) // cancel
menu_add_option("Up","Move Up.",script_for_update,-1) // up
menu_add_option("Down","Move Down.",script_for_update,-1) // down
menu_add_option("Left","Move Left.",script_for_update,-1) // left
menu_add_option("Right","Move Right.",script_for_update,-1) // right
menu_add_option("Shift Left","Shift Left.",script_for_update,-1) // left Tab
menu_add_option("Shift Right","Shift Right.",script_for_update,-1) // right Tab
menu_add_option("Sort","Sort a list.",script_for_update,-1)  // sort
menu_add_option("View Card","View a card.",script_for_update,-1)  // sort
menu_add_option("Speed Up","Hold to speed up the game.",script_for_update,-1) //speed
menu_add_option("Help","Opens help menu.",script_for_update,-1) // help
menu_add_option("Options","Opens options menu.",script_for_update,-1) // options
menu_add_option("Screenshot","Takes a screenshot!",script_for_update,-1) // Screenshot
menu_add_option("View Controls","View a screen's control scheme.",script_for_update,-1) // Controls
menu_add_option("Exit","Exits the game.",script_for_update,-1) // exit

axis_values = ds_list_create();
axis_strings = ds_list_create();

vib_values = ds_list_create();
vib_strings = ds_list_create();

keybind_text = ds_list_create();

key_ids = ds_list_create();

ds_list_add(key_ids,"","select","select2","cancel","up","down","left","right","tableft","tabright","sort","view_card","speed","help","options","screenshot","keydisplay","exit")

menuOverworldKeyboardSetKeybindText(keybind_text);

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

title = "Controller Configuration"

menuParentUpdateBoxDimension();

//menu_draw = menuOverworldOptionControllerDraw

menu_draw =  menuOverworldOptionKeyboardDraw

//menu_up = menuOverworldOptionRealUp;
//menu_down = menuOverworldOptionRealDown;

menu_left = -1
menu_right = -1