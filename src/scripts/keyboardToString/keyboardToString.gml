/// @function keyboardToString(virtual_key)
/// @description Turns a keyboard virtual key into a string.
/// @param {real} virtual_key The virtual key.

	var key = argument[0];
	if(key != 0){
		var test_key = stringUppercaseOnly(chr(key));

		if(string_lettersdigits(test_key) != ""){
			return test_key;
		}
	
		if(key >= 112 && key <=123){
			var keyval = key - 111;
		
			return 	"F" + string(keyval);
		}
	
		if(key >= 96 && key <= 105){
			var keyval = key - 96;
			return "Num " + string(keyval);
		}

		switch(key){
			case vk_control:
			return "CTRL"
		
			case vk_tab:
			return "Tab";
	
			case 20:
			return "Caps Lock";
	
			case vk_shift:
			return "Shift";
	
			case vk_lcontrol:
			return "L-Ctrl";
	
			case vk_rcontrol:
			return "R-Ctrl";
	
			case vk_lalt:
			return "L-Alt";
	
			case vk_ralt:
			return "R-Alt";
	
			case vk_space:
			return "Space";
	
			case vk_left:
			return "Left"
	
			case vk_up:
			return "Up"
	
			case vk_right:
			return "Right"
	
			case vk_down:
			return "Down"
	
			case vk_enter:
			return "Enter"
	
			case 189:
			return "-"
	
			case 187:
			return "+"
	
			case vk_backspace:
			return "Backspace"
	
			case 188:
			return "<"
	
			case 190:
			return ">"
	
			case 191:
			return "?"
	
			case 186:
			return ":"
	
			case 222:
			return "\""
	
			case 219:
			return "{";
	
			case 221:
			return "}"
	
			case 220:
			return "|"
	
			case 192:
			return "~"
	
			case vk_escape:
			return "ESC"
		
			case 111:
			return "Num" + " /"
		
			case 106:
			return "Num" + " *"
		
			case 109:
			return "Num" + " -"
		
			case 107:
			return "Num" + " +"
		
			case 110:
			return "Num" + " ."
		}
	}
	return string(key);
