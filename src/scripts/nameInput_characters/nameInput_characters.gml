chara_length = array_length_1d(chara)

if(uppercase == NAMEINPUT_CASE_UPPER){
	uppercase = NAMEINPUT_CASE_LOWER	
} 
else if(uppercase == NAMEINPUT_CASE_LOWER){
	uppercase = NAMEINPUT_CASE_SPECIAL_UPPER
	menupos -= (chara_length - special_upper_length)
} 
else if(uppercase == NAMEINPUT_CASE_SPECIAL_UPPER){
	uppercase = NAMEINPUT_CASE_SPECIAL_LOWER
	menupos += (special_upper_length - special_lower_length)
} 
else {
	uppercase = NAMEINPUT_CASE_UPPER
	menupos += (special_lower_length - chara_length)
}

for(var i = 0; i < chara_length; i++){
	if(uppercase == NAMEINPUT_CASE_UPPER){
		display_chara[i] = string_upper(chara[i]);
	} else {
		display_chara[i] = string_lower(chara[i]);
	}
}

if(global.menu_sounds){
	soundfxPlay(sfx_sound_menu_select)	
}

event_user(0)