var pos = -1;

if(argument_count > 1){
	pos = argument[1]
}

if(!instance_exists(duelController.animation_stack_run) ||
	duelController.animation_stack_run.object_index == animationQueue){
		
	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	
	duelController.running_obj = obj;
	if(queue > -1 && ds_exists(queue,ds_type_list)){
		var counter = ds_list_size(queue);

		var json = ds_map_create();
		ds_map_add(json,"card",argument[0])
	
		if(pos > -1){
			duel_animation_pack(obj,queue,json,duel_anim_queue_show_selected,pos)
		
		} else {
			duel_animation_pack(obj,queue,json,duel_anim_queue_show_selected)
		}
	}
}
