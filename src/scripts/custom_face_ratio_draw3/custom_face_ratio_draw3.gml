var scale = argument[0]
var xx = argument[1]
var yy = argument[2]

var hair_color = argument[3];
var skin_color = argument[4];
var earring_color = argument[5];
var hat_color = argument[6];

var glasses_color = argument[7];

draw_sprite_extended_ratio(spr_faces_glasses,argument[8],scale*(256+xx),scale*(256+yy),scale,scale,0,glasses_color,1)
draw_sprite_extended_ratio(spr_faces_ear,argument[9],scale*(256+xx),scale*(256+yy),scale,scale,0,skin_color,1)
draw_sprite_extended_ratio(spr_faces_earring,argument[10],scale*(256+xx),scale*(256+yy),scale,scale,0,earring_color,1)
draw_sprite_extended_ratio(spr_faces_front_hair,argument[11],scale*(256+xx),scale*(256+yy),scale,scale,0,hair_color,1)
draw_sprite_extended_ratio(spr_faces_hat,argument[12],scale*(256+xx),scale*(256+yy),scale,scale,0,hat_color,1)