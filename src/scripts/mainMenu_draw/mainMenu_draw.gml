var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

with(mainMenu){
	if(!instance_exists(darkScreen)){
		if(global.high_contrast){
			draw_clear(c_black)
		} else {
			mainMenu_DrawBackground()
		}
	
		if(!surface_exists(surf)){
			surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
			surface_update = true;
		} else {
			if(ScaleManager.updated){
				surface_resize(surf,global.letterbox_w,global.letterbox_h);
				surface_update = true;
			}
		}
		
if(global.menu_animate){
	var fadescale = 2.5
	
	if(!surface_exists(fade_surface)){
		fade_surface = surface_create_access(fadescale*245*global.scale_factor,fadescale*256*global.scale_factor)	
		fade_surface_updated=true;
	} else {
		if(ScaleManager.updated){
			surface_resize(fade_surface,fadescale*245*global.scale_factor,fadescale*256*global.scale_factor);
			fade_surface_updated=true;
		}
	}

	if(fade_surface_updated){
		//preview_philosopher	
		surface_set_target(fade_surface)
		draw_clear_alpha(c_black,0)
		gpu_set_colorwriteenable(true,true,true,true)
		//if(draw_the_menu){
		draw_sprite_extended_noratio(preview_philosopher,0,30,0,
									fadescale,fadescale,0,c_white,1)
		//} else {
		//draw_sprite_extended_noratio(review_philosopher,0,30,0,
		//							fadescale,fadescale,0,c_white,1)	
		//}

		gpu_set_colorwriteenable(true,true,true,false)
		if(card_trans_total < 1){
			draw_sprite_extended_noratio(obj_data.card_sprite[card_transition[0]],0,30,0,
				fadescale,fadescale,0,c_white,1)	
		} else {
			draw_sprite_extended_noratio(obj_data.card_sprite[card_transition[card_display_index]],0,30,0,
				fadescale,fadescale,0,c_white,1)	
		}
		gpu_set_colorwriteenable(true,true,true,true)

		surface_reset_target();
		fade_surface_updated = false;
	}
		
	draw_set_alpha(card_display_alpha*.33)
	gpu_set_blendmode(bm_add)
	draw_surface(fade_surface,global.display_x + (300+card_display_x)*global.scale_factor,global.display_y)
	draw_set_alpha(1)
	gpu_set_blendmode(bm_normal)
		
}
		if(surface_update){
			
			var bla = global.menu_bgcolor;

			if(global.high_contrast){
				bla = c_black;	
			}
			
			surface_set_target(surf)
			gpu_set_colorwriteenable(true,true,true,true)
			draw_clear_alpha(c_black,0)
			draw_set_alpha(1)
			draw_set_color(c_white)
			draw_set_font(global.largeFont)

					gpu_set_colorwriteenable(true,true,true,true)
					draw_set_color(bla)
					draw_set_alpha(.7)
					draw_rectangle_noratio(0,0,
											option_x_offset*2+draw_width-20,600,
											false)
					//gpu_set_colorwriteenable(true,true,true,false)
					draw_set_alpha(1)
					draw_set_color(col)

				draw_text_transformed_noratio(60,35,"Main Menu",3,3,0)

				draw_set_font(global.normalFont)
				var counter = 0;
				for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
					
					gpu_set_colorwriteenable(true,true,true,true)
					if(i == menupos){
						draw_set_color(c_white)
						if(global.menu_animate){
							draw_set_alpha(alpha_adder);
						} else {
							draw_set_alpha(alpha_max)
						}
						if(global.high_contrast){
							draw_rectangle_noratio(option_x_offset+5,5+option_start_y+option_spacer*(counter-.5+.25),
										option_x_offset+draw_width+5,-5+option_start_y+option_spacer*(counter+.5+.25),
										false)
						} else {
							draw_rectangle_color_noratio(option_x_offset+5,5+option_start_y+option_spacer*(counter-.5+.25),
							option_x_offset+draw_width+5,-5+option_start_y+option_spacer*(counter+.5+.25),
							global.menu_selcolor[1],global.menu_selcolor[1],
							global.menu_selcolor[0],global.menu_selcolor[0],
							false)
						}
					} else {
						draw_set_color(bla)
						draw_set_alpha(.85)
						draw_rectangle_noratio(option_x_offset+5,5+option_start_y+option_spacer*(counter-.5+.25),
												option_x_offset+draw_width+5,-5+option_start_y+option_spacer*(counter+.5+.25),
												false)
					}
					
					//gpu_set_colorwriteenable(true,true,true,false)
					draw_set_alpha(1)
					draw_set_color(col)
					
					draw_text_transformed_noratio(option_x_offset+15,-5+option_start_y+option_spacer*counter,menu_name[i],option_text_size,option_text_size,0)
				
					counter++;
				}

			draw_set_halign(fa_left)
			
			if(menu_count > per_line){
				// scrollbar
				draw_set_color(bla)
				draw_set_alpha(.8)
				draw_rectangle_noratio(10,option_start_y-10,40,option_start_y+option_spacer*per_line-20,false)
				//scroll_increment
				draw_set_color(col)
				draw_rectangle_noratio(
						10,scroll_increment*option_top+option_start_y-10,
						40,min(
							option_start_y+option_spacer*per_line-20,
							scroll_increment*(option_top+per_line)+option_start_y-10),
						false)
			}
			surface_reset_target();
			surface_update = false;
		}
		draw_set_alpha(1)
		draw_surface(surf,global.display_x,global.display_y);
	
		with(animationWindow){
			duelController_draw_message();	
		}
	
		menuParent_draw_cancel_button();
	
		with(fadeIn){
			draw_fade()	
		}
		with(fadeOut){
			draw_fade()	
		}
	
		draw_letterbox();
	}
}