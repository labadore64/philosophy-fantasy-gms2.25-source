if(!argument[0]){
	var this = code_get_this();
	var this_enemy = false;
	if(string_pos("pos_enemy",this) > 0){
		this_enemy = true;
	}

	if(string_lower(duelController.code_arguments[0]) == "enemy"){
		this_enemy = !this_enemy;
	}
	
	duel_animation_shuffle(duelController.field[this_enemy].deck);
}