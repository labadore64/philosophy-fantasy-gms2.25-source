
// draw parent
	
//surf
if(!surface_exists(surf)){
	surf = surface_create(global.scale_factor*(800-x),global.letterbox_h);	
	surf_main_update = true
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.scale_factor*(800-x),global.letterbox_h)	
		surf_main_update = true
	}
}
with(parent){
	script_execute(menu_draw);	
}

// draw you
var xxx = x+20
var yyy = y+225
	
var size = 50;
var x_shift = xxx+230
var y_shift = yyy-150

if(surf_main_update){
	var move_textx = x_shift-235
	var move_texty = y_shift-15
	gpu_set_colorwriteenable(true,true,true,true)
	surface_set_target(surf)
	draw_clear_alpha(c_black,0)
	draw_set_font(global.normalFont)

	draw_set_color(c_white)
	draw_set_alpha(1)
	draw_set_color(c_white)
	if(global.high_contrast){
		draw_rectangle_noratio(0,y-5,menu_width+5,y+menu_height+5,false)
	} else {
		draw_rectangle_color_noratio(0,y-7,menu_width+7,y+menu_height+7,
		global.menu_grcolor[0],global.menu_grcolor[0],
		global.menu_grcolor[1],global.menu_grcolor[1],
		false)
		
		if(global.color_gradient){
			draw_rectangle_color_noratio(0,y-7,menu_width+7,y+menu_height+7,
			global.menu_grcolor[0],global.menu_grcolor[0],
			global.menu_grcolor[1],global.menu_grcolor[1],
			false)
		
		} else {
			draw_set_color(global.menu_grcolor[0])
			draw_rectangle_noratio(0,y-7,menu_width+7,y+menu_height+7,
			false)
		}
	}

	if(global.high_contrast){
		draw_set_color(c_black)
	} else {
		draw_set_color(global.menu_bgcolor)
	}
	draw_set_alpha(1)
	draw_rectangle_noratio(5,y,menu_width,y+menu_height,false)

	draw_set_color(global.text_fgcolor)
	draw_set_font(global.largeFont)
	draw_set_halign(fa_center)
	draw_text_transformed_noratio(menu_width*.5,y+10,title,2,2,0)
	draw_set_halign(fa_left)
	draw_set_font(global.normalFont)

	//draw color map
	draw_set_alpha(1)


	if(global.high_contrast){
		draw_rectangle_noratio(20,yyy-5-40,10+xxx+310-x,yyy+310-20,false)
	} else {
		draw_rectangle_color_noratio(20,yyy-7-40-20,10+xxx+310-x,yyy+310-20,
		$BBBBBB,$BBBBBB,
		global.menu_selcolor[1],global.menu_selcolor[1],
		false)
	}
	draw_set_color(global.menu_bgcolor)
	draw_rectangle_noratio(10+xxx-x,yyy-5-35-20,10+xxx+305-x,yyy-10-20,false)

	draw_rectangle_noratio(10+xxx-x,yyy-5-20,10+xxx+305-x,yyy+300-20,false)

	draw_sprite_extended_noratio(spr_colormap,0,10+xxx+5-x,yyy-20,.575,.575,0,c_white,1)
	
	if(global.high_contrast){
		draw_set_color($AAAAAA)
		draw_rectangle_noratio(40+x_shift-5-x,y_shift-5,
						40+5+x_shift+size-x,5+size+y_shift,
						false)
	} else {
		draw_rectangle_color_noratio(40+x_shift-5-x,y_shift-5,
						40+5+x_shift+size-x,5+size+y_shift,
						$AAAAAA,$7F7F7F,
						$444444,$7F7F7F,
						false)
	}
	
	draw_set_font(global.normalFont)
	draw_set_color(global.text_fgcolor)
	// draw color info
	gpu_set_colorwriteenable(true,true,true,false)
	for(var i = 0; i < 3; i++){
		draw_set_halign(fa_right)
		if(menupos-1 == i){
			if(selected){
				draw_set_alpha(.2)
			}	else {
				draw_set_alpha(.1)
			}
			draw_rectangle_noratio(move_textx+70-x+5,move_texty+i*30,move_textx+60+65-x,move_texty+i*30+30,false)	
		}
		draw_set_alpha(1)
		draw_set_halign(fa_left)
		draw_text_transformed_noratio(move_textx-x+5,move_texty+i*30,rgb_name[i] + ":",2,2,0)
	}
	
	// draw color info
	var move_textx = x_shift-90

	for(var i = 0; i < 3; i++){
		draw_set_halign(fa_right)
		if(menupos-4 == i){
			if(selected){
				draw_set_alpha(.2)	
			}	else {
				draw_set_alpha(.1)	
			}
			draw_rectangle_noratio(move_textx+60-x+5,move_texty+i*30,move_textx+60+55-x,move_texty+i*30+30,false)	
		}
		draw_set_alpha(1)
		draw_set_halign(fa_left)
		draw_text_transformed_noratio(move_textx-x+5,move_texty+i*30,hsv_name[i] + ":",2,2,0)
	}
	gpu_set_colorwriteenable(true,true,true,true)
	surface_reset_target()
	surf_main_update = false;
}

var move_textx = x_shift-235
var move_texty = y_shift-15

draw_surface(surf,global.display_x+x*global.scale_factor,global.display_y)

// draw color

draw_rectangle_color_ratio(10+xxx+5,yyy-35-20,10+xxx+300,yyy-20-15,
c_black,color_map,
color_map,c_black,
false)

var hextext_x = x+menu_width*.5
var hextext_y = y+menu_height-30
draw_set_font(global.largeFont)
draw_set_color(global.text_fgcolor)
draw_set_halign(fa_left)
draw_text_transformed_ratio(hextext_x-50,hextext_y-2,"#",1,1,0)
draw_set_halign(fa_right)
draw_text_transformed_ratio(hextext_x+50,hextext_y,selected_color_display,1,1,0)
draw_set_font(global.normalFont)
draw_set_color(global.text_fgcolor)
draw_set_halign(fa_right)
// draw color info
for(var i = 0; i < 3; i++){
	
	if(selected && menupos-1 == i){
		draw_set_alpha(1)
		draw_text_transformed_ratio(move_textx+130,move_texty+i*30,stringer,2,2,0)
	} else { 
		draw_text_transformed_ratio(move_textx+130,move_texty+i*30,string(color_rgb[i]),2,2,0)	
	}
}

// draw color info
var move_textx = x_shift-90

for(var i = 0; i < 3; i++){
	if(selected && menupos-4 == i){
		draw_set_alpha(1)
		draw_text_transformed_ratio(move_textx+120,move_texty+i*30,stringer,2,2,0)
	} else {
		draw_text_transformed_ratio(move_textx+120,move_texty+i*30,string(color_hsv[i]),2,2,0)
	}
}

// draw selected hue
draw_set_color(global.menu_bgcolor)
if(menupos == 8){
	if(selected){
		draw_set_color(selected_color_component)
	} else {
		draw_set_color($777777)
	}
}
draw_circle_ratio(10+xxx+5 + 295*col_pos[0],yyy-20+ 295*col_pos[1],11,false)
draw_set_color(global.menu_bgcolor)
if(menupos == 7){
	if(selected){
		draw_set_color(selected_color_component)
	} else {
		draw_set_color($777777)
	}
}
draw_rectangle_ratio(5-2+xxx+300*col_pos[2],yyy-35-20-5,
					15+2+xxx+300*col_pos[2],yyy-10-20,false)
draw_set_color(color_map)
draw_circle_ratio(10+xxx+5 + 295*col_pos[0],yyy-20+ 295*col_pos[1],7,false)
draw_set_color(selected_color)

draw_rectangle_ratio(7+xxx+300*col_pos[2],yyy-33-20-2,
					13+xxx+300*col_pos[2],yyy-12-20-3,false)

draw_rectangle_ratio(40+x_shift,y_shift,
				40+x_shift+size,size+y_shift,false)