var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _text = map[? "text"];
	var _side = map[? "side"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_text) &&
		!is_undefined(_side)){
		if(_side == DUEL_PLAYER_YOU){
			_text = textForceCharsPerLine(_text,25)
			// create the binary choice menu
	
			if(global.dont_record){
				var input = duelinput_take(0);
				
				if(string_pos("yes",input) > 0){
					duelinput_insert("choice_yes",duelController.field[duelController.active_player].hand,0);
				} else {
					duelinput_insert("choice_no",duelController.field[duelController.active_player].hand,0);
				}
				duelinput_execute(duelinput_length()-1);
			} else {
				var obj = instance_create(200,150,duelSub)
				obj.parent = id;
				var me = id;
				with(obj){
					menu_add_option("Yes","",duel_anim_yes,-1)	
					menu_add_option("No","",duel_anim_no,-1)	
					menu_cancel = duel_anim_no
					option_y_offset = 100;
					option_x_offset = 200-30+string_height(_text);
					title = _text
					menu_width+=40
					menu_height = 200-30 + string_height(_text);
					draw_title = true;
					title_y = 20;
					title_x = 225
					open_sound  = sfx_sound_question
					execute = true;
				}
				menu_push(obj)
				return obj
			}
		} else {
			// maybe add AI later
			duelinput_insert("choice_yes",duelController.field[duelController.active_player].hand,0);
			duelinput_execute(duelinput_length()-1);
		}
	}

}

return noone;