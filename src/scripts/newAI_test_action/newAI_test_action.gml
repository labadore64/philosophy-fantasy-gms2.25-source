var input = argument[0];

var pos = string_pos(":",input);
var command = string_copy(input,1,pos-1)

input = string_delete(input,1,pos)
pos = string_pos(":",input);

var ori = real(string_copy(input,1,pos-1));

input = string_delete(input,1,pos)
pos = string_pos(":",input);

var side = real(string_copy(input,1,pos-1));

input = string_delete(input,1,pos)
pos = string_pos(":",input);

var ind = real(input);

var field = duelController.field[side];

if(command == "activate"){
	if(field.switch_counter == 0){
		if(duel_can_activate_philosopher(field.part[ori].card_array[input],
											field.part[DUEL_FIELD_ACTIVE].card_array[0])){
			return true;
		}
	}
} else if(command == "overlay"){
	if(field.overlay_counter == 0){
		if(duel_can_overlay(field.part[ori].card_array[input],
											field.part[DUEL_FIELD_ACTIVE].card_array[0])){
			return true;
		}
	}
} else if(command == "discard_end"){
	return true;
}


return false