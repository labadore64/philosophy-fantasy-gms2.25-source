var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

if(global.high_contrast){
	draw_clear(c_black)
} else {
	with(mainMenu){
		mainMenu_DrawBackground();
	}	
}

if(!surface_exists(surf_main)){
	surf_main = surface_create(512*global.scale_factor,512*global.scale_factor);	
	surf_main_update = true
} else {
	if(ScaleManager.updated){
		surface_resize(surf_main,512*global.scale_factor,512*global.scale_factor)	
		surf_main_update = true
	}
}
	
if(surf_main_update){
	surface_set_target(surf_main)
	
	var translation = -256

	var hair_color = portrait_color[CHARA_CUSTOM_COL_HAIR]
	var skin_color = portrait_color[CHARA_CUSTOM_COL_SKIN]
	var eye_color = portrait_color[CHARA_CUSTOM_COL_EYE]
	var mouth_color = portrait_color[CHARA_CUSTOM_COL_MOUTH]
		
	var earring_color = portrait_color[CHARA_CUSTOM_COL_EARRING]
	var hat_color = portrait_color[CHARA_CUSTOM_COL_HAT]
	var glasses_color = portrait_color[CHARA_CUSTOM_COL_GLASSES]
	var shirt_color = portrait_color[CHARA_CUSTOM_COL_SHIRT]
	var necklace_color = portrait_color[CHARA_CUSTOM_COL_NECKLACE]
			
	var scale = .9

	custom_face_draw1(scale,0,0,
		hair_color,skin_color,shirt_color,necklace_color,
		portrait_property[CHARA_CUSTOM_VAL_CUTE],
		portrait_property[CHARA_CUSTOM_VAL_BACKHAIR],
		portrait_property[CHARA_CUSTOM_VAL_BODY],
		portrait_property[CHARA_CUSTOM_VAL_NECK],
		portrait_property[CHARA_CUSTOM_VAL_SHIRT],
		portrait_property[CHARA_CUSTOM_VAL_NECKLACE])
	custom_face_draw2(scale,0,0,
		skin_color,eye_color,hair_color,mouth_color,
		portrait_property[CHARA_CUSTOM_VAL_FACE],
		portrait_property[CHARA_CUSTOM_VAL_NOSE],
		portrait_property[CHARA_CUSTOM_VAL_MOUTH],
		portrait_property[CHARA_CUSTOM_VAL_EYE],
		portrait_property[CHARA_CUSTOM_VAL_EYEBROW],
		portrait_property[CHARA_CUSTOM_VAL_SNOUT])
	custom_face_draw3(scale,0,0,
		hair_color,skin_color,earring_color,hat_color,glasses_color,
		portrait_property[CHARA_CUSTOM_VAL_GLASSES],
		portrait_property[CHARA_CUSTOM_VAL_EAR],
		portrait_property[CHARA_CUSTOM_VAL_EARRING],
		portrait_property[CHARA_CUSTOM_VAL_FRONTHAIR],
		portrait_property[CHARA_CUSTOM_VAL_HAT]);
		
	surface_reset_target()
		
	surf_main_update = false;
}

var pos = 20;

draw_surface(surf_main,
		global.display_x+450*global.scale_factor,
		global.display_y+105*global.scale_factor)

draw_set_alpha(alpha)
draw_set_color(col)
gpu_set_blendmode(bm_add)
draw_set_font(global.largeFont)
draw_text_transformed_ratio(pos,30,"Stats",4,4,0)

draw_set_font(global.normalFont);
draw_text_transformed_ratio(pos,120,"Name: " + global.character_name,2,2,0)
if(fave_cando){
	draw_text_transformed_ratio(pos,120+30,"Favorite Philosopher: " + obj_data.card_name[obj_data.current_main_card],2,2,0)
}
var start = 150+30;
var spacer = 30;

for(var i = 0; i < stat_size; i++){
	draw_text_transformed_ratio(pos,start+spacer*(1+i),stat_name[i] + stat_value[i],2,2,0)
}

gpu_set_blendmode(bm_normal)

menuParent_draw_cancel_button()

draw_letterbox();