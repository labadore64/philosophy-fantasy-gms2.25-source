var obj = instance_create(50,150,deckBuildSub);
obj.card_id = trunk_display[menupos]

var selectt = select_card
with(obj){
	var windowCharPerLine = 30
	
	if(selectt){
		menu_add_option("Select",textForceCharsPerLine("Select this card.",windowCharPerLine),deckBuildSub_script_select,spr_menu_activate)
		
	}
	
	menu_add_option("View",textForceCharsPerLine("View this card.",windowCharPerLine),deckBuildSub_script_view,spr_menu_info)
	
	menu_add_option("Cancel",textForceCharsPerLine("Go Back.",windowCharPerLine),menuParent_cancel,-1)
	
	event_user(0)
}