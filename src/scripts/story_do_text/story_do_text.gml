
var map = argument[0]
var name = argument[1]

var obj = instance_create(0,0,textStart_story)

var cando = true;

global.goto_map = MainRoom
var val = map[? "name"]

if(!is_undefined(val)){
	obj.duelist_name = val;
}

val = map[? "music"]

if(!is_undefined(val)){
	obj.music = asset_get_index("sfx_music_"+val)
}

val = map[? "text"]
if(!is_undefined(val)){
	obj.text_id = val;	
}

val = map[? "sprite"]
if(!is_undefined(val)){
	obj.duelist_sprite = asset_get_index("spr_portrait_"+val);
}

val = map[? "bg"]
if(!is_undefined(val)){
	obj.duelist_bg_image = "bg_" + val;
	var spritename = working_directory + "resources\\bg\\" + obj.duelist_bg_image  + ".png";
	obj.duelist_img_sprite =  sprite_add(spritename, 0, false, false, 0, 0)

}
global.story_next = "";
val = map[? "next"]
if(!is_undefined(val)){
	global.story_next = string_lower(val)	
}

val = map[? "duel"]
if(!is_undefined(val)){
	obj.duel_id = val;	
}

val = map[? "duel_level"]
if(!is_undefined(val)){
	obj.duel_level = real(val);
}
global.do_pack_select = false;
val = map[? "pack_select"]
if(!is_undefined(val)){
	if(string_lower(val) == "true"){
		global.do_pack_select = true	
	} else if(string_lower(val) == "flag"){
		if(!eventCheck(name)){
			global.do_pack_select = true	
		}
	}
}

val = map[? "once"]
if(!is_undefined(val)){
	if(string_lower(val) == "true"){
		if(eventCheck(name)){
			obj.duel_id = ""
			cando = false
		}
	}
}

val = map[? "next_room"]
if(!is_undefined(val)){
	val = string_lower(val)
	if(val == "this"){
		global.goto_map = room	
	} else if(val == "last"){
		global.goto_map = room_last	
	}  else if(val == "main"){
		global.goto_map = MainRoom
	} else {
		var test = asset_get_index(val);
		if(room_exists(test)){
			global.goto_map = test	
		}
	}
}

if(cando){
	with(obj){
		// test
		ds_queue_enqueue(load_queue,
						gameStart_script0a,
						textbox_script_new);
				
		// add text boxes
		ds_queue_enqueue(load_queue,
		textbox_script1_new)

				
		ds_queue_enqueue(load_queue,
						gameStart_script4);
				
		if(obj_data.loaded_particle_script > -1){
			script_execute(obj_data.loaded_particle_script)	
		}	
	}
} else {
	// stops infinite loops
	if(global.story_next == name){
		global.story_next = ""
	}
	instance_destroy(cando);	
}