with(duelController){
	
	
	with(running_obj){
		//instance_destroy();	
	}
	if(subscript_queue < 0){
		with(animationQueue){
			instance_destroy()	
		}
	}
	duelController.game_started = true;
	
	duelController_effects_phase();
	if(active_player == DUEL_PLAYER_YOU){
		// do your phase stuff
		if(phase_current == DUEL_PHASE_DRAW){
			duel_animation_run_script(duelCamera_center_camera_special)
			duel_animation_do_draw_until_5(duelController.field[duelController.active_player])
			duel_animation_run_script(duelCamera_center_camera_special)
			duel_animation_run_script(duel_phase_draw_check_valid)	
		} else if (phase_current == DUEL_PHASE_MAIN){
			duelController.disable_mouse_mode =false
			if(!mouse_mode){
				duelField_selectObject(field[0],DUEL_FIELD_HAND)
			} else {
				duel_animation_run_script(duelCamera_center_camera_special)
			}
			duel_animation_display_textbox("tutorial003")
			////duel_animation_resume_control()
		} else if (phase_current == DUEL_PHASE_LIMBO_RETURN){
			if(field[0].wait.card_array_size > 0){
				duel_animation_goto_part(field[0],DUEL_FIELD_LIMBO)
				duel_animation_merge_limbo(field[0]);	
				
			}
			duel_animation_run_script(duelCamera_center_camera_special)
			duel_animation_next_phase();
			
		} else if (phase_current == DUEL_PHASE_BATTLE){
			if(duelController.mouse_mode){
				var modi = 0;
				var scale = 2.75;
			
				if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
					modi -= TOOLTIP_CAMERA_ADJUST_Y
					scale = TOOLTIP_CAMERA_ADJUST_SCALE
				}
				

				duel_animation_move_camera(225+110,290+290-240+modi,scale,10)

			}
			duel_animation_goto_part(field[0],DUEL_FIELD_ACTIVE)
			duel_animation_display_textbox("tutorial007")
			
			var you = field[duelController.active_player].active_monster
			var them = field[!duelController.active_player].active_monster

			duel_animation_card_effect(you.card_array[0],"dialog",code_set_cardstate(you,0));
			duel_animation_card_effect(them.card_array[0],"dialog",code_set_cardstate(them,0));
			if(you.card_array_size > 1){
				duel_animation_card_effect(you.card_array[1],"dialog_overlay",code_set_cardstate(you,1));
			}
			if(them.card_array_size > 1){
				duel_animation_card_effect(them.card_array[1],"dialog_overlay",code_set_cardstate(them,1));
			}
			if(you.card_array_size > 2){
				duel_animation_card_effect(you.card_array[2],"dialog_overlay",code_set_cardstate(you,2));
			}
			if(them.card_array_size > 2){
				duel_animation_card_effect(them.card_array[2],"dialog_overlay",code_set_cardstate(them,2));
			}
			duel_animation_card_effect(you.card_array[0],"dialog_attack",code_set_cardstate(you,0));
			duel_animation_card_effect(them.card_array[0],"dialog_defend",code_set_cardstate(them,0));
			if(you.card_array_size > 1){
				duel_animation_card_effect(you.card_array[1],"dialog_overlay_attack",code_set_cardstate(you,1));
			}
			if(them.card_array_size > 1){
				duel_animation_card_effect(them.card_array[1],"dialog_overlay_defend",code_set_cardstate(them,1));
			}
			if(you.card_array_size > 2){
				duel_animation_card_effect(you.card_array[2],"dialog_overlay_attack",code_set_cardstate(you,2));
			}
			if(them.card_array_size > 2){
				duel_animation_card_effect(them.card_array[2],"dialog_overlay_defend",code_set_cardstate(them,2));
			}
			duel_animation_show_battle()
		} else if (phase_current == DUEL_PHASE_RETURN){
			duel_animation_run_script(duelCamera_center_camera_special)
			if(field[0].bench.card_array_size > 0 &&
				field[0].active_monster.card_array_size > 1){
				duel_animation_goto_part(field[0],DUEL_FIELD_BENCH)
				duel_animation_display_textbox("tutorial008")
				duel_animation_show_textbox("You can select a philosopher to switch the main philosopher, at the cost of its overlays.")
			} else {
				duel_animation_next_phase();
			}
		} else if(phase_current == DUEL_PHASE_END){
			duel_phase_do_end()

		} else {
			duelController_next_phase();
		}
	} else {
		// do enemy phase stuff	
		
		//newAI_start(DUEL_PLAYER_ENEMY);
		
		// to do: when its the enemy's turn, if there are no actions,
		// populate with actions. If the selected action cannot be taken
		// then move to the next phase
		
		if(phase_current == DUEL_PHASE_DRAW){
			duel_animation_run_script(duelCamera_center_camera_special)	
			duel_animation_do_draw_until_5(duelController.field[duelController.active_player])	
			duel_animation_run_script(duelCamera_center_camera_special)
			duel_animation_run_script(duel_phase_draw_check_valid)	
			duel_animation_run_script(duelCamera_center_camera_special)
		} else if (phase_current == DUEL_PHASE_LIMBO_RETURN){
			if(field[1].wait.card_array_size > 0){
				duel_animation_goto_part(field[1],DUEL_FIELD_LIMBO)
				duel_animation_merge_limbo(field[1]);	
			}
			duel_animation_run_script(duelCamera_center_camera_special)
			duel_animation_next_phase();
			
		} else if (phase_current == DUEL_PHASE_BATTLE){
			duel_animation_goto_part(field[1],DUEL_FIELD_ACTIVE)
			
			var modi = 0;
			var scale = 2.75;
			
			if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
				modi -= TOOLTIP_CAMERA_ADJUST_Y
				scale = TOOLTIP_CAMERA_ADJUST_SCALE
			}
			

			duel_animation_move_camera(225+110,290+290-240+modi,scale,10)

			duel_animation_display_textbox("tutorial005")
			
			var you = field[duelController.active_player].active_monster
			var them = field[!duelController.active_player].active_monster
			duel_animation_card_effect(you.card_array[0],"dialog",code_set_cardstate(you,0));
			duel_animation_card_effect(them.card_array[0],"dialog",code_set_cardstate(them,0));
			if(you.card_array_size > 1){
				duel_animation_card_effect(you.card_array[1],"dialog_overlay",code_set_cardstate(you,1));
			}
			if(them.card_array_size > 1){
				duel_animation_card_effect(them.card_array[1],"dialog_overlay",code_set_cardstate(them,1));
			}
			if(you.card_array_size > 2){
				duel_animation_card_effect(you.card_array[2],"dialog_overlay",code_set_cardstate(you,2));
			}
			if(them.card_array_size > 2){
				duel_animation_card_effect(them.card_array[2],"dialog_overlay",code_set_cardstate(them,2));
			}
			duel_animation_card_effect(you.card_array[0],"dialog_attack",code_set_cardstate(you,0));
			duel_animation_card_effect(them.card_array[0],"dialog_defend",code_set_cardstate(them,0));
			if(you.card_array_size > 1){
				duel_animation_card_effect(you.card_array[1],"dialog_overlay_attack",code_set_cardstate(you,1));
			}
			if(them.card_array_size > 1){
				duel_animation_card_effect(them.card_array[1],"dialog_overlay_defend",code_set_cardstate(them,1));
			}
			if(you.card_array_size > 2){
				duel_animation_card_effect(you.card_array[2],"dialog_overlay_attack",code_set_cardstate(you,2));
			}
			if(them.card_array_size > 2){
				duel_animation_card_effect(them.card_array[2],"dialog_overlay_defend",code_set_cardstate(them,2));
			}
			duel_animation_show_battle()
			duel_animation_display_textbox("tutorial006")
		} else if (phase_current == DUEL_PHASE_MAIN){
			if(!mouse_mode){
				duelField_selectObject(field[0],DUEL_FIELD_HAND)
			} else {
				duel_animation_run_script(duelCamera_center_camera_special)
			}
			// remove this later
			duel_ai_main_do();	
		} else if (phase_current == DUEL_PHASE_RETURN){
			duel_animation_run_script(duelCamera_center_camera_special)
			// remove this later
			duel_ai_return_do();	
		} else if (phase_current == DUEL_PHASE_END){
			
			duel_phase_do_end()	
			
			// remove this later
			duel_animation_end_turn()
		} 	else {
			duelController_next_phase();
		}
	}
}

return noone;