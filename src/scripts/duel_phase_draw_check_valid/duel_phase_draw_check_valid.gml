// check if you have a valid game state.
// At the beginning of the turn, you must start with a philosopher as an active philosopher.
// if not, You must select a benched character to set as your active.
// if neither exists, you then merge your limbo with your deck, and 
// you must keep redrawing until you have an active philosopher.

var field = duelController.field[duelController.active_player]
var cando = true;
if(field.active_monster.card_array_size == 0){
	var philo_count = 0;
	for(var i = 0; i < field.hand.card_array_size; i++){
		if(obj_data.card_type[field.hand.card_array[i]] == CARD_TYPE_PHILOSOPHER &&
			!card_has_keyword(field.hand.card_array[i],"timid")){
			philo_count++;	
		}
	}
	
	if(philo_count == 0){
		
		if(field.bench.card_array_size == 0){
			duel_animation_merge_limbo(field);
			duel_animation_return_hand(field);
			duel_animation_do_draw_until_5(field)
			cando = false;
		}
	}
}

if(cando){
	duel_animation_next_phase();	
}
