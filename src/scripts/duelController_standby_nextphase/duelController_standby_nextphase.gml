with(duelController){
	with(running_obj){
		instance_destroy()	
	}
	running_obj = noone;
	
	if(field[active_player].wait.card_array_size != 0){
		duelController_next_phase();
	} else {
		phase_current = DUEL_PHASE_LIMBO_RETURN
		duelController_next_phase();
	}
}