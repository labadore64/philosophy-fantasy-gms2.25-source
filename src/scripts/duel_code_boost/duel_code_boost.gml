if(!argument[0]){
	var target_index = -1; // the card index to move
	var target_origin = -1; // the location of the target
	var target_destination = -1; // where to move the target
	var target_side = duelController.active_player; // which side the target is on
	var facedown = false;

	var this = code_get_this();

	var block = false;

	var att ="";
	var def = "";
	var enemy = DUEL_PLAYER_YOU
	
	if(string_pos("pos_enemy",this)){
		enemy = DUEL_PLAYER_ENEMY	
	}

	var stringer = "";
	for(var i = 0; i < code_argument_size; i++){

		if(string_pos("att",code_arguments[i]) > 0){
			var att = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
		if(string_pos("def",code_arguments[i]) > 0){
			var def = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
		if(string_pos("enemy",code_arguments[i]) > 0){
			if(enemy == DUEL_PLAYER_YOU){
				enemy = DUEL_PLAYER_ENEMY
			} else {
				enemy = DUEL_PLAYER_YOU
			}
		}

	}

	duel_animation_boost(att,def,enemy)

}