with(duelController){
	var val = ds_map_find_value(card_status,argument[0]);	
	if(!is_undefined(val)){
		if(val > 0){
			val--;
			
			if(val == 0){
				removeStatusString(argument[0]);
			} else {
				ds_map_replace(card_status,argument[0],val);
			}
		}
	}
}