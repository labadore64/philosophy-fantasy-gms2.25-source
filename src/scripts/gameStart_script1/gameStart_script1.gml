textboxShow("startgame001","Mystery Man")
textbox.portrait_sprite = spr_portrait_mystery_man
running_object = textbox;

textbox.newalarm[2] = TIMER_INACTIVE
textbox.newalarm[3] = TIMER_INACTIVE

with(textbox){
	event_perform(ev_alarm,2)
	event_perform(ev_alarm,3)
	with(obj_dialogue){
		newalarm[0] = TIMER_INACTIVE
		event_perform(ev_alarm,0)
	}
}