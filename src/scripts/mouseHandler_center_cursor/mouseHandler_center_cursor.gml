if(global.mouse_snap){
	with(MouseHandler){
		var i = argument[0]

		if(mouse_count > i){
			var xx = mouse_points_trans[i,0] 
			var yy = mouse_points_trans[i,1] 
			var w = mouse_points_trans[i,2] - xx
			var h = mouse_points_trans[i,3] - yy

			window_mouse_set(xx+w*.5,yy+h*.5)
		}
	}
}