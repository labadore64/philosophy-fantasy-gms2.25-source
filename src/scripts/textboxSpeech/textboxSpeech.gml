if(global.text_sound){
	var current_char = string_lower(char_array[message_current,cutoff]);

	var portrait_update = 0;

	if(current_char == " " ||
		current_char == "." ||
		current_char == "!" ||
		current_char == "," ||
		current_char == ";" ||
		current_char == ":" ||
		current_char == "\n" ||
		current_char == "\'" ||
		current_char == "\"" ||
		current_char == "?"){
			//punctuation
		audio_stop_sound(text_sound1);
		audio_stop_sound(text_sound2);
		audio_stop_sound(text_sound3);
		audio_stop_sound(text_sound4);
		portrait_update = 0;
	} else if(current_char == "a" ||
		current_char == "e" ||
		current_char == "i" ||
		current_char == "o" ||
		current_char == "y" ||
		current_char == "u"){
			//vowels
		if(talking_sound ){
			soundfxPlay(text_sound1);
		}
		portrait_update = 1;
	} else if(current_char == "c" ||
		current_char == "d" ||
		current_char == "k" ||
		current_char == "t" ||
		current_char == "q" ||
		current_char == "x"){
		//hard sounds
		if(talking_sound ){
			soundfxPlay(text_sound2);
		}
		portrait_update = 2;
	} else if(current_char == "f" ||
		current_char == "h" ||
		current_char == "l" ||
		current_char == "m" ||
		current_char == "r" ||
		current_char == "s" ||
		current_char == "v" ||
		current_char == "w" ||
		current_char == "z" ||
		current_char == "n"){
		//soft sounds
		portrait_update = 3;
		if(talking_sound ){
			soundfxPlay(text_sound3);
		}
	} else if(current_char == "b" ||
		current_char == "p" ||
		current_char == "g" ||
		current_char == "j"){
		//pop sounds
		portrait_update = 4;
		if(talking_sound ){
			soundfxPlay(text_sound4);
		}
	}

}