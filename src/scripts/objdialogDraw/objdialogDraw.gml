
	//if(!(access_draw() && !global.force_graphics)){
	/// @description Text

	if(global.text_full  && room != BattleRoom){
		if(!surface_exists(surf)){
			surf = surface_create_access(800*global.scale_factor,600*global.scale_factor);
			surface_draw = true;

			surface_set_target(surf)
			draw_clear_alpha(global.text_bgcolor[0],0)
			surface_reset_target()
		} else {
			if(ScaleManager.updated){
				surface_resize(surf,800*global.scale_factor,600*global.scale_factor);
			}
		}
	} else {
		if(!surface_exists(surf)){
			surf = surface_create_access(800*global.scale_factor,120*global.scale_factor);
			surface_draw = true;

			surface_set_target(surf)
			draw_clear(global.text_bgcolor[0])
			surface_reset_target()
		} else {
			if(ScaleManager.updated){
				surface_resize(surf,800*global.scale_factor,120*global.scale_factor);
			}
		}
	
	}

	//Set Font
	draw_set_halign(fa_left);
	gpu_set_colorwriteenable(true,true,true,true)
	//Set Colour
	
	draw_set_font(fontz);
	draw_set_alpha(1)
	//Read in array length


	draw_set_halign(fa_center)

	if(cutoff < string_lenger && (timer > delay)){
		surface_draw = true;
	}

	if(force_draw){
		surface_draw = true;	
	}
	draw_set_colour(global.text_fgcolor);
	
	if(surface_draw){
	//If there are messages

		surface_set_target(surf);
		var ttY = 15
		i=1;
		if(global.text_full  && room != BattleRoom){
			draw_clear_alpha(global.text_bgcolor[0],0)
		} else {
			draw_clear_alpha(global.text_bgcolor[0],1)
		}
		//i = clamp(cutoff-1,1,10000000);
	
	
		//line = last_line;
		//space = last_space;
	
		draw_set_alpha(1)		
		if(global.color_gradient){
			draw_rectangle_color_noratio(0,0,800,120,
										global.text_bgcolor[0],global.text_bgcolor[0],
										global.text_bgcolor[1],global.text_bgcolor[1],
										false)
		} else {
			draw_set_color(global.text_bgcolor[0])
			draw_rectangle_noratio(0,0,800,120,
										false)
		}
		draw_set_colour(global.text_fgcolor);
		
		//gpu_set_colorwriteenable(true,true,true,false)
		if (message_end > 0)
		{
			//modifier_last = 0;
			modifier = 0;
		    //Text
		    while(i <= string_lenger && i <= cutoff)
		    {   
		        //check for modifier
				modifier_last = modifier;
			
		        if (char_array[message_current,i] == "\\")
		        {
					++i;
		            modifier = real(char_array[message_current,i]);
		            ++i;
		        }
        
				//draw set color optimization
				if(modifier_last != modifier){
					if(modifier == 0 ||
						modifier == 1 ||
						modifier == 4){
						draw_set_colour(global.text_fgcolor);
					} else if (modifier == 2
						|| modifier == 3){
						draw_set_colour(my_color);	
					}
				}
		
		        //Go to next line
		        //Check if the current word fits inside the text box, and if not we go to the next line
		        length = 0;
		        while (char_array[message_current,i] != " " && i <= string_lenger)
		        {
		            i++;
		            length++;
		        }
        
		        if (space+length > lineEnd)
		        {
		            space = 0;
		            line++;
		        }
		        i -= length;
		
		        //Draw Text
		        switch(modifier)
		        {
		            case 0: //normal
		            {
                
		                draw_text_transformed_textbox_ratio(tX+(space*charWidth), ttY+(line_spacer*line), (char_array[message_current,i]),text_scale,text_scale,0);
		                break;
		            }
		            case 2: //color
		            {
		                //draw_set_colour(my_color);
		                draw_text_transformed_textbox_ratio(tX+(space*charWidth), ttY+(line_spacer*line), (char_array[message_current,i]),text_scale,text_scale,0);
		                break;
		            }
		        }
        
		        //Move to next character
		        space++;
		        i++;
		    }
		}

		surface_reset_target();
		surface_draw = false;
		gpu_set_colorwriteenable(true,true,true,true)

	}


	//If there are messages
	if (message_end > 0)
	{
		random_this_step = false;
		line = 0;
		space = 0;
		i = 1;
		draw_surface(surf,global.display_x,global.display_y+(tY-15)*global.scale_factor)

	    //Draw chat box
	    //draw_sprite(spr_chatbox, 0, __view_get( e__VW.WView, 0 )/2, __view_get( e__VW.HView, 0 )-65);
	    //Draw Portrait
	    //if (portrait != "none") draw_sprite(portrait, 0, 10, __view_get( e__VW.HView, 0 )-60);

    
	    //If we are done printing out the current message
	    if (cutoff == string_lenger)
	    {
			if(continue_draw){
	        //draw blinking cursor
			if(global.text_full  && room != BattleRoom){
					draw_sprite_extended_ratio(spr_talk_continue,
									ScaleManager.spr_counter+15,
									global.display_x+465*global.scale_factor,
									global.display_y+430*global.scale_factor,
									.5*global.scale_factor,.5*global.scale_factor,0,global.text_fgcolor,1);
			} else {
					draw_sprite_extended_ratio(spr_talk_continue,
									ScaleManager.spr_counter+15,
									global.display_x+765*global.scale_factor,
									global.display_y+580*global.scale_factor,
									.5*global.scale_factor,.5*global.scale_factor,0,global.text_fgcolor,1);
			}
			}
	    }
    
		//cutoff = text_box_object.counter;
	
		//modifier_last = -1;
		draw_set_colour(global.text_fgcolor);
	    //Text
		modifier = 0;
	    while(i <= string_lenger && i <= cutoff)
	    {   
	        //check for modifier
			modifier_last = modifier;
		
	        if (char_array[message_current,i] == "\\")
	        {
				++i;
	            modifier = real(char_array[message_current,i]);
	            ++i;
	        }
        
	        //Go to next line
	        //Check if the current word fits inside the text box, and if not we go to the next line
	        length = 0;
	        while (char_array[message_current,i] != " " && i <= string_lenger)
	        {
	            i++;
	            length++;
	        }
        
	        if (space+length > lineEnd)
	        {
	            space = 0;
	            line++;
	        }
	        i -= length;
        
			//draw set color optimization
			if(modifier_last != modifier){
				if(modifier == 0 ||
					modifier == 1 ||
					modifier == 4){
					draw_set_colour(global.text_fgcolor);
				} else if (modifier == 2
					|| modifier == 3){
					draw_set_colour(my_color);	
				}
			}
			i_value_mod = sin(.75*.75*(i+t))
			i_value_mody = cos(.75*.75*(i+t))
	        //Draw Text
	        switch(modifier)
	        {
				/*
	            case 0: //normal
	            {
                
	                draw_text(tX+(space*charWidth), tY+(line_spacer*line), (char_array[message_current,i]));
	                break;
	            }
				*/
	            case 1: //shaky
	            {
	                //draw_set_colour(c_white);
					random_this_step = true;
	                draw_text_transformed_ratio(tX+(space*charWidth)+random_val+i_value_mod, tY+(line_spacer*line)+random_valy+i_value_mody, (char_array[message_current,i]),text_scale,text_scale,0);
	                break;
	            }
				/*
	            case 2: //color
	            {
	                //draw_set_colour(my_color);
	                draw_text(tX+(space*charWidth), tY+(line_spacer*line), (char_array[message_current,i]));
	                break;
	            }
				*/
	            case 3: //color and shake
	            {
	                //draw_set_colour(my_color);
					random_this_step = true;
	                draw_text_transformed_ratio(tX+(space*charWidth)+random_val+i_value_mod, tY+(line_spacer*line)+random_valy+i_value_mody, (char_array[message_current,i]),text_scale,text_scale,0);
	                break;
	            }
	            case 4: //Sine movement
	            {
	                var so = t + i;
	                var shift = sin(so*pi*freq/60)*amplitude;
	                draw_set_colour(global.text_fgcolor);
	                draw_text_transformed_ratio(tX+(space*charWidth), tY+(line_spacer*line)+shift, (char_array[message_current,i]),text_scale,text_scale,0);
	                break;
	            }
	            case 5: //Gradient Text
	            {
	                draw_set_colour(make_colour_hsv((t+i)%360, 255, 255));
	                draw_text_transformed_ratio(tX+(space*charWidth), tY+(line_spacer*line), (char_array[message_current,i]),text_scale,text_scale,0);
	                break;
	            }
	            case 6: //Gradient & Sine
	            {
	                draw_set_colour(make_colour_hsv((t+i)%360, 255, 255));
	                var so = t + i;
	                var shift = sin(so*pi*freq/60)*amplitude;
	                draw_text_transformed_ratio(tX+(space*charWidth), tY+(line_spacer*line)+shift, (char_array[message_current,i]),text_scale,text_scale,0);
	                break;
	            }
	        }
        
	        //Move to next character
	        space++;
	        i++;
	    }
	
		last_line = line;
		last_space = space;
	}

	if(cutoff == string_lenger && !finish_draw){
		surface_draw = true;
		finish_draw = true;
	}

	force_draw = false;
	gpu_set_colorwriteenable(true,true,true,true)
	//}