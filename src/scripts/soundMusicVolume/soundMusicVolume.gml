/// @description - set volume of music
/// @param volume

var vol = argument[0]

//check date. if date is April 3rd, play no music.

var multiplier = 1;

if(current_month == 4 && current_day == 3){
	multiplier = 0;	
}

vol = vol * multiplier;

with(MusicPlayer){
	volume = vol;
	audio_sound_gain(current_song,volume,0);
}