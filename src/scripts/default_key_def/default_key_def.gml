
add_key_def("Up","Move up","up")
add_key_def("Down","Move down","down")
add_key_def("Left","Move left","left")
add_key_def("Right","Move right","right")
add_key_def("Select","Select option","select")
add_key_def("Cancel","Cancel","cancel")