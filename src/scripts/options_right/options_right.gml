if(menu_sprite[menupos] >= 0){
	var menusize = array_length_2d(option_text,menu_sprite[menupos])
	
	if(menusize > 1){
		var val = options_map[? menu_name[menupos]];
		if(is_undefined(val)){
			val = 0;	
		}
		val--
		if(val < 0){
			val = menusize-1;	
		}
		if(val > menusize-1){
			val = 0	
		}
	
		options_map[? menu_name[menupos]] = val
	
		// the select values
		for(var i = 0; i < menu_count; i++){
			option_select[i] = 0;
			var val = options_map[? menu_name[i]];
			if(!is_undefined(val)){
				option_select[i] = val;	
			}
		}
	
		options_update();
		
		tts_clear()
		tts_say(option_text[menu_sprite[menupos],option_select[menupos]])
	
		if(global.menu_sounds){soundfxPlay(sfx_sound_menu_default);}
		RUMBLE_MOVE_MENU
	}

	if(menu_script[menupos] > -1){
		script_execute(menu_script[menupos])	
	}
} else {
	if(menu_script[menupos] > -1){
		script_execute(menu_script[menupos])	
	}	
}