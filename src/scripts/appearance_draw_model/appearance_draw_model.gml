// draw model selection list
// draw color selection list
var counter = 0;

for(var i = option_top; i < option_top+per_line; i++){
	
	var size = option_spacer-40
	
	var x_start = 190;
	var y_start = option_y_offset+10;
	var len = 275

	draw_set_font(global.normalFont);
	draw_set_color(global.text_fgcolor)
	draw_set_alpha(1)
	draw_text_transformed_noratio(15+10,option_y_offset+ option_spacer*counter,model_title[i],2,2,0)

	var modid = model_id[i];
	draw_text_transformed_noratio(15+10,option_y_offset+35+ option_spacer*counter,property_part_names[modid,portrait_property[modid]],1,1,0)
	
	//sprite
	
	if(!surface_exists(model_surface[counter])){
		model_surface[counter] = surface_create(size*global.scale_factor,size*global.scale_factor);	
		model_surf_update = true
	} else {
		if(ScaleManager.updated){
			surface_resize(model_surface[counter],size*global.scale_factor,size*global.scale_factor)	
			model_surf_update = true
		}
	}
	var col = c_white;
	if(model_surf_update){
		surface_set_target(model_surface[counter])
		draw_clear(portrait_color[CHARA_CUSTOM_COL_BACKGROUND]);
		for(var j = 0; j < model_sprite_size[i]; j++){
			col = property_default_color[modid,j];
			if(col != c_white){
				col = portrait_color[property_default_color[modid,j]];	
			}
			draw_sprite_extended_noratio(property_sprite[modid,j],portrait_property[modid],
										property_x[modid]+size*.5,
										property_y[modid]+size*.5,
										.15*property_scale[modid],.15*property_scale[modid],
										0,col,1)
		}
		surface_reset_target()
	}
	
	draw_surface(model_surface[counter],
					(670-option_x_offset)*global.scale_factor,
					(131+counter*110)*global.scale_factor)

	
	//property_part_names[
	counter++
}

model_surf_update = false