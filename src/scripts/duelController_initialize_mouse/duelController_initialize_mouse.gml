mouseHandler_menu_default();
if(!global.dont_record){
	if(game_started){
		window_set_cursor(cr_none);
		mouseHandler_clear()
		if(active_player == DUEL_PLAYER_YOU){
			pop_menu_pos = 0;
			mouseHandler_add(0+25,140+10,800-25,485-10,-1,"")
			
			if(!instance_exists(navigation_menu)){
				mouseHandler_add(380,85,420,135,duelController_pop_menu,"More")
			}
			// help button

			if(instance_exists(duelNavigation)){
				if(duelNavigation.x > 450){
					side_button_right = false
				} else {
					side_button_right = true;	
				}
			}
			
			if(side_button_right){
				mouseHandler_add(750,150,790,200,duelController_open_help,"Help")
				mouseHandler_add(750-50,150,790-50,200,duelController_open_nav,"Navigation")
			} else {
				mouseHandler_add(20,150,50,200,duelController_open_help,"Help")
				mouseHandler_add(60,150,105,200,duelController_open_nav,"Navigation")	
			}
			// pop menu
			
			// add buttons
			if(pop_menu_active){
				pop_menu_pos = MouseHandler.mouse_count;
				for(var i = 0; i < side_button_total; i++){
					mouseHandler_add(side_button_x+side_button_spacer*(i),side_button_y,
									side_button_x+side_button_spacer*(i+1),side_button_y+50,
									side_button_script[i],side_button_name[i])
				}
			} else {
			
				if(instance_exists(navigation_menu)){
					script_execute(navigation_menu.mouse_menu_script );	
				}
			}
		}
	}
}