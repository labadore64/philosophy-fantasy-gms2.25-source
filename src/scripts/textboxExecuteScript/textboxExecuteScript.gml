
var getscript = ds_map_find_value(scripts,line);

if(!is_undefined(getscript)){
	//if the scriptval is positive
	var scriptval = real(getscript);
	if(script_exists(scriptval)){
		script_execute(scriptval,arguments);	
		ds_map_delete(scripts,line);
	}
} else {
	var size = ds_list_size(text);
	var test_index = line-size;	
	getscript = ds_map_find_value(scripts,test_index);
	if(!is_undefined(getscript)){
		//if the scriptval is negative
		var scriptval = real(getscript);
		if(script_exists(scriptval)){
			script_execute(scriptval,arguments);	
			ds_map_delete(scripts,test_index);
		}
	}
	
}