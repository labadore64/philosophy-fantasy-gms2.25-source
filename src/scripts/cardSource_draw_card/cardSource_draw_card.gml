draw_set_color(c_black)
var size_x = 26;
var size_y = 37

draw_rectangle(argument[0]-size_x,argument[1]-size_y,argument[0]+size_x,argument[1]+size_y,false)


draw_sprite_ext(spr_duel_cards,argument[2],
						argument[0],argument[1],
						.55,.55,0,c_white,1);
						
if(global.high_contrast || global.card_icon){
	if(argument[2] < 5){
		if(argument[2] == 0){
			argument[2] = 2
		}
		else if(argument[2] == 1){
			argument[2] = 0	
		}
		else if(argument[2] == 2){
			argument[2] = 1
		}
		
	draw_sprite_ext(spr_card_white_type,argument[2],
							argument[0],-5+argument[1],
							.75,.75,0,c_white,1);
	}
}