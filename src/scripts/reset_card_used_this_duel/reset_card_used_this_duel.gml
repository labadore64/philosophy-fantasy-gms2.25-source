reset_card_used_this_turn();
duelField_counters_reset();	
reset_card_used_this_turn();
with(obj_data){
	for(var i = 0; i < cards_total; i++){
		// how many times a card has been used this game
		card_used_this_game[i] = 0;	
	}
}