var stringe = argument[0];
var side = argument[1];

if(duelController.ai_mode){
	if(stringe == "deck"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_DECK]
	} else if (stringe == "hand"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_HAND]
	} else if (stringe == "theory"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_THEORY]
	} else if (stringe == "active"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_ACTIVE]
	} else if (stringe == "limbo"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_LIMBO]
	} else if (stringe == "bench"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_BENCH]
	}
	// special exception but gets treated as active
	else if (stringe == "overlay"){
		return duelController.field[side].ai_field.part[DUEL_FIELD_ACTIVE]
	}
} else {
	if(stringe == "deck"){
		return duelController.field[side].part[DUEL_FIELD_DECK]
	} else if (stringe == "hand"){
		return duelController.field[side].part[DUEL_FIELD_HAND]
	} else if (stringe == "theory"){
		return duelController.field[side].part[DUEL_FIELD_THEORY]
	} else if (stringe == "active"){
		return duelController.field[side].part[DUEL_FIELD_ACTIVE]
	} else if (stringe == "limbo"){
		return duelController.field[side].part[DUEL_FIELD_LIMBO]
	} else if (stringe == "bench"){
		return duelController.field[side].part[DUEL_FIELD_BENCH]
	}
	// special exception but gets treated as active
	else if (stringe == "overlay"){
		return duelController.field[side].part[DUEL_FIELD_ACTIVE]
	}
}

return noone;