/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;
global.cardpack = argument[0]
// gets the count of the items
var filename = working_directory + "resources\\card\\" + argument[0] + "\\card.txt";
var ext = ".ini";
var testname = "";

testname = filename//+string(itemcount)+ext;

archetype_name[0] = "";
archetype_count = 0;

var bg_sprite_count = 0;
bg_sprite[0] = -1;
bg_sprite_name[0] = ""

if(file_exists(testname)){
	//loads data
	// read the code variable cuz ini doesn't like it for some reason
	var file = file_text_open_read(testname)
	var ss;
	var len;
	while (!file_text_eof(file))
	{

		card_name[itemcount] = text_file_readln_correct(file)
		for(var i = 0; i < 3; i++){
			card_name_chara[itemcount,i] = string_upper(string_char_at(card_name[itemcount],i))
		}
		var type = string_lower(text_file_readln_correct(file));
		if(type == "philosopher"){
			card_type[itemcount] = CARD_TYPE_PHILOSOPHER
		} else if (type == "metaphys"){
			card_type[itemcount] = CARD_TYPE_METAPHYS
		} else {
			card_type[itemcount] = CARD_TYPE_EFFECT
		}
	
		var type = string_lower(text_file_readln_correct(file));
		if(type == "mind"){
			card_element[itemcount] = CARD_ELEMENT_MIND	
		} else if (type == "spirit"){
			card_element[itemcount] = CARD_ELEMENT_SPIRIT
		} else {
			card_element[itemcount] = CARD_ELEMENT_MATTER
		}
	
		card_sprite_name[itemcount] = text_file_readln_correct(file)
		var card_sprite_bg_name = text_file_readln_correct(file)
		
		var spritename = working_directory + "resources\\card\\" + argument[0] + "\\sprite\\fg\\spr_card_" + card_sprite_name[itemcount] + ".png";
	
		card_sprite[itemcount] = review_philosopher//sprite_add(spritename, 0, false, false, 0, 0);//asset_get_index("spr_card_" + card_sprite_name[itemcount]);
	
		// add bg sprite
		var cando = true;
		var sprite = 0;

		for(var i = 0; i < bg_sprite_count; i++){
			if(bg_sprite_name[i] == card_sprite_bg_name){
				cando = false;	
				sprite = i;
				break;
			}
		}
		
		// add sprite if new
		if(cando){
			var spritename = working_directory + "resources\\card\\" + argument[0] + "\\sprite\\bg\\bg_" + card_sprite_bg_name + ".png";
			bg_sprite[i] = review_philosopher//sprite_add(spritename, 0, false, false, 0, 0);
			bg_sprite_name[i] = card_sprite_bg_name
			sprite = bg_sprite_count
			bg_sprite_count++
		}
		
		card_sprite_bg[itemcount] = bg_sprite[sprite];
	
		card_archetype1[itemcount]  = text_file_readln_correct(file)
		objdata_add_archetype(card_archetype1[itemcount])
		card_archetype2[itemcount]  = text_file_readln_correct(file)
		objdata_add_archetype(card_archetype2[itemcount])
	
		card_attack[itemcount] = real(text_file_readln_correct(file))
		card_defense[itemcount] = real(text_file_readln_correct(file))
		card_attack_overlay[itemcount] = real(text_file_readln_correct(file))
		card_defense_overlay[itemcount] = real(text_file_readln_correct(file))
	
		card_bio[itemcount] = text_file_readln_correct(file)
		card_years[itemcount] = text_file_readln_correct(file)
		card_keywords[itemcount] = text_file_readln_correct(file)
		card_effect_desc[itemcount] = newline(text_file_readln_correct(file))
		card_code[itemcount,0] = "";
		file_text_readln(file)
		var stringer = "";
		while(string_pos("=====",stringer) == 0){
	
			if(stringer != ""){
				card_code[itemcount,0] += stringer+ " ";
			}
			stringer = file_text_readln(file);
		}
		
		card_code[itemcount,0] = string_replace_all(card_code[itemcount,0],"\r\n","")
		card_code_count[itemcount] = 0
		
		// split the code into sections split by }
		var fullstring = card_code[itemcount,0];
		card_code[itemcount,0] = "";
		var counter = 0;
		var len = 0;
		var pos = 1;
		
		while(pos > 0){
			pos = string_pos("}",fullstring);
			if(pos > 0){
				len = string_length(fullstring);
				var space = 1;
				while(string_char_at(fullstring,space) == " "){
					space++;
				}
				
				card_code[itemcount,counter] += string_copy(fullstring,space,pos-space+1);
				card_code[itemcount,counter] = 
							string_replace(
							string_replace(card_code[itemcount,counter],
									" { ",
									"{"),
								" }",
								"}");
				fullstring = string_delete(fullstring,space,pos-space+1);
				if(string_char_at(fullstring,pos-space) == "\\"){
					continue;	
				} else {
					if(pos+space < len){
						counter++;	
						card_code[itemcount,counter]="";
					}
				}
			}
		}
		
		card_code_count[itemcount] = array_length_2d(obj_data.card_code,itemcount);
		
		itemcount++;
		// limit to 99 cards
		if(PAID_VERSION == 0 && itemcount > 99){
			break;	
		}
	}
	
	file_text_close(file);
	
	// stuff used for tracking variables
	card_in_trunk[itemcount] = 3;
	
	// how many times a card has been used this turn.
	card_used_this_turn[itemcount] = 0;
	
	// how many times a card has been used this game
	card_used_this_game[itemcount] = 0;
	
	itemcount++;
	show_debug_message(string(itemcount))
	

	
	testname = filename+string(itemcount)+ext;
}

// alpha archetype names
var list = ds_list_create()

for(var i=0; i < archetype_count; i++){
	ds_list_add(list,archetype_name[i])	
}

ds_list_sort(list,true)

for(var i=0; i < archetype_count; i++){
	archetype_name[i] = list[|i]
}


ds_list_destroy(list)

cards_total = itemcount-1;

current_deck = ds_list_create(); // your deck list. can be not 20, but you can't enter duels.
current_main_card = 11; //which card is your main

load_keywords();/**/