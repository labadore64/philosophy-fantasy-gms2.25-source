var posx = argument[0]*global.scale_factor;
var posy = argument[1]*global.scale_factor;
var posx2 = argument[2]*global.scale_factor;
var posy2 = argument[3]*global.scale_factor;

draw_rectangle_color(global.display_x 
				+posx,
				global.display_y 
				+posy,
				global.display_x 
				+posx2,
				global.display_y
				+posy2,
				argument[4],
				argument[5],
				argument[6],
				argument[7],
				argument[8]);