if(!argument[0]){
	var target_index = -1; // the card index to move
	var target_origin = -1; // the location of the target
	var target_destination = -1; // where to move the target
	var target_side = duelController.active_player; // which side the target is on
	var test_value = "";

	var this = code_get_this();
	var this_enemy = false;
	if(string_pos("pos_enemy",this) > 0){
		this_enemy = true;
	}

	var return_value = 0;

	var return_variable = "return"
	var targe = "this";

	var stringer = "";
	for(var i = 0; i < code_argument_size; i++){

		if(string_pos("return",code_arguments[i]) > 0){
			return_variable = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		
			// the variable must exist. this forces uses of the predefined registers
			// to prevent any funny business
			if(!ds_map_exists(duelController.code_values,return_variable)){
				return_variable = "return";	
			}
		}
	
	
		// parse targets
		if(string_pos("target:",code_arguments[i]) > 0){
			targe = string_copy(code_arguments[i],
									string_pos(":",code_arguments[i])+1,
									string_length(code_arguments[i])-string_pos(":",code_arguments[i]))
		}
	
	}

	duel_animation_card_at(targe,return_variable);
} else {

}