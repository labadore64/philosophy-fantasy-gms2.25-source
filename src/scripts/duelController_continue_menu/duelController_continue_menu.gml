
			
		with(duelController){
			if(pop_menu_active){
				duelController_pop_menu();	
			}
		}

		var obj = instance_create(50,150,duelSub);
		obj.menu_selected = -1
		obj.parent = id

		var me = id;

		with(obj){
			var windowCharPerLine = 30

			x = 200+50;
			menu_height = 180
			menu_width = 400-100
			menu_add_option("Resume","Continue your turn.",menuParent_cancel,-1)
			if(duelController.phase_current != DUEL_PHASE_STANDBY){
				if(duelController.turns != 0){
					if(duelController.field[0].active_monster.card_array_size > 0){
						if(duelController.phase_current == DUEL_PHASE_MAIN){
							menu_add_option("Dialog","Move to the Dialog Phase.",duelController_continue_nextphase,-1)
						} else if(duelController.phase_current == DUEL_PHASE_BATTLE){
							menu_add_option("Dialog","Move to the Return Phase.",duelController_continue_nextphase,-1)
						}
					}
				}
				if(duelController.phase_current == DUEL_PHASE_MAIN ||
					duelController.phase_current == DUEL_PHASE_RETURN){
					menu_add_option("End Turn","Complete your turn.",duelController_continue_nextturn,-1)
					menu_height+=40
				}
			}
			
			menu_add_option("Forfeit","Forfeit the game.",duel_confirm_forfeit,-1);
			open_sound = sfx_sound_duel_menu
			//event_user(0)
			menu_push(id)
			tts_clear();
			tts_say("Continue turn?")
		}
