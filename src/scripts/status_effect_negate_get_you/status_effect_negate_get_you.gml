// if starts with you, you will check if the current user is a you card.

var target = argument[0];
var this = argument[1];
if((string_pos("enemy",this) > 0 && duelController.active_player == DUEL_PLAYER_ENEMY)
	|| (string_pos("you",this) > 0 && duelController.active_player == DUEL_PLAYER_YOU)){
	
	if(target == "you"){
		// if target is you, always return true.
		return true
	} else {
		return status_effect_negate_get_range(target,this);	
	}
}

return false;