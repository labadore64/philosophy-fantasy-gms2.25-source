var stringer = argument[0];

with(duelController){
	var poss = string_pos(":",stringer);

	temp_status_id = stringer;

	temp_status_name = string_copy(stringer,1,poss-1);

	stringer = string_copy(stringer,poss+1,string_length(stringer)-poss);
	var poss = string_pos(":",stringer);
	
	var adder = 0;
	
	//if the next character is \", find the end and copy the whole string
	if(string_char_at(stringer,1) == "\""){
		var stringea = string_length(stringer);
		for(var i = poss+1; i < stringea; i++){
			if(string_char_at(stringer,i) == "\""){
				poss = i;
				adder +=1;
				break;
			}
		}
	}

	temp_status_arg = string_copy(stringer,1,poss )

	stringer = string_copy(stringer,poss+1+adder,string_length(stringer)-poss);

	var stringe = stringer
	var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
	stringe = string_delete(stringe,1, string_pos(":",stringe));
	var side = string_copy(stringe,1, string_pos(":",stringe)-1);
	stringe = string_delete(stringe,1, string_pos(":",stringe));

	var target_side = DUEL_PLAYER_YOU
	if(side != "pos_you"){
		target_side = DUEL_PLAYER_ENEMY
	} else {
		target_side = DUEL_PLAYER_YOU
	}


	temp_status_card_loc = code_get_location(ori,target_side);
	temp_status_card_loc_index = real(stringe);
}