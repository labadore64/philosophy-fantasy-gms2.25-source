if(obj_data.last_game_state = 1){
	textboxShow(duelist_say2,duelist_name)
} else {
	textboxShow(duelist_say3,duelist_name)
}
textbox.name = duelist_name;
textbox.portrait_sprite = duelist_sprite
running_object = textbox;

textbox.newalarm[2] = TIMER_INACTIVE
textbox.newalarm[3] = TIMER_INACTIVE

with(textbox){
	event_perform(ev_alarm,2)
	event_perform(ev_alarm,3)
	with(obj_dialogue){
		newalarm[0] = TIMER_INACTIVE
		event_perform(ev_alarm,0)
	}
}