	var menupos_old = menupos

	menupos-=4;

	if(menupos < 0){
		if(wrap){
			menupos = menu_count-1;	
		} else {
			menupos = 0;	
		}
	}
	randomise();
	menuParent_sound()

	RUMBLE_MOVE_MENU

	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}
	menuParent_default_accessString()
	menuParent_ttsread();

	with(MouseHandler){
		ignore_menu = true
		menu_hover = true
	}
	if(global.menu_animate){
		alpha_adder = 0
	}

	surface_update = true

	// do option top stuff

	if(menupos_old != menupos){
		if(selected_draw_index > 0){
			selected_draw_index--;	
		} else {
			option_top--
			if(option_top < 0){
				option_top = 0	
			}
		}
	}

appearanceSet_Scrollupdate();