#macro RUMBLE_MOVE_MENU with(Rumble){instance_destroy();}gamepad_rumble(.25,7);

#macro RUMBLE_CARD_SELECT with(Rumble){instance_destroy();}gamepad_rumble(.5,20);with(Rumble){rumble_do_change=false;rumble_left=clamp(.5-(duelController.card_rumbleset_x-duelController.centerx)/200,0,1);rumble_right=clamp(.5+(duelController.card_rumbleset_x-duelController.centerx)/200,0,1);}

#macro RUMBLE_CARD_REVEAL with(Rumble){instance_destroy();}gamepad_rumble(.05,30);with(Rumble){rumble_change=-.075;rumble_do_change=false;rumble_max=.7;}

#macro RUMBLE_CARD_REVEAL_PACK with(Rumble){instance_destroy();}gamepad_rumble(.05,10);with(Rumble){rumble_change=-.15;rumble_do_change=false;rumble_max=.7;}


#macro RUMBLE_MOVE_ATTACK with(Rumble){instance_destroy();}gamepad_rumble(.7,45);
#macro RUMBLE_MOVE_ATTACK_WEAK with(Rumble){instance_destroy();}gamepad_rumble(.5,45);
#macro RUMBLE_MOVE_ATTACK_STRONG with(Rumble){instance_destroy();}gamepad_rumble(1,45);