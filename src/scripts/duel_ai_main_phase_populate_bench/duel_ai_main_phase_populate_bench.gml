// checks bench to see if there's a high attack dude, will bench.

with(duelController){
	if(field[1].hand.card_array_size > 0 && field[1].bench.card_array_size < 1){
		var current_attack = 0
		var highest_index = -1;
		var highest_val = current_attack
		for(var i = 0; i < field[1].hand.card_array_size; i++){
			if(obj_data.card_defense[field[1].hand.card_array[i]] > highest_val &&
				duel_can_bench(field[1].hand.card_array[i],field[1].active_monster.card_array[0])){
				highest_index = i;
				highest_val = obj_data.card_defense[field[1].hand.card_array[i]] 
			}
		}
		
		if(highest_index > -1){
			duel_do_bench(field[1].hand,highest_index);
		}
	}
}