	var benchy = argument[0];
	var acti = argument[1]
	
	var bench_card = argument[2]
	var menuposs = argument[3]
	
	var mon = bench_card 
	if(acti.card_array_size == 3){
		duel_animation_card_effect(acti.card_array[2],"overlay_cost",code_set_cardstate(acti,2));
		duel_animation_move_card(2,acti,benchy.parent.wait)
	}
	if(acti.card_array_size >= 2){
		duel_animation_card_effect(acti.card_array[1],"overlay_cost",code_set_cardstate(acti,1));
		duel_animation_move_card(1,acti,benchy.parent.wait)
	}
	
	// do the actual swap
	duel_animation_swap_cards(0,menuposs,acti,benchy)
	var mon = bench_card 
	duel_animation_goto_part(duelController.field[duelController.active_player],DUEL_FIELD_ACTIVE)
	
	duel_battle_showcard(mon);
	
	benchy.parent.switch_counter++