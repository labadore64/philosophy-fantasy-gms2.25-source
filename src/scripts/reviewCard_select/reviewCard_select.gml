draw_the_menu = false
active = false

if(selected_card.card_id > -1){
	var textbox_string = "review_invalid"

	var number = review_counter[selected_card.card_id]
	
	if(number > global.max_knowledge){
		number = 0;
		review_counter[selected_card.card_id] = 0
		var textbox_string = "review_max"
	} else {

		var name_id = "review_ph_" + obj_data.card_sprite_name[selected_card.card_id] + string(number);

		var filename_id = working_directory + "resources\\text\\"+name_id+".ini";

		if(file_exists(filename_id)){
	
			textbox_string = name_id;
			review_counter[selected_card.card_id]++;

		} else {
			// try again at 0
		
			review_counter[selected_card.card_id] = 0;
			var number = review_counter[selected_card.card_id]

			var name_id = "review_ph_" + obj_data.card_sprite_name[selected_card.card_id] + string(number);

			var filename_id = working_directory + "resources\\text\\"+name_id+".ini";
			if(file_exists(filename_id)){
	
				textbox_string = name_id;
				review_counter[selected_card.card_id]++;
			}
		}
	}
}

textbox = textboxShow(textbox_string,"Roger")
textbox.portrait_sprite = spr_portrait_roger_cutscene
textbox.portrait_x-=175
textboxz = true;
fade_surface_updated = true