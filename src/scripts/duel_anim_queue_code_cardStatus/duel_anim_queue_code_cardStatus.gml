var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _status = map[? "status"];
	var _arg = map[? "arg"];
	var _target = map[? "target"];
	var _quantity = map[? "quantity"];
	ds_map_destroy(map)
	
	if(!is_undefined(_status) &&
		!is_undefined(_target) &&
		!is_undefined(_arg)){
			
		var targe = _target;
		var this = code_get_this();
		var this_enemy = false;
		if(string_pos("pos_enemy",this) > 0){
			this_enemy = true;
		}
		
		var target_side = DUEL_PLAYER_YOU;
		var target_index = 0;
		var target_origin = noone;
		
		
		if(targe != ""){
		

			// parse target
			// if target is this just use the character stored in this

			var is_this = false;
			if(targe == "this"){
				targe = this
				is_this = true;
			}

			var stringe = targe;
			var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			var side = string_copy(stringe,1, string_pos(":",stringe)-1);
			stringe = string_delete(stringe,1, string_pos(":",stringe));
			
			//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		
			if(is_this){
				if(this_enemy){
					target_side = DUEL_PLAYER_ENEMY	
				} else {
					target_side = DUEL_PLAYER_YOU	
				}
			} else {
				if(side == "enemy"){
					if(!this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}
				} else {
					if(this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}	
				}
			}
			
			// if the code is a location, parse as location.
			if(code_is_location(ori)){
				target_index = string_copy(stringe,string_pos(":",stringe)+1,
											string_length(stringe) - string_pos(":",stringe))
			
				// string == variable
				if(string_length(string_digits(target_index)) != string_length(target_index)){
					var value = duelController.code_values[? target_index];
					if(is_undefined(value) || !is_real(value)){
						value = 0;	
					}
				
					target_index = value;
				} else {
					target_index = real(target_index);	
				}
				
				if(ori == "overlay"){
					target_index++;	
				}
				
				target_origin = code_get_location(ori,target_side)
			}
		}
		
		duel_card_status_set(getStatusString(_status,_arg,target_origin,target_index),_quantity)
		
		return noone;
	}


}

return noone;