if(menu_tabright != -1){
	if(global.inputEnabled){
		if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_TABRIGHT]) ||
			global.gamepad_state_array[KEYBOARD_KEY_TABRIGHT] == KEY_STATE_PRESS){
			script_execute(menu_tabright);
			keypress_this_frame = true;
		}
	}
}  