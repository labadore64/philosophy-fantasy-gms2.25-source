

// sort trunk
var pri = ds_priority_create();
for(var i = 0; i < trunk_total; i++){
	ds_priority_add(pri,trunk_display[i],deck_sort_cardtype(trunk_display[i]));
}

var list = deck_sort_prioritylist(pri)

for(var i = 0; i < trunk_total; i++){
	trunk_cards[i] = list[|i];
}

ds_priority_destroy(pri);

ds_list_destroy(list)

// sort deck
// obj_data.current_deck
var lister = obj_data.current_deck;


var pri = ds_priority_create();
var decksize = ds_list_size(lister)
for(var i = 0; i < decksize; i++){
	var dd = lister[|i]
	ds_priority_add(pri,dd,deck_sort_cardtype(dd));
}

var list = deck_sort_prioritylist(pri)

ds_list_clear(lister)

for(var i = 0; i < decksize; i++){
	ds_list_add(lister,list[|i]);
}

ds_priority_destroy(pri);

ds_list_destroy(list)