option_number_count = array_length_1d(option_number)

draw_option_bg[0] = false;
draw_title_bg[0] = false;

option_text[0,0] = "Off"
option_text[0,1] = "On"
for(var i = 0; i < 11; i++){
	option_text[1,i] = "Lv. " + string(i);
}
option_text[2,0] = "More..."
option_text[3,0] = "Left"
option_text[3,1] = "Right"
//just for difficulty
for(var i = 0; i < 6; i++){
	option_text[4,i] = "Lv. " + string(i);
}
//just for game speed
for(var i = 0; i < 6; i++){
	option_text[5,i] = string(i+1) +"0" + " FPS";
}
// for replays
option_text[6,0] = "Never"
option_text[6,1] = "Ask"
option_text[6,2] = "Always"

// for tooltips
option_text[7,0] = "None"
option_text[7,1] = "Tooltip"
option_text[7,2] = "Full UI"

// the select values
for(var i = 0; i < menu_count; i++){
	option_select[i] = 0;
	var val = options_map[? menu_name[i]];
	if(!is_undefined(val)){
		option_select[i] = val;	
	}
}

// draw the option bg?
for(var i = 0; i < menu_count; i++){
	draw_option_bg[i] = true;
	draw_title_bg[i] = false;
	
	if(menu_name[i] == ""){
		draw_option_bg[i] = false
	} else {
		
		for(var j = 0;j < option_number_count; j++){
			if(i == option_number[j]){
				draw_option_bg[i] = false;
				draw_title_bg[i] = true;
				break;
			}
		}
	}
}

options_update();

selected_draw_index=0
info_x_offset = 20;
info_y_offset = 495

option_spacer = 45;
option_x_offset = 20;
option_y_offset = 120
per_line = 10
option_top = 0;
wrap = false;

menu_select = optionsSelect
menu_up = optionsMoveUp
menu_down = optionsMoveDown
menu_left = optionsMoveLeft
menu_right = optionsMoveRight
menu_tabright = options_left
menu_tableft = options_right

menu_help="options"

clear_key_def()
add_key_def("Up","Move up","up")
add_key_def("Down","Move down","down")
add_key_def("Page Up","","left")
add_key_def("Page Down","","right")
add_key_def("Option+","","lefttab")
add_key_def("Option-","","righttab")
add_key_def("Select","","select")
add_key_def("Cancel","Go back","cancel")

draw_width = 760
mousepos_x = 400-42

scrollcounter = 0;
scroll_increment_draw = 0

ax_title = "Options"

menu_push(id)

scrolling = false;
