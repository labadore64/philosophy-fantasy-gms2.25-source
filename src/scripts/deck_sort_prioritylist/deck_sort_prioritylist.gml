var list_of_lists = ds_list_create();
var sizer = 0;

var pri = argument[0];
dir = false;
if(argument_count > 1){
	dir = argument[1];
}
var obj = noone;
var val = 0;
var last_val = -31231222
while(!ds_priority_empty(pri)){
	if(dir){
		val = ds_priority_find_priority(pri,ds_priority_find_max(pri))
	} else {
		val = ds_priority_find_priority(pri,ds_priority_find_min(pri))
	}
	
	if(!is_undefined(obj)){
		if(val != last_val){
			ds_list_add(list_of_lists,ds_list_create());
		}
		last_val = val;
		if(dir){
			obj = ds_priority_delete_max(pri)
		} else {
			obj = ds_priority_delete_min(pri)
		}
		
		
		sizer = ds_list_size(list_of_lists)-1;
		var list = list_of_lists[|sizer];
		if(!is_undefined(list)){
			ds_list_add(list,obj);	
		}
	} else {
		break;	
	}
}

// alphabetize all added sublists
var sizer = ds_list_size(list_of_lists);
for(var i = 0; i < sizer; i++){
	var val = list_of_lists[|i];
	if(!is_undefined(val)){
		if(ds_exists(val,ds_type_list)){
			if(ds_list_size(val) > 0){
				deck_list_alpha(val);	
			}
		}
	}
}

//return list
var returner = ds_list_create();

// destroys all added sublists
var sizer = ds_list_size(list_of_lists);
for(var i = 0; i < sizer; i++){
	var val = list_of_lists[|i];
	if(!is_undefined(val)){
		if(ds_exists(val,ds_type_list)){
			var lesize = ds_list_size(val);
			for(var j = 0; j < lesize; j++){
				ds_list_add(returner,val[|j]);	
			}
			ds_list_destroy(val);	
		}
	}
}
ds_list_destroy(list_of_lists);

return returner;