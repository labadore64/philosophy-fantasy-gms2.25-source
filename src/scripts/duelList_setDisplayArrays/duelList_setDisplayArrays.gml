if(list_reference > -1){

	trunk_display[0] = -1
	trunk_total = 0

	var counter = 0;

	var sizer = ds_list_size(list_reference);

	for(var i = 0; i < sizer; i++){
		if(list_reference[|i ] > -1){
			trunk_display[counter] = list_reference[|i ]
			counter++;
		}
	}

	trunk_total = counter;

	menu_count = trunk_total;
	
	if(menupos >= menu_count){
		menupos = 0	
	}
	
	for(var i = 0; i < per_line; i++){
		if(option_top + i < trunk_total){
			draw_id[i] = trunk_display[option_top+i] 
		} else {
			draw_id[i] = -1;	
		}
	}
	selected_card.card_id = trunk_display[menupos]
	viewcard_id = selected_card.card_id;

	menu_surf_update = true;
	selected_card.name_display = obj_data.card_name[trunk_display[menupos]]
	selected_card.surface_update = true;
	selected_card.image_blend = obj_data.type_color[obj_data.card_element[selected_card.card_id]];
	with(selected_card){
		card_boost_attack = 0;
		card_boost_defense = 0
		card_update()
		card_reset_size();	
	}
	
	ax_card(selected_card.card_id)

	archetype_string = obj_data.card_archetype1[selected_card.card_id]

	if(string_lower(obj_data.card_archetype2[selected_card.card_id]) == "none"){
		access_string[6] = obj_data.card_archetype1[selected_card.card_id] + " archetype"
	} else {
		archetype_string = obj_data.card_archetype1[selected_card.card_id] + "/" + obj_data.card_archetype2[selected_card.card_id]
		access_string[6] = obj_data.card_archetype1[selected_card.card_id] + " " + obj_data.card_archetype2[selected_card.card_id] + " archetypes"
	}
	access_string[7] = string(trunk_total) + " total cards"	

	loaded_list = true;
	menu_count = 0;
	for(var i = 0; i < trunk_total; i++){
		menu_add_option(obj_data.card_name[trunk_display[i]],"",-1,-1)	
	}
	// scroll display
	scroll_increment = (485-20)/menu_count //increment size

	// event user
	event_user(0)
}