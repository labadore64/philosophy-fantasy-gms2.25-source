
var obj = instance_create(argument[0],argument[1],Card)
obj.card_id = argument[3];
if(argument[3] > -1){
	obj.image_blend = obj_data.type_color[obj_data.card_element[argument[3]]];
	obj.name_display = string_copy(obj_data.card_name[argument[3]],1,17);
}
obj.card_scale = argument[2]

with(obj){
	card_update();	
}
return obj;


return noone;