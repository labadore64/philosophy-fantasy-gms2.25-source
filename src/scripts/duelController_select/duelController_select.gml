ax_string_clear();

with(duelSub){
	instance_destroy()	
}

if(!resume_control){
	if(running_obj < 0  || code_select_active){
		if(active_player == DUEL_PLAYER_YOU){
			if(!duel_camera_moving() || duelController.code_select_active){
				if(!instance_exists(animationPhaseTransition)){
					if(duelController.mouse_mode){
						with(duelController){
							do_context_menu = true
							context_menu_x = (MouseHandler.mousepos_x/global.scale_factor) -global.display_x
							context_menu_y = ((MouseHandler.mousepos_y)/global.scale_factor) - global.display_y
							context_menu_select = mouse_card_menupos[mouse_card_selected]
							context_menu_obj = mouse_card_element[mouse_card_selected]
						}
					}	
					
					with(selected_object){
						if(menu_select != -1){
							script_execute(menu_select);	
						}
			
					}
					duelController.do_context_menu = false;
					duelController.click_countdown = 2
				}
			}
		}
	}
}