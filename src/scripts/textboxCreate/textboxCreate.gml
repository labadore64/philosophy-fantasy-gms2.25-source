/// @description Insert description here
// You can write your code in this editor


RUMBLE_MOVE_MENU
event_id = "";
destroyed = false;

talking_sound = true;

menu_draw = textboxDraw;

text = ds_list_create();
add_beforetext = ds_list_create(); //text to be added at the beginning of the text.
add_aftertext = ds_list_create(); //text to be added at the end of the text. Such as for picking up items.

//ds_list_add(text,"Hello, this is a test line! I hope you don't mind. I'm going to write a lot to make sure it spaces properly!");

line = 0; //which line into the dialog is displayed.
draw_pos = 0; //what character to draw on current line.
draw_speed = 1; //how much to add to the draw pos each game cycle.
if(global.text_full && room != BattleRoom){
	char_per_line = 20
} else {
	char_per_line = 40	
}

current_text = ""//

character = -1;
name = "";
is_monster = false;

pitch_base = 1;

text_sound1 = textbox_sound_default;
text_sound2 = textbox_sound_default//speech_mon03;
text_sound3 = textbox_sound_default//speech_mon04;
text_sound4 = textbox_sound_default//speech_mon05;

text_mod_sound = false;

newalarm[0] = 1;
newalarm[1] = TIMER_INACTIVE 
active = false;

menu_select = textboxEnter;
menu_cancel = textboxCancel;

textbox_hold_counter = 0;
textbox_hold_max = global.text_auto_speed*4;

menu_select_hold = textboxEnterHold;

display_object = -1;

//scripts for special things
scripts = ds_map_create();
arguments = ds_map_create(); //holds arguments for all scripts. MUST BE UNIQUE

triggered_by_player = false; //if triggered by player, will release after destroyed.
unlock_player = false;

//following is for loading text from file
filename_id = working_directory + "resources\\text\\";
text_id = "test";

tts_speak = true; //whether to disable tts

end_script = -1; //what script to execute when textbox is killed

draw_portrait = false; //whether or not to draw portrait
portrait = -1;

delay_display = 0

x_align = 30;

lock = false;

wait = true;

parent = noone;

text_section = "data"
text_name = "text"

portrait_surf = -1;
portrait_sprite = -1;
portrait_update = false;

portrait_x = 120
portrait_y = 0

if(instance_exists(duelController)){
	draw_bg = false;	
}