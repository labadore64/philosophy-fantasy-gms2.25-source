if(global.tooltip){
	with(argument[0]){
		var col = global.text_fgcolor
		
		draw_set_alpha(1)
		if(global.high_contrast){
			col = c_white;
			draw_set_color(c_white)
			draw_rectangle_ratio(x-3,y-3,x+width+3,y+height+3,false)	
			draw_set_color(c_black)
		} else {
			draw_set_color(global.menu_bgcolor)
			draw_set_alpha(.85)
			draw_rectangle_ratio(x-3,y-3,x+width+3,y+height+3,false)	
			draw_set_alpha(1)
		}
		
		draw_rectangle_ratio(x,y,x+width,y+height,false)	
		
		draw_set_alpha(1)
		
		draw_set_color(col)
		draw_text_transformed_ratio(x+10, y+10, text,2,2,0)
	}
	
}