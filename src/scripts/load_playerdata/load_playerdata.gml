var data = global.playerdata;

if(is_string(data)){
	var map = json_decode(data);
	if(validate_playerdata(map)){
		return map;	
	} else {
		ds_map_destroy(map);	
	}
}

return -1;