with(duelController){
	// the current card
	//ds_map_replace(code_values,"this",argument[0]);	
	//ds_map_replace(code_values,"card_id",argument[0]);
	
	// duel state. these are read only.
	// if you want to modify the actual duel state,
	// you have to use special functions.
	if(ds_stack_size(code_this) <= 1){
		ds_map_replace(code_values,"turns",turns);
		ds_map_replace(code_values,"player",active_player);
		ds_map_replace(code_values,"selection",""); //selection from a list or the field
		ds_map_replace(code_values,"return",""); //selection from a list or the field
		// registers
		for(var i = 0; i < 8; i++){
			ds_map_replace(code_values,"r"+string(i),"");	
		}
	
	}
}