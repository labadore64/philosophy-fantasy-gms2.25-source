// argument0 = selected card
// argument1 = target card

var card1 = true;
var card2 = true;
var card3 = true;

if(argument[0] > -1){
	if(card_has_keyword(argument[0],"timid")){
		card1 = false;	
	}
}

if(argument[0] > -1){
	if(card_has_keyword(argument[0],"sacrifice") &&
		duelController.field[duelController.active_player].active_monster.card_array_size < 1){
		card3 = false;	
	}
}


if(argument[1] > -1){
	if(card_has_keyword(argument[1],"block")){
		card2 = false;	
	}
}


	
return card1 && card2 && card3;