var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _val = map[? "val"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_val)){
		
		var rand = 0;
	
		with(duelController){
			if(code_argument_size > 0){
				var val = _val;
				if(string_length(val) == string_length(string_digits(val))){
					rand = real(val);	
				}  else {
					// get number from variables
					if(ds_map_exists(duelController.code_values,val)){
						var att = duelController.code_values[? val]
						if(!is_undefined(att)){
								rand = real(att);	
						}
					}
				}
			}
		}	
		
		ds_map_replace(duelController.code_values,"return",coolrandom_inext(rand))
	}

}

return noone;