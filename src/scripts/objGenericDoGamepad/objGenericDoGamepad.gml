if(gamepad_is_connected(global.gamepad)){
		var axis_h = gp_axislh;
		var axis_v = gp_axislv;
	
		if(!global.gamepadRightAxis){
			axis_h = gp_axisrh;
			axis_v = gp_axisrv
		}
		
		var trigger_press = false;

		axish = gamepad_axis_value(global.gamepad,axis_h)
		axisv = gamepad_axis_value(global.gamepad,axis_v)
		axis_angle = point_direction(0,0,axish,axisv)
		axis_amount = point_distance(0,0,axish,axisv);
		if(axis_start){
			if(axis_angle != axis_angle_last || 
				axis_amount != axis_amount_last){
				axis_changed = true;
				
				if(axis_amount_last < axis_min &&
					axis_amount > axis_min){
						trigger_press = true;	
					}
			} else {
				axis_changed = false;	
			}
		} else {
			axis_start = true;	
		}
		
		if(axis_amount < axis_min){
			pressed = false		
		}
		
	if(abs(axisv) > abs(axish)){
		//up_down axis 	
		if(sign(axisv) == 1){
			axis_direction = AXIS_DOWN	
		} else {
			axis_direction = AXIS_UP
		}
	} else {
		//left_right axis
		if(sign(axish) == 1){
			axis_direction = AXIS_RIGHT
		} else {
			axis_direction = AXIS_LEFT
		}
	}
	if(!pressed){
		if(delay_gamepad < 0){
			if(trigger_press){
				if(axis_direction == AXIS_LEFT){
					axis_left_pressed = true;	
					global.key_state_array[KEYBOARD_KEY_LEFT] = KEY_STATE_PRESS;
				} else if (axis_direction == AXIS_RIGHT){
					axis_right_pressed = true;
					global.key_state_array[KEYBOARD_KEY_RIGHT] = KEY_STATE_PRESS;
				} else if (axis_direction == AXIS_UP){
					axis_up_pressed = true;	
					global.key_state_array[KEYBOARD_KEY_UP] = KEY_STATE_PRESS;
				} else if (axis_direction == AXIS_DOWN){
					axis_down_pressed  = true;
					global.key_state_array[KEYBOARD_KEY_DOWN] = KEY_STATE_PRESS;
				}
				pressed = true;
				delay_gamepad = 1
			}
		}
	}
		
}