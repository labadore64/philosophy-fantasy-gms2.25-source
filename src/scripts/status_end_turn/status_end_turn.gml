
with(duelController){
	var size = ds_map_size(card_status)
	var key = ds_map_find_first(card_status)
	for (var i = 0; i < size; i++)
	{
		if(!is_undefined(key)){

			if(string_pos("decay",key) > 0){
				decrementStatusString(key);
			}

			key = ds_map_find_next(card_status, key);
		}
	}
}

return "";