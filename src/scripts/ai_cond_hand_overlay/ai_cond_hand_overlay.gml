var me = argument[0];

var pos = me.menupos;

if(argument_count > 1){
	pos = argument[1];	
}

var ai_val = -1

if(argument_count > 2){
	ai_val = argument[2]
}


var menu_selected = me.card_array[pos]

if(ai_val > -1){
	menu_selected = me.ai_card_array[ai_val,pos]
}

if(!duelController.code_select_active){
	if(duelController.phase_current == DUEL_PHASE_MAIN){
		if(obj_data.card_type[me.card_array[me.menupos]] == CARD_TYPE_PHILOSOPHER){
			if(me.overlay_counter == 0 // you haven't done your normal overlay
				&& me.parent.active_monster.card_array_size != 0 // there is at least one active philosopher
				&& duel_can_overlay(menu_selected,me.parent.active_monster.card_array[0])
				// there is room within the limit 
				){
				return true;
			}
		}

	}
}

return false;