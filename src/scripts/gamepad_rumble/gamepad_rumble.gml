
	var rumb_amount = clamp(argument[0],0,1);
	var rumb_time = argument[1];

	with(Rumble){
		instance_destroy();	
	}
	if(global.gamepadVibration){
		var obj = instance_create(0,0,Rumble);

		obj.rumble_amount = rumb_amount;
		obj.rumble_time = rumb_time;
	}