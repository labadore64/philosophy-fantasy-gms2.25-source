var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _card = map[? "card"];

	ds_map_destroy(map)

	if(!is_undefined(_card)){

		if(_card > -1){
			
			duel_animation_wait(20);	
			duel_battle_showcard(_card,sfx_sound_duel_select_card);
					
			// wait before continuing
			duel_animation_wait(20);
		}
	}

}

return noone;