var query = argument[1];
var card = argument[0]

var pos = string_pos("=",query)
var nott = false;
// if string is not
if(string_char_at(query,pos-1) == "!"){
	query = string_delete(query,pos-1,1);
	pos--;
	nott = true;
}
var len = string_length(query);

// this means the value represents a query about the card
if(pos > 0){
	var part1 = string_copy(query,1,pos-1)
	var part2 = string_copy(query,pos+1,len-pos)

	// test element
	if(part1 == "element"){
		
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					part2 = obj_data.card_element[test];
				}
			}
		} else {
		
		if(string_length(string_digits(part2)) == string_length(part2)){
			part2 = real(part2)	
		} else {
				if(part2 == "mind"){
					part2 = 0;	
				} else if (part2 == "spirit"){
					part2 = 1	
				} else if (part2 == "matter"){
					part2 = 2;	
				} else {
					part2 = duelController.code_values[? part2];
					if(is_undefined(part2)){
						part2 = 0;	
					}
				}
			}
		}
		
		if(nott){
			if(obj_data.card_element[card] == part2){
				return 0;	
			} else {
				return 1;	
			}
		} else {
			if(obj_data.card_element[card] == part2){
				return 1;	
			} else {
				return 0;	
			}
		}
	}
	
	// test card type
	if(part1 == "type"){
		
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					part2 = obj_data.card_type[test];
				}
			}
		} else {
			if(string_length(string_digits(part2)) == string_length(part2)){
				part2 = real(part2)	
			} else {
		
				if(part2 == "philosopher"){
					part2 = CARD_TYPE_PHILOSOPHER
				} else if (part2 == "theory"){
					part2 = CARD_TYPE_METAPHYS
				} else if (part2 == "praxis"){
					part2 = CARD_TYPE_EFFECT
				} else {
					part2 = duelController.code_values[? part2];
					if(is_undefined(part2)){
						part2 = 0;	
					}
				}
			}
		}
		
		if(nott){
			if(obj_data.card_type[card] == part2){
				return 0;	
			} else {
				return 1;	
			}	
		} else {
			if(obj_data.card_type[card] == part2){
				return 1;	
			} else {
				return 0;	
			}
		}
	// test archetype
	} 
	else if (part1 == "arch"){
		part2 = string_lower(part2);
		
		if(nott){
			if(part2 == string_lower(obj_data.card_archetype1[card]) ||
				part2 == string_lower(obj_data.card_archetype2[card])){
					return 0;	
			}
			
			return 1;
		} else {
			if(part2 == string_lower(obj_data.card_archetype1[card]) ||
				part2 == string_lower(obj_data.card_archetype2[card])){
					return 1;	
			}
			
			return 0;
		}
	} 
	// test name
	else if (part1 == "name"){
		
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					part2 = string_lower(obj_data.card_name[test]);
				}
			}
		} else {
			part2 = string_replace_all(string_lower(part2),"\"","");
		}
		
		if(nott){
			if(part2 == string_lower(obj_data.card_name[card])){
					return 0;	
			}
			
			return 1;
		} else {
			if(part2 == string_lower(obj_data.card_name[card])){
					return 1;	
			}
			
			return 0;
		}
	}
	
	else if (part1 == "att"){
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					return obj_data.card_attack[test];
				}
			}
		}	
	}else if (part1 == "def"){
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					return obj_data.card_defense[test];
				}
			}
		}	
	}else if (part1 == "att_overlay"){
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					return obj_data.card_attack_overlay[test];
				}
			}
		}	
	}else if (part1 == "def_overlay"){
		// check if part2 is a variable
		// if it is, check it directly.
		
		var test = duelController.code_values[? part2];
		if(!is_undefined(test)){
			part2 = -1;
			
			if(is_real(test)){
				if(test > -1){
					return obj_data.card_defense_overlay[test];
				}
			}
		}	
	}
} else {
	if(query == "att"){
		return obj_data.card_attack[card]	
	} else if (query == "def"){
		return obj_data.card_defense[card]	
	} else if (query == "att_overlay"){
		return obj_data.card_attack_overlay[card]	
	} else if (query == "def_overlay"){
		return obj_data.card_defense_overlay[card]	
	}
}

return real(query)