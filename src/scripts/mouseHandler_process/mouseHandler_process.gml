/// @description Insert description here
// You can write your code in this editor
var clickedz = false;

if(global.drawing_enabled && global.mouse_active){
	with(MouseHandler){
		if(clicked_countdown > -1){
			if(clicked_countdown == 0){
				clicked = false;
			}
			clicked_countdown--
		}
	
		menu_hover = false;
		var hover = false;
	
		if(clicked_countdown <= -1){
			if(active && enabled){

				mouse_xprevious = mousepos_x
				mouse_yprevious = mousepos_y
				
				if(mouse_xprevious != mousepos_x ||
					mouse_yprevious != mousepos_y){
					global.le_mouse = true;	
				}
		
				clicked_this_frame = false

				var posi = mouse_active_position 
				if(ScaleManager.updated){
					mouseHandler_update_values();
				}

				var mousex = window_mouse_get_x();
				var mousey = window_mouse_get_y()
		
				mousepos_x = mousex;
				mousepos_y = mousey

				selected = -1;	

				if(clicked_countdown <= -1){
					for(var i = 0; i < mouse_count; i++){
						mouse_hovered[i] = false;
						if(instance_exists(mouse_obj[i])){
							if(mousex >= mouse_points_trans[i,0] && 
								mousex <= mouse_points_trans[i,2] &&
								mousey >= mouse_points_trans[i,1] && 
								mousey <= mouse_points_trans[i,3]){
								mouse_hovered[i] = true;
								var script = mouse_script[i];
								with(mouse_obj[i]){
									if(script == menu_cancel &&
										cancel_button_enabled){
											cancel_hover = 1.1;
									} else if (script == help_menu_do &&
										help_button_enabled){
											help_hover = 1.1;			
									}
								}
								
								if(mouse_check_button_pressed(mb_left)){
									global.le_mouse = true;

									if(mouse_script[i] != -1){
										var script = mouse_script[i];
										clicked_this_frame = true
										var playsound = mouse_menupos[i];
										with(mouse_obj[i]){
											if(script == menu_cancel &&
												cancel_button_enabled){
												cancel_pressed = true;
											} else if (script == help_menu_do &&
												help_button_enabled){
												help_pressed = true	
											} else {
												script_execute(script)	
											}
											
											if(playsound ){
												menuParent_selectSound();
											}
										}
										clickedz = true
										break;
									}
								}
						
								if(mouse_menupos[i]){
									hover=true;
									if(posi != i){
										with(mouse_obj[i]){
											if(!keypress_this_frame){
												menupos = i;
												if(menupos > menu_count-1){
													menupos = menu_count-1;	
												}
												posi = menupos;

												if(menu_update_values > -1){
													script_execute(menu_update_values);	
												}
												alpha_adder = 0
												tts_say(MouseHandler.mouse_text[i])
											} else {
												posi = menupos;	
											}
										}
									}
								} else {
									if(!mouse_hovered_last[i]){
										tts_say(mouse_text[i])
									}	
								}

							}
						}
					}


				}
		
				if(mouse_xprevious != mousex || mouse_yprevious != mousey){
					ignore_menu = false
				}

				if(!ignore_menu && !clickedz){
					if(instance_exists(mouse_menu_obj)){
						var arrays = mouse_menu_script;
	
	
						with(mouse_menu_obj){
							posi = menupos
							if(active){
								if(!keypress_this_frame){
									var sizer = min(option_top + per_line, MouseHandler.mouse_menu_count);
									var counter = 0;
									for(var i = option_top; i < sizer; i++){
										if(mousex >= MouseHandler.mouse_menu_points_trans[i,0] && 
											mousex <= MouseHandler.mouse_menu_points_trans[i,2] &&
											mousey >= MouseHandler.mouse_menu_points_trans[i,1]-MouseHandler.mouse_menu_spacer*option_top &&
											mousey <= MouseHandler.mouse_menu_points_trans[i,3]-MouseHandler.mouse_menu_spacer*option_top){
											hover=true;
											if(mouse_check_button_pressed(mb_left)){
												global.le_mouse = true;

												menuParent_selectSound();
												if(arrays[i] != -1){
													script_execute(arrays[i])	
													clickedz = true
													break;
												}
											}
						
											if(posi != i){
												menupos = i;
												if(menupos > menu_count-1){
													menupos = menu_count-1;	
												}
												posi = menupos;
												with(MouseHandler){
													clicked_this_frame = true	
												}
												alpha_adder = .1
												if(menu_update_values > -1){
													script_execute(menu_update_values);	
												}
												tts_say(menu_name[menupos])
											}

										}
										counter++;
									}
								} else {
									// if key pressed, update menupos so it doesn't trigger tts	
									posi = menupos;
								}
							}
						}
					}
				}
	
				if(clickedz){
					clicked = true
					clicked_countdown = 5	
					enabled = false
				}
	
				mouse_active_position = posi
	
				for(var i = 0; i < mouse_count; i++){
					mouse_hovered_last[i] = mouse_hovered[i]
				}
				menu_hover = hover
			}
		}
	}
	
	if(!clickedz){
		if(	right_click_move ){
					
			var mx = (MouseHandler.mousepos_x - global.display_x)/global.scale_factor
			var my = (MouseHandler.mousepos_y - global.display_y)/global.scale_factor
					
			var start_move = false;
			var end_move = false;
			var move_it = false;
					
			if(right_clicked){
				if(!left_dragging){
					if(mouse_check_button(mb_right)){
						// move the shit
						move_it = true;
					
					} else if(mouse_check_button_released(mb_right)){
						// update the position
					
						// update the shit
						end_move = true
					}
				}
			} else {
				
				if(left_dragging){
				
					if(mouse_check_button(mb_left)){
						// move the shit
						move_it = true;
					
					} else if(mouse_check_button_released(mb_left)){
						// update the position
					
						// update the shit
						end_move = true
					}
				} else {
					if(mouse_check_button_pressed(mb_left)){
						// check that you're in range:
						if(mx > x-7 && mx < x+menu_width+7 &&
							my > y-7 && my < y+option_y_offset){
					
							left_dragging = true;
							start_move = true;
						}
					} else if(mouse_check_button_pressed(mb_right)){
						// check that you're in range:
						if(mx > x-7 && mx < x+menu_width+7 &&
							my > y-7 && my < y+menu_height+7){
					
							right_clicked = true;
							start_move = true;
						}
					}
				}
			}
			
			if(start_move){
				right_clicked_x = x
				right_clicked_y = y
				right_clicked_mx = mx
				right_clicked_my = my
				buttons_rendered = false;
					
				with(MouseHandler){
					mouse_count = 0
					mouse_menu_count = 0
				}
			}
			
			if(end_move){
				right_clicked = false
				left_dragging = false
				menuParent_menu_makeMouse();
				with(MouseHandler){
					clicked_countdown = 5	
				}
			}
			
			if(move_it){
				x = right_clicked_x + (mx - right_clicked_mx)
				y = right_clicked_y + (my - right_clicked_my)

				if(x < 5){
					x = 5;	
				}
				if(y < 5){
					y = 5;	
				}
				if(x > 800 - menu_width - 5){
					x = 800 - menu_width - 5	
				}
				if(y > 600 - menu_height - 5){
					y = 600 - menu_height - 5	
				}
					
				cancel_button_x = x + menu_width;
				cancel_button_y = y;
			}
			
		}
	}
	

}