// particles

Sname = part_system_create();

var scale = .05;

particle1 = part_type_create();
if(!global.enableShader){
	part_type_color1(particle1,$7F7F00);
} else {
	part_type_color1(particle1,16776960);	
}


quantity = 10
part_type_shape(particle1,pt_shape_pixel);
part_type_size(particle1,2,6,0,0);
part_type_scale(particle1,1,0.25);
part_type_alpha3(particle1,0,0.15,0);
part_type_gravity(particle1,0.05,180);
part_type_blend(particle1,1);
part_type_life(particle1,60,60);

emitter1 = part_emitter_create(Sname);
part_emitter_region(Sname,emitter1,
					global.display_x,
					global.window_width,
					global.display_y,
					global.window_height,
					ps_shape_rectangle,
					ps_distr_linear);

part_system_automatic_draw(Sname, false)
part_system_automatic_update(Sname,false)

part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
repeat(150){
	if(global.particles){part_system_update(Sname)}
}

shader_bg_color = c_black;
shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = irandom(65535)

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

bg_surf = -1;
bg_surf_updated = false;
