var cando = false;
with(deckBuild){
	if(!(trunk_total == 1 && obj_data.card_in_trunk[trunk_display[0]] == 1) && deck_total < 20){
		var carde = trunk_display[menupos]
		deckBuild_trunkToDeck(trunk_display[menupos])

		soundfxPlay(sfx_sound_deck_add)
		
		gamepad_rumble(.5,15)
		with(Rumble){
			rumble_left = 0
			rumble_right = 1
		}

		var counter = 0;
		var sizer = ds_list_size(obj_data.current_deck);
		for(var i = 0; i < sizer; i++){
			if(obj_data.current_deck[|i] == carde){
				counter++;	
			}
		}
		
		if(counter == 3){
			menupos++;
			option_top++
			if(option_top > menu_count-per_line){
				option_top = menu_count-per_line;
			}
			//calculate selected from option top
			var counter = 0;
			for(var i = option_top; i < per_line+option_top && i < menu_count; i++){
				if(i == menupos){
					selected_draw_index = counter;
					break;
				}
				counter++;
			}
		}
		/*
		if(obj_data.card_in_trunk[trunk_display[menupos]] == 0){
			menupos--;
			if(menupos < 0){
				menupos = 0	
			} else {
				if(selected_draw_index > 0){
					selected_draw_index--;	
				} else {
					option_top--
				}
			}
		}
		*/
		deckBuild_setDisplayArrays()
		
		soundfxPlay(sfx_sound_deck_add)
		deckBuild_updateSprite();
		if((trunk_total == 1 && obj_data.card_in_trunk[trunk_display[0]] == 1) || deck_total >= 20){
			cando = true
		}
	}
}
event_user(0)