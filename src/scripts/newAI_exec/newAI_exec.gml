#macro AI_END_PHASE "endphase"
#macro AI_END_TURN "endturn"
#macro AI_MAX_BATCH_TIME 15000

var max_depth = 2*global.max_ai+1;

// populate the first commands
// set the field

// if not resuming an old batch
// initialize everything
if(duelController.ai_resume_point == 0){
	ai_phase[0] = duelController.temp_phase_current
	newAI_copy_current_state();
	//newAI_copy_state(ai_counter);

	// get the first state of commands possible from the current gamestate.
	var command_init = newAI_get_commands();
	var command = ""
	var command_init_size = string_count(",",command_init);

	// split commands into ai command array
	for(var i = 0; i < command_init_size; i++){
		var pos = string_pos(",",command_init);
		ai_command[i] = string_copy(command_init,1,pos-1);
		ai_full_command[i] = ai_command[i];
		ai_hp[i] = ai_hp[0]
		ai_hp_enemy[i] = ai_hp_enemy[0]
		ai_phase[i] = duelController.temp_phase_current
		newAI_copy_state(i);
		command_init = string_copy(command_init,pos+1,string_length(command_init)-pos);
	}

	// add endphase and endturn
	next = newAI_find_next(0);

	if(ai_phase[0] < DUEL_PHASE_END){
		ai_command[next] = AI_END_PHASE
		ai_full_command[next] = ai_command[next];
		ai_hp[next] = ai_hp[0]
		ai_hp_enemy[next] = ai_hp_enemy[0]
		//ai_full_command[next] = ai_full_command[ai_counter] + "," + ai_command[next];
		ai_card_depth[next] = 0
		ai_phase[next] = ai_phase[0]
	}

	if(ai_phase[0] < DUEL_PHASE_END){
		next = newAI_find_next(next);
		ai_command[next] = AI_END_TURN
		ai_full_command[next] = ai_command[next];
		ai_hp[next] = ai_hp[0]
		ai_hp_enemy[next] = ai_hp_enemy[0]
		ai_phase[next] = ai_phase[0]
		//ai_full_command[next] = ai_full_command[ai_counter] + "," + ai_command[next];
		ai_card_depth[next] = 0

		newAI_copy_state(next);
	}
}

//command_init = string_copy(command_init,pos+1,string_length(command_init)-pos);

var counter = 0;
var updated = false;
var final_depth = 0;
ai_counter = duelController.ai_resume_point
ai_depth = ai_card_depth[ai_counter]
var curtime = get_timer() + AI_MAX_BATCH_TIME;

while(ai_depth <= max_depth){

	// test if the time limit passed
	if(curtime < get_timer()){
		duelController.ai_resume_point = ai_counter;
		show_debug_message("Taking a break! at: " + string(ai_counter));
		break;
	}

	if(ai_counter >= AI_MAX_PASS ||
		ai_counter < 0){
		// DO STUFF HERE
		
		duelController.ai_resume_complete = true
		break;
	}
	
	// if there is no phase for the current choice,
	// break it
	if(ai_phase[ai_counter] == -1){
		
		duelController.ai_resume_complete = true
		break;
	}

	// loaded command
	command = ai_command[ai_counter];
	
	// populate future commands
	
	// set current depth
	ai_depth = ai_card_depth[ai_counter]
	
	newAI_load_state(ai_counter);
	
	var field_obj = duelController.field[argument[0]].ai_field;
	var curplay = argument[0];
	
	// executes command
	// if not end turn, do processing
	if(command != AI_END_TURN){
		if(command == ""){
			command = AI_END_PHASE;
		}
		if(!ai_processed[ai_counter]){
			updated = true
			// execute command normally
			ai_processed[ai_counter] = true;
		
			// if the command is end phase, 
			// just progress the phase counter.
			if(command == AI_END_PHASE){
				duelController.phase_current++
				newAI_next_phase(field_obj,ai_counter,curplay);
			} else {
				newAI_execute_script(command);
				
				if(duelController.temp_turns == 0){
					newAI_calculate_battle(!curplay)
				}
				newAI_copy_state(ai_counter);
				
			}
				
			newAI_copy_state(ai_counter);
		
			ai_phase[ai_counter] = duelController.phase_current
			ai_turn[ai_counter] = duelController.turns
			// get the next row of commands and populate.
			command_init = newAI_get_commands();
			command_init_size = string_count(",",command_init);
	
			var next = newAI_find_next(ai_counter);
	
			// split commands into ai command array
			if(next > -1){
				for(var i = 0; i < command_init_size; i++){
			
					var pos = string_pos(",",command_init);
					ai_command[next] = string_copy(command_init,1,pos-1);
					if(ai_full_command[ai_counter] == ""){
						ai_full_command[next] = ai_command[next];
					} else {
						ai_full_command[next] = ai_full_command[ai_counter] + "," + ai_command[next];
					}
					ai_card_depth[next] = string_count(",",ai_full_command[next]);
					if(final_depth < ai_card_depth[next]){
						final_depth = ai_card_depth[next]	
					}
					ai_hp[next] = ai_hp[ai_counter]
					ai_hp_enemy[next] = ai_hp_enemy[ai_counter]
					ai_switch_counter[next] = ai_switch_counter[ai_counter]
					ai_overlay_counter[next] = ai_overlay_counter[ai_counter]
					ai_phase[next] = ai_phase[ai_counter]
					ai_weight[next] = ai_weight[ai_counter]
					newAI_copy_state(next);
					command_init = string_copy(command_init,pos+1,string_length(command_init)-pos);
			
					next = newAI_find_next(next);
					if(next == -1){
						final_depth--;
						break;	
					}
				}
			} else {
				break;	
			}
			// also add end phase and end turn branches!
			if(next > -1){
				if(ai_phase[ai_counter] < DUEL_PHASE_END){
					ai_command[next] = AI_END_PHASE
					if(ai_full_command[ai_counter] == ""){
						ai_full_command[next] = ai_command[next];
					} else {
						ai_full_command[next] = ai_full_command[ai_counter] + "," + ai_command[next];
					}
					ai_card_depth[next] = string_count(",",ai_full_command[next]);
					ai_hp[next] = ai_hp[ai_counter]
					ai_hp_enemy[next] = ai_hp_enemy[ai_counter]
					if(final_depth < ai_card_depth[next]){
						final_depth = ai_card_depth[next]	
					}
					ai_switch_counter[next] = ai_switch_counter[ai_counter]
					ai_overlay_counter[next] = ai_overlay_counter[ai_counter]
					ai_phase[next] = ai_phase[ai_counter]
					ai_weight[next] = ai_weight[ai_counter]

					newAI_copy_state(next);
					
					next = newAI_find_next(next);
				}
			}
				
			if(next > -1){
				if(ai_phase[ai_counter] < DUEL_PHASE_END){
					ai_command[next] = AI_END_TURN
					if(ai_full_command[ai_counter] == ""){
						ai_full_command[next] = ai_command[next];
					} else {
						ai_full_command[next] = ai_full_command[ai_counter] + "," + ai_command[next];
					}
					ai_card_depth[next] = final_depth
					ai_hp[next] = ai_hp[ai_counter]
					ai_hp_enemy[next] = ai_hp_enemy[ai_counter]
					if(final_depth < ai_card_depth[next]){
						final_depth = ai_card_depth[next]	
					}
					ai_switch_counter[next] = ai_switch_counter[ai_counter]
					ai_overlay_counter[next] = ai_overlay_counter[ai_counter]
					ai_weight[next] = ai_weight[ai_counter]

					newAI_copy_state(next);
				}
			}
			// post execution
		
			
		}
	} else {
		// basically makes it so that this entry can never be branched 
		ai_processed[ai_counter] = true;
		ai_phase[ai_counter] = DUEL_PHASE_END+1
		//ai_counter++;
	}

	ai_counter++;
	
	// take a break right after the first iteration
	if(ai_counter == 1){
		duelController.ai_resume_point = ai_counter;
		show_debug_message("Taking a break! at: " + string(ai_counter));
		break;
	}
}

var max_weight = -100000;
var chosen_index = 0;

var counter = 0;

// select final action
for(var i = 0; i < AI_MAX_PASS; i++){
	// only calculate weights for proper phases
	if(ai_phase[i] > -1){
		if(final_depth == ai_card_depth[i] || (ai_command[i] == AI_END_PHASE || ai_command[i] == AI_END_TURN)){
			ai_weight[i] = newAI_calculate_weight(i)
			if(ai_weight[i] >= max_weight){
				max_weight = ai_weight[i];
				chosen_index = i;
			}
		}
	}
}

var arr;
arr[0] = 0;

for(var i = 0; i < AI_MAX_PASS; i++){
	// only calculate weights for proper phases
	if(ai_phase[i] > -1){
		if(ai_weight[i] == max_weight){
			arr[counter] = i
			counter++
		}
	}
}

if(duelController.temp_phase_current == DUEL_PHASE_END){
	show_debug_message("fsdfsdfs");	
}

var pos = string_pos(",",ai_full_command[chosen_index])-1

if(pos > 0){
	return string_copy(ai_full_command[chosen_index],1,pos);
} else {
	return ai_full_command[chosen_index]	
}