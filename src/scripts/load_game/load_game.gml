#macro GAME_COMPATIBILITY_VERSION 13

gamepad_keybind_load("gamepad.json")
keyboard_keybind_load("keyboard.json")

if(save_game_valid()){

	load_cards();
	soundMusicVolume(global.musicVolume)
	var appear = ds_map_secure_load("avatar.bin")
	if(ds_exists(appear,ds_type_map)){
		save_playerdata(appear)
		
		ds_map_destroy(appear);	
	}

} else {
	file_delete("cards.bin");
	file_delete("packs.bin");
}

// write the compatibility version to file
ini_open("config.ini")
ini_write_real("info","version",GAME_COMPATIBILITY_VERSION)
ini_close();