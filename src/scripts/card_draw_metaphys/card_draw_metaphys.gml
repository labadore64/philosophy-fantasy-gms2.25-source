var space = 20;
var space2 = 3;
gpu_set_texfilter(false);
gpu_set_colorwriteenable(true,true,true,true)
draw_set_alpha(1)
if(global.high_contrast){
	draw_clear($999999)
} else {
	draw_sprite_extended_noratio(spr_card_front,0,0,0,1,1,0,c_white,1)
}
		
gpu_set_colorwriteenable(true,true,true,false)
draw_set_color($33AAAA)
		
draw_rectangle_noratio(0,0,400,space,false)
draw_rectangle_noratio(0,600-space,400,600,false)
draw_rectangle_noratio(400-space,0,400,600,false)
draw_rectangle_noratio(0,0,space,600,false)

draw_set_alpha(1)
			
// Draw sprite
				
if(card_sprite_ref > -1){
	draw_sprite_extended_noratio(card_sprite_ref,0,40,70,1.25,1.25,0,c_white,1);
}
draw_set_color(c_black)
		
draw_rectangle_noratio(40,70,360,70+space2,false)
draw_rectangle_noratio(40,70,40+space2,390,false)
draw_rectangle_noratio(40,390-space2,360,390,false)
draw_rectangle_noratio(360-space2,70,360,390,false)
			
// draw element
draw_set_color(c_black)
draw_circle_noratio(400-85,115,45,false)
draw_rectangle_noratio(400-85-45,70,360,115,false)
draw_sprite_extended_noratio(spr_type_metaphys,0,400-85,115,2,2,0,c_white,1);

// draw gradients
draw_set_alpha(.3)
// name and infoboxes
draw_rectangle_noratio(43-3,30-3,357+3,60+3,false)
draw_rectangle_noratio(43-3,400-3,357+3,560+3,false)
				
draw_rectangle_noratio(43,30,357,60,false)
draw_rectangle_noratio(43,400,357,560,false)
				
// draw text
				
draw_set_alpha(1)
draw_set_color(c_white)
draw_set_font(global.normalFont)
draw_text_transformed_noratio(50,38,name_display,2,2,0)	