var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _time = map[? "return"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_time)){
			
		var obj = instance_create(0,0,animationTimer);
		if(duelController.do_coinflip <= -1 &&
			!instance_exists(duelCoinFlip)){
			instance_create(0,0,fadeOut)
			duelController.do_coinflip = 20;
			duelController.return_name = _time;
		}
		obj.timer = 120
		
		return obj;
	}

}

return noone;