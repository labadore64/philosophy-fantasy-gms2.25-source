var obj = instance_create(0,0,afterBattleQueue)
var queue = obj.queue;

if(ds_exists(queue,ds_type_queue)){
	
	// money textbox
	// no money if free duel
	if(!obj_data.free_duel){
		if(duel_money > 0 &&
			(duelController.duel_hp[1].hp_current != duelController.duel_hp[0].hp_current)){
				var moneystring = "";
				var moneymount = duel_money*.01;
				var add = true;
			
				if(duelController.duel_hp[1].hp_current <= 0){
					moneystring = "You got $@0 for winning!";
				} else if(duelController.duel_hp[0].hp_current <= 0){
					moneymount = moneymount*.33;
					moneystring = "You lost $@0 in defeat...";
					add = false;
				} 
			
				if(add){
					money_add(moneymount);	
				} else {
					money_take(moneymount);	
				}
		
				moneystring = string_replace_all(moneystring,"@0",money_stringify(moneymount))
		
				ds_queue_enqueue(queue,duelEnd_insert_textbox(
						moneystring
						));
		}
	}
	
	// if replay is ask, ask the player to do replay
	if(global.replay == 1){
		ds_queue_enqueue(queue,duelEnd_insert_recordReplay());
	}
}

room_goto(afterBattle)	