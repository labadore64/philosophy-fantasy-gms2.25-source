var lister = ds_list_create();
var facedown = ds_list_create();

ds_list_copy(lister,cards);

// shuffle must be done separately...

var pq = ds_priority_create();

var sizer = ds_list_size(lister);
for(var i = 0; i < sizer; i++){
	ds_priority_add(pq,lister[|i],coolrandom_next_raw());	
}

ds_list_clear(lister);
var counter = 0;
while(!ds_priority_empty(pq)){
	ds_list_add(lister,ds_priority_delete_min(pq));
	counter++;
	if(counter > 1000000){
		ds_list_clear(lister)
		break;
	}
}

ds_priority_destroy(pq);


ds_list_clear(cards);
for(var i = 0; i < card_array_size; i++){
	card_array[i] = lister[|i];
	ds_list_add(cards,card_array[i]);
	ds_list_add(facedown,card_facedown[i]);
}

for(var i = 0; i < card_array_size; i++){
	var val = ds_list_find_index(lister,card_array[i]);
	card_facedown[0] = facedown[| val]
}

ds_list_destroy(facedown)
ds_list_destroy(lister);