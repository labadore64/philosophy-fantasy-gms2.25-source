with(ai_duelField){
	var card_id = part[DUEL_FIELD_ACTIVE].card_array[0]
	
	if(card_id > -1){
		var adder1 = 0;
		var adder2 = 0;

		for(var i = 1; i < 3; i++){
			if(part[DUEL_FIELD_ACTIVE].card_array[i] > -1){
				adder1 += obj_data.card_attack_overlay[part[DUEL_FIELD_ACTIVE].card_array[i]]
				adder2 += obj_data.card_defense_overlay[part[DUEL_FIELD_ACTIVE].card_array[i]]
			}
		}
		
		
			
		card_attack_power = adder1 + obj_data.card_attack[card_id] + att_boost
		card_defense_power = adder2 + obj_data.card_defense[card_id] + def_boost
	}
}