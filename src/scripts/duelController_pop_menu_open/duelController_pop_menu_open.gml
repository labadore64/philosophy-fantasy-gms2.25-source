mouse_card_selected = -1
mouse_card_selected_last = -1
selected_object = field[0];
last_selected_object = noone
last_selected_thingie = noone

// make tooltips
menuClearTooltip();
for(var i = 0; i < side_button_total; i++){
	menuAddTooltip(side_button_x+side_button_spacer*(i),side_button_y-60,
					side_button_name[i])
	var poz = tooltip_total-1

	tooltip[poz].x -= tooltip[poz].width*.25
}

soundfxPlay(sfx_sound_battle_menu_open)
side_sprite_yscale = -1
duelController_initialize_mouse();