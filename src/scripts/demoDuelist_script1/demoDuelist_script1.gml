textboxShow(duelist_say1,duelist_name)
textbox.name = duelist_name;
textbox.portrait_sprite = duelist_sprite
running_object = textbox;

textbox.newalarm[2] = TIMER_INACTIVE
textbox.newalarm[3] = TIMER_INACTIVE

soundPlaySong(challenge_music,0)

with(textbox){
	event_perform(ev_alarm,2)
	event_perform(ev_alarm,3)
	with(obj_dialogue){
		newalarm[0] = TIMER_INACTIVE
		event_perform(ev_alarm,0)
	}
}