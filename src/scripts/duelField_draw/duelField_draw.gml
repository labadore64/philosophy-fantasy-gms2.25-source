with(argument[0]){
	/// @description Insert description here
	// You can write your code in this editor

	// draw the field

	if(!surface_exists(surf)){
		surf = surface_create(600,550);	
		surface_update = true;
	}

	if(surface_update){
		
		with(duelController){
			if(!mouse_card_updated){
				mouse_card_count = 0;	
			}
		}
		
		var move_x = 200;
		var move_y = 200;
	
		surface_set_target(surf)

		draw_clear_alpha(c_black,0)
		gpu_set_colorwriteenable(true,true,true,true)
		
		if(global.high_contrast){
			draw_set_alpha(.95)
		} else {
			draw_set_alpha(.9)
		}
		var shift = -90-25
		var shift_y = 110+30

			draw_set_color(c_black)

		draw_rectangle(move_x+shift-3,move_y-3-shift_y,move_x+490+shift+3,move_y+360+3-shift_y,false)
		draw_rectangle(move_x+shift,move_y-shift_y,move_x+490+shift,move_y+360-shift_y,false)

		draw_set_color($7F7FFF)

		// bench
		draw_rectangle(move_x-3,move_y-3,move_x+250+3,move_y+80+3,false)
		draw_rectangle(move_x,move_y,move_x+250,move_y+80,false)

		draw_set_color($7FFFFF)
		// active
		var shift = 100
		var shift_y = 110;
		draw_rectangle(move_x+shift-3,move_y-3-shift_y,move_x+60+shift+3,move_y+80+3-shift_y,false)
		draw_rectangle(move_x+shift,move_y-shift_y,move_x+60+shift,move_y+80-shift_y,false)

		draw_set_color($FF7F7F)
		// deck
		var shift = 280
		var shift_y = -110;
		draw_rectangle(move_x+shift-3,move_y-3-shift_y,move_x+60+shift+3,move_y+80+3-shift_y,false)
		draw_rectangle(move_x+shift,move_y-shift_y,move_x+60+shift,move_y+80-shift_y,false)

		draw_set_color($7FFF7F)
		// wait pile
		var shift = 280
		var shift_y = 0;
		draw_rectangle(move_x+shift-3,move_y-3-shift_y,move_x+60+shift+3,move_y+80+3-shift_y,false)
		draw_rectangle(move_x+shift,move_y-shift_y,move_x+60+shift,move_y+80-shift_y,false)

		draw_set_color($FF7FFF)
		// theory
		var shift = -90
		var shift_y = 0;
		draw_rectangle(move_x+shift-3,move_y-3-shift_y,move_x+60+shift+3,move_y+80+3-shift_y,false)
		draw_rectangle(move_x+shift,move_y-shift_y,move_x+60+shift,move_y+80-shift_y,false)

		// so that way you dont break shit
		draw_set_alpha(1)

		var sprite_posy = 100;

		// Draw the cards
		//bench
		with(bench){
	
			var startx = 50
			var spacer = 75;
	
			var posy = 340
	
			var index = 0;
			for(var i = 0; i < card_array_size; i++){
				if(card_array[i] > -1){
					if(card_facedown[i]){
						index = 5;	
					} else {
						if(obj_data.card_type[card_array[i]] == CARD_TYPE_PHILOSOPHER){
							index = obj_data.card_element[card_array[i]];
						} else if (obj_data.card_type[card_array[i]] == CARD_TYPE_EFFECT){
							index = 3;
						} else {
							index = 4;
						}
					}
		
					cardSource_draw_card(move_x+startx+spacer*i,posy-sprite_posy,index)
					if(enemy){
						duelController_add_mouse_card(10+(spacer*2) + move_x+startx-spacer*i,posy-sprite_posy-130,id,i);	
					} else {
						duelController_add_mouse_card(move_x+startx+spacer*i,posy-sprite_posy+300,id,i);	
					}
					
				}
			}
		}

		// active philosopher
		with(active_monster){
	
			var startx = 130
	
			var posy = 210
	
			spacer = 10;
	
			var index = 0;
			var counter = 0;
			
			var modi = (3-card_array_size)*spacer;
			
			for(var i = card_array_size-1; i >= 0; i--){
		
				if(card_array[i] != -1){
					if(card_facedown[i]){
						index = 5;	
					} else {
						if(obj_data.card_type[card_array[i]] == CARD_TYPE_PHILOSOPHER){
							index = obj_data.card_element[card_array[i]];
						} else if (obj_data.card_type[card_array[i]] == CARD_TYPE_EFFECT){
							index = 3;
						} else {
							index = 4;
						}
					}
					
					if(card_array_size > 1){
						if(enemy){
							duelController_add_mouse_shape(move_x+startx,170+90-(spacer+40)*(counter),60,50,id,i);	
						} else {
						
							duelController_add_mouse_shape(move_x+startx,400+(spacer+40)*(counter),60,50,id,i);	
						}
						

					} else {
						if(enemy){
							duelController_add_mouse_card(move_x+startx,90+posy-sprite_posy+modi-spacer*counter,id,i);	
						} else {
							duelController_add_mouse_card(move_x+startx,300+posy-sprite_posy+modi+spacer*counter,id,i);	
						}
					}

					cardSource_draw_card(move_x+startx,posy-sprite_posy+modi+spacer*counter,index)
					counter++;
				}
			}
		}

		// theory
		with(theory){
	
			var startx = -60
	
			var posy = 340
	
			var index = 0;
			var counter = 0;
			if(card_array_size > 0){
				if(card_array[0] != -1){
					if(card_facedown[0]){
						index = 5;	
					} else {
						if(obj_data.card_type[card_array[0]] == CARD_TYPE_PHILOSOPHER){
							index = obj_data.card_element[card_array[0]];
						} else if (obj_data.card_type[card_array[0]] == CARD_TYPE_EFFECT){
							index = 3;
						} else {
							index = 4;
						}
		
						cardSource_draw_card(move_x+startx,posy-sprite_posy,index)
						counter++;
					}
				}
			}
			if(enemy){
				duelController_add_mouse_card(150+370,110,id,i);	
			} else {
				duelController_add_mouse_card(510-370,540,id,i);	
			}
		}

		// deck
		with(deck){
			if(card_array_size > 0){
				var startx = 310
	
				var posy = 340+110
				if(card_array_size > 1){
					cardSource_draw_card(move_x+startx+4,posy-sprite_posy-4,5)
				}
				cardSource_draw_card(move_x+startx,posy-sprite_posy,5)
			}
			
			if(enemy){
				duelController_add_mouse_card(150,110-110,id,i);	
			} else {
				duelController_add_mouse_card(510,540+110,id,i);	
			}
		}

		// waiting zone
		with(wait){
			var startx = 310
	
			var posy = 340
			
			if(card_array_size > 0){

				if(card_array_size > 1){
					cardSource_draw_card(move_x+startx+4,posy-sprite_posy-4,5)
				}
		
				if(card_array[0] != -1){
					if(obj_data.card_type[card_array[0]] == CARD_TYPE_PHILOSOPHER){
						index = obj_data.card_element[card_array[0]];
					} else if (obj_data.card_type[card_array[0]] == CARD_TYPE_EFFECT){
						index = 3;
					} else {
						index = 4;
					}
		
					cardSource_draw_card(move_x+startx,posy-sprite_posy,index)
					
					counter++;
				}
			}
			
			if(enemy){
				duelController_add_mouse_card(150,110,id,i);	
			} else {
				duelController_add_mouse_card(510,540,id,i);	
			}
			
		}

		with(hand){
	
			var start_x = -60;
			var end_x = 420;
			var spacer = (end_x-start_x)/5;
			var posy = 340+240
	
			if(card_array_size > 5){
				spacer = (end_x-start_x)/(card_array_size+1);
			}
	
			for(var i = card_array_size-1; i >= 0; i--){
				if(card_array[i] != -1){
					if(card_facedown[i]){
						index = 5;	
					} else {
						if(obj_data.card_type[card_array[i]] == CARD_TYPE_PHILOSOPHER){
							index = obj_data.card_element[card_array[i]];
						} else if (obj_data.card_type[card_array[i]] == CARD_TYPE_EFFECT){
							index = 3;
						} else {
							index = 4;
						}
					}
		
					cardSource_draw_card(move_x+start_x+spacer*i,posy-sprite_posy,index)
					if(enemy){
						duelController_add_mouse_card(move_x+start_x-spacer*(i)+380,-610+posy-sprite_posy,id,i);	
					} else {
						duelController_add_mouse_card(move_x+start_x+spacer*i,300+posy-sprite_posy,id,i);	
					}
					counter++;
					
				}	
			}
		}
		surface_reset_target();
		surface_update = false;
	}
}

