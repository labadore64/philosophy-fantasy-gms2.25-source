menupos = 0;
selected_draw_index = 0;
option_top = 0

sort_mode_pos++;
if(sort_mode_pos >= sort_size){
	sort_mode_pos = 0	
}
sort_mode = sort_id[sort_mode_pos]

script_execute(sort_script[sort_mode_pos])


deckBuild_setDisplayArrays();
deckBuild_updateSprite();
if(global.menu_sounds){
soundfxPlay(sfx_sound_menu_twinkle)
}

tts_say(sort_ax_id[sort_mode_pos] + " sorted");