var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}

draw_set_color(col)

var stringer = "";
var xx = 0;
var yy = 0;

//get max number of items that can be displayed
var drawmax = 0;
var counter = 0;

var drawmax = option_top + display_lines-1;

counter = 0;
for(var i = option_top; i < drawmax && i < menu_count; i++){
	
	stringer = menu_name[ i];
	

		xx = x1+option_x_offset;
		yy = -7+y1+option_y_offset+option_space*counter;

		draw_text_transformed_noratio(xx,yy,stringer,2,2,0)
	counter++;
}

draw_set_alpha(1)
draw_set_color(col )