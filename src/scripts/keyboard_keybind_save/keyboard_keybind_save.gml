/// @function keyboard_keybind_save(file)
/// @description Saves the state of the keyboard keybind in JSON to a file.
/// @param {string} file The file to save to.

var save_map = ds_map_create();

for(var i = 0; i < global.keybind_size; i++){
	ds_map_add(save_map,
				global.keybind_array[i], 
				keyboardToString(global.keybind_map[? global.keybind_array[i]]));
}

var stringer = json_encode(save_map);

var file = file_text_open_write(argument[0]);

file_text_write_string(file, stringer);

file_text_close(file);

ds_map_destroy(save_map);