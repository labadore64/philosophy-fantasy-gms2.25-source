var obj = instance_create(0,0,menuBook);

obj.newalarm[2] = TIMER_INACTIVE

obj.active = true

var list = ds_list_create();
var text = text_text[menupos];
var titler = title_text[menupos];
var display_linesize = 43
var queue = -1;

var dia = ds_map_create();

with(obj){
	//var stringwidth = (string_width(" "))*text_size
	
	menu_clear_option()
	text = string_replace_all(text,"\n","~");
	// do diagrams
	text = menuBookDiagramReplace(text,dia)
	text = textForceCharsPerLine(text,display_linesize);
	text = string_replace_all(text,"\n","~");
	
	text_string = text;
	queue = split_string(text,"~");
	while(!ds_queue_empty(queue)){
		if(ds_queue_size(queue) > 1){
			ds_list_add(list,ds_queue_dequeue(queue)+"~");
		} else {
			ds_list_add(list,ds_queue_dequeue(queue));	
		}
	}
	ds_queue_destroy(queue);	
	

	var sizer = ds_list_size(list);
	var sss = 0;
	for(var i = 0; i < sizer; i++){
		menu_add_option(list[|i],"",-1,-1)
		sss += string_length(list[|i]);
	}
	
	title = titler
	
	var size, key, i;
	size = ds_map_size(dia);
	key = ds_map_find_first(dia);
	for (i = 0; i < size; i++;)
	{
		var char_index = key;
		var lenny;
		var counter = 0;
		var column_count = 0;
		var pagenum = 0;
		var strinlen =0;
		
		var row_count = 0;
		for(var j = 0; j < menu_count; j++){
			strinlen = string_length(list[|j]);
			lenny = counter+ strinlen;
			if(char_index > lenny){
				counter = lenny;
			} else {
				row_count = j % display_lines;
				pagenum = floor(j / display_lines)
				column_count = char_index - counter
				break;	
			}
		}
		
		
		
		var xx = x1+option_x_offset+(string_width(string_copy(list[|j],1,column_count))*text_size);
		var yy = y1+option_y_offset+option_space*row_count;
		
		menuBookCreateDiagram(xx,yy,dia[? key],pagenum)
		key = ds_map_find_next(dia, key);
	}
	
	tts_say(titler);
	
	ds_list_destroy(list);
	
	x = 395
	y = 0
	draw_width = 800
	
	menuParent_menu_makeMouse();
}

ds_map_destroy(dia);