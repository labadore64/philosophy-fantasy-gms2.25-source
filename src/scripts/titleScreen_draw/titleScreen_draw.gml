var col = global.text_fgcolor

if(global.high_contrast){
	col = c_white;
}
	
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		titleScreen_DrawBackground()
	}
	
	
	//if(global.menu_animate){
		/*
		with(card_display){
			if(card_sprite > -1){
				gpu_set_blendmode(bm_add)
				var shadow = 380
				draw_sprite_pos_ratio(card_sprite,0,
								spec_xx[0],spec_yy[0]+shadow,
								spec_xx[1],spec_yy[1]+shadow,
								spec_xx[2],spec_yy[2]+shadow,
								spec_xx[3],spec_yy[3]+shadow,
								$aaaaaa,
								.15)	
				gpu_set_blendmode(bm_normal)
			}
		}
		*/
	//}
	
	//card_draw(card_display);
	
	// draw game title
	/*
	draw_set_font(global.largeFont)
	var spacerx = 60;
	var spacery = 80;
	var t = card_counter;
	var sin_adder = 0;
	
	for(var i = 0; i < 3; i++){
		for(var j = 0;j < title_len[i]; j++){
			t++;
			sin_adder= sin(t*.1)*10;
			gpu_set_blendmode(bm_add)
			draw_set_color(c_white);
			draw_text_transformed_ratio(xx+spacerx+15*i,yy+spacery*i+sin_adder+10,title_bounce[i,j],3,3,0)
			spacerx += string_width(title_bounce[i,j])*3;
			gpu_set_blendmode(bm_normal)
		}
		spacerx = 0;
	}*/

	// draw game options
	draw_set_halign(fa_center)
	draw_set_font(global.normalFont)

	var bla = global.menu_bgcolor;

	if(global.high_contrast){
		bla = c_black;	
	}

	if(title_anim > 74){
		for(var i = 0; i < menu_count; i++){
			gpu_set_colorwriteenable(true,true,true,true)
				draw_set_color(bla)
				draw_set_alpha(.45)
				draw_rectangle_ratio(option_start_x-175,option_start_y+option_spacer*(i-.5+.25),
										option_start_x+175,option_start_y+option_spacer*(i+.4+.25),
										false)
			if(i == menupos){
			
			
				draw_set_color(c_white)
				if(global.menu_animate){
					draw_set_alpha(alpha_adder);
				} else {
					draw_set_alpha(alpha_max)
				}
				if(global.high_contrast){
					draw_rectangle_ratio(option_start_x-175,option_start_y+option_spacer*(i-.5+.25),
											option_start_x+175,option_start_y+option_spacer*(i+.4+.25),
											false)
				} else {
					gpu_set_blendmode(bm_add)
					draw_rectangle_color_ratio(option_start_x-175,option_start_y+option_spacer*(i-.5+.25),
												option_start_x+175,option_start_y+option_spacer*(i+.4+.25),
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
												false)
					gpu_set_blendmode(bm_normal)
				}
			
			}
			draw_set_color(col)
			draw_set_alpha(1)
			draw_text_transformed_ratio(option_start_x,-7+option_start_y+option_spacer*i,menu_name[i],option_text_size,option_text_size,0)
			if(i == menupos){
				//draw_sprite_extended_ratio(spr_talk_continue,ScaleManager.spr_counter+15,option_start_x-105,option_start_y+option_spacer*i-10,1.5,1.5,0,c_white,1)
			}
		}
	
	}
	if(!global.enableShader || title_anim < 71){
		draw_sprite_extended_ratio(sprite_index,title_anim,0,0,1,1,0,c_white,1)	
	} else {
		draw_title_shader();	
	}
	
	draw_set_halign(fa_left)
	
	with(fadeIn){
		draw_fade();	
	}

	with(fadeOut){
		draw_fade();	
	}

	draw_letterbox();
