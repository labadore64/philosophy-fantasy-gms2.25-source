/// @description - plays a sound effect
/// @function soundfxPlay(sound)
/// @param sound

	audio_stop_sound(argument[0]);
	var sound = audio_play_sound(argument[0],1,false);
	audio_sound_gain(sound,global.sfxVolume,0);
	return sound;