/// @description Insert description here
// You can write your code in this editor

with(coinFlip){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		titleScreen_DrawBackground()
	}

	
	if(!start_coin_flip || menu_choose){
		draw_set_halign(fa_left)
		draw_set_font(global.normalFont)

		if(global.high_contrast){
			draw_rectangle_ratio(x-3,y-3,x+menu_width+3,y+menu_height+3,false)
		} else {
			if(global.color_gradient){
				draw_rectangle_color_ratio(x-5,y-5,x+menu_width+5,y+menu_height+5,
					global.menu_grcolor[1],global.menu_grcolor[1],
					global.menu_grcolor[0],global.menu_grcolor[0],
				false)
		
			} else {
				draw_set_color(global.menu_grcolor[0])
				draw_rectangle_ratio(x-5,y-5,x+menu_width+5,y+menu_height+5,
				false)
			}
		}

		if(global.high_contrast){
			draw_set_color(c_black)
		} else {
			draw_set_color(global.menu_bgcolor)
		}
		draw_set_alpha(1)
		draw_rectangle_ratio(x,y,x+menu_width,y+menu_height,false)

		draw_rectangle_ratio(0,540,800,600,false)

		draw_set_color(c_white)
		if(global.menu_animate){
			draw_set_alpha(alpha_adder);
		} else {
			draw_set_alpha(alpha_max)
		}
		if(global.high_contrast){
			draw_rectangle_ratio(x,y-20+option_y_offset+option_spacer*(menupos),
							x+menu_width,y-20+option_y_offset+option_spacer*(menupos+1),
						false)
		} else {
			draw_rectangle_color_ratio(x,y-20+option_y_offset+option_spacer*(menupos),
							x+menu_width,y-20+option_y_offset+option_spacer*(menupos+1),
			global.menu_selcolor[1],global.menu_selcolor[1],
			global.menu_selcolor[0],global.menu_selcolor[0],
			false)
		}
		draw_set_alpha(1)
		draw_set_color(global.text_fgcolor)

		draw_set_halign(fa_center)
		for(var i = 0; i < menu_count; i++){
			draw_text_transformed_ratio(x+menu_width*.5,-10+y+option_y_offset+option_spacer*i,menu_name[i],4,4,0)	
		}

		//draw_sprite_extended_ratio(spr_talk_continue,ScaleManager.spr_counter+15, x+option_x_offset-100,y-10+option_y_offset+option_spacer*menupos,1.5,1.5,0,c_white,1)	

		//draw_set_halign(fa_center)
		draw_text_transformed_ratio(400,550,menu_desc[menupos],3,3,0)	
		draw_set_halign(fa_left)

	} else {
		if(!calling){
			if(coin_flip_animate){
				draw_sprite_extended_ratio(spr_coinflip_animate,index,0,0,1,1,0,c_white,1)
			} else {
				if(countdown <= -1){
					if(heads){
						draw_sprite_extended_ratio(spr_coinflip_heads,0,0-coin_transition_x,0,1,1,0,c_white,1)
					} else {
						draw_sprite_extended_ratio(spr_coinflip_tails,0,0-coin_transition_x,0,1,1,0,c_white,1)
					}
		
					if(coin_transition_x <= 0){
						draw_set_color(global.text_fgcolor)
						draw_set_alpha(1)
						draw_set_font(global.largeFont)
						draw_set_halign(fa_center)
						draw_text_transformed_ratio(400,500,coin_string,3,3,0)
						draw_set_halign(fa_left)
			
					}
				}
			}
		}
	}
}

with(animationWindow){
	duelController_draw_message()	
}

with(fadeIn){
	draw_fade()	
}

with(fadeOut){
	draw_fade()	
}

draw_letterbox()