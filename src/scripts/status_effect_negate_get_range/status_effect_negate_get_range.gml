// this tests if the card is in the specified zone. if it is, then cool.
// since this is generalized for both card sides they both pipe here.

var target = argument[0];
var this = argument[1]; 

if(string_pos("_bench",target) > 0){
	if(string_pos("bench", this) > 0){
		return true;	
	}
} else if(string_pos("_hand",target) > 0){
	if(string_pos("hand", this) > 0){
		return true;	
	}	
} else if(string_pos("_deck",target) > 0){
	if(string_pos("deck", this) > 0){
		return true;	
	}
} else if(string_pos("_theory",target) > 0){
	if(string_pos("theory", this) > 0){
		return true;	
	}
} else if(string_pos("_active",target) > 0){
	if(string_pos("active", this) > 0){
		return true;	
	}
} else if(string_pos("_overlay",target) > 0){
	if(string_pos("overlay", this) > 0){
		return true;	
	}
} else if(string_pos("_limbo",target) > 0){
	if(string_pos("limbo", this) > 0){
		return true;	
	}
}

return false