with(duelController){
	var card = argument[0]; //card ID to test
	var query = argument[1]; // query to use
	var result = 0;
	if(card > -1){

		var bracket_level = 0;

		// reset the array
		code_query = -1;
		code_query[0] = "";
	
		var test = ""
		var literal = false;
	
		var counter = 1;
		var len = string_length(query);
		var bracket_len = 0
		var char = "";
		
		var counterz = 0;
		while(counter <= len){
			char = string_char_at(query,counter);
			if(char == "["){
				bracket_level++;
				code_query[bracket_level] = "";
			} else if (char == "]"){
				// process that bracket.
			
				bracket_len = string_length(code_query[bracket_level]);
				counter-=bracket_len+1;
			
				test= string_copy(query,counter+1,bracket_len);
			
				result = code_card_query_process(card,code_query[bracket_level]);
			
				query = string_delete(query,counter,bracket_len+2);
				query = string_insert(string(result),query,counter);
				counter--;
				len = string_length(query);
				bracket_level--;
			} else {
				//add to the current bracket
				code_query[bracket_level] += char;
			}
			counter++;	
			
			counterz++;
			if(counterz > 1000000){
				break;	
			}
		}
	}
	
	return result;
} 
return 0
