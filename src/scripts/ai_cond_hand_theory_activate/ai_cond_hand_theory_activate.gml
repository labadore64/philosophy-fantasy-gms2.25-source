var me = argument[0];

var pos = me.menupos;

if(argument_count > 1){
	pos = argument[1];	
}

var ai_val = -1

if(argument_count > 2){
	ai_val = argument[2]
}


var menu_selected = me.card_array[pos]

if(ai_val > -1){
	menu_selected = me.ai_card_array[ai_val,pos]
}

if(!duelController.code_select_active){
	if(duelController.phase_current == DUEL_PHASE_MAIN){
		if(me.card_array[me.menupos] > -1){
			if(obj_data.card_type[me.card_array[me.menupos]] == CARD_TYPE_METAPHYS &&
				parent.parent.theory.card_array_size == 0){
					return true;
				}
		}
	}
}

return false;