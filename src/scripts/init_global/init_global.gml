#macro POSITION_PITCH_MIN .8
#macro POSITION_PITCH_MAX 1.2

#macro CAMERA_TRACK_NONE 0
#macro CAMERA_TRACK_BASIC 1
#macro CAMERA_TRACK_ALL 2

#macro TOOLTIP_TYPE_NONE 0
#macro TOOLTIP_TYPE_ONLY 1
#macro TOOLTIP_TYPE_BOTH 2

#macro DEFAULT_ACTIVITY_POINTS 6

#macro PAID_VERSION 0
global.game_ending = false;
global.playerdata = "";
global.dont_record = false;
global.goto_map = MainRoom
global.do_pack_select = false;
global.force_pack_select = false
global.activity_points = DEFAULT_ACTIVITY_POINTS
global.story_next = ""
global.selected_pack = 0;
global.selected_pack_id = 0;
global.story_last[0] = "";
global.story_last[1] = "";
global.loop_sound = -1
global.loop_sound_val = -1

global.game_week = 0
global.game_month = 0;
global.game_year = 2022

global.month_names[0] = "January"
global.month_names[1] = "February"
global.month_names[2] = "March"
global.month_names[3] = "April"
global.month_names[4] = "May"
global.month_names[5] = "June"
global.month_names[6] = "July"
global.month_names[7] = "August"
global.month_names[8] = "September"
global.month_names[9] = "October"
global.month_names[10] = "November"
global.month_names[11] = "December"

draw_set_circle_precision(32);
	init_font();
	randomize()
	global.inputEnabled = true;

	if(!file_exists(environment_get_variable("APPDATA")+ "\\WhatIsPhilosophyAnyways\\config.ini")){
		file_copy(working_directory + "\\default_config.ini", environment_get_variable("APPDATA")+ "\\WhatIsPhilosophyAnyways\\config.ini");	
	}

	file_delete("config_backup.ini");

	file_copy("config.ini","config_backup.ini");

	ini_open("config.ini")
	
	global.version_no = ini_read_real("info","version",0);

	var tts = stringToBool(ini_read_string("ax","tts","false"))
	var tts_details = stringToBool(ini_read_string("ax","tts_details","false"))
	
	var graphics = stringToBool(ini_read_string("graphics","enabled","true"))
	var shader = stringToBool(ini_read_string("graphics","shader_enabled","true"))
	var fullscreen = stringToBool(ini_read_string("graphics","fullscreen","false"))
	var highcontrast = stringToBool(ini_read_string("graphics","high_contrast","false"))
	var menu_animate = stringToBool(ini_read_string("graphics","menu_animate","true"))
	var particles = stringToBool(ini_read_string("graphics","particles","true"))
	var card_icon = stringToBool(ini_read_string("graphics","card_icon","false"))
	var menu_sprite = ini_read_string("graphics","menu_bg","none")
	var skip_splash = stringToBool(ini_read_string("graphics","skip_splash","false"))
	
	var text_auto = stringToBool(ini_read_string("text","auto","true"))
	var text_full = stringToBool(ini_read_string("text","full","false"));
	
	var stick = stringToBool(ini_read_string("control","stick","false"))
	var vibration = stringToBool(ini_read_string("control","vibration","true"))
	var vibration_amount = ini_read_real("control","vibration_amount",1)
	var mouse = stringToBool(ini_read_string("control","mouse","true"))
	var keyword_desc = stringToBool(ini_read_string("control","tooltip","true"))
	
	var scroll_speed = ini_read_real("battle","mouse_pan_speed",5)
	var zoom_speed = ini_read_real("battle","mouse_zoom_speed",5)
	var tooltip = ini_read_real("battle","tooltip",TOOLTIP_TYPE_BOTH)
	var mouseright = stringToBool(ini_read_string("battle","mouse_display_right","true"))
	var fastmode = stringToBool(ini_read_string("battle","fastbattle","false"))
	var difficulty = ini_read_real("battle","difficulty",1)
	var skipduel = stringToBool(ini_read_string_length("battle","skip","false",6))
	var drawhealth = stringToBool(ini_read_string_length("battle","draw_health","true",6))
	var soundhealth = stringToBool(ini_read_string_length("battle","health_sound","true",6))
	var coinflip = stringToBool(ini_read_string_length("battle","coin_toss","true",6))
	var press_continue = stringToBool(ini_read_string_length("battle","press_continue","true",6))
	var reversepointer = stringToBool(ini_read_string_length("battle","invert_cursor","false",6))
	var pause_anim = stringToBool(ini_read_string_length("battle","pause_anim","false",6))
	var replay = ini_read_real("battle","replay",0)
	
	var charaname = ini_read_string_length("character","name","Alex",10)
	var money = clamp(ini_read_real("character","money",10),0,9999)

	var sfx = ini_read_real("sound","sfx_vol",.5);
	var music = ini_read_real("sound","music_vol",.5)
	var menusounds = stringToBool(ini_read_string_length("sound","menu","true",6))
	var positionpitch = stringToBool(ini_read_string_length("sound","positional_pitch","true",6))
	var textsound = stringToBool(ini_read_string_length("sound","text_sound","true",6))
	var extra_sounds = stringToBool(ini_read_string_length("sound","extrasound","false",6))
	
	game_set_speed(ini_read_real("graphics","fps",60),gamespeed_fps)

	ini_close()

	// phone signal strength
	
	global.signal_strength = 5;

	// represents the default aspect ratio of the game
	// will redraw graphics when window is resized
	global.window_width = window_get_width();
	global.window_height = window_get_height();
	global.display_x = 0;
	global.display_y = 0;
	global.letterbox_w = 0;
	global.letterbox_h = 0;
	global.scale_factor = 1;
	global.aspect_ratio = global.window_width/global.window_height;

	global.cantSeeMode = false; //for dark rooms

	//debug
	global.debugMenu = false;
	global.debugPlayerMovement = false;
	global.debugSkipIntro = false

	//options

	global.textSpeed = 1; //speed of text
	global.text_auto_speed = 10;
	global.textFont = global.normalFont;
	global.speak_details = true; //whether or not to read details for menus in tts
	global.tutorial = true; //do the tutorials?


	global.high_contrast = true; //high contrast?

	//button
	global.gamepadRightAxis = false;
	global.gamepadVibration = false;

	// lol whatever ignore the shit above, this is what matters lmao
	global.first_player = DUEL_PLAYER_YOU

	global.voice = tts
	global.drawing_enabled = graphics
	global.enableShader = shader
	global.fullscreen = fullscreen
	global.high_contrast = highcontrast
	global.menu_animate = menu_animate
	global.particles = particles
	global.card_icon = card_icon
	global.skip_splash = skip_splash
	if(menu_sprite == "none"){
		global.menu_index = -1;
	} else {
		global.menu_index = asset_get_index("background_img_" + menu_sprite); //what sprite to draw for backgrounds
	}
	global.tooltip = keyword_desc; // whether or not tooltips are displayed
	
	global.sfxVolume = sfx
	global.musicVolume = music
	global.extra_sounds = extra_sounds;
	global.gamepadRightAxis = stick
	global.gamepadVibration = vibration
	global.gamepadVibrationAmount = vibration_amount
	global.speak_details = tts_details
	global.menu_sounds = menusounds
	global.positional_pitch = positionpitch // whether or not positional pitch is enabled
	global.text_sound = textsound; //whether or not text makes sound in cutscenes
	
	global.fastmode = fastmode;
	global.mouse_active = mouse
	global.mouse_snap = true; //mouse snap

	global.display_hp = drawhealth
	global.hp_sound = soundhealth
	global.pan_speed = scroll_speed // how fast you pan around the duel map
	global.zoom_speed = zoom_speed // how fast you zoom around the duel map
	global.tooltip_type = tooltip // which tooltip configuration to use
	global.mouse_right = mouseright; // which side to show the mouse controls
	global.difficulty = difficulty; // how hard the game's battle's AI is
	global.skip_duel = skipduel; // you can skip duels
	global.coin_flip = coinflip // whether or not to do the coin flip to go first
	global.skip_this_coin = false; //whether or not to skip the coin flip 
	global.press_continue = press_continue; //whether or not you have to press to continue input. always enabled if TTS.
	global.reverse_pointer = reversepointer; //whether or not to reverse the pointing cursor
	global.pause_anim = pause_anim; //whether or not animations have accessibility pauses
	global.replay = replay; // replay behavior
	
	// used only when asking to save a replay
	global.replay_temp = "";
	
	global.text_auto = text_auto; // whether or not auto continue is true for dialog
	global.text_full = text_full // whether or not the text is full screen
	
	global.max_ai = 0; // the maximum level of the AI at a certain part in the game.
	global.event_set = "" // event to be triggered
	
	global.character_name = charaname	
	global.max_knowledge = 0
	
	global.money = money;

	window_set_fullscreen(global.fullscreen)
	soundMusicVolume(global.musicVolume)
	
	var json = -1;
	var filenm = environment_get_variable("APPDATA")+ "\\WhatIsPhilosophyAnyways\\theme.json";
	
	if(!file_exists(filenm)){
		json = json_decode(file_text_open_read(filenm))
	}
	
	global.menu_grcolor[0] = $000000
	global.menu_grcolor[1] = $661100
	
	global.menu_selcolor[0] = c_white
	global.menu_selcolor[1] = $661100
	
	global.menu_bgcolor = $440500
	
	global.text_bgcolor[0] = c_black
	global.text_bgcolor[1] = $661100
	
	global.screen_bgcolor[0] = c_black
	global.screen_bgcolor[1] = $338800
	
	global.text_fgcolor = $FFDDAA
	
	global.color_gradient = true
	
	if(ds_exists(json,ds_type_map)){
		
		if(ds_map_exists(json,"menu_colorbg1")){
			global.menu_grcolor[0] = json[? "menu_colorbg1"]
		}
		if(ds_map_exists(json,"menu_colorbg2")){
			global.menu_grcolor[1] = json[? "menu_colorbg2"]
		}
	
		if(ds_map_exists(json,"menu_colorsel1")){
			global.menu_selcolor[0] = json[? "menu_colorsel1"]
		}
		if(ds_map_exists(json,"menu_colorsel2")){
			global.menu_selcolor[1] = json[? "menu_colorsel2"]
		}
	
		if(ds_map_exists(json,"menu_colorbg")){
			global.menu_bgcolor = json[? "menu_colorbg"]
		}
	
		if(ds_map_exists(json,"text_colorbg1")){
			global.text_bgcolor[0] = json[? "text_colorbg1"]
		}
		if(ds_map_exists(json,"text_colorbg2")){
			global.text_bgcolor[1] = json[? "text_colorbg2"]
		}
	
		if(ds_map_exists(json,"text_colorfg")){
			global.text_fgcolor = json[? "text_colorfg"]
		}
	
		if(ds_map_exists(json,"do_gradient")){
			global.color_gradient = json[? "do_gradient"]
		}
		
		ds_map_destroy(json);	
	}
	
	global.le_mouse = false;
	
	global.camera_track = CAMERA_TRACK_ALL;


// stupid bullshit globals 
global.alpha_recurse = 0;
// used to store the main menu cursor
global.mainMenupos = 0;

// stores data about duelists
global.duel_player[0] = ""
global.duel_player[1] = ""
global.duel_actions = -1
global.duel_actions[0] = "";
global.duel_random = -1
global.first_player = 0

// stores data for appearance
global.appearance_room = TitleRoom