var obj = instance_create(argument[0],argument[1],StarterDeck);
with(obj){
	image_blend = $7FFF7F
	card_sprite = card_from_name("marx")
	bg_sprite = obj_data.card_sprite_bg[card_from_name("marx")]
	type_sprite = spr_typeMaterial

	surface_update = false;
	surf = -1;

	card_width = 325
	card_height = 440

	name = "Marxism"
	subtitle = "A deck based\naround protecting\nthe castle."

	scale = .65
}
return obj;