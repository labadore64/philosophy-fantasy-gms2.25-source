var stringe = string_upper(argument[0]);
var size = string_length(stringe);
var returner = 0;

for(var i = 0; i < size; i++){
	var char = string_char_at(stringe,size-i);
	var val = 0;
	// value is numeric
	if(string_length(string_digits(char)) > 0){
		val = real(char)
	} else {
	
		// value is A-F
		val = ord(char) - 55
	}
	
	returner += val * power(16,i);
}

return returner;