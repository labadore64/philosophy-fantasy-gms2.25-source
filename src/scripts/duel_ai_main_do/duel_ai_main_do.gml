var difficult = min(global.max_ai,global.difficulty)
duel_animation_wait(30)

duel_animation_run_script(duel_ai_standby_select_philosopher)
if(duelController.turns == 0){
	duelController.phase_current = DUEL_PHASE_RETURN
	// populate the bench
	if(difficult > 0){
		duel_animation_run_script(duel_ai_main_phase_populate_bench)
		// overlay from hand
		duel_animation_run_script(duel_ai_main_phase_decide_overlay)
	}
	
	duel_animation_run_script(duel_next_phase_)
	
} else {
	if(difficult > 0){
		// populate the bench
		duel_animation_run_script(duel_ai_main_phase_populate_bench)
	
		// switch with the bench
		duel_animation_run_script(duel_ai_main_phase_decide_switch)
	
		// overlay from hand
		duel_animation_run_script(duel_ai_main_phase_decide_overlay)
	}
	
	duel_animation_run_script(duel_next_phase_)
}
