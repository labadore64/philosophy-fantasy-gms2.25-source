/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\opponent\\enemy";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	// this value will be updated as you do new battles
	var deck = -1;
	opponent_encounter[itemcount] = -1;
	ini_open(testname)
	opponent_name[itemcount] = ini_read_string_length("main","name","",IMPORT_MAX_SIZE_CARDNAME)
	opponent_sprite[itemcount] = asset_get_index("spr_portrait_" + ini_read_string_length("main","sprite","",IMPORT_MAX_SIZE_SPRITE))
	opponent_philo[itemcount] = card_from_name(ini_read_string_length("main","philosopher","",IMPORT_MAX_SIZE_CARDNAME))
	
	ini_close()
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

opponents_total = itemcount;