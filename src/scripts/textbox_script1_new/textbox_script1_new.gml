if(duelist_sprite == spr_portrait_roger){
	duelist_sprite = spr_portrait_roger_cutscene	
}

textboxShow(text_id,duelist_name)

textbox.portrait_sprite = duelist_sprite
running_object = textbox;

textbox.newalarm[2] = TIMER_INACTIVE
textbox.newalarm[3] = TIMER_INACTIVE

if(music > -1){
	soundPlaySong(music,0)
}

with(textbox){
	event_perform(ev_alarm,2)
	event_perform(ev_alarm,3)
	with(obj_dialogue){
		newalarm[0] = TIMER_INACTIVE
		event_perform(ev_alarm,0)
	}
}