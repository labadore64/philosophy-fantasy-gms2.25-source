with(mainMenu){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		mainMenu_DrawBackground()	
	}
}

with(duelBackground){
	if(global.high_contrast){
		draw_clear(c_black)
	} else {
		draw_duelbackground()	
	}
}

if(!surface_exists(surf)){
	surf = surface_create_access(global.letterbox_w,global.letterbox_h)	
	surface_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.letterbox_w,global.letterbox_h);
		surface_update = true;
	}
}


if(surface_update){
	surface_set_target(surf)
	draw_set_color(c_black)
	draw_set_alpha(.75)
	draw_rectangle_noratio(0,0,800,600,false)

	draw_set_alpha(1)
	draw_set_halign(fa_center)
	draw_set_font(global.normalFont)

	gpu_set_colorwriteenable(true,true,true,true)
	draw_set_color(global.text_fgcolor)

	draw_text_transformed_noratio(400,25,title,4,4,0)
	draw_set_halign(fa_left)
	//draw_set_color(subtitle_color)

	gpu_set_colorwriteenable(true,true,true,true)
	
	//draw_set_color(c_black)
	
	//draw_rectangle(0,120,800,600,false)
	
	var scrole = 85
	
	draw_set_color(c_black)
	draw_set_alpha(1)
	draw_rectangle_noratio(770,scrole,800,600,false)

	draw_set_color(c_white)
	if(global.high_contrast){
		draw_set_alpha(.75)
	} else {
		draw_set_alpha(.65)	
	}
	draw_rectangle_noratio(770,scrole+scroll_increment_draw*(option_top),800,min(560,scrole+scroll_increment_draw*(option_top+per_line)-1),false)
	draw_set_alpha(1)

	var drawmax = option_top + display_lines;

	counter = 0;
	for(var i = option_top; i < drawmax && i < menu_count; i++){
	
		draw_set_alpha(1)
		stringer = menu_name[ i];
	
		gpu_set_colorwriteenable(true,true,true,true)
			xx = 10+option_x_offset;
			yy = 37+y1+option_y_offset+option_space*counter;

			draw_text_transformed_noratio(xx,yy,stringer,2,2,0)
			draw_set_halign(fa_center)
			draw_text_transformed_noratio(660-40,yy,display_key[i],2,2,0)
			draw_set_halign(fa_left)
			
		if(counter == selected_draw_index){
			if(global.high_contrast){
				draw_set_alpha(.6)	
			} else {
				draw_set_alpha(.4)
			}
		} else {
			if(global.high_contrast){
				draw_set_alpha(.25)	
			} else {
				draw_set_alpha(.1)
			}
		}
		gpu_set_colorwriteenable(true,true,true,false)
		draw_rectangle_noratio(0,option_y_offset+y1+3-10 + option_space*(counter+1),
						770,option_y_offset+y1-3-10 + option_space*(counter+2),
						false)
		counter++;
	}

	draw_set_color(c_black)

	gpu_set_colorwriteenable(true,true,true,false)

	draw_rectangle_noratio(0,540,800,600,false)
	draw_set_alpha(1)
	draw_set_color(global.text_fgcolor)
	draw_set_halign(fa_center)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_text_transformed_noratio(400,565,menu_desc[ menupos],2,2,0)
	draw_set_halign(fa_left)
	

	with(keyboardSetKey){
		keybind_set_key_draw()	
	}
	with(controllerSetKey){
		keybind_set_key_draw()	
	}
	
	surface_reset_target();
}

draw_set_alpha(1)
draw_surface(surf,global.display_x,global.display_y);
	
draw_letterbox();