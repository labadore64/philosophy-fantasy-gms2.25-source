if(navigation_menu == noone){
	if(global.menu_sounds){soundfxPlay(sfx_sound_battle_menu_nav)}
	navigation_menu = instance_create(start_navx,start_navy,duelNavigation);	
} else {
	with(navigation_menu){
		instance_destroy();	
	}
	if(global.menu_sounds){soundfxPlay(sfx_sound_menu_cancel);}
}

with(duelController){
	script_execute(mouse_menu_script)	
}