var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _val = map[? "val"];
	
	ds_map_destroy(map)
	if(!is_undefined(_val)){
		code_pop_this();
	}

}

return noone;