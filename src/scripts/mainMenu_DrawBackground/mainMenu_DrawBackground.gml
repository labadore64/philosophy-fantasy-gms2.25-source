
	if(!surface_exists(part_surf)){
		part_surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){
			surface_resize(part_surf,global.window_width,global.window_height);
		}
	}
	
	surface_set_target(part_surf)
	
	draw_set_alpha(1)
	if(global.color_gradient){
		draw_rectangle_color_ratio(0,0,800,600,
								global.screen_bgcolor[0],global.screen_bgcolor[0],
								global.screen_bgcolor[1],global.screen_bgcolor[1],
										false)
	} else {
		draw_set_color(global.screen_bgcolor[0])
		draw_rectangle_ratio(0,480,800,600,
									false)
	}
	

	gpu_set_colorwriteenable(true,true,true,false)
	if(global.menu_index > -1){
		gpu_set_blendmode(bm_add)
		if(global.menu_animate){
			draw_sprite_extended_ratio(global.menu_index,ScaleManager.spr_counter,400,300,2,2,0,$181818,1)
		} else {
			draw_sprite_extended_ratio(global.menu_index,75,400,300,2,2,0,$181818,1)
		}
		gpu_set_blendmode(bm_normal)
	}
	
	if(global.particles){part_system_drawit(Sname);}
	surface_reset_target();
	

	draw_surface(part_surf,0,0);	

	
	gpu_set_blendmode(bm_add)

	gpu_set_blendmode(bm_normal)
	
	draw_set_alpha(.75)
	draw_set_color(c_black)
	draw_rectangle_ratio(0,0,800,600,false)	
