// checks bench to see if there's a high attack dude, will switch.

with(duelController){
	if(field[1].bench.card_array_size > 0 && field[1].active_monster.card_array_size > 0){
		var current_attack  = field[1].active_monster.card.card_defense_power;
		var highest_index = -1;
		var highest_val = current_attack
		for(var i = 0; i < field[1].bench.card_array_size; i++){
			if(obj_data.card_defense[field[1].bench.card_array[i]] > highest_val){
				highest_index = i;
				highest_val = obj_data.card_defense[field[1].bench.card_array[i]] 
			}
		}
		
		if(highest_index > -1){
			var mon = field[1].bench.card_array[highest_index]
			var mon2 = field[1].active_monster.card_array[0]
			if(mon > -1){
				tts_say( obj_data.card_name[mon2] + " switched with " + obj_data.card_name[mon] );	
			}	
			
			// switch the current active card with the benched card	
			duel_anim_switch_benched(field[1].bench,field[1].active_monster,field[1].bench.card_array[highest_index],highest_index)
			duel_animation_wait(60)
		}
	}
}