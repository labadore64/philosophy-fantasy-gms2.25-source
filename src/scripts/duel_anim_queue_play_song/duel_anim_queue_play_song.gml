var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _sound = map[? "sound"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_sound)){
			
		soundPlaySong(_sound,0)
		return noone
	}

}

return noone;