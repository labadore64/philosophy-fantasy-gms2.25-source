// check if you have a valid game state.
// At the beginning of the turn, you must start with a philosopher as an active philosopher.
// if not, You must select a benched character to set as your active.
// if neither exists, you then merge your limbo with your deck, and 
// you must keep redrawing until you have an active philosopher.

var field = argument[0]
if(instance_exists(field)){
	if(field.enemy){
		duel_ai_standby_select_philosopher();
	} else {
		duelController.disable_mouse_mode =false
		if(field.active_monster.card_array_size == 0){
			var philo_count = 0;
			for(var i = 0; i < field.hand.card_array_size; i++){
				if(obj_data.card_type[field.hand.card_array[i]] == CARD_TYPE_PHILOSOPHER){
					philo_count++;	
				}
			}
	
			if(philo_count == 0){
		
				if(field.bench.card_array_size != 0){
					// select one from your bench.
					duel_animation_show_textbox("Select a philosopher from your bench as the Active Philosopher.")
					duel_animation_run_script(duel_anim_mouse_start)
					////duel_animation_resume_control()
				}
			} else {
				//tutorial
				duel_animation_display_textbox("tutorial001")
				
				// Select an active philosopher
				duel_animation_show_textbox("Select a philosopher from your hand as the Active Philosopher.")
				duel_animation_run_script(duel_anim_mouse_start)
				//////duel_animation_resume_control()
			}
		} else {
			duel_animation_run_script(duelController_standby_nextphase)	
		}

	}
}