var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _if = map[? "expr"];
	var _then = map[? "return"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_if) &&
		!is_undefined(_then)){
			
		var result = 0;
	
		var result_name = _then
		if(!ds_map_exists(duelController.code_values,result_name)){
			result_name = "return";	
		}

		
		var cond1 = string_copy(_if,1,string_pos(" ",_if)-1);
		_if = string_delete(_if,1,string_pos(" ",_if));
		var cond2 = string_copy(_if,1,string_pos(" ",_if)-1);
		_if = string_delete(_if,1,string_pos(" ",_if));
		var cond3 = _if
		
		
		// prepare the values
		if(string_length(string_digits(cond1)) == string_length(cond1)){
			cond1 = real(cond1);
		} else {
			var value = duelController.code_values[? cond1];
			if(is_undefined(value) ||
				!real(value)){
				value = 0;	
			}
			cond1 = value;
		}
		
		if(string_length(string_digits(cond3)) == string_length(cond3)){
			cond3 = real(cond3);
		} else {
			var value = duelController.code_values[? cond3];
			if(is_undefined(value) ||
				!real(value)){
				value = 0;	
			}
			cond3 = value;
		}
		
		result = code_process_expr(cond1,cond2,cond3);
		
		// set return value to the result
		duelController.code_values[? result_name] = result;
		
		return noone;
	}

}

return noone;