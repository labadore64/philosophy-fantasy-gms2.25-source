var returner = ""
var me = argument[1]
var ind = argument[0]

var ai_counter = -1;
if(argument_count > 2){
	ai_counter = argument[2]
}

if(ind < me.card_array_size){
	if(ai_cond_hand_discard_end(me,ind,ai_counter)){
		returner += duel_create_input_string("discard_end",me,ind) + ",";
	}
	if(ai_cond_hand_discard(me,ind)){
		returner += duel_create_input_string("discard",me,ind) + ",";
	}
	if(ai_cond_hand_activate(me,ind)){
		returner += duel_create_input_string("activate",me,ind) + ",";
	}
	if(ai_cond_hand_overlay_return(me,ind)){
		returner += duel_create_input_string("returnoverlay",me,ind) + ",";
	}
	if(ai_cond_hand_overlay(me,ind)){
		returner += duel_create_input_string("overlay",me,ind) + ",";
	}
	if(ai_cond_hand_bench(me,ind)){
		returner += duel_create_input_string("bench",me,ind) + ",";
	}
}


return returner;