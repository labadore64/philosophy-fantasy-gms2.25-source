var old = menupos;

menupos+=3;

if(menupos >= menu_count){
	menupos = menu_count-1	
}
randomise();
menuParent_sound()

RUMBLE_MOVE_MENU


with(fadeIn){
	instance_destroy()	
}
with(fadeOut){
	instance_destroy()	
}

menuParent_default_accessString()
menuParent_ttsread();

with(MouseHandler){
	ignore_menu = true
	menu_hover = true
}

if(global.menu_animate){
	alpha_adder = 0
}

surface_update = true

// update option_top

if(old != menupos){

	selected_draw_index = menupos % 9;

	if(floor(menupos / 9) != floor(old / 9)){
		page = floor(menupos / 9)
		option_top = page*9
		appearanceModelMap_updateSurface();
	}
	
}
surf_main_update = true