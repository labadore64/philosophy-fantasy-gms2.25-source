

with(parent){
	duelController.speak_tts = false;
	duel_animation_move_card(1,parent.part[DUEL_FIELD_ACTIVE],parent.wait)
	duel_animation_move_card(menupos,id,parent.part[DUEL_FIELD_ACTIVE])
	var mon = card_array[menupos]
	if(mon > -1){
		tts_say(obj_data.card_name[mon] + " Overlayed");	
	}
	duel_animation_wait(20)
	duel_animation_play_sound(sfx_sound_duel_overlay_card)
	duel_animation_activate_card(mon)
	////duel_animation_resume_control()
	parent.overlay_counter++;
}
menu_pop();
instance_destroy()