var obj;

for(var i = 0; i < diagram_length; i++){
	obj = diagrams[|i];
	if(!is_undefined(obj)){
		with(obj){
			if(sprite_index > -1){
				draw_sprite_extended_noratio(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,1)
			}
		}
	}
}