if(!color_selected){
	selected = true
	appearanceColorMap_select(false)	
	//appearanceColorMap_mouseSelect(menupos+1)
	stringer = "";
	if(menupos != 3){
		menupos++;
	} else {
		menupos = 7;	
	}

	if(menupos >= menu_count){
		if(!wrap){
			menupos = menu_count-1;	
		} else {
			menupos = 0;	
		}
	}
	if(menupos > 6){
		selected = false;	
		color_selected = false;
	}
	
	randomise();
	menuParent_sound()

	RUMBLE_MOVE_MENU


	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}

	menuParent_default_accessString()
	menuParent_ttsread();

	with(MouseHandler){
		ignore_menu = true
		menu_hover = true
	}

	if(global.menu_animate){
		alpha_adder = 0
	}

	surface_update = true
	surf_main_update=true
}