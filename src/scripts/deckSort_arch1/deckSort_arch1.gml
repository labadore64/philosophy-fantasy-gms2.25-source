

// sort trunk
var pri = ds_priority_create();
for(var i = 0; i < trunk_total; i++){
	ds_priority_add(pri,trunk_display[i],get_archetype_value(obj_data.card_archetype1[trunk_display[i]]));
}

var list = deck_sort_prioritylist(pri,true)

for(var i = 0; i < trunk_total; i++){
	trunk_cards[i] = list[|i];
}

ds_priority_destroy(pri);

ds_list_destroy(list)

// sort deck
// obj_data.current_deck
var pri = ds_priority_create();
var decksize = ds_list_size(obj_data.current_deck)
for(var i = 0; i < decksize; i++){
	var dd = obj_data.current_deck[|i]
	ds_priority_add(pri,dd,get_archetype_value(obj_data.card_archetype1[dd]));
}

var list = deck_sort_prioritylist(pri,true)

ds_list_clear(obj_data.current_deck)

for(var i = 0; i < decksize; i++){
	ds_list_add(obj_data.current_deck,list[|i]);
}

ds_priority_destroy(pri);

ds_list_destroy(list)