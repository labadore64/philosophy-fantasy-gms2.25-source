draw_set_alpha(1)

draw_clear(portrait_color[CHARA_CUSTOM_COL_BACKGROUND])

if(global.particles){part_system_drawit(Sname);}

if(!surface_exists(surf_main)){
	surf_main = surface_create(512*global.scale_factor,512*global.scale_factor);	
	surf_main_update = true
} else {
	if(ScaleManager.updated){
		surface_resize(surf_main,512*global.scale_factor,512*global.scale_factor)	
		surf_main_update = true
	}
}
		
		
if(surf_main_update){
	surface_set_target(surf_main)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)
	custom_face_draw1(1,0,0,
		portrait_color[CHARA_CUSTOM_COL_HAIR],portrait_color[CHARA_CUSTOM_COL_SKIN],
		portrait_color[CHARA_CUSTOM_COL_SHIRT],portrait_color[CHARA_CUSTOM_COL_NECKLACE],
		portrait_property[CHARA_CUSTOM_VAL_CUTE],
		portrait_property[CHARA_CUSTOM_VAL_BACKHAIR],
		portrait_property[CHARA_CUSTOM_VAL_BODY],
		portrait_property[CHARA_CUSTOM_VAL_NECK],
		portrait_property[CHARA_CUSTOM_VAL_SHIRT],
		portrait_property[CHARA_CUSTOM_VAL_NECKLACE],false)
	custom_face_draw2(1,0,0,
		portrait_color[CHARA_CUSTOM_COL_SKIN],portrait_color[CHARA_CUSTOM_COL_EYE],
		portrait_color[CHARA_CUSTOM_COL_HAIR],portrait_color[CHARA_CUSTOM_COL_MOUTH],
		portrait_property[CHARA_CUSTOM_VAL_FACE],
		portrait_property[CHARA_CUSTOM_VAL_NOSE],
		portrait_property[CHARA_CUSTOM_VAL_MOUTH],
		portrait_property[CHARA_CUSTOM_VAL_EYE],
		portrait_property[CHARA_CUSTOM_VAL_EYEBROW],
		portrait_property[CHARA_CUSTOM_VAL_SNOUT])
	custom_face_draw3(1,0,0,
		portrait_color[CHARA_CUSTOM_COL_HAIR],portrait_color[CHARA_CUSTOM_COL_SKIN],
		portrait_color[CHARA_CUSTOM_COL_EARRING],portrait_color[CHARA_CUSTOM_COL_HAT],
		portrait_color[CHARA_CUSTOM_COL_GLASSES],
		portrait_property[CHARA_CUSTOM_VAL_GLASSES],
		portrait_property[CHARA_CUSTOM_VAL_EAR],
		portrait_property[CHARA_CUSTOM_VAL_EARRING],
		portrait_property[CHARA_CUSTOM_VAL_FRONTHAIR],
		portrait_property[CHARA_CUSTOM_VAL_HAT]);
	gpu_set_colorwriteenable(true,true,true,false)
	surface_reset_target();
	surf_main_update = false;
}

if(!surface_exists(menu_surf)){
	menu_surf = surface_create(global.scale_factor*(800-option_x_offset),global.letterbox_h);	
	menu_surf_update = true
} else {
	if(ScaleManager.updated){
		surface_resize(menu_surf,global.scale_factor*(800-option_x_offset),global.letterbox_h)	
		menu_surf_update = true
	}
}
		
if(menu_surf_update){
	surface_set_target(menu_surf)
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0)
	draw_set_color(c_black)
	draw_set_alpha(.90)
	draw_rectangle_noratio(0,35,
							800-option_x_offset-10,600-10,
							false)
	draw_set_alpha(1)
	draw_set_font(global.largeFont)
	draw_set_halign(fa_center)
	draw_set_color(global.text_fgcolor)
	draw_text_transformed_noratio(630-option_x_offset,50,menu_title,2,2,0)
	
	draw_set_halign(fa_left)

	draw_set_alpha(.85)

	draw_set_color(global.menu_bgcolor)

	draw_rectangle_noratio(760+1-option_x_offset,110,800-20-option_x_offset,545,false)

	draw_set_color(global.text_fgcolor)

	draw_rectangle_noratio(760+1-option_x_offset,110+scroll_increment*(option_top),800-20-option_x_offset,min(545,110+scroll_increment*(option_top+per_line)),false)
	draw_set_alpha(1)
	
	// draw tiles
	var counter = 0;

	for(var i = option_top; i < option_top+per_line; i++){
	
		var size = option_spacer-40
	
		var x_start = 190;
		var y_start = option_y_offset+10;
		var len = 275


		draw_set_color(global.menu_bgcolor)
		draw_set_alpha(.95)
		draw_rectangle_noratio(0+10,option_y_offset+3-10 + option_spacer*counter,
								len+10,option_y_offset-3 + option_spacer*(counter+1),
								false)

		draw_set_alpha(1)
		if(global.high_contrast){
			draw_set_color($AAAAAA)
			draw_rectangle_noratio(x_start-5+10,+y_start-5+option_spacer*counter,
							5+10+x_start+size,5+size+y_start+option_spacer*counter,
							false)
		} else {
			draw_rectangle_color_noratio(x_start+10-5,y_start-5+option_spacer*counter,
							5+10+x_start+size,5+size+y_start+option_spacer*counter,
							$AAAAAA,$7F7F7F,
							$444444,$7F7F7F,
							false)
		}
		counter++;
	}
	draw_set_alpha(1)
	// draw lists
	if(list_selected == 0){
		appearance_draw_model();
	} else {
		appearance_draw_color();	
	}

	// draw buttons
	draw_sprite_extended_noratio(spr_point_arrow,0,520-option_x_offset,70,.75,.75,90,global.text_fgcolor,1)
	draw_sprite_extended_noratio(spr_point_arrow,0,740-option_x_offset,70,.75,.75,270,global.text_fgcolor,1)
	surface_reset_target();
	menu_surf_update = false
}

draw_sprite_extended_ratio(
		spr_faces_cute,portrait_property[CHARA_CUSTOM_VAL_CUTE],
		256,90+256,
		1,1,0,c_white,1)
draw_surface(surf_main,global.display_x,global.display_y+90*global.scale_factor);
if(draw_menu){
	draw_surface(menu_surf,global.display_x+option_x_offset*global.scale_factor,global.display_y)
}

// selection

gpu_set_blendmode(bm_add)

	//draw_set_color(c_white)
	if(global.menu_animate){
		draw_set_alpha(alpha_adder);
	} else {
		draw_set_alpha(alpha_max)
	}
	if(global.high_contrast){
		draw_rectangle_ratio(10+option_x_offset,option_y_offset+3-10 + option_spacer*selected_draw_index,
					option_x_offset+275+10,option_y_offset-3-10 + option_spacer*(selected_draw_index+1),
					false)
	} else {
		draw_rectangle_color_ratio(10+option_x_offset,option_y_offset+3-10 + option_spacer*selected_draw_index,
					option_x_offset+275+10,option_y_offset-3-10 + option_spacer*(selected_draw_index+1),
		c_black,c_black,
		$999999,$999999,
		false)
	}
		
gpu_set_blendmode(bm_normal)

with(fadeIn){
	draw_fade()	
}

with(fadeOut){
	draw_fade()	
}

draw_letterbox();