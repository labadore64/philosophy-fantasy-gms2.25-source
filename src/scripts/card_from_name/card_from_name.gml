var name = string_lower(argument[0]);

for(var i = 0; i < obj_data.cards_total; i++){
	if(string_pos(name,(string_lower(obj_data.card_name[i]))) != 0){
		return i;	
	}
}

return -1;