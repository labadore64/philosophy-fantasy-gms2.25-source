var obj = argument[0]
var index = argument[1];
var text = "";

if(!mouse_check_button(mb_middle)){

	if(index > -1){
		var card_id = obj.card_array[index];
	
		if(card_id > -1){
			if(!obj.card_facedown[index]){
				with(duelController){
					with(tooltip[0]){
						instance_destroy();	
					}
			
					var charperline = 35;
					var stringer = "";
					var type = ""
					var keywords = "";
					if(obj_data.card_type[card_id] == CARD_TYPE_PHILOSOPHER){
						if(obj_data.card_archetype2[card_id] != "None"){
							stringer = obj_data.card_archetype1[card_id] + "/" + obj_data.card_archetype2[card_id]
						} else {
							stringer = obj_data.card_archetype1[card_id]
						}
						stringer += "\n"
						if(obj_data.card_element[card_id] == CARD_ELEMENT_SPIRIT){
							type = "Š"
						} else if(obj_data.card_element[card_id] == CARD_ELEMENT_MIND){
							type = "‹"
						} else {
							type = "™"
						} 
				
						var key  = obj_data.card_keywords[card_id];
						if(key != ""){
							key = string_delete(key,string_length(key),1)
							keywords = "[+]" + string_replace_all(key,",","\n[+]")
				
							keywords += "\n";
						}
					}
	
					text = type + textForceCharsPerLine(obj_data.card_name[card_id],charperline) + "\n" + stringer +
								+ keywords +
								"†" + string(obj_data.card_attack[card_id]) + "/+" + string(obj_data.card_attack_overlay[card_id])
								+" ‡" + string(obj_data.card_defense[card_id]) + "/+" + string(obj_data.card_defense_overlay[card_id])
								+ "\n" + menuBookDiagramReplace(textForceCharsPerLineKeepNewline(obj_data.card_effect_desc[card_id],charperline),-1,-1,true);
				}
			}
		}
	}

	if(obj.object_index == duelDeck ||
		obj.object_index == duelWaiting ||
		obj.object_index == duelDiscard){
		text = obj.source_name + "\n" + "Cards: " + string(obj.card_array_size);	
	}

	if(text != ""){
		var text_width = string_width(text)*2
		var text_heigtht = string_height(text)*2
			
		var xx = duelCursor.x/global.scale_factor - 20 - text_width
		var yy = duelCursor.y/global.scale_factor -20 - text_heigtht - 20
			
		if(xx < 20){
			xx = 20
		} else if(xx+text_width > 800- 20){
			xx = 800 - 20 - text_width
		}
		
		if(global.tooltip_type == 2){
			if(yy < 20){
				yy = 20
			} else if(yy+text_heigtht > 600 -  20){
				yy = 495 - 20 - text_heigtht
			}
		} else {
			if(yy < 20){
				yy = 20
			} else if(yy+text_heigtht > 600 -  20){
				yy = 600 - 20 - text_heigtht
			}
		}
	
		with(duelController){
			tooltip[0] = instance_create(xx,yy,menuTooltip);
			tooltip[0].text = text;
			tooltip[0].height = 20 + text_heigtht;
			tooltip[0].width = 20 + text_width;
	
			tooltip_total = 1;	
		}
	}
}