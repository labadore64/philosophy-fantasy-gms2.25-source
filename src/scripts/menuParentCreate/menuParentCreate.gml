//control
mousepos_x = 0
mousepos_y = 0
// this one ignores if you use the tab keys to switch menus
ignore_tab = false;
// if this uses the generic keys for the menu movement
menu_use_generic = false;

menu_name_array[0] = "";
menu_desc_array[0] = "";
draw_bg_alpha = .45

force_draw_bg = false;

do_not_draw_me = false;

animation_complete = false;
image_speed = 0;
update_image_only_active = true;
drawOtherMenus = true;

menupos = 0; //represents the position of the menu
option_top = 0; //represents the top displayed item in the menu
menu_count = 1; //represents the full size of the menu
wrap = true; //whether or not if you get to the end of the list, if it wraps around.
skip = 10; //how many points in the list to skip if you're skipping through the list
debug = global.debugMenu; //if in debug mode
draw_bg = true; //whether to draw transparent bg

title = "Title"; //title of the menu
subtitle = "Subtitle"; //subtitle of the menu

//other scripts

menu_update_values = -1

cancelled = false;

//drawing variables
x = random_range(200,600)//400;
y = random_range(150,450)//300;

draw_centered = true; //Is the origin in the center of the box, or on the top left? 
draw_width = 300; //width of box in pixels
draw_height = 200; //height of box in pixels

gradient_height = 80; //height of the title
gradient_color1 = c_black; //gradient colour for the title left
gradient_color2 = c_white; //gradient colour for the title right

menu_background_color = c_black // bg color of menu

title_color = c_white; //text color for the title
subtitle_color = c_white; //text color for the subtitle
text_color = c_white; //text color for most of the menu

titleFontSize = 4; //title font size
subtitleFontSize = 2; //subtitle font size
text_size = 2; //size of option text

drawTitle = true; //whether or not to draw title
drawSubtitle = true; //whether or not to draw subtitle
drawInfoText = true; //whether or not to draw info text
drawGradient = true; //whether or not to draw gradient

title_x_offset = 10; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 20 //xoffset for subtitle
subtitle_y_offset = 60; //yoffset for subtitle
option_x_offset = 10 //xoffset for options
option_y_offset = 100; //yoffset for options

option_space = 40; //how many pixels between each option
select_alpha = .25; //alpha of current selection
select_color = c_white; //selection color
select_percent = 1; //how much of the menu does the selection graphic occupy

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = c_white; //color of the title of the info box
info_subtitle_color = c_white; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

//following are specifically for the pop up animation
animation_start = 4 //animation length
animation_len = animation_start; //animation counter
anim_width = 0; //animation width size
anim_height = 0; //animation height size
anim_grad = 0; //animation gradient size

// if not an empty string, opens up the help
menu_help = "";

//following represent the corners of the box
x1 = 0;
x2 = 0;
y1 = 0;
y2 = 0;

//if in debug mode, add these objects to allow stretching the box
stretch[0] = noone; //top left corner
stretch[1] = noone; //bottom right corner
stretch[2] = noone; //gradient height

newalarm[11] = 1;
newalarm[1] = 1;
active = false;

destroy_time = 1;

destroy_script = -1;

clean_script = -1;

ignore_clean_script = false;


// the string to use to read the title
tts_title_string = "";
infokeys_loaded = false;
infokey_script = -1;

// surface for this menu
surf = -1;
surface_update = false;

enable_scrollwheel = true;
scrollwheel_skip = 1; // how many entries to skip with the scroll wheel