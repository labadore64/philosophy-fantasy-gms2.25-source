ax_string_clear();

with(duelSub){
	instance_destroy()	
}
with(menuTooltip){
	instance_destroy()	
}
if(!resume_control){
	if(!instance_exists(animationPhaseTransition)){
	if(selected_object < 0){
		selected_object= last_selected_object
		selected_object.selected_object = last_selected_thingie
	}
	mouse_mode = false;
	if(running_obj < 0 || code_select_active){
		if(active_player == DUEL_PLAYER_YOU || code_select_active){
			if(!duel_camera_moving() || duelController.code_select_active){
				with(selected_object){
					if(menu_left != -1){
						script_execute(menu_left);	
						RUMBLE_MOVE_MENU
					}
				}
				duelController.click_countdown = 2
			
				with(MouseHandler){
					MouseHandler.ignore_menu = true	
				}
			}
		}
		}
	}
}