if(!duelController.ai_mode){
	var time = argument[0]

	var pos = -1;
	var anim = card_activate

	if(argument_count > 1){
		pos = argument[1]
	}

	if(argument_count > 2){
		anim = argument[2]	
	}

	if(!instance_exists(duelController.animation_stack_run) ||
		duelController.animation_stack_run.object_index == animationQueue){
		
		var queue = -1;
		var obj = noone;
	
		// set the queue if the running object is already an animation
		if(instance_exists(duelController.animation_stack_run)){
			if(duelController.animation_stack_run.object_index == animationQueue){
				obj = duelController.animation_stack_run
				queue = obj.queue;
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	
		if(queue > -1 && ds_exists(queue,ds_type_list)){
			duelController.running_obj = obj;

			var json = ds_map_create();
			ds_map_add(json,"x",400)
			ds_map_add(json,"y",300)
			ds_map_add(json,"scale",1)
			ds_map_add(json,"card_id",time)
			ds_map_add(json,"time",120)
			ds_map_add(json,"anim",anim)
		
			if(pos > -1){
				duel_animation_pack(obj,queue,json,duel_anim_queue_show_card,pos)
			} else {
				duel_animation_pack(obj,queue,json,duel_anim_queue_show_card)
			}
		}
	}
}