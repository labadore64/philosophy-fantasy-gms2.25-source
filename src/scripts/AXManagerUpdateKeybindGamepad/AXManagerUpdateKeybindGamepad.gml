/// @function AXManagerUpdateKeybindGamepad()
/// @description Updates the state of the gamepad Keybinds for the AXManager.

// if the gamepad is active
if(global.gamepad > -1){
	var key;
	var state;

	// test the state of all keybound gamepad values
	for(var i = 0; i < global.gamepad_size; i++){
		key = global.gamepad_value_array[i];

		// updates the states of the gamepad 
		if (gamepad_button_check_pressed(global.gamepad,key)){
			state = KEY_STATE_PRESS
		}
		else if(gamepad_button_check(global.gamepad,key)){
			state = KEY_STATE_HOLD
		} else if (gamepad_button_check_released(global.gamepad,key)){
			state = KEY_STATE_RELEASE
		} else {
			state = KEY_STATE_NONE
		}
		
		global.gamepad_state_array[i] = state;

	}
	
	objGenericDoGamepad();
}