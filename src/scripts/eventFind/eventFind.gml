var compare = argument[0];

if(compare == "none"){
	return -1;	
}

with(obj_data){
	for(var i = 0; i < event_total; i++){
		if(event_id[i] == compare){
			return i;
		}
	}
}

return -1;