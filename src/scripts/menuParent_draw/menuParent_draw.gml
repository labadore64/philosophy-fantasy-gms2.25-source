// first make the menu surface

if(!surface_exists(surf)){
	surf = surface_create(menu_width*global.scale_factor,menu_height*global.scale_factor);	
	surface_update = true
} else {
	if(ScaleManager.updated){
		surface_resize(surf,menu_width*global.scale_factor,menu_height*global.scale_factor)	
		surface_update = true
	}
}

if(surface_update){
	surface_set_target(surf);
	
	if(menu_draw_surface != -1){
		script_execute(menu_draw_surface);	
	}
	
	surface_reset_target();
}

draw_surface(surf,global.display_x+x*global.scale_factor,global.display_x+y*global.scale_factor)

