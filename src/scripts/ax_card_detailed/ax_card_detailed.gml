
for(var i = 0; i < 10; i++){
	access_string[i] = "";
}

if(menupos == 0){
	if(argument[0] > -1){
		access_string[0] = obj_data.card_name[argument[0]];
		access_string[1] = obj_data.card_effect_desc[argument[0]]
		
		// set alt text for diagramss
		for(var i = 0; i < objdata_diagram.data_size; i++){
			access_string[1]=string_replace_all(access_string[1],"{{"+objdata_diagram.name[i]+"}}",objdata_diagram.alt_text[i])
		}

		var stringer = "";
		if(obj_data.card_type[argument[0]] == CARD_TYPE_PHILOSOPHER){
			if(obj_data.card_archetype2[argument[0]] != ""){
				stringer = obj_data.card_archetype1[argument[0]] + ", " + obj_data.card_archetype2[argument[0]] + " archetypes"
			} else {
				stringer = obj_data.card_archetype1[argument[0]] + " archetype"
			}
		} else if(obj_data.card_type[argument[0]] == CARD_TYPE_EFFECT){
			stringer = "praxis"	
		} else {
			stringer = "theory"	
		}
	
		access_string[2] = stringer;
	
		access_string[3] = obj_data.card_bio[argument[0]];
		access_string[4] = obj_data.card_years[argument[0]] + " years alive"
	}
} else if (menupos == 1){
	if(argument[0] > -1){
		access_string[0] = string(obj_data.card_attack[argument[0]]) + " attack "
		access_string[1] = string(obj_data.card_attack_overlay[argument[0]]) + " overlay attack"
		access_string[2] = string(obj_data.card_defense[argument[0]]) + " defense "
		access_string[3] = string(obj_data.card_defense_overlay[argument[0]]) + " overlay defense"
	
		var stringer = "";
		if(obj_data.card_type[argument[0]] == CARD_TYPE_PHILOSOPHER){
			if(obj_data.card_archetype2[argument[0]] != ""){
				stringer = obj_data.card_archetype1[argument[0]] + ", " + obj_data.card_archetype2[argument[0]] + " archetypes"
			} else {
				stringer = obj_data.card_archetype1[argument[0]] + " archetype"
			}
		} else if(obj_data.card_type[argument[0]] == CARD_TYPE_EFFECT){
			stringer = "praxis"	
		} else {
			stringer = "theory"	
		}
	
		access_string[4] = stringer;
	
		stringer = "mind"
		var stringstrong = "matter"
		var stringweak = "spirit"
		if(obj_data.card_element[argument[0]] == CARD_ELEMENT_MATTER){
			stringer = "matter"	
			stringstrong = "spirit"
			stringweak = "mind"
		} else if(obj_data.card_element[argument[0]] == CARD_ELEMENT_SPIRIT){
			stringer = "spirit"
			stringstrong = "mind"
			stringweak = "matter"
		}
	
		access_string[5] = stringer + " Type"
		access_string[6] = stringweak + " Weak"
		access_string[7] = stringstrong + " Resist"
	}
} else {
	access_string[0] = menu_name[menupos]	
	access_string[1] = menu_desc[menupos]	
}