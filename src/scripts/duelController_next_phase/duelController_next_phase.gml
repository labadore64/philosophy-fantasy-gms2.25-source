if(!duelController.complete){
	if(!instance_exists(duelController.running_obj)){
		with(cardInfo){
			menu_pop()
			instance_destroy()
		}
		with(duelSub){
			menu_pop()
			instance_destroy()
		}
		
		if(duelController.phase_current == DUEL_PHASE_END && field[0].hand.card_array_size > 5){
			duelCamera_center_camera_special()
			duel_animation_show_textbox("Discard from your hand until you only have 5 cards remaining.")
		} else {
		
			with(duelController){
				draw_phase_change = true;
				phase_current++;
				// skip the standby phase now.
				if(phase_current == DUEL_PHASE_STANDBY){
					phase_current++;	
				}
				// end the turn if its the first turn instead of doing battle
				if(turns == 0 && phase_current == DUEL_PHASE_BATTLE){
					phase_current = array_length_1d(phase_name)-1;
				}
				
				do_return_overlay = false;
				if(array_length_1d(phase_name) <= phase_current){
					duel_start_turn();
				}
			
		
				if(phase_current == DUEL_PHASE_DRAW){
					duelCamera_center_camera_special()
				}
	
			}
		
			var obj = instance_create(0,0,animationPhaseTransition);
			obj.timer = 120

			duelController.running_obj = obj;
		}
		
		with(duelController){
			if(pop_menu_active){
				duelController_pop_menu();	
			}
		}
	}
}