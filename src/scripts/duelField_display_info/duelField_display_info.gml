if(!surface_exists(info_surf)){
	info_surf = surface_create_access(global.letterbox_w,115*global.scale_factor)	
	info_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(info_surf,global.letterbox_w,115*global.scale_factor);
		info_surf_update = true;
	}
}
gpu_set_colorwriteenable(true,true,true,true)
if(info_surf_update){
	surface_set_target(info_surf)
	draw_clear_alpha(c_black,0)

	if(card_array_size > 0){
		if(!card_facedown[menupos]){
			if(card_array[menupos] > -1){
				draw_set_color(global.text_fgcolor)
				draw_set_alpha(1)
				draw_text_transformed_noratio(30,0 ,
									obj_data.card_name[card_array[menupos]],4,4,0)
					
				var index = 3;
				if(obj_data.card_type[card_array[menupos]] == CARD_TYPE_PHILOSOPHER){
					if(obj_data.card_element[card_array[menupos]] == CARD_ELEMENT_MIND){
						index = 2
					} else if(obj_data.card_element[card_array[menupos]] == CARD_ELEMENT_MATTER){
						index = 1
					} else {
						index = 0;	
					}
				} else if(obj_data.card_type[card_array[menupos]] == CARD_TYPE_METAPHYS){
					index = 4
				}
		
				draw_sprite_extended_noratio(spr_card_white_type,index,
										30+15,70+5 ,
										1.2,1.2,0,global.text_fgcolor,1)
		
				if(obj_data.card_type[card_array[menupos]] == CARD_TYPE_PHILOSOPHER){
					draw_sprite_extended_noratio(spr_card_white_stat,0,
											30+90,70+5 ,
											1,1,0,global.text_fgcolor,1)
									
					draw_sprite_extended_noratio(spr_card_white_stat,1,
											30+230,70+5 ,
											1,1,0,global.text_fgcolor,1)
									
					draw_text_transformed_noratio(30+120,60+5 ,
										string(obj_data.card_attack[card_array[menupos]]) + "/+"
										+ string(obj_data.card_attack_overlay[card_array[menupos]])
										,2,2,0)
								
					draw_text_transformed_noratio(30+260,60+5 ,
										string(obj_data.card_defense[card_array[menupos]]) + "/+"
										+ string(obj_data.card_defense_overlay[card_array[menupos]])
										,2,2,0)
				}
			}
		}
	}
	draw_set_halign(fa_right)
	draw_set_color(global.text_fgcolor)
	draw_text_transformed_noratio(780,45,source_name,3,3,0)
	draw_set_halign(fa_left)
	
	surface_reset_target()
	info_surf_update = false;
}

draw_surface(info_surf,global.display_x + -5*global.scale_factor,global.display_y + 495*global.scale_factor);