var _card = argument[0];
var _loc = argument[1];
var _this = argument[2]

if(!(card_has_keyword(_card,"once-per-turn") && obj_data.card_used_this_turn[_card] > 0)){

	var codeblock = code_find_block(_card,_loc)
	
	if(codeblock > -1){
					
		// test if the condition is true
					
		duelController.code_start = 0;
		duelController.code_pointer = 0;
		duelController.code_end = false
		var condition = code_find_block(_card,"condition");
					
		var specific_condition = code_find_block(_card,"condition_"+_loc)
					
		if(condition > -1){
			code_set_thisdirect(_this);
			code_set_card_id(_card);
			//code_default_values()
			code_load(obj_data.card_code[_card,condition]);	
			code_execute(true);
			var value = duelController.code_values[? "return"];
						
			if(!is_undefined(value)){
				if(value){
					condition = -1;	
				}
			}
			code_pop_this();
		}
		
		if(specific_condition > -1 && condition < 0){
			duelController.code_start = 0;
			duelController.code_pointer = 0;
			duelController.code_end = false
			
			code_set_thisdirect(_this);
			code_set_card_id(_card);
			//code_default_values()
			code_load(obj_data.card_code[_card,specific_condition]);	
			code_execute(true);
			code_pop_this();
			var value = duelController.code_values[? "return"];
						
			if(!is_undefined(value)){
				if(value){
					condition = -1;	
				}
			}
		}

		return condition < 0;
	}
}
return false;