

if(menupos > card_array_size){
	menupos = card_array_size-1;	
}

if(menupos < 0){
	menupos = 0	
}
var startx = 50
var spacer = 75;
	
var posy = 340

var do_camera = true;

with(drawMoveCard){
	do_camera = move_camera;	
}

if(do_camera){
	if(enemy){
		posy = -95
		startx = 50 + 150
		if(card_array_size == 0){
			startx = 135
		}
	
		duel_hand_update_cursor_player(startx,posy,-spacer);
	} else {
		if(card_array_size == 0){
			startx = 135
		}
	
		duel_hand_update_cursor_player(startx,posy,spacer);
	}
}