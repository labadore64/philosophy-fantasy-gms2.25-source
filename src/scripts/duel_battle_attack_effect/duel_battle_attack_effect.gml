// argument0 field
// argument1 card

var field = argument[0]
var card = argument[1]

var card_show = false;

if(instance_exists(field)){
	if(card > -1){

		// if inflicted damage inflicts stun status to the enemy.
		if(card_has_keyword(argument[1], "stun")){
			
			// if inflicted positive damage
			if(total_damage > 0){
				if(!card_show){
					duel_battle_showcard(card);
				}
				card_show = true;
				
				var enemy = field.parent.enemy;
				var newfield = duelController.field[!enemy];

				// to do: add graphic for stun effect

				duel_status_set(newfield,"stun",2);
				if(!duelController.ai_mode){
					duel_animation_wait(20)
				}

			}
		}

		// if card has knockback, send the overlay to limbo
		// if no overlay, send the active card.
		if(card_has_keyword(argument[1], "knockback")){
			
			// if inflicted positive damage
			if(total_damage > 0){
				if(!card_show){
					duel_battle_showcard(card);
				}
				card_show = true;
				
				var enemy = field.parent.enemy;
				var newfield = duelController.field[!enemy];
			
				duel_animation_move_card(newfield.active_monster.card_array_size-1,newfield.active_monster,newfield.wait);
			
				duel_animation_shuffle(newfield.deck)
				if(!duelController.ai_mode){
					duel_animation_wait(20)
				}
				
			}
		}
			
		// if card has return, return this card to the deck
		if(card_has_keyword(argument[1],"return")){
			if(!duelController.ai_mode){
				if(!card_show){
					duel_battle_showcard(card);
				}
			}
			
			card_show = true;
			
			repeat(field.parent.part[DUEL_FIELD_BENCH].card_array_size-1){
				duel_animation_move_card(1,field,field.parent.wait);
			}
			duel_animation_move_card(0,field,field.parent.part[DUEL_FIELD_DECK]);
			duel_animation_shuffle(field.parent.part[DUEL_FIELD_DECK])
			if(!duelController.ai_mode){
				duel_animation_wait(20)
			}
		}
	}
}