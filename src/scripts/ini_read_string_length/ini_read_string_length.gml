/// @function ini_read_string_length(section, key, default, maxsize)
/// @description Imports with INI but restricts the maximum size of the import.

return string_copy(ini_read_string(argument[0],argument[1],argument[2]),1,argument[3]);