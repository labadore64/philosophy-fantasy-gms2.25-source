with(parent){
	script_execute(menu_draw)	
}

var yyy = 40
draw_set_color(c_black)
draw_set_alpha(1)
draw_rectangle_ratio(0,0,800,yyy +draw_size+20,false)

var dist = 250;


draw_set_font(global.normalFont)
draw_set_halign(fa_center)
draw_set_color(global.text_fgcolor)

if(show_gamepad){
	var counter = 0;
	var column = 0;
	var xxx = 0;
	draw_text_transformed_ratio(400,10,"Controller",3,3,0)
	
	for(var i = 0; i < def_count; i++){
		xxx = (400-dist) + dist*counter;
		
		draw_text_transformed_ratio(xxx,yyy +y+20+column*60,gamepad_name[i],2,2,0)
		draw_text_transformed_ratio(xxx,yyy +y+40+column*60,gamepad_chara[i],2,2,0)
		
		counter++;
		if(counter == 3){
			counter = 0;
			column++;
		}
	}
} else {
	var counter = 0;
	var column = 0;
	var xxx = 0;
	draw_text_transformed_ratio(400,10,"Keyboard",3,3,0)
	for(var i = 0; i < def_count; i++){
		xxx = (400-dist) + dist*counter;
		
		draw_text_transformed_ratio(xxx,yyy +y+20+column*60,key_name[i],2,2,0)
		draw_text_transformed_ratio(xxx,yyy +y+40+column*60,key_chara[i],2,2,0)
		
		counter++;
		if(counter == 3){
			counter = 0;
			column++;
		}
	}	
}

draw_set_halign(fa_left)