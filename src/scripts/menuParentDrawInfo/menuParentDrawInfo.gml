

	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_color(info_bg_color)

	draw_rectangle_ratio(0,info_height,800,600,false)

	draw_set_halign(fa_center)

	draw_set_color(info_title_color);
	draw_text_transformed_ratio(400,info_height+info_title_y_offset,menu_name_array[ menupos],info_title_size,info_title_size,0)

	draw_set_color(info_subtitle_color)
	draw_text_transformed_ratio(400,info_height+info_subtitle_y_offset,menu_desc_array[ menupos],info_subtitle_size,info_subtitle_size,0)
	gpu_set_colorwriteenable(true,true,true,true)

draw_set_halign(fa_left);