if(argument[0] == CHARA_CUSTOM_COL_EARRING){
	if(!property_enabled[CHARA_CUSTOM_VAL_EARRING]){
		return false	
	}
} else if(argument[0] == CHARA_CUSTOM_COL_HAT){
	if(!property_enabled[CHARA_CUSTOM_VAL_HAT]){
		return false
	}
} else if(argument[0] == CHARA_CUSTOM_COL_NECKLACE){
	if(!property_enabled[CHARA_CUSTOM_VAL_NECKLACE]){
		return false
	}
} else if(argument[0] == CHARA_CUSTOM_COL_SNOUT){
	if(!property_enabled[CHARA_CUSTOM_VAL_SNOUT]){
		return false
	}
}

return true