if(!keypress_this_frame){
	var mousex = window_mouse_get_x()
	var mousey = window_mouse_get_y()
	if(mouse_check_button(mb_left)){
		for(var i =0; i < 2; i++){
		if(mousex >= MouseHandler.mouse_points_trans[i,0] && 
			mousex <= MouseHandler.mouse_points_trans[i,2] &&
			mousey >= MouseHandler.mouse_points_trans[i,1] && 
			mousey <= MouseHandler.mouse_points_trans[i,3])
			{
				script_execute(mouse_script[i],
							(mousex-MouseHandler.mouse_points_trans[i,0])/
								(MouseHandler.mouse_points_trans[i,2]-MouseHandler.mouse_points_trans[i,0]),
							(mousey-MouseHandler.mouse_points_trans[i,1])/
								(MouseHandler.mouse_points_trans[i,3]-MouseHandler.mouse_points_trans[i,1]))
				if(parent_update > -1){
					script_execute(parent_update);	
				}
				surf_main_update = true
				
				if(global.extra_sounds){
					if(i == 1){
						if(!audio_exists(sound[0]) || mouse_selected != i){
							for(var j = 0; j < 2; j++){
								if(audio_exists(sound[j])){
									audio_stop_sound(sound[j])	
								}
							}
							sound[0] = soundfxPlayLoop(sfx_sound_col_hue)
							sound_released = false
							color_selected = true
						}
						if(!audio_exists(sound[1])){
							sound[1] = soundfxPlayLoop(sfx_sound_col_sat)
							sound_released = false
							color_selected = true
						}
						audio_sound_pitch(sound[0], col_pos[0]+.5)
						audio_sound_pitch(sound[1], col_pos[1]+.5)
					} else if(i == 0){
						if(!audio_exists(sound[0]) || mouse_selected != i){
							for(var j = 0; j < 2; j++){
								if(audio_exists(sound[j])){
									audio_stop_sound(sound[j])	
								}
							}
							sound[0] = soundfxPlayLoop(sfx_sound_col_val)
							sound_released = false
						}
						sound[1] = -1;
						audio_sound_pitch(sound[0], col_pos[2]+.5)
					}
					mouse_selected = i
				}

			}
		}
	} else if (mouse_check_button_released(mb_left)){
		appearanceColorMap_release();
	} else {
		if(!sound_released){
			if(!keyboard_check(global.keybind_value_array[KEYBOARD_KEY_LEFT]) &&
				!keyboard_check(global.keybind_value_array[KEYBOARD_KEY_RIGHT]) &&
				!keyboard_check(global.keybind_value_array[KEYBOARD_KEY_UP]) &&
				!keyboard_check(global.keybind_value_array[KEYBOARD_KEY_DOWN])){
				appearanceColorMap_release()
			}	
		}
	}
	
	if(mouse_check_button_pressed(mb_left)){
		if(global.extra_sounds){
			for(var i = 0; i < 2; i++){
				if(audio_exists(sound[i])){
					audio_stop_sound(sound[i])	
				}
			}
		}
	
		for(var i =2; i < 8; i++){
		
		if(mousex >= MouseHandler.mouse_points_trans[i,0] && 
			mousex <= MouseHandler.mouse_points_trans[i,2] &&
			mousey >= MouseHandler.mouse_points_trans[i,1] && 
			mousey <= MouseHandler.mouse_points_trans[i,3])
			{
				//if(selected){
				//	appearanceColorMap_select()
				//	break;
				//} else {
					appearanceColorMap_mouseSelect(i)
					break;
				//}
			} else {
				selected=false
				menupos = 0
			}
		}
	}
}