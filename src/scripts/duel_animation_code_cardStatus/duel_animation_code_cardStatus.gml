var status = argument[0]
var arg = argument[1]
var target = argument[2]
var quantity = argument[3]
if(!instance_exists(duelController.animation_stack_run) ||
	duelController.animation_stack_run.object_index == animationQueue){
		
	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	
	if(queue > -1 && ds_exists(queue,ds_type_list)){
		duelController.running_obj = obj;
	
		var json = ds_map_create();
		ds_map_add(json,"status",status);
		ds_map_add(json,"arg",arg);
		ds_map_add(json,"target",target);
		ds_map_add(json,"quantity",quantity);
		var map_id = get_timer();
		ds_map_replace(obj.args,map_id,json_encode(json))
		ds_map_replace(obj.arg_to_script,map_id,duel_anim_queue_code_cardStatus)
		ds_map_destroy(json);
		
		duelController_add_anim_to_queue(queue,map_id)	
	}
}
