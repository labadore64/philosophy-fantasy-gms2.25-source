argument[1] = string_lower(argument[1]);

// will always ignore entries with blanks.
// these are used to organize items during development
// and will be removed later...
if(argument[1] != ""){
	with(argument[0]){
		var len = array_length_1d(name);
	
		for(var i = 0; i < len; i++){
			if(string_lower(name[i]) == argument[1]){
				return i;	
			}
		}
	}
}
return -1;