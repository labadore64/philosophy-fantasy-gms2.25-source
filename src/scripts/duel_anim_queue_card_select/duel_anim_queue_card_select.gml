var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _src = map[? "source"];
	var _cond = map[? "cond"];
	var _turn = map[? "turn"]
	
	ds_map_destroy(map)
	
	if(!is_undefined(_src) 
		&& !is_undefined(_cond)
		&& !is_undefined(_turn)){
			
		// empty source array
		with(duelController){
			code_select_location = -1;
			code_select_location[0] = "";
			code_select_location_size = 0
				
			var fullstring = _src
				
			var stringlen = string_length(fullstring)+1;
			var counter = string_pos("(",fullstring)+1
			var charat = string_char_at(fullstring,counter);
			var lastcharat = "";
			var literal = false;
		var counterz = 0;
			while(counter < stringlen && charat != ")"){
				// if the character is " switch the literalness
				if(charat == "\""){
					literal = !literal
				}
		
				if(charat == "," && !literal){
					code_select_location_size++;
					code_select_location[code_select_location_size] = ""
				} else if (lastcharat == "\\"){
					//delete escape
					code_select_location[code_select_location_size] = string_delete(code_select_location[code_select_location_size],
														string_length(code_select_location[code_select_location_size])-1,
														1);
					if(charat == "\""){
						code_select_location[code_select_location_size] += "\"";	
					}
				} else {
					code_select_location[code_select_location_size] += charat;
				}
		
				counter++
				lastcharat = charat;
				charat = string_char_at(fullstring,counter);
				
				counterz++;
				if(counterz > 1000000){
					break;	
				}
			}
			code_select_location_size++;
				
			// get this
			var this = code_get_this();
			var this_enemy = false;
			if(string_pos("pos_enemy",this) > 0){
				this_enemy = true;
			}
			var target_side = DUEL_PLAYER_YOU
				
			// now that code is populated, parse into locations
			for(var i = 0; i < code_select_location_size; i++){
				var stringe = code_select_location[i]
				var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
				stringe = string_delete(stringe,1, string_pos(":",stringe));
				var target = string_copy(stringe,1, string_pos(":",stringe)-1);
					
				if(target == "enemy"){
					if(!this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}
				} else {
					if(this_enemy){
						target_side = DUEL_PLAYER_ENEMY
					} else {
						target_side = DUEL_PLAYER_YOU
					}	
				}
					
				code_select_location[i] = code_get_location(ori,target_side);
			}
		}
		
		// split the source into a list of sources
		
		with(duelController){
			code_select_query = _cond
		}
			
			
		if(global.dont_record){
			// check what the argument on the next input is
			// then delete it
			
			var input = duelinput_take(0);
			// calculate the index
			while(string_pos(":",input) > 0){
				var poss = string_pos(":",input)
				input = string_copy(input,poss+1,string_length(input)-poss);
			}
			
			input = string_digits(input);
			
			if(input == ""){
				input = "0";	
			}
			
			var ind = real(input)
			
			if(duelController.code_select_location[0] > 0){
				var cardee = duelController.code_select_location[0].card_array[ind];
				duel_animation_show_selected(cardee,0)
				duelinput_insert("cardselect",duelController.code_select_location[0],ind);
				duelinput_execute(duelinput_length()-1);
			} else {
				show_debug_message("that's not supposed to happen");	
			}
		} else {
			if(_turn == DUEL_PLAYER_YOU){
				// this object will be deleted when you make a selection.
				var obj = instance_create(0,0,animationSelect);
		
				with(duelController){
					code_select_active = true
				}
		
				return obj;
			} else {
				// use AI to make a selection from the possible sources.	
				var location = duelController.code_select_location[0];
				var le_card = coolrandom_inext(location.card_array_size-1);
				var counter = 0;
				while(counter < 5000 && !code_card_query(location.card_array[le_card],_cond)){
					le_card = coolrandom_inext(location.card_array_size-1);
					counter++;	
				}
				
				duel_animation_show_selected(location.card_array[le_card],0)
				duelinput_insert("cardselect",location,le_card);
				duelinput_execute(duelinput_length()-1);
			
			}
		}
	}

}

return noone;