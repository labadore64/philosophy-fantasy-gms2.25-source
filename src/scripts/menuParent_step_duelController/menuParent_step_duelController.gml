if(!lock){
if(textbox == noone){

	if(alpha_adder < alpha_max){
		surface_update=true
		alpha_adder += ScaleManager.timer_diff*alpha_rate	
		if(alpha_adder > alpha_max){
			alpha_adder = alpha_max	
		}
	}
	
	if(menuControl.exit_obj < 0 &&
		(object_index == menuHelp || menuControl.help_obj < 0)){
		objGenericStepMain();
		
		if(!keypress_this_frame){
			if(global.inputEnabled){
					if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_KEYDISPLAY]) ||
						global.gamepad_state_array[KEYBOARD_KEY_KEYDISPLAY] == KEY_STATE_PRESS) {
							keypress_this_frame = true;
							if(object_index == menuKeysDisplay){
								menuParent_cancel();	
							} else {
								menuKeysDisplay_show();
							}
					}
			}
		}
		
		if(objGenericCanDo()){
			if(!keypress_this_frame){
				if(global.drawing_enabled){
				if(mouse_check_button_pressed(mb_right)){
					if(!MouseHandler.clicked){
						if(menu_cancel > -1){
							script_execute(menu_cancel)	
							keypress_this_frame = true;
							with(MouseHandler){
								clicked = true
								clicked_countdown = 5	
							}
						}
					}
				} 

				}
			}
		}
		
		if(!keypress_this_frame){
			mouseHandler_process()
		}
	}
}
}