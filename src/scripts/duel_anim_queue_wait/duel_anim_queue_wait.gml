var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _time = map[? "time"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_time)){
			
		var obj = instance_create(0,0,animationTimer);
		obj.timer = _time;
		
		return obj;
	}

}

return noone;