var field = argument[0];
var flag = 0
if(argument_count > 1){
	flag = argument[1]	
}
if(instance_exists(field)){
if(!instance_exists(duelController.animation_stack_run) ||
	duelController.animation_stack_run.object_index == animationQueue){
		
	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	
	if(queue > -1 && ds_exists(queue,ds_type_list)){
		duelController.running_obj = obj;
	
		var size = field.wait.card_array_size;

		if(size > 0){
			duel_animation_display_textbox("tutorial002")	
		}

		var counter = 0;

		for(var i = size-1; i >= 0; i--){
			if(!card_has_keyword(field.wait.card_array[i],"permadeath")){
				var json = ds_map_create();
				ds_map_add(json,"origin",field.wait)
				ds_map_add(json,"dest",field.deck)
				ds_map_add(json,"card_id",i)
				ds_map_add(json,"time",7)
				ds_map_add(json,"facedown",false);
				ds_map_add(json,"movecamera",false)
				ds_map_add(json,"block",false)
				ds_map_add(json,"flag",flag);
				counter++;
				duel_animation_pack(obj,queue,json,duel_anim_queue_move_card)
			}
		}
	
		if(counter > 0){
			duel_animation_shuffle(field.deck)
		}
	}
}
}
