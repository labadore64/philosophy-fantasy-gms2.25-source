
if(!instance_exists(duelController.animation_stack_run) ||
	duelController.animation_stack_run.object_index == animationQueue){
	var modi = 0;
			
	if(global.tooltip_type == TOOLTIP_TYPE_ONLY){
		modi = 135
	}

	duel_animation_move_camera(350,290-modi,5,10);
		
	var queue = -1;
	var obj = noone;
	
	// set the queue if the running object is already an animation
	if(instance_exists(duelController.animation_stack_run)){
		if(duelController.animation_stack_run.object_index == animationQueue){
			obj = duelController.animation_stack_run
			queue = obj.queue;
		} else {
			queue = ds_list_create();
			obj = instance_create(0,0,animationQueue);
			obj.queue = queue;
		}
	} else {
		queue = ds_list_create();
		obj = instance_create(0,0,animationQueue);
		obj.queue = queue;
	}
	
	if(queue > -1 && ds_exists(queue,ds_type_list)){
		duelController.running_obj = obj;
		var msg = ""
		if(duelController.duel_hp[0].hp_current <= 0 && duelController.duel_hp[0].hp_current){

			msg = "Draw!"
			obj_data.last_game_state = 2
			duelController.game_complete = true;
			obj_data.draws++;
			obj_data.savescum = false
		} else if(duelController.duel_hp[1].hp_current <= 0){
			duel_animation_play_sound(sfx_sound_duel_victory)
			msg = "Win!"
			obj_data.last_game_state = 1
			duelController.game_complete = true;
			obj_data.wins++;
			if(obj_data.story_duel){
				obj_data.loaded_cutscene_count++;
				obj_data.story_duel = false;
				obj_data.story_counter++;
			}
		
			obj_data.savescum = false
		} else if(duelController.duel_hp[0].hp_current <= 0){
			duel_animation_play_sound(sfx_sound_duel_defeat)
			msg = "Lose!"
			obj_data.last_game_state = 0
			duelController.game_complete = true;
			obj_data.losses++;
			obj_data.savescum = false
		} 

		var json = ds_map_create();
		ds_map_add(json,"msg",msg)
		
		duel_animation_pack(obj,queue,json,duel_anim_queue_display_victory)
	}
}
