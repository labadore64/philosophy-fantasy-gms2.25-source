// should be in all scripts
var index = argument[0];
var origin = argument[1];
	
tts_stop();

duelController.duel_hp[0].hp_current = 0;

duelController.test_victory = true;

with(duelController){
	if(pop_menu_active){
		duelController_pop_menu();	
	}
}

return noone;
