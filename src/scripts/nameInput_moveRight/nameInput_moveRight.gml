if(selected_part == NAMEINPUT_STATE_NAME){
	// do name modification stuff ( usually used for changing the pointer)
	insert_pos++
	if(insert_pos > current_name_length){
		insert_pos = current_name_length;	
	} else {
		menuParent_sound()
	}
} else if (selected_part == NAMEINPUT_STATE_CONTROLS){
	// do control stuff (confirm, backspace, clear ect)
	if(menupos == lengy+1 ||
		menupos == lengy+3){
		menupos++;		
	}
	menuParent_sound()
} else {
	// do alpha stuff
	if(selected_part == NAMEINPUT_STATE_ALPHABET){
		alpha_menupos = menupos	
	}
	
	menupos++;

	if(menupos >= menu_count){
		if(!wrap){
			menupos = menu_count-1;	
		} else {
			menupos = 0;	
		}
	}
	randomise();
	menuParent_sound()

	RUMBLE_MOVE_MENU


	with(fadeIn){
		instance_destroy()	
	}
	with(fadeOut){
		instance_destroy()	
	}
	
	with(MouseHandler){
		ignore_menu = true	
	}

	if(global.menu_animate){
		alpha_adder = 0
	}

	surface_update = true

}
nameInput_updateMove()

if(selected_part == NAMEINPUT_STATE_NAME){
	if(insert_pos < current_name_length){
		tts_clear();
		tts_say(name[insert_pos]);	
	}
}