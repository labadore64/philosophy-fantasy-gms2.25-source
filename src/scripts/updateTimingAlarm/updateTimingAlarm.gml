#macro TIMER_INACTIVE -10000000

for(var i = 0; i < 12; i++){
	if(newalarm[i] > TIMER_INACTIVE){
		newalarm[i] -= ScaleManager.timer_diff;	
		if(newalarm[i] < 0){
			event_perform(ev_alarm,i);
			if(newalarm[i] < 0){
				newalarm[i] = TIMER_INACTIVE;
			}
		}
	}
}
spr_counter += ScaleManager.timer_diff*image_speed*.5;
alarm_done = true;
