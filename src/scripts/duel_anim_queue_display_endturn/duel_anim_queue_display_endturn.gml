var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _msg = map[? "msg"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_msg)){
		var name = duelController.duel_hp[!duelController.active_player].name
		var obj = instance_create(0,0,animationEndTurnMessage);
		
		obj.message = name+"'s\nTurn"
		tts_say(_msg);
		with(obj){
			mouseHandler_clear()
			mouseHandler_add(0,0,800,600,duel_anim_message_continue,message)	
		}
		return obj;
	}

}

return noone;