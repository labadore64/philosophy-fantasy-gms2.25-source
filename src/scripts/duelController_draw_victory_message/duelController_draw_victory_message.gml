draw_set_font(global.largeFont)
draw_set_halign(fa_center)
draw_set_valign(fa_middle)
draw_set_color(c_black)
draw_set_alpha(image_alpha)

if(global.menu_animate){
	draw_text_transformed_ratio(400,300,message,
								sizer,sizer,0)
	draw_set_color(c_white)
	draw_text_transformed_ratio(400,300,message,
								sizer,sizer,0)
} else {
	draw_text_transformed_ratio(400,300,message,
								3,3,0)
	draw_set_color(c_white)
	draw_text_transformed_ratio(400,300,message,
								3,3,0)	
}
draw_set_halign(fa_left)
draw_set_valign(fa_top)
draw_set_alpha(1)