var space = 20;
var space2 = 3;
gpu_set_texfilter(false);
gpu_set_colorwriteenable(true,true,true,true)
draw_set_alpha(1)
if(global.high_contrast){
	draw_clear(merge_color(image_blend,c_black,.5))
} else {
	draw_sprite_extended_noratio(spr_card_front,0,0,0,1,1,0,image_blend,1)
}
		
gpu_set_colorwriteenable(true,true,true,false)
draw_set_color($33AAAA)
		
draw_rectangle_noratio(0,0,400,space,false)
draw_rectangle_noratio(0,600-space,400,600,false)
draw_rectangle_noratio(400-space,0,400,600,false)
draw_rectangle_noratio(0,0,space,600,false)

draw_set_alpha(1)
			
// Draw sprite
				
if(card_sprite_ref > -1){
	draw_sprite_extended_noratio(card_sprite_ref,0,40,70,1.25,1.25,0,c_white,1);
}
draw_set_color(c_black)
		
draw_rectangle_noratio(40,70,360,70+space2,false)
draw_rectangle_noratio(40,70,40+space2,390,false)
draw_rectangle_noratio(40,390-space2,360,390,false)
draw_rectangle_noratio(360-space2,70,360,390,false)
			
// draw element
draw_set_color(c_black)
draw_circle_noratio(400-85,115,45,false)
draw_rectangle_noratio(400-85-45,70,360,115,false)
if(global.high_contrast){
	var index = 3;
	if(obj_data.card_type[card_id] == CARD_TYPE_PHILOSOPHER){
		if(obj_data.card_element[card_id] == CARD_ELEMENT_MIND){
			index = 2
		} else if(obj_data.card_element[card_id] == CARD_ELEMENT_MATTER){
			index = 1
		} else {
			index = 0;	
		}
	}
	draw_sprite_extended_noratio(spr_card_white_type,index,400-85,115,2,2,0,c_white,1);
} else {
	draw_sprite_extended_noratio(obj_data.type_sprite[obj_data.card_element[card_id]],0,400-85,115,2,2,0,c_white,1);	
}
// draw gradients
if(global.high_contrast){
	draw_set_alpha(.45)
} else {
	draw_set_alpha(.3)
}
// name and infoboxes
draw_rectangle_noratio(43-3,30-3,357+3,60+3,false)
draw_rectangle_noratio(43-3,400-3,357+3,510+3,false)
draw_rectangle_noratio(43-3,520-3,357+3,560+3,false)
				
draw_rectangle_noratio(43,30,357,60,false)
draw_rectangle_noratio(43,400,357,510,false)
draw_rectangle_noratio(43,520,357,560,false)
				
// stats
if(global.high_contrast){
	draw_set_alpha(1)
} else {
	draw_set_alpha(.8)
}
draw_rectangle_noratio(40,340,360,390,false)
// draw text
				
draw_set_alpha(1)
draw_set_color(c_white)
draw_set_font(global.normalFont)

var display_text = name_display;
while(string_width(display_text)*2 > 300){
	display_text = string_copy(display_text,1,string_length(display_text)-1)	
}
draw_text_transformed_noratio(50,38-5,display_text,2,2,0)
				
draw_set_halign(fa_right)
draw_text_transformed_noratio(300,533-5,"Resist:",2,2,0)
draw_set_halign(fa_left)
draw_text_transformed_noratio(50,533-5,"Weak:",2,2,0)
				
var index = 0;
if(obj_data.type_weak[obj_data.card_element[card_id]] == CARD_ELEMENT_MATTER){
	index = 1;	
} else if (obj_data.type_weak[obj_data.card_element[card_id]] == CARD_ELEMENT_MIND){
	index = 2;	
}

// TYPE WEAK AND RESIST
if(global.high_contrast){
	draw_sprite_extended_noratio(spr_card_white_type,index,160,540,1.5,1.5,0,c_white,1);
} else {
	draw_sprite_extended_noratio(spr_card_white_type,index,160,540,1.5,1.5,0,c_white,.5);
}

var index = 0;
if(obj_data.type_resist[obj_data.card_element[card_id]] == CARD_ELEMENT_MATTER){
	index = 1;	
} else if (obj_data.type_resist[obj_data.card_element[card_id]] == CARD_ELEMENT_MIND){
	index = 2;	
}
if(global.high_contrast){
	draw_sprite_extended_noratio(spr_card_white_type,index,325,540,1.5,1.5,0,c_white,1);
} else {
	draw_sprite_extended_noratio(spr_card_white_type,index,325,540,1.5,1.5,0,c_white,.5);
}
//type_weak[CARD_ELEMENT_MIND] 
				
// stats
				
// draw base stats.
var att_x = 85;
var def_x = 245;
var stat_h = 360
				
if(global.high_contrast){
	draw_set_alpha(1)
	draw_sprite_extended_noratio(spr_card_white_stat,0,att_x,stat_h+3,1.20,1.20,0,c_white,1);
	draw_sprite_extended_noratio(spr_card_white_stat,1,def_x,stat_h+3,1.20,1.20,0,c_white,1);
} else {
	draw_set_alpha(.65)
	draw_sprite_extended_noratio(spr_card_stat,0,att_x,stat_h+3,1.20,1.20,0,c_white,.5);
	draw_sprite_extended_noratio(spr_card_stat,1,def_x,stat_h+3,1.20,1.20,0,c_white,.5);
}
			
draw_text_transformed_noratio(att_x+30,stat_h-5-3,string(obj_data.card_attack[card_id]) + "/+" + string(obj_data.card_attack_overlay[card_id]),2,2,0)
draw_text_transformed_noratio(def_x+30,stat_h-5-3,string(obj_data.card_defense[card_id]) + "/+" + string(obj_data.card_defense_overlay[card_id]),2,2,0)
	
att_x = 140;
def_x = 290;
stat_h = 440
	
// draw current stats. if there is an overlay the stats are different.
if(text_display_mode == CARD_DISPLAY_MODE_NORMAL){
	draw_set_alpha(1)
	if(global.high_contrast){
		draw_sprite_extended_noratio(spr_card_white_stat,0,att_x-45,stat_h+20,1.75,1.75,0,c_white,1);
		draw_sprite_extended_noratio(spr_card_white_stat,1,def_x-45,stat_h+20,1.75,1.75,0,c_white,1);
	} else {
		draw_sprite_extended_noratio(spr_card_stat,0,att_x-45,stat_h+20,1.75,1.75,0,c_white,1);
		draw_sprite_extended_noratio(spr_card_stat,1,def_x-45,stat_h+20,1.75,1.75,0,c_white,1);
	}
				
	draw_set_font(global.largeFont)

	draw_text_transformed_noratio(att_x,stat_h-5,string(card_attack_power),2,2,0)
	draw_text_transformed_noratio(def_x,stat_h-5,string(card_defense_power),2,2,0)
} else if(text_display_mode == CARD_DISPLAY_MODE_ATTACK){
	
	att_x = 200
	
	draw_set_alpha(1)
				
	if(global.high_contrast){
		draw_sprite_extended_noratio(spr_card_white_stat,0,att_x-45,stat_h+20,1.75,1.75,0,c_white,1);
	} else {
		draw_sprite_extended_noratio(spr_card_stat,0,att_x-45,stat_h+20,1.75,1.75,0,c_white,1);
	
	}
				
	draw_set_font(global.largeFont)

	draw_text_transformed_noratio(att_x+20,stat_h-5,string(card_attack_power),3,3,0)
} else if (text_display_mode == CARD_DISPLAY_MODE_DEFENSE){
	def_x = 200
	
	draw_set_alpha(1)
	if(global.high_contrast){
		draw_sprite_extended_noratio(spr_card_white_stat,1,def_x-45,stat_h+20,1.75,1.75,0,c_white,1);
	} else {
		draw_sprite_extended_noratio(spr_card_stat,1,def_x-45,stat_h+20,1.75,1.75,0,c_white,1);
	}
				
	draw_set_font(global.largeFont)

	draw_text_transformed_noratio(def_x+20,stat_h-5,string(card_defense_power),3,3,0)	
}
