if(global.tutorial){
	var teee = instance_create_depth(0,30,0,TextBox);
	var me = noone
	with(duelController){
		textbox = teee;	
		me = id;
	}
	teee.text_id = argument[0]
	
	if(global.text_full && room != BattleRoom){
		teee.char_per_line = 30
	} else {
		teee.char_per_line = 50
	}
	
	teee.parent_lock = me
	if(argument_count > 1){
		teee.name = argument[1]	
	}
	var destro = false;
	with(teee){
		event_perform(ev_alarm,2)	
		destro = destroyed
	}
	if(destro){
		with(duelController){
			textbox = noone
		}
	}
	return teee;
}

return noone;