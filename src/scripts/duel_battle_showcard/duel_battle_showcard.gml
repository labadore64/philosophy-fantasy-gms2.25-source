if(!duelController.ai_mode){
	var sfx = -1;
	var anim = card_activate
	if(argument_count > 1){
		sfx = argument[1]	
	}
	if(argument_count > 2){
		anim = argument[2]	
	}

	duel_animation_wait(20)
	if(sfx > -1){
		duel_animation_play_sound(sfx)
	}
	duel_animation_activate_card(argument[0],-1,anim)

	var timer = 20;

	if(anim == card_flash){
		timer = 50;	
	}

	duel_animation_wait(timer)

	if(duelController.phase_current != DUEL_PHASE_BATTLE){
		//duelCamera_center_camera_special()
	}
}