var menu = argument[0];
if(instance_exists(menu)){
	if(object_is_ancestor(menu.object_index,menuParent) ||
		menu.object_index == menuParent){
		with(menuControl){
			if(ds_exists(menu_stack,ds_type_stack)){
				ds_stack_push(menu_stack,menu);
				active_menu = menu;
				if(!instance_exists(TextBox)){
					with(active_menu){
						if(menu_mouse_activate != -1){
							script_execute(menu_mouse_activate)	
							event_user(0)
						}
				
					}
				}
			}
		}
		with(menu){
			event_user(1)	
			// add the cancel button and other special buttons last.
			//event_user(2)	
		}
	}
}