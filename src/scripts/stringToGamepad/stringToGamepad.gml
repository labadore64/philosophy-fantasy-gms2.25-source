/// @function stringToGamepad(name)
/// @description Turns a string into a gamepad button
/// @param {string} name The name of the button.

	if(is_undefined(argument[0])){
		argument[0] = ""
	}

		switch(argument[0]){
			
			case "Button1":
			return gp_face1;
		
			
			case "Button2":
			return gp_face2;
	
			
			case "Button3":
			return gp_face3;
	
			
			case "Button4":
			return gp_face4;
	
			
			case "D-Down":
			return gp_padd;
	
			
			case "D-Left":
			return gp_padl;
	
			
			case "D-Right":
			return gp_padr;
	
			
			case "D-Up":
			return gp_padu;
	
			case "Start":
			return gp_start;
	
			case "Select":
			return gp_select;
	
			case "L-Shoulder 1":
			return gp_shoulderl;
	
			case "L-Shoulder 2":
			return gp_shoulderlb;
	
			case "R-Shoulder 1":
			return gp_shoulderr;
	
			case "R-Shoulder 2":
			return gp_shoulderrb;
	
			case "L-Stick":
			return gp_stickl;
	
			case "R-Stick":
			return gp_stickr;
		}
		
	
