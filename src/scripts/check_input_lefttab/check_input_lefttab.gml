if(menu_tableft != -1){
	if(global.inputEnabled){
		if(keyboard_check_pressed(global.keybind_value_array[KEYBOARD_KEY_TABLEFT]) ||
			global.gamepad_state_array[KEYBOARD_KEY_TABLEFT] == KEY_STATE_PRESS){
			script_execute(menu_tableft);
			keypress_this_frame = true;
		}
	}
}  