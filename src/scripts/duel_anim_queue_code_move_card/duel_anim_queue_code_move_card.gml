var argu = argument[0]; // args as json string

var map = json_decode(argu);
var target_index = -1; // the card index to move
var target_origin = noone; // the location of the target
var target_side = DUEL_PLAYER_YOU

var cando = false;

var move_all = false;
if(argument_count > 1){
	cando = true
	
	var targe= argument[0]
	var block = argument[1]
	var target_destination = argument[2]
	var this = code_get_this();
	var this_enemy = false;
	var flag=  argument[3]
	if(string_pos("pos_enemy",this) > 0){
		this_enemy = true;
	}
} else {
	if(!ds_map_empty(map)){
		// do your normal code down here

		var targe= map[? "target"];
		var block = map[? "block"];
		var target_destination = map[? "dest"];
		var this = code_get_this();
		var this_enemy = false;
		var flag=  map[? "flag"]
		if(string_pos("pos_enemy",this) > 0){
			this_enemy = true;
		}
	
		if(!is_undefined(targe) &&
			!is_undefined(target_destination) &&
			!is_undefined(flag) &&
			!is_undefined(block)){
			
				cando = true
		
			}
	
		}
	ds_map_destroy(map)
}
		
if(cando){
	var targe_variable = duelController.code_values[? targe];
	if(!is_undefined(targe_variable)){
		targe = targe_variable	
	}

		if(targe != ""){
		
		// parse target
		// if target is this just use the character stored in this

		var is_this = false;
		if(targe == "this"){
			targe = this
			is_this = true;
		}

		var stringe = targe;
		var ori = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
		var side = string_copy(stringe,1, string_pos(":",stringe)-1);
		stringe = string_delete(stringe,1, string_pos(":",stringe));
			
		//ori = string_copy(stringe,string_pos(":",ori)+1, string_length(ori)-string_pos(":",ori));
		
		if(is_this){
			if(this_enemy){
				target_side = DUEL_PLAYER_ENEMY	
			} else {
				target_side = DUEL_PLAYER_YOU	
			}
		} else {
			if(side == "enemy"){
				if(!this_enemy){
					target_side = DUEL_PLAYER_ENEMY
				} else {
					target_side = DUEL_PLAYER_YOU
				}
			} else {
				if(this_enemy){
					target_side = DUEL_PLAYER_ENEMY
				} else {
					target_side = DUEL_PLAYER_YOU
				}	
			}
		}
			
		if(side == "pos_enemy"){
			target_side = DUEL_PLAYER_ENEMY	
		} else if (side == "pos_you"){
			target_side = DUEL_PLAYER_YOU
		}
			
		// if the code is a location, parse as location.
		if(code_is_location(ori)){
				
			if( string_length(string_digits(stringe)) == string_length(stringe)){
				
				target_index = string_copy(stringe,string_pos(":",stringe)+1,
										string_length(stringe) - string_pos(":",stringe))

				// string == variable
				if(string_length(string_digits(target_index)) != string_length(target_index)){
					var value = duelController.code_values[? target_index];
					if(is_undefined(value) || !is_real(value)){
						value = 0;	
					}
				
					target_index = value;
				} else {
					target_index = real(target_index);	
				}
				
				if(ori == "overlay"){
					target_index++;	
				}
			} else {
				if(stringe == "all"){
					// loop through all targets.	
					move_all = true;
				} else {
					// check if variable	
					// get number from variables
					if(ds_map_exists(duelController.code_values,stringe)){
						var def = duelController.code_values[? stringe]
						if(!is_undefined(def)){
							target_index = real(def);	
						}
					}
				}
			}
				
			target_origin = code_get_location(ori,target_side)
		} else {
			// otherwise, track the search terms of the card.
			// note: you're going to need to update how you track this location
			// after doing this... damb.
				
			// anyways target_index is going to contain a list of targets
			// listed in brackets :) like so:
			// cardMove("target:[type:mind+name:Deleuze+archetype:psychoanalysis]","dest:hand");
		}

		target_destination = code_get_location(target_destination,target_side)

					
		// if deck
		if(target_origin.object_index == duelDeck){
				flag +=power(2,ANIMATION_MOVE_FLAG_CARD_SHUFFLE)
		}
		
		var facedown = false;
			
		if(target_origin.enemy){
			if(target_destination.object_index == duelHand ||
				target_destination.object_index == duelDeck){
				facedown = true;		
			}
		}
		
		if(move_all){
			var i = 0;
			duel_animation_wait(3)
			// do card move	
			for(i = 0; i < target_origin.card_array_size; i++){
				duel_animation_move_card(i,target_origin,target_destination,facedown,block);
			}
					
	
			// update this position
			
			with(duelController){
				// the current card
				if(is_this ){
					//code_pop_this();
					//code_set_this(target_destination,target_destination.card_array_size+1);	
					code_set_card_id(target_origin.card_array[i]);
				}
			}
		} else {
			var enm = "you";
			if(target_origin.enemy){
				enm = "enemy"	
			}
			duel_animation_wait(3)
			
			duel_animation_move_card(target_index,target_origin,target_destination,facedown,block,flag);
			// do card move		
	
			// update this position
				
			with(duelController){
				// the current card
				if(is_this ){
					//code_pop_this();
					//code_set_this(target_destination,target_destination.card_array_size+1);	
					code_set_card_id(target_origin.card_array[target_index]);
				}
			}
		}
			
		return noone;
	}

}

return noone;