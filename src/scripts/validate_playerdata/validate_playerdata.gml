

var map = argument[0];

if(ds_exists(map,ds_type_map)){
	if(ds_map_size(map) == 2){
		if(ds_map_exists(map,"val") &&
			ds_map_exists(map,"col")){
		
			var list1 = map[? "val"];
			var list2 = map[? "col"];
		
			if(ds_exists(list1,ds_type_list)
				&& ds_exists(list2,ds_type_list)){
			
				if(ds_list_size(list1) == TOTAL_CHARA_PARTS &&
					ds_list_size(list2) == TOTAL_CHARA_COLORS){
			
					var cando = true;
					var val = 0;
					for(var i = 0; i < TOTAL_CHARA_PARTS; i++){
						val = list1[|i];
						if(is_undefined(val)){
							cando = false;
							break;
						}
						
						if(is_string(val)){
							cando = false;
							break;
						}
						
						if(val > 99){
							break;	
						}
					}
					
					for(var i = 0; i < TOTAL_CHARA_COLORS; i++){
						val = list2[|i];
						if(is_undefined(val)){
							cando = false;
							break;
						}
						
						if(is_string(val)){
							cando = false;
							break;
						}
					}
				
					return cando;
				}
			}
		}
	}
}

return false