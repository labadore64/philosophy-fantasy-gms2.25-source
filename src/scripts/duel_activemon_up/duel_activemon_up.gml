menupos++
if(menupos > card_array_size-1){
	// do stuff here;
	menupos = card_array_size-1;
	
	// switch sides!
	
	with(duelController){
		if(selected_object == field[0]){
			selected_object = field[1]
		} else {
			selected_object = field[0];	
		}
		var obj = selected_object.selected_object;
		var size = 1;
		
		if(instance_exists(obj)){
		
			size = obj.card_array_size;
	
			for(var i = 0; i < obj.card_array_size; i++){
				if(obj.card_array[i] == -1){
					size = i;
					break;
				}
			}
	
			if(size < 1){
				size = 1	
			}
		
			selected_object.active_monster.menupos = size-1
		}
		selected_object.active_monster.menupos = size-1
		duelField_selectObject(selected_object,DUEL_FIELD_ACTIVE)
	}
	
} else {
	duel_activemon_activate();
}

duelField_tts_read();
duelField_tts_part_read()