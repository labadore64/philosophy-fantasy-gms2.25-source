// should be in all scripts
var index = argument[1];
var origin = argument[0];

with(origin){
	
	duel_animation_move_card(index,id,parent.wait)

	duel_animation_card_effect(card_array[index],"discard",code_set_cardstate(parent.wait,parent.wait.card_array_size-1));

}