
var field1 = duelController.field[argument[0]].ai_field;
var field2 = duelController.field[!argument[0]].ai_field;

var card_att, card_def;

for(var i = 0; i < 3; i++){
	card_att[i] = field1.part[DUEL_FIELD_ACTIVE].card_array[i];
	card_def[i] = field2.part[DUEL_FIELD_ACTIVE].card_array[i];
}

var attacking = 0;
var defending = 0;

if(card_att[0] > -1){
	attacking = obj_data.card_attack[card_att[0]] + field1.att_boost
}
if(card_def[0] > -1){
	defending = obj_data.card_defense[card_def[0]] + field2.def_boost
}

for(var i = 1; i < 3; i++){
	if(card_att[i] > -1){
		attacking += obj_data.card_attack_overlay[card_att[i]]
	}
	if(card_def[i] > -1){
		defending += obj_data.card_defense_overlay[card_def[i]]
	}
}

var total_damage = attacking - defending;

// check type

if(card_att[0] > -1 && card_def[0] > -1 &&
	!card_has_keyword(card_att[0],"neutral") && !card_has_keyword(card_def[0],"neutral")){

	var type1 = obj_data.card_element[card_att[0]]
	var type2 = obj_data.card_element[card_def[0]]

	// if the attacking type is strong against the defending type
	if(obj_data.type_weak[type2] == type1){
		total_damage += 1;	
	}

	// if the defending type is strong against the attacking type
	if(obj_data.type_weak[type1] == type2){
		total_damage -= 1;	
	}
}

duel_battle_attack_effect(field1.part[DUEL_FIELD_ACTIVE],card_att[0])	
duel_battle_defend_effect(field2.part[DUEL_FIELD_ACTIVE],card_def[0])	

if(total_damage > 0){
	field1.current_hp_enemy-=total_damage;	
	field2.current_hp-=total_damage
} else {
	field1.current_hp+=total_damage;	
	field2.current_hp_enemy+=total_damage;
}

// scripts
duelController_complete_battle_func(field1,field2);