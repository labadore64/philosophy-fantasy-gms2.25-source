
duelist_name = ""
duelist_sprite = -1
duelist_deck = 0;
duelist_say1 = ""
duelist_say2 = ""
duelist_bg_image = ""

var itemcount = obj_data.loaded_character[obj_data.loaded_cutscene_counter]
var loaded_duel = 0

// gets the count of the items
var filename = working_directory + "resources\\opponent\\enemy";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

if(file_exists(testname)){
	ini_open(testname)
	
	duelist_name = ini_read_string_length("main","name","",IMPORT_MAX_SIZE_CARDNAME)
	duelist_sprite = asset_get_index("spr_portrait_" + ini_read_string_length("main","sprite","",IMPORT_MAX_SIZE_SPRITE))
	
	if(obj_data.free_duel){
		duelist_say1 = ini_read_string_length("duel"+string(loaded_duel),"free_start","",50)
		duelist_say2 = ini_read_string_length("duel"+string(loaded_duel),"free_win","",50)
		duelist_say3 = ini_read_string_length("duel"+string(loaded_duel),"free_lose","",50)	
	} else {
		duelist_say1 = ini_read_string_length("duel"+string(loaded_duel),"say_start","",50)
		duelist_say2 = ini_read_string_length("duel"+string(loaded_duel),"say_win","",50)
		duelist_say3 = ini_read_string_length("duel"+string(loaded_duel),"say_lose","",50)
	}
	
	challenge_music = asset_get_index("sfx_music_"+ ini_read_string_length("duel"+string(loaded_duel),"challenge_music","",IMPORT_MAX_SIZE_SPRITE))
	obj_data.duel_music = asset_get_index("sfx_music_"+ ini_read_string_length("duel"+string(loaded_duel),"music","",IMPORT_MAX_SIZE_SPRITE))
	
	fave = card_from_name(ini_read_string_length("duel"+string(loaded_duel),"fave","",IMPORT_MAX_SIZE_CARDNAME))
	event_id = ini_read_string_length("duel"+string(loaded_duel),"event","",IMPORT_MAX_SIZE_EVENT)
	duelist_deck = ini_read_real("duel"+string(loaded_duel),"deck",0)
	duelist_bg_image = "bg_" + ini_read_string_length("duel"+string(loaded_duel),"bg","",IMPORT_MAX_SIZE_SPRITE)
	
	var spritename = working_directory + "resources\\bg\\" + duelist_bg_image  + ".png";
	duelist_img_sprite =  sprite_add(spritename, 0, false, false, 0, 0)


	
	ini_close();
}