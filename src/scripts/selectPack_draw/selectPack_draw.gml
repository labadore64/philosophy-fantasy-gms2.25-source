deckBuild_drawBackground()

if(!global.high_contrast){
	draw_set_alpha(.05)
	draw_set_color(c_white)
	gpu_set_blendmode(bm_add)
	draw_rectangle_ratio(0,65,800,470,false)
	gpu_set_blendmode(bm_normal)
}
draw_set_color(c_black)
draw_set_alpha(1)
draw_rectangle_ratio(0,0,800,50,false)
draw_set_alpha(1)
if(global.color_gradient){
	draw_rectangle_color_ratio(0,485,800,600,
								global.text_bgcolor[0],global.text_bgcolor[0],
								global.text_bgcolor[1],global.text_bgcolor[1],
								false)
} else {
	draw_set_color(global.text_bgcolor[0])
	draw_rectangle_ratio(0,485,800,600,
								false)
}
draw_set_color(global.text_fgcolor)
draw_set_font(global.normalFont)
draw_set_halign(fa_center)
draw_text_transformed_ratio(400,15,"Select a Booster Pack.",2,2,0)
draw_text_transformed_ratio(400,500,pack_name,3,3,0)
draw_text_transformed_ratio(400,535,pack_desc,2,2,0)
draw_set_halign(fa_left)

if(animate <= -1){
	pack_draw(pack_display[1],50,y,$7F7F7F,.75)
	pack_draw(pack_display[2],300,y,c_white,1)
	pack_draw(pack_display[3],550,y,$7F7F7F,.75)
} else {
	for(var i =0; i < 5; i++){
		pack_draw(pack_display[i],animate_x+50+250*(i-1),y,$7F7F7F,.75)	
	}
}

with(fadeIn){
	draw_fade()	
}
with(fadeOut){
	draw_fade()	
}


draw_letterbox();