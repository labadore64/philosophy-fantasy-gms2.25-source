var returner = ""
var me = argument[1]
var ind = argument[0]

if(ind < me.card_array_size){
	if(ai_cond_bench_overlay_return(me,ind)){
		returner += duel_create_input_string("returnoverlay",me,ind) + ",";
	}
	if(ai_cond_bench_overlay(me,ind)){
		returner += duel_create_input_string("overlay",me,ind) + ",";
	}
	if(ai_cond_bench_switch(me,ind)){
		returner += duel_create_input_string("swap",me,ind) + ",";
	}
	if(ai_cond_bench_activate(me,ind)){
		returner += duel_create_input_string("activate",me,ind) + ",";
	}
}


return returner;