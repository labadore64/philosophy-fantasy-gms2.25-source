var target_string = argument[0]
var status = argument[1]

with(duelController){
	var size = ds_map_size(card_status)
	var key = ds_map_find_first(card_status)
	for (var i = 0; i < size; i++)
	{
		if(!is_undefined(key)){
			if(string_pos(status,key) > 0){
				//var value = card_status[? key]
			
				if(string_pos(target_string,key) > 0){
					return key
				}

				key = ds_map_find_next(card_status, key);
			}
		}
	}
}

return "";