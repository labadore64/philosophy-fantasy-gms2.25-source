// use this to set the options from the map

var drawing_old = global.drawing_enabled
var rumble_old = global.gamepadVibration
var rumble2_old = global.gamepadVibrationAmount;
var fastmode_changed = global.fastmode;

if(ds_map_exists(options_map,"Text-to-Speech")){
	global.voice =  options_map[? "Text-to-Speech"]
}
if(ds_map_exists(options_map,"Enable Graphics")){
	global.drawing_enabled =  options_map[? "Enable Graphics"]
}
if(ds_map_exists(options_map,"Enable Shaders")){
	global.enableShader =  options_map[? "Enable Shaders"]
}
if(ds_map_exists(options_map,"Fullscreen")){
	global.fullscreen =  options_map[? "Fullscreen"]
}
if(ds_map_exists(options_map,"High Contrast")){
	global.high_contrast =  options_map[? "High Contrast"]
}
if(ds_map_exists(options_map,"SFX")){
	global.sfxVolume =  options_map[? "SFX"] * .1
}
if(ds_map_exists(options_map,"Music")){
	global.musicVolume =  options_map[? "Music"] * .1
}
if(ds_map_exists(options_map,"Stick Axis")){
	global.gamepadRightAxis =  options_map[? "Stick Axis"]
}
if(ds_map_exists(options_map,"Vibration")){
	global.gamepadVibration =  options_map[? "Vibration"]
}
if(ds_map_exists(options_map,"TTS Details")){
	global.speak_details = options_map[? "TTS Details"]
}
if(ds_map_exists(options_map,"Fast Battles")){
	global.fastmode = options_map[? "Fast Battles"]
}
if(ds_map_exists(options_map,"Menu Side")){
	global.mouse_right = options_map[? "Menu Side"]
}
if(ds_map_exists(options_map,"Pan Speed")){
	global.pan_speed = options_map[? "Pan Speed"]+1
}
if(ds_map_exists(options_map,"Zoom Speed")){
	global.zoom_speed = options_map[? "Zoom Speed"]+1
}
if(ds_map_exists(options_map,"Mouse")){
	global.mouse_active = options_map[? "Mouse"]
}
if(ds_map_exists(options_map,"Difficulty")){
	global.difficulty = options_map[? "Difficulty"]
}
if(ds_map_exists(options_map,"Skip Games")){
	global.skip_duel = options_map[? "Skip Games"]
}
if(ds_map_exists(options_map,"Animate Menus")){
	global.menu_animate = options_map[? "Animate Menus"]
}
if(ds_map_exists(options_map,"Particles")){
	global.particles = options_map[? "Particles"]
}
if(ds_map_exists(options_map,"Vibration Rate")){
	global.gamepadVibrationAmount = options_map[? "Vibration Rate"]*.1
}
if(ds_map_exists(options_map,"Show Health")){
	global.display_hp = options_map[? "Show Health"]
}
if(ds_map_exists(options_map,"Card Icon")){
	global.card_icon = options_map[? "Card Icon"]
}
if(ds_map_exists(options_map,"Skip Intro")){
	global.skip_splash = options_map[? "Skip Intro"]
}
if(ds_map_exists(options_map,"Coin Toss")){
	global.coin_flip = options_map[? "Coin Toss"]
}
if(ds_map_exists(options_map,"Menu Sound")){
	global.menu_sounds = options_map[? "Menu Sound"]
}
if(ds_map_exists(options_map,"Positional Pitch")){
	global.positional_pitch = options_map[? "Positional Pitch"]
}
if(ds_map_exists(options_map,"Menu Background")){
	var val = options_map[? "Menu Background"]
	if(val < array_length_2d(option_text,6) && val > -1){
		var txt = string_replace_all(string_lower(option_text[6,val])," ","_");
		
		global.menu_index = asset_get_index("background_img_" + txt)
	}
}
if(ds_map_exists(options_map,"Text Sound")){
	global.text_sound = options_map[? "Text Sound"]
}
if(ds_map_exists(options_map,"Press to Continue")){
	global.press_continue = options_map[? "Press to Continue"]
}
if(ds_map_exists(options_map,"Health Sound")){
	global.hp_sound = options_map[? "Health Sound"]
}
if(ds_map_exists(options_map,"Cursor Direction")){
	global.reverse_pointer = options_map[? "Cursor Direction"]
}
if(ds_map_exists(options_map,"Auto Text")){
	global.text_auto = options_map[? "Auto Text"]
}
if(ds_map_exists(options_map,"Fullsize Text")){
	global.text_full = options_map[? "Fullsize Text"]
}
if(ds_map_exists(options_map,"Navigation Sounds")){
	global.extra_sounds = options_map[? "Navigation Sounds"]
}
if(ds_map_exists(options_map,"Pause Animations")){
	global.pause_anim = options_map[? "Pause Animations"]
}
if(ds_map_exists(options_map,"Frame Rate")){
	game_set_speed((options_map[? "Frame Rate"]+1)*10,gamespeed_fps)
}
if(ds_map_exists(options_map,"Replay")){
	global.replay = options_map[? "Replay"]
}
if(ds_map_exists(options_map,"Tooltips")){
	global.tooltip_type = options_map[? "Tooltips"]	
}

if(!global.drawing_enabled &&
	global.drawing_enabled != drawing_old){
	global.voice = true;
	if(ds_map_exists(options_map,"Text-to-Speech")){
		options_map[? "Text-to-Speech"] = true;
	}
	tts_say("Text to Speech enabled.")
}

if(global.gamepadVibration &&
	(rumble_old != global.gamepadVibration ||
		rumble2_old != global.gamepadVibrationAmount)){
	gamepad_rumble(1,15);		
}

if(fastmode_changed != global.fastmode){
	with(duelController){
		if(global.fastmode){
			fast = 2	
		} else {
			fast = 1.5	
		}
	}
}