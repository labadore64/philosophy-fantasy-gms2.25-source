if(!surface_exists(info_surf)){
	info_surf = surface_create_access(global.letterbox_w,115*global.scale_factor)	
	info_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(info_surf,global.letterbox_w,115*global.scale_factor);
		info_surf_update = true;
	}
}
gpu_set_colorwriteenable(true,true,true,true)
if(info_surf_update){
	surface_set_target(info_surf)
	draw_clear_alpha(c_black,0)
	
	draw_set_alpha(1)

	draw_set_color(global.text_fgcolor)

	draw_set_halign(fa_right)
	draw_text_transformed_noratio(780,45,source_name,3,3,0)
	
	draw_text_transformed_noratio(775,80,"Cards:"+string(card_array_size),2,2,0)
	draw_set_halign(fa_left)
	
	surface_reset_target()
	info_surf_update = false;
}

draw_surface(info_surf,global.display_x + -5*global.scale_factor,global.display_y + 495*global.scale_factor);