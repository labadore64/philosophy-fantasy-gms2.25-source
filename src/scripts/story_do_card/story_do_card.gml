
var map = argument[0]
var name = argument[1]

var cando = true;

var pack_id = 0;

global.story_next = "";
val = map[? "next"]
if(!is_undefined(val)){
	global.story_next = string_lower(val)	
}

val = map[? "pack"]
if(!is_undefined(val)){
	pack_id = real(val);
}

val = map[? "once"]
if(!is_undefined(val)){
	if(string_lower(val) == "true"){
		if(eventCheck(name)){
			obj.duel_id = ""
			cando = false
		}
	}
}

val = map[? "next_room"]
if(!is_undefined(val)){
	val = string_lower(val)
	if(val == "this"){
		global.goto_map = room	
	} else if(val == "last"){
		global.goto_map = room_last	
	}  else if(val == "main"){
		global.goto_map = MainRoom
	} else {
		var test = asset_get_index(val);
		if(room_exists(test)){
			global.goto_map = test	
		}
	}
}

if(cando){

	// test
	var pack = generateCardsForPack(pack_id)
	global.selected_pack_id = pack_id;
	global.selected_pack = pack;
	var obj = instance_create(0,0,PackList);

} else {
	// stops infinite loops
	if(global.story_next == name){
		global.story_next = ""
	}
	instance_destroy(cando);	
}