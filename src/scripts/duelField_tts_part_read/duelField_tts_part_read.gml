
	with(duelController){
		if(access_string[0] = ""){
			duelField_access_update(0)
		}
		
		if(can_tts){
			if(instance_exists(selected_object) && instance_exists(selected_object.selected_object)){
				var charaname = "Your";
	
				if(selected_object.enemy){
					charaname = duel_hp[1].name +"'s";	
				}
	
				if(selected_object.selected_object.object_index == duelActive){
					if(selected_object.selected_object.menupos == 0 ||
						selected_object.selected_object.card_array_size == 0){
						tts_say(charaname + " " + selected_object.selected_object.source_name)	
					} else {
						tts_say(charaname + " " + "Overlay")	
					}
				} else {
					tts_say(charaname + " " + selected_object.selected_object.source_name)
				}
			}
		}
	}
