var counter = 0;
for(var i = 0; i < color_total; i++){
	
	if(appearance_check_color_enabled(i)){
		color_count[counter] = i
		color_title[counter] = color_names[i] // name of the model element (eyes, mouth ect)
		color_id[counter] = portrait_color[i]
		color_hex[counter] = dec_to_hex(color_get_red(portrait_color[i])) +
						dec_to_hex(color_get_green(portrait_color[i])) +
						dec_to_hex(color_get_blue(portrait_color[i]))
		color_rgb[counter] = string(round(color_get_red(portrait_color[i]))) + ", " +
						string(round(color_get_green(portrait_color[i]))) + ", " +
						string(round(color_get_blue(portrait_color[i])))
		color_hsv[counter] = string(round(color_get_hue(portrait_color[i]))) + ", " +
						string(round(color_get_saturation(portrait_color[i]))) + ", " +
						string(round(color_get_value(portrait_color[i])))
		counter++
	}
}

color_title_total = counter;