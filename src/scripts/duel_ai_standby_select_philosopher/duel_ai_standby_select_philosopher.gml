duelCamera_center_camera()
if(duelController.field[1].active_monster.card_array_size == 0){
	var container = duelController.field[1].bench;
	
	if(duelController.field[1].bench.card_array_size == 0){
		container = duelController.field[1].hand;	
	}
	
	// randomly select a card
	var selected_menupos = coolrandom_inext(container.card_array_size-1)
	var selected_card = container.card_array[selected_menupos]
	
	// if not in dipshit AI mode pick the highest attack
	if(min(global.difficulty,global.max_ai) > 0){
		var selected_card = -1;
		var selected_menupos = 0;
		var max_att = 0;
	
		// cycle through the cards and get the highest attack.
		for(var i = 0; i < container.card_array_size; i++){
			if(duel_can_activate_philosopher(container.card_array[i],duelController.field[1].active_monster.card_array[0])){
				if(obj_data.card_attack[container.card_array[i]] >= max_att){
					max_att = obj_data.card_attack[container.card_array[i]];
					selected_card = container.card_array[i]
					selected_menupos = i;
					if(coolrandom_next_decimal() < .45){
						break;	
					}
				}
			}
		}
		
		duel_do_philosopher_activate(container,selected_menupos)
	
	}
}