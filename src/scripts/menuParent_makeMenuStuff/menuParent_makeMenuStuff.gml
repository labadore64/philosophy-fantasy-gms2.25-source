/// @description Insert description here
// You can write your code in this editor

if(!global.mouse_active){
	cancel_button_enabled = false;	
}

if(cancel_button_enabled){
	// if either aren't set, default to top right corner
	if(cancel_button_x == MENU_PARENT_CANCEL_DEFAULT_POS
		|| cancel_button_y == MENU_PARENT_CANCEL_DEFAULT_POS){
	
		cancel_button_x = x + menu_width;
		cancel_button_y = y;
	}

	// create button
	mouseHandler_add(cancel_button_x - cancel_button_width*.5,
					cancel_button_y - cancel_button_height*.5,
					cancel_button_x + cancel_button_width*.5,
					cancel_button_y + cancel_button_height*.5,
					menu_cancel, "Cancel")
}

if(help_button_enabled &&
	menu_help != ""){
	// if either aren't set, default to top right corner
	if(help_button_x == MENU_PARENT_CANCEL_DEFAULT_POS
		|| help_button_y == MENU_PARENT_CANCEL_DEFAULT_POS){
	
		help_button_x = x + menu_width - cancel_button_width*.5 - 5;
		help_button_y = y;
	}

	// create button
	
	mouseHandler_add(help_button_x - help_button_width*.5,
					help_button_y - help_button_height*.5,
					help_button_x + help_button_width*.5,
					help_button_y + help_button_height*.5,
					help_menu_do, "Help")
}