
if(!instance_exists(darkScreen)){
	
	// background
	
	if(!surface_exists(part_surf)){
		part_surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){
			surface_resize(part_surf,global.window_width,global.window_height);
		}
	}
	
	surface_set_target(part_surf)
	draw_clear_alpha(c_black,1);

	draw_set_alpha(1)
	if(duelist_bg_sprite > -1){
		draw_sprite_extended_ratio(duelist_bg_sprite,0,0,0,1,1,0,c_white,1)
	} else {
		if(global.color_gradient){
			draw_rectangle_color_ratio(0,0,800,600,
									global.screen_bgcolor[0],global.screen_bgcolor[0],
									global.screen_bgcolor[1],global.screen_bgcolor[1],
											false)
		} else {
			draw_set_color(global.screen_bgcolor[0])
			draw_rectangle_ratio(0,480,800,600,
										false)
		}
	

		gpu_set_colorwriteenable(true,true,true,false)
		if(global.menu_index > -1){
			gpu_set_blendmode(bm_add)
			if(global.menu_animate){
				draw_sprite_extended_ratio(global.menu_index,ScaleManager.spr_counter,400,300,2,2,0,$181818,1)
			} else {
				draw_sprite_extended_ratio(global.menu_index,75,400,300,2,2,0,$181818,1)
			}
			gpu_set_blendmode(bm_normal)
		}
	}

	if(duelist_img_sprite > -1){
		draw_sprite_extended_ratio(duelist_img_sprite,ScaleManager.timer_diff,
									0,0,1,1,0,c_white,1)
	}

	if(Sname > -1){
		gpu_set_blendmode(bm_add)
		if(global.particles){part_system_drawit(Sname);}
		gpu_set_blendmode(bm_normal)
	}
	surface_reset_target();

	if(obj_data.loaded_shader > - 1 && shader_enabled && global.enableShader){
		shader_set(shd_background_rainbow);

		shader_set_uniform_f(shd_vcr_counter,shader_counter)
		shader_set_uniform_f(shd_vcr_seed,shader_seed)

		shader_set_uniform_f(shd_vcr_u_uv,global.window_width,global.window_height)
	
		draw_surface(part_surf,0,0);
	
		shader_reset();
	} else {
		draw_surface(part_surf,0,0);	
	}


	
if(global.text_full && room != BattleRoom){

		
	// foreground
		
		with(selectStarterDeck){
			
			starterDeck_draw();	
		}

		gpu_set_blendmode(bm_normal)

		with(fadeOut){
			if(object_index == fadeOut){
				if(!instance_exists(TextBox)){
					if(sprite_index > -1){
						draw_sprite_extended_ratio(sprite_index,0,
													120+5+200,0,1.25,1.25,0,c_black,1)
						draw_sprite_extended_ratio(sprite_index,0,
													120+200,0,1.25,1.25,0,c_white,1)
					}
				}
				draw_fade();	
			}
		}
	
		with(fadeIn){
			if(object_index == fadeIn){
				if(!instance_exists(TextBox)){
					if(sprite_index > -1){
						draw_sprite_extended_ratio(sprite_index,0,
													120+5+200,0,1.25,1.25,0,c_black,1)
						draw_sprite_extended_ratio(sprite_index,0,
													120+200,0,1.25,1.25,0,c_white,1)
	
					}

				}

				draw_fade();	
			}
		}

	} else {

		draw_set_color(c_black)
		draw_set_alpha(1)
		draw_rectangle_ratio(0,480,800,600,false)

		menu_control_draw_bgelement();

		//with(textbox){

		//	script_execute(menu_draw);	
		//}
	
		with(fadeOut){
			if(sprite_index > -1){
				draw_sprite_extended_ratio(sprite_index,0,
											120+5,0,1,1,0,c_black,1)
				draw_sprite_extended_ratio(sprite_index,0,
											120,0,1,1,0,c_white,1)
			}
			draw_set_color(c_black)
			draw_set_alpha(1)
			draw_rectangle_ratio(0,480,800,600,false)
		
			draw_fade();	
		}
	
		with(fadeIn){
			if(sprite_index > -1){
				draw_sprite_extended_ratio(sprite_index,0,
											120+5,0,1,1,0,c_black,1)
				draw_sprite_extended_ratio(sprite_index,0,
											120,0,1,1,0,c_white,1)
			}
			draw_set_color(c_black)
			draw_set_alpha(1)
			draw_rectangle_ratio(0,480,800,600,false)
		
			draw_fade();	
		}
	}
}

draw_letterbox();