	if(!surface_exists(surf)){
		surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,global.window_width,global.window_height);
		}
	}
	
	surface_set_target(surf)
	draw_set_alpha(1)
	if(global.color_gradient){
		draw_rectangle_color_ratio(0,0,800,600,
								global.screen_bgcolor[0],global.screen_bgcolor[0],
								global.screen_bgcolor[1],global.screen_bgcolor[1],
										false)
	} else {
		draw_set_color(global.screen_bgcolor[0])
		draw_rectangle_ratio(0,480,800,600,
									false)
	}
	

	gpu_set_colorwriteenable(true,true,true,false)
	if(global.menu_index > -1){
		gpu_set_blendmode(bm_add)
		if(global.menu_animate){
			draw_sprite_extended_ratio(global.menu_index,ScaleManager.spr_counter,400,300,2,2,0,$181818,1)
		} else {
			draw_sprite_extended_ratio(global.menu_index,75,400,300,2,2,0,$181818,1)
		}
		gpu_set_blendmode(bm_normal)
	}
	if(global.particles){part_system_drawit(Sname);}
	surface_reset_target();
	
	draw_surface(surf,0,0);	