var counter = 0;

for(var i = 0; i < portrait_property_total; i++){
	if(property_enabled[i]){
		model_title[counter] = property_names[i] // name of the model element (eyes, mouth ect)
		model_id[counter] = i; // id of the model element
		model_sprite_size[counter] = array_length_2d(property_sprite,i);
		counter++;
	}
}

model_total = counter;