if(argument_count > 3){
	return string(argument[0].part_num) +":" + string(argument[0].enemy) + ":" + string(argument[1]);
}

if(instance_exists(argument[0])){
	var pos = "pos_";
	if(argument_count > 2){
		pos = "";	
	}
	var num = DUEL_PLAYER_YOU
	var you = "you";
	if(argument[0].enemy){
		num = DUEL_PLAYER_ENEMY
		you = "enemy"
	}
	return string_lower(argument[0].source_name) +":" + pos + you + ":" + string(argument[1]);
}
	
return "";	
