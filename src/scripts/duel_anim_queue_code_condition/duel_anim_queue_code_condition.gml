var argu = argument[0]; // args as json string
var cando = false;
var _if,_then,_else;

if(argument_count > 1){
	cando = true;
	_if = argument[0];
	_then = argument[1];
	_else = argument[2]
} else {

	var map = json_decode(argu);

	if(!ds_map_empty(map)){
		// reset the code position (this should only be changed inside a code execution)
		duelController.code_start = 0;
		duelController.code_pointer = 0;
		duelController.code_end = false
	
		// do your normal code down here
		_if = map[? "if"];
		_then = map[? "then"];
		_else = map[? "else"];
	
		ds_map_destroy(map)
	
		if(!is_undefined(_if) &&
			!is_undefined(_then) && 
			!is_undefined(_else)){
				cando = true;
			}
	
		ds_map_destroy(map)
		}
}
	
if(cando){
	// parse condition
	var result = 0;
		
	var cond1 = string_copy(_if,1,string_pos(" ",_if)-1);
	_if = string_delete(_if,1,string_pos(" ",_if));
	var cond2 = string_copy(_if,1,string_pos(" ",_if)-1);
	_if = string_delete(_if,1,string_pos(" ",_if));
	var cond3 = _if
		
		
	// prepare the values
	if(string_length(string_digits(cond1)) == string_length(cond1)){
		cond1 = real(cond1);
	} else {
		var value = duelController.code_values[? cond1];
		if(is_undefined(value) ||
			!is_real(value)){
			value = 0;	
		}
		cond1 = value;
	}
		
	if(string_length(string_digits(cond3)) == string_length(cond3)){
		cond3 = real(cond3);
	} else {
		var value = duelController.code_values[? cond3];
		if(is_undefined(value) ||
			!is_real(value)){
			value = 0;	
		}
		cond3 = value;
	}
		
	result = code_process_expr(cond1,cond2,cond3)
		
	// end parse condition
	var doblock = "";
	if(result){
		doblock = _then;
	} else {
		doblock = _else;
	}
	var _card = code_get_card_id();
		
	var codeblock = code_find_block(_card,doblock)
		
	if(codeblock > -1){
			
		//code_set_thisdirect(code_get_this())
			
		code_load(obj_data.card_code[_card,codeblock]);	

		// yo reset the pointer too...
		duelController.code_pointer = 0;

		// now that the code is loaded, run the code
		// execute the code
		code_execute();
			
		//duel_animation_code_complete();
	}
		
	return noone;
}

return noone;