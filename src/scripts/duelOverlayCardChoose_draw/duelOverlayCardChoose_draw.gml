with(duelSub){
	duelSub_draw()	
}

if(!surface_exists(info_surf)){
	info_surf = surface_create_access(global.letterbox_w,115*global.scale_factor)	
	info_surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(info_surf,global.letterbox_w,115*global.scale_factor);
		info_surf_update = true;
	}
}

if(info_surf_update){
	surface_set_target(info_surf)
	draw_clear_alpha(c_black,1)
	if(menupos > -1){
		if(card[menupos] > -1){
			draw_set_alpha(1)
			draw_text_transformed_noratio(30,5 ,
								obj_data.card_name[card[menupos]],4,4,0)
					
			var index = 3;
			if(obj_data.card_type[card[menupos]] == CARD_TYPE_PHILOSOPHER){
				if(obj_data.card_element[card[menupos]] == CARD_ELEMENT_MIND){
					index = 2
				} else if(obj_data.card_element[card[menupos]] == CARD_ELEMENT_MATTER){
					index = 1
				} else {
					index = 0;	
				}
			} else if(obj_data.card_type[card[menupos]] == CARD_TYPE_METAPHYS){
				index = 4
			}
		
			draw_sprite_extended_noratio(spr_card_white_type,index,
									30+15,70+5 ,
									1.2,1.2,0,c_white,1)
		
			if(obj_data.card_type[card[menupos]] == CARD_TYPE_PHILOSOPHER){
				draw_sprite_extended_noratio(spr_card_white_stat,0,
										30+90,70+5 ,
										1,1,0,c_white,1)
									
				draw_sprite_extended_noratio(spr_card_white_stat,1,
										30+230,70+5 ,
										1,1,0,c_white,1)
									
				draw_text_transformed_noratio(30+120,60+5 ,
									string(obj_data.card_attack[card[menupos]]) + "/+"
									+ string(obj_data.card_attack_overlay[card[menupos]])
									,2,2,0)
								
				draw_text_transformed_noratio(30+260,60+5 ,
									string(obj_data.card_defense[card[menupos]]) + "/+"
									+ string(obj_data.card_defense_overlay[card[menupos]])
							,2,2,0)
			}
		}
	}
	//draw_set_halign(fa_right)
	//draw_text_transformed_noratio(780,45,obj_data.card_name[card[menupos]],3,3,0)
	//draw_set_halign(fa_left)
	
	surface_reset_target()
	info_surf_update = false;
}

if(global.high_contrast){
	draw_set_alpha(1)
} else {
	draw_set_alpha(.85)
}
draw_set_color(c_black)
draw_rectangle_ratio(0,0,800,600,false)

draw_set_alpha(1)

draw_rectangle_ratio(0,0,800,100,false)

draw_set_color(global.text_fgcolor)
draw_set_halign(fa_middle)
draw_text_transformed_ratio(400,20,"Select a card\nto send to Limbo.",3,3,0)
draw_set_halign(fa_left)

for(var i = 0; i < 2; i++){
	card_draw(card_display[i]);	
}

draw_surface(info_surf,global.display_x + -5*global.scale_factor,global.display_y + 495*global.scale_factor);

if(!mouse_mode){
	draw_sprite_ext(spr_duel_cursor,ScaleManager.spr_counter*FAST,250+menupos*300,400,-global.scale_factor*2,global.scale_factor*2,0,c_white,1)
} else {
	with(duelCursor){
		draw_sprite_ext(spr_duel_cursor,0,x,y,-global.scale_factor*2,global.scale_factor*2,0,c_white,1)
	}
}
