if(!instance_exists(fadeIn)){
	if(!lock){
		if(global.menu_sounds){
			soundfxPlay(sfx_sound_chapter)
		}
	}
	lock = true;

	running_object = instance_create(0,0,fadeOut)
	running_object.time = 120;
	running_object.amount_per_cycle = 1/running_object.time;

	cancelled = true
	mouseHandler_clear()
}