for(var i = 0; i < menu_count; i++){
	if(menu_sprite[i] > -1 && option_select[i] > -1){
		option_visible_draw[i] = option_text[menu_sprite[i],option_select[i]];	
	} else {
		option_visible_draw[i] = "";	
	}
}

options_complete();

// update music sound
soundMusicVolume(global.musicVolume)

// updates full screen
window_set_fullscreen(global.fullscreen)

surface_update = true;