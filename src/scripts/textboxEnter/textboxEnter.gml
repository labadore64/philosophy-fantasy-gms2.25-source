if(!wait){
	if(ds_exists(text,ds_type_list)){
		if(display_object != -1){
			var size = ds_list_size(text);
			if(line < size){
				var cando = false;
	
				with(display_object){
					if(cutoff != string_lenger){
						cutoff = string_lenger;
						audio_stop_sound(text_sound1);
						audio_stop_sound(text_sound2);
						audio_stop_sound(text_sound3);
						audio_stop_sound(text_sound4);

					} else {
						message_current++;
						cutoff = 0;
						cando = true;
					}
				}
	
				if(cando){
					line++;
					if(line < size){
						textboxExecuteScript();
						textboxSetCurrentText();
						if(tts_speak){
							tts_say(current_text);
						}	
					} else {
						instance_destroy();	
					}
				}
	
			} else {
				instance_destroy();	
			}
		}
	}
} else {
	wait = false;	
}
RUMBLE_MOVE_MENU