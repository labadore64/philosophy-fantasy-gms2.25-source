var me = id;

with(MouseHandler){
	mouse_points[mouse_count,0] = argument[0];
	mouse_points[mouse_count,1] = argument[1];
	mouse_points[mouse_count,2] = argument[2];
	mouse_points[mouse_count,3] = argument[3];
	mouse_script[mouse_count] = argument[4];
	mouse_text[mouse_count] = argument[5]
	mouse_hovered[mouse_count] = false;
	mouse_hovered_last[mouse_count] = false;
	var test = false;
	if(argument_count >=7){
		test = argument[6]	
	}
	mouse_menupos[mouse_count] = test
	mouse_obj[mouse_count] = me;
	mouse_count++;
	mouseHandler_update_values();
}