if(menupos > 6){
	if(menupos == 7){
		if(selected){
			color_hsv[2]-=1;
			if(global.extra_sounds){
				if(!audio_exists(sound[0])){
					sound[0] = soundfxPlayLoop(sfx_sound_col_val)
					sound_released = false
				}
				sound[1] = -1;
				audio_sound_pitch(sound[0], col_pos[2]+.5)
			}
		}
	}
	if(menupos == 8){
		if(color_selected){
			color_hsv[0]-=1;
			if(global.extra_sounds){
				if(!audio_exists(sound[0])){
					sound[0] = soundfxPlayLoop(sfx_sound_col_hue)
					sound_released = false
				}
				if(!audio_exists(sound[1])){
					sound[1] = soundfxPlayLoop(sfx_sound_col_sat)
				}
				audio_sound_pitch(sound[0], col_pos[0]+.5)
				audio_sound_pitch(sound[1], col_pos[1]+.5)
			}
		}
	}
	appearanceColorMap_update_hsv();
}
surf_main_update=true