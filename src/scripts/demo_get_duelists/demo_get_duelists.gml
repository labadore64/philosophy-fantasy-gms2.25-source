
duelist_name = ""
duelist_sprite = spr_portrait_thomas;
duelist_deck = 0;
duelist_say1 = ""
duelist_say2 = ""
duelist_say3 = "";
duelist_bg_image = ""
progress_story = false
duelist_particle = titleScreen_initParticle

var itemcount = obj_data.loaded_opponent;
var loaded_duel = obj_data.loaded_duel

// lowest difficulty always picks the first deck regardless of progress
if(global.difficulty == 0){
	loaded_duel = 0;	
}

// gets the count of the items
var filename = working_directory + "resources\\opponent\\enemy";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

if(file_exists(testname)){
	ini_open(testname)
	
	duelist_name = ini_read_string_length("main","name","",IMPORT_MAX_SIZE_CARDNAME)
	duelist_sprite = asset_get_index("spr_portrait_" + ini_read_string_length("main","sprite","",IMPORT_MAX_SIZE_SPRITE))
	
	if(obj_data.free_duel){
		global.skip_this_coin = false
		duelist_say1 = ini_read_string_length("duel"+string(loaded_duel),"free_start","",50)
		duelist_say2 = ini_read_string_length("duel"+string(loaded_duel),"free_win","",50)
		duelist_say3 = ini_read_string_length("duel"+string(loaded_duel),"free_lose","",50)	
	} else {
		duelist_say1 = ini_read_string_length("duel"+string(loaded_duel),"say_start","",50)
		duelist_say2 = ini_read_string_length("duel"+string(loaded_duel),"say_win","",50)
		duelist_say3 = ini_read_string_length("duel"+string(loaded_duel),"say_lose","",50)
	}
	
	challenge_music = asset_get_index("sfx_music_"+ ini_read_string_length("duel"+string(loaded_duel),"challenge_music","",IMPORT_MAX_SIZE_SPRITE))
	obj_data.duel_music = asset_get_index("sfx_music_"+ ini_read_string_length("duel"+string(loaded_duel),"music","",IMPORT_MAX_SIZE_SPRITE))
	
	fave = card_from_name(ini_read_string_length("duel"+string(loaded_duel),"fave","",IMPORT_MAX_SIZE_CARDNAME))
	event_id = ini_read_string_length("duel"+string(loaded_duel),"event","",IMPORT_MAX_SIZE_EVENT)
	duelist_deck = ini_read_real("duel"+string(loaded_duel),"deck",0)
	duelist_bg_image = "bg_" + ini_read_string_length("duel"+string(loaded_duel),"bg","",IMPORT_MAX_SIZE_SPRITE)
	duelist_particle = asset_get_index(ini_read_string_length("duel" + string(loaded_duel),"particle","",IMPORT_MAX_SIZE_SPRITE))
	progress_story = stringToBool(ini_read_string_length("duel"+string(loaded_duel),"progress_story","false",6))
	
	if(duelist_particle == -1){
		duelist_particle = particle_dust_init
	}
	
	money_paid = ini_read_real("duel"+string(loaded_duel),"money",0)
	item_dropped = ini_read_string_length("duel"+string(loaded_duel),"item_drop","",IMPORT_MAX_SIZE_SPRITE)
	ini_close();
}