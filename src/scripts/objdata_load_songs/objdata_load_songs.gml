var songe = ds_list_create()

ds_list_add(songe,sfx_music_title)

ds_list_add(songe,sfx_music_main_menu)
ds_list_add(songe,sfx_music_deck_construction)
ds_list_add(songe,sfx_music_review_mode)

ds_list_add(songe,sfx_music_university)

ds_list_add(songe,sfx_music_fanfare_strategy)
ds_list_add(songe,sfx_music_fanfare_mind)
ds_list_add(songe,sfx_music_fanfare_tough)
ds_list_add(songe,sfx_music_fanfare_prepare)

ds_list_add(songe,sfx_music_freebattle)
ds_list_add(songe,sfx_music_battle1)
ds_list_add(songe,sfx_music_battle2)
ds_list_add(songe,sfx_music_battle3)
ds_list_add(songe,sfx_music_battle4)
ds_list_add(songe,sfx_music_battle5)
ds_list_add(songe,sfx_music_battle6)

ds_list_add(songe,sfx_music_victory)

var sizer = ds_list_size(songe);
song_count = sizer;
for(var i = 0; i < sizer;i++){
	song[i] = songe[|i]	
}

// now do sfx

ds_list_clear(songe)

ds_list_add(songe,sfx_sound_menu_default)
ds_list_add(songe,sfx_sound_menu_cancel)
ds_list_add(songe,sfx_sound_menu_select)
ds_list_add(songe,sfx_sound_menu_backspace)
ds_list_add(songe,sfx_sound_menu_twinkle)
ds_list_add(songe,sfx_sound_menu_keyboard)
ds_list_add(songe,sfx_sound_help_open)
ds_list_add(songe,sfx_start1)
ds_list_add(songe,sfx_start2)
ds_list_add(songe,sfx_start3)
ds_list_add(songe,sfx_sound_deck_add)
ds_list_add(songe,sfx_sound_deck_remove)
ds_list_add(songe,sfx_sound_view_card)
ds_list_add(songe,sfx_sound_duel_window)
ds_list_add(songe,sfx_sound_question)
ds_list_add(songe,sfx_sound_confirm)
ds_list_add(songe,sfx_sound_exit_game)
ds_list_add(songe,sfx_sound_menu_options)
ds_list_add(songe,sfx_sound_chapter)
ds_list_add(songe,sfx_sound_textbox)
ds_list_add(songe,sfx_sound_coin_start)
ds_list_add(songe,sfx_sound_coin_flip)
ds_list_add(songe,sfx_sound_coin_result)
ds_list_add(songe,sfx_sound_play_card)
ds_list_add(songe,sfx_sound_deck_shuffle)
ds_list_add(songe,sfx_sound_duel_play_card)
ds_list_add(songe,sfx_sound_duel_overlay_card)
ds_list_add(songe,sfx_sound_duel_bench_card)
ds_list_add(songe,sfx_sound_duel_activate_card)
ds_list_add(songe,sfx_sound_duel_hand_card)
ds_list_add(songe,sfx_sound_duel_deck_card)
ds_list_add(songe,sfx_sound_duel_limbo_card)
ds_list_add(songe,sfx_sound_duel_theory_card)
ds_list_add(songe,sfx_sound_duel_praxis_card)
ds_list_add(songe,sfx_sound_duel_select_card)
ds_list_add(songe,sfx_sound_duel_return_control)
ds_list_add(songe,sfx_sound_duel_menu)
ds_list_add(songe,sfx_sound_battle_menu_open)
ds_list_add(songe,sfx_sound_battle_menu_close)
ds_list_add(songe,sfx_sound_battle_menu_nav)
ds_list_add(songe,sfx_sound_duel_details)
ds_list_add(songe,sfx_sound_hit)
ds_list_add(songe,sfx_sound_super_effective)
ds_list_add(songe,sfx_sound_not_effective)
ds_list_add(songe,sfx_sound_HP_left)
ds_list_add(songe,sfx_sound_HP_right)
ds_list_add(songe,sfx_sound_duel_phase_change)
ds_list_add(songe,sfx_sound_duel_dialog)
ds_list_add(songe,sfx_sound_duel_end_turn)
ds_list_add(songe,sfx_sound_duel_start)
ds_list_add(songe,sfx_sound_duel_victory)
ds_list_add(songe,sfx_sound_duel_defeat)
ds_list_add(songe,sfx_sound_pack_change)
ds_list_add(songe,sfx_sound_pack_select)
ds_list_add(songe,sfx_sound_duel_ax_select)
ds_list_add(songe,sfx_sound_col_hue)
ds_list_add(songe,sfx_sound_col_sat)
ds_list_add(songe,sfx_sound_col_val)

var sizer = ds_list_size(songe);
sound_count = sizer;
for(var i = 0; i < sizer;i++){
	sound[i] = songe[|i]	
}

ds_list_destroy(songe)