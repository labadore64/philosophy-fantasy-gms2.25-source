/// @description - plays a sound effect
/// @function soundfxPlay(sound)
/// @param sound

	audio_stop_sound(argument[0]);
	return audio_play_sound(argument[0],1,true);