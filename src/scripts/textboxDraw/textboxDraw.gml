if(global.text_full && room != BattleRoom){
	if(portrait_sprite != -1){
		draw_sprite_extended_ratio(portrait_sprite,0,portrait_x+200+5,0,1.25,1.25,0,c_black,1)
		draw_sprite_extended_ratio(portrait_sprite,0,portrait_x+200,0,1.25,1.25,0,c_white,1)


	}

	if(active){

	//spr_portrait_mystery_man
		var heiiii = 80;
		var spacerrr = 40;
		var lengeee = string_length(name);

		draw_set_colour($666666);
		
		draw_set_alpha(.65);
		draw_rectangle_ratio(10,70,500,450,false)
		draw_set_alpha(1);

		draw_set_colour(c_white);
		
		if(show_chara_name){
			draw_set_halign(fa_left);
			draw_set_color(global.text_fgcolor)
			draw_set_font(global.largeFont)
			draw_text_transformed_ratio(30,heiiii+10,name,2,2,0)
			draw_set_font(global.textFont)
		}
		

		with(display_object){
			if(object_index == obj_dialogue){
				objdialogDraw();	
			}
		}
	}

} else {
	if(portrait_sprite != -1){
		draw_sprite_extended_ratio(portrait_sprite,0,portrait_x+5,0,1,1,0,c_black,1)
		draw_sprite_extended_ratio(portrait_sprite,0,portrait_x,0,1,1,0,c_white,1)

		gpu_set_colorwriteenable(true,true,true,false)

	}

	if(active){
	draw_set_color(global.text_bgcolor[0])
	draw_set_alpha(1);
	if(global.color_gradient){
		draw_rectangle_color_ratio(0,480,800,600,
									global.text_bgcolor[0],global.text_bgcolor[0],
									global.text_bgcolor[1],global.text_bgcolor[1],
									false)
	} else {
		draw_set_color(global.text_bgcolor[0])
		draw_rectangle_ratio(0,480,800,600,
									false)
	}

	//spr_portrait_mystery_man
		var heiiii = 430;
		var spacerrr = 30;
		var lengeee = string_length(name);

		draw_set_colour(c_white);
		if(show_chara_name){
			draw_set_color(global.text_bgcolor[0])
			draw_rectangle_ratio(0,heiiii-4,15 + spacerrr*lengeee+4,480,false)
			draw_set_halign(fa_left)
			draw_rectangle_ratio(0,heiiii,15 + spacerrr*lengeee,480,false)
			draw_set_color(global.text_fgcolor)
			draw_set_font(global.largeFont)
			draw_text_transformed_ratio(15,heiiii+10,name,2,2,0)
			draw_set_font(global.textFont)
		}

		with(display_object){
			if(object_index == obj_dialogue){
				objdialogDraw();	
			}
		}
	}
}

menuParent_draw_cancel_button()
draw_letterbox();