var argu = argument[0]; // args as json string

var map = json_decode(argu);

if(!ds_map_empty(map)){
	// do your normal code down here
	var _sound = map[? "sound"];
	var _pitch = map[? "pitch"];
	
	ds_map_destroy(map)
	
	if(!is_undefined(_sound &&
		!is_undefined(_pitch))){
			
		var sound = soundfxPlay(_sound)
		audio_sound_pitch(sound,_pitch);
		return noone
	}

}

return noone;