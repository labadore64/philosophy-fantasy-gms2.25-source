// reset to default.
animation = CARD_ANIMATION_NONE;
animation_time = -1;
x_scale = 1;
y_scale = 1;
animate_flip = false;

point_scale_x[0] = 1;
point_scale_x[1] = 1;
point_scale_x[2] = 1;
point_scale_x[3] = 1;
point_scale_y[0] = 1;
point_scale_y[1] = 1;
point_scale_y[2] = 1;
point_scale_y[3] = 1;

if(surface_exists(card_surface)){
	surface_free(card_surface);	
}

card_calculate_points();