// 0-1 = x,y
// 2-3 = w,h
// 4,5 = field part, menupos

with(duelController){
	mouse_card[mouse_card_count,0] = argument[0]-argument[2]*.5
	mouse_card[mouse_card_count,1] = argument[1]-argument[3]*.5
	mouse_card[mouse_card_count,2] = argument[0]+argument[2]*.5
	mouse_card[mouse_card_count,3] = argument[1]+argument[3]*.5
	mouse_card_element[mouse_card_count] = argument[4]
	mouse_card_menupos[mouse_card_count] = argument[5]
	mouse_card_count++
	mouse_card_updated = true;
}