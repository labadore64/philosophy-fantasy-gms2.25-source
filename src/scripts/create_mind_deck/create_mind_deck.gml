var obj = instance_create(argument[0],argument[1],StarterDeck);
with(obj){
	image_blend = $FF7F7F
	card_sprite = card_from_name("freud")
	bg_sprite = obj_data.card_sprite_bg[card_from_name("freud")]
	type_sprite = spr_typeMind

	surface_update = false;
	surf = -1;

	card_width = 325
	card_height = 440

	name = "Psych"
	subtitle = "A deck based\naround offense\nand speed."

	scale = .65
}
return obj;