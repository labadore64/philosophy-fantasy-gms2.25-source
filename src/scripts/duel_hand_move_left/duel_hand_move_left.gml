if(card_array_size > 0){

	menupos--;

	if(menupos < 0){
		menupos = card_array_size-1	
	}
} else {
	menupos = 0;	
}

if(enemy){
	var start_x = 420-105;
	var end_x = -60-105
	var spacer = (end_x-start_x)/5;
	var posy = -330
	
	if(card_array_size > 5){
		spacer = (end_x-start_x)/(card_array_size+1);
	}
	duel_hand_update_cursor_player(start_x,posy,spacer);
} else {
	var start_x = -60;
	var end_x = 420;
	var spacer = (end_x-start_x)/5;
	var posy = 340+240
	
	if(card_array_size > 5){
		spacer = (end_x-start_x)/(card_array_size+1);
	}
	duel_hand_update_cursor_player(start_x,posy,spacer);
}

// tts
duelField_tts_read();
