var cond1 = argument[0];
var cond2 = argument[1];
var cond3 = argument[2];
var compare = false;

var result = 0;

// now test for each condition
if(cond2 == "=="){
	result = (cond1 == cond3);
	compare = true;
} else if (cond2 == "<"){
	result = (cond1 < cond3);
	compare = true;
} else if (cond2 == ">"){
	compare = true;
	result = (cond1 > cond3);
} else if (cond2 == "<="){
	compare = true;
	result = (cond1 <= cond3);
} else if (cond2 == ">="){
	compare = true;
	result = (cond1 >= cond3);
} else if(cond2 == "!="){
	compare = true;
	result = (cond1 != cond3);
} else if(cond2 == "&&"){
	compare = true;
	result = (cond1 && cond3);
} else if(cond2 == "||"){
	compare = true;
	result = (cond1 || cond3);
} else if(cond2 == "+"){
	result = (cond1 + cond3);
} else if(cond2 == "-"){
	result = (cond1 - cond3);
} else if(cond2 == "*"){
	result = (cond1 * cond3);
} else if(cond2 == "/"){
	if(cond3 != 0){
		result = floor(cond1 / cond3);
	}
} else if(cond2 == "%"){
	if(cond3 != 0){
		result = (cond1 % cond3);
	}
}
	
if(compare){
	if(result){
		result = 1;	
	} else {
		result = 0;	
	}
}
		
return result;