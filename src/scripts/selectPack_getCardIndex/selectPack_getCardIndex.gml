var index = 5;
if(argument[0] > -1){
	if(obj_data.card_type[argument[0]] == CARD_TYPE_PHILOSOPHER){
		index = obj_data.card_element[argument[0]];
	} else if (obj_data.card_type[argument[0]] == CARD_TYPE_EFFECT){
		index = 3;
	} else {
		index = 4;
	}
}

return index;