phase_current = 0;	
active_player=!active_player;
turns++;
duelController.ai_mode = false;

reset_card_used_this_turn();
duelField_counters_reset();	

status_end_turn()

with(duelField){
	var size = ds_map_size(status_effects)
	var key = ds_map_find_first(status_effects)
	for (var i = 0; i < size; i++)
	{
		if(!is_undefined(key)){
			var value = status_effects[? key]
			if(!is_undefined(value)){
				if(value > 0){
					var pos = string_pos(":",key);
					var ids = key;
					var args = ""
					if(pos > -1){
						ids = string_copy(key,1,pos-1);
						args = string_copy(key,pos+1,string_length(key)-pos);
					}

					if(value == 1){
						// remove and trigger the status_end script for this status
						ds_map_delete(status_effects,key);
						
						var script = asset_get_index("status_end_" + ids);
						if(script > -1){
							script_execute(script);	
						}
					} else {
						// remove and trigger the status_end_turn script for this status.
						status_effects[? key] = value-1;
						var script = asset_get_index("status_end_turn_" + ids);
						if(script > -1){
							script_execute(script);	
						}
					}
				}
			}
		
			key = ds_map_find_next(status_effects, key);
		}
	}
}