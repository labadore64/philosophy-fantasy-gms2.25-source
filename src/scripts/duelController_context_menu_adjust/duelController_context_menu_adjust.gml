var sizer = 0;
var temp = 0
for(var i = 0; i < menu_count; i++){
	temp = string_width(menu_name[i])*2;
	if(temp > sizer){
		sizer = temp;
	}
}
				
// adjust x position
x = duelController.context_menu_x
y = duelController.context_menu_y
				
menu_width = sizer+100
menu_height += 10
option_x_offset= 20
				

if(x < 20+60+5){
	x = 20+60+5
} else if(x+menu_width > 800- 20-5){
	x = 800 - 20 - menu_width-5
}
				
if(global.tooltip_type != 1){
	if(y < 40){
		y = 40
	} else if(y+menu_height > 495 - 40){
		y = 495 - 40 - menu_height
	}
} else {
	if(y < 20){
		y = 20
	} else if(y+menu_height > 600 - 40){
		y = 600 - 40 - menu_height
	}
}
with(duelController.context_menu){
	instance_destroy();	
}
duelController.context_menu = id