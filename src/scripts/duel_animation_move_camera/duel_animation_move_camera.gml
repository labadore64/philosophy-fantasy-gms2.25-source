if(!duelController.ai_mode){
	var xx = argument[0];
	var yy = argument[1]
	var scale = argument[2]
	var time = argument[3]
	
	if(global.camera_track == CAMERA_TRACK_NONE){
		xx = 350
		yy = 290
		scale = 5
		time = 10
	}

	// check camera
	if(floor(xx) != floor(duelCamera.x) ||
		floor(yy) != floor(duelCamera.y) ||
		floor(scale*1000) != floor(duelCamera.scale*1000)){

		if(!instance_exists(duelController.animation_stack_run) ||
			duelController.animation_stack_run.object_index == animationQueue){
		
			var queue = -1;
			var obj = noone;
	
			// set the queue if the running object is already an animation
			if(instance_exists(duelController.animation_stack_run)){
				if(duelController.animation_stack_run.object_index == animationQueue){
					obj = duelController.animation_stack_run
					queue = obj.queue;
				} else {
					queue = ds_list_create();
					obj = instance_create(0,0,animationQueue);
					obj.queue = queue;
				}
			} else {
				queue = ds_list_create();
				obj = instance_create(0,0,animationQueue);
				obj.queue = queue;
			}
			if(queue > -1 && ds_exists(queue,ds_type_list)){
				duelController.running_obj = obj;
	
				var counter = ds_list_size(queue);


				var json = ds_map_create();
				ds_map_add(json,"x",xx)
				ds_map_add(json,"y",yy)
				ds_map_add(json,"scale",scale)
				ds_map_add(json,"time",time)
		
				duel_animation_pack(obj,queue,json,duel_anim_queue_move_camera)
			}
		}
	}
}